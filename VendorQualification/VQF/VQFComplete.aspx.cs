﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using VMSDAL;

#region Comments
/* ***********************************************************************************************************************************************************************
 * 001 G. VERA 06/02/2014: Toggle message based on Design ocnsultant or not
 * 
 * 
 * 
 *************************************************************************************************************************************************************************/
#endregion

public partial class VQF_VQFComplete : System.Web.UI.Page
{
    Common objCommon = new Common();
    public DataSet Vendordetails = new DataSet();
    string FirstName, Lastname,State;
    const string DESIGN_TOP_MESSAGE = "Thank you for registering with Haskell’s Vendor Management System. Your business will now<br/>"+
                                                                    "be available for use by any of our project teams.";

    protected void Page_Load(object sender, EventArgs e)
    {
        VQFMenu.GetVendorStatus();
        if (VQFMenu.VendorStatus == 1 || VQFMenu.VendorStatus == 3)
        {
            ImageButton imgAttach = (ImageButton)VQFMenu.FindControl("imbAttch");
            imgAttach.Visible = false;
        }


        if (!(string.IsNullOrEmpty(Convert.ToString(Session["VendorId"])) || Convert.ToString(Session["VendorId"]) == "0"))
       {
           long VendorID = Convert.ToInt64(Session["VendorId"].ToString());     //001
           Vendordetails = objCommon.GetVendorDetails(VendorID);                //001
           bool IsDesignConsultant = (new clsCompany().GetTypeOfCompany(VendorID) == TypeOfCompany.DesignConsultant);
           if (IsDesignConsultant)
           {
               lblTopMessage.Text = DESIGN_TOP_MESSAGE;
           }

        if (Vendordetails.Tables[0].Rows.Count > 0)
        {
            lblCompanyname.Text = Vendordetails.Tables[0].Rows[0]["Company"].ToString();
            lblSubmitDate.Text = Convert.ToDateTime(Vendordetails.Tables[0].Rows[0]["LastModifiedDT"]).ToShortDateString();
            FirstName = Vendordetails.Tables[0].Rows[0]["FirstName"].ToString();
            Lastname = Vendordetails.Tables[0].Rows[0]["LastName"].ToString();
            lblUserName.Text = FirstName + " " + Lastname;
            lblAddress.Text = Vendordetails.Tables[0].Rows[0]["Address"].ToString(); ;
            lblCity.Text = Vendordetails.Tables[0].Rows[0]["City"].ToString();
           
            State = Vendordetails.Tables[0].Rows[0]["FK_State"].ToString(); 
            if (lblState.Text != "0")
            {
                 if (lblState.Text == "Other")
                 {
                lblState.Text = Vendordetails.Tables[0].Rows[0]["OtherState"].ToString();
                 }
                 else
                 {
                lblState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(State)).Tables[0].Rows[0].ItemArray[1]);
                 }
            }
            else
            {
                lblState.Text = "";
            }

            lblZIP.Text = Vendordetails.Tables[0].Rows[0]["ZipCode"].ToString();
            lblPhone.Text = Vendordetails.Tables[0].Rows[0]["Phone"].ToString();
            lblFax.Text = Vendordetails.Tables[0].Rows[0]["Fax"].ToString();
            lblEmail.Text = Vendordetails.Tables[0].Rows[0]["Email"].ToString();
            lblTrackingNumber.Text = Vendordetails.Tables[0].Rows[0]["TrackingNumber"].ToString();
        }
        }
    }
}
