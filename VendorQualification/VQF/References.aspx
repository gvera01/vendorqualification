﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="References.aspx.cs" Inherits="VQF_References" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>References</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <script src="../Script/jquery.js" type="text/javascript"></script>
    <style type="text/css">
        body
        {
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbsubmit">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:VQFTopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:SideMenu ID="VQFMenu" runat="server" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="bodytextbold_right">
                                                    Welcome
                                                    <asp:Label ID="lblwelcome" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    Vendor <span style="letter-spacing: 1px">Qualification</span> Form - References
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <asp:HiddenField ID="hdnVendorStatus" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="updPanel" runat="server">
                                                        <ContentTemplate>
                                                            <Ajax:Accordion ID="accReferences" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
                                                                HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                                                                FadeTransitions="true" FramesPerSecond="40" TransitionDuration="250" AutoSize="none"
                                                                RequireOpenedPane="true" SuppressHeaderPostbacks="true">
                                                                <Panes>
                                                                    <Ajax:AccordionPane ID="apnProject" runat="server">
                                                                        <Header>
                                                                            Project References
                                                                        </Header>
                                                                        <Content>
                                                                            <table align="center" border="0" cellpadding="5" cellspacing="5" width="100%" class="searchtableclass">
                                                                                <tr>
                                                                                    <td class="left_border formtdbold_rt">
                                                                                        Please list three <span class="formtdboldHighLight">NON-HASKELL</span> projects <u>completed</u>
                                                                                        within the last three years
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="sectionpad">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="keyheader" colspan="4">
                                                                                                                Reference One &nbsp;&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="4" class="tdheight">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt" colspan="4">
                                                                                                                <asp:CheckBox ID="chkNA1" runat="server" Text="N/A" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trExplain1" runat="server" style="display: none">
                                                                                                            <td class="formtdrt" colspan="4">
                                                                                                                <span class="mandatorystar">*</span>Please Explain
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trtxtExplain1" runat="server" style="display: none">
                                                                                                            <td class="formtdrt" colspan="4">
                                                                                                                <asp:TextBox ID="txtExplain1" runat="server" TextMode="MultiLine" CssClass="txtboxlarge"
                                                                                                                    onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                    onKeyUp="javascript:textCounter(this,1000,'yes');"></asp:TextBox>
                                                                                                                <asp:CustomValidator ID="cusvExplain1" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                    runat="server" SetFocusOnError="true" OnServerValidate="cusvExplain1_ServerValidate"
                                                                                                                    EnableClientScript="true">
                                                                                                                </asp:CustomValidator>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt" width="40%">
                                                                                                                <span class="mandatorystar">*</span>Project Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="30%">
                                                                                                                <span class="mandatorystar">*</span>Country
                                                                                                            </td>
                                                                                                            <td class="formtdrt" colspan="2">
                                                                                                                <span class="mandatorystar">*</span>City
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxlarge" ID="txtProjectNameOne" runat="server" MaxLength="100"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <asp:CustomValidator ID="cusvProjectNameOne" OnServerValidate="cusvProjectNameOne_ServerValidate"
                                                                                                                    ValidationGroup="ProjectReferences" Display="None" runat="server" SetFocusOnError="true"
                                                                                                                    EnableClientScript="true">
                                                                                                                </asp:CustomValidator>
                                                                                                                <%--<Ajax:FilteredTextBoxExtender ID="ftxtProjectNameOne" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" "
                                                                                           TargetControlID="txtProjectNameOne">
                                                                                         </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtProjectNameOne" TargetControlID="txtProjectNameOne"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
                                                                                                                        InvalidChars="_-#.$">
                                                                                                                </Ajax:FilteredTextBoxExtender>--%>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:DropDownList  ID="ddlCountryOne" runat="server" CssClass="ddlboxCountry"
                                                                                                                    OnSelectedIndexChanged="ddlRefCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                            <td class="formtdrt" colspan="2">
                                                                                                                <asp:TextBox ID="txtCity" runat="server" CssClass="txtboxmedium" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                                                                <%--<Ajax:FilteredTextBoxExtender ID="ftxtcity" runat="server" FilterType="UppercaseLetters,LowercaseLetters"
                                                                                            TargetControlID="txtCity">
                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                <asp:CustomValidator ID="cusvCity1" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                    runat="server" SetFocusOnError="true" OnServerValidate="cusvCity1_ServerValidate"
                                                                                                                    EnableClientScript="true">
                                                                                                                </asp:CustomValidator>
                                                                                                                <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtcity" TargetControlID="txtCity"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>State <span class="spnState1" id="trOther1lbl"
                                                                                                                    runat="server" style="display: none">Please Specify State </span>
                                                                                                            </td>
                                                                                                            <td class="formtdrt" colspan="3">
                                                                                                                <span class="mandatorystar">*</span>Completion Date
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:DropDownList ID="ddlStateOne" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                    TabIndex="0">
                                                                                                                </asp:DropDownList>
                                                                                                                <span id="trOther1" runat="server" style="display: none">&nbsp; &nbsp;<asp:TextBox
                                                                                                                    ID="txtOtherStateOne" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                                    <asp:CustomValidator ID="cusvOtherStateOne" runat="server" ValidationGroup="ProjectReferences"
                                                                                                                        Display="None" OnServerValidate="CusvOtherStateOne_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateOne" TargetControlID="txtOtherStateOne"
                                                                                                                        FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                        InvalidChars="_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </span>
                                                                                                            </td>
                                                                                                            <td class="formtdrt" colspan="2">
                                                                                                                <asp:DropDownList ID="ddlMonth1" runat="server" CssClass="ddlbox" TabIndex="0">
                                                                                                                </asp:DropDownList>
                                                                                                                &nbsp;
                                                                                                                <asp:DropDownList ID="ddlYear1" runat="server" CssClass="ddlbox" TabIndex="0">
                                                                                                                </asp:DropDownList>
                                                                                                                <asp:CustomValidator ID="cusvMonth1" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                    runat="server" OnServerValidate="cusvMonth1_ServerValidate">
                                                                                                                </asp:CustomValidator>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td width="60%">
                                                                                                                <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <span class="mandatorystar">*</span>Approximate Contract Value $
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <span class="mandatorystar">*</span> <span id="spnProjectReferenceTypeOne" runat="server">
                                                                                                                                General Contractor</span>
                                                                                                                            <asp:DropDownList CssClass="ddlbox" TabIndex="-1" ID="ddlProjectReferenceTypeOne"
                                                                                                                                runat="server">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt" valign="top">
                                                                                                                            <asp:TextBox CssClass="txtbox1" ID="txtApproximateContractAmtOne" runat="server"
                                                                                                                                MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtApproximateContractAmtOne" runat="server" TargetControlID="txtApproximateContractAmtOne"
                                                                                                                                FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                                                            <asp:CustomValidator ID="cusvApproximate1" OnServerValidate="cusvApproximate1_ServerValidate"
                                                                                                                                ValidationGroup="ProjectReferences" Display="None" runat="server" SetFocusOnError="true"
                                                                                                                                EnableClientScript="true">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" valign="top">
                                                                                                                            <asp:TextBox CssClass="txtbox" ID="txtContractorNameOne" runat="server" MaxLength="50"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <asp:CustomValidator ID="cusvContractorNameOne" OnServerValidate="cusvContractorNameOne_ServerValidate"
                                                                                                                                ValidationGroup="ProjectReferences" Display="None" runat="server" SetFocusOnError="true"
                                                                                                                                EnableClientScript="true">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <span class="mandatorystar">*</span>Contact Name
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <span class="mandatorystar">*</span>Contact Phone Number
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt" valign="top">
                                                                                                                            <asp:TextBox CssClass="txtbox" ID="txtCtrContactNameOne" runat="server" MaxLength="50"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <%--<Ajax:FilteredTextBoxExtender ID="ftxtCtrContactNameOne" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" "
                                                                                            TargetControlID="txtCtrContactNameOne">
                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            <asp:CustomValidator ID="cusvContactName1" OnServerValidate="cusvContactName1_ServerValidate"
                                                                                                                                ValidationGroup="ProjectReferences" Display="None" runat="server" SetFocusOnError="true"
                                                                                                                                EnableClientScript="true">
                                                                                                                            </asp:CustomValidator>
                                                                                                                            <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtCtrContactNameOne" TargetControlID="txtCtrContactNameOne"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" nowrap="nowrap" valign="top">
                                                                                                                            <asp:TextBox CssClass="txtboxsmall" ID="txtMainPhnoOne" runat="server" MaxLength="3"
                                                                                                                                TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtMainPhnoOne" runat="server" TargetControlID="txtMainPhnoOne"
                                                                                                                                FilterType="Numbers">
                                                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                                                            <asp:TextBox CssClass="txtboxsmall" ID="txtMainPhnoTwo" runat="server" MaxLength="3"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <span id="spnMainPhnoTwo" runat="server">-</span>
                                                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtMainPhnoTwo" runat="server" TargetControlID="txtMainPhnoTwo"
                                                                                                                                FilterType="Numbers">
                                                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                                                            <asp:TextBox CssClass="txtboxsmall" ID="txtMainPhnoThree" runat="server" MaxLength="4"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtMainPhnoThree" runat="server" TargetControlID="txtMainPhnoThree"
                                                                                                                                FilterType="Numbers">
                                                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                                                            <asp:CustomValidator ID="cusvrefMainPhno" runat="server" Display="None" ValidationGroup="ProjectReferences"
                                                                                                                                EnableClientScript="false" OnServerValidate="CusvrefMainPhno_ServerValidate"></asp:CustomValidator>
                                                                                                                            <div id="divMainPhnoTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                Country Code / Area + Phone</div>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                            <td width="40%" valign="top">
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <span class="mandatorystar">*</span>Please give a brief description of work completed
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox CssClass="txtboxlarge1" ID="txtWorkCompletedOne" runat="server" TextMode="MultiLine"
                                                                                                                                onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'Yes');"
                                                                                                                                onFocus="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                                                            <asp:CustomValidator ID="cusvWorkComplated1" ValidationGroup="ProjectReferences"
                                                                                                                                Display="None" runat="server" SetFocusOnError="true" OnServerValidate="cusvWorkComplated1_ServerValidate"
                                                                                                                                EnableClientScript="true">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trRefSpeaksEnglishOne" runat="server">
                                                                                                <td class="formtdrt" align="left" valign="middle">
                                                                                                    Speaks English?
                                                                                                    <asp:DropDownList ID="ddlRefSpeaksEnglishOne" runat="server" CssClass="ddlbox">
                                                                                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="right">
                                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="keyheader" colspan="4">
                                                                                                                            Reference Two &nbsp;&nbsp;
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td colspan="4" class="tdheight">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:CheckBox ID="chkNA2" runat="server" Text="N/A" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="trExplain2" runat="server" style="display: none">
                                                                                                                        <td class="formtdrt">
                                                                                                                            <span class="mandatorystar">*</span>Please Explain
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="trtxtExplain2" runat="server" style="display: none">
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox ID="txtExplain2" runat="server" TextMode="MultiLine" CssClass="txtboxlarge"
                                                                                                                                onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                                onKeyUp="javascript:textCounter(this,1000,'yes');"></asp:TextBox>
                                                                                                                            <asp:CustomValidator ID="cusvExplain2" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" SetFocusOnError="true" OnServerValidate="cusvExplain2_ServerValidate"
                                                                                                                                EnableClientScript="true">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt" width="40%">
                                                                                                                            <span class="mandatorystar">*</span>Project Name
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" width="30%">
                                                                                                                            <span class="mandatorystar">*</span>Country
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            <span class="mandatorystar">*</span>City
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox CssClass="txtboxlarge" ID="txtProjectNameTwo" runat="server" MaxLength="100"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <asp:CustomValidator ID="cusvProjectNameTwo" OnServerValidate="cusvProjectNameTwo_ServerValidate"
                                                                                                                                ValidationGroup="ProjectReferences" Display="None" runat="server" SetFocusOnError="true"
                                                                                                                                EnableClientScript="true">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList  ID="ddlCountryTwo" runat="server" CssClass="ddlboxCountry"
                                                                                                                                OnSelectedIndexChanged="ddlRefCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            <asp:TextBox ID="txtCityTwo" runat="server" CssClass="txtboxmedium" MaxLength="50"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <asp:CustomValidator ID="cusvCity2" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" SetFocusOnError="true" OnServerValidate="cusvCity2_ServerValidate"
                                                                                                                                EnableClientScript="true">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <span class="mandatorystar">*</span>State <span class="spnState" id="trOther2lbl"
                                                                                                                                runat="server" style="display: none">Please Specify State </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="3">
                                                                                                                            <span class="mandatorystar">*</span>Completion Date
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList ID="ddlStateTwo" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                                TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <span id="trOther2" runat="server" style="display: none">
                                                                                                                                <asp:TextBox ID="txtOtherStateTwo" runat="server" CssClass="txtbox" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <asp:CustomValidator ID="cusvOtherStateTwo" runat="server" ValidationGroup="ProjectReferences"
                                                                                                                                    Display="None" OnServerValidate="CusvOtherStateTwo_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtOtherStateTwo" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                                    ValidChars=" ,@" TargetControlID="txtOtherStateTwo">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="3">
                                                                                                                            <asp:DropDownList ID="ddlMonth2" CssClass="ddlbox" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            &nbsp;
                                                                                                                            <asp:DropDownList ID="ddlYear2" CssClass="ddlbox" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <asp:CustomValidator ID="cusvMonth2" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" OnServerValidate="cusvMonth2_ServerValidate">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td width="60%">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <span class="mandatorystar">*</span>Approximate Contract Value $
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" colspan="2" valign="top">
                                                                                                                                        <span class="mandatorystar">*</span> <span id="spnProjectReferenceTypeTwo" runat="server">
                                                                                                                                            General Contractor</span>
                                                                                                                                        <asp:DropDownList CssClass="ddlbox" TabIndex="-1" ID="ddlProjectReferenceTypeTwo"
                                                                                                                                            runat="server">
                                                                                                                                        </asp:DropDownList>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox1" ID="txtApproximateContractAmtTwo" runat="server"
                                                                                                                                            MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtdApproximateContractAmtTwo" runat="server"
                                                                                                                                            TargetControlID="txtApproximateContractAmtTwo" FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0"
                                                                                                                                            InvalidChars="A-Z,a-z,-,.,">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:CustomValidator ID="cusvApproximate2" OnServerValidate="cusvApproximate2_ServerValidate"
                                                                                                                                            ValidationGroup="ProjectReferences" Display="None" runat="server" SetFocusOnError="true"
                                                                                                                                            EnableClientScript="true">
                                                                                                                                        </asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" colspan="2" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtContractorNameTwo" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="cusvContractorNameTwo" OnServerValidate="cusvContractorNameTwo_ServerValidate"
                                                                                                                                            ValidationGroup="ProjectReferences" Display="None" runat="server" SetFocusOnError="true"
                                                                                                                                            EnableClientScript="true">
                                                                                                                                        </asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span class="mandatorystar">*</span>Contact Name
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span class="mandatorystar">*</span>Contact Phone Number
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtCtrContactNameTwo" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <%-- <Ajax:FilteredTextBoxExtender ID="ftxtCtrContactNameTwo" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCtrContactNameTwo">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                                        <asp:CustomValidator ID="cusvContactName2" OnServerValidate="cusvContactName2_ServerValidate"
                                                                                                                                            ValidationGroup="ProjectReferences" Display="None" runat="server" SetFocusOnError="true"
                                                                                                                                            EnableClientScript="true">
                                                                                                                                        </asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" nowrap="nowrap">
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtSecondPhnoOne" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        -
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtSecondPhnoOne" runat="server" TargetControlID="txtSecondPhnoOne"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtSecondPhnoTwo" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <span id="spnSecondPhnoTwo" runat="server">-</span>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtSecondPhnoTwo" runat="server" TargetControlID="txtSecondPhnoTwo"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtSecondPhnoThree" runat="server" MaxLength="4"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtSecondPhnoThree" runat="server" TargetControlID="txtSecondPhnoThree"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:CustomValidator ID="cusvrefSecondPhnoTwo" runat="server" Display="None" ValidationGroup="ProjectReferences"
                                                                                                                                            EnableClientScript="false" OnServerValidate="CusvrefSecondPhnoTwo_ServerValidate"></asp:CustomValidator>
                                                                                                                                        <div id="divSecondPhnoTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                            Country Code / Area + Phone</div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                        <td width="40%" valign="top">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" colspan="2" width="48%" valign="top">
                                                                                                                                        <span class="mandatorystar">*</span>Please give a brief description of work completed
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" colspan="2" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtboxlarge1" ID="txtWorkCompletedTwo" runat="server" TextMode="MultiLine"
                                                                                                                                            onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            onFocus="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="cusvWorkComplated2" ValidationGroup="ProjectReferences"
                                                                                                                                            Display="None" runat="server" SetFocusOnError="true" OnServerValidate="cusvWorkComplated2_ServerValidate"
                                                                                                                                            EnableClientScript="true">
                                                                                                                                        </asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trRefSpeaksEnglishTwo" runat="server">
                                                                                                            <td class="formtdrt" align="left" valign="middle">
                                                                                                                Speaks English?
                                                                                                                <asp:DropDownList ID="ddlRefSpeaksEnglishTwo" runat="server" CssClass="ddlbox">
                                                                                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="right">
                                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="keyheader" colspan="4">
                                                                                                                            Reference Three &nbsp;&nbsp;
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td colspan="4" class="tdheight">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:CheckBox ID="chkNA3" runat="server" Text="N/A" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="trExplain3" runat="server" style="display: none">
                                                                                                                        <td class="formtdrt">
                                                                                                                            <span class="mandatorystar">*</span>Please Explain
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="trtxtExplain3" runat="server" style="display: none">
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox ID="txtExplain3" runat="server" TextMode="MultiLine" CssClass="txtboxlarge"
                                                                                                                                onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                                onKeyUp="javascript:textCounter(this,1000,'yes');"></asp:TextBox>
                                                                                                                            <asp:CustomValidator ID="cusvExplain3" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" SetFocusOnError="true" OnServerValidate="cusvExplain3_ServerValidate"
                                                                                                                                EnableClientScript="true">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt" width="40%">
                                                                                                                            <span class="mandatorystar">*</span>Project Name
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" width="30%">
                                                                                                                            <span class="mandatorystar">*</span>Country
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            <span class="mandatorystar">*</span>City
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox CssClass="txtboxlarge" ID="txtProjectNameThree" runat="server" MaxLength="100"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <asp:CustomValidator ID="cusvProjectNameThree" OnServerValidate="cusvProjectNameThree_ServerValidate"
                                                                                                                                ValidationGroup="ProjectReferences" Display="None" runat="server" SetFocusOnError="true"
                                                                                                                                EnableClientScript="true">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList  ID="ddlCountryThree" runat="server" CssClass="ddlboxCountry"
                                                                                                                                OnSelectedIndexChanged="ddlRefCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            <asp:TextBox ID="txtCityThree" runat="server" CssClass="txtboxmedium" MaxLength="50"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <asp:CustomValidator ID="cusvCity3" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" SetFocusOnError="true" OnServerValidate="cusvCity3_ServerValidate"
                                                                                                                                EnableClientScript="true">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <span class="mandatorystar">*</span>State <span class="spnState" id="trOther3lbl"
                                                                                                                                runat="server" style="display: none">Please Specify State </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="3">
                                                                                                                            <span class="mandatorystar">*</span>Completion Date
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList ID="ddlStateThree" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                                TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <span id="trOther3" runat="server" style="display: none">
                                                                                                                                <asp:TextBox ID="txtOtherStateThree" runat="server" CssClass="txtbox" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <asp:CustomValidator ID="cusvOtherStateThree" runat="server" ValidationGroup="ProjectReferences"
                                                                                                                                    Display="None" OnServerValidate="CusvOtherStateThree_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtOtherStateThree" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                                    ValidChars=" ,@" TargetControlID="txtOtherStateThree">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList ID="ddlMonth3" CssClass="ddlbox" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            &nbsp;
                                                                                                                            <asp:DropDownList ID="ddlYear3" CssClass="ddlbox" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <asp:CustomValidator ID="cusvMonth3" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" OnServerValidate="cusvMonth3_ServerValidate">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td width="60%">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span class="mandatorystar">*</span>Approximate Contract Value $
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span class="mandatorystar">*</span> <span id="spnProjectReferenceTypeThree" runat="server">
                                                                                                                                            General Contractor</span>
                                                                                                                                        <asp:DropDownList CssClass="ddlbox" TabIndex="-1" ID="ddlProjectReferenceTypeThree"
                                                                                                                                            runat="server">
                                                                                                                                        </asp:DropDownList>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox1" ID="txtApproximateContractAmtThree" runat="server"
                                                                                                                                            MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="cusvApproximate3" OnServerValidate="cusvApproximate3_ServerValidate"
                                                                                                                                            ValidationGroup="ProjectReferences" Display="None" runat="server" SetFocusOnError="true"
                                                                                                                                            EnableClientScript="true">
                                                                                                                                        </asp:CustomValidator>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="fltbxApproximateContractAmtThree" runat="server"
                                                                                                                                            TargetControlID="txtApproximateContractAmtThree" FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0"
                                                                                                                                            InvalidChars="A-Z,a-z,-,.,">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtContractorNameThree" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="cusvContractorNameThree" OnServerValidate="cusvContractorNameThree_ServerValidate"
                                                                                                                                            ControlToValidate="txtApproximateContractAmtThree" ValidationGroup="ProjectReferences"
                                                                                                                                            Display="None" runat="server" SetFocusOnError="true" EnableClientScript="true">
                                                                                                                                        </asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span class="mandatorystar">*</span>Contact Name
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span class="mandatorystar">*</span>Contact Phone Number
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtCtrContactNameThree" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="cusvContactName3" OnServerValidate="cusvContactName3_ServerValidate"
                                                                                                                                            ValidationGroup="ProjectReferences" Display="None" runat="server" SetFocusOnError="true"
                                                                                                                                            EnableClientScript="true">
                                                                                                                                        </asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" nowrap="nowrap">
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtThirdPhnoOne" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        -
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtThirdPhnoOne" runat="server" TargetControlID="txtThirdPhnoOne"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtThirdPhnoTwo" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <span id="spnThirdPhnoTwo" runat="server">-</span>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtThirdPhnoTwo" runat="server" TargetControlID="txtThirdPhnoTwo"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtThirdPhnoThree" runat="server" MaxLength="4"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtThirdPhnoThree" runat="server" TargetControlID="txtThirdPhnoThree"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:CustomValidator ID="cusvhirdPhnoThree" runat="server" Display="None" ValidationGroup="ProjectReferences"
                                                                                                                                            EnableClientScript="false" OnServerValidate="CusvhirdPhnoThree_ServerValidate"></asp:CustomValidator>
                                                                                                                                        <div id="divThirdPhnoTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                            Country Code / Area + Phone</div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                        <td width="40%" valign="top">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="48%">
                                                                                                                                        <span class="mandatorystar">*</span>Please give a brief description of work completed
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtboxlarge1" ID="txtWorkCompletedThree" runat="server" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            onKeyUp="javascript:textCounter(this,1000,'yes');" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            TextMode="MultiLine" TabIndex="0"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="cusvWorkComplated3" ValidationGroup="ProjectReferences"
                                                                                                                                            Display="None" runat="server" SetFocusOnError="true" OnServerValidate="cusvWorkComplated3_ServerValidate"
                                                                                                                                            EnableClientScript="true">
                                                                                                                                        </asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trRefSpeaksEnglishThree" runat="server">
                                                                                                            <td class="formtdrt" align="left" valign="middle">
                                                                                                                Speaks English?
                                                                                                                <asp:DropDownList ID="ddlRefSpeaksEnglishThree" runat="server" CssClass="ddlbox">
                                                                                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trReferencesFour" runat="server" style="display: none" width="100%">
                                                                                                <td class="right">
                                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="keyheader" colspan="4">
                                                                                                                            Reference Four
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td colspan="4" class="tdheight">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt" width="40%">
                                                                                                                            Project Name
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" width="30%">
                                                                                                                            Country
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            City
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox CssClass="txtboxlarge" ID="txtProjectNameFour" runat="server" MaxLength="100"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList  ID="ddlCountryReferencesFour" runat="server" CssClass="ddlboxCountry"
                                                                                                                                OnSelectedIndexChanged="ddlRefCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            <asp:TextBox ID="txtCityFour" runat="server" CssClass="txtboxmedium" MaxLength="50"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            State <span class="spnState" id="trOtherReferences4lbl" runat="server" style="display: none">
                                                                                                                                Please Specify State </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="3">
                                                                                                                            Completion Date
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList ID="ddlStateReferencesFour" runat="server" CssClass="ddlboxState"
                                                                                                                                AutoPostBack="True" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <span id="trOtherReferences4" runat="server" style="display: none">
                                                                                                                                <asp:TextBox ID="txtStateReferencesFour" runat="server" CssClass="txtbox" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <asp:CustomValidator ID="cusvStateReferencesFour" runat="server" ValidationGroup="ProjectReferences"
                                                                                                                                    Display="None" OnServerValidate="cusvStateReferencesFour_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtStateReferencesFour" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                                    ValidChars=" ,@" TargetControlID="txtStateReferencesFour">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList ID="ddlMonth4" CssClass="ddlbox" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            &nbsp;
                                                                                                                            <asp:DropDownList ID="ddlYear4" CssClass="ddlbox" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <asp:CustomValidator ID="cusvMonth4" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" OnServerValidate="cusvMonth4_ServerValidate">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td width="60%">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Approximate Contract Value $
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span id="spnProjectReferenceTypeFour" runat="server">General Contractor</span>
                                                                                                                                        <asp:DropDownList CssClass="ddlbox" TabIndex="-1" ID="ddlProjectReferenceTypeFour"
                                                                                                                                            runat="server">
                                                                                                                                        </asp:DropDownList>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox1" ID="txtApproximateContractAmtFour" runat="server"
                                                                                                                                            MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtApproximateContractAmtFour" runat="server"
                                                                                                                                            TargetControlID="txtApproximateContractAmtFour" FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0"
                                                                                                                                            InvalidChars="A-Z,a-z,-,.,">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtContractorNameFour" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Contact Name
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Contact Phone Number
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtCtrContactNameFour" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" nowrap="nowrap">
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFourthPhnoOne" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFourthPhnoOne" runat="server" TargetControlID="txtFourthPhnoOne"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFourthPhnoTwo" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <span id="spnFourthPhnoTwo" runat="server">-</span>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFourthPhnoTwo" runat="server" TargetControlID="txtFourthPhnoTwo"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFourthPhnoThree" runat="server" MaxLength="4"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFourthPhnoThree" runat="server" TargetControlID="txtFourthPhnoThree"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:CustomValidator ID="cusvFourthPhnoThree" runat="server" Display="None" ValidationGroup="ProjectReferences"
                                                                                                                                            EnableClientScript="false" OnServerValidate="cusvFourthPhnoThree_ServerValidate"></asp:CustomValidator>
                                                                                                                                        <div id="divFourthPhnoTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                            Country Code / Area + Phone</div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                        <td width="40%" valign="top">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Please give a brief description of work completed
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtboxlarge1" ID="txtWorkCompletedFour" runat="server" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            onKeyUp="javascript:textCounter(this,1000,'yes');" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            TextMode="MultiLine" TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trRefSpeaksEnglishFour" runat="server">
                                                                                                            <td class="formtdrt" align="left" valign="middle">
                                                                                                                Speaks English?
                                                                                                                <asp:DropDownList ID="ddlRefSpeaksEnglishFour" runat="server" CssClass="ddlbox">
                                                                                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trReferencesFive" runat="server" style="display: none">
                                                                                                <td class="right">
                                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="keyheader" colspan="5">
                                                                                                                            Reference Five
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td colspan="4" class="tdheight">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt" width="40%">
                                                                                                                            Project Name
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" width="30%">
                                                                                                                            Country
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            City
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox CssClass="txtboxlarge" ID="txtProjectNameFive" runat="server" MaxLength="100"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <%--<Ajax:FilteredTextBoxExtender ID="ftxtProjectNameFive" runat="server" FilterType="Custom"
                                                                                                                        ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"  TargetControlID="txtProjectNameFive">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList  ID="ddlCountryReferencesFive" runat="server" CssClass="ddlboxCountry"
                                                                                                                                OnSelectedIndexChanged="ddlRefCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            <asp:TextBox ID="txtCityFive" runat="server" CssClass="txtboxmedium" MaxLength="50"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <%--<Ajax:FilteredTextBoxExtender ID="ftxtCityFive" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCityFive">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            State <span class="spnState" id="trOtherReferences5lbl" runat="server" style="display: none">
                                                                                                                                Please Specify State </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="3">
                                                                                                                            Completion Date
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList ID="ddlStateReferencesFive" runat="server" CssClass="ddlboxState"
                                                                                                                                AutoPostBack="True" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <span id="trOtherReferences5" runat="server" style="display: none">
                                                                                                                                <asp:TextBox ID="txtStateReferencesFive" runat="server" CssClass="txtbox" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <asp:CustomValidator ID="cusvOtherReferencesFive" runat="server" ValidationGroup="ProjectReferences"
                                                                                                                                    Display="None" OnServerValidate="cusvOtherReferencesFive_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtStateReferencesFive" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                                    ValidChars=" ,@" TargetControlID="txtStateReferencesFive">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlMonth5" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            &nbsp;
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlYear5" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <asp:CustomValidator ID="cusvMonth5" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" OnServerValidate="cusvMonth5_ServerValidate">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td width="60%">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Approximate Contract Value $
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span id="spnProjectReferenceTypeFive" runat="server">General Contractor</span>
                                                                                                                                        <asp:DropDownList CssClass="ddlbox" TabIndex="-1" ID="ddlProjectReferenceTypeFive"
                                                                                                                                            runat="server">
                                                                                                                                        </asp:DropDownList>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox1" ID="txtApproximateContractAmtFive" runat="server"
                                                                                                                                            MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtApproximateContractAmtFive" runat="server"
                                                                                                                                            TargetControlID="txtApproximateContractAmtFive" FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0"
                                                                                                                                            InvalidChars="A-Z,a-z,-,.,">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtContractorNameFive" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="26%">
                                                                                                                                        Contact Name
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Contact Phone Number
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtCtrContactNameFive" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <%--<Ajax:FilteredTextBoxExtender ID="ftxtCtrContactNameFive" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCtrContactNameFive">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" nowrap="nowrap">
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFifthPhnoOne" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFifthphnoOne" runat="server" TargetControlID="txtFifthPhnoOne"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFifthPhnoTwo" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <span id="spnFifthPhnoTwo" runat="server">-</span>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFifthPhnoTwo" runat="server" TargetControlID="txtFifthPhnoTwo"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFifthPhnoThree" runat="server" MaxLength="4"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFifthPhnoThree" runat="server" TargetControlID="txtFifthPhnoThree"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:CustomValidator ID="cusvFifthPhnoThree" runat="server" Display="None" ValidationGroup="ProjectReferences"
                                                                                                                                            EnableClientScript="false" OnServerValidate="CusvFifthPhnoThree_ServerValidate"></asp:CustomValidator>
                                                                                                                                        <div id="divFifthPhnoTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                            Country Code / Area + Phone</div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                        <td width="40%">
                                                                                                                            <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="48%">
                                                                                                                                        Please give a brief description of work completed
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtboxlarge1" ID="txtWorkCompletedFive" runat="server" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            onKeyUp="javascript:textCounter(this,1000,'yes');" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            TextMode="MultiLine" TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trRefSpeaksEnglishFive" runat="server">
                                                                                                            <td class="formtdrt" align="left" valign="middle">
                                                                                                                Speaks English?
                                                                                                                <asp:DropDownList ID="ddlRefSpeaksEnglishFive" runat="server" CssClass="ddlbox">
                                                                                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trReferencesSix" runat="server" style="display: none">
                                                                                                <td class="right">
                                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="keyheader" colspan="5">
                                                                                                                            Reference Six
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td colspan="4" class="tdheight">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt" width="40%">
                                                                                                                            Project Name
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" width="30%">
                                                                                                                            Country
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            City
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox CssClass="txtboxlarge" ID="txtProjectNameSix" runat="server" MaxLength="100"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <%-- <Ajax:FilteredTextBoxExtender ID="ftxtProjectNameSix" runat="server" FilterType="Custom"
                                                                                                                              ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"  TargetControlID="txtProjectNameSix">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList  ID="ddlCountryReferencesSix" runat="server" CssClass="ddlboxCountry"
                                                                                                                                OnSelectedIndexChanged="ddlRefCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            <asp:TextBox ID="txtCitySix" runat="server" CssClass="txtboxmedium" MaxLength="50"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <%-- <Ajax:FilteredTextBoxExtender ID="ftxtCitySix" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCitySix">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            State <span class="spnState" id="trOtherReferences6lbl" runat="server" style="display: none">
                                                                                                                                Please Specify State </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="3">
                                                                                                                            Completion Date
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList ID="ddlStateReferencesSix" runat="server" CssClass="ddlboxState"
                                                                                                                                AutoPostBack="True" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <span id="trOtherReferences6" runat="server" style="display: none">
                                                                                                                                <asp:TextBox ID="txtStateReferencesSix" runat="server" CssClass="txtbox" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <asp:CustomValidator ID="cusvStateReferencesSix" runat="server" ValidationGroup="ProjectReferences"
                                                                                                                                    Display="None" OnServerValidate="cusvStateReferencesSix_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtStateReferencesSix" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                                    ValidChars=" ,@" TargetControlID="txtStateReferencesSix">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="3">
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlMonth6" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            &nbsp;
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlYear6" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <asp:CustomValidator ID="cusvMonth6" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" OnServerValidate="cusvMonth6_ServerValidate">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td width="60%">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Approximate Contract Value $
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span id="spnProjectReferenceTypeSix" runat="server">General Contractor</span>
                                                                                                                                        <asp:DropDownList CssClass="ddlbox" TabIndex="-1" ID="ddlProjectReferenceTypeSix"
                                                                                                                                            runat="server">
                                                                                                                                        </asp:DropDownList>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox1" ID="txtApproximateContractAmtSix" runat="server"
                                                                                                                                            MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtApproximateContractAmtSix" runat="server" TargetControlID="txtApproximateContractAmtSix"
                                                                                                                                            FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtContractorNameSix" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="26%">
                                                                                                                                        Contact Name
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Contact Phone Number
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtCtrContactNameSix" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <%-- <Ajax:FilteredTextBoxExtender ID="ftxtCtrContactNameSix" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCtrContactNameSix">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" nowrap="nowrap">
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtSixthPhnoOne" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtSixthPhnoOne" runat="server" TargetControlID="txtSixthPhnoOne"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtSixthPhnoTwo" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <span id="spnSixthPhnoTwo" runat="server">-</span>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtSixthPhnoTwo" runat="server" TargetControlID="txtSixthPhnoTwo"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtSixthPhnoThree" runat="server" MaxLength="4"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtSixthPhnoThree" runat="server" TargetControlID="txtSixthPhnoThree"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:CustomValidator ID="cusvSixthPhnoThree" runat="server" Display="None" ValidationGroup="ProjectReferences"
                                                                                                                                            EnableClientScript="false" OnServerValidate="cusvSixthPhnoThree_ServerValidate"></asp:CustomValidator>
                                                                                                                                        <div id="divSixthPhnoTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                            Country Code / Area + Phone</div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                        <td width="40%">
                                                                                                                            <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="48%">
                                                                                                                                        Please give a brief description of work completed
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtboxlarge1" ID="txtWorkCompletedSix" runat="server" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            onKeyUp="javascript:textCounter(this,1000,'yes');" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            TextMode="MultiLine" TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trRefSpeaksEnglishSix" runat="server">
                                                                                                            <td class="formtdrt" align="left" valign="middle">
                                                                                                                Speaks English?
                                                                                                                <asp:DropDownList ID="ddlRefSpeaksEnglishSix" runat="server" CssClass="ddlbox">
                                                                                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trReferencesSeven" runat="server" style="display: none">
                                                                                                <td class="right">
                                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="keyheader" colspan="5">
                                                                                                                            Reference Seven
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td colspan="4" class="tdheight">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt" width="40%">
                                                                                                                            Project Name
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" width="30%">
                                                                                                                            Country
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            City
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox CssClass="txtboxlarge" ID="txtProjectNameSeven" runat="server" MaxLength="100"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList  ID="ddlCountryReferencesSeven" runat="server" CssClass="ddlboxCountry"
                                                                                                                                OnSelectedIndexChanged="ddlRefCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            <asp:TextBox ID="txtCitySeven" runat="server" CssClass="txtboxmedium" MaxLength="50"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            State <span class="spnState" id="trOtherReferences7lbl" runat="server" style="display: none">
                                                                                                                                Please Specify State </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="3">
                                                                                                                            Completion Date
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList ID="ddlStateReferencesSeven" runat="server" CssClass="ddlboxState"
                                                                                                                                AutoPostBack="True" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <span id="trOtherReferences7" runat="server" style="display: none">
                                                                                                                                <asp:TextBox ID="txtStateReferencesSeven" runat="server" CssClass="txtbox" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <asp:CustomValidator ID="cusvStateReferencesSeven" runat="server" ValidationGroup="ProjectReferences"
                                                                                                                                    Display="None" OnServerValidate="cusvStateReferencesSeven_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtStateReferencesSeven" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                                    ValidChars=" ,@" TargetControlID="txtStateReferencesSeven">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlMonth7" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            &nbsp;
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlYear7" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <asp:CustomValidator ID="cusvMonth7" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" OnServerValidate="cusvMonth7_ServerValidate">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td width="60%">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Approximate Contract Value $
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span id="spnProjectReferenceTypeSeven" runat="server">General Contractor</span>
                                                                                                                                        <asp:DropDownList CssClass="ddlbox" TabIndex="-1" ID="ddlProjectReferenceTypeSeven"
                                                                                                                                            runat="server">
                                                                                                                                        </asp:DropDownList>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox1" ID="txtApproximateContractAmtSeven" runat="server"
                                                                                                                                            MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtApproximateContractAmtSeven" runat="server"
                                                                                                                                            TargetControlID="txtApproximateContractAmtSeven" FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0"
                                                                                                                                            InvalidChars="A-Z,a-z,-,.,">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtContractorNameSeven" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="26%">
                                                                                                                                        Contact Name
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Contact Phone Number
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtCtrContactNameSeven" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <%-- <Ajax:FilteredTextBoxExtender ID="ftxtCtrContactNameSeven" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCtrContactNameSeven">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" nowrap="nowrap">
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtSeventhPhnoOne" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtSeventhPhnoOne" runat="server" TargetControlID="txtSeventhPhnoOne"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtSeventhPhnoTwo" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <span id="spnSeventhPhnoTwo" runat="server">-</span>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtSeventhPhnoTwo" runat="server" TargetControlID="txtSeventhPhnoTwo"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtSeventhPhnoThree" runat="server" MaxLength="4"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtSeventhPhnoThree" runat="server" TargetControlID="txtSeventhPhnoThree"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:CustomValidator ID="custSeventhPhnoThree" runat="server" Display="None" ValidationGroup="ProjectReferences"
                                                                                                                                            EnableClientScript="false" OnServerValidate="CusvSevethPhnoThree_ServerValidate"></asp:CustomValidator>
                                                                                                                                        <div id="divSeventhPhnoTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                            Country Code / Area + Phone</div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                        <td width="40%">
                                                                                                                            <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="48%">
                                                                                                                                        Please give a brief description of work completed
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtboxlarge1" ID="txtWorkCompletedSeven" runat="server" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            onKeyUp="javascript:textCounter(this,1000,'yes');" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            TextMode="MultiLine" TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trRefSpeaksEnglishSeven" runat="server">
                                                                                                            <td class="formtdrt" align="left" valign="middle">
                                                                                                                Speaks English?
                                                                                                                <asp:DropDownList ID="ddlRefSpeaksEnglishSeven" runat="server" CssClass="ddlbox">
                                                                                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trReferencesEight" runat="server" style="display: none">
                                                                                                <td class="right">
                                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="keyheader" colspan="5">
                                                                                                                            Reference Eight
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td colspan="4" class="tdheight">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt" width="40%">
                                                                                                                            Project Name
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" width="30%">
                                                                                                                            Country
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            City
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox CssClass="txtboxlarge" ID="txtProjectNameEight" runat="server" MaxLength="100"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <%-- <Ajax:FilteredTextBoxExtender ID="ftxtProjectNameEight" runat="server" FilterType="Custom"
                                                                                                                              ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"  TargetControlID="txtProjectNameEight">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList  ID="ddlCountryReferencesEight" runat="server" CssClass="ddlboxCountry"
                                                                                                                                OnSelectedIndexChanged="ddlRefCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            <asp:TextBox ID="txtCityEight" runat="server" CssClass="txtboxmedium" MaxLength="50"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <%--  <Ajax:FilteredTextBoxExtender ID="ftxtCityEight" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCityEight">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            State <span class="spnState" id="trOtherReferences8lbl" runat="server" style="display: none">
                                                                                                                                Please Specify State </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="3">
                                                                                                                            Completion Date
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList ID="ddlStateReferencesEight" runat="server" CssClass="ddlboxState"
                                                                                                                                AutoPostBack="True" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <span id="trOtherReferences8" runat="server" style="display: none">
                                                                                                                                <asp:TextBox ID="txtStateRefencesEight" runat="server" CssClass="txtbox" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <asp:CustomValidator ID="cusvstateReferencesEight" runat="server" ValidationGroup="ProjectReferences"
                                                                                                                                    Display="None" OnServerValidate="cusvstateReferencesEight_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtStateRefeencesEight" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                                    ValidChars=" ,@" TargetControlID="txtStateRefencesEight">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlMonth8" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            &nbsp;
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlYear8" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <asp:CustomValidator ID="cusvMonth8" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" OnServerValidate="cusvMonth8_ServerValidate">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td width="60%">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Approximate Contract Value $
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span id="spnProjectReferenceTypeEight" runat="server">General Contractor</span>
                                                                                                                                        <asp:DropDownList CssClass="ddlbox" TabIndex="-1" ID="ddlProjectReferenceTypeEight"
                                                                                                                                            runat="server">
                                                                                                                                        </asp:DropDownList>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox1" ID="txtApproximateContractAmtEight" runat="server"
                                                                                                                                            MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtApproximateContractAmtEight" runat="server"
                                                                                                                                            TargetControlID="txtApproximateContractAmtEight" FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0"
                                                                                                                                            InvalidChars="A-Z,a-z,-,.,">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" colspan="2" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtContractorNameEight" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="26%">
                                                                                                                                        Contact Name
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Contact Phone Number
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtCtrContactNameEight" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <%-- <Ajax:FilteredTextBoxExtender ID="ftxtCtrContactNameEight" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCtrContactNameEight">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" nowrap="nowrap">
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtEighthPhnoOne" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtEighthPhnoOne" runat="server" TargetControlID="txtEighthPhnoOne"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtEighthPhnoTwo" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <span id="spnEighthPhnoTwo" runat="server">-</span>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtEighthPhnoTwo" runat="server" TargetControlID="txtEighthPhnoTwo"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtEighthPhnoThree" runat="server" MaxLength="4"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtEighthPhnoThree" runat="server" TargetControlID="txtEighthPhnoThree"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:CustomValidator ID="cusvEightPhnoThree" runat="server" Display="None" ValidationGroup="ProjectReferences"
                                                                                                                                            EnableClientScript="false" OnServerValidate="cusvEightPhnoThree_ServerValidate"></asp:CustomValidator>
                                                                                                                                        <div id="divEighthPhnoTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                            Country Code / Area + Phone</div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                        <td width="40%">
                                                                                                                            <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="48%">
                                                                                                                                        Please give a brief description of work completed
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtboxlarge1" ID="txtWorkCompletedEight" runat="server" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            onKeyUp="javascript:textCounter(this,1000,'yes');" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            TextMode="MultiLine" TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trRefSpeaksEnglishEight" runat="server">
                                                                                                            <td class="formtdrt" align="left" valign="middle">
                                                                                                                Speaks English?
                                                                                                                <asp:DropDownList ID="ddlRefSpeaksEnglishEight" runat="server" CssClass="ddlbox">
                                                                                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trReferencesNine" runat="server" style="display: none">
                                                                                                <td class="right">
                                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="keyheader" colspan="5">
                                                                                                                            Reference Nine
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td colspan="4" class="tdheight">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt" width="40%">
                                                                                                                            Project Name
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" width="30%">
                                                                                                                            Country
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            City
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox CssClass="txtboxlarge" ID="txtProjectNameNine" runat="server" MaxLength="100"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <%-- <Ajax:FilteredTextBoxExtender ID="ftxtProjectNamenine" runat="server" FilterType="Custom"
                                                                                                                              ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"  TargetControlID="txtProjectNameNine">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList  ID="ddlCountryReferencesNine" runat="server" CssClass="ddlboxCountry"
                                                                                                                                OnSelectedIndexChanged="ddlRefCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            <asp:TextBox ID="txtCityNine" runat="server" CssClass="txtboxmedium" MaxLength="50"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <%-- <Ajax:FilteredTextBoxExtender ID="ftxtCityNine" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCityNine">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            State <span class="spnState" id="trOtherReferences9lbl" runat="server" style="display: none">
                                                                                                                                Please Specify State </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="3">
                                                                                                                            Completion Date
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList ID="ddlStateReferencesNine" runat="server" CssClass="ddlboxState"
                                                                                                                                AutoPostBack="True" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <span id="trOtherReferences9" runat="server" style="display: none">
                                                                                                                                <asp:TextBox ID="txtStateReferencesNine" runat="server" CssClass="txtbox" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <asp:CustomValidator ID="custStateReferencesNine" runat="server" ValidationGroup="ProjectReferences"
                                                                                                                                    Display="None" OnServerValidate="custStateReferencesNine_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtStateReferencesNine" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                                    ValidChars=" ,@" TargetControlID="txtStateReferencesNine">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlMonth9" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            &nbsp;
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlYear9" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <asp:CustomValidator ID="cusvMonth9" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" OnServerValidate="cusvMonth9_ServerValidate">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td width="60%">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Approximate Contract Value $
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span id="spnProjectReferenceTypeNine" runat="server">General Contractor</span>
                                                                                                                                        <asp:DropDownList CssClass="ddlbox" TabIndex="-1" ID="ddlProjectReferenceTypeNine"
                                                                                                                                            runat="server">
                                                                                                                                        </asp:DropDownList>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox1" ID="txtApproximateContractAmtnine" runat="server"
                                                                                                                                            MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtApproximateContractAmtnine" runat="server"
                                                                                                                                            TargetControlID="txtApproximateContractAmtNine" FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0"
                                                                                                                                            InvalidChars="A-Z,a-z,-,.,">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtContractorNameNine" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="26%">
                                                                                                                                        Contact Name
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Contact Phone Number
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtCtrContactNameNinth" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <%-- <Ajax:FilteredTextBoxExtender ID="ftxtCtrContactNameninth" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCtrContactNameNinth">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtNinthPhnoOne" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtNinthPhnoOne" runat="server" TargetControlID="txtNinthPhnoOne"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtNinthPhnoTwo" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <span id="spnNinthPhnoTwo" runat="server">-</span>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtNinthPhnoTwo" runat="server" TargetControlID="txtNinthPhnoTwo"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtNinthPhnoThree" runat="server" MaxLength="4"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtNinthPhnoThree" runat="server" TargetControlID="txtNinthPhnoThree"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:CustomValidator ID="cusvninthPhnoThree" runat="server" Display="None" ValidationGroup="ProjectReferences"
                                                                                                                                            EnableClientScript="false" OnServerValidate="cusvninthPhnoThree_ServerValidate"></asp:CustomValidator>
                                                                                                                                        <div id="divNinthPhnoTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                            Country Code / Area + Phone</div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                        <td width="40%">
                                                                                                                            <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Please give a brief description of work completed
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtboxlarge1" ID="txtWorkCompletedNinth" runat="server" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            onKeyUp="javascript:textCounter(this,1000,'yes');" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            TextMode="MultiLine" TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trRefSpeaksEnglishNine" runat="server">
                                                                                                            <td class="formtdrt" align="left" valign="middle">
                                                                                                                Speaks English?
                                                                                                                <asp:DropDownList ID="ddlRefSpeaksEnglishNine" runat="server" CssClass="ddlbox">
                                                                                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trReferencesTen" runat="server" style="display: none">
                                                                                                <td class="right">
                                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="keyheader" colspan="4">
                                                                                                                            Reference Ten
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td colspan="4" class="tdheight">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt" width="40%">
                                                                                                                            Project Name
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" width="30%">
                                                                                                                            Country
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            City
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:TextBox CssClass="txtboxlarge" ID="txtProjectNameTen" runat="server" MaxLength="100"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <%-- <Ajax:FilteredTextBoxExtender ID="ftxtProjectNameTen" runat="server" FilterType="Custom"
                                                                                                                              ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"  TargetControlID="txtProjectNameTen">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList  ID="ddlCountryReferencesTen" runat="server" CssClass="ddlboxCountry"
                                                                                                                                OnSelectedIndexChanged="ddlRefCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            <asp:TextBox ID="txtCityTen" runat="server" CssClass="txtboxmedium" MaxLength="50"
                                                                                                                                TabIndex="0"></asp:TextBox>
                                                                                                                            <%-- <Ajax:FilteredTextBoxExtender ID="ftxtCityTen" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCityTen">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            State <span class="spnState" id="trOtherReferences10lbl" runat="server" style="display: none">
                                                                                                                                Please Specify State </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt" colspan="3">
                                                                                                                            Completion Date
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList ID="ddlStateReferencesTen" runat="server" CssClass="ddlboxState"
                                                                                                                                AutoPostBack="True" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <span id="trOtherReferences10" runat="server" style="display: none">
                                                                                                                                <asp:TextBox ID="txtStateReferencesTen" runat="server" CssClass="txtbox" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <asp:CustomValidator ID="cusvStateReferencesTen" runat="server" ValidationGroup="ProjectReferences"
                                                                                                                                    Display="None" OnServerValidate="cusvStateReferencesTen_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtStateReferencesTen" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                                                                                    ValidChars=" ,@" TargetControlID="txtStateReferencesTen">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </span>
                                                                                                                        </td>
                                                                                                                        <td class="formtdrt">
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlMonth10" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            &nbsp;
                                                                                                                            <asp:DropDownList CssClass="ddlbox" ID="ddlYear10" runat="server" TabIndex="0">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <asp:CustomValidator ID="cusvMonth10" ValidationGroup="ProjectReferences" Display="None"
                                                                                                                                runat="server" OnServerValidate="cusvMonth10_ServerValidate">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td width="60%">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="28%">
                                                                                                                                        Approximate Contract Value $
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <span id="spnProjectReferenceTypeTen" runat="server">General Contractor</span>
                                                                                                                                        <asp:DropDownList CssClass="ddlbox" TabIndex="-1" ID="ddlProjectReferenceTypeTen"
                                                                                                                                            runat="server">
                                                                                                                                        </asp:DropDownList>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox1" ID="txtApproximateContractAmtTen" runat="server"
                                                                                                                                            MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtApproximateContractAmtTen" runat="server" TargetControlID="txtApproximateContractAmtTen"
                                                                                                                                            FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <asp:TextBox Class="txtbox1" ID="txtContractorNameTen" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="28%">
                                                                                                                                        Contact Name
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        Contact Phone Number
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtCtrContactNameTen" runat="server" MaxLength="50"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <%-- <Ajax:FilteredTextBoxExtender ID="ftxtCtrContactNameTen" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                        ValidChars=" " TargetControlID="txtCtrContactNameTen">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                                    </td>
                                                                                                                                    <td class="formtdrt">
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtTenthPhnoOne" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtTenthPhnoOne" runat="server" TargetControlID="txtTenthPhnoOne"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtTenthPhnoTwo" runat="server" MaxLength="3"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <span id="spnTenthPhnoTwo" runat="server">-</span>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtTenthPhnoTwo" runat="server" TargetControlID="txtTenthPhnoTwo"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtTenthPhnoThree" runat="server" MaxLength="4"
                                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtTenthPhnoThree" runat="server" TargetControlID="txtTenthPhnoThree"
                                                                                                                                            FilterType="Numbers">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                        <asp:CustomValidator ID="custTenthPhnoThree" runat="server" Display="None" ValidationGroup="ProjectReferences"
                                                                                                                                            EnableClientScript="false" OnServerValidate="custTenthPhnoThree_ServerValidate"></asp:CustomValidator>
                                                                                                                                        <div id="divTenthPhnoTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                            Country Code / Area + Phone</div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                        <td width="40%">
                                                                                                                            <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" width="48%">
                                                                                                                                        Please give a brief description of work completed
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td class="formtdrt" valign="top">
                                                                                                                                        <asp:TextBox CssClass="txtboxlarge1" ID="txtWorkCompletedTen" runat="server" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            onKeyUp="javascript:textCounter(this,1000,'yes');" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                                                            TextMode="MultiLine" TabIndex="0"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trRefSpeaksEnglishTen" runat="server">
                                                                                                            <td class="formtdrt" align="left" valign="middle">
                                                                                                                Speaks English?
                                                                                                                <asp:DropDownList ID="ddlRefSpeaksEnglishTen" runat="server" CssClass="ddlbox">
                                                                                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextcenter">
                                                                                                    <asp:LinkButton ID="lnkAddMoreReference" runat="server" Text="Click to Add More Project References"></asp:LinkButton>
                                                                                                    <asp:HiddenField ID="hdnAdditionalReferencesCount" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="formtdrt" width="43%">
                                                                                                                Have you ever completed any projects with Haskell?
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="57%">
                                                                                                                <asp:RadioButtonList ID="rdoHaskellProjectsCompleted" runat="server" RepeatDirection="Horizontal"
                                                                                                                    onclick="javascript:ShowProject();">
                                                                                                                    <asp:ListItem Value="True">Yes</asp:ListItem>
                                                                                                                    <asp:ListItem Value="False">No</asp:ListItem>
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="2" id="tdProject" runat="server">
                                                                                                                <span id="trAddProject01" runat="server" style="display: none; width: 100%">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Name
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Manager
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="50%">
                                                                                                                                Year Completed
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectNameOne" runat="server" MaxLength="100"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<Ajax:FilteredTextBoxExtender ID="ftxtHaskellProjectNameOne" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                            ValidChars=" " TargetControlID="txtHaskellProjectNameOne">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectManagerOne" runat="server" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<Ajax:FilteredTextBoxExtender ID="ftbeHaskellProjectManagerOne" runat="server" TargetControlID="txtHaskellProjectManagerOne"
                                                                                                                            FilterType="LowercaseLetters,UppercaseLetters,Custom" ValidChars=" ">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtYearOne" runat="server" MaxLength="4"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<asp:CustomValidator ID="cusvYearOne" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvYearOne_ServerValidate" EnableClientScript="false">
                                                                                                        </asp:CustomValidator>--%>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftbeYearOne" runat="server" TargetControlID="txtYearOne"
                                                                                                                                    FilterType="Numbers">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                                <%--   <asp:CustomValidator ID="cusvHaskellProjectManagerOne" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvHaskellProjectManagerOne_ServerValidate"
                                                                                                            EnableClientScript="false">
                                                                                                        </asp:CustomValidator>--%>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </span><span id="trAddProject02" runat="server" style="display: none">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Name
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Manager
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="50%">
                                                                                                                                Year Completed
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectNameTwo" runat="server" MaxLength="100"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<Ajax:FilteredTextBoxExtender ID="ftxtHaskellProjectNameTwo" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                            ValidChars=" " TargetControlID="txtHaskellProjectNameTwo">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectManagerTwo" runat="server" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%-- <Ajax:FilteredTextBoxExtender ID="ftbeHaskellProjectManagerTwo" runat="server" TargetControlID="txtHaskellProjectManagerTwo"
                                                                                                                            FilterType="LowercaseLetters,UppercaseLetters,Custom" ValidChars=" ">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtYearTwo" runat="server" MaxLength="4"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<asp:CustomValidator ID="cusvYearTwo" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvYearTwo_ServerValidate" EnableClientScript="false">
                                                                                                        </asp:CustomValidator>--%>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftbeYearTwo" runat="server" TargetControlID="txtYearTwo"
                                                                                                                                    FilterType="Numbers">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                                <%-- <asp:CustomValidator ID="cusvHaskellProjectManagerTwo" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvHaskellProjectManagerTwo_ServerValidate"
                                                                                                            EnableClientScript="false">
                                                                                                        </asp:CustomValidator>--%>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </span><span id="trAddProject03" runat="server" style="display: none">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Name
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Manager
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="50%">
                                                                                                                                Year Completed
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectNameThree" runat="server" MaxLength="100"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<Ajax:FilteredTextBoxExtender ID="ftxtHaskellProjectNameThree" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                            ValidChars=" " TargetControlID="txtHaskellProjectNameThree">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectManagerThree" runat="server"
                                                                                                                                    MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                                                                                <%-- <Ajax:FilteredTextBoxExtender ID="ftbeHaskellProjectManagerThree" runat="server"
                                                                                                                            TargetControlID="txtHaskellProjectManagerTwo" FilterType="LowercaseLetters,UppercaseLetters,Custom"
                                                                                                                            ValidChars=" ">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtYearThree" runat="server" MaxLength="4"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<asp:CustomValidator ID="cusvYearThree" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvYearThree_ServerValidate" EnableClientScript="false">
                                                                                                        </asp:CustomValidator>--%>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftbeYearThree" runat="server" TargetControlID="txtYearThree"
                                                                                                                                    FilterType="Numbers">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                                <%-- <asp:CustomValidator ID="cusvHaskellProjectManagerThree" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvHaskellProjectManagerThree_ServerValidate"
                                                                                                            EnableClientScript="false">
                                                                                                        </asp:CustomValidator>--%>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </span><span id="trAddProject04" runat="server" style="display: none">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Name
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Manager
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="50%">
                                                                                                                                Year Completed
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectNameFour" runat="server" MaxLength="100"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%-- <Ajax:FilteredTextBoxExtender ID="ftxtHaskellProjectNameFour" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                            ValidChars=" " TargetControlID="txtHaskellProjectNameFour">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectManagerFour" runat="server" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<Ajax:FilteredTextBoxExtender ID="ftbeHaskellProjectManagerFour" runat="server" TargetControlID="txtHaskellProjectManagerFour"
                                                                                                                            FilterType="LowercaseLetters,UppercaseLetters,Custom" ValidChars=" ">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtYearFour" runat="server" MaxLength="4"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<asp:CustomValidator ID="cusvYearFour" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvYearFour_ServerValidate" EnableClientScript="false"></asp:CustomValidator>--%>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftbeYearFour" runat="server" TargetControlID="txtYearFour"
                                                                                                                                    FilterType="Numbers">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                                <%--       <asp:CustomValidator ID="cusvHaskellProjectManagerFour" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvHaskellProjectManagerFour_ServerValidate"
                                                                                                            EnableClientScript="false"></asp:CustomValidator>--%>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </span><span id="trAddProject05" runat="server" style="display: none">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Name
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Manager
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="50%">
                                                                                                                                Year Completed
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectNameFive" runat="server" MaxLength="100"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--  <Ajax:FilteredTextBoxExtender ID="ftxtHaskellProjectNameFive" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                            ValidChars=" " TargetControlID="txtHaskellProjectNameFive">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectManagerFive" runat="server" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%-- <Ajax:FilteredTextBoxExtender ID="ftbeHaskellProjectManagerFive" runat="server" TargetControlID="txtHaskellProjectManagerFive"
                                                                                                                            FilterType="LowercaseLetters,UppercaseLetters,Custom" ValidChars=" ">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtYearFive" runat="server" MaxLength="4"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<asp:CustomValidator ID="cusvYearFive" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvYearFive_ServerValidate" EnableClientScript="false"></asp:CustomValidator>--%>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftbeYearFive" runat="server" TargetControlID="txtYearFive"
                                                                                                                                    FilterType="Numbers">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                                <%--         <asp:CustomValidator ID="cusvHaskellProjectManagerFive" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvHaskellProjectManagerFive_ServerValidate"
                                                                                                            EnableClientScript="false"></asp:CustomValidator>--%>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </span><span id="trAddProject06" runat="server" style="display: none">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Name
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Manager
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="50%">
                                                                                                                                Year Completed
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectNameSix" runat="server" MaxLength="100"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%-- <Ajax:FilteredTextBoxExtender ID="ftxtHaskellProjectNameSix" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                                                                                                                            ValidChars=" " TargetControlID="txtHaskellProjectNameSix">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectManagerSix" runat="server" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<Ajax:FilteredTextBoxExtender ID="ftbeHaskellProjectManagerSix" runat="server" TargetControlID="txtHaskellProjectManagerSix"
                                                                                                                            FilterType="LowercaseLetters,UppercaseLetters,Custom" ValidChars=" ">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtYearSix" runat="server" MaxLength="4"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <%--<asp:CustomValidator ID="cusvYearSix" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvYearSix_ServerValidate" EnableClientScript="false"></asp:CustomValidator>--%>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftbeYearSix" runat="server" TargetControlID="txtYearSix"
                                                                                                                                    FilterType="Numbers">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                                <%--<asp:CustomValidator ID="cusvHaskellProjectManagerSix" runat="server" ValidationGroup="ProjectReferences"
                                                                                                            Display="None" OnServerValidate="CusvHaskellProjectManagerSix_ServerValidate"
                                                                                                            EnableClientScript="false"></asp:CustomValidator>--%>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </span><span id="trAddProject07" runat="server" style="display: none">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Name
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Manager
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="50%">
                                                                                                                                Year Completed
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectNameSeven" runat="server" MaxLength="100"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectManagerSeven" runat="server"
                                                                                                                                    MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtYearSeven" runat="server" MaxLength="4"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftbeYearSeven" runat="server" TargetControlID="txtYearSeven"
                                                                                                                                    FilterType="Numbers">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </span><span id="trAddProject08" runat="server" style="display: none">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Name
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Manager
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="50%">
                                                                                                                                Year Completed
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectNameEight" runat="server" MaxLength="100"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectManagerEight" runat="server"
                                                                                                                                    MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtYearEight" runat="server" MaxLength="4"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftbeYearEight" runat="server" TargetControlID="txtYearEight"
                                                                                                                                    FilterType="Numbers">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </span><span id="trAddProject09" runat="server" style="display: none">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Name
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Manager
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="50%">
                                                                                                                                Year Completed
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectNameNine" runat="server" MaxLength="100"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectManagerNine" runat="server" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtYearNine" runat="server" MaxLength="4"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftbeYearNine" runat="server" TargetControlID="txtYearNine"
                                                                                                                                    FilterType="Numbers">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </span><span id="trAddProject10" runat="server" style="display: none">
                                                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Name
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="25%">
                                                                                                                                Project Manager
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt" width="50%">
                                                                                                                                Year Completed
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectNameTen" runat="server" MaxLength="100"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtHaskellProjectManagerTen" runat="server" MaxLength="50"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td class="formtdrt">
                                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtYearTen" runat="server" MaxLength="4"
                                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftbeYearTen" runat="server" TargetControlID="txtYearTen"
                                                                                                                                    FilterType="Numbers">
                                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </span>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="3" id="tdlnkbtnaddproject" align="center" style="display: none" runat="server">
                                                                                                                <asp:LinkButton ID="lnladdProject" runat="server" CssClass="hyplinks">Click to Add More Information</asp:LinkButton>
                                                                                                                <asp:HiddenField ID="hdnaddProjectcount" runat="server" />
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </Content>
                                                                    </Ajax:AccordionPane>
                                                                    <Ajax:AccordionPane ID="apnSupplier" runat="server">
                                                                        <Header>
                                                                            Supplier/Credit
                                                                        </Header>
                                                                        <Content>
                                                                            <table border="0" cellpadding="5" cellspacing="5" width="100%" class="searchtableclass">
                                                                                <tr>
                                                                                    <td class="left_border formtdbold_rt">
                                                                                        <!-- Modified by N Schoenberger on 8/10/2011 -->
                                                                                        <!-- Please provide us at least 3 supplier/credit references. -->
                                                                                        Please provide a minumum of three supplier/credit references.
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="left_border bodytextboldBlue">
                                                                                        <!-- Modified by N Schoenberger on 9/29/2011 -->
                                                                                        Many companies have a policy against providing credit references. Please verify
                                                                                        before completing the next section, that the suppliers you list provide credit references
                                                                                        and the correct contact information for obtaining the reference.
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellpadding="1" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="keyheader">
                                                                                                    Supplier/Credit One
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table id="Table1" width="100%" runat="server" cellpadding="2" cellspacing="0" border="0">
                                                                                                        <tr>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                                <span class="mandatorystar">*</span>Company Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                                <span class="mandatorystar">*</span>Company Address
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="30%">
                                                                                                                <span class="mandatorystar">*</span>Country
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxlarge" ID="txtCompanyNameOne" runat="server" MaxLength="100"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqCompanyNameOne" ControlToValidate="txtCompanyNameOne"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter company name under supplier/credit one" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxlargeCustom" ID="txtCompanyAddressOne" runat="server"
                                                                                                                    MaxLength="100" TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqCompanyAddressOne" ControlToValidate="txtCompanyAddressOne"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter company address under supplier/credit  one" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:DropDownList  ID="ddlCountryFour" runat="server" CssClass="ddlboxCountry"
                                                                                                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>City
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>State <span class="spnState2" id="trOther4lbl"
                                                                                                                    runat="server" style="display: none">Please Specify State </span>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>ZIP/Postal Code
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxmedium " ID="txtSuplCityFour" runat="server" MaxLength="50"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqtxtSuplCityFour" ControlToValidate="txtSuplCityFour"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter city under supplier/credit one" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:DropDownList ID="ddlStateFour" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                    TabIndex="0">
                                                                                                                </asp:DropDownList>
                                                                                                                <span id="trOther4" runat="server" style="display: none">
                                                                                                                    <asp:TextBox ID="txtOtherStateFour" runat="server" CssClass="txtboxstate" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <asp:CustomValidator ID="cusvOtherStateFour" runat="server" ValidationGroup="Supplier"
                                                                                                                        SetFocusOnError="true" Display="None" OnServerValidate="CusvOtherStateFour_ServerValidate"
                                                                                                                        EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateFour" TargetControlID="txtOtherStateFour"
                                                                                                                        FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars=" ,@"
                                                                                                                        InvalidChars="_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </span>
                                                                                                            </td>
                                                                                                            <td class="formtdrt" nowrap="nowrap">
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplZipOne" runat="server" MaxLength="5"
                                                                                                                    TabIndex="0"></asp:TextBox>&nbsp;
                                                                                                                <asp:RequiredFieldValidator ID="reqvSuplZipOne" ControlToValidate="txtSuplZipOne"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter ZIP/Postal Code under supplier/credit one" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                                <asp:RegularExpressionValidator ID="regvsuplzipone" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under supplier/Credit one"
                                                                                                                    ControlToValidate="txtSuplZipOne" ValidationExpression="^[0-9a-zA-Z -]+$" ValidationGroup="Supplier"
                                                                                                                    SetFocusOnError="true" EnableClientScript="true" Display="None">
                                                                                                                </asp:RegularExpressionValidator>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplZipOne" runat="server" TargetControlID="txtSuplZipOne"
                                                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>Contact Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>Phone Number
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt" valign="top">
                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtContactNameOne" runat="server" MaxLength="50"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqContactNameOne" ControlToValidate="txtContactNameOne"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter contact name under supplier/credit one" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplMainPhoneOne" runat="server" MaxLength="3"
                                                                                                                    TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplMainPhoneTwo" runat="server" MaxLength="3"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <span id="spnSuplMainPhoneTwo" runat="server">-</span>
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplMainPhoneThree" runat="server" MaxLength="4"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplMainPhoneOne" runat="server" TargetControlID="txtSuplMainPhoneOne"
                                                                                                                    FilterType="Numbers">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplMainPhoneTwo" runat="server" TargetControlID="txtSuplMainPhoneTwo"
                                                                                                                    FilterType="Numbers">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplMainPhoneThree" runat="server" TargetControlID="txtSuplMainPhoneThree"
                                                                                                                    FilterType="Numbers">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                <asp:CustomValidator ID="cusvSuplMainPhon" runat="server" Display="None" ValidationGroup="Supplier"
                                                                                                                    EnableClientScript="false" OnServerValidate="CusvSuplMainPhon_ServerValidate"></asp:CustomValidator>
                                                                                                                <div id="divSuplMainPhoneTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                    Country Code / Area + Phone</div>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <div id="divSpeaksEnglishOne" runat="server">
                                                                                                                    Speaks English?
                                                                                                                    <asp:DropDownList ID="ddlSupSpeaksEnglishOne" runat="server" CssClass="ddlbox">
                                                                                                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellpadding="1" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="keyheader">
                                                                                                    Supplier/Credit Two
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="emptytdheight">
                                                                                                    <table id="tblSecondSupplier" runat="server" cellpadding="2" cellspacing="0" border="0"
                                                                                                        width="100%">
                                                                                                        <tr>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                                <span class="mandatorystar">*</span>Company Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                                <span class="mandatorystar">*</span>Company Address
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="30%">
                                                                                                                <span class="mandatorystar">*</span>Country
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxlarge" ID="txtCompanyNameTwo" runat="server" MaxLength="100"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqvCompanyNameTwo" ControlToValidate="txtCompanyNameTwo"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter company name under supplier/credit two" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxlargeCustom" ID="txtCompanyAddressTwo" runat="server" MaxLength="100"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqvCompanyAddressTwo" ControlToValidate="txtCompanyAddressTwo"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter company address under supplier/credit two" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:DropDownList  ID="ddlCountryFive" runat="server" CssClass="ddlboxCountry"
                                                                                                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>City
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>State <span class="spnState2" id="trOther5lbl"
                                                                                                                    runat="server" style="display: none">Please Specify State </span>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>ZIP/Postal Code
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxmedium" ID="txtSuplCityFive" runat="server" MaxLength="50"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqvSuplCityFive" ControlToValidate="txtSuplCityFive"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter city under supplier/credit two" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:DropDownList ID="ddlStateFive" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                    TabIndex="0">
                                                                                                                </asp:DropDownList>
                                                                                                                <span id="trOther5" runat="server" style="display: none">
                                                                                                                    <asp:TextBox ID="txtOtherStateFive" runat="server" CssClass="txtboxstate" MaxLength="50"></asp:TextBox>
                                                                                                                    <asp:CustomValidator ID="cusvOtherStateFive" runat="server" ValidationGroup="Supplier"
                                                                                                                        SetFocusOnError="true" Display="None" OnServerValidate="CusvOtherStateFive_ServerValidate"
                                                                                                                        EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateFive" TargetControlID="txtOtherStateFive"
                                                                                                                        FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars=" ,@"
                                                                                                                        InvalidChars="_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </span>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplZipTwo" runat="server" MaxLength="5"
                                                                                                                    TabIndex="0"></asp:TextBox>&nbsp;
                                                                                                                <asp:RequiredFieldValidator ID="reqvSuplZipTwo" ControlToValidate="txtSuplZipTwo"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter ZIP under supplier/credit two" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                                <asp:RegularExpressionValidator ID="regsuplziptwo" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under supplier/credit two"
                                                                                                                    ControlToValidate="txtSuplZipTwo" ValidationExpression="^[0-9a-zA-Z -]+$" ValidationGroup="Supplier"
                                                                                                                    SetFocusOnError="true" EnableClientScript="true" Display="None">
                                                                                                                </asp:RegularExpressionValidator>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplZipTwo" runat="server" TargetControlID="txtSuplZipTwo"
                                                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>Contact Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>Phone Number
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt" valign="top">
                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtContactNameTwo" runat="server" MaxLength="50"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqvContactNameTwo" ControlToValidate="txtContactNameTwo"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter contact name under supplier/credit two" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplSecondPhoneOne" runat="server" MaxLength="3"
                                                                                                                    TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplSecondPhoneTwo" runat="server" MaxLength="3"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <span id="spnSuplSecondPhoneTwo" runat="server">-</span>
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplSecondPhoneThree" runat="server" MaxLength="4"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplSecondPhoneOne" runat="server" TargetControlID="txtSuplSecondPhoneOne"
                                                                                                                    FilterType="Numbers">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplSecondPhoneTwo" runat="server" TargetControlID="txtSuplSecondPhoneTwo"
                                                                                                                    FilterType="Numbers">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplSecondPhoneThree" runat="server" TargetControlID="txtSuplSecondPhoneThree"
                                                                                                                    FilterType="Numbers">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                <asp:CustomValidator ID="cusvSuplSecondPhone" runat="server" Display="None" ValidationGroup="Supplier"
                                                                                                                    EnableClientScript="false" OnServerValidate="CusvSuplSecondPhone_ServerValidate"></asp:CustomValidator>
                                                                                                                <div id="divSuplSecondPhoneTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                    Country Code / Area + Phone</div>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <div id="divSpeaksEnglishTwo" runat="server">
                                                                                                                    Speaks English?
                                                                                                                    <asp:DropDownList ID="ddlSupSpeaksEnglishTwo" runat="server" CssClass="ddlbox">
                                                                                                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellpadding="1" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="keyheader">
                                                                                                    Supplier/Credit Three
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="emptytdheight">
                                                                                                    <table id="Table11" runat="server" cellpadding="2" cellspacing="0" border="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                                <span class="mandatorystar">*</span>Company Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                                <span class="mandatorystar">*</span>Company Address
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="30%">
                                                                                                                <span class="mandatorystar">*</span>Country
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxlarge" ID="txtCompanyNameThree" runat="server"
                                                                                                                    MaxLength="100" TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqvtxtCompanyNameThree" ControlToValidate="txtCompanyNameThree"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter company name under supplier/credit three" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxlargeCustom" ID="txtCompanyAddressThree" runat="server" MaxLength="100"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqvCompanyAddressThree" ControlToValidate="txtCompanyAddressThree"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter company address under supplier/credit three" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:DropDownList  ID="ddlCountrySix" runat="server" CssClass="ddlboxCountry"
                                                                                                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>City
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>State <span class="spnState2" id="trOther6lbl"
                                                                                                                    runat="server" style="display: none">Please Specify State </span>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>ZIP/Postal Code
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxmedium" ID="txtSuplCitySix" runat="server" MaxLength="50"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqvSuplCitySix" ControlToValidate="txtSuplCitySix"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter city under supplier/credit three" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:DropDownList ID="ddlStateSix" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                    TabIndex="0">
                                                                                                                </asp:DropDownList>
                                                                                                                <span id="trOther6" runat="server" style="display: none">
                                                                                                                    <asp:TextBox ID="txtOtherStateSix" runat="server" CssClass="txtboxstate" MaxLength="50"></asp:TextBox>
                                                                                                                    <asp:CustomValidator ID="cusvOtherStateSix" runat="server" ValidationGroup="Supplier"
                                                                                                                        SetFocusOnError="true" Display="None" OnServerValidate="CusvOtherStateSix_ServerValidate"
                                                                                                                        EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateSix" TargetControlID="txtOtherStateSix"
                                                                                                                        FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                        InvalidChars="_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </span>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplZipThree" runat="server" MaxLength="5"
                                                                                                                    TabIndex="0"></asp:TextBox>&nbsp;
                                                                                                                <asp:RequiredFieldValidator ID="reqvtSuplZipThree" ControlToValidate="txtSuplZipThree"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter ZIP under supplier/credit three" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                                <asp:RegularExpressionValidator ID="regvSuplZipThree" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under supplier/credit three"
                                                                                                                    ControlToValidate="txtSuplZipThree" ValidationExpression="^[0-9a-zA-Z -]+$" ValidationGroup="Supplier"
                                                                                                                    SetFocusOnError="true" EnableClientScript="true" Display="None">
                                                                                                                </asp:RegularExpressionValidator>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplZipThree" runat="server" TargetControlID="txtSuplZipThree"
                                                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>Contact Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <span class="mandatorystar">*</span>Phone Number
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtdrt" valign="top">
                                                                                                                <asp:TextBox CssClass="txtbox" ID="txtContactNameThree" runat="server" MaxLength="50"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="reqvtxtContactNameThree" ControlToValidate="txtContactNameThree"
                                                                                                                    Display="None" runat="server" ValidationGroup="Supplier" SetFocusOnError="true"
                                                                                                                    ErrorMessage="Please enter contact name under supplier/credit three" EnableClientScript="true">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplThirdPhoneOne" runat="server" MaxLength="3"
                                                                                                                    TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplThirdPhoneTwo" runat="server" MaxLength="3"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <span id="spnSuplThirdPhoneTwo" runat="server">-</span>
                                                                                                                <asp:TextBox CssClass="txtboxsmall" ID="txtSuplThirdPhoneThree" runat="server" MaxLength="4"
                                                                                                                    TabIndex="0"></asp:TextBox>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplThirdPhoneOne" runat="server" TargetControlID="txtSuplThirdPhoneOne"
                                                                                                                    FilterType="Numbers">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplThirdPhoneTwo" runat="server" TargetControlID="txtSuplThirdPhoneTwo"
                                                                                                                    FilterType="Numbers">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSuplThirdPhoneThree" runat="server" TargetControlID="txtSuplThirdPhoneThree"
                                                                                                                    FilterType="Numbers">
                                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                                <asp:CustomValidator ID="cusvSuplThirdPhone" runat="server" Display="None" ValidationGroup="Supplier"
                                                                                                                    EnableClientScript="false" OnServerValidate="CusvSuplThirdPhone_ServerValidate"></asp:CustomValidator>
                                                                                                                <div id="divSuplThirdPhoneTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                    Country Code / Area + Phone</div>
                                                                                                            </td>
                                                                                                            <td class="formtdrt">
                                                                                                                <div id="divSpeaksEnglishThree" runat="server">
                                                                                                                    Speaks English? &nbsp;
                                                                                                                    <asp:DropDownList ID="ddlSupSpeaksEnglishThree" runat="server" CssClass="ddlbox">
                                                                                                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="spnmore1" runat="server" style="display: none">
                                                                                    <td>
                                                                                        <table cellpadding="1" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td class="keyheader">
                                                                                                    Supplier/Credit Four
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="emptytdheight">
                                                                                                    <span>
                                                                                                        <table width="100%" id="tblSecondary" runat="server" cellspacing="0" cellpadding="2"
                                                                                                            border="0">
                                                                                                            <tr>
                                                                                                                <td class="formtdrt" width="35%">
                                                                                                                    Company Name
                                                                                                                </td>
                                                                                                                <td class="formtdrt" width="35%">
                                                                                                                    Company Address
                                                                                                                </td>
                                                                                                                <td class="formtdrt" width="30%">
                                                                                                                    Country
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtCompanyNameFour" runat="server"
                                                                                                                        MaxLength="100" TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlargeCustom" ID="txtCompanyAddressFour" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList  ID="ddlCountrySeven" runat="server" CssClass="ddlboxCountry"
                                                                                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    City
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    State <span class="spnState2" id="trOther7lbl" runat="server" style="display: none">
                                                                                                                        Please Specify State </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    ZIP/Postal Code
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtSuplCitySeven" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList ID="ddlStateSeven" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                        TabIndex="0">
                                                                                                                    </asp:DropDownList>
                                                                                                                    <span id="trOther7" runat="server" style="display: none">
                                                                                                                        <asp:TextBox ID="txtOtherStateSeven" runat="server" CssClass="txtboxstate" MaxLength="50"></asp:TextBox>
                                                                                                                        <asp:CustomValidator ID="cusvOtherStateSeven" runat="server" ValidationGroup="Supplier"
                                                                                                                            SetFocusOnError="true" Display="None" OnServerValidate="CusvOtherStateSeven_ServerValidate"
                                                                                                                            EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateSeven" TargetControlID="txtOtherStateSeven"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplZipFour" runat="server" MaxLength="5"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;
                                                                                                                    <asp:RegularExpressionValidator ID="regvSuplZipFour" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under supplier/credit four"
                                                                                                                        ControlToValidate="txtSuplZipFour" ValidationExpression="^[0-9a-zA-Z -]+$" ValidationGroup="Supplier"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None">
                                                                                                                    </asp:RegularExpressionValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplZipFour" runat="server" TargetControlID="txtSuplZipFour"
                                                                                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    Contact Name
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    Phone Number
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt" valign="top">
                                                                                                                    <asp:TextBox CssClass="txtbox" ID="txtContactNameFour" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplFourthPhoneOne" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplFourthPhoneTwo" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <span id="spnSuplFourthPhoneTwo" runat="server">-</span>
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplFourthPhoneThree" runat="server" MaxLength="4"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtuplFourthPhoneOne" runat="server" TargetControlID="txtSuplFourthPhoneOne"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplFourthPhoneTwo" runat="server" TargetControlID="txtSuplFourthPhoneTwo"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftbxSuplFourthPhoneThree" runat="server" TargetControlID="txtSuplFourthPhoneThree"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="cusvSuplFourthPhone" runat="server" Display="None" ValidationGroup="Supplier"
                                                                                                                        EnableClientScript="false" OnServerValidate="cusSuplFourthPhone_ServerValidate"></asp:CustomValidator>
                                                                                                                    <div id="divSuplFourthPhoneTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <div id="divSpeaksEnglishFour" runat="server">
                                                                                                                        Speaks English? &nbsp;
                                                                                                                        <asp:DropDownList ID="ddlSupSpeaksEnglishFour" runat="server" CssClass="ddlbox">
                                                                                                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="tdheight">&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="spnmore2" runat="server" style="display: none" cellpadding="0" cellspacing="0"
                                                                                    border="0" width="100%">
                                                                                    <td>
                                                                                        <table cellpadding="1" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="keyheader">
                                                                                                    Supplier/Credit Five
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="emptytdheight">
                                                                                                    <span>
                                                                                                        <table width="100%" id="Table2" runat="server" cellpadding="2" cellspacing="0" border="0">
                                                                                                            <tr>
                                                                                                         
  <td class="formtdrt" width="35%">
                                                                                                                Company Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                           Company Address
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="30%">
                                                                                                             Country
                                                                                                            </td>

                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtCompanyNameFive" runat="server"
                                                                                                                        MaxLength="100" TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlargeCustom" ID="txtCompanyAddressFive" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList  ID="ddlCountryEight" runat="server" CssClass="ddlboxCountry"
                                                                                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                             
                                                                                                            </tr>
                                                                                                            <tr>
 <td class="formtdrt">
                                                                                                            City
                                                                                                            </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    State <span class="spnState2" id="trOther8lbl" runat="server" style="display: none">
                                                                                                                        Please Specify State </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    ZIP/Postal Code
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>   <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtSuplCityEight" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList ID="ddlStateEight" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                        TabIndex="0">
                                                                                                                    </asp:DropDownList>
                                                                                                                    <span id="trOther8" runat="server" style="display: none">
                                                                                                                        <asp:TextBox ID="txtOtherStateEight" runat="server" CssClass="txtboxstate" MaxLength="50"></asp:TextBox>
                                                                                                                        <asp:CustomValidator ID="cusvOtherStateEight" runat="server" ValidationGroup="Supplier"
                                                                                                                            SetFocusOnError="true" Display="None" OnServerValidate="CusvOtherStateEight_ServerValidate"
                                                                                                                            EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateEight" TargetControlID="txtOtherStateEight"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplZipFive" runat="server" MaxLength="5"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;
                                                                                                                    <asp:RegularExpressionValidator ID="regvSuplZipFive" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under supplier/credit five"
                                                                                                                        ControlToValidate="txtSuplZipFive" ValidationExpression="^[0-9a-zA-Z -]+$" ValidationGroup="Supplier"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None">
                                                                                                                    </asp:RegularExpressionValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplZipFive" runat="server" TargetControlID="txtSuplZipFive"
                                                                                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    Contact Name
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    Phone Number
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt" valign="top">
                                                                                                                    <asp:TextBox CssClass="txtbox" ID="txtContactNameFive" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplFivePhoneOne" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplFivePhoneTwo" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <span id="spnSuplFivePhoneTwo" runat="server">-</span>
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplFivePhoneThree" runat="server" MaxLength="4"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplFivePhoneOne" runat="server" TargetControlID="txtSuplFivePhoneOne"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplFivePhoneTwo" runat="server" TargetControlID="txtSuplFivePhoneTwo"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplFivePhoneThree" runat="server" TargetControlID="txtSuplFivePhoneThree"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="cusvSuplFivePhone" runat="server" Display="None" ValidationGroup="Supplier"
                                                                                                                        EnableClientScript="false" OnServerValidate="CusvSuplFivePhone_ServerValidate"></asp:CustomValidator>
                                                                                                                    <div id="divSuplFivePhoneTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <div id="divSpeaksEnglishFive" runat="server">
                                                                                                                        Speaks English? &nbsp;
                                                                                                                        <asp:DropDownList ID="ddlSupSpeaksEnglishFive" runat="server" CssClass="ddlbox">
                                                                                                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="spnmore3" runat="server" style="display: none">
                                                                                    <td>
                                                                                        <table cellpadding="1" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="keyheader">
                                                                                                    Supplier/Credit Six
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="emptytdheight">
                                                                                                    <span>
                                                                                                        <table width="100%" id="Table3" runat="server" cellpadding="2" cellspacing="0" border="0">
                                                                                                            <tr>
                                                                                                              
  <td class="formtdrt" width="35%">
                                                                                                                Company Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                           Company Address
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="30%">
                                                                                                             Country
                                                                                                            </td>

                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtCompanyNameSix" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlargeCustom" ID="txtCompanyAddressSix" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList  ID="ddlCountryNine" runat="server" CssClass="ddlboxCountry"
                                                                                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                               
                                                                                                            </tr>
                                                                                                            <tr>
 <td class="formtdrt">
                                                                                                            City
                                                                                                            </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    State <span class="spnState2" id="trOther9lbl" runat="server" style="display: none">
                                                                                                                        Please Specify State </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    ZIP/Postal Code
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr> <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtSuplCityNine" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList ID="ddlStateNine" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                        TabIndex="0">
                                                                                                                    </asp:DropDownList>
                                                                                                                    <span id="trOther9" runat="server" style="display: none">
                                                                                                                        <asp:TextBox ID="txtOtherStateNine" runat="server" CssClass="txtboxstate" MaxLength="50"></asp:TextBox>
                                                                                                                        <asp:CustomValidator ID="cusvOtherStateNine" runat="server" ValidationGroup="Supplier"
                                                                                                                            SetFocusOnError="true" Display="None" OnServerValidate="CusvOtherStateNine_ServerValidate"
                                                                                                                            EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateNine" TargetControlID="txtOtherStateNine"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplZipSix" runat="server" MaxLength="5"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;
                                                                                                                    <asp:RegularExpressionValidator ID="regvSuplZipSix" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under supplier/credit six"
                                                                                                                        ControlToValidate="txtSuplZipSix" ValidationExpression="^[0-9a-zA-Z -]+$" ValidationGroup="Supplier"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None">
                                                                                                                    </asp:RegularExpressionValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplZipSix" runat="server" TargetControlID="txtSuplZipSix"
                                                                                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    Contact Name
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    Phone Number
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt" valign="top">
                                                                                                                    <asp:TextBox CssClass="txtbox" ID="txtContactNameSix" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplSixPhoneOne" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplSixPhoneTwo" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <span id="spnSuplSixPhoneTwo" runat="server">-</span>
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplSixPhoneThree" runat="server" MaxLength="4"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplSixPhoneOne" runat="server" TargetControlID="txtSuplSixPhoneOne"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplSixPhoneTwo" runat="server" TargetControlID="txtSuplSixPhoneTwo"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplSixPhoneThree" runat="server" TargetControlID="txtSuplSixPhoneThree"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="cusvSuplSixPhone" runat="server" Display="None" ValidationGroup="Supplier"
                                                                                                                        EnableClientScript="false" OnServerValidate="CusvSuplSixPhone_ServerValidate"></asp:CustomValidator>
                                                                                                                    <div id="divSuplSixPhoneTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <div id="divSpeaksEnglishSix" runat="server">
                                                                                                                        Speaks English? &nbsp;
                                                                                                                        <asp:DropDownList ID="ddlSupSpeaksEnglishSix" runat="server" CssClass="ddlbox">
                                                                                                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="spnmore4" runat="server" style="display: none">
                                                                                    <td>
                                                                                        <table cellpadding="1" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="keyheader">
                                                                                                    Supplier/Credit Seven
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="emptytdheight">
                                                                                                    <span>
                                                                                                        <table width="100%" id="Table4" runat="server" cellpadding="2" cellspacing="0" border="0">
                                                                                                            <tr>
                                                                                                               
  <td class="formtdrt" width="35%">
                                                                                                                Company Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                           Company Address
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="30%">
                                                                                                             Country
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtCompanyNameSeven" runat="server"
                                                                                                                        MaxLength="100" TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlargeCustom" ID="txtCompanyAddressSeven" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList  ID="ddlCountryTen" runat="server" CssClass="ddlboxCountry"
                                                                                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                             
                                                                                                            </tr>
                                                                                                            <tr>
 <td class="formtdrt">
                                                                                                            City
                                                                                                            </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    State <span class="spnState2" id="trOther10lbl" runat="server" style="display: none">
                                                                                                                        Please Specify State </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    ZIP/Postal Code
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>   <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtSuplCityTen" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList ID="ddlStateTen" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                        TabIndex="0">
                                                                                                                    </asp:DropDownList>
                                                                                                                    <span id="trOther10" runat="server" style="display: none">
                                                                                                                        <asp:TextBox ID="txtOtherStateTen" runat="server" CssClass="txtboxstate" MaxLength="50"></asp:TextBox>
                                                                                                                        <asp:CustomValidator ID="cusvOtherStateTen" runat="server" ValidationGroup="Supplier"
                                                                                                                            SetFocusOnError="true" Display="None" OnServerValidate="CusvOtherStateTen_ServerValidate"
                                                                                                                            EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateTen" TargetControlID="txtOtherStateTen"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplZipSeven" runat="server" MaxLength="5"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;
                                                                                                                    <asp:RegularExpressionValidator ID="regvSuplZipSeven" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under supplier/credit seven"
                                                                                                                        ControlToValidate="txtSuplZipSeven" ValidationExpression="^[0-9a-zA-Z -]+$" ValidationGroup="Supplier"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None">
                                                                                                                    </asp:RegularExpressionValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplZipSeven" runat="server" TargetControlID="txtSuplZipSeven"
                                                                                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    Contact Name
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    Phone Number
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt" valign="top">
                                                                                                                    <asp:TextBox CssClass="txtbox" ID="txtContactNameSeven" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplSevenPhoneOne" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplSevenPhoneTwo" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <span id="spnSuplSevenPhoneTwo" runat="server">-</span>
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplSevenPhoneThree" runat="server" MaxLength="4"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplSevenPhoneOne" runat="server" TargetControlID="txtSuplSevenPhoneOne"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplSevenPhoneTwo" runat="server" TargetControlID="txtSuplSevenPhoneTwo"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplSevenPhoneThree" runat="server" TargetControlID="txtSuplSevenPhoneThree"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="cusvSuplSevenPhone" runat="server" Display="None" ValidationGroup="Supplier"
                                                                                                                        EnableClientScript="false" OnServerValidate="CusvSuplSevenPhone_ServerValidate"></asp:CustomValidator>
                                                                                                                    <div id="divSuplSevenPhoneTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <div id="divSpeaksEnglishSeven" runat="server">
                                                                                                                        Speaks English? &nbsp;
                                                                                                                        <asp:DropDownList ID="ddlSupSpeaksEnglishSeven" runat="server" CssClass="ddlbox">
                                                                                                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="spnmore5" runat="server" style="display: none">
                                                                                    <td>
                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="keyheader">
                                                                                                    Supplier/Credit Eight
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="emptytdheight">
                                                                                                    <span>
                                                                                                        <table width="100%" id="Table5" runat="server" cellpadding="2" cellspacing="0" border="0">
                                                                                                            <tr>
                                                                                                             
  <td class="formtdrt" width="35%">
                                                                                                                Company Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                           Company Address
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="30%">
                                                                                                             Country
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtCompanyNameEight" runat="server"
                                                                                                                        MaxLength="100" TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlargeCustom" ID="txtCompanyAddressEight" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList  ID="ddlCountryEleven" runat="server" CssClass="ddlboxCountry"
                                                                                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                               
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                            
 <td class="formtdrt">
                                                                                                            City
                                                                                                            </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    State <span class="spnState2" id="trOther11lbl" runat="server" style="display: none">
                                                                                                                        Please Specify State </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    ZIP/Postal Code
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                             <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtSuplCityEleven" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"> </asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList ID="ddlStateEleven" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                        TabIndex="0">
                                                                                                                    </asp:DropDownList>
                                                                                                                    <span id="trOther11" runat="server" style="display: none">
                                                                                                                        <asp:TextBox ID="txtOtherStateEleven" runat="server" CssClass="txtboxstate" MaxLength="50"></asp:TextBox>
                                                                                                                        <asp:CustomValidator ID="cusvOtherStateEleven" runat="server" ValidationGroup="Supplier"
                                                                                                                            SetFocusOnError="true" Display="None" OnServerValidate="CusvOtherStateEleven_ServerValidate"
                                                                                                                            EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateEleven" TargetControlID="txtOtherStateEleven"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplZipEight" runat="server" MaxLength="5"></asp:TextBox>&nbsp;
                                                                                                                    <asp:RegularExpressionValidator ID="regvSuplZipEight" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under supplier/credit eight"
                                                                                                                        ControlToValidate="txtSuplZipEight" ValidationExpression="^[0-9a-zA-Z -]+$" ValidationGroup="Supplier"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None">
                                                                                                                    </asp:RegularExpressionValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplZipEight" runat="server" TargetControlID="txtSuplZipEight"
                                                                                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    Contact Name
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    Phone Number
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt" valign="top">
                                                                                                                    <asp:TextBox CssClass="txtbox" ID="txtContactNameEight" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplEightPhoneOne" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplEightPhoneTwo" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <span id="spnSuplEightPhoneTwo" runat="server">-</span>
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplEightPhoneThree" runat="server" MaxLength="4"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplEightPhoneOne" runat="server" TargetControlID="txtSuplEightPhoneOne"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplEightPhoneTwo" runat="server" TargetControlID="txtSuplEightPhoneTwo"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplEightPhoneThree" runat="server" TargetControlID="txtSuplEightPhoneThree"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="cusvSuplEightPhone" runat="server" Display="None" ValidationGroup="Supplier"
                                                                                                                        EnableClientScript="false" OnServerValidate="CusvSuplEightPhone_ServerValidate"></asp:CustomValidator>
                                                                                                                    <div id="divSuplEightPhoneTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <div id="divSpeaksEnglishEight" runat="server">
                                                                                                                        Speaks English? &nbsp;
                                                                                                                        <asp:DropDownList ID="ddlSupSpeaksEnglishEight" runat="server" CssClass="ddlbox">
                                                                                                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="spnmore6" runat="server" style="display: none">
                                                                                    <td>
                                                                                        <table cellpadding="1" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="keyheader">
                                                                                                    Supplier/Credit Nine
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="emptytdheight">
                                                                                                    <span>
                                                                                                        <table width="100%" id="Table6" runat="server" cellpadding="2" cellspacing="0" border="0">
                                                                                                            <tr>
                                                                                                               <td class="formtdrt" width="35%">
                                                                                                                Company Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                           Company Address
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="30%">
                                                                                                             Country
                                                                                                            </td>

                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtCompanyNameNine" runat="server"
                                                                                                                        MaxLength="100" TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlargeCustom" ID="txtCompanyAddressNine" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList  ID="ddlCountryTwelve" runat="server" CssClass="ddlboxCountry"
                                                                                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                             
                                                                                                            </tr>
                                                                                                            <tr>
 <td class="formtdrt">
                                                                                                            City
                                                                                                            </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    State <span class="spnState2" id="trOther12lbl" runat="server" style="display: none">
                                                                                                                        Please Specify State </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" colspan="3">
                                                                                                                    ZIP/Postal Code
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                               <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtSuplCityTwelve" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList ID="ddlStateTwelve" runat="server" CssClass="ddlboxState" AutoPostBack="false"
                                                                                                                        TabIndex="0">
                                                                                                                    </asp:DropDownList>
                                                                                                                    <span id="trOther12" runat="server" style="display: none">
                                                                                                                        <asp:TextBox ID="txtOtherStateTwelve" runat="server" CssClass="txtboxstate" MaxLength="50"></asp:TextBox>
                                                                                                                        <asp:CustomValidator ID="cusvOtherStateTwelve" runat="server" ValidationGroup="Supplier"
                                                                                                                            SetFocusOnError="true" Display="None" OnServerValidate="CusvOtherStateTwelve_ServerValidate"
                                                                                                                            EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateTwelve" TargetControlID="txtOtherStateTwelve"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplZipNine" runat="server" MaxLength="5"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;
                                                                                                                    <asp:RegularExpressionValidator ID="regvSuplZipNine" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under supplier/credit nine"
                                                                                                                        ControlToValidate="txtSuplZipNine" ValidationExpression="^[0-9a-zA-Z -]+$" ValidationGroup="Supplier"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None">
                                                                                                                    </asp:RegularExpressionValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplZipNine" runat="server" TargetControlID="txtSuplZipNine"
                                                                                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    Contact Name
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    Phone Number
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt" valign="top">
                                                                                                                    <asp:TextBox CssClass="txtbox" ID="txtContactNameNine" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplNinePhoneOne" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplNinePhoneTwo" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <span id="spnSuplNinePhoneTwo" runat="server">-</span>
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplNinePhoneThree" runat="server" MaxLength="4"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplNinePhoneOne" runat="server" TargetControlID="txtSuplNinePhoneOne"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxttxtSuplNinePhoneTwo" runat="server" TargetControlID="txtSuplNinePhoneTwo"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplNinePhoneThree" runat="server" TargetControlID="txtSuplNinePhoneThree"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="cusvSuplNinePhone" runat="server" Display="None" ValidationGroup="Supplier"
                                                                                                                        EnableClientScript="false" OnServerValidate="CusvSuplNinePhone_ServerValidate"></asp:CustomValidator>
                                                                                                                    <div id="divSuplNinePhoneTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <div id="divSpeaksEnglishNine" runat="server">
                                                                                                                        Speaks English? &nbsp;
                                                                                                                        <asp:DropDownList ID="ddlSupSpeaksEnglishNine" runat="server" CssClass="ddlbox">
                                                                                                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="spnmore7" runat="server" style="display: none">
                                                                                    <td>
                                                                                        <table cellpadding="1" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="keyheader">
                                                                                                    Supplier/Credit Ten
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="emptytdheight">
                                                                                                    <span>
                                                                                                        <table width="100%" id="Table7" runat="server" cellpadding="2" cellspacing="0" border="0">
                                                                                                            <tr>
                                                                                                              
  <td class="formtdrt" width="35%">
                                                                                                                Company Name
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="35%">
                                                                                                           Company Address
                                                                                                            </td>
                                                                                                            <td class="formtdrt" width="30%">
                                                                                                             Country
                                                                                                            </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtCompanyNameTen" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxlargeCustom" ID="txtCompanyAddressTen" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList  ID="ddlCountryThirteen" runat="server" CssClass="ddlboxCountry"
                                                                                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                              
                                                                                                            </tr>
                                                                                                            <tr>
 <td class="formtdrt">
                                                                                                            City
                                                                                                            </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    State <span class="spnState2" id="trOther13lbl" runat="server" style="display: none">
                                                                                                                        Please Specify State </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    ZIP/Postal Code
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>  <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtSuplCityThirteen" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList ID="ddlStateThirteen" runat="server" CssClass="ddlboxState" AutoPostBack="True"
                                                                                                                        TabIndex="0">
                                                                                                                    </asp:DropDownList>
                                                                                                                    <span id="trOther13" runat="server" style="display: none">
                                                                                                                        <asp:TextBox ID="txtOtherStateThirteen" runat="server" CssClass="txtboxstate" MaxLength="50"></asp:TextBox>
                                                                                                                        <asp:CustomValidator ID="cusvOtherStateThirteen" runat="server" ValidationGroup="Supplier"
                                                                                                                            SetFocusOnError="true" Display="None" OnServerValidate="CusvOtherStateThirteen_ServerValidate"
                                                                                                                            EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateThirteen" TargetControlID="txtOtherStateThirteen"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplZipTen" MaxLength="5" runat="server"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;
                                                                                                                    <asp:RegularExpressionValidator ID="regvSuplZipTen" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under supplier/credit ten"
                                                                                                                        ControlToValidate="txtSuplZipTen" ValidationExpression="^[0-9a-zA-Z -]+$" ValidationGroup="Supplier"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None">
                                                                                                                    </asp:RegularExpressionValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplZipTen" runat="server" TargetControlID="txtSuplZipTen"
                                                                                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt">
                                                                                                                    Contact Name
                                                                                                                </td>
                                                                                                                <td class="formtdrt" >
                                                                                                                    Phone Number
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtdrt" valign="top">
                                                                                                                    <asp:TextBox CssClass="txtbox" ID="txtContactNameTen" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplTenPhoneOne" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplTenPhoneTwo" runat="server" MaxLength="3"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <span id="spnSuplTenPhoneTwo" runat="server">-</span>
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtSuplTenPhoneThree" runat="server" MaxLength="4"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplTenPhoneOne" runat="server" TargetControlID="txtSuplTenPhoneOne"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplTenPhoneTwo" runat="server" TargetControlID="txtSuplTenPhoneTwo"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSuplTenPhoneThree" runat="server" TargetControlID="txtSuplTenPhoneThree"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="cusvSuplTenPhone" runat="server" Display="None" ValidationGroup="Supplier"
                                                                                                                        EnableClientScript="false" OnServerValidate="CusvSuplTenPhone_ServerValidate"></asp:CustomValidator>
                                                                                                                    <div id="divSuplTenPhoneTwo" runat="server" class="phoneLabel" style="display: none">
                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <div id="divSpeaksEnglishTen" runat="server">
                                                                                                                        Speaks English? &nbsp;
                                                                                                                        <asp:DropDownList ID="ddlSupSpeaksEnglishTen" runat="server" CssClass="ddlbox">
                                                                                                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextcenter" align="center">
                                                                                        <asp:LinkButton ID="imgAddMore" runat="server" CssClass="hyplinks">Click to Add More Information</asp:LinkButton>
                                                                                        <asp:HiddenField ID="hdnCount" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </Content>
                                                                    </Ajax:AccordionPane>
                                                                </Panes>
                                                            </Ajax:Accordion>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <asp:HiddenField ID="hdnprjreferenceid" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td class="bodytextleft">
                                                                <asp:ImageButton ID="imbSaveandClose" CausesValidation="false" runat="server" ImageUrl="~/Images/save.jpg"
                                                                    ToolTip="Save" OnClick="imbSaveandClose_Click" />
                                                            </td>
                                                            <td class="bodytextcenter">
                                                            </td>
                                                            <td class="bodytextright">
                                                                <asp:ImageButton ID="imbsubmit" runat="server" ImageUrl="~/Images/save_next.jpg"
                                                                    ToolTip="Save and Next" OnClick="imbsubmit_Click" CausesValidation="false" ValidationGroup="ProjectReferences" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="LblHidden"
                                PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                            </Ajax:ModalPopupExtender>
                            <asp:HiddenField ID="LblHidden" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="modalPopupDiv" runat="server" class="popupdivsmall" Style="display: none;
                                width: 525px; height: auto;">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td class="searchhdrbarbold" runat="server" id="Td3">
                                            <asp:Label ID="Lblheading" runat="server" class="searchhdrbarbold"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl">
                                            <asp:Label ID="LblErrorMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                            <asp:Label ID="LblMsg" runat="server" CssClass="errormsg"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="popupdivcl">
                                            <input type="button" title="Ok" value="OK" id="imbOk" class="ModalPopupButton" onclick="$find('modalExtnd').hide();" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="Footer" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        //(function (i, s, o, g, r, a, m) {
        //    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        //        (i[r].q = i[r].q || []).push(arguments)
        //    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        //    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        //})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        //ga('create', 'UA-41184898-1', 'haskell.com');
        //ga('send', 'pageview');

    </script>

</body>
</html>
<script type="text/javascript">
    function DisplayOthers1() {
        var TextBoxFilter = null;
        TextBoxFilter = $find("<%=ftxtSuplZipNine.ClientID%>");
        var ddl = document.getElementById("<%=ddlStateTwelve.ClientID%>");
        document.getElementById("<%=txtSuplZipNine.ClientID%>").value = "";
        var stateOtherlabel = document.getElementById("<%=trOther12lbl.ClientID%>")
        var ddltext = ddl.options[ddl.selectedIndex].text;
        if (ddltext == "Other") {
            document.getElementById("<%=trOther12.ClientID%>").style.display = "";
            var TXT = document.getElementById("<%=txtOtherStateTwelve.ClientID%>");
            TXT.value = "";
            document.getElementById("<%=regvSuplZipNine.ClientID%>").ValidationGroup = "State";
            document.getElementById("<%=txtSuplZipNine.ClientID%>").maxLength = 10;
            document.getElementById("<%=txtSuplZipNine.ClientID%>").style.width = "80px";
            TXT.focus();
            if (stateOtherlabel != null)
                stateOtherlabel.style.display = "";
            if (TextBoxFilter != null) {
                // TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.Numbers + AjaxControlToolkit.FilterTypes.UppercaseLetters + AjaxControlToolkit.FilterTypes.LowercaseLetters + AjaxControlToolkit.FilterTypes.Custom);
                // TextBoxFilter.set_ValidChars(" ,@");
            }
        }
        else {
            document.getElementById("<%=trOther12.ClientID%>").style.display = "none";
            document.getElementById("<%=regvSuplZipNine.ClientID%>").ValidationGroup = "Supplier";
            document.getElementById("<%=txtSuplZipNine.ClientID%>").maxLength = 5;
            document.getElementById("<%=txtSuplZipNine.ClientID%>").style.width = "40px";
            if (TextBoxFilter != null) {
                // TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.Numbers);
            }
            if (stateOtherlabel != null)
                stateOtherlabel.style.display = "none";
        }
    }
</script>
<script language="javascript" type="text/javascript">

    function DisplayExplain(chkStatus, trexplain, trtxtexplain, txtexplain, projectname, city, state, month, year, otherstate, approximateamount, contractorname, contactname, phone1, phone2, phone3, work, refernce, speakEnglish, country) {
        if (chkStatus == true) {
            document.getElementById(trexplain).style.display = "";
            document.getElementById(trtxtexplain).style.display = "";
            document.getElementById(projectname).value = "";
            document.getElementById(projectname).disabled = true;

            document.getElementById(city).value = "";
            document.getElementById(city).disabled = true;

            document.getElementById(state).selectedIndex = 0;
            document.getElementById(state).disabled = true;

            document.getElementById(country).selectedIndex = 0;
            document.getElementById(country).disabled = true;

            document.getElementById(month).selectedIndex = 0;
            document.getElementById(month).disabled = true;

            document.getElementById(year).selectedIndex = 0;
            document.getElementById(year).disabled = true;

            document.getElementById(otherstate).value = "";
            document.getElementById(otherstate).disabled = true;

            document.getElementById(approximateamount).value = "";
            document.getElementById(approximateamount).disabled = true;

            document.getElementById(contractorname).value = "";
            document.getElementById(contractorname).disabled = true;

            document.getElementById(contactname).value = "";
            document.getElementById(contactname).disabled = true;

            document.getElementById(phone1).value = "";
            document.getElementById(phone1).disabled = true;

            document.getElementById(phone2).value = "";
            document.getElementById(phone2).disabled = true;

            var phone3Control = document.getElementById(phone3);
            if (phone3Control != null) {
                phone3Control.value = "";
                phone3Control.disabled = true;
            }

            document.getElementById(work).value = "";
            document.getElementById(work).disabled = true;
            var refereceControl = document.getElementById(refernce);
            if (refereceControl != null) {
                refereceControl.selectedIndex = 0;
                refereceControl.disabled = true;
            }
            var speakEnglishControl = document.getElementById(speakEnglish);
            if (speakEnglishControl != null) {
                speakEnglishControl.selectedIndex = 0;
                speakEnglishControl.disabled = true;
            }
        }
        else {
            document.getElementById(trexplain).style.display = "none";
            document.getElementById(trtxtexplain).style.display = "none";
            document.getElementById(txtexplain).value = "";
            document.getElementById(projectname).disabled = false;
            document.getElementById(city).disabled = false;
            document.getElementById(state).disabled = false;
            document.getElementById(country).disabled = false;
            document.getElementById(month).disabled = false;
            document.getElementById(year).disabled = false;
            document.getElementById(otherstate).disabled = false;
            document.getElementById(approximateamount).disabled = false;
            document.getElementById(contractorname).disabled = false;
            document.getElementById(contactname).disabled = false;
            document.getElementById(phone1).disabled = false;
            document.getElementById(phone2).disabled = false;
            var phone3Control = document.getElementById(phone3);
            if (phone3Control != null) {
                phone3Control.disabled = false;
            }
            document.getElementById(work).disabled = false;
            var refereceControl = document.getElementById(refernce);
            if (refereceControl != null) {

                refereceControl.disabled = false;
            }

            var speakEnglishControl = document.getElementById(speakEnglish);
            if (speakEnglishControl != null) {

                speakEnglishControl.disabled = false;
            }
        }
    }
    function returnnone() {
        return false;
    }
    function numberFormat(nStr, prefix, ctrl) {
        var txt = document.getElementById(ctrl);
        if (txt.value != "0") {
            var prefix = prefix || '';
            nStr += '';
            nStr = nStr.replace(/^[0]+/g, "");

            y = nStr.split(',');
            var y1 = '';
            for (var i = 0; i < y.length; i++) {
                y1 += y[i];
            }
            if (y.length > 0) {
                nStr = y1;
            }
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1))
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            var txt = document.getElementById(ctrl);
            txt.value = x1 + x2;
        }
    }
    function DisplayOthers(ddlState, trState, txtState, ftxtZip, reqvzip, txtZip) {
        var TextBoxFilter = null;
        if (ftxtZip != "") {
            TextBoxFilter = $find(ftxtZip);
        }
        var TXT = document.getElementById(txtState);
        TXT.value = "";
        var ddl = document.getElementById(ddlState);
        var ddltext = ddl.options[ddl.selectedIndex].text;
        var stateOtherlabel = document.getElementById(trState + "lbl");

        if (ddltext == "Other") {
            document.getElementById(trState).style.display = "";
            if (stateOtherlabel != null)
                stateOtherlabel.style.display = "";
            if (reqvzip != "") {
                document.getElementById(reqvzip).ValidationGroup = "State";
            }
            if (txtZip != "" && txtZip != undefined) {
                document.getElementById(txtZip).value = "";
                document.getElementById(txtZip).maxLength = 10;
                document.getElementById(txtZip).style.width = "80px";
            }
            TXT.focus();
            if (TextBoxFilter != null) {
                //  TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.Numbers + AjaxControlToolkit.FilterTypes.UppercaseLetters + AjaxControlToolkit.FilterTypes.LowercaseLetters + AjaxControlToolkit.FilterTypes.Custom);
                //TextBoxFilter.set_ValidChars(" ,@"); //003-Sooraj commented
            }
        }
        else {
            if (stateOtherlabel != null)
                stateOtherlabel.style.display = "none";
            document.getElementById(trState).style.display = "none";
            if (reqvzip != "") {
                document.getElementById(reqvzip).ValidationGroup = "Supplier";
            }
            if (txtZip != "" && txtZip != undefined) {
                document.getElementById(txtZip).value = "";
                document.getElementById(txtZip).maxLength = 5;
                document.getElementById(txtZip).style.width = "40px";
            }

            if (TextBoxFilter != null) {
                // TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.Numbers); //003-Sooraj commented
            }
            if (ddl != null && ddl.value > 51) {
                if (txtZip != "" && txtZip != undefined) {
                    document.getElementById(txtZip).maxLength = 10;
                    document.getElementById(txtZip).style.width = "80px";
                }
            }
        }
        return false;
    }
    function ShowProject() {

        var lblprj = document.getElementById("<%=lnladdProject.ClientID%>");
        if (document.getElementById("<%=rdoHaskellProjectsCompleted.ClientID%>_0").checked) {
            document.getElementById("<%=tdProject.ClientID%>").style.display = "";
            document.getElementById("<%=trAddProject01.ClientID%>").style.display = "";
            document.getElementById("<%=tdlnkbtnaddproject.ClientID%>").style.display = "";
            lblprj.style.display = "";
            var dl = document.getElementById("<%=hdnaddProjectcount.ClientID%>");
            dl.value = "1";
        }
        else if (document.getElementById("<%=rdoHaskellProjectsCompleted.ClientID%>_1").checked) {
            document.getElementById("<%=trAddProject01.ClientID%>").style.display = "none";
            document.getElementById("<%=trAddProject02.ClientID%>").style.display = "none";
            document.getElementById("<%=trAddProject03.ClientID%>").style.display = "none";
            document.getElementById("<%=trAddProject04.ClientID%>").style.display = "none";
            document.getElementById("<%=trAddProject05.ClientID%>").style.display = "none";
            document.getElementById("<%=trAddProject06.ClientID%>").style.display = "none";
            document.getElementById("<%=trAddProject07.ClientID%>").style.display = "none";
            document.getElementById("<%=trAddProject08.ClientID%>").style.display = "none";
            document.getElementById("<%=trAddProject09.ClientID%>").style.display = "none";
            document.getElementById("<%=trAddProject10.ClientID%>").style.display = "none";
            document.getElementById("<%=tdlnkbtnaddproject.ClientID%>").style.display = "none";
            document.getElementById("<%=hdnaddProjectcount.ClientID%>").value = 0;

            document.getElementById("<%=txtHaskellProjectNameOne.ClientID%>").value = "";
            document.getElementById("<%=txtHaskellProjectManagerOne.ClientID%>").value = "";
            document.getElementById("<%=txtYearOne.ClientID%>").value = "";

            document.getElementById("<%=txtHaskellProjectNameTwo.ClientID%>").value = "";
            document.getElementById("<%=txtHaskellProjectManagerTwo.ClientID%>").value = "";
            document.getElementById("<%=txtYearTwo.ClientID%>").value = "";

            document.getElementById("<%=txtHaskellProjectNameThree.ClientID%>").value = "";
            document.getElementById("<%=txtHaskellProjectManagerThree.ClientID%>").value = "";
            document.getElementById("<%=txtYearThree.ClientID%>").value = "";

            document.getElementById("<%=txtHaskellProjectNameFour.ClientID%>").value = "";
            document.getElementById("<%=txtHaskellProjectManagerFour.ClientID%>").value = "";
            document.getElementById("<%=txtYearFour.ClientID%>").value = "";

            document.getElementById("<%=txtHaskellProjectNameFive.ClientID%>").value = "";
            document.getElementById("<%=txtHaskellProjectManagerFive.ClientID%>").value = "";
            document.getElementById("<%=txtYearFive.ClientID%>").value = "";

            document.getElementById("<%=txtHaskellProjectNameSix.ClientID%>").value = "";
            document.getElementById("<%=txtHaskellProjectManagerSix.ClientID%>").value = "";
            document.getElementById("<%=txtYearSix.ClientID%>").value = "";

            document.getElementById("<%=txtHaskellProjectNameSeven.ClientID%>").value = "";
            document.getElementById("<%=txtHaskellProjectManagerSeven.ClientID%>").value = "";
            document.getElementById("<%=txtYearSeven.ClientID%>").value = "";

            document.getElementById("<%=txtHaskellProjectNameEight.ClientID%>").value = "";
            document.getElementById("<%=txtHaskellProjectManagerEight.ClientID%>").value = "";
            document.getElementById("<%=txtYearEight.ClientID%>").value = "";

            document.getElementById("<%=txtHaskellProjectNameNine.ClientID%>").value = "";
            document.getElementById("<%=txtHaskellProjectManagerNine.ClientID%>").value = "";
            document.getElementById("<%=txtYearNine.ClientID%>").value = "";

            document.getElementById("<%=txtHaskellProjectNameTen.ClientID%>").value = "";
            document.getElementById("<%=txtHaskellProjectManagerTen.ClientID%>").value = "";
            document.getElementById("<%=txtYearTen.ClientID%>").value = "";
            var dl = document.getElementById("<%=hdnaddProjectcount.ClientID%>");
            dl.value = "0";
        }
    }
    function VisibleProject() {
        var prcount = document.getElementById("<%=hdnaddProjectcount.ClientID %>");
        var prcount1 = parseInt(prcount.value) + 1;
        prcount.value = prcount1;
        if (prcount.value == 1) {
            document.getElementById("<%=trAddProject01.ClientID %>").style.display = "";
        }
        else if (prcount.value == 2) {
            document.getElementById("<%=trAddProject02.ClientID %>").style.display = "";
        }
        else if (prcount.value == 3) {
            document.getElementById("<%=trAddProject03.ClientID %>").style.display = "";
        }
        else if (prcount.value == 4) {
            document.getElementById("<%=trAddProject04.ClientID %>").style.display = "";
            document.getElementById("<%=tdlnkbtnaddproject.ClientID%>").style.display = "";
        }
        else if (prcount.value == 5) {
            document.getElementById("<%=trAddProject05.ClientID %>").style.display = "";
        }
        else if (prcount.value == 6) {
            document.getElementById("<%=trAddProject06.ClientID %>").style.display = "";
        }
        else if (prcount.value == 7) {
            document.getElementById("<%=trAddProject07.ClientID %>").style.display = "";
        }
        else if (prcount.value == 8) {
            document.getElementById("<%=trAddProject08.ClientID %>").style.display = "";
        }
        else if (prcount.value == 9) {
            document.getElementById("<%=trAddProject09.ClientID %>").style.display = "";
        }
        else if (prcount.value == 10) {
            document.getElementById("<%=trAddProject10.ClientID %>").style.display = "";
            document.getElementById("<%=lnladdProject.ClientID %>").style.display = "none";
        }
        prcount = prcount1;
        return false;
    }
    function Visibletr() {

        var count = document.getElementById("<%=hdnCount.ClientID %>");
        var count1 = parseInt(count.value) + 1;
        count.value = count1;
        if (count.value == 1)
            document.getElementById("<%=spnmore1.ClientID %>").style.display = "";
        else if (count.value == 2)
            document.getElementById("<%=spnmore2.ClientID %>").style.display = "";
        else if (count.value == 3)
            document.getElementById("<%=spnmore3.ClientID %>").style.display = "";
        else if (count.value == 4)
            document.getElementById("<%=spnmore4.ClientID %>").style.display = "";
        else if (count.value == 5)
            document.getElementById("<%=spnmore5.ClientID %>").style.display = "";
        else if (count.value == 6)
            document.getElementById("<%=spnmore6.ClientID %>").style.display = "";
        else if (count.value == 7) {
            document.getElementById("<%=spnmore7.ClientID %>").style.display = "";
            document.getElementById("<%=imgAddMore.ClientID %>").style.display = "none";
        }
        count.value = count1;
        return false;
    }
    function VisibleReferencestr() {

        var count = document.getElementById("<%=hdnAdditionalReferencesCount.ClientID %>");
        var count1 = parseInt(count.value) + 1;
        count.value = count1;
        if (count.value == 4)
            document.getElementById("<%=trReferencesFour.ClientID %>").style.display = "";
        else if (count.value == 5)
            document.getElementById("<%=trReferencesFive.ClientID %>").style.display = "";
        else if (count.value == 6)
            document.getElementById("<%=trReferencesSix.ClientID %>").style.display = "";
        else if (count.value == 7)
            document.getElementById("<%=trReferencesSeven.ClientID %>").style.display = "";
        else if (count.value == 8)
            document.getElementById("<%=trReferencesEight.ClientID %>").style.display = "";
        else if (count.value == 9)
            document.getElementById("<%=trReferencesNine.ClientID %>").style.display = "";
        else if (count.value == 10) {
            document.getElementById("<%=trReferencesTen.ClientID %>").style.display = "";
            document.getElementById("<%=lnkAddMoreReference.ClientID %>").style.display = "none";
        }
        count.value = count1;
        return false;
    }
    function validateNumber(TxtBox1) {
        var str = document.getElementById(TxtBox1).value;
        var str1 = "0123456789.";
        var i = 0;
        for (i = 0; i < str.length; i++) {
            if (str1.indexOf(str.substring(i, i + 1)) < 0 || str1.indexOf(str.substring(i, i + 1)) > str1.length || str.indexOf(".") != str.lastIndexOf(".")) {
                document.getElementById(TxtBox1).value = str.substring(0, str.length - 1);
                document.getElementById(TxtBox1).focus();
                break;
            }
        }
    }
    function checkDecimal(TxtBox1) {
        var str2 = document.getElementById(TxtBox1).value;
        var str3;
        if (document.getElementById(TxtBox1).value > 0 && document.getElementById(TxtBox1).value != "" && document.getElementById(TxtBox1).value != null) {
            //alert(document.getElementById(TxtBox1).value);
            if (str2.indexOf('.') < 0) {
                str3 = str2 + ".00";
                document.getElementById(TxtBox1).value = roundNumber(str3, 2);
            }
            else {
                document.getElementById(TxtBox1).value = roundNumber(str2, 2);
            }
        }
    }
    function phoneFiledFocus(prev, next) {
        var prevControl = prev;
        if (prevControl.value.length >= 3) {
            next.focus();
        }
    }
    function Close() {
        var x = $find("ModalAddDoc");
        if (x) {
            x.hide();
        }
    }
    function popup() {
        var width = 600;
        var height = 620;
        var left = (screen.width - width) / 2;
        var top = (screen.height - height) / 2;
        var params = 'width=' + width + ', height=' + height;
        params += ', top=' + top + ', left=' + left;
        params += ', directories=no';
        params += ', location=no';
        params += ', menubar=no';
        params += ', resizable=yes';
        params += ', scrollbars=yes';
        params += ', status=yes';
        params += ', toolbar=no';
        newwin = window.open('../VQFView/ReferencesView.aspx?Print=1', 'windowname5', params);
        if (window.focus) { newwin.focus() }
        return false;
    }
</script>
