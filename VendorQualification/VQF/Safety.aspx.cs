﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using VMSDAL;

#region Change Comments

/*******************************************************************************************************************************
 * 001 - G. Vera 05/18/2012:  Find a way to handle the "Save" and "Save and Next" event to not lock the EMR text boxes until
 * after "Submit" is trigerred by user.
 * 002 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 003 G. Vera 01/29/2013: Hot Fix 1.3.1 (JIRA:VPI-90)
 * 004 Sooraj Sudhakaran.T 09/22/2013: VMS Modification - Added code to hide area for design consultant 
 * 005 Sooraj Sudhakaran.T 10/24/2013: VMS Modification - Added new question for Non US vendors 
 * 006 Sooraj Sudhakaran.T 12/12/2013: VMS Modification - Added code to handle International contact phone number 
 * 007 G. Vera 06/09/2014: Adding Year 4 to OSHA if user clicks on "N/A" for the first OSHA year
 * 008 G. Vera 06/12/2014: Add OSHA and EMR attachments (required)
 * 009 G. Vera 06/302014:  Validate section status upon Save click to get the correct status
 * 010 G. Vera 07/01/2014: Allow user to modify the Year 1 EMR rate at all times but send audit email to Admin when changes have been
 *                         after initial submission.
 * 011 G. Vera 07/09/2014: Last Modifed date change upon save
 * 012 G. Vera 12/02/2014: Correcting how the audit email works. Need to send email only when user is editing year 1 after submission.
 * 013 G. Vera 12/14/2016: Add new questions to Questionnaire section
 * 014 G. Vera 03/01/2017:  Change safety emr and osha new year from 05/01 to 03/01
 * 015 G. Vera 03/01/2018:  Change safety emr and osha new year from 03/01 to 01/01
 *******************************************************************************************************************************/

#endregion

public partial class VQF_Safety : CommonPage
{

    #region Instantiate the class and declare varaibles
    bool? CompanySafety = null;
    bool? SafetyRequirements = null;
    bool? SafetyProgram = null;
    bool? SafetyRepresentation = null;//005
    byte SafetyStatus;
    Common objCommon = new Common();
    clsSafety objSafety = new clsSafety();
    clsCompany objCompany = new clsCompany();
    long VendorId, SafetyId;

    public DataSet Vendordetails = new DataSet();
    public int CountSafety;
    bool Result;
    private static string year1EMRRateOLD = string.Empty;           //010
    private static string year1EMRExplainOLD = string.Empty;        //010   
    
    private VQFSafetyCitation citation1
    {
        get { return ViewState["citation1"] != null ? (ViewState["citation1"] as VQFSafetyCitation) : null ; }
        set { ViewState["citation1"] = value; }
    } //013
    private VQFSafetyCitation citation2
    {
        get { return ViewState["citation2"] != null ? (ViewState["citation2"] as VQFSafetyCitation) : null; }
        set { ViewState["citation2"] = value; }
    } //013
    private VQFSafetyCitation citation3
    {
        get { return ViewState["citation3"] != null ? (ViewState["citation3"] as VQFSafetyCitation) : null; }
        set { ViewState["citation3"] = value; }
    } //013
    private string CitationYear1
    {
        get { return ViewState["CitationYear1"] != null ? ViewState["CitationYear1"].ToString() : string.Empty; }
        set { ViewState["CitationYear1"] = value; }
    } //013
    private string CitationYear2
    {
        get { return ViewState["CitationYear2"] != null ? ViewState["CitationYear2"].ToString() : string.Empty; }
        set { ViewState["CitationYear2"] = value; }
    } //013
    private string CitationYear3
    {
        get { return ViewState["CitationYear3"] != null ? ViewState["CitationYear3"].ToString() : string.Empty; }
        set { ViewState["CitationYear3"] = value; }
    } //013

    #endregion

    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {


        
        //Check for vendor id if null, redirect to home page.
        if (string.IsNullOrEmpty(Convert.ToString(Session["VendorId"])) || Convert.ToString(Session["VendorId"]) == "0")
        {
            Response.Redirect("~/Common/Home.aspx?timeout=1");  //002

        }
        //002
        else if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Common/Home.aspx?timeout=1");
        }
        else
        {
            if (!Page.IsPostBack)
            {
                ViewState["EditStatus"] = 0;        //010 - moved here page load only
                //002
                Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

                VendorId = Convert.ToInt64(Session["VendorId"]);
                /* Changed since client asked to change from current Year 
                 txtYearOne.Text= Convert.ToString(DateTime.Now.Year - 1);
                 txtYearTwo.Text= Convert.ToString(DateTime.Now.Year - 2);
                 txtYearThree.Text= Convert.ToString(DateTime.Now.Year - 3);
                 txtOSHAYearOne.Text= Convert.ToString(DateTime.Now.Year - 1);
                 txtOSHAYearTwo.Text= Convert.ToString(DateTime.Now.Year - 2);
                 txtOSHAYearThree.Text= Convert.ToString(DateTime.Now.Year - 3);*/


                // G. Vera Phase 3 Modifications Item 1.3
                //DateTime today = DateTime.Now.AddMonths(-1);
                DateTime today = DateTime.Now;
                Session["CanEditEMR"] = false;



                //Included the above line for task no : 20
                //014
                if (today >= Convert.ToDateTime("01/01/" + Convert.ToString(DateTime.Now.Year)))
                {
                    txtYearOne.Text = Convert.ToString(DateTime.Now.Year - 0);
                    txtYearTwo.Text = Convert.ToString(DateTime.Now.Year - 1);
                    txtYearThree.Text = Convert.ToString(DateTime.Now.Year - 2);
                    txtYearFour.Text = Convert.ToString(DateTime.Now.Year - 3);         //003

                    // G. Vera Phase 3 Modifications Item 1.3
                    txtOSHAYearOne.Text = Convert.ToString(DateTime.Now.Year - 1);      
                    txtOSHAYearTwo.Text = Convert.ToString(DateTime.Now.Year - 2);      
                    txtOSHAYearThree.Text = Convert.ToString(DateTime.Now.Year - 3);    
                    txtOSHAYearFour.Text = Convert.ToString(DateTime.Now.Year - 4);     //007

                }
                else
                {
                    txtYearOne.Text = Convert.ToString(DateTime.Now.Year - 1);
                    txtYearTwo.Text = Convert.ToString(DateTime.Now.Year - 2);
                    txtYearThree.Text = Convert.ToString(DateTime.Now.Year - 3);
                    txtYearFour.Text = Convert.ToString(DateTime.Now.Year - 4);         //003

                    // G. Vera Phase 3 Modifications Item 1.3
                    txtOSHAYearOne.Text = Convert.ToString(DateTime.Now.Year - 2);      
                    txtOSHAYearTwo.Text = Convert.ToString(DateTime.Now.Year - 3);      
                    txtOSHAYearThree.Text = Convert.ToString(DateTime.Now.Year - 4);    
                    txtOSHAYearFour.Text = Convert.ToString(DateTime.Now.Year - 5);     //007
                }
                //VendorId = 2; 
                Vendordetails = objCommon.GetVendorDetails(Convert.ToInt64(VendorId));
                
                //012 - added
                string date = Vendordetails.Tables[0].Rows[0]["SubmittedDate"].ToString();
                string year = (!string.IsNullOrEmpty(date)) ? date.Substring(date.ToString().LastIndexOf('/') + 1, 4) : string.Empty;
                ViewState["SubmittedYear"] = year;

                //013 - START
                CitationYear1 = DateTime.Now.Year.ConvertToString();
                CitationYear2 = (DateTime.Now.Year-1).ConvertToString();
                CitationYear3 = (DateTime.Now.Year-2).ConvertToString();
                //013 - END


                //G. Vera - According to Dianne, this question was supposed to apply only to all Onsite Support vendors
                SwitchQuestion((Vendordetails.Tables[0].Rows[0]["TypeOfCompany"].ConvertToString() == TypeOfCompany.EquipAndOnsiteSupport), txtRepresentationContactNumberOne, txtRepresentationContactNumberTwo, txtRepresentationContactNumberThree);//005

                //Show or hide design consultant view based on Type of company - sooraj //004
                SwitchDesignConsultantView(Vendordetails.Tables[0].Rows[0]["TypeOfCompany"].ConvertToString() == TypeOfCompany.DesignConsultant);

                lblwelcome.Text = Vendordetails.Tables[0].Rows[0]["Company"].ToString();
                CountSafety = objSafety.GetSafetyStatusCount(Convert.ToInt64(Session["VendorId"]));

                CallScripts();
                // BindRequiredFieldValidators();
                ShowHideControls();
                //LoadDropdowns();
                HandleStatus(); //008


                LoadEditDatas(true);
            }

        }

        

        
        SwitchPhoneForKeyContact(chkContactUSA);//006
        SwitchPhoneForKeyContact(chkReperesentUSA);//006

        ControlVisiblity();
        uclAttachEMR1.CloseEvent += uclAttachEMR1_CloseEvent;   //008
        uclAttachEMR1.IsUseSafetySection = true;    //014

        //EmptyControlVisiblity();
    }

    private void HandleStatus()
    {
        VQFMenu.GetVendorStatus();
        hdnVendorStatus.Value = Convert.ToString(VQFMenu.VendorStatus);

        if (VQFMenu.VendorStatus == 1 || VQFMenu.VendorStatus == 2)
        {
            apnRates.Enabled = false;
            apnOSHA.Enabled = false;
            apnQuestionnaire.Enabled = false;
            imbSaveandClose.Visible = false;
            imbSaveandNext.Visible = false;
            ImageButton imgAttach = (ImageButton)VQFMenu.FindControl("imbAttch");
            imgAttach.Visible = true;
        }

        // G. Vera Phase 3 Modifications Item 1.3
        if (VQFMenu.VendorStatus == 0 && VQFMenu.VendorStatus == 3) Session["CanEditEMR"] = true;
        else Session["CanEditEMR"] = false;

        VQFMenu.GetCompleteStatus();    //008
        VQFMenu.GetIncompleteStatus();  //008

    }

    #endregion

    #region Other Methods

    /// <summary>
    /// 010 - implemented
    /// </summary>
    /// <param name="maxContract"></param>
    /// <param name="revenue"></param>
    /// <param name="explain"></param>
    /// <param name="vendorId"></param>
    private void SendYear1EMRChangesToAdmin(string rate, string explain, long vendorId)
    {
        string body = string.Empty;
        var vendor = new clsRegistration().GetVendorDetails(vendorId);
        bool isSendEmail = false;

        body += "<html style=\"font-family: Calibri; font-size: 1.1rem\"><body>";
        body += "<h4>Current Year (" + txtYearOne.Text + ") EMR data has changed for:<br/>" + vendor.Company + "</h4><br/>";

        if (explain != year1EMRExplainOLD)
        {
            body += "<h4><u>Vendor has provided a different explanation of why the data is not available:</u></h4>";
            body += "<p>Previous explanation: " + year1EMRExplainOLD + "</p>";
            body += "<p>New explanation: " + explain + "</p>";
            isSendEmail = true;
        }
        if (rate != year1EMRRateOLD)
        {
            body += "<h4><u>EMR Rate value has changed:</u></h4>";
            body += "<p>Previous value: " + year1EMRRateOLD + "</p>";
            body += "<p>New value: " + rate + "</p>";
            isSendEmail = true;
        }

        body += "</html></body>";

        if (isSendEmail)
        {
            //send email using Haskell email class
            HaskellWebMail webMail = new HaskellWebMail();
            string emailTo = ConfigurationManager.AppSettings["adminemail"];
            webMail.SendMail(body, true, "Safety EMR Data Change by Vendor (" + vendor.Company + ")", "VMS.Audit@haskell.com", emailTo.Split());
        }

    }

    /// <summary>
    /// 006-Switch phone for Key Contacts 
    /// </summary>
    /// <param name="chk"></param>
    private void SwitchPhoneForKeyContact(CheckBox chk)
    {
        if (chk != null)
        {
            switch (chk.ID)
            {
                case "chkContactUSA":
                    SwitchPhone(chkContactUSA, txtMainPhno1, txtMainPhno2, txtMainPhno3, spnCPhone2);
                    break;
                case "chkReperesentUSA":
                    SwitchPhone(chkReperesentUSA, txtRepresentationContactNumberOne, txtRepresentationContactNumberTwo, txtRepresentationContactNumberThree, spnRepresentPhone);
                    break;
            }
        }



    }

    /// <summary>
    /// 006
    /// </summary>
    /// <param name="val"></param>
    /// <param name="phone1"></param>
    /// <param name="phone2"></param>
    /// <param name="phone3"></param>
    /// <param name="spnPhone"></param>
    private void SwitchPhone(CheckBox val, TextBox phone1, TextBox phone2, TextBox phone3, HtmlControl spnPhone)
    {

        if (val.Checked)//Non-US
        {
            val.Text = "Non-US";
            phone3.Style["display"] = "none";

            phone1.MaxLength = 5;
            phone2.MaxLength = 15;
            phone2.CssClass = "txtbox";
            phone3.Text = "";
            spnPhone.Style["display"] = "none";
            phone1.Attributes.Remove("onkeyup");
            phone2.Attributes.Remove("onkeyup");
        }
        else  //US
        {
            phone1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + phone1.ClientID + "," + phone2.ClientID + ")");
            phone2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + phone2.ClientID + "," + phone3.ClientID + ")");
            // val.Text = "US";
            phone3.Style["display"] = "";
            phone2.CssClass = "txtboxsmall";
            phone1.MaxLength = 3;
            phone2.MaxLength = 3;

            spnPhone.Style["display"] = "";

        }

    }


    /// <summary>
    /// 007-Non US Question changes
    /// </summary>
    ///  <param name="taxtID"></param>
    /// <param name="phone1"></param>
    /// <param name="phone2"></param>
    /// <param name="phone3"></param>

    private void SwitchQuestion(bool IsOnsiteSupport, TextBox phone1, TextBox phone2, TextBox phone3)
    {

   
        //G. vera 05/16/2014 changed: 
        if (IsOnsiteSupport)
        {
            trNonUSSafetyQuestion.Style["display"] = "";

            phone3.Style["display"] = "none";
            phone1.MaxLength = 5;
            phone2.MaxLength = 15;
            phone2.CssClass = "txtbox";

           
        }
        else
        {
            trNonUSSafetyQuestion.Style["display"] = "none";
        }

    }

    /// <summary>
    /// 004- Switch view based on logged in user
    /// </summary>
    /// <param name="IsShow"></param>
    /// <Author>Sooraj Sudhakaran.T</Author>
    ///  <Date>20-SEP-2013</Date>
    private void SwitchDesignConsultantView(bool IsShow)
    {
        //Design Consultant 
        accSafety.Visible = !IsShow;
        imbSaveandClose.Visible = !IsShow;
    }
    /// <summary>
    /// visible controls in postback
    /// </summary>
    public void EmptyControlVisiblity()
    {
        int CountSafety = objSafety.GetSafetyCount(Convert.ToInt64(Session["VendorId"]));
        if (CountSafety > 0)
        {
            DAL.VQFSafety EditData = objSafety.GetSingleVQLSafetyData(Convert.ToInt64(Session["VendorId"]));
            SafetyId = EditData.PK_SafetyID;

            if (Convert.ToInt32(ViewState["EditStatus"]) == 0)
            {
                if ((txtNoofCasesFrmWorkTwo.Text.Length == 0 || txtNoofCasesTransferTwo.Text.Length == 0 || txtNoofDeathsYearTwo.Text.Length == 0 || txtRecordableCasesTwo.Text.Length == 0 || txtTotalHours2.Text.Length == 0) && (txtExplain5.Text.Length == 0))
                {
                    txtOSHAYearTwo.ReadOnly = true; //008 was set to false
                    txtNoofCasesFrmWorkTwo.Enabled = true;
                    txtNoofCasesTransferTwo.Enabled = true;
                    txtNoofDeathsYearTwo.Enabled = true;
                    txtRecordableCasesTwo.Enabled = true;
                    chkNA5.Enabled = true;
                    txtExplain5.Enabled = true;
                    txtTotalHours2.Enabled = true;
                    VMSDAL.VQFSafetyOSHA objOshaYearTwo = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearTwo.Text.Trim());
                    if (objOshaYearTwo != null)
                    {
                        objSafety.UpdateSafetyOSHABit(SafetyId, 1, txtOSHAYearTwo.Text.Trim());
                    }

                }

                if ((txtNoofCasesFrmWorkThree.Text.Length == 0 || txtNoofCasesTransferThree.Text.Length == 0 || txtNoofDeathsYearThree.Text.Length == 0 || txtRecordableCasesThree.Text.Length == 0 || txtTotalHours3.Text.Length == 0) && (txtExplain6.Text.Length == 0))
                {

                    txtOSHAYearThree.ReadOnly = true;   //008 was set to false
                    txtNoofCasesFrmWorkThree.Enabled = true;
                    txtNoofCasesTransferThree.Enabled = true;
                    txtNoofDeathsYearThree.Enabled = true;
                    txtRecordableCasesThree.Enabled = true;
                    chkNA6.Enabled = true;
                    txtExplain6.Enabled = true;
                    txtTotalHours3.Enabled = true;
                    VMSDAL.VQFSafetyOSHA objOshaYearThree = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearThree.Text.Trim());
                    if (objOshaYearThree != null)
                    {
                        objSafety.UpdateSafetyOSHABit(SafetyId, 1, txtOSHAYearThree.Text.Trim());
                    }
                }

                //007
                if ((txtNoofCasesFrmWorkFour.Text.Length == 0 || txtNoofCasesTransferFour.Text.Length == 0 || txtNoofDeathsYearFour.Text.Length == 0 || txtRecordableCasesFour.Text.Length == 0 || txtTotalHours4.Text.Length == 0) && (txtExplain7.Text.Length == 0))
                {

                    txtOSHAYearFour.ReadOnly = false;   //008 was set to false
                    txtNoofCasesFrmWorkFour.Enabled = true;
                    txtNoofCasesTransferFour.Enabled = true;
                    txtNoofDeathsYearFour.Enabled = true;
                    txtRecordableCasesFour.Enabled = true;
                    chkNA6.Enabled = true;
                    txtExplain6.Enabled = true;
                    txtTotalHours3.Enabled = true;
                    VMSDAL.VQFSafetyOSHA objOshaYearFour = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearFour.Text.Trim());
                    if (objOshaYearFour != null)
                    {
                        objSafety.UpdateSafetyOSHABit(SafetyId, 1, txtOSHAYearFour.Text.Trim());
                    }
                }

                if ((txtRateTwo.Text.Length == 0) && (txtExplanation2.Text.Length == 0))
                {
                    txtYearTwo.ReadOnly = true;     //008 was set to false
                    chkNA2.Enabled = true;
                    txtRateTwo.Enabled = true;
                    txtExplanation2.Enabled = true;
                    VMSDAL.VQFSafetyEMRRate objRatesYearTwo = objSafety.SelectSafetyEMRRates(SafetyId, txtYearTwo.Text.Trim());
                    if (objRatesYearTwo != null)
                    {
                        objSafety.UpdateSafetyEMRRateBit(SafetyId, 1, txtYearTwo.Text.Trim());
                    }
                }

                if ((txtRateThree.Text.Length == 0) && (txtExplanation3.Text.Length == 0))
                {

                    txtYearThree.ReadOnly = true;     //008 was set to false
                    chkNA3.Enabled = true;
                    txtRateThree.Enabled = true;
                    txtExplanation3.Enabled = true;
                    VMSDAL.VQFSafetyEMRRate objRatesYearThree = objSafety.SelectSafetyEMRRates(SafetyId, txtYearThree.Text.Trim());
                    if (objRatesYearThree != null)
                    {
                        objSafety.UpdateSafetyEMRRateBit(SafetyId, 1, txtYearThree.Text.Trim());
                    }
                }

                //003
                if ((txtRateFour.Text.Length == 0) && (txtExplanation4.Text.Length == 0))
                {

                    txtYearFour.ReadOnly = true;     //008 was set to false
                    chkEMRNA4.Enabled = true;
                    txtRateFour.Enabled = true;
                    txtExplanation4.Enabled = true;
                    VMSDAL.VQFSafetyEMRRate objRatesYearFour = objSafety.SelectSafetyEMRRates(SafetyId, txtYearFour.Text.Trim());
                    if (objRatesYearFour != null)
                    {
                        objSafety.UpdateSafetyEMRRateBit(SafetyId, 1, txtYearFour.Text.Trim());
                    }
                }
            }

        }
    }

    /// <summary>
    /// Load data to edit
    /// </summary>
    /// <param name="IsMyProfile"></param>
    private void LoadEditDatas(bool IsMyProfile)
    {
        VendorId = Convert.ToInt64(Session["VendorId"]);
        //VendorId = 2;
        int CountSafety = objSafety.GetSafetyCount(Convert.ToInt64(Session["VendorId"]));
        if (CountSafety > 0)
        {
            VMSDAL.VQFSafety EditData = objSafety.GetSingleVQFSafetyData(VendorId); //013 - changed to use the VMSDAL instead

            hdnSafetyId.Value = Convert.ToString(EditData.PK_SafetyID);
            ViewState["EditStatus"] = EditData.EditStatus;
            txtSIC.Text = EditData.SICNAICSCode;
            SafetyId = EditData.PK_SafetyID;

            //013 - START
            txtProLevelOfTraining.Text = EditData.ProfessionalLevelOfTraining;
            if ((EditData.SafetyRequirements != null)) rdoSafetytraining.SelectedIndex = (((bool)EditData.SafetyRequirements) ? 0 : 1);
            if ((EditData.IsProvideEmployeeTrainingDocumentation != null)) rdoListEmployeeProgramDocumentation.SelectedIndex = (((bool)EditData.IsProvideEmployeeTrainingDocumentation) ? 0 : 1);
            if ((EditData.IsWrittenDrugAlcoholProgram != null)) rdoListDrugAlcohol.SelectedIndex = (((bool)EditData.IsWrittenDrugAlcoholProgram) ? 0 : 1);
            if ((EditData.IsPeriodicSafetyMeetings != null)) rdoListSafetyMeetings.SelectedIndex = (((bool)EditData.IsPeriodicSafetyMeetings) ? 0 : 1);
            if(EditData.SafetyMeetingsPeriods != null) chkSafetyMeetings.Items.Cast<ListItem>().ToList().ForEach(si => si.Selected = (EditData.SafetyMeetingsPeriods.Split('|').Contains(si.Text)));
            if (EditData.IsSafetyInspectionOfWIP != null) rdoListInspectionWIP.SelectedIndex = (((bool)EditData.IsSafetyInspectionOfWIP) ? 0 : 1);
            if (EditData.InspectionOfWIPPeriods != null) chkInspectionWIP.Items.Cast<ListItem>().ToList().ForEach(si => si.Selected = (EditData.InspectionOfWIPPeriods.Split('|').Contains(si.Text)));
            txtWhoInspectionWIP.Text = EditData.WhoConductsInspectionOfWIP;
            if (EditData.IsConductHazardAnalysis != null) rdoHazardAnalysis.SelectedIndex = (((bool)EditData.IsConductHazardAnalysis) ? 0 : 1);
            if (EditData.ConductHazardAnalysisPeriods != null) chkHazardAnalysisPeriods.Items.Cast<ListItem>().ToList().ForEach(si => si.Selected = (EditData.ConductHazardAnalysisPeriods.Split('|').Contains(si.Text)));
            

            #region Citations

            lblCitationYear1.Text = "Any citations in " + CitationYear1 + "?";
            citation1 = new clsSafety().SelectSafetyCitation(EditData.PK_SafetyID, CitationYear1);
            if (citation1.IsCitation != null) rdoCitationYear1.SelectedIndex = ((bool)citation1.IsCitation) ? 0 : 1;
            hdnCitationFile1.Value = citation1.CitationFilePath;
            lnkAttachCITATION1.Text = (!string.IsNullOrEmpty(citation1.CitationFilePath)) ?
                                     "(Edit " + citation1.CitationFilePath + " )" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick1.Attributes["style"] = (rdoCitationYear1.SelectedIndex == 0) ? "display: " : "display: none";

            lblCitationYear2.Text = "Any citations in " + CitationYear2 + "?";
            citation2 = new clsSafety().SelectSafetyCitation(EditData.PK_SafetyID, CitationYear2);
            if (citation2.IsCitation != null) rdoCitationYear2.SelectedIndex = ((bool)citation2.IsCitation) ? 0 : 1;
            hdnCitationFile2.Value = citation2.CitationFilePath;
            lnkAttachCITATION2.Text = (!string.IsNullOrEmpty(citation2.CitationFilePath)) ?
                                     lnkAttachCITATION2.Text = "(Edit " + citation2.CitationFilePath + " )" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick2.Attributes["style"] = (rdoCitationYear2.SelectedIndex == 0) ? "display: " : "display: none";

            lblCitationYear3.Text = "Any citations in " + CitationYear3 + "?";
            citation3 = new clsSafety().SelectSafetyCitation(EditData.PK_SafetyID, CitationYear3);
            if (citation3.IsCitation != null) rdoCitationYear3.SelectedIndex = ((bool)citation3.IsCitation) ? 0 : 1;
            hdnCitationFile3.Value = citation3.CitationFilePath;
            lnkAttachCITATION3.Text = (!string.IsNullOrEmpty(citation3.CitationFilePath)) ?
                                     lnkAttachCITATION3.Text = "(Edit " + citation3.CitationFilePath + " )" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick3.Attributes["style"] = (rdoCitationYear3.SelectedIndex == 0) ? "display: " : "display: none";

            #endregion

            //013 - END

            #region EMR Rate 1
            VMSDAL.VQFSafetyEMRRate objRates = objSafety.SelectSafetyEMRRates(SafetyId, txtYearOne.Text.Trim());
            if (objRates != null)
            {
                txtRateOne.Text = objRates.EMRRate;
                chkNA1.Checked = Convert.ToBoolean(objRates.NotAvailable);
                txtExplanation1.Text = objRates.Explanation;
                year1EMRExplainOLD = objRates.Explanation;      //010
                year1EMRRateOLD = objRates.EMRRate;             //010

                //001 - Commented this out as visibility is being handled through the 
                //      ControlVisibility() method
                // G. Vera Phase 3 Modifications Item 1.3
                //if (EditData.EditStatus == 1 || EditData.EditStatus == null)
                //{
                //    if (txtRateOne.Text.Length == 0 || 
                //        txtExplanation1.Text != "" ||
                //        double.Parse(txtRateOne.Text) == 0)
                //    {
                //        chkNA1.Enabled = true;
                //        txtRateOne.Enabled = true;
                //        txtExplanation1.Enabled = true;
                //    }
                //    else
                //    {
                //        chkNA1.Enabled = false;
                //        txtRateOne.Enabled = false;
                //        txtExplanation1.Enabled = false;
                //    }
                //}
                //else
                //{
                //    chkNA1.Enabled = false;
                //    txtRateOne.Enabled = false;
                //    txtExplanation1.Enabled = false;
                //}

                // G. Vera Phase 3 Modifications Item 1.3 Why is it checking for status > 0 here, is this rule valid?
                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (EditData.EditStatus == 1))
                //{
                //    if (txtRateOne.Text == "0.00" || txtExplanation1.Text != "")
                //    {
                //        chkNA1.Enabled = true;
                //        txtRateOne.Enabled = true;
                //        txtExplanation1.Enabled = true;
                //    }
                //    else
                //    {
                //        chkNA1.Enabled = false;
                //        txtRateOne.Enabled = false;
                //        txtExplanation1.Enabled = false;
                //    }
                //}
            }
            //else
            //{
            //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) )
            //{
            //chkNA1.Enabled = true;
            //txtRateOne.Enabled = true;
            //txtExplanation1.Enabled = true;
            //}
            //else
            //{
            //    txtRateOne.Text = string.Empty;
            //}
            //}
            // }
            #endregion

            #region EMR Rate 2
            VMSDAL.VQFSafetyEMRRate objRates1 = objSafety.SelectSafetyEMRRates(SafetyId, txtYearTwo.Text.Trim());
            if (objRates1 != null)
            {
                txtRateTwo.Text = objRates1.EMRRate;
                chkNA2.Checked = Convert.ToBoolean(objRates1.NotAvailable);
                txtExplanation2.Text = objRates1.Explanation;

                // G. Vera Phase 3 Modifications Item 1.3
                //if (EditData.EditStatus == 1 || EditData.EditStatus == null)
                //{
                //    if (!IsPostBack)    // 001 Skip check and leave as it was on post backs
                //    {
                //        if (txtRateTwo.Text.Length == 0 ||
                //            txtExplanation2.Text != "" ||
                //            double.Parse(txtRateTwo.Text) == 0)
                //        {
                //            txtRateTwo.Enabled = true;
                //            chkNA2.Enabled = true;
                //            txtExplanation2.Enabled = true;
                //        }
                //        else
                //        {
                //            txtRateTwo.Enabled = false;
                //            chkNA2.Enabled = false;
                //            txtExplanation2.Enabled = false;
                //        }
                //    }
                //}
                //else
                //{
                //    txtRateTwo.Enabled = false;
                //    chkNA2.Enabled = false;
                //    txtExplanation2.Enabled = false;
                //}

                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (EditData.EditStatus == 1))
                //{
                //    // G. Vera Phase 3 Modifications Item 1.3
                //    if (txtRateTwo.Text == "0.00" || txtExplanation2.Text != "")
                //    {
                //        txtRateTwo.Enabled = true;
                //        chkNA2.Enabled = true;
                //        txtExplanation2.Enabled = true;
                //    }
                //    else
                //    {
                //        txtRateTwo.Enabled = false;
                //        chkNA2.Enabled = false;
                //        txtExplanation2.Enabled = false;
                //    }

                //}

            }
            //else
            //{
            //    //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0))
            //    //{
            //    txtRateTwo.Enabled = true;
            //    chkNA2.Enabled = true;
            //    txtExplanation2.Enabled = true;

            //    //}
            //    //else
            //    //{
            //    //    txtRateTwo.Text = string.Empty;
            //    //}
            //}
            #endregion

            #region EMR Rate 3
            VMSDAL.VQFSafetyEMRRate objRates2 = objSafety.SelectSafetyEMRRates(SafetyId, txtYearThree.Text.Trim());
            if (objRates2 != null)
            {
                txtRateThree.Text = objRates2.EMRRate;
                chkNA3.Checked = Convert.ToBoolean(objRates2.NotAvailable);
                txtExplanation3.Text = objRates2.Explanation;

                // G. Vera Phase 3 Modifications Item 1.3
                //if (EditData.EditStatus == 1 || EditData.EditStatus == null)
                //{
                //    if (!IsPostBack)    // 001 Skip check and leave as it was on post backs
                //    {
                //        if (txtRateThree.Text.Length == 0 || txtExplanation3.Text != "" || double.Parse(txtRateThree.Text) == 0)
                //        {
                //            chkNA3.Enabled = true;
                //            txtRateThree.Enabled = true;
                //            txtExplanation3.Enabled = true;
                //        }
                //        else
                //        {
                //            chkNA3.Enabled = false;
                //            txtRateThree.Enabled = false;
                //            txtExplanation3.Enabled = false;
                //        }
                //    }
                //}
                //else
                //{
                //    chkNA3.Enabled = false;
                //    txtRateThree.Enabled = false;
                //    txtExplanation3.Enabled = false;
                //}

                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (EditData.EditStatus == 1))
                //{
                //    // G. Vera Phase 3 Modifications Item 1.3
                //    if (txtRateThree.Text == "0.00" || txtExplanation3.Text != "")
                //    {
                //        chkNA3.Enabled = true;
                //        txtRateThree.Enabled = true;
                //        txtExplanation3.Enabled = true;
                //    }
                //    else
                //    {
                //        chkNA3.Enabled = false;
                //        txtRateThree.Enabled = false;
                //        txtExplanation3.Enabled = false;
                //    }

                //}
            }
            //else
            //{
            //    //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0))
            //    //{
            //    chkNA3.Enabled = true;
            //    txtRateThree.Enabled = true;
            //    txtExplanation3.Enabled = true;
            //    //}
            //    //else
            //    //{
            //    //    txtRateThree.Text = string.Empty;
            //    //}
            //}
            #endregion

            //003
            #region EMR Rate 4
            VMSDAL.VQFSafetyEMRRate objRates4 = objSafety.SelectSafetyEMRRates(SafetyId, txtYearFour.Text.Trim());
            if (objRates4 != null)
            {
                txtRateFour.Text = objRates4.EMRRate;
                chkEMRNA4.Checked = Convert.ToBoolean(objRates4.NotAvailable);
                txtExplanation4.Text = objRates4.Explanation;

            }
            #endregion


            if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) == 0))
            {
                if (chkNA1.Checked)
                {
                    trExplain1.Style["Display"] = "";
                    txtRateOne.Enabled = false;
                }
                else
                {
                    trExplain1.Style["Display"] = "None";
                    txtRateOne.Enabled = true;
                }

                if (chkNA2.Checked)
                {
                    trExplain2.Style["Display"] = "";
                    txtRateTwo.Enabled = false;
                }
                else
                {
                    trExplain2.Style["Display"] = "None";
                    txtRateTwo.Enabled = true;
                }

                if (chkNA3.Checked)
                {
                    trExplain3.Style["Display"] = "";
                    txtRateThree.Enabled = false;
                }
                else
                {
                    trExplain3.Style["Display"] = "None";
                    txtRateThree.Enabled = true;
                }

                //003
                if (chkEMRNA4.Checked)
                {
                    trExplainEMR4.Style["Display"] = "";
                    txtRateFour.Enabled = false;
                }
                else
                {
                    trExplainEMR4.Style["Display"] = "None";
                    txtRateFour.Enabled = true;
                }
            }

            #region OSHA Year 1
            VMSDAL.VQFSafetyOSHA objOsha1 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearOne.Text.Trim());
            if (objOsha1 != null)
            {
                txtNoofCasesFrmWorkOne.Text = objOsha1.TotalNoDays;
                txtNoofCasesTransferOne.Text = objOsha1.TotalNoJobTransfer;
                txtNoofDeathsYearOne.Text = objOsha1.TotalFatalities;
                txtRecordableCasesOne.Text = objOsha1.TotalNoOtherRecord;
                txtTotalHours1.Text = objOsha1.TotalHoursWorked;
                chkNA4.Checked = Convert.ToBoolean(objOsha1.NotAvailable);
                txtExplain4.Text = objOsha1.Explanation;

                // G. Vera Phase 3 Modifications Item 1.3
                txtNoofCasesFrmWorkOne.Enabled = true;
                txtNoofCasesTransferOne.Enabled = true;
                txtNoofDeathsYearOne.Enabled = true;
                txtRecordableCasesOne.Enabled = true;
                txtTotalHours1.Enabled = true;
                chkNA4.Enabled = true;
                txtExplain4.Enabled = true;

                if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (EditData.EditStatus == 1))
                {
                    // G. Vera Phase 3 Modifications Item 1.3
                    //txtNoofCasesFrmWorkOne.Enabled = false;
                    //txtNoofCasesTransferOne.Enabled = false;
                    //txtNoofDeathsYearOne.Enabled = false;
                    //txtRecordableCasesOne.Enabled = false;
                    //txtTotalHours1.Enabled = false;
                    //chkNA4.Enabled = false;
                    //txtExplain4.Enabled = false;
                }
                //if ((Convert.ToInt32(objSafety.GetSafetyStatusCount(Convert.ToInt64(Session["VendorId"]))) == 0) && ((hdnVendorStatus.Value != "3") && (hdnVendorStatus.Value != "2")))
                //{
                //    txtNoofCasesFrmWorkOne.Enabled = true;
                //    txtNoofCasesTransferOne.Enabled = true;
                //    txtNoofDeathsYearOne.Enabled = true;
                //    txtRecordableCasesOne.Enabled = true;
                //    txtTotalHours1.Enabled = true;
                //    chkNA4.Enabled = true;
                //    txtExplain4.Enabled = true;
                //}

                //008
                chkNoAttachment1.Checked = !(bool)objOsha1.HasOSHALog;
            }
            else
            {
                txtNoofCasesFrmWorkOne.Text = string.Empty;
                txtNoofCasesTransferOne.Text = string.Empty;
                txtNoofDeathsYearOne.Text = string.Empty;
                txtRecordableCasesOne.Text = string.Empty;
                txtTotalHours1.Text = string.Empty;
                chkNA4.Checked = false;
                txtExplain4.Text = string.Empty;

                // G. Vera Phase 3 Modifications Item 1.3
                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                txtNoofCasesFrmWorkOne.Enabled = true;
                txtNoofCasesTransferOne.Enabled = true;
                txtNoofDeathsYearOne.Enabled = true;
                txtRecordableCasesOne.Enabled = true;
                txtTotalHours1.Enabled = true;
                chkNA4.Enabled = true;
                txtExplain4.Enabled = true;
                //}
            }
            #endregion

            #region OSHA Year 2
            VMSDAL.VQFSafetyOSHA objOsha2 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearTwo.Text.Trim());
            if (objOsha2 != null)
            {
                txtNoofCasesFrmWorkTwo.Text = objOsha2.TotalNoDays;
                txtNoofCasesTransferTwo.Text = objOsha2.TotalNoJobTransfer;
                txtNoofDeathsYearTwo.Text = objOsha2.TotalFatalities;
                txtRecordableCasesTwo.Text = objOsha2.TotalNoOtherRecord;
                txtTotalHours2.Text = objOsha2.TotalHoursWorked;
                chkNA5.Checked = Convert.ToBoolean(objOsha2.NotAvailable);
                txtExplain5.Text = objOsha2.Explanation;

                // G. Vera Phase 3 Modifications Item 1.3
                txtNoofCasesFrmWorkTwo.Enabled = true;
                txtNoofCasesTransferTwo.Enabled = true;
                txtNoofDeathsYearTwo.Enabled = true;
                txtRecordableCasesTwo.Enabled = true;
                txtTotalHours2.Enabled = true;
                chkNA5.Enabled = true;
                txtExplain5.Enabled = true;

                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                //    txtNoofCasesFrmWorkTwo.Enabled = false;
                //    txtNoofCasesTransferTwo.Enabled = false;
                //    txtNoofDeathsYearTwo.Enabled = false;
                //    txtRecordableCasesTwo.Enabled = false;
                //    txtTotalHours2.Enabled = false;
                //    chkNA5.Enabled = false;
                //    txtExplain5.Enabled = false;
                //}

                //008
                chkNoAttachment2.Checked = !(bool)objOsha2.HasOSHALog;
            }
            else
            {
                txtNoofCasesFrmWorkTwo.Text = string.Empty;
                txtNoofCasesTransferTwo.Text = string.Empty;
                txtNoofDeathsYearTwo.Text = string.Empty;
                txtRecordableCasesTwo.Text = string.Empty;
                txtTotalHours2.Text = string.Empty;
                chkNA5.Checked = false;
                txtExplain5.Text = string.Empty;

                // G. Vera Phase 3 Modifications Item 1.3
                txtNoofCasesFrmWorkTwo.Enabled = true;
                txtNoofCasesTransferTwo.Enabled = true;
                txtNoofDeathsYearTwo.Enabled = true;
                txtRecordableCasesTwo.Enabled = true;
                txtTotalHours2.Enabled = true;
                chkNA5.Enabled = true;
                txtExplain5.Enabled = true;

                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                //    txtNoofCasesFrmWorkTwo.Enabled = true;
                //    txtNoofCasesTransferTwo.Enabled = true;
                //    txtNoofDeathsYearTwo.Enabled = true;
                //    txtRecordableCasesTwo.Enabled = true;
                //    txtTotalHours2.Enabled = true;
                //    chkNA5.Enabled = true;
                //    txtExplain5.Enabled = true;
                //}

            }
            #endregion

            #region OSHA Year 3
            VMSDAL.VQFSafetyOSHA objOsha3 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearThree.Text.Trim());
            if (objOsha3 != null)
            {
                txtNoofCasesFrmWorkThree.Text = objOsha3.TotalNoDays;
                txtNoofCasesTransferThree.Text = objOsha3.TotalNoJobTransfer;
                txtNoofDeathsYearThree.Text = objOsha3.TotalFatalities;
                txtRecordableCasesThree.Text = objOsha3.TotalNoOtherRecord;
                txtTotalHours3.Text = objOsha3.TotalHoursWorked;
                chkNA6.Checked = Convert.ToBoolean(objOsha3.NotAvailable);
                txtExplain6.Text = objOsha3.Explanation;

                // G. Vera Phase 3 Modifications Item 1.3
                txtNoofCasesFrmWorkThree.Enabled = true;
                txtNoofCasesTransferThree.Enabled = true;
                txtNoofDeathsYearThree.Enabled = true;
                txtRecordableCasesThree.Enabled = true;
                txtTotalHours3.Enabled = true;
                chkNA6.Enabled = true;
                txtExplain6.Enabled = true;

                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                //    txtNoofCasesFrmWorkThree.Enabled = false;
                //    txtNoofCasesTransferThree.Enabled = false;
                //    txtNoofDeathsYearThree.Enabled = false;
                //    txtRecordableCasesThree.Enabled = false;
                //    txtTotalHours3.Enabled = false;
                //    chkNA6.Enabled = false;
                //    txtExplain6.Enabled = false;
                //}

                //008
                chkNoAttachment3.Checked = !(bool)objOsha3.HasOSHALog;
            }
            else
            {
                txtNoofCasesFrmWorkThree.Text = string.Empty;
                txtNoofCasesTransferThree.Text = string.Empty;
                txtNoofDeathsYearThree.Text = string.Empty;
                txtRecordableCasesThree.Text = string.Empty;
                txtTotalHours3.Text = string.Empty;
                chkNA6.Checked = false;
                txtExplain6.Text = string.Empty;

                // G. Vera Phase 3 Modifications Item 1.3
                txtNoofCasesFrmWorkThree.Enabled = true;
                txtNoofCasesTransferThree.Enabled = true;
                txtNoofDeathsYearThree.Enabled = true;
                txtRecordableCasesThree.Enabled = true;
                txtTotalHours3.Enabled = true;
                chkNA6.Enabled = true;
                txtExplain6.Enabled = true;

                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                //    txtNoofCasesFrmWorkThree.Enabled = true;
                //    txtNoofCasesTransferThree.Enabled = true;
                //    txtNoofDeathsYearThree.Enabled = true;
                //    txtRecordableCasesThree.Enabled = true;
                //    txtTotalHours3.Enabled = true;
                //    chkNA6.Enabled = true;
                //    txtExplain6.Enabled = true;
                //}
            }
            #endregion

            //007
            #region OSHA Year 4
            VMSDAL.VQFSafetyOSHA objOsha4 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearFour.Text.Trim());
            if (objOsha4 != null)
            {
                txtNoofCasesFrmWorkFour.Text = objOsha4.TotalNoDays;
                txtNoofCasesTransferFour.Text = objOsha4.TotalNoJobTransfer;
                txtNoofDeathsYearFour.Text = objOsha4.TotalFatalities;
                txtRecordableCasesFour.Text = objOsha4.TotalNoOtherRecord;
                txtTotalHours4.Text = objOsha4.TotalHoursWorked;
                chkNA7.Checked = Convert.ToBoolean(objOsha4.NotAvailable);
                txtExplain7.Text = objOsha4.Explanation;

                txtNoofCasesFrmWorkFour.Enabled = true;
                txtNoofCasesTransferFour.Enabled = true;
                txtNoofDeathsYearFour.Enabled = true;
                txtRecordableCasesFour.Enabled = true;
                txtTotalHours4.Enabled = true;
                chkNA7.Enabled = true;
                txtExplain7.Enabled = true;

                //008
                chkNoAttachment4.Checked = !(bool)objOsha4.HasOSHALog;

            }
            else
            {
                txtNoofCasesFrmWorkFour.Text = string.Empty;
                txtNoofCasesTransferFour.Text = string.Empty;
                txtNoofDeathsYearFour.Text = string.Empty;
                txtRecordableCasesFour.Text = string.Empty;
                txtTotalHours4.Text = string.Empty;
                chkNA7.Checked = false;
                txtExplain7.Text = string.Empty;

                txtNoofCasesFrmWorkFour.Enabled = true;
                txtNoofCasesTransferFour.Enabled = true;
                txtNoofDeathsYearFour.Enabled = true;
                txtRecordableCasesFour.Enabled = true;
                txtTotalHours4.Enabled = true;
                chkNA7.Enabled = true;
                txtExplain7.Enabled = true;

                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                //    txtNoofCasesFrmWorkFour.Enabled = true;
                //    txtNoofCasesTransferThree.Enabled = true;
                //    txtNoofDeathsYearThree.Enabled = true;
                //    txtRecordableCasesThree.Enabled = true;
                //    txtTotalHours3.Enabled = true;
                //    chkNA6.Enabled = true;
                //    txtExplain6.Enabled = true;
                //}
            }
            #endregion

            //008 - has osha log checkboxes...aaarrrggghh
            starAttachOSHA1.Visible = !chkNoAttachment1.Checked;
            lnkAttachOSHA1.CssClass = (!chkNoAttachment1.Checked) ? "lnkAttach" : "lnk-disabled";
            lnkAttachOSHA1.Enabled = !chkNoAttachment1.Checked;

            starAttachOSHA2.Visible = !chkNoAttachment2.Checked;
            lnkAttachOSHA2.Enabled = !chkNoAttachment2.Checked;
            lnkAttachOSHA2.CssClass = (!chkNoAttachment2.Checked) ? "lnkAttach" : "lnk-disabled";

            starAttachOSHA3.Visible = !chkNoAttachment3.Checked;
            lnkAttachOSHA3.Enabled = !chkNoAttachment3.Checked;
            lnkAttachOSHA3.CssClass = (!chkNoAttachment3.Checked) ? "lnkAttach" : "lnk-disabled";

            starAttachOSHA4.Visible = !chkNoAttachment4.Checked;
            lnkAttachOSHA4.Enabled = !chkNoAttachment4.Checked;
            lnkAttachOSHA4.CssClass = (!chkNoAttachment4.Checked) ? "lnkAttach" : "lnk-disabled";


            if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) == 0))
            {
                if (chkNA4.Checked)
                {
                    trExplain4.Style["Display"] = "";
                    txtNoofCasesFrmWorkOne.Enabled = false;
                    txtNoofCasesTransferOne.Enabled = false;
                    txtNoofDeathsYearOne.Enabled = false;
                    txtRecordableCasesOne.Enabled = false;
                    txtTotalHours1.Enabled = false;
                }
                else
                {
                    trExplain4.Style["Display"] = "None";
                    txtNoofCasesFrmWorkOne.Enabled = true;
                    txtNoofCasesTransferOne.Enabled = true;
                    txtNoofDeathsYearOne.Enabled = true;
                    txtRecordableCasesOne.Enabled = true;
                    txtTotalHours1.Enabled = true;
                }

                if (chkNA5.Checked)
                {
                    trExplain5.Style["Display"] = "";
                    txtNoofCasesFrmWorkTwo.Enabled = false;
                    txtNoofCasesTransferTwo.Enabled = false;
                    txtNoofDeathsYearTwo.Enabled = false;
                    txtRecordableCasesTwo.Enabled = false;
                    txtTotalHours2.Enabled = false;
                }
                else
                {
                    trExplain5.Style["Display"] = "None";
                    txtNoofCasesFrmWorkTwo.Enabled = true;
                    txtNoofCasesTransferTwo.Enabled = true;
                    txtNoofDeathsYearTwo.Enabled = true;
                    txtRecordableCasesTwo.Enabled = true;
                    txtTotalHours2.Enabled = true;
                }

                if (chkNA6.Checked)
                {
                    trExplain6.Style["Display"] = "";
                    txtNoofCasesFrmWorkThree.Enabled = false;
                    txtNoofCasesTransferThree.Enabled = false;
                    txtNoofDeathsYearThree.Enabled = false;
                    txtRecordableCasesThree.Enabled = false;
                    txtTotalHours3.Enabled = false;
                }
                else
                {
                    trExplain6.Style["Display"] = "None";
                    txtNoofCasesFrmWorkThree.Enabled = true;
                    txtNoofCasesTransferThree.Enabled = true;
                    txtNoofDeathsYearThree.Enabled = true;
                    txtRecordableCasesThree.Enabled = true;
                    txtTotalHours3.Enabled = true;
                }

                //007
                if (chkNA7.Checked)
                {
                    trExplain7.Style["Display"] = "";
                    txtNoofCasesFrmWorkFour.Enabled = false;
                    txtNoofCasesTransferFour.Enabled = false;
                    txtNoofDeathsYearFour.Enabled = false;
                    txtRecordableCasesFour.Enabled = false;
                    txtTotalHours4.Enabled = false;
                }
                else
                {
                    trExplain7.Style["Display"] = "None";
                    txtNoofCasesFrmWorkFour.Enabled = true;
                    txtNoofCasesTransferFour.Enabled = true;
                    txtNoofDeathsYearFour.Enabled = true;
                    txtRecordableCasesFour.Enabled = true;
                    txtTotalHours4.Enabled = true;
                }
            }

            string SafetyProgram = Convert.ToString(EditData.SafetyProgram);

            chkSICNA.Checked = EditData.SICNA == true ? true : false;

            if (chkSICNA.Checked)
            {
                trSICExplain.Style["display"] = "";
                txtSICExplain.Text = EditData.Explanation;
            }
            else
            {
                trSICExplain.Style["display"] = "None";
                txtSIC.Text = EditData.SICNAICSCode;
            }
            if (SafetyProgram == "False")
            {
                rdoSafetyProgram.SelectedIndex = 1;
            }
            else if (SafetyProgram == "True")
            {
                rdoSafetyProgram.SelectedIndex = 0;
            }

            string SafetyRequirement = Convert.ToString(EditData.SafetyRequirements);
            if (SafetyRequirement == "False")
            {
                rdoSafetytraining.SelectedIndex = 1;
            }
            else if (SafetyRequirement == "True")
            {
                rdoSafetytraining.SelectedIndex = 0;
            }

            string CompanySafety = Convert.ToString(EditData.CompanySafety);
            if (CompanySafety == "False")
            {
                rdoSafetyDirector.SelectedIndex = 1;
            }
            else if (CompanySafety == "True")
            {
                rdoSafetyDirector.SelectedIndex = 0;
            }
            else
            {
                rdoSafetyDirector.SelectedIndex = -1;
            }


            txtContactName.Text = EditData.CompanySafetyContactName;
            objSafety.VentID = VendorId;

            chkContactUSA.Checked = !(bool)EditData.CompanyContactIsBasedInUS;//006
            SwitchPhoneForKeyContact(chkContactUSA);

            chkReperesentUSA.Checked = !(bool)EditData.RepresentContactIsBasedInUS;//006
            SwitchPhoneForKeyContact(chkReperesentUSA);

            string ContactPhoneNo = EditData.CompanySafetyContactNumber;
            objCommon.loadphone(txtMainPhno1, txtMainPhno2, txtMainPhno3, ContactPhoneNo);

            //005
            string SafetyRepresentation = Convert.ToString(EditData.SafetyRepresentation);
            if (SafetyRepresentation == "False")
            {
                rdoRepresentation.SelectedIndex = 1;
            }
            else if (SafetyRepresentation == "True")
            {
                rdoRepresentation.SelectedIndex = 0;
            }
            else
            {
                rdoRepresentation.SelectedIndex = -1;
            }

            txtRepresentationContactName.Text = EditData.SafetyRepresentationContactName;      //005
            objCommon.loadphone(txtRepresentationContactNumberOne, txtRepresentationContactNumberTwo, txtRepresentationContactNumberThree, EditData.SafetyRepresentationContactNumber);      //005

            if (EditData.EditStatus == 1)
            {
                //if (!((string.IsNullOrEmpty(txtRateOne.Text.Trim())) && (string.IsNullOrEmpty(txtExplanation1.Text.Trim()))))
                //{
                //    chkNA1.Enabled = false;
                //    txtExplanation1.ReadOnly = true;
                //    chkNA2.Enabled = false;
                //    txtExplanation2.ReadOnly = true;
                //    chkNA3.Enabled = false;
                //    txtExplanation3.ReadOnly = true;
                //}
                //else
                //{
                //    chkNA1.Enabled = true;
                //    txtExplanation1.ReadOnly = false;
                //    chkNA2.Enabled = false;
                //    txtExplanation2.ReadOnly = true;
                //    chkNA3.Enabled = false;
                //    txtExplanation3.ReadOnly = true;
                //}

                foreach (Control ctrl in pnlEmr.Controls)
                {
                    if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.Text.Trim()Box"))
                    {

                        TextBox txt = (TextBox)ctrl;
                        if (!string.IsNullOrEmpty(txt.Text.Trim()))
                        {
                            txt.ReadOnly = true;

                        }
                        else
                        {
                            txt.ReadOnly = false;
                        }
                    }
                    if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
                    {
                        LinkButton lnk = (LinkButton)ctrl;
                        lnk.Visible = false;
                    }
                }
                foreach (Control ctrl in pnlOsha.Controls)
                {

                    //if (!((string.IsNullOrEmpty(txtExplain4.Text.Trim())) && (string.IsNullOrEmpty(txtNoofDeathsYearOne.Text.Trim())) && (string.IsNullOrEmpty(txtNoofCasesFrmWorkOne.Text.Trim())) && (string.IsNullOrEmpty(txtNoofCasesTransferOne.Text.Trim())) && (string.IsNullOrEmpty(txtRecordableCasesOne.Text.Trim()))))
                    //{
                    //    chkNA4.Enabled = false;
                    //    txtExplain4.ReadOnly = true;
                    //    chkNA5.Enabled = false;
                    //    txtExplain5.ReadOnly = true;
                    //    chkNA6.Enabled = false;
                    //    txtExplain6.ReadOnly = true;
                    //}
                    //else
                    //{
                    //    chkNA4.Enabled = true;
                    //    txtExplain4.ReadOnly = false;
                    //    chkNA5.Enabled = false;
                    //    txtExplain5.ReadOnly = true;
                    //    chkNA6.Enabled = false;
                    //    txtExplain6.ReadOnly = true;
                    //}
                    if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.Text.Trim()Box"))
                    {
                        TextBox txt = (TextBox)ctrl;
                        if (!string.IsNullOrEmpty(txt.Text.Trim()))
                        {
                            txt.ReadOnly = true;

                        }
                        else
                        {
                            txt.ReadOnly = false;
                        }
                    }
                    if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
                    {
                        LinkButton lnk = (LinkButton)ctrl;
                        lnk.Visible = false;
                    }
                }
            }
        }
        else
        {
            #region Citations
            //013
            citation1 = new clsSafety().SelectSafetyCitation(00, CitationYear1);
            lblCitationYear1.Text = "Any citations in " + CitationYear1 + "?";
            lnkAttachCITATION1.Text = (rdoCitationYear1.SelectedIndex != 0) ?
                                     "" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick1.Attributes["style"] = (rdoCitationYear1.SelectedIndex == 0) ? "display: " : "display: none";

            citation2 = new clsSafety().SelectSafetyCitation(00, CitationYear2);
            lblCitationYear2.Text = "Any citations in " + CitationYear2 + "?";
            lnkAttachCITATION2.Text = (rdoCitationYear2.SelectedIndex != 0) ?
                                     "" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick2.Attributes["style"] = (rdoCitationYear2.SelectedIndex == 0) ? "display: " : "display: none";

            citation3 = new clsSafety().SelectSafetyCitation(00, CitationYear3);
            lblCitationYear3.Text = "Any citations in " + CitationYear3 + "?";
            lnkAttachCITATION3.Text = (rdoCitationYear3.SelectedIndex != 0) ?
                                     "" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick3.Attributes["style"] = (rdoCitationYear3.SelectedIndex == 0) ? "display: " : "display: none";

            #endregion


        }

        


        HandleStatus();     //008

    }

    //008 - added
    private void LoadSafetyAttachmentControl(string buttonID = "")
    {
        // save info first to make sure we have synched safety data
        this.imbSaveandClose_Click(lnkAttachEMR1, null);

        // then load attachment user control
        switch (buttonID.ToUpper())
        {
            case "EMR1":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.EMR = new clsSafety().SelectSafetyEMRRates(Convert.ToInt64(hdnSafetyId.Value), txtYearOne.Text);
                uclAttachEMR1.SafetySection = SafetySection.EMR;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtYearOne.Text + " EMR Letter";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "EMR2":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.EMR = new clsSafety().SelectSafetyEMRRates(Convert.ToInt64(hdnSafetyId.Value), txtYearTwo.Text);
                uclAttachEMR1.SafetySection = SafetySection.EMR;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtYearTwo.Text + " EMR Letter";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "EMR3":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.EMR = new clsSafety().SelectSafetyEMRRates(Convert.ToInt64(hdnSafetyId.Value), txtYearThree.Text);
                uclAttachEMR1.SafetySection = SafetySection.EMR;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtYearThree.Text + " EMR Letter";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "EMR4":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.EMR = new clsSafety().SelectSafetyEMRRates(Convert.ToInt64(hdnSafetyId.Value), txtYearFour.Text);
                uclAttachEMR1.SafetySection = SafetySection.EMR;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtYearFour.Text + " EMR Letter";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "OSHA1":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.OSHA = new clsSafety().SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearOne.Text);
                uclAttachEMR1.SafetySection = SafetySection.OSHA;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtOSHAYearOne.Text + " OSHA Attachment";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "OSHA2":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.OSHA = new clsSafety().SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearTwo.Text);
                uclAttachEMR1.SafetySection = SafetySection.OSHA;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtOSHAYearTwo.Text + " OSHA Attachment";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "OSHA3":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.OSHA = new clsSafety().SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearThree.Text);
                uclAttachEMR1.SafetySection = SafetySection.OSHA;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtOSHAYearThree.Text + " OSHA Attachment";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "OSHA4":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.OSHA = new clsSafety().SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearFour.Text);
                uclAttachEMR1.SafetySection = SafetySection.OSHA;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtOSHAYearFour.Text + " OSHA Attachment";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            //013 - START
            case "CITATION1":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.CITATION = new clsSafety().SelectSafetyCitation(Convert.ToInt64(hdnSafetyId.Value), CitationYear1);
                uclAttachEMR1.SafetySection = SafetySection.CITATIONS;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = "Citation Document";
                hdnCitationFile1.Value = uclAttachEMR1.CITATION.CitationFilePath;
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "CITATION2":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.CITATION = new clsSafety().SelectSafetyCitation(Convert.ToInt64(hdnSafetyId.Value), CitationYear2);
                uclAttachEMR1.SafetySection = SafetySection.CITATIONS;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = "Citation Document";
                hdnCitationFile2.Value = uclAttachEMR1.CITATION.CitationFilePath;
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "CITATION3":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.CITATION = new clsSafety().SelectSafetyCitation(Convert.ToInt64(hdnSafetyId.Value), CitationYear3);
                uclAttachEMR1.SafetySection = SafetySection.CITATIONS;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = "Citation Document";
                hdnCitationFile3.Value = uclAttachEMR1.CITATION.CitationFilePath;
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            //013 - END
            default:
                break;
        } 

    }

    /// <summary>
    /// visible controls in postback
    /// </summary>
    public void ControlVisiblity()
    {
        if (rdoSafetyDirector.SelectedIndex == 0)
        {
            spnContact.Style["display"] = "";

        }
        spnContactNonUS.Style["display"] = "none";
        if (rdoRepresentation.SelectedIndex == 0)//005
        {
            spnContactNonUS.Style["display"] = "";

        }
        if (chkSICNA.Checked)
        {
            trSICExplain.Style["display"] = "";
            txtSIC.Enabled = false;
        }
        else
        {
            trSICExplain.Style["display"] = "None";
            txtSIC.Enabled = true;
        }

        if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 0))
        {

            if (chkNA1.Checked)
            {
                trExplain1.Style["Display"] = "";
                txtRateOne.Enabled = false;
                trEMRYEar4Field.Attributes["Style"] = @"display: """;   //003
                trAttachEMR1.Attributes["Style"] = @"display: none";   //008
                //txtRateFour.Enabled = (!chkEMRNA4.Checked && string.IsNullOrEmpty(txtRateFour.Text.Trim()));    //012
            }
            else
            {
                trExplain1.Style["Display"] = "none";
                txtRateOne.Enabled = true;
                trEMRYEar4Field.Attributes["Style"] = @"display: none";     //003
                trAttachEMR1.Attributes["Style"] = @"display: ";   //008

            }
            if (chkNA2.Checked)
            {
                trExplain2.Style["Display"] = "";
                txtRateTwo.Enabled = false;
                trAttachEMR2.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain2.Style["Display"] = "none";
                txtRateTwo.Enabled = true;
                trAttachEMR2.Attributes["Style"] = @"display: ";   //008
            }
            if (chkNA3.Checked)
            {
                trExplain3.Style["Display"] = "";
                txtRateThree.Enabled = false;

                trAttachEMR3.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain3.Style["Display"] = "none";
                txtRateThree.Enabled = true;
                trAttachEMR3.Attributes["Style"] = @"display: ";   //008
            }
            //003
            if (chkEMRNA4.Checked)
            {
                trExplainEMR4.Style["Display"] = "";
                txtRateFour.Enabled = false;
                trAttachEMR4.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplainEMR4.Style["Display"] = "none";
                txtRateFour.Enabled = true;
                trAttachEMR4.Attributes["Style"] = @"display: ";   //008
            }

            if (chkNA4.Checked)
            {
                trExplain4.Style["Display"] = "";
                txtNoofCasesFrmWorkOne.Enabled = false;
                txtNoofCasesTransferOne.Enabled = false;
                txtNoofDeathsYearOne.Enabled = false;
                txtRecordableCasesOne.Enabled = false;
                txtTotalHours1.Enabled = false;
                pnlOSHAYear4.Attributes["Style"] = @"display: """;   //007
                trAttachOSHA1.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment1.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain4.Style["Display"] = "None";
                txtNoofCasesFrmWorkOne.Enabled = true;
                txtNoofCasesTransferOne.Enabled = true;
                txtNoofDeathsYearOne.Enabled = true;
                txtRecordableCasesOne.Enabled = true;
                txtTotalHours1.Enabled = true;
                pnlOSHAYear4.Attributes["Style"] = @"display: none";   //007
                trAttachOSHA1.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment1.Attributes["Style"] = @"display: ";   //008
            }

            if (chkNA5.Checked)
            {
                trExplain5.Style["Display"] = "";
                txtNoofCasesFrmWorkTwo.Enabled = false;
                txtNoofCasesTransferTwo.Enabled = false;
                txtNoofDeathsYearTwo.Enabled = false;
                txtRecordableCasesTwo.Enabled = false;
                txtTotalHours2.Enabled = false;
                trAttachOSHA2.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment2.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain5.Style["Display"] = "None";
                txtNoofCasesFrmWorkTwo.Enabled = true;
                txtNoofCasesTransferTwo.Enabled = true;
                txtNoofDeathsYearTwo.Enabled = true;
                txtRecordableCasesTwo.Enabled = true;
                txtTotalHours2.Enabled = true;
                trAttachOSHA2.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment2.Attributes["Style"] = @"display: ";   //008
            }

            if (chkNA6.Checked)
            {
                trExplain6.Style["Display"] = "";
                txtNoofCasesFrmWorkThree.Enabled = false;
                txtNoofCasesTransferThree.Enabled = false;
                txtNoofDeathsYearThree.Enabled = false;
                txtRecordableCasesThree.Enabled = false;
                txtTotalHours3.Enabled = false;
                trAttachOSHA3.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment3.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain6.Style["Display"] = "None";
                txtNoofCasesFrmWorkThree.Enabled = true;
                txtNoofCasesTransferThree.Enabled = true;
                txtNoofDeathsYearThree.Enabled = true;
                txtRecordableCasesThree.Enabled = true;
                txtTotalHours3.Enabled = true;
                trAttachOSHA3.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment3.Attributes["Style"] = @"display: ";   //008
            }

            //007
            if (chkNA7.Checked)
            {
                trExplain7.Style["Display"] = "";
                txtNoofCasesFrmWorkFour.Enabled = false;
                txtNoofCasesTransferFour.Enabled = false;
                txtNoofDeathsYearFour.Enabled = false;
                txtRecordableCasesFour.Enabled = false;
                txtTotalHours4.Enabled = false;
                trAttachOSHA4.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment4.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain7.Style["Display"] = "None";
                txtNoofCasesFrmWorkFour.Enabled = true;
                txtNoofCasesTransferFour.Enabled = true;
                txtNoofDeathsYearFour.Enabled = true;
                txtRecordableCasesFour.Enabled = true;
                txtTotalHours4.Enabled = true;
                trAttachOSHA4.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment4.Attributes["Style"] = @"display: ";   //008
            }

            this.SafetyVisibilityBasedOnData();        //007


        }
        else
        {
            this.SafetyVisibilityBasedOnData(1);        //007


            if (chkNA1.Checked)
            {
                trExplain1.Style["Display"] = "";
                //     txtRateOne.Enabled = false;
                trAttachEMR1.Attributes["Style"] = @"display: none";   //008
                //txtRateFour.Enabled = (!chkEMRNA4.Checked && string.IsNullOrEmpty(txtRateFour.Text.Trim()));    //012
            }
            else
            {
                trExplain1.Style["Display"] = "none";
                //  txtRateOne.Enabled = true;
                trAttachEMR1.Attributes["Style"] = @"display: ";   //008
            }
            if (chkNA2.Checked)
            {
                trExplain2.Style["Display"] = "";
                //  txtRateTwo.Enabled = false;
                trAttachEMR2.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain2.Style["Display"] = "none";
                //   txtRateTwo.Enabled = true;
                trAttachEMR2.Attributes["Style"] = @"display: ";   //008
            }
            if (chkNA3.Checked)
            {
                trExplain3.Style["Display"] = "";
                //  txtRateThree.Enabled = false;
                trAttachEMR3.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain3.Style["Display"] = "none";
                //   txtRateThree.Enabled = true;
                trAttachEMR3.Attributes["Style"] = @"display: ";   //008
            }

            //003
            if (chkEMRNA4.Checked)
            {
                trExplainEMR4.Style["Display"] = "";
                trAttachEMR4.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplainEMR4.Style["Display"] = "none";
                trAttachEMR4.Attributes["Style"] = @"display: ";   //008
            }

            if (chkNA4.Checked)
            {
                trExplain4.Style["Display"] = "";
                pnlOSHAYear4.Attributes["Style"] = "display: "; //007
                trAttachOSHA1.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment1.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain4.Style["Display"] = "None";
                pnlOSHAYear4.Attributes["Style"] = "display: none";     //007
                trAttachOSHA1.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment1.Attributes["Style"] = @"display: ";   //008

            }

            if (chkNA5.Checked)
            {
                trExplain5.Style["Display"] = "";
                trAttachOSHA2.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment2.Attributes["Style"] = @"display: none";   //008
                
            }
            else
            {
                trExplain5.Style["Display"] = "None";
                trAttachOSHA2.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment2.Attributes["Style"] = @"display: ";   //008

            }

            if (chkNA6.Checked)
            {
                trExplain6.Style["Display"] = "";
                trAttachOSHA3.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment3.Attributes["Style"] = @"display: none";   //008

            }
            else
            {
                trExplain6.Style["Display"] = "None";
                trAttachOSHA3.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment3.Attributes["Style"] = @"display: ";   //008

            }

            //007
            if (chkNA7.Checked)
            {
                trExplain7.Style["Display"] = "";
                trAttachOSHA4.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment4.Attributes["Style"] = @"display: none";   //008

            }
            else
            {
                trExplain7.Style["Display"] = "None";
                trAttachOSHA4.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment4.Attributes["Style"] = @"display: ";   //008

            }


        } 
        
    }

    /// <summary>
    /// 007
    /// </summary>
    /// <param name="editStatus"></param>
    private void SafetyVisibilityBasedOnData(int editStatus = 0)
    {
        //012 - added
        if (!string.IsNullOrEmpty(hdnSafetyId.Value))
        {
            SafetyId = Convert.ToInt64(hdnSafetyId.Value);
        }
        else
        {
            SafetyId = Convert.ToInt64(ViewState["SafetyID"]);
        }

        #region EMR Rates Year 1
        VMSDAL.VQFSafetyEMRRate objRates = objSafety.SelectSafetyEMRRates(SafetyId, txtYearOne.Text.Trim());
        if (objRates != null)
        {
            // G. Vera Phase 3 Modifications Item 1.3 - At this point we have laready checked the vendor status and edit status
            // Why check again?
            //010 - all this has changed.  EMR year 1 should always be editable
            chkNA1.Enabled = true;
            txtRateOne.Enabled = !chkNA1.Checked;

            #region Archived
            //if (objRates.PastYearStatus == 0)
            //{
            //    // G. Vera Phase 3 Modifications Item 1.3
            //    if (txtRateOne.Text.Length == 0 || txtExplanation1.Text != "" || double.Parse(txtRateOne.Text) == 0)
            //    {
            //        Session["Year1EMRChanged"] = "1";   //001
            //        chkNA1.Enabled = true;
            //        txtExplanation1.Enabled = true;

            //        if (!chkNA1.Checked) txtRateOne.Enabled = true;
            //        else txtRateOne.Enabled = false;
            //    }
            //    else
            //    {
            //        if (Session["Year1EMRChanged"] != "1")  //001
            //        {
            //            chkNA1.Enabled = false;
            //            txtRateOne.Enabled = false;
            //            txtExplanation1.Enabled = false;
            //        }
            //        else
            //        {
            //            chkNA1.Enabled = true;
            //            txtExplanation1.Enabled = true;

            //            if (!chkNA1.Checked) txtRateOne.Enabled = true;
            //            else txtRateOne.Enabled = false;
            //        }
            //    }
            //}
            //else
            //{
            //    // G. Vera Phase 3 Modifications Item 1.3
            //    if (txtRateOne.Text.Length == 0 || txtExplanation1.Text != "" || double.Parse(txtRateOne.Text) == 0)
            //    {
            //        Session["Year1EMRChanged"] = "1";   //001
            //        chkNA1.Enabled = true;
            //        txtExplanation1.Enabled = true;

            //        if (!chkNA1.Checked) txtRateOne.Enabled = true;
            //        else txtRateOne.Enabled = false;

            //    }
            //    else
            //    {
            //        if (Session["Year1EMRChanged"] != "1")  //001
            //        {
            //            chkNA1.Enabled = false;
            //            txtRateOne.Enabled = false;
            //            txtExplanation1.Enabled = false;
            //        }
            //        else
            //        {
            //            chkNA1.Enabled = true;
            //            txtExplanation1.Enabled = true;

            //            if (!chkNA1.Checked) txtRateOne.Enabled = true;
            //            else txtRateOne.Enabled = false;

            //        }
            //    }
            //} 
            #endregion

            //008
            if (!string.IsNullOrEmpty(objRates.FilePath))
            {
                imbAttachEMR1.Visible = true;
                lnkAttachEMR1.Visible = false;
            }
            else
            {
                imbAttachEMR1.Visible = false;
                lnkAttachEMR1.Visible = true;
            }
        }
        else
        {
            //if (SafetyId != 0) Session["Year1EMRChanged"] = "1";   //001
            chkNA1.Enabled = true;
            txtRateOne.Enabled = true;
            txtExplanation1.Enabled = true;
            trAttachEMR1.Attributes["Style"] = @"display: ";   //008
        }

        #region Archive
        //int CountSafety = objSafety.GetSafetyCount(Convert.ToInt64(Session["VendorId"]));
        //if (CountSafety > 0)
        //{
        //    if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1)) || (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
        //    {
        //        txtSICExplain.Enabled = false;
        //        chkSICNA.Enabled = false;
        //        txtSIC.Enabled = false;
        //    }
        //    else
        //    {
        //        txtSICExplain.Enabled = true;
        //        chkSICNA.Enabled = true;
        //        txtSIC.Enabled = true;
        //    }
        //} 
        #endregion

        #endregion

        #region EMR Rates Year 2
        objRates = objSafety.SelectSafetyEMRRates(SafetyId, txtYearTwo.Text.Trim());
        if (objRates != null)
        {
            //012
            bool enabled = (!string.IsNullOrEmpty(ViewState["SubmittedYear"] as String)) ? (ViewState["SubmittedYear"].ConvertToInteger() < txtYearTwo.Text.Trim().ConvertToInteger()) : true;

            // G. Vera Phase 3 Modifications Item 1.3
            //007
            if (editStatus == 1)
            {
                if (objRates.PastYearStatus == 0)
                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1)) || (Convert.ToInt32(hdnVendorStatus.Value) > 0)) && objRates.PastYearStatus == 0)
                {
                    // G. Vera Phase 3 Modifications Item 1.3
                    if (txtRateTwo.Text.Length == 0 || txtExplanation2.Text != "" || double.Parse(txtRateTwo.Text) == 0)
                    {
                        Session["Year2EMRChanged"] = "1";   //001
                        chkNA2.Enabled = true;
                        txtExplanation2.Enabled = true;

                        if (!chkNA2.Checked) txtRateTwo.Enabled = true;
                        else txtRateTwo.Enabled = false;
                    }
                    else
                    {
                        if (Session["Year2EMRChanged"] != "1" &&  !enabled)   // 001
                        {
                            chkNA2.Enabled = false;
                            txtRateTwo.Enabled = false;
                            txtExplanation2.Enabled = false;
                        }
                        else
                        {
                            chkNA2.Enabled = true;
                            txtExplanation2.Enabled = true;

                            if (!chkNA2.Checked) txtRateTwo.Enabled = true;
                            else txtRateTwo.Enabled = false;
                        }
                    }
                }
                else
                {
                    // G. Vera Phase 3 Modifications Item 1.3
                    if (txtRateTwo.Text.Length == 0 || txtExplanation2.Text != "" || double.Parse(txtRateTwo.Text) == 0)
                    {
                        Session["Year2EMRChanged"] = "1";   //001
                        chkNA2.Enabled = true;
                        txtExplanation2.Enabled = true;

                        if (!chkNA2.Checked) txtRateTwo.Enabled = true;
                        else txtRateTwo.Enabled = false;
                    }
                    else
                    {
                        if (Session["Year2EMRChanged"] != "1" && !enabled)  //001, 012
                        {
                            chkNA2.Enabled = false;
                            txtRateTwo.Enabled = false;
                            txtExplanation2.Enabled = false;
                        }
                        else
                        {
                            chkNA2.Enabled = true;
                            txtExplanation2.Enabled = true;

                            if (!chkNA2.Checked) txtRateTwo.Enabled = true;
                            else txtRateTwo.Enabled = false;
                        }
                    }
                }
            }

            //008
            if (!string.IsNullOrEmpty(objRates.FilePath))
            {
                imbAttachEMR2.Visible = true;
                lnkAttachEMR2.Visible = false;
            }
            else
            {
                imbAttachEMR2.Visible = false;
                lnkAttachEMR2.Visible = true;
            }
        }
        else
        {
            Session["Year2EMRChanged"] = "1";   //012
        }
        #endregion

        #region EMR Rates Year 3

        objRates = objSafety.SelectSafetyEMRRates(SafetyId, txtYearThree.Text.Trim());
        if (objRates != null)
        {
            //012
            bool enabled = (!string.IsNullOrEmpty(ViewState["SubmittedYear"] as String)) ? (ViewState["SubmittedYear"].ConvertToInteger() < txtYearThree.Text.Trim().ConvertToInteger()) : true;
            // G. Vera Phase 3 Modifications Item 1.3
            if (editStatus == 1)
            {
                if (objRates.PastYearStatus == 0)
                {
                    // G. Vera Phase 3 Modifications Item 1.3
                    if (txtRateThree.Text.Length == 0 || txtExplanation3.Text != "" || double.Parse(txtRateThree.Text) == 0)
                    {
                        Session["Year3EMRChanged"] = "1";   //001
                        chkNA3.Enabled = true;
                        txtExplanation3.Enabled = true;

                        if (!chkNA3.Checked) txtRateThree.Enabled = true;
                        else txtRateThree.Enabled = false;
                    }
                    else
                    {
                        if (Session["Year3EMRChanged"] != "1" && !enabled)  //001
                        {
                            chkNA3.Enabled = false;
                            txtRateThree.Enabled = false;
                            txtExplanation3.Enabled = false;
                        }
                        else
                        {
                            chkNA3.Enabled = true;
                            txtExplanation3.Enabled = true;

                            if (!chkNA3.Checked) txtRateThree.Enabled = true;
                            else txtRateThree.Enabled = false;
                        }
                    }
                }
                else
                {
                    // G. Vera Phase 3 Modifications Item 1.3
                    if (txtRateThree.Text.Length == 0 || txtExplanation3.Text != "" || double.Parse(txtRateThree.Text) == 0)
                    {
                        Session["Year3EMRChanged"] = "1";   //001
                        chkNA3.Enabled = true;
                        txtExplanation3.Enabled = true;

                        if (!chkNA3.Checked) txtRateThree.Enabled = true;
                        else txtRateThree.Enabled = false;
                    }
                    else
                    {
                        if (Session["Year3EMRChanged"] != "1" && !enabled)  //001, 012
                        {
                            chkNA3.Enabled = false;
                            txtRateThree.Enabled = false;
                            txtExplanation3.Enabled = false;
                        }
                        else
                        {
                            chkNA3.Enabled = true;
                            txtExplanation3.Enabled = true;

                            if (!chkNA3.Checked) txtRateThree.Enabled = true;
                            else txtRateThree.Enabled = false;
                        }
                    }
                } 
            }

            //008
            if (!string.IsNullOrEmpty(objRates.FilePath))
            {
                imbAttachEMR3.Visible = true;
                lnkAttachEMR3.Visible = false;
            }
            else
            {
                imbAttachEMR3.Visible = false;
                lnkAttachEMR3.Visible = true;
            }
        }
        else
        {
            Session["Year3EMRChanged"] = "1";   //012
        }
        #endregion
        //003
        #region EMR Rates Year 4

        if (chkNA1.Checked)
        {
            trEMRYEar4Field.Attributes["Style"] = @"display: """;
            
        }
        else
        {
            trEMRYEar4Field.Attributes["Style"] = @"display: none";
        }

        objRates = objSafety.SelectSafetyEMRRates(SafetyId, txtYearFour.Text.Trim());
        if (objRates != null)
        {
            bool enabled = (!string.IsNullOrEmpty(ViewState["SubmittedYear"] as String)) ? (ViewState["SubmittedYear"].ConvertToInteger() < txtYearFour.Text.Trim().ConvertToInteger()) : true;

            if (editStatus == 1)
            {
                if (objRates.PastYearStatus == 0)
                {
                    if (txtRateFour.Text.Length == 0 || txtExplanation4.Text != "" || double.Parse(txtRateFour.Text) == 0)
                    {
                        Session["Year4EMRChanged"] = "1";
                        chkEMRNA4.Enabled = true;
                        txtExplanation4.Enabled = true;

                        if (!chkEMRNA4.Checked) txtRateFour.Enabled = true;
                        else txtRateFour.Enabled = false;
                    }
                    else
                    {
                        if (Session["Year4EMRChanged"] != "1" && !enabled)
                        {
                            chkEMRNA4.Enabled = false;
                            txtRateFour.Enabled = false;
                            txtExplanation4.Enabled = false;
                        }
                        else
                        {
                            chkEMRNA4.Enabled = true;
                            txtExplanation4.Enabled = true;

                            if (!chkEMRNA4.Checked) txtRateFour.Enabled = true;
                            else txtRateFour.Enabled = false;
                        }
                    }
                }
                else
                {
                    if (txtRateFour.Text.Length == 0 || txtExplanation4.Text != "" || double.Parse(txtRateFour.Text) == 0)
                    {
                        Session["Year4EMRChanged"] = "1";
                        chkEMRNA4.Enabled = true;
                        txtExplanation4.Enabled = true;

                        if (!chkEMRNA4.Checked) txtRateFour.Enabled = true;
                        else txtRateFour.Enabled = false;
                    }
                    else
                    {
                        if (Session["Year4EMRChanged"] != "1" && !enabled)      //012
                        {
                            chkEMRNA4.Enabled = false;
                            txtRateFour.Enabled = false;
                            txtExplanation4.Enabled = false;
                        }
                        else
                        {
                            chkEMRNA4.Enabled = true;
                            txtExplanation4.Enabled = true;

                            if (!chkEMRNA4.Checked) txtRateFour.Enabled = true;
                            else txtRateFour.Enabled = false;
                        }
                    }
                } 
            }

            //008
            if (!string.IsNullOrEmpty(objRates.FilePath))
            {
                imbAttachEMR4.Visible = true;
                lnkAttachEMR4.Visible = false;
            }
            else
            {
                imbAttachEMR4.Visible = false;
                lnkAttachEMR4.Visible = true;
            }
        }
        else
        {
            Session["Year4EMRChanged"] = "1";   //012
        }
        #endregion


        #region OSHA Year 1
        VMSDAL.VQFSafetyOSHA objOsha1 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearOne.Text.Trim());
        if (objOsha1 != null)
        {
            // G. Vera Phase 3 Modifications Item 1.3
            //001
            if (!chkNA4.Checked)
            {
                txtNoofCasesFrmWorkOne.Enabled = true;
                txtNoofCasesTransferOne.Enabled = true;
                txtNoofDeathsYearOne.Enabled = true;
                txtRecordableCasesOne.Enabled = true;
                txtTotalHours1.Enabled = true;
                trAttachOSHA1.Attributes["Style"] = @"display: ";   //008

            }
            else
            {
                txtNoofCasesFrmWorkOne.Enabled = false;
                txtNoofCasesTransferOne.Enabled = false;
                txtNoofDeathsYearOne.Enabled = false;
                txtRecordableCasesOne.Enabled = false;
                txtTotalHours1.Enabled = false;
                trAttachOSHA1.Attributes["Style"] = @"display: none";   //008
            }

            //008
            if (!string.IsNullOrEmpty(objOsha1.FilePath))
            {
                imbAttachOSHA1.Visible = true;
                lnkAttachOSHA1.Visible = false;
                starAttachOSHA1.Visible = false;
            }
            else
            {
                imbAttachOSHA1.Visible = false;
                lnkAttachOSHA1.Visible = true;
                starAttachOSHA1.Visible =  !chkNoAttachment1.Checked;
            }

            chkNA4.Enabled = true;
            txtExplain4.Enabled = true;


            //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1) )
            //{
            //    txtNoofCasesFrmWorkOne.Enabled = false;
            //    txtNoofCasesTransferOne.Enabled = false;
            //    txtNoofDeathsYearOne.Enabled = false;
            //    txtRecordableCasesOne.Enabled = false;
            //    chkNA4.Enabled = false;
            //    txtExplain4.Enabled = false;
            //    txtTotalHours1.Enabled = false;
            //}
            //else
            //{
            //    txtNoofCasesFrmWorkOne.Enabled = true;
            //    txtNoofCasesTransferOne.Enabled = true;
            //    txtNoofDeathsYearOne.Enabled = true;
            //    txtRecordableCasesOne.Enabled = true;
            //    chkNA4.Enabled = true;
            //    txtExplain4.Enabled = true;
            //    txtTotalHours1.Enabled = true;
            //}


        }
        else
        {

            if (!chkNA4.Checked)
            {
                txtNoofCasesFrmWorkOne.Enabled = true;
                txtNoofCasesTransferOne.Enabled = true;
                txtNoofDeathsYearOne.Enabled = true;
                txtRecordableCasesOne.Enabled = true;
                txtTotalHours1.Enabled = true;
                trAttachOSHA1.Attributes["Style"] = @"display: ";   //008
            }
            else
            {
                txtNoofCasesFrmWorkOne.Enabled = false;
                txtNoofCasesTransferOne.Enabled = false;
                txtNoofDeathsYearOne.Enabled = false;
                txtRecordableCasesOne.Enabled = false;
                txtTotalHours1.Enabled = false;
                trAttachOSHA1.Attributes["Style"] = @"display: none";   //008
            }

            chkNA4.Enabled = true;
            txtExplain4.Enabled = true;

        }
        #endregion

        #region OSHA Year 2
        objOsha1 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearTwo.Text.Trim());
        if (objOsha1 != null)
        {
            // G. Vera Phase 3 Modifications Item 1.3
            //001
            if (!chkNA5.Checked)
            {
                txtNoofCasesFrmWorkTwo.Enabled = true;
                txtNoofCasesTransferTwo.Enabled = true;
                txtNoofDeathsYearTwo.Enabled = true;
                txtRecordableCasesTwo.Enabled = true;
                txtTotalHours2.Enabled = true;
                trAttachOSHA2.Attributes["Style"] = @"display: ";   //008

            }
            else
            {
                txtNoofCasesFrmWorkTwo.Enabled = false;
                txtNoofCasesTransferTwo.Enabled = false;
                txtNoofDeathsYearTwo.Enabled = false;
                txtRecordableCasesTwo.Enabled = false;
                txtTotalHours2.Enabled = false;
                trAttachOSHA2.Attributes["Style"] = @"display: none";   //008
            }

            chkNA5.Enabled = true;
            txtExplain5.Enabled = true;

            //008
            if (!string.IsNullOrEmpty(objOsha1.FilePath))
            {
                imbAttachOSHA2.Visible = true;
                lnkAttachOSHA2.Visible = false;
                starAttachOSHA2.Visible = false;
            }
            else
            {
                imbAttachOSHA2.Visible = false;
                lnkAttachOSHA2.Visible = true;
                starAttachOSHA2.Visible = !chkNoAttachment2.Checked;
            }

            //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1)) || (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
            //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1)) || (Convert.ToInt32(hdnVendorStatus.Value) > 0)) && objOsha1.PastYearStatus == 0)
            //{

            //    txtNoofCasesFrmWorkTwo.Enabled = false;
            //    txtNoofCasesTransferTwo.Enabled = false;
            //    txtNoofDeathsYearTwo.Enabled = false;
            //    txtRecordableCasesTwo.Enabled = false;
            //    chkNA5.Enabled = false;
            //    txtExplain5.Enabled = false;
            //    txtTotalHours2.Enabled = false;

            //}
            //else
            //{

            //    txtNoofCasesFrmWorkTwo.Enabled = true;
            //    txtNoofCasesTransferTwo.Enabled = true;
            //    txtNoofDeathsYearTwo.Enabled = true;
            //    txtRecordableCasesTwo.Enabled = true;
            //    chkNA5.Enabled = true;
            //    txtExplain5.Enabled = true;
            //    txtTotalHours2.Enabled = true;

            //}

        }
        #endregion

        #region OSHA Year 3
        objOsha1 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearThree.Text.Trim());
        if (objOsha1 != null)
        {
            // G. Vera Phase 3 Modifications Item 1.3
            //001
            if (!chkNA6.Checked)
            {
                txtNoofCasesFrmWorkThree.Enabled = true;
                txtNoofCasesTransferThree.Enabled = true;
                txtNoofDeathsYearThree.Enabled = true;
                txtRecordableCasesThree.Enabled = true;
                txtTotalHours3.Enabled = true;
                trAttachOSHA3.Attributes["Style"] = @"display: ";   //008
            }
            else
            {
                txtNoofCasesFrmWorkThree.Enabled = false;
                txtNoofCasesTransferThree.Enabled = false;
                txtNoofDeathsYearThree.Enabled = false;
                txtRecordableCasesThree.Enabled = false;
                txtTotalHours3.Enabled = false;
                trAttachOSHA3.Attributes["Style"] = @"display: none";   //008
            }

            chkNA6.Enabled = true;
            txtExplain6.Enabled = true;

            //008
            if (!string.IsNullOrEmpty(objOsha1.FilePath))
            {
                imbAttachOSHA3.Visible = true;
                lnkAttachOSHA3.Visible = false;
                starAttachOSHA3.Visible = false;
            }
            else
            {
                imbAttachOSHA3.Visible = false;
                lnkAttachOSHA3.Visible = true;
                starAttachOSHA3.Visible = !chkNoAttachment3.Checked;
            }

            //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1)) || (Convert.ToInt32(hdnVendorStatus.Value) > 0)) && objOsha1.PastYearStatus == 0)
            //    {
            //        txtNoofCasesFrmWorkThree.Enabled = false;
            //        txtNoofCasesTransferThree.Enabled = false;
            //        txtNoofDeathsYearThree.Enabled = false;
            //        txtRecordableCasesThree.Enabled = false;
            //        chkNA6.Enabled = false;
            //        txtExplain6.Enabled = false;
            //        txtTotalHours3.Enabled = false;

            //    }
            //    else
            //    {

            //        txtNoofCasesFrmWorkThree.Enabled = true;
            //        txtNoofCasesTransferThree.Enabled = true;
            //        txtNoofDeathsYearThree.Enabled = true;
            //        txtRecordableCasesThree.Enabled = true;
            //        chkNA6.Enabled = true;
            //        txtExplain6.Enabled = true;
            //        txtTotalHours3.Enabled = true;

            //    }

        }
        #endregion
        //007
        #region OSHA Year 4
        objOsha1 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearFour.Text.Trim());
        if (objOsha1 != null)
        {
            // G. Vera Phase 3 Modifications Item 1.3
            //001
            if (!chkNA7.Checked)
            {
                txtNoofCasesFrmWorkFour.Enabled = true;
                txtNoofCasesTransferFour.Enabled = true;
                txtNoofDeathsYearFour.Enabled = true;
                txtRecordableCasesFour.Enabled = true;
                txtTotalHours4.Enabled = true;
                trAttachOSHA4.Attributes["Style"] = @"display: ";   //008
            }
            else
            {
                txtNoofCasesFrmWorkFour.Enabled = false;
                txtNoofCasesTransferFour.Enabled = false;
                txtNoofDeathsYearFour.Enabled = false;
                txtRecordableCasesFour.Enabled = false;
                txtTotalHours4.Enabled = false;
                trAttachOSHA4.Attributes["Style"] = @"display: none";   //008
            }

            chkNA7.Enabled = true;
            txtExplain7.Enabled = true;

            //008
            if (!string.IsNullOrEmpty(objOsha1.FilePath))
            {
                imbAttachOSHA4.Visible = true;
                lnkAttachOSHA4.Visible = false;
                starAttachOSHA4.Visible = false;
            }
            else
            {
                imbAttachOSHA4.Visible = false;
                lnkAttachOSHA4.Visible = true;
                starAttachOSHA4.Visible = chkNoAttachment4.Checked;
            }

        }
        #endregion
    }


    /// <summary>
    /// Display the panel based on the sub menu selected
    /// </summary>
    private void ShowHideControls()
    {
        if (Request.QueryString.Count > 0)
        {
            if (Request.QueryString["submenu"] == "1")
            {
                accSafety.SelectedIndex = 0;
            }
            else if (Request.QueryString["submenu"] == "2")
            {
                accSafety.SelectedIndex = 1;
            }
            else if (Request.QueryString["submenu"] == "3")
            {
                accSafety.SelectedIndex = 2;
            }
        }
    }


    /// <summary>
    /// Call scripts for menus
    /// </summary>


    private void CallScripts()
    {
        txtRateOne.Attributes.Add("onBlur", "javascript:numberRateFormat(this.value,'','" + txtRateOne.ClientID + "');validateRateRange('" + txtRateOne.ClientID + "','one')");
        txtRateTwo.Attributes.Add("onBlur", "javascript:numberRateFormat(this.value,'','" + txtRateTwo.ClientID + "');validateRateRange('" + txtRateTwo.ClientID + "','two')");
        txtRateThree.Attributes.Add("onBlur", "javascript:numberRateFormat(this.value,'','" + txtRateThree.ClientID + "');validateRateRange('" + txtRateThree.ClientID + "','three')");
        //003
        txtRateFour.Attributes.Add("onBlur", "javascript:numberRateFormat(this.value,'','" + txtRateFour.ClientID + "');validateRateRange('" + txtRateFour.ClientID + "','four')");

        chkNA1.Attributes.Add("onClick", "javascript:DisplayExplain(this.checked,'" + txtRateOne.ClientID + "','" + trExplain1.ClientID + "', '" + txtExplanation1.ClientID + "');DisplayYear4(this.checked,'" + trEMRYEar4Field.ClientID + "');DisplayAttachment('" + trAttachEMR1.ClientID + "',this.checked)");    //003, 007, 008
        chkNA2.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + txtRateTwo.ClientID + "','" + trExplain2.ClientID + "', '" + txtExplanation2.ClientID + "');DisplayAttachment('" + trAttachEMR2.ClientID + "',this.checked)");   //008
        chkNA3.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + txtRateThree.ClientID + "','" + trExplain3.ClientID + "', '" + txtExplanation3.ClientID + "');DisplayAttachment('" + trAttachEMR3.ClientID + "',this.checked)"); //008
        chkSICNA.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + txtSIC.ClientID + "','" + trSICExplain.ClientID + "', '" + txtSICExplain.ClientID + "');");
        //003
        chkEMRNA4.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + txtRateFour.ClientID + "','" + trExplainEMR4.ClientID + "', '" + txtExplanation4.ClientID + "');DisplayAttachment('" + trAttachEMR4.ClientID + "',this.checked)");    //008


        chkNA4.Attributes.Add("onClick", "DisplayOSHAExplain(this.checked,'" + txtNoofDeathsYearOne.ClientID + "','" + txtNoofCasesFrmWorkOne.ClientID + "','" + txtNoofCasesTransferOne.ClientID + "','" + txtRecordableCasesOne.ClientID + "','" + trExplain4.ClientID + "', '" + txtExplain4.ClientID + "','" + txtTotalHours1.ClientID + "');DisplayYear4(this.checked,'" + pnlOSHAYear4.ClientID + "');DisplayAttachment('" + trAttachOSHA1.ClientID + "',this.checked);DisplayAttachment('" + tdNoAttachment1.ClientID + "',this.checked)");  //007, 008
        chkNA5.Attributes.Add("onClick", "DisplayOSHAExplain(this.checked,'" + txtNoofDeathsYearTwo.ClientID + "','" + txtNoofCasesFrmWorkTwo.ClientID + "','" + txtNoofCasesTransferTwo.ClientID + "','" + txtRecordableCasesTwo.ClientID + "','" + trExplain5.ClientID + "', '" + txtExplain5.ClientID + "','" + txtTotalHours2.ClientID + "');DisplayAttachment('" + trAttachOSHA2.ClientID + "',this.checked);DisplayAttachment('" + tdNoAttachment2.ClientID + "',this.checked)");    //008
        chkNA6.Attributes.Add("onClick", "DisplayOSHAExplain(this.checked,'" + txtNoofDeathsYearThree.ClientID + "','" + txtNoofCasesFrmWorkThree.ClientID + "','" + txtNoofCasesTransferThree.ClientID + "','" + txtRecordableCasesThree.ClientID + "','" + trExplain6.ClientID + "', '" + txtExplain6.ClientID + "','" + txtTotalHours3.ClientID + "');DisplayAttachment('" + trAttachOSHA3.ClientID + "',this.checked);DisplayAttachment('" + tdNoAttachment3.ClientID + "',this.checked)");    //008
        //007
        chkNA7.Attributes.Add("onClick", "DisplayOSHAExplain(this.checked,'" + txtNoofDeathsYearFour.ClientID + "','" + txtNoofCasesFrmWorkFour.ClientID + "','" + txtNoofCasesTransferFour.ClientID + "','" + txtRecordableCasesFour.ClientID + "','" + trExplain7.ClientID + "', '" + txtExplain7.ClientID + "','" + txtTotalHours4.ClientID + "');DisplayAttachment('" + trAttachOSHA4.ClientID + "',this.checked);DisplayAttachment('" + tdNoAttachment4.ClientID + "',this.checked)");    //007, 008

        txtRateOne.Attributes.Add("onkeypress", "javascript:numberEMRFormat(this.value,'','" + txtRateOne.ClientID + "')");
        txtRateTwo.Attributes.Add("onkeypress", "javascript:numberEMRFormat(this.value,'','" + txtRateTwo.ClientID + "')");
        txtRateThree.Attributes.Add("onkeypress", "javascript:numberEMRFormat(this.value,'','" + txtRateThree.ClientID + "')");
        //003
        txtRateFour.Attributes.Add("onkeypress", "javascript:numberEMRFormat(this.value,'','" + txtRateFour.ClientID + "')");


        txtTotalHours1.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtTotalHours1.ClientID + "')");
        txtTotalHours2.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtTotalHours2.ClientID + "')");
        txtTotalHours3.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtTotalHours3.ClientID + "')");
        //007
        txtTotalHours4.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtTotalHours4.ClientID + "')");


        txtTotalHours1.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours1.ClientID + "')");
        txtTotalHours2.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours2.ClientID + "')");
        txtTotalHours3.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours3.ClientID + "')");
        //007
        txtTotalHours4.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours4.ClientID + "')");

        txtNoofCasesFrmWorkOne.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkOne.ClientID + "')");
        txtNoofCasesFrmWorkTwo.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkTwo.ClientID + "')");
        txtNoofCasesFrmWorkThree.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkThree.ClientID + "')");
        //007
        txtNoofCasesFrmWorkFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkFour.ClientID + "')");

        txtNoofCasesTransferOne.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferOne.ClientID + "')");
        txtNoofCasesTransferTwo.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferTwo.ClientID + "')");
        txtNoofCasesTransferThree.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferThree.ClientID + "')");
        //007
        txtNoofCasesTransferFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferFour.ClientID + "')");

        txtNoofDeathsYearOne.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearOne.ClientID + "')");
        txtNoofDeathsYearTwo.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearTwo.ClientID + "')");
        txtNoofDeathsYearThree.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearThree.ClientID + "')");
        //007
        txtNoofDeathsYearFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearFour.ClientID + "')");
        // txtTotalHours1.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkOne.ClientID + "')");

        txtRecordableCasesOne.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtRecordableCasesOne.ClientID + "')");
        txtRecordableCasesTwo.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtRecordableCasesTwo.ClientID + "')");
        txtRecordableCasesThree.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtRecordableCasesThree.ClientID + "')");
        //007
        txtRecordableCasesFour.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtRecordableCasesFour.ClientID + "')");

        txtNoofCasesFrmWorkOne.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkOne.ClientID + "')");
        txtNoofCasesFrmWorkTwo.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkTwo.ClientID + "')");
        txtNoofCasesFrmWorkThree.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkThree.ClientID + "')");
        //007
        txtNoofCasesFrmWorkFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkFour.ClientID + "')");

        txtNoofCasesTransferOne.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferOne.ClientID + "')");
        txtNoofCasesTransferTwo.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferTwo.ClientID + "')");
        txtNoofCasesTransferThree.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferThree.ClientID + "')");
        //007
        txtNoofCasesTransferFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferFour.ClientID + "')");

        txtNoofDeathsYearOne.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearOne.ClientID + "')");
        txtNoofDeathsYearTwo.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearTwo.ClientID + "')");
        txtNoofDeathsYearThree.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearThree.ClientID + "')");
        //007
        txtNoofDeathsYearFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearFour.ClientID + "')");

        txtRecordableCasesOne.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtRecordableCasesOne.ClientID + "')");
        txtRecordableCasesTwo.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtRecordableCasesTwo.ClientID + "')");
        txtRecordableCasesThree.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtRecordableCasesThree.ClientID + "')");
        //007
        txtRecordableCasesFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtRecordableCasesFour.ClientID + "')");

        //txtTotalHours1.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtTotalHours1.ClientID + "')");
        //txtTotalHours2.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtTotalHours2.ClientID + "')");
        //txtTotalHours3.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtTotalHours3.ClientID + "')");


        //txtTotalHours1.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours1.ClientID + "')");
        //txtTotalHours2.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours2.ClientID + "')");
        //txtTotalHours3.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours3.ClientID + "')");


        txtMainPhno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtMainPhno1.ClientID + "," + txtMainPhno2.ClientID + ")");
        txtMainPhno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtMainPhno2.ClientID + "," + txtMainPhno3.ClientID + ")");

        //
        //txtRateOne.Attributes.Add("onblur", "javascript:validateRateRange('" + txtRateOne.ClientID + "')");
        //txtRateTwo.Attributes.Add("onblur", "javascript:validateRateRange('" + txtRateTwo.ClientID + "')");
        //txtRateThree.Attributes.Add("onblur", "javascript:validateRateRange('" + txtRateThree.ClientID + "')");

        //006 - Sooraj --Internationalization for Phone 
        chkContactUSA.Attributes.Add("OnClick", "SwitchPhone(this,'" + txtMainPhno1.ClientID + "','" + txtMainPhno2.ClientID + "','" + txtMainPhno3.ClientID + "','" + spnCPhone2.ClientID + "')");
        chkReperesentUSA.Attributes.Add("OnClick", "SwitchPhone(this,'" + txtRepresentationContactNumberOne.ClientID + "','" + txtRepresentationContactNumberTwo.ClientID + "','" + txtRepresentationContactNumberThree.ClientID + "','" + spnRepresentPhone.ClientID + "')");

    }

    #endregion

    #region Button Events
    /// <summary>
    /// Save and Next
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSaveandNext_Click(object sender, ImageClickEventArgs e)
    {
        
        VendorId = Convert.ToInt64(Session["VendorId"]);
        this.DisableOSHAYear4Validators(chkNA4.Checked); //007
        this.DisableEMRYear4Validators(chkNA1.Checked);  //007
        bool shouldSendEmail = false;
        if (!string.IsNullOrEmpty(ViewState["SubmittedYear"] as String))
        {
            shouldSendEmail = (Convert.ToInt32(ViewState["SubmittedYear"].ToString()) >= Convert.ToInt32(txtYearOne.Text.Trim()));        //010, 012
        }

        Page.Validate("Current");
        if (Page.IsValid)
        {

            Page.Validate("Osha");
            if (Page.IsValid)
            {
                Page.Validate("Questionair");
                if (Page.IsValid)
                {
                    //013 - START
                    #region Archived
                    //SafetyRepresentation = objCommon.ReturnCheckedStatus(rdoRepresentation); //005 

                    ////Sooraj Modified for radio button checked status to avoid redundant code

                    //SafetyProgram = objCommon.ReturnCheckedStatus(rdoSafetyProgram);

                    //CompanySafety = objCommon.ReturnCheckedStatus(rdoSafetyDirector);

                    //SafetyRequirements = objCommon.ReturnCheckedStatus(rdoSafetytraining);

                    ////Sooraj Commented 

                    ////  if (rdoSafetyProgram.SelectedIndex == -1)
                    ////{
                    ////    SafetyProgram = null;
                    ////}
                    ////else if (rdoSafetyProgram.SelectedIndex == 0)
                    ////{
                    ////    SafetyProgram = true;
                    ////}
                    ////else if (rdoSafetyProgram.SelectedIndex == 1)
                    ////{
                    ////    SafetyProgram = false;
                    ////}

                    ////if (rdoSafetyDirector.SelectedIndex == -1)
                    ////{
                    ////    CompanySafety = null;
                    ////}
                    ////else if (rdoSafetyDirector.SelectedIndex == 0)
                    ////{
                    ////    CompanySafety = true;
                    ////}
                    ////else if (rdoSafetyDirector.SelectedIndex == 1)
                    ////{
                    ////    CompanySafety = false;
                    ////}

                    ////if (rdoSafetytraining.SelectedIndex == -1)
                    ////{
                    ////    SafetyRequirements = null;
                    ////}
                    ////else if (rdoSafetytraining.SelectedIndex == 0)
                    ////{
                    ////    SafetyRequirements = true;
                    ////}
                    ////else if (rdoSafetytraining.SelectedIndex == 1)
                    ////{
                    ////    SafetyRequirements = false;
                    ////}


                    //string MainPhone = objCommon.Phoneformat(txtMainPhno1.Text.Trim(), txtMainPhno2.Text.Trim(), txtMainPhno3.Text.Trim(), txtMainPhno3);//006
                    //string RepPhone = objCommon.CheckPhone(txtRepresentationContactNumberOne.Text.Trim(), txtRepresentationContactNumberTwo.Text.Trim(), txtRepresentationContactNumberThree.Text.Trim(), txtRepresentationContactNumberThree); //005




                    //int CountSafety = objSafety.GetSafetyCount(VendorId);
                    //if (CountSafety == 0)
                    //{
                    //    SafetyStatus = 1;
                    //    Result = objSafety.InsertSafety(CompanySafety, txtContactName.Text.Trim(), MainPhone, SafetyProgram, SafetyRequirements, Convert.ToInt64(Session["VendorId"]), SafetyStatus, txtSIC.Text.Trim(), chkSICNA.Checked, txtSICExplain.Text, SafetyRepresentation, txtRepresentationContactName.Text, RepPhone, !chkContactUSA.Checked,!chkReperesentUSA.Checked);//005

                    //    objSafety.insertSafetyEMR(txtRateOne.Text.Trim(), txtYearOne.Text.Trim(), chkNA1.Checked, txtExplanation1.Text.Trim());

                    //    if (shouldSendEmail) this.SendYear1EMRChangesToAdmin(txtRateOne.Text.Trim(), txtExplanation1.Text.Trim(), VendorId);     //010, 012

                    //    objSafety.insertSafetyEMR(txtRateTwo.Text.Trim(), txtYearTwo.Text.Trim(), chkNA2.Checked, txtExplanation2.Text.Trim());
                    //    objSafety.insertSafetyEMR(txtRateThree.Text.Trim(), txtYearThree.Text.Trim(), chkNA3.Checked, txtExplanation3.Text.Trim());
                    //    //003
                    //    objSafety.insertSafetyEMR(txtRateFour.Text.Trim(), txtYearFour.Text.Trim(), chkEMRNA4.Checked, txtExplanation4.Text.Trim());

                    //    objSafety.InsertSafetyOsha(txtOSHAYearOne.Text.Trim(), txtNoofDeathsYearOne.Text.Trim(), txtNoofCasesFrmWorkOne.Text.Trim(), txtNoofCasesTransferOne.Text.Trim(), txtRecordableCasesOne.Text.Trim(), chkNA4.Checked, txtExplain4.Text.Trim(), txtTotalHours1.Text.Trim(), !chkNoAttachment1.Checked);
                    //    objSafety.InsertSafetyOsha(txtOSHAYearTwo.Text.Trim(), txtNoofDeathsYearTwo.Text.Trim(), txtNoofCasesFrmWorkTwo.Text.Trim(), txtNoofCasesTransferTwo.Text.Trim(), txtRecordableCasesTwo.Text.Trim(), chkNA5.Checked, txtExplain5.Text.Trim(), txtTotalHours2.Text.Trim(), !chkNoAttachment3.Checked);
                    //    objSafety.InsertSafetyOsha(txtOSHAYearThree.Text.Trim(), txtNoofDeathsYearThree.Text.Trim(), txtNoofCasesFrmWorkThree.Text.Trim(), txtNoofCasesTransferThree.Text.Trim(), txtRecordableCasesThree.Text.Trim(), chkNA6.Checked, txtExplain6.Text.Trim(), txtTotalHours3.Text.Trim(), !chkNoAttachment3.Checked);
                    //    //007
                    //    objSafety.InsertSafetyOsha(txtOSHAYearFour.Text.Trim(), txtNoofDeathsYearFour.Text.Trim(), txtNoofCasesFrmWorkFour.Text.Trim(), txtNoofCasesTransferFour.Text.Trim(), txtRecordableCasesFour.Text.Trim(), chkNA7.Checked, txtExplain7.Text.Trim(), txtTotalHours4.Text.Trim(), !chkNoAttachment4.Checked);

                    //    modalExtnd.Show();
                    //}
                    //else
                    //{
                    //    SafetyStatus = 2;
                    //    if (!string.IsNullOrEmpty(hdnSafetyId.Value))
                    //    {
                    //        SafetyId = Convert.ToInt64(hdnSafetyId.Value);
                    //    }
                    //    else
                    //    {
                    //        SafetyId = Convert.ToInt64(ViewState["SafetyID"]);
                    //    }
                    //        Result = objSafety.UpdateSafety(CompanySafety, txtContactName.Text.Trim(), MainPhone, SafetyProgram, SafetyRequirements, SafetyId, SafetyStatus, txtSIC.Text.Trim(), chkSICNA.Checked, txtSICExplain.Text, SafetyRepresentation, txtRepresentationContactName.Text, RepPhone, !chkContactUSA.Checked,!chkReperesentUSA.Checked);//005

                    //        objSafety.UpdateSafetyEMR(txtRateOne.Text.Trim(), txtYearOne.Text.Trim(), chkNA1.Checked, txtExplanation1.Text.Trim());

                    //        if (shouldSendEmail) this.SendYear1EMRChangesToAdmin(txtRateOne.Text.Trim(), txtExplanation1.Text.Trim(), VendorId);     //010, 012

                    //        objSafety.UpdateSafetyEMR(txtRateTwo.Text.Trim(), txtYearTwo.Text.Trim(), chkNA2.Checked, txtExplanation2.Text.Trim());   
                    //        objSafety.UpdateSafetyEMR(txtRateThree.Text.Trim(), txtYearThree.Text.Trim(), chkNA3.Checked, txtExplanation3.Text.Trim());
                    //        //003
                    //        objSafety.UpdateSafetyEMR(txtRateFour.Text.Trim(), txtYearFour.Text.Trim(), chkEMRNA4.Checked, txtExplanation4.Text.Trim());

                    //        objSafety.UpdateSafetyOsha(txtOSHAYearOne.Text.Trim(), txtNoofDeathsYearOne.Text.Trim(), txtNoofCasesFrmWorkOne.Text.Trim(), txtNoofCasesTransferOne.Text.Trim(), txtRecordableCasesOne.Text.Trim(), chkNA4.Checked, txtExplain4.Text.Trim(), txtTotalHours1.Text.Trim(), !chkNoAttachment1.Checked);
                    //        objSafety.UpdateSafetyOsha(txtOSHAYearTwo.Text.Trim(), txtNoofDeathsYearTwo.Text.Trim(), txtNoofCasesFrmWorkTwo.Text.Trim(), txtNoofCasesTransferTwo.Text.Trim(), txtRecordableCasesTwo.Text.Trim(), chkNA5.Checked, txtExplain5.Text.Trim(), txtTotalHours2.Text.Trim(), !chkNoAttachment2.Checked);
                    //        objSafety.UpdateSafetyOsha(txtOSHAYearThree.Text.Trim(), txtNoofDeathsYearThree.Text.Trim(), txtNoofCasesFrmWorkThree.Text.Trim(), txtNoofCasesTransferThree.Text.Trim(), txtRecordableCasesThree.Text.Trim(), chkNA6.Checked, txtExplain6.Text.Trim(), txtTotalHours3.Text.Trim(), !chkNoAttachment3.Checked);
                    //        //007
                    //        objSafety.UpdateSafetyOsha(txtOSHAYearFour.Text.Trim(), txtNoofDeathsYearFour.Text.Trim(), txtNoofCasesFrmWorkFour.Text.Trim(), txtNoofCasesTransferFour.Text.Trim(), txtRecordableCasesFour.Text.Trim(), chkNA7.Checked, txtExplain7.Text.Trim(), txtTotalHours4.Text.Trim(), !chkNoAttachment4.Checked);
                    //        lblError.Text = "";
                    //}

                    ////Get Safety ID
                    //if (Result == true)
                    //{
                    //    VQFMenu.LastUpdatedDate(Convert.ToInt32(Session["VendorId"]), DateTime.Now);    //011
                    //    DAL.VQFSafety EditSafetyData = objSafety.GetSingleVQLSafetyData(VendorId);
                    //    hdnSafetyId.Value = Convert.ToString(EditSafetyData.PK_SafetyID);
                    //    ViewState["SafetyID"] = Convert.ToString(EditSafetyData.PK_SafetyID);   //GV - 11/17/2014
                    //    SafetyId = Convert.ToInt64(hdnSafetyId.Value);
                    //    
                    //} 
                    #endregion
                    //013 - END
                    imbSaveandClose_Click(this.imbSaveandNext, null);  //008 need to save anyway
                    Response.Redirect("References.aspx");

                }
                else
                {
                    lblheading.Text = "Questionnaire - Validation";
                    lblMsg.Text = "";
                    lblError.Text = Errormsg();
                    imbSaveandClose_Click(this.imbSaveandNext, null);  //008 need to save anyway
                    ControlVisiblity();
                    modalExtnd.Show(); 
                    accSafety.SelectedIndex = 2;

                }
            }
            else
            {

                lblheading.Text = "OSHA 200/300 - Validation";
                lblMsg.Text = "";
                lblError.Text = Errormsg();
                imbSaveandClose_Click(this.imbSaveandNext, null);  //008 need to save anyway
                modalExtnd.Show();
                accSafety.SelectedIndex = 1;
            }
        }


        else
        {
            lblheading.Text = "Current EMR Rates - Validation";
            lblMsg.Text = "";
            lblError.Text = Errormsg();
            imbSaveandClose_Click(this.imbSaveandNext, null);  //008 need to save anyway
            modalExtnd.Show();
            accSafety.SelectedIndex = 0;
        }

        
        //LoadEditDatas(true);
        

    }


    /// <summary>
    /// Save and close
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSaveandClose_Click(object sender, ImageClickEventArgs e)
    {
        string MainPhone = string.Empty;
        string RepPhone = string.Empty; //005
        VendorId = Convert.ToInt64(Session["VendorId"]);
        this.DisableOSHAYear4Validators(chkNA4.Checked); //007
        this.DisableEMRYear4Validators(chkNA1.Checked);  //007
        bool shouldSendEmail = false;

        //013 - START
        string levelOfTraining = txtProLevelOfTraining.Text;
        bool? isProvideDocumentation = (rdoListEmployeeProgramDocumentation.SelectedItem != null) ? rdoListEmployeeProgramDocumentation.SelectedItem.Text.ConvertToBool() : new bool?();  
        bool? isDrugAlcohol = (rdoListDrugAlcohol.SelectedItem != null) ? rdoListDrugAlcohol.SelectedItem.Text.ConvertToBool() :  new bool?();  
        bool? isSafetyMeetingsHeld = (rdoListSafetyMeetings.SelectedItem != null) ? rdoListSafetyMeetings.SelectedItem.Text.ConvertToBool() : new bool?();  
        string safetyMeetingsPeriods = (chkSafetyMeetings.SelectedIndex >= 0) ?
                                        string.Join("|", chkSafetyMeetings.Items.Cast<ListItem>().Where(c => c.Selected).Select(cc => cc.Text).ToArray<string>()) :
                                        string.Empty;   
        bool? isInspectionWIP = (rdoListInspectionWIP.SelectedItem != null) ? rdoListInspectionWIP.SelectedItem.Text.ConvertToBool() : new bool?();  
        string whoConductsInspectionWIP = txtWhoInspectionWIP.Text;  
        string inspectionPeriods = (chkInspectionWIP.SelectedIndex >= 0) ?
                                    string.Join("|", chkInspectionWIP.Items.Cast<ListItem>().Where(c => c.Selected).Select(cc => cc.Text).ToArray<string>()) :
                                    string.Empty;

        bool? isHazardAnalysis = (rdoHazardAnalysis.SelectedItem != null) ? rdoHazardAnalysis.SelectedItem.Text.ConvertToBool() : new bool?();
        string hazardPeriods = (chkHazardAnalysisPeriods.SelectedIndex >= 0) ?
                                    string.Join("|", chkHazardAnalysisPeriods.Items.Cast<ListItem>().Where(c => c.Selected).Select(cc => cc.Text).ToArray<string>()) :
                                    string.Empty;
        //013 - END

        


        if (!string.IsNullOrEmpty(ViewState["SubmittedYear"] as String))
        {
            shouldSendEmail = (Convert.ToInt32(ViewState["SubmittedYear"].ToString()) >= Convert.ToInt32(txtYearOne.Text.Trim()));        //010, 012
        }

        //int State1 = 0;
        //int State2 = 0;
        //int State3 = 0;

        //Sooraj Commented 

        //if (rdoSafetyProgram.SelectedIndex == -1)
        //{
        //    SafetyProgram = null;
        //}
        //else if (rdoSafetyProgram.SelectedIndex == 0)
        //{
        //    SafetyProgram = true;
        //}
        //else if (rdoSafetyProgram.SelectedIndex == 1)
        //{
        //    SafetyProgram = false;
        //}

        //if (rdoSafetyDirector.SelectedIndex == -1)
        //{
        //    CompanySafety = null;
        //}
        //else if (rdoSafetyDirector.SelectedIndex == 0)
        //{
        //    CompanySafety = true;
        //}
        //else if (rdoSafetyDirector.SelectedIndex == 1)
        //{
        //    CompanySafety = false;
        //}

        //if (rdoSafetytraining.SelectedIndex == -1)
        //{
        //    SafetyRequirements = null;
        //}
        //else if (rdoSafetytraining.SelectedIndex == 0)
        //{
        //    SafetyRequirements = true;
        //}
        //else if (rdoSafetytraining.SelectedIndex == 1)
        //{
        //    SafetyRequirements = false;
        //}



        SafetyRepresentation = objCommon.ReturnCheckedStatus(rdoRepresentation); //005 

        //Sooraj Modified for radio button checked status to avoid redundant code

        SafetyProgram = objCommon.ReturnCheckedStatus(rdoSafetyProgram);

        CompanySafety = objCommon.ReturnCheckedStatus(rdoSafetyDirector);

        SafetyRequirements = objCommon.ReturnCheckedStatus(rdoSafetytraining);

        MainPhone = objCommon.CheckPhone(txtMainPhno1.Text.Trim(), txtMainPhno2.Text.Trim(), txtMainPhno3.Text.Trim(), txtMainPhno3);//006


        RepPhone = objCommon.CheckPhone(txtRepresentationContactNumberOne.Text.Trim(), txtRepresentationContactNumberTwo.Text.Trim(), txtRepresentationContactNumberThree.Text.Trim(), txtRepresentationContactNumberThree); //005

        


        int CountSafety = objSafety.GetSafetyCount(Convert.ToInt64(Session["VendorId"]));
        if (CountSafety == 0)
        {
            SafetyStatus = Convert.ToByte(this.IsPageValid());      //009
            Result = objSafety.InsertSafety(CompanySafety, txtContactName.Text.Trim(), MainPhone, SafetyProgram, SafetyRequirements, Convert.ToInt64(Session["VendorId"]), SafetyStatus, txtSIC.Text.Trim(),
                                            chkSICNA.Checked, txtSICExplain.Text, SafetyRepresentation, txtRepresentationContactName.Text, RepPhone, !chkContactUSA.Checked, !chkReperesentUSA.Checked,
                                            levelOfTraining, isProvideDocumentation, isDrugAlcohol,isSafetyMeetingsHeld, safetyMeetingsPeriods,isInspectionWIP, whoConductsInspectionWIP,inspectionPeriods,isHazardAnalysis, hazardPeriods);   //005, 013
            
            //if ((txtRateOne.Text.Trim() != "" || chkNA1.Checked))
            //{
            objSafety.insertSafetyEMR(txtRateOne.Text.Trim(), txtYearOne.Text.Trim(), chkNA1.Checked, txtExplanation1.Text.Trim());
            //}

            if (shouldSendEmail) this.SendYear1EMRChangesToAdmin(txtRateOne.Text.Trim(), txtExplanation1.Text.Trim(), VendorId);     //010, 012

            objSafety.insertSafetyEMR(txtRateTwo.Text.Trim(), txtYearTwo.Text.Trim(), chkNA2.Checked, txtExplanation2.Text.Trim());


            objSafety.insertSafetyEMR(txtRateThree.Text.Trim(), txtYearThree.Text.Trim(), chkNA3.Checked, txtExplanation3.Text.Trim());

            //003
            objSafety.insertSafetyEMR(txtRateFour.Text.Trim(), txtYearFour.Text.Trim(), chkEMRNA4.Checked, txtExplanation4.Text.Trim());

            //if ((txtNoofDeathsYearOne.Text.Trim() != "" || chkNA4.Checked))
            //{
            objSafety.InsertSafetyOsha(txtOSHAYearOne.Text.Trim(), txtNoofDeathsYearOne.Text.Trim(), txtNoofCasesFrmWorkOne.Text.Trim(), txtNoofCasesTransferOne.Text.Trim(), txtRecordableCasesOne.Text.Trim(), chkNA4.Checked, txtExplain4.Text.Trim(), txtTotalHours1.Text.Trim(), !chkNoAttachment1.Checked);
            //}

            objSafety.InsertSafetyOsha(txtOSHAYearTwo.Text.Trim(), txtNoofDeathsYearTwo.Text.Trim(), txtNoofCasesFrmWorkTwo.Text.Trim(), txtNoofCasesTransferTwo.Text.Trim(), txtRecordableCasesTwo.Text.Trim(), chkNA5.Checked, txtExplain5.Text.Trim(), txtTotalHours2.Text.Trim(), !chkNoAttachment2.Checked);


            objSafety.InsertSafetyOsha(txtOSHAYearThree.Text.Trim(), txtNoofDeathsYearThree.Text.Trim(), txtNoofCasesFrmWorkThree.Text.Trim(), txtNoofCasesTransferThree.Text.Trim(), txtRecordableCasesThree.Text.Trim(), chkNA6.Checked, txtExplain6.Text.Trim(), txtTotalHours3.Text.Trim(), !chkNoAttachment3.Checked);
            //007
            objSafety.InsertSafetyOsha(txtOSHAYearFour.Text.Trim(), txtNoofDeathsYearFour.Text.Trim(), txtNoofCasesFrmWorkFour.Text.Trim(), txtNoofCasesTransferFour.Text.Trim(), txtRecordableCasesFour.Text.Trim(), chkNA7.Checked, txtExplain7.Text.Trim(), txtTotalHours4.Text.Trim(), !chkNoAttachment4.Checked);
            
            //013 - START
            if (Result == true)
            {
                DAL.VQFSafety EditSafetyData = objSafety.GetSingleVQLSafetyData(VendorId);
                hdnSafetyId.Value = Convert.ToString(EditSafetyData.PK_SafetyID);
                ViewState["SafetyID"] = Convert.ToString(EditSafetyData.PK_SafetyID);
                SafetyId = Convert.ToInt64(hdnSafetyId.Value);

                #region Citations
                citation1 = objSafety.NewCitationRecord(SafetyId, CitationYear1);
                citation2 = objSafety.NewCitationRecord(SafetyId, CitationYear2);
                citation3 = objSafety.NewCitationRecord(SafetyId, CitationYear3);
                citation1.IsCitation = (rdoCitationYear1.SelectedIndex >= 0) ? (rdoCitationYear1.SelectedIndex == 0) : new bool?();
                citation2.IsCitation = (rdoCitationYear2.SelectedIndex >= 0) ? (rdoCitationYear2.SelectedIndex == 0) : new bool?();
                citation3.IsCitation = (rdoCitationYear3.SelectedIndex >= 0) ? (rdoCitationYear3.SelectedIndex == 0) : new bool?();
                citation1 = objSafety.UpdateCitations(citation1);
                citation2 = objSafety.UpdateCitations(citation2);
                citation3 = objSafety.UpdateCitations(citation3);
                #endregion
            }
            //013 - END


        }
        else
        {
            SafetyStatus = Convert.ToByte(this.IsPageValid());      //009
            if (!string.IsNullOrEmpty(hdnSafetyId.Value))
            {
                SafetyId = Convert.ToInt64(hdnSafetyId.Value);
            }
            else
            {
                SafetyId = Convert.ToInt64(ViewState["SafetyID"]);
            }
            Result = objSafety.UpdateSafety(CompanySafety, txtContactName.Text.Trim(), MainPhone, SafetyProgram, SafetyRequirements, SafetyId, SafetyStatus, txtSIC.Text.Trim(), chkSICNA.Checked, txtSICExplain.Text,
                                            SafetyRepresentation, txtRepresentationContactName.Text, RepPhone, !chkContactUSA.Checked, !chkReperesentUSA.Checked, levelOfTraining,
                                            isProvideDocumentation, isDrugAlcohol, isSafetyMeetingsHeld, safetyMeetingsPeriods, isInspectionWIP, whoConductsInspectionWIP, inspectionPeriods, isHazardAnalysis, hazardPeriods);   //005, 013
            //if ((txtRateOne.Text.Trim() != "" || chkNA1.Checked))
            //{
            objSafety.UpdateSafetyEMR(txtRateOne.Text.Trim(), txtYearOne.Text.Trim(), chkNA1.Checked, txtExplanation1.Text.Trim());
            //}

            if (shouldSendEmail) this.SendYear1EMRChangesToAdmin(txtRateOne.Text.Trim(), txtExplanation1.Text.Trim(), VendorId);     //010, 012

            objSafety.UpdateSafetyEMR(txtRateTwo.Text.Trim(), txtYearTwo.Text.Trim(), chkNA2.Checked, txtExplanation2.Text.Trim());


            objSafety.UpdateSafetyEMR(txtRateThree.Text.Trim(), txtYearThree.Text.Trim(), chkNA3.Checked, txtExplanation3.Text.Trim());
            //003
            objSafety.UpdateSafetyEMR(txtRateFour.Text.Trim(), txtYearFour.Text.Trim(), chkEMRNA4.Checked, txtExplanation4.Text.Trim());

            //if ((txtNoofDeathsYearOne.Text.Trim() != "" || chkNA4.Checked))
            //{
            objSafety.UpdateSafetyOsha(txtOSHAYearOne.Text.Trim(), txtNoofDeathsYearOne.Text.Trim(), txtNoofCasesFrmWorkOne.Text.Trim(), txtNoofCasesTransferOne.Text.Trim(), txtRecordableCasesOne.Text.Trim(), chkNA4.Checked, txtExplain4.Text.Trim(), txtTotalHours1.Text.Trim(), !chkNoAttachment1.Checked);
            //}

            objSafety.UpdateSafetyOsha(txtOSHAYearTwo.Text.Trim(), txtNoofDeathsYearTwo.Text.Trim(), txtNoofCasesFrmWorkTwo.Text.Trim(), txtNoofCasesTransferTwo.Text.Trim(), txtRecordableCasesTwo.Text.Trim(), chkNA5.Checked, txtExplain5.Text.Trim(), txtTotalHours2.Text.Trim(), !chkNoAttachment2.Checked);


            objSafety.UpdateSafetyOsha(txtOSHAYearThree.Text.Trim(), txtNoofDeathsYearThree.Text.Trim(), txtNoofCasesFrmWorkThree.Text.Trim(), txtNoofCasesTransferThree.Text.Trim(), txtRecordableCasesThree.Text.Trim(), chkNA6.Checked, txtExplain6.Text.Trim(), txtTotalHours3.Text.Trim(), !chkNoAttachment3.Checked);

            //007
            objSafety.UpdateSafetyOsha(txtOSHAYearFour.Text.Trim(), txtNoofDeathsYearFour.Text.Trim(), txtNoofCasesFrmWorkFour.Text.Trim(), txtNoofCasesTransferFour.Text.Trim(), txtRecordableCasesFour.Text.Trim(), chkNA7.Checked, txtExplain7.Text.Trim(), txtTotalHours4.Text.Trim(), !chkNoAttachment4.Checked);

            //013 - START
            citation1.IsCitation = (rdoCitationYear1.SelectedIndex >= 0) ? (rdoCitationYear1.SelectedIndex == 0) : new bool?();
            citation2.IsCitation = (rdoCitationYear2.SelectedIndex >= 0) ? (rdoCitationYear2.SelectedIndex == 0) : new bool?();
            citation3.IsCitation = (rdoCitationYear3.SelectedIndex >= 0) ? (rdoCitationYear3.SelectedIndex == 0) : new bool?();
            citation1 = objSafety.UpdateCitations(citation1);
            citation2 = objSafety.UpdateCitations(citation2);
            citation3 = objSafety.UpdateCitations(citation3);
            //013 - END

        }


        

        //Get Safety ID
        if (Result == true)
        {
            DAL.VQFSafety EditSafetyData = objSafety.GetSingleVQLSafetyData(VendorId);
            hdnSafetyId.Value = Convert.ToString(EditSafetyData.PK_SafetyID);
            ViewState["SafetyID"] = Convert.ToString(EditSafetyData.PK_SafetyID);   //GV - 11/17/2014

            SafetyId = Convert.ToInt64(hdnSafetyId.Value);
            //imbPrint.Visible = false;
            VQFMenu.GetCompleteStatus();
            VQFMenu.GetIncompleteStatus();
            VQFMenu.LastUpdatedDate(Convert.ToInt32(Session["VendorId"]), DateTime.Now);        //011

            //010 - start
            if (e != null)
            {
                lblError.Text = string.Empty;
                lblMsg.Text = "&nbsp;<li>Safety details has been saved successfully</li>";
                lblheading.Text = "Result";

                modalExtnd.Show();
            }
            //010 - end
        }
        LoadEditDatas(true);
        ControlVisiblity();

    }


    /// <summary>
    /// Disbale all the validation for OSHA YEar 4 based on N/A checked
    /// </summary>
    /// <param name="isChecked"></param>
    private void DisableOSHAYear4Validators(bool isChecked)
    {
            cusvOSHAYearFour.Enabled = isChecked;
            CustomValidator2.Enabled = isChecked;
            CustomValidator3.Enabled = isChecked;
            CustomValidator4.Enabled = isChecked;
            CustomValidator5.Enabled = isChecked;
    }


    /// <summary>
    /// Disbale all the validation for EMR Year 4 based on N/A checked
    /// </summary>
    /// <param name="isChecked"></param>
    private void DisableEMRYear4Validators(bool isChecked)
    {
        cusvYearFour.Enabled = isChecked;
    }


    /// <summary>
    /// 009 added to be called from save and close button event
    /// </summary>
    /// <returns></returns>
    private bool IsPageValid()
    {
        Page.Validate("Current");
        if (Page.IsValid)
        {

            Page.Validate("Osha");
            if (Page.IsValid)
            {
                Page.Validate("Questionair");

                return Page.IsValid;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    #endregion

    #region Other Errormsg

    /// <summary>
    /// validate page and list the errors
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {

            if (!validator.IsValid)
            {
                //013 - Corrected format
                if (!validator.ErrorMessage.Contains("<li>")) errmsg += "<li>" + validator.ErrorMessage + "</li>";
                else errmsg += validator.ErrorMessage;
            }
        }
        return errmsg;

    }

    private string OSHAErrormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {

            if (!validator.IsValid)
            {
                errmsg += validator.ErrorMessage;
            }
        }
        return errmsg;

    }

    #endregion

    #region Validate Methods
    //005, 013
    protected void cusSafetyRepresentation_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (trNonUSSafetyQuestion.Style["display"] != "none") //Non US
        {
            if (rdoRepresentation.SelectedIndex == 0)
            {
                if (string.IsNullOrEmpty(txtRepresentationContactName.Text.Trim()))
                {
                    cusSafetyRepresentation.ErrorMessage = "Please specify contact name for safety representation";
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }

            }
            else if (rdoRepresentation.SelectedIndex == 1)
            {
                args.IsValid = true;
            }
            else
            {
                cusSafetyRepresentation.ErrorMessage = "Please select a option in safety representation";
                args.IsValid = false;
            }
        }
        else
        {
            args.IsValid = true; //Ignore validation in US vendors 
        }

    }

    /// <summary>
    /// 005 validate phone
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void cusPhoneNonUS_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rdoRepresentation.SelectedIndex == 0)
        {
            if ((txtRepresentationContactNumberOne.Text.Trim() == "")
                          && ((txtRepresentationContactNumberTwo.Text.Trim() == "")
                          && (txtRepresentationContactNumberThree.Text.Trim() == "")))
            {
                cusPhoneNonUS.ErrorMessage = "Please enter contact phone number for Safety Representation ";
                args.IsValid = false;

            }
            else
            {
                bool b = objCommon.ValidatePhoneformat(txtRepresentationContactNumberOne.Text.Trim(), txtRepresentationContactNumberTwo.Text.Trim(), txtRepresentationContactNumberThree.Text.Trim(), txtRepresentationContactNumberThree);
                if ((b == false))
                {
                    cusPhoneNonUS.ErrorMessage = "Please enter contact phone number in required format for safety representation";
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }

        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cusSafetyDirector_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rdoSafetyDirector.SelectedIndex == 0)
        {
            if (string.IsNullOrEmpty(txtContactName.Text.Trim()))
            {
                cusSafetyDirector.ErrorMessage = "Please specify contact name";
                args.IsValid = false;
            }
            //013 - ADD
            else if (string.IsNullOrEmpty(txtProLevelOfTraining.Text.Trim()))
            {
                cusSafetyDirector.ErrorMessage = "Please specify certification or level of training";
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }

        }
        else if (rdoSafetyDirector.SelectedIndex == 1)
        {
            args.IsValid = true;
        }
        else
        {
            cusSafetyDirector.ErrorMessage = "Please select a option in company safety director";
            args.IsValid = false;
        }

    }
    /// <summary>
    /// validate phone
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void cusPhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rdoSafetyDirector.SelectedIndex == 0)
        {
            if ((txtMainPhno1.Text.Trim() == "")
                          && ((txtMainPhno2.Text.Trim() == "")
                          && (txtMainPhno3.Text.Trim() == "")))
            {
                cusPhone.ErrorMessage = "Please enter contact phone number";
                args.IsValid = false;

            }
            else
            {
                bool b = objCommon.ValidatePhoneformat(txtMainPhno1.Text.Trim(), txtMainPhno2.Text.Trim(), txtMainPhno3.Text.Trim(), txtMainPhno3);
                if ((b == false))
                {
                    cusPhone.ErrorMessage = "Please enter contact phone number in required format";
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }

        }
        else
        {
            args.IsValid = true;
        }
    }



    protected void cusvYearOne_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (chkNA1.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplanation1.Text.Trim()))
            {
                cusvYearOne.ErrorMessage = "Please enter the explanation under year one";
                args.IsValid = false;
            }

            else
            {
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtRateOne.Text.Trim()))
            {
                cusvYearOne.ErrorMessage = "Please enter rate under year one";

                args.IsValid = false;
            }

            else
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvYearTwo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (chkNA2.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplanation2.Text.Trim()))
            {
                cusvYearTwo.ErrorMessage = "Please enter the explanation under year two";
                args.IsValid = false;
            }

            else
            {
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtRateTwo.Text.Trim()))
            {
                cusvYearTwo.ErrorMessage = "Please enter rate under year two";

                args.IsValid = false;
            }

            else
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvYearThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (chkNA3.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplanation3.Text.Trim()))
            {
                cusvYearThree.ErrorMessage = "Please enter the explanation under year three";
                args.IsValid = false;
            }

            else
            {
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtRateThree.Text.Trim()))
            {
                cusvYearThree.ErrorMessage = "Please enter rate under year three";

                args.IsValid = false;
            }

            else
            {
                args.IsValid = true;
            }
        }
    }

    //003, 007
    protected void cusvYearFour_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (chkEMRNA4.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplanation4.Text.Trim()))
            {
                cusvYearFour.ErrorMessage = "Please enter the explanation under year Four";
                args.IsValid = false;
            }

            else
            {
                args.IsValid = true;
            }
        }
        else
        {
            // only required if year 1 is not available
            if (string.IsNullOrEmpty(txtRateFour.Text.Trim()))
            {
                cusvYearFour.ErrorMessage = "Please enter rate under year four";

                args.IsValid = false;
            }            
        }
    }



    protected void cusvOSHAYearOne_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        cusvOSHAYearOne.ErrorMessage = string.Empty;

        if (chkNA4.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplain4.Text.Trim()))
            {
                cusvOSHAYearOne.ErrorMessage = "Please enter the explanation under OSHA year one";
                args.IsValid = false;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtNoofDeathsYearOne.Text.Trim()))
            {
                cusvOSHAYearOne.ErrorMessage = "Please enter total number of fatalities under OSHA year one";
                args.IsValid = false;
            }
            
            //008
            if (!string.IsNullOrEmpty(hdnSafetyId.Value))
            {
                if (!chkNoAttachment1.Checked)
                {
                    var osha = objSafety.SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearOne.Text);
                    if (osha != null)
                    {
                        if (string.IsNullOrEmpty(osha.FilePath))
                        {
                            cusvOSHAYearOne.ErrorMessage += "Please attach OSHA document for year one";
                            args.IsValid = false;
                        }
                    }
                    else
                    {
                        if (!chkNoAttachment1.Checked && lnkAttachOSHA1.Visible)
                        {
                            cusvOSHAYearOne.ErrorMessage += "Please attach OSHA document for year one";
                            args.IsValid = false;
                        }
                    }
                }
            }
            else
            {
                if (!chkNoAttachment1.Checked && lnkAttachOSHA1.Visible)
                {
                    cusvOSHAYearOne.ErrorMessage += "Please attach OSHA document for year one";
                    args.IsValid = false;
                }
            }

        }
    }

    protected void cusvOSHAYearTwo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        cusvOSHAYearTwo.ErrorMessage = string.Empty;

        if (chkNA5.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplain5.Text.Trim()))
            {
                cusvOSHAYearTwo.ErrorMessage = "Please enter the explanation under OSHA year two";
                args.IsValid = false;
            }

        }
        else
        {
            if (string.IsNullOrEmpty(txtNoofDeathsYearTwo.Text.Trim()))
            {
                cusvOSHAYearTwo.ErrorMessage = "Please enter total number of fatalities under OSHA year two";
                args.IsValid = false;
            }
            //008
            if (!string.IsNullOrEmpty(hdnSafetyId.Value))
            {
                if (!chkNoAttachment2.Checked)
                {
                    var osha = objSafety.SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearTwo.Text);
                    if (osha != null)
                    {
                        if (string.IsNullOrEmpty(osha.FilePath))
                        {
                            cusvOSHAYearTwo.ErrorMessage += "Please attach OSHA document for year two";
                            args.IsValid = false;
                        }
                    }
                    else
                    {
                        if (!chkNoAttachment2.Checked && lnkAttachOSHA2.Visible)
                        {
                            cusvOSHAYearTwo.ErrorMessage += "Please attach OSHA document for year two";
                            args.IsValid = false;
                        }
                    }
                }
            }
            else
            {
                if (!chkNoAttachment2.Checked && lnkAttachOSHA2.Visible)
                {
                    cusvOSHAYearTwo.ErrorMessage += "Please attach OSHA document for year two";
                    args.IsValid = false;
                }
            }



        }
    }

    protected void cusvOSHAYearThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        cusvOSHAYearThree.ErrorMessage = string.Empty;

        if (chkNA6.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplain6.Text.Trim()))
            {
                cusvOSHAYearThree.ErrorMessage = "Please enter the explanation under OSHA year three";
                args.IsValid = false;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtNoofDeathsYearThree.Text.Trim()))
            {
                cusvOSHAYearThree.ErrorMessage = "Please enter total number of fatalities under OSHA year three";
                args.IsValid = false;
            }
            //008
            if (!string.IsNullOrEmpty(hdnSafetyId.Value))
            {
                if (!chkNoAttachment3.Checked)
                {
                    var osha = objSafety.SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearThree.Text);
                    if (osha != null)
                    {
                        if (string.IsNullOrEmpty(osha.FilePath))
                        {
                            cusvOSHAYearThree.ErrorMessage += "Please attach OSHA document for year three";
                            args.IsValid = false;
                        }
                    }
                    else
                    {
                        if (!chkNoAttachment3.Checked && lnkAttachOSHA3.Visible)
                        {
                            cusvOSHAYearThree.ErrorMessage += "Please attach OSHA document for year three";
                            args.IsValid = false;
                        }
                    }
                }
            }
            else
            {
                if (!chkNoAttachment3.Checked && lnkAttachOSHA3.Visible)
                {
                    cusvOSHAYearThree.ErrorMessage += "Please attach OSHA document for year three";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cusvOSHARecord1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA4.Checked == false)
        {
            if (string.IsNullOrEmpty(txtRecordableCasesOne.Text.Trim()))
            {
                cusvOSHARecord1.ErrorMessage = "Please enter total number of other recordable cases under OSHA year one";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHATransfer1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA4.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesTransferOne.Text.Trim()))
            {
                cusvOSHATransfer1.ErrorMessage = "Please enter total number of cases with job transfer or restriction under OSHA year one";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHAWork1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA4.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesFrmWorkOne.Text.Trim()))
            {
                cusvOSHAWork1.ErrorMessage = "Please enter total number of cases with days away from work under OSHA year one";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }


    protected void cusvOSHARecord2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA5.Checked == false)
        {
            if (string.IsNullOrEmpty(txtRecordableCasesTwo.Text.Trim()))
            {
                cusvOSHARecord2.ErrorMessage = "Please enter total number of other recordable cases under OSHA year two";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHATransfer2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA5.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesTransferTwo.Text.Trim()))
            {
                cusvOSHATransfer2.ErrorMessage = "Please enter total number of cases with job transfer or restriction under OSHA year two";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHAWork2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA5.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesFrmWorkTwo.Text.Trim()))
            {
                cusvOSHAWork2.ErrorMessage = "Please enter total number of cases with days away from work under OSHA year two";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }


    protected void cusvOSHARecord3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA6.Checked == false)
        {
            if (string.IsNullOrEmpty(txtRecordableCasesThree.Text.Trim()))
            {
                cusvOSHARecord3.ErrorMessage = "Please enter total number of other recordable cases under OSHA year three";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHATransfer3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA6.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesTransferThree.Text.Trim()))
            {
                cusvOSHATransfer3.ErrorMessage = "Please enter total number of cases with job transfer or restriction under OSHA year three";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHAWork3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA6.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesFrmWorkThree.Text.Trim()))
            {
                cusvOSHAWork3.ErrorMessage = "Please enter total number of cases with days away from work under OSHA year three";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvTotalHours1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA4.Checked == false)
        {
            if (string.IsNullOrEmpty(txtTotalHours1.Text.Trim()))
            {
                cusvTotalHours1.ErrorMessage = "Please enter total hours worked by all employees under OSHA year one";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }
    protected void cusvTotalHours2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA5.Checked == false)
        {
            if (string.IsNullOrEmpty(txtTotalHours2.Text.Trim()))
            {
                cusvTotalHours2.ErrorMessage = "Please enter total hours worked by all employees under OSHA year two";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }
    protected void cusvTotalHours3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA6.Checked == false)
        {
            if (string.IsNullOrEmpty(txtTotalHours3.Text.Trim()))
            {
                cusvTotalHours3.ErrorMessage = "Please enter total hours worked by all employees under OSHA year three";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }


    protected void cusvSICNA_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA4.Checked == false || chkNA5.Checked == false || chkNA6.Checked == false)
        {
            if (chkSICNA.Checked == false && string.IsNullOrEmpty(txtSIC.Text))
            {
                cusvSICNA.ErrorMessage = "Please enter SIC or NAICS Code";
                args.IsValid = false;
            }
            else if (chkSICNA.Checked && string.IsNullOrEmpty(txtSICExplain.Text))
            {
                cusvSICNA.ErrorMessage = "Please enter explanation for SIC or NAICS code";
                args.IsValid = false;
            }
            else { args.IsValid = true; }

        }
        else
        {
            if (chkSICNA.Checked && string.IsNullOrEmpty(txtSICExplain.Text))
            {
                cusvSICNA.ErrorMessage = "Please enter explanation for SIC or NAICS code";
                args.IsValid = false;
            }
            else { args.IsValid = true; }
        }
    }

    //007 - new custom validator events for year 4
    protected void cusvOSHAYearFour_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        cusvOSHAYearFour.ErrorMessage = string.Empty;

        if (chkNA7.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplain7.Text.Trim()))
            {
                cusvOSHAYearFour.ErrorMessage = "Please enter the explanation under OSHA year four";
                args.IsValid = false;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtNoofDeathsYearFour.Text.Trim()))
            {
                cusvOSHAYearFour.ErrorMessage = "Please enter total number of fatalities under OSHA year four";
                args.IsValid = false;
            }
            //008
            if (!string.IsNullOrEmpty(hdnSafetyId.Value))
            {
                if (!chkNoAttachment4.Checked)
                {
                    var osha = objSafety.SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearFour.Text);
                    if (osha != null)
                    {
                        if (string.IsNullOrEmpty(osha.FilePath))
                        {
                            cusvOSHAYearFour.ErrorMessage += "Please attach OSHA document for year four";
                            args.IsValid = false;
                        }
                    }
                    else
                    {
                        if (!chkNoAttachment4.Checked && lnkAttachOSHA4.Visible)
                        {
                            cusvOSHAYearFour.ErrorMessage += "Please attach OSHA document for year four";
                            args.IsValid = false;
                        }
                    }
                }
            }
            else
            {
                if (!chkNoAttachment4.Checked && lnkAttachOSHA4.Visible)
                {
                    cusvOSHAYearFour.ErrorMessage += "Please attach OSHA document for year four";
                    args.IsValid = false;
                }
            }

        }
    }
    protected void CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA7.Checked == false)
        {
            if (string.IsNullOrEmpty(txtRecordableCasesFour.Text.Trim()))
            {
                CustomValidator2.ErrorMessage = "Please enter total number of other recordable cases under OSHA year four";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }
    protected void CustomValidator3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA7.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesTransferFour.Text.Trim()))
            {
                CustomValidator3.ErrorMessage = "Please enter total number of cases with job transfer or restriction under OSHA year four";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }
    protected void CustomValidator4_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA7.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesFrmWorkFour.Text.Trim()))
            {
                CustomValidator4.ErrorMessage = "Please enter total number of cases with days away from work under OSHA year four";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }
    protected void CustomValidator5_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA7.Checked == false)
        {
            if (string.IsNullOrEmpty(txtTotalHours4.Text.Trim()))
            {
                CustomValidator5.ErrorMessage = "Please enter total hours worked by all employees under OSHA year four";
                args.IsValid = false;

            }
            else { args.IsValid = true; }

        }
    }


    //013 - ADD
    protected void cusQuestionaire_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        cusQuestionaire.ErrorMessage = string.Empty;


        //New employee program
        if (rdoSafetytraining.SelectedIndex == 0)
        {
            if (rdoListEmployeeProgramDocumentation.SelectedIndex < 0)
            {
                args.IsValid = false;
                cusQuestionaire.ErrorMessage += "<li>Please specify whether you can provide documentation on new employee training or not</li>";
            }
        }

        //Drug and Alcohol
        if (rdoListDrugAlcohol.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you have a written drug &amp; alcohol program or not</li>";
        }

        // Safety meetings
        if (rdoListSafetyMeetings.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you hold periodic safety meetings or not</li>";
        }
        else if (rdoListSafetyMeetings.SelectedIndex == 0)
        {
            if (chkSafetyMeetings.SelectedIndex < 0)
            {
                args.IsValid = false;
                cusQuestionaire.ErrorMessage += "<li>Please specify how often you hold Safety meetings</li>";
            }
        }

        //Field safety inspection
        if (rdoListInspectionWIP.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you conduct field safety inspections</li>";
        }
        else if (rdoListInspectionWIP.SelectedIndex == 0)
        {
            if (chkInspectionWIP.SelectedIndex < 0)
            {
                args.IsValid = false;
                cusQuestionaire.ErrorMessage += "<li>Please specify how often you conduct safety field inspections</li>";
            }

            if (string.IsNullOrEmpty(txtWhoInspectionWIP.Text.Trim()))
            {
                args.IsValid = false;
                cusQuestionaire.ErrorMessage += "<li>Please specify who conducts safety inspections</li>";
            }
        }

        //Any safety citations?
        if (rdoCitationYear1.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you had any citations for year</li>" + CitationYear1;
        }
        if (rdoCitationYear2.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you had any citations for year</li>" + CitationYear2;
        }
        if (rdoCitationYear3.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you had any citations for year</li>" + CitationYear3;
        }
        if (rdoCitationYear1.SelectedIndex == 0 && string.IsNullOrEmpty(hdnCitationFile1.Value))
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please attach description of circumstances for year " + CitationYear1 + "</li>";
        }
        if (rdoCitationYear2.SelectedIndex == 0 && string.IsNullOrEmpty(hdnCitationFile2.Value))
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please attach description of circumstances for year " + CitationYear2 + "</li>";
        }
        if (rdoCitationYear3.SelectedIndex == 0 && string.IsNullOrEmpty(hdnCitationFile3.Value))
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please attach description of circumstances for year " + CitationYear3 + "</li>"; 
        }

        //Hazard Analysis
        if (rdoHazardAnalysis.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you conduct Hazard analysis</li>";
        }
        else if (rdoHazardAnalysis.SelectedIndex == 0)
        {
            if (chkHazardAnalysisPeriods.SelectedIndex < 0)
            {
                args.IsValid = false;
                cusQuestionaire.ErrorMessage += "<li>Please specify how often you conduct Hazard analysis</li>";
            }
        }

    }


    #endregion


    /// <summary>
    /// 008
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkAttach_Click(object sender, EventArgs e)
    {
        string id = (sender as LinkButton).ID.Replace("lnkAttach", "");
        LoadSafetyAttachmentControl(id);
        uclAttachEMR1.Open();
        
        
    }

    void uclAttachEMR1_CloseEvent(object sender, EventArgs e)
    {   
        LoadEditDatas(true);
        //013 lets find out where was the box triggered and determine submenu to open
        var usercontrol = sender as UserControl_uclSafetyAttachmentBox;
        var linkfileName = usercontrol.FindControl("lnkFileName") as LinkButton;
        string subMenu = string.Empty;
        if (linkfileName.Text.Contains("EMR")) subMenu = "1";
        if (linkfileName.Text.Contains("OSHA")) subMenu = "2";
        if (linkfileName.Text.Contains("CITATION")) subMenu = "3";

        if (!Request.RawUrl.Contains("?submenu") && !string.IsNullOrEmpty(subMenu))
        {
            Response.Redirect(Request.RawUrl + "?submenu=" + subMenu);
        }
    }

    /// <summary>
    /// 008
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAttach_Click(object sender, EventArgs e)
    {
        string id = (sender as ImageButton).ID.Replace("imbAttach", "");
        LoadSafetyAttachmentControl(id);

        uclAttachEMR1.CloseEvent += uclAttachEMR1_CloseEvent;
        uclAttachEMR1.Open();
        if(id.Contains("CITATION")) ClientScript.RegisterClientScriptBlock(this.GetType(), "RunJSFunction", "javascript:ShowHideRequiredContent()");
    }

    protected void chkNoAttachment1_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (sender as CheckBox);
        switch (chk.ID)
        {
            case "chkNoAttachment1":
                lnkAttachOSHA1.Enabled = !chk.Checked;
                lnkAttachOSHA1.CssClass = (!chk.Checked) ? "lnkAttach" : "lnk-disabled";
                starAttachOSHA1.Visible = !chk.Checked;
                break;
            case "chkNoAttachment2":
                lnkAttachOSHA2.Enabled = !chk.Checked;
                lnkAttachOSHA2.CssClass = (!chk.Checked) ? "lnkAttach" : "lnk-disabled";
                starAttachOSHA2.Visible = !chk.Checked;
                break;
            case "chkNoAttachment3":
                lnkAttachOSHA3.Enabled = !chk.Checked;
                lnkAttachOSHA3.CssClass = (!chk.Checked) ? "lnkAttach" : "lnk-disabled";
                starAttachOSHA3.Visible = !chk.Checked;
                break;
            case "chkNoAttachment4":
                lnkAttachOSHA4.Enabled = !chk.Checked;
                lnkAttachOSHA4.CssClass = (!chk.Checked) ? "lnkAttach" : "lnk-disabled";
                starAttachOSHA4.Visible = !chk.Checked;
                break;
            default:
                break;
        }
    }


}
