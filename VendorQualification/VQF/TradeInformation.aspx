﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TradeInformation.aspx.cs"
    Inherits="VQF_TradeInformation" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <%--<script src="../Script/tabcontent.js" type="text/javascript"></script>--%>
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache">
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <style type="text/css">
        body
        {
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
    <script src="../Script/jquery.js" type="text/javascript"></script>
    <%--G. Vera 02/14/2013 - Added--%>
</head>
<body onload="Javascript:scroll(0,0); return false;">
    <form id="form1" runat="server" defaultbutton="imbsubmit">
    <asp:ScriptManager ID="scriptMgr" runat="server" ScriptMode="Release" LoadScriptsBeforeUI="false">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:VQFTopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr id="TabletrContents" runat="server">
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:SideMenu ID="VQFMenu" runat="server" />
                                    </td>
                                    <td class="contenttd">
                                        <%--<asp:UpdatePanel ID="updTradeInfo" runat="server">
                                            <ContentTemplate>--%>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="bodytextbold_right">
                                                    Welcome
                                                    <asp:Label ID="lblwelcome" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    Vendor <span style="letter-spacing: 1px">Qualification</span> Form - Trade Information
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <span>
                                                        <asp:CustomValidator ValidationGroup="Regions" ID="cusvScopes" runat="server" Display="None"
                                                            OnServerValidate="cusvScopes_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <span>
                                                        <asp:CustomValidator ValidationGroup="Regions" ID="cusvRegions" runat="server" Display="None"
                                                            OnServerValidate="cusvRegions_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="hdnScopeTypeOpen" runat="server" Value="MasterFormat" />
                                                    <Ajax:Accordion ID="accTrade" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
                                                        HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                                                        FadeTransitions="true" FramesPerSecond="40" TransitionDuration="250" AutoSize="none"
                                                        RequireOpenedPane="true" SuppressHeaderPostbacks="true">
                                                        <Panes>
                                                            <Ajax:AccordionPane ID="apnScope" runat="server">
                                                                <Header>
                                                                    Scopes of Work
                                                                </Header>
                                                                <Content>
                                                                    <%--Master Format Codes--%>
                                                                    <asp:UpdatePanel ID="updpnlScopeMain" runat="server">
                                                                        <ContentTemplate>
                                                                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr>
                                                                                    <td class="exampletext" colspan="2">
                                                                                        <span class="mandatorystar">*</span>&nbsp;Select at least one category that represents
                                                                                        the scope of work your company typically performs
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <div>
                                                                                            <p style="font-family: Tahoma; font-size: 11px; padding-left: 15px;">
                                                                                                <strong>For your respective Scope of work:</strong></p>
                                                                                        </div>
                                                                                        <div style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 95%">
                                                                                            <asp:DataList runat="server" ID="dlTradeQuestions" OnItemDataBound="dlTradeQuestionAnswers_DataBound"
                                                                                                Width="100%">
                                                                                                <ItemTemplate>
                                                                                                    <div style="float: left; padding-left: 4px; width: 35%; text-align: left; height: 20px">
                                                                                                        <span class="mandatorystar">*</span>&nbsp;
                                                                                                        <asp:Literal ID="lblTradeQuestion" runat="server" Text='<%# Bind("Question") %>' />
                                                                                                        <asp:HiddenField runat="server" ID="hdnQuestionId" Value='<%# Bind("QuestionID") %>' />
                                                                                                    </div>
                                                                                                    <div style="float: left; padding-left: 10px; padding-bottom: 10px; vertical-align: top;
                                                                                                        height: 20px">
                                                                                                        <asp:RadioButtonList runat="server" ID="rdoTradeQuestionAnswers" RepeatDirection="Horizontal">
                                                                                                            <asp:ListItem Text="Yes" Value="Y" />
                                                                                                            <asp:ListItem Text="No" Value="N" />
                                                                                                            <asp:ListItem Text="N/A" Value="NA" />
                                                                                                        </asp:RadioButtonList>
                                                                                                        <asp:RequiredFieldValidator ID="reqvrdoTradeAnswers" ControlToValidate="rdoTradeQuestionAnswers"
                                                                                                            runat="server" ValidationGroup="TradeQuestions" SetFocusOnError="True" EnableClientScript="true"
                                                                                                            Display="None" />
                                                                                                    </div>
                                                                                                </ItemTemplate>
                                                                                            </asp:DataList>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <div class="tab-container" id="divNonDesign" runat="server">
                                                                                            <%--Sooraj - Design Consultant Changes--%>
                                                                                            <ul class="tabs">
                                                                                                <li id="liMasterFormat" runat="server" class="">
                                                                                                    <asp:LinkButton ID="lnkSwitch2PMMI" runat="server" OnClientClick="Javascript:SwitchScopePanel('ctl06_updPnlPMMI', 'ctl06_updpnlScope', 'MasterFormat');scroll(0,0); return false;">MasterFormat Codes</asp:LinkButton></li>
                                                                                                <li id="liPMMI" runat="server" class="">
                                                                                                    <asp:LinkButton runat="server" OnClientClick="Javascript:SwitchScopePanel('ctl06_updpnlScope', 'ctl06_updPnlPMMI', 'PMMI');scroll(0,0); return false;">Packaging Machinery Manufacturers Institute Codes</asp:LinkButton></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <%--Sooraj - Design Consultant--%>
                                                                                        <div class="tab-container" id="divDesign" runat="server">
                                                                                            <ul class="tabs">
                                                                                                <li id="liDesign" runat="server" class="selected"><a onlick="javascript:return false"
                                                                                                    style="cursor: text">Design</a></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <%--MasterFormat Codes--%>
                                                                                    <td colspan="3">
                                                                                        <div id="updpnlScope" runat="server">
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td class="list_head" valign="top" width="50%" style="color: #005785;">
                                                                                                        <%-- SubSection1: Scope Codes--%>
                                                                                                        <asp:GridView ID="grdLevel1" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                                                            ShowHeader="False" OnRowDataBound="GridView1_RowDataBound">
                                                                                                            <Columns>
                                                                                                                <asp:TemplateField>
                                                                                                                    <EditItemTemplate>
                                                                                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                                                                                    </EditItemTemplate>
                                                                                                                    <ItemTemplate>
                                                                                                                        <table>
                                                                                                                            <tr>
                                                                                                                                <td style="text-indent: -40px; padding-left: 40px">
                                                                                                                                    <asp:Image ID="imgStatus" runat="server" ImageUrl="~/Images/plus.gif" />
                                                                                                                                    <asp:Label ID="lblScopeName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hdnFlag" runat="server" />
                                                                                                                        <asp:HiddenField ID="hdnParentScopeId" runat="server" Value='<%# Bind("Pk_ScopeId") %>' />
                                                                                                                        <br />
                                                                                                                        <asp:GridView ID="grdScopeLevele2" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_ScopeId"
                                                                                                                            GridLines="None" ShowHeader="False" OnRowDataBound="grdGeneralReq_RowDataBound">
                                                                                                                            <Columns>
                                                                                                                                <asp:TemplateField>
                                                                                                                                    <EditItemTemplate>
                                                                                                                                        <asp:TextBox ID="txtScopeId" runat="server" Text='<%# Bind("PK_ScopeId") %>'></asp:TextBox>
                                                                                                                                    </EditItemTemplate>
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:HiddenField ID="hdnScopeId" runat="server" Value='<%# Bind("PK_ScopeId") %>' />
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                                <asp:TemplateField ShowHeader="False">
                                                                                                                                    <ItemStyle VerticalAlign="Top" />
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:CheckBox ID="chkCheck" runat="server" CssClass="display_Check" AutoPostBack="false" />
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                                <asp:TemplateField>
                                                                                                                                    <EditItemTemplate>
                                                                                                                                        <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                                                                                                                                    </EditItemTemplate>
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <table>
                                                                                                                                            <tr>
                                                                                                                                                <td style="text-indent: -45px; padding-left: 45px">
                                                                                                                                                    <asp:Label ID="lblName" runat="server" CssClass="list_display" Text='<%# Bind("Name") %>'></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                        <asp:CheckBoxList ID="chklGeneralReq" CssClass="list_display1" runat="server">
                                                                                                                                        </asp:CheckBoxList>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                            </Columns>
                                                                                                                        </asp:GridView>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                            </Columns>
                                                                                                        </asp:GridView>
                                                                                                    </td>
                                                                                                    <td class="list_head" valign="top" width="50%" style="color: #005785;">
                                                                                                        <asp:GridView ID="grdLevel2" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                                                            ShowHeader="False" OnRowDataBound="GridView1_RowDataBound">
                                                                                                            <Columns>
                                                                                                                <asp:TemplateField>
                                                                                                                    <EditItemTemplate>
                                                                                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                                                                                    </EditItemTemplate>
                                                                                                                    <ItemTemplate>
                                                                                                                        <table>
                                                                                                                            <tr>
                                                                                                                                <td style="text-indent: -40px; padding-left: 40px">
                                                                                                                                    <asp:Image ID="imgStatus" runat="server" ImageUrl="~/Images/plus.gif" />
                                                                                                                                    <asp:Label ID="lblScopeName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hdnFlag" runat="server" />
                                                                                                                        <asp:HiddenField ID="hdnParentScopeId" runat="server" Value='<%# Bind("Pk_ScopeId") %>' />
                                                                                                                        <br />
                                                                                                                        <asp:GridView ID="grdScopeLevele2" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_ScopeId"
                                                                                                                            GridLines="None" ShowHeader="False" OnRowDataBound="grdGeneralReq_RowDataBound">
                                                                                                                            <Columns>
                                                                                                                                <asp:TemplateField>
                                                                                                                                    <EditItemTemplate>
                                                                                                                                        <asp:TextBox ID="txtScopeId" runat="server" Text='<%# Bind("PK_ScopeId") %>'></asp:TextBox>
                                                                                                                                    </EditItemTemplate>
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:HiddenField ID="hdnScopeId" runat="server" Value='<%# Bind("PK_ScopeId") %>' />
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                                <asp:TemplateField ShowHeader="False">
                                                                                                                                    <ItemStyle VerticalAlign="Top" />
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:CheckBox ID="chkCheck" runat="server" CssClass="display_Check" AutoPostBack="false" />
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                                <asp:TemplateField>
                                                                                                                                    <EditItemTemplate>
                                                                                                                                        <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                                                                                                                                    </EditItemTemplate>
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <table>
                                                                                                                                            <tr>
                                                                                                                                                <td style="text-indent: -45px; padding-left: 45px">
                                                                                                                                                    <asp:Label ID="lblName" runat="server" CssClass="list_display" Text='<%# Bind("Name") %>'></asp:Label>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                        <asp:CheckBoxList ID="chklGeneralReq" CssClass="list_display1" runat="server">
                                                                                                                                        </asp:CheckBoxList>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                            </Columns>
                                                                                                                        </asp:GridView>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                            </Columns>
                                                                                                        </asp:GridView>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <%--G. Vera - PMMI Codes--%>
                                                                                    <td colspan="3">
                                                                                        <div id="updPnlPMMI" runat="server" style="display: none;">
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td class="list_head" width="100%" style="color: #005785;" valign="top">
                                                                                                        <asp:DataList ID="dlPMMICodes1" runat="server" OnItemDataBound="dlPMMICodes1_ItemDataBound">
                                                                                                            <ItemTemplate>
                                                                                                                <asp:CheckBox ID="chkPMMICode" runat="server" Text='<%# Bind("Name") %>' />&nbsp;
                                                                                                                <asp:HiddenField ID="hdnPMMIScopeId" runat="server" Value='<%# Bind("PK_ScopeID") %>' />
                                                                                                            </ItemTemplate>
                                                                                                        </asp:DataList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <%--Sooraj - Design Consultant--%>
                                                                                    <td colspan="3">
                                                                                        <div id="updPnlDesign" runat="server">
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td class="list_head" width="100%" style="color: #005785;" valign="top">
                                                                                                        <asp:DataList ID="dlistDesign" runat="server" RepeatColumns="3" RepeatDirection="Vertical">
                                                                                                            <ItemTemplate>
                                                                                                                <asp:CheckBox ID="chkDesignCode" CssClass="checkboxthreeColumn" runat="server" Text='<%# Bind("Name") %>' />&nbsp;
                                                                                                                <asp:HiddenField ID="hdnDesignScopeId" runat="server" Value='<%# Bind("PK_ScopeID") %>' />
                                                                                                            </ItemTemplate>
                                                                                                        </asp:DataList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </Content>
                                                            </Ajax:AccordionPane>
                                                            <Ajax:AccordionPane ID="apnRegions" runat="server">
                                                                <Header>
                                                                    Regions
                                                                </Header>
                                                                <Content>
                                                                    <asp:UpdatePanel ID="upnlCSI" runat="server">
                                                                        <ContentTemplate>
                                                                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr>
                                                                                    <td class="exampletext" colspan="2">
                                                                                        <span class="mandatorystar">*</span>&nbsp;Select at least one region where your
                                                                                        company is qualified to work
                                                                                        <%-- Sooraj : Inserted verbage for Design Consultant --%>
                                                                                        <span id="spnRegionDesign" runat="server">
                                                                                            <br />
                                                                                            <br />
                                                                                            If your scope of work is one that is regulated by a specific state’s professional
                                                                                            board, “qualified” means both the firm and respective individuals have all required
                                                                                            licenses/registrations with the respective state. &nbsp;</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding-left: 5px">
                                                                                        <span id="pnlRegions" runat="server">
                                                                                            <%--006-Sooraj--%>
                                                                                            <table cellpadding="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="regionTableCell" colspan="3">
                                                                                                        <asp:CheckBox ID="chkUS" runat="server" Text="United States of America" CssClass="list_displaybold" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <div id="divUS" runat="server" style="display: none">
                                                                                                            <table width="100%" cellpadding="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <tr>
                                                                                                                            <td valign="top" bgcolor="#dfdfdf" style="padding: 3px 0px 5px 12px;" colspan="3">
                                                                                                                                <asp:CheckBox ID="chkCheckAll" runat="server" Text="ALL-National" CssClass="list_displaybold" />
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td valign="top" bgcolor="#efefef" width="33%">
                                                                                                                                <asp:GridView ID="grdState1" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_StateId"
                                                                                                                                    GridLines="None" ShowHeader="False" OnRowDataBound="grdState1_RowDataBound">
                                                                                                                                    <Columns>
                                                                                                                                        <asp:TemplateField>
                                                                                                                                            <EditItemTemplate>
                                                                                                                                                <asp:TextBox ID="txtStateID" runat="server" Text='<%# Bind("PK_StateId") %>'></asp:TextBox>
                                                                                                                                            </EditItemTemplate>
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <asp:HiddenField ID="hdnStateID" runat="server" Value='<%# Bind("PK_StateId") %>' />
                                                                                                                                            </ItemTemplate>
                                                                                                                                        </asp:TemplateField>
                                                                                                                                        <asp:TemplateField ShowHeader="False">
                                                                                                                                            <ItemStyle VerticalAlign="Top" />
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <asp:CheckBox ID="chkCheck" runat="server" CssClass="display_Check" AutoPostBack="false" />
                                                                                                                                            </ItemTemplate>
                                                                                                                                            <ItemStyle VerticalAlign="Top" />
                                                                                                                                        </asp:TemplateField>
                                                                                                                                        <asp:TemplateField>
                                                                                                                                            <EditItemTemplate>
                                                                                                                                                <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("StateCode") %>'></asp:TextBox>
                                                                                                                                            </EditItemTemplate>
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <asp:Label ID="lblName" runat="server" CssClass="list_display" Text='<%# Bind("StateCode") %>'></asp:Label>
                                                                                                                                                <asp:CheckBoxList ID="chklRegions" CssClass="list_displayView" runat="server" CellPadding="0"
                                                                                                                                                    CellSpacing="0">
                                                                                                                                                </asp:CheckBoxList>
                                                                                                                                            </ItemTemplate>
                                                                                                                                        </asp:TemplateField>
                                                                                                                                    </Columns>
                                                                                                                                </asp:GridView>
                                                                                                                            </td>
                                                                                                                            <td valign="top" bgcolor="#efefef" width="33%">
                                                                                                                                <asp:GridView ID="grdState2" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_StateId"
                                                                                                                                    GridLines="None" ShowHeader="False" OnRowDataBound="grdState1_RowDataBound">
                                                                                                                                    <Columns>
                                                                                                                                        <asp:TemplateField>
                                                                                                                                            <EditItemTemplate>
                                                                                                                                                <asp:TextBox ID="txtStateID" runat="server" Text='<%# Bind("PK_StateId") %>'></asp:TextBox>
                                                                                                                                            </EditItemTemplate>
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <asp:HiddenField ID="hdnStateID" runat="server" Value='<%# Bind("PK_StateId") %>' />
                                                                                                                                            </ItemTemplate>
                                                                                                                                        </asp:TemplateField>
                                                                                                                                        <asp:TemplateField ShowHeader="False">
                                                                                                                                            <ItemStyle VerticalAlign="Top" />
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <asp:CheckBox ID="chkCheck" runat="server" CssClass="display_Check" AutoPostBack="false" />
                                                                                                                                            </ItemTemplate>
                                                                                                                                        </asp:TemplateField>
                                                                                                                                        <asp:TemplateField>
                                                                                                                                            <EditItemTemplate>
                                                                                                                                                <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("StateCode") %>'></asp:TextBox>
                                                                                                                                            </EditItemTemplate>
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <asp:Label ID="lblName" runat="server" CssClass="list_display" Text='<%# Bind("StateCode") %>'></asp:Label>
                                                                                                                                                <asp:CheckBoxList ID="chklRegions" CssClass="list_displayView" runat="server" CellPadding="0"
                                                                                                                                                    CellSpacing="0">
                                                                                                                                                </asp:CheckBoxList>
                                                                                                                                            </ItemTemplate>
                                                                                                                                        </asp:TemplateField>
                                                                                                                                    </Columns>
                                                                                                                                </asp:GridView>
                                                                                                                            </td>
                                                                                                                            <td valign="top" bgcolor="#efefef" width="34%">
                                                                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                                    <tr>
                                                                                                                                        <td align="left" width="100%">
                                                                                                                                            <asp:GridView ID="grdState3" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_StateId"
                                                                                                                                                GridLines="None" ShowHeader="False" OnRowDataBound="grdState1_RowDataBound">
                                                                                                                                                <Columns>
                                                                                                                                                    <asp:TemplateField>
                                                                                                                                                        <EditItemTemplate>
                                                                                                                                                            <asp:TextBox ID="txtStateID" runat="server" Text='<%# Bind("PK_StateId") %>'></asp:TextBox>
                                                                                                                                                        </EditItemTemplate>
                                                                                                                                                        <ItemTemplate>
                                                                                                                                                            <asp:HiddenField ID="hdnStateID" runat="server" Value='<%# Bind("PK_StateId") %>' />
                                                                                                                                                        </ItemTemplate>
                                                                                                                                                    </asp:TemplateField>
                                                                                                                                                    <asp:TemplateField ShowHeader="False">
                                                                                                                                                        <ItemStyle VerticalAlign="Top" />
                                                                                                                                                        <ItemTemplate>
                                                                                                                                                            <asp:CheckBox ID="chkCheck" runat="server" CssClass="display_Check" AutoPostBack="false" />
                                                                                                                                                        </ItemTemplate>
                                                                                                                                                    </asp:TemplateField>
                                                                                                                                                    <asp:TemplateField>
                                                                                                                                                        <EditItemTemplate>
                                                                                                                                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("StateCode") %>'></asp:TextBox>
                                                                                                                                                        </EditItemTemplate>
                                                                                                                                                        <ItemTemplate>
                                                                                                                                                            <asp:Label ID="lblName" runat="server" CssClass="list_display" Text='<%# Bind("StateCode") %>'></asp:Label>
                                                                                                                                                            <asp:CheckBoxList ID="chklRegions" CssClass="list_displayView" runat="server" CellPadding="0"
                                                                                                                                                                CellSpacing="0">
                                                                                                                                                            </asp:CheckBoxList>
                                                                                                                                                        </ItemTemplate>
                                                                                                                                                    </asp:TemplateField>
                                                                                                                                                </Columns>
                                                                                                                                            </asp:GridView>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td id="trRegions" runat="server" style="display: none; padding-left: 35px; padding-bottom: 20px"
                                                                                                                                            class="bodytextbold">
                                                                                                                                            Please specify
                                                                                                                                            <asp:TextBox ID="txtRegions" runat="server" CssClass="txtboxmedium"></asp:TextBox>
                                                                                                                                            <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherRegions" TargetControlID="txtRegions"
                                                                                                                                                FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                                                InvalidChars="_-#.$">
                                                                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--006--%>
                                                                                                <tr>
                                                                                                    <td class="regionTableCell" colspan="3">
                                                                                                        <asp:GridView ID="grdContinent" runat="server" DataKeyNames="PK_ContinentId" AutoGenerateColumns="false"
                                                                                                            GridLines="None" Width="100%" HeaderStyle-CssClass="regionHeaderHidden">
                                                                                                            <Columns>
                                                                                                                <asp:TemplateField>
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:CheckBox ID="chkContinent" runat="server" Text='<%# Bind("ContinentName") %>'
                                                                                                                            CssClass="list_displaybold regionTableBorderBottom" />
                                                                                                                        <div id="divCountries" class="regionCountry" runat="server" style="display: none;">
                                                                                                                            <asp:CheckBoxList ID="chkContinentCountries" runat="server" RepeatDirection="Horizontal"
                                                                                                                                RepeatColumns="3" CssClass="checkboxthreeColumn regionFont" />
                                                                                                                            <asp:TextBox ID="txtContinentCountries" runat="server" CssClass="txtbox" Visible="false" />
                                                                                                                        </div>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                            </Columns>
                                                                                                        </asp:GridView>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="5" style="border-bottom: 1px solid #cccccc" class="style1">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <%--006-Sooraj --%>
                                                                                        </span>
                                                                                        <%-- <asp:CheckBoxList ID="testcheck1" runat="server" RepeatColumns="4" RepeatDirection="Horizontal"
                                                                   CssClass="lstbox" Width="100%" AutoPostBack="true"
                                                                       onselectedindexchanged="testcheck1_SelectedIndexChanged" >
                                                                   </asp:CheckBoxList>--%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding-left: 10px; padding-right: 10px">
                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td colspan="2" class="bodytextbold">
                                                                                                    &nbsp;&nbsp;List License numbers in which your company is legally qualified to work
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" class="keyheader" style="padding-left: 10px">
                                                                                                    License 1
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" class="tdheight">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtd" width="35%">
                                                                                                    Country
                                                                                                </td>
                                                                                                <td class="formtdrt">
                                                                                                    <asp:DropDownList  ID="ddlCountry" runat="server" CssClass="ddlboxCountry"
                                                                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtd" width="35%">
                                                                                                    State
                                                                                                </td>
                                                                                                <td class="formtdrt">
                                                                                                    <asp:DropDownList ID="ddlStates" runat="server" CssClass="ddlboxState" AutoPostBack="True">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:CustomValidator ValidationGroup="Regions" ID="cusvState" runat="server" Display="None"
                                                                                                        OnServerValidate="cusvState_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trOther" runat="server" style="display: none">
                                                                                                <td class="formtdlt">
                                                                                                    Please Specify
                                                                                                </td>
                                                                                                <td class="formtdrt">
                                                                                                    <asp:TextBox ID="txtOtherState" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherState" TargetControlID="txtOtherState"
                                                                                                        FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                        InvalidChars="_-#.$">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtdlt">
                                                                                                    License Number
                                                                                                </td>
                                                                                                <td class="formtdrt">
                                                                                                    <asp:TextBox ID="txtLicense" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtdlt">
                                                                                                    Expiration Date
                                                                                                </td>
                                                                                                <td class="formtdrt">
                                                                                                    <asp:TextBox ID="txtExpiration" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="fteExpiration" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="/" TargetControlID="txtExpiration">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <asp:ImageButton ID="imbCalc" runat="server" ImageUrl="~/Images/cal.gif" />
                                                                                                    <Ajax:CalendarExtender ID="calcExpiration" runat="server" Format="MM/dd/yyyy" TargetControlID="txtExpiration"
                                                                                                        PopupButtonID="imbCalc">
                                                                                                    </Ajax:CalendarExtender>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <tr id="spnmoreadd1" runat="server" style="display: none">
                                                                                                    <td colspan="2">
                                                                                                        <span>
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="keyheader">
                                                                                                                        &nbsp;&nbsp;License 2
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="tdheight">&nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtd" width="35%">
                                                                                                                        Country
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:DropDownList  ID="ddlCountry1" runat="server" CssClass="ddlboxCountry"
                                                                                                                            OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtd" width="35%">
                                                                                                                        State
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:DropDownList ID="ddlState1" runat="server" CssClass="ddlboxState" AutoPostBack="True">
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:CustomValidator ValidationGroup="Regions" ID="cusvState1" runat="server" Display="None"
                                                                                                                            OnServerValidate="cusvState1_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr id="trOther1" runat="server" style="display: none">
                                                                                                                    <td class="formtdlt">
                                                                                                                        Please Specify
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:TextBox ID="txtOther1" runat="server" MaxLength="50" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOther1" TargetControlID="txtOther1"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtdlt">
                                                                                                                        License Number
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:TextBox ID="txtLicense1" runat="server" MaxLength="50" CssClass="txtbox"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtdlt">
                                                                                                                        Expiration Date
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:TextBox ID="txtExpiration1" runat="server" MaxLength="10" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <asp:ImageButton ID="imbCalc1" runat="server" ImageUrl="~/Images/cal.gif" />
                                                                                                                        <Ajax:CalendarExtender ID="calcExpiration1" runat="server" TargetControlID="txtExpiration1"
                                                                                                                            PopupButtonID="imbCalc1" Format="MM/dd/yyyy">
                                                                                                                        </Ajax:CalendarExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="spnmoreadd2" runat="server" style="display: none">
                                                                                                    <td colspan="2">
                                                                                                        <span>
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="keyheader">
                                                                                                                        &nbsp;&nbsp; License 3
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="tdheight">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtd" width="35%">
                                                                                                                        Country
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:DropDownList  ID="ddlCountry2" runat="server" CssClass="ddlboxCountry"
                                                                                                                            OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtd" width="35%">
                                                                                                                        State
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:DropDownList ID="ddlState2" runat="server" CssClass="ddlboxState" AutoPostBack="True">
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:CustomValidator ValidationGroup="Regions" ID="cusvState2" runat="server" Display="None"
                                                                                                                            OnServerValidate="cusvState2_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr id="trOther2" runat="server" style="display: none">
                                                                                                                    <td class="formtdlt">
                                                                                                                        Please Specify
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:TextBox ID="txtOther2" runat="server" MaxLength="50" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOther2" TargetControlID="txtOther2"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtdlt">
                                                                                                                        License Number
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:TextBox ID="txtLicense2" runat="server" MaxLength="50" CssClass="txtbox"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtdlt">
                                                                                                                        Expiration Date
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:TextBox ID="txtExpiration2" runat="server" MaxLength="10" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <%-- <asp:CustomValidator ValidationGroup="Regions" ID="cusExpirationDate2" runat="server"
                                                                                                                            Display="None" EnableClientScript="false" OnServerValidate="cusExpirationDate2_ServerValidate"></asp:CustomValidator>--%>
                                                                                                                        <asp:ImageButton ID="imbCalc2" runat="server" ImageUrl="~/Images/cal.gif" />
                                                                                                                        <Ajax:CalendarExtender ID="calcExpiration2" runat="server" TargetControlID="txtExpiration2"
                                                                                                                            PopupButtonID="imbCalc2" Format="MM/dd/yyyy">
                                                                                                                        </Ajax:CalendarExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="spnmoreadd3" runat="server" style="display: none">
                                                                                                    <td colspan="2">
                                                                                                        <span>
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="keyheader">
                                                                                                                        &nbsp;&nbsp;License 4
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="tdheight">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtd" width="35%">
                                                                                                                        Country
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:DropDownList  ID="ddlCountry3" runat="server" CssClass="ddlboxCountry"
                                                                                                                            OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtd" width="35%">
                                                                                                                        State
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:DropDownList ID="ddlState3" runat="server" CssClass="ddlboxState">
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:CustomValidator ValidationGroup="Regions" ID="cusvState3" runat="server" Display="None"
                                                                                                                            OnServerValidate="cusvState3_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr id="trOther3" runat="server" style="display: none">
                                                                                                                    <td class="formtdlt">
                                                                                                                        Please Specify
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:TextBox ID="txtOther3" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOther3" TargetControlID="txtOther3"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtdlt">
                                                                                                                        License Number
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:TextBox ID="txtLicense3" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtdlt">
                                                                                                                        Expiration Date
                                                                                                                    </td>
                                                                                                                    <td class="formtdrt">
                                                                                                                        <asp:TextBox ID="txtExpiration3" runat="server" MaxLength="10" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <%--<asp:CustomValidator ValidationGroup="Regions" ID="cusExpirationDate3" runat="server"
                                                                                                                            Display="None" EnableClientScript="false" OnServerValidate="cusExpirationDate3_ServerValidate"></asp:CustomValidator>--%>
                                                                                                                        <asp:ImageButton ID="imbCalc3" runat="server" ImageUrl="~/Images/cal.gif" />
                                                                                                                        <Ajax:CalendarExtender ID="calcExpiration3" runat="server" TargetControlID="txtExpiration3"
                                                                                                                            PopupButtonID="imbCalc3" Format="MM/dd/yyyy">
                                                                                                                        </Ajax:CalendarExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="spnmoreadd4" runat="server" style="display: none">
                                                                                                    <td colspan="2">
                                                                                                        <span>
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="keyheader">
                                                                                                                        &nbsp;&nbsp;License 5
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="tdheight">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <td class="formtd" width="35%">
                                                                                                                    Country
                                                                                                                </td>
                                                                                                                <td class="formtdrt">
                                                                                                                    <asp:DropDownList  ID="ddlCountry4" runat="server" CssClass="ddlboxCountry"
                                                                                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtd" width="35%">
                                                                                                        State
                                                                                                    </td>
                                                                                                    <td class="formtdrt">
                                                                                                        <asp:DropDownList ID="ddlState4" runat="server" CssClass="ddlboxState" AutoPostBack="True">
                                                                                                        </asp:DropDownList>
                                                                                                        <asp:CustomValidator ValidationGroup="Regions" ID="cusvState4" runat="server" Display="None"
                                                                                                            OnServerValidate="cusvState4_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trOther4" runat="server" style="display: none">
                                                                                                    <td class="formtdlt">
                                                                                                        Please Specify
                                                                                                    </td>
                                                                                                    <td class="formtdrt">
                                                                                                        <asp:TextBox ID="txtOther4" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOther4" TargetControlID="txtOther4"
                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                            InvalidChars="_-#.$">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtdlt">
                                                                                                        License Number
                                                                                                    </td>
                                                                                                    <td class="formtdrt">
                                                                                                        <asp:TextBox ID="txtLicense4" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtdlt">
                                                                                                        Expiration Date
                                                                                                    </td>
                                                                                                    <td class="formtdrt">
                                                                                                        <asp:TextBox ID="txtExpiration4" runat="server" MaxLength="10" CssClass="txtbox"></asp:TextBox>
                                                                                                        <%--<asp:CustomValidator ValidationGroup="Regions" ID="cusExpirationDate4" runat="server"
                                                                                                                            Display="None" EnableClientScript="false" OnServerValidate="cusExpirationDate4_ServerValidate"></asp:CustomValidator>--%>
                                                                                                        <asp:ImageButton ID="imbCalc4" runat="server" ImageUrl="~/Images/cal.gif" />
                                                                                                        <Ajax:CalendarExtender ID="CalcExpiration4" runat="server" TargetControlID="txtExpiration4"
                                                                                                            PopupButtonID="imbCalc4" Format="MM/dd/yyyy">
                                                                                                        </Ajax:CalendarExtender>
                                                                                                    </td>
                                                                                                </tr>
                                                                                        </table>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="spnmoreadd5" runat="server" style="display: none">
                                                                                    <td colspan="2">
                                                                                        <span>
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td colspan="2" class="keyheader">
                                                                                                        &nbsp;&nbsp;License 6
                                                                                                    </td>
                                                                                                </tr>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <td class="formtd" width="35%">
                                                                                    Country
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:DropDownList  ID="ddlCountry5" runat="server" CssClass="ddlboxCountry"
                                                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtd" width="35%">
                                                                                        State
                                                                                    </td>
                                                                                    <td class="formtdrt">
                                                                                        <asp:DropDownList ID="ddlState5" runat="server" CssClass="ddlboxState" AutoPostBack="True">
                                                                                        </asp:DropDownList>
                                                                                        <asp:CustomValidator ValidationGroup="Regions" ID="cusvState5" runat="server" Display="None"
                                                                                            OnServerValidate="cusvState5_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trOther5" runat="server" style="display: none">
                                                                                    <td class="formtdlt">
                                                                                        Please Specify
                                                                                    </td>
                                                                                    <td class="formtdrt">
                                                                                        <asp:TextBox ID="txtOther5" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOther5" TargetControlID="txtOther5"
                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                            InvalidChars="_-#.$">
                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtdlt">
                                                                                        License Number
                                                                                    </td>
                                                                                    <td class="formtdrt">
                                                                                        <asp:TextBox ID="txtLicense5" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtdlt">
                                                                                        Expiration Date
                                                                                    </td>
                                                                                    <td class="formtdrt">
                                                                                        <asp:TextBox ID="txtExpiration5" runat="server" MaxLength="10" CssClass="txtbox"></asp:TextBox>
                                                                                        <%--  <asp:CustomValidator ValidationGroup="Regions" ID="cusExpirationDate5" runat="server"
                                                                                                            Display="None" EnableClientScript="false" OnServerValidate="cusExpirationDate5_ServerValidate"></asp:CustomValidator>--%>
                                                                                        <asp:ImageButton ID="imbCalc5" runat="server" ImageUrl="~/Images/cal.gif" />
                                                                                        <Ajax:CalendarExtender ID="CalcExpiration5" runat="server" TargetControlID="txtExpiration5"
                                                                                            PopupButtonID="imbCalc5" Format="MM/dd/yyyy">
                                                                                        </Ajax:CalendarExtender>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            </span> </td> </tr>
                                                                            <tr id="spnmoreadd6" runat="server" style="display: none">
                                                                                <td colspan="2">
                                                                                    <span>
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td colspan="2" class="keyheader">
                                                                                                    &nbsp;&nbsp; License 7
                                                                                                </td>
                                                                                            </tr>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                            <td class="formtd" width="35%">
                                                                                Country
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:DropDownList  ID="ddlCountry6" runat="server" CssClass="ddlboxCountry"
                                                                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtd" width="35%">
                                                                                    State
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:DropDownList ID="ddlState6" runat="server" CssClass="ddlboxState" AutoPostBack="True">
                                                                                    </asp:DropDownList>
                                                                                    <asp:CustomValidator ValidationGroup="Regions" ID="cusvState6" runat="server" Display="None"
                                                                                        OnServerValidate="cusvState6_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trOther6" runat="server" style="display: none">
                                                                                <td class="formtdlt">
                                                                                    Please Specify
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtOther6" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOther6" TargetControlID="txtOther6"
                                                                                        FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                        InvalidChars="_-#.$">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtdlt">
                                                                                    License Number
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtLicense6" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtdlt">
                                                                                    Expiration Date
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtExpiration6" runat="server" MaxLength="10" CssClass="txtbox"></asp:TextBox>
                                                                                    <%-- <asp:CustomValidator ValidationGroup="Regions" ID="cusExpirationDate6" runat="server"
                                                                                            Display="None" EnableClientScript="false" OnServerValidate="cusExpirationDate6_ServerValidate"></asp:CustomValidator>--%>
                                                                                    <asp:ImageButton ID="imbCalc6" runat="server" ImageUrl="~/Images/cal.gif" />
                                                                                    <Ajax:CalendarExtender ID="calcExpiration6" runat="server" TargetControlID="txtExpiration6"
                                                                                        PopupButtonID="imbCalc6" Format="MM/dd/yyyy">
                                                                                    </Ajax:CalendarExtender>
                                                                                </td>
                                                                            </tr>
                                                                            </table> </span> </td> </tr>
                                                                            <tr id="spnmoreadd7" runat="server" style="display: none">
                                                                                <td colspan="2">
                                                                                    <span>
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td colspan="2" class="keyheader">
                                                                                                    &nbsp;&nbsp;License 8
                                                                                                </td>
                                                                                            </tr>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                            <td class="formtd" width="35%">
                                                                                Country
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:DropDownList  ID="ddlCountry7" runat="server" CssClass="ddlboxCountry"
                                                                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtd" width="35%">
                                                                                    State
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:DropDownList ID="ddlState7" runat="server" CssClass="ddlboxState" AutoPostBack="True">
                                                                                    </asp:DropDownList>
                                                                                    <asp:CustomValidator ValidationGroup="Regions" ID="cusvState7" runat="server" Display="None"
                                                                                        OnServerValidate="cusvState7_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trOther7" runat="server" style="display: none">
                                                                                <td class="formtdlt">
                                                                                    Please Specify
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtOther7" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOther7" TargetControlID="txtOther7"
                                                                                        FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                        InvalidChars="_-#.$">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtdlt">
                                                                                    License Number
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtLicense7" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtdlt">
                                                                                    Expiration Date
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtExpiration7" runat="server" MaxLength="10" CssClass="txtbox"></asp:TextBox>
                                                                                    <%-- <asp:CustomValidator ValidationGroup="Regions" ID="cusExpirationDate7" runat="server"
                                                                                        Display="None" EnableClientScript="false" OnServerValidate="cusExpirationDate7_ServerValidate"></asp:CustomValidator>--%>
                                                                                    <asp:ImageButton ID="imbCalc7" runat="server" ImageUrl="~/Images/cal.gif" />
                                                                                    <Ajax:CalendarExtender ID="calcExpiration7" runat="server" TargetControlID="txtExpiration7"
                                                                                        PopupButtonID="imbCalc7" Format="MM/dd/yyyy">
                                                                                    </Ajax:CalendarExtender>
                                                                                </td>
                                                                            </tr>
                                                                            </table> </span> </td> </tr>
                                                                            <tr id="spnmoreadd8" runat="server" style="display: none">
                                                                                <td colspan="2">
                                                                                    <span>
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td colspan="2" class="keyheader">
                                                                                                    &nbsp;&nbsp;License 9
                                                                                                </td>
                                                                                            </tr>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                            <td class="formtd" width="35%">
                                                                                Country
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:DropDownList  ID="ddlCountry8" runat="server" CssClass="ddlboxCountry"
                                                                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtd" width="35%">
                                                                                    State
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:DropDownList ID="ddlState8" runat="server" CssClass="ddlboxState" AutoPostBack="True">
                                                                                    </asp:DropDownList>
                                                                                    <asp:CustomValidator ValidationGroup="Regions" ID="cusvState8" runat="server" Display="None"
                                                                                        OnServerValidate="cusvState8_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trOther8" runat="server" style="display: none">
                                                                                <td class="formtdlt">
                                                                                    Please Specify
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtOther8" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOther8" TargetControlID="txtOther8"
                                                                                        FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                        InvalidChars="_-#.$">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtdlt">
                                                                                    License Number
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtLicense8" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtdlt">
                                                                                    Expiration Date
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtExpiration8" runat="server" MaxLength="10" CssClass="txtbox"></asp:TextBox>
                                                                                    <asp:ImageButton ID="imbCalc8" runat="server" ImageUrl="~/Images/cal.gif" />
                                                                                    <Ajax:CalendarExtender ID="calcExpiration8" runat="server" TargetControlID="txtExpiration8"
                                                                                        PopupButtonID="imbCalc8" Format="MM/dd/yyyy">
                                                                                    </Ajax:CalendarExtender>
                                                                                </td>
                                                                            </tr>
                                                                            </table> </span> </td> </tr>
                                                                            <tr id="spnmoreadd9" runat="server" style="display: none">
                                                                                <td colspan="2">
                                                                                    <span>
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td colspan="2" class="keyheader">
                                                                                                    &nbsp;&nbsp;License 10
                                                                                                </td>
                                                                                            </tr>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                            <td class="formtd" width="35%">
                                                                                Country
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:DropDownList  ID="ddlCountry9" runat="server" CssClass="ddlboxCountry"
                                                                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" TabIndex="0" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtd" width="35%">
                                                                                    State
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:DropDownList ID="ddlState9" runat="server" CssClass="ddlboxState" AutoPostBack="True">
                                                                                    </asp:DropDownList>
                                                                                    <asp:CustomValidator ValidationGroup="Regions" ID="cusvState9" runat="server" Display="None"
                                                                                        OnServerValidate="cusvState9_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trOther9" runat="server" style="display: none">
                                                                                <td class="formtdlt">
                                                                                    Please Specify
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtOther9" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOther9" TargetControlID="txtOther9"
                                                                                        FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                        InvalidChars="_-#.$">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtdlt">
                                                                                    License Number
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtLicense9" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtdlt">
                                                                                    Expiration Date
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtExpiration9" runat="server" MaxLength="10" CssClass="txtbox"></asp:TextBox>
                                                                                    <asp:ImageButton ID="imbCalc9" runat="server" ImageUrl="~/Images/cal.gif" />
                                                                                    <Ajax:CalendarExtender ID="calcExpiration9" runat="server" TargetControlID="txtExpiration9"
                                                                                        PopupButtonID="imbCalc9" Format="MM/dd/yyyy">
                                                                                    </Ajax:CalendarExtender>
                                                                                </td>
                                                                            </tr>
                                                                            </table> </span> </td> </tr>
                                                                            <tr>
                                                                                <td colspan="2" align="center">
                                                                                    <asp:LinkButton ID="lnkAddMore" runat="server" CssClass="hyplinks">Click to Add More Information</asp:LinkButton>
                                                                                    <asp:HiddenField ID="hdnCount" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtdlt">
                                                                                    &nbsp;Will you perform all work with your own forces?
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:RadioButtonList ID="rdlyeano" runat="server" onclick="javascript:ShowPerform()"
                                                                                        RepeatDirection="Horizontal">
                                                                                        <asp:ListItem>Yes</asp:ListItem>
                                                                                        <asp:ListItem>No</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                    <asp:CustomValidator ValidationGroup="Regions" ID="custRadioYesno" runat="server"
                                                                                        Display="None" OnServerValidate="custRadioYesno_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <span id="trexplain" runat="server" style="display: none">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td class="formtdlt" width="20%">
                                                                                                    If No, Please Explain
                                                                                                </td>
                                                                                                <td class="formtdrt" width="80%">
                                                                                                    <asp:TextBox ID="txtexplain" runat="server" TextMode="MultiLine" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                        onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                                                        CssClass="txtboxmultilarge"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                            </tr></table></td> </table>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </Content>
                                                            </Ajax:AccordionPane>
                                                            <Ajax:AccordionPane ID="apnProject" runat="server">
                                                                <Header>
                                                                    Types of Projects
                                                                </Header>
                                                                <Content>
                                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                        <tr>
                                                                            <td class="exampletext">
                                                                                &nbsp;Select project types that your company performs work or in which it specializes
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBoxList ID="chkProject" runat="server" CssClass="lstbox" RepeatColumns="3"
                                                                                                RepeatDirection="Horizontal" Width="100%">
                                                                                            </asp:CheckBoxList>
                                                                                            <%--G. Vera 10/16/2012 - Added below--%>
                                                                                            <br />
                                                                                            <asp:CheckBox ID="chkProjectOther" runat="server" Text="Other" CssClass="lstbox"
                                                                                                onclick="Javascript:ClearTextBox(this);" />&nbsp;&nbsp;<asp:TextBox ID="txtChkOther"
                                                                                                    runat="server" MaxLength="250" CssClass="txtbox" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </Content>
                                                            </Ajax:AccordionPane>

                                                            <%--G. Vera - 02/13/2013 New BIM Section--%>
                                                            <Ajax:AccordionPane ID="apnBIM" runat="server">
                                                                <Header>
                                                                    Building Information Modeling Qualifications (BIM)
                                                                </Header>
                                                                <Content>
                                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                        <tr>
                                                                            <td class="exampletext">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%">
                                                                                    <tr id="trBIMQuestion1" runat="server" style="display: ">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion1" runat="server" />
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                            <asp:RadioButtonList runat="server" ID="rdoBIMQuestion1" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Text="Yes" Value="Y" />
                                                                                                <asp:ListItem Text="No" Value="N" />
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMQuestion1A" runat="server" style="display: none">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion1A" runat="server" />
                                                                                            <br />
                                                                                            <br />
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtBIMQuestion1A" runat="server" MaxLength="50" Width="100%" CssClass="txtboxlarge" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMQuestion1B" runat="server" style="display: none">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion1B" runat="server" />
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                            <asp:RadioButtonList runat="server" ID="rdoBIMQuestion1B" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Text="Yes" Value="Y" />
                                                                                                <asp:ListItem Text="No" Value="N" />
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMQuestion1C" runat="server" style="display: none">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion1C" runat="server" />
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                            <div style="margin-left: 10px">
                                                                                                <%--G. Vera - 10/29/2015 added maxlength--%>
                                                                                                <asp:TextBox ID="txtBIMQuestion1C" runat="server" Width="70px" CssClass="txtboxNum" MaxLength="50" />
                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtBIMQuestion1C" runat="server" TargetControlID="txtBIMQuestion1C"
                                                                                                    FilterType="Numbers">
                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMQuestion1D" runat="server" style="display: none">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion1D" runat="server" />
                                                                                            <br />
                                                                                            <br />
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtBIMQuestion1D" runat="server" MaxLength="50" Width="100%" CssClass="txtboxlarge" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMQuestion2" runat="server" style="display: ">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion2" runat="server" />
                                                                                            <br />
                                                                                            <br />
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtBIMQuestion2" runat="server" CssClass="txtboxlarge" TabIndex="49"
                                                                                                    Width="100%" Height="35px" TextMode="MultiLine" MaxLength="500" onFocus="javascript:textCounter(this,500,'yes');"
                                                                                                    onKeyDown="javascript:textCounter(this,500,'yes');" onKeyUp="javascript:textCounter(this,500,'yes');">
                                                                                                </asp:TextBox>
                                                                                                <%--<asp:TextBox ID="txtBIMQuestion2" runat="server" MaxLength="500" Width="100%" Height="100PX"
                                                                                                    TextMode="SingleLine" Wrap="true" CssClass="multi-text" />--%>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMQuestion3" runat="server" style="display: ">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion3" runat="server" />
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                            <asp:RadioButtonList runat="server" ID="rdoBIMQuestion3" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Text="Yes" Value="Y" />
                                                                                                <asp:ListItem Text="No" Value="N" />
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMQuestion3A" runat="server" style="display: none">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion3A" runat="server" />
                                                                                            <br />
                                                                                            <br />
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtBIMQuestion3A" runat="server" CssClass="txtboxlarge" TabIndex="49"
                                                                                                    Width="100%" Height="35px" TextMode="MultiLine" MaxLength="500" onFocus="javascript:textCounter(this,500,'yes');"
                                                                                                    onKeyDown="javascript:textCounter(this,500,'yes');" onKeyUp="javascript:textCounter(this,500,'yes');">
                                                                                                </asp:TextBox>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--<tr>
                                                                                        <td colspan="3" style="font-family: Tahoma; font-size: 11px; padding-left: 15px;
                                                                                            width: 80%">
                                                                                            <asp:TextBox ID="txtBIMQuestion3A" runat="server" MaxLength="500" />
                                                                                        </td>
                                                                                    </tr>--%>
                                                                                    <tr id="trBIMQuestion4" runat="server" style="display: ">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion4" runat="server" />
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                            <asp:RadioButtonList runat="server" ID="rdoBIMQuestion4" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Text="Yes" Value="Y" />
                                                                                                <asp:ListItem Text="No" Value="N" />
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMQuestion4A" runat="server" style="display: none">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion4A" runat="server" />
                                                                                            <br />
                                                                                            <br />
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtBIMQuestion4A" runat="server" CssClass="txtboxlarge" TabIndex="49"
                                                                                                    Width="100%" Height="35px" TextMode="MultiLine" MaxLength="500" onFocus="javascript:textCounter(this,500,'yes');"
                                                                                                    onKeyDown="javascript:textCounter(this,500,'yes');" onKeyUp="javascript:textCounter(this,500,'yes');">
                                                                                                </asp:TextBox>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--<tr>
                                                                                        <td colspan="2" style="font-family: Tahoma; font-size: 11px; padding-left: 15px;
                                                                                            width: 80%">
                                                                                            <asp:TextBox ID="txtBIMQuestion4A" runat="server" MaxLength="500" />
                                                                                        </td>
                                                                                    </tr>--%>
                                                                                    <tr id="trBIMQuestion5" runat="server" style="display: ">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion5" runat="server" />
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                            <asp:RadioButtonList runat="server" ID="rdoBIMQuestion5" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Text="Yes" Value="Y" />
                                                                                                <asp:ListItem Text="No" Value="N" />
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMQuestion5A" runat="server" style="display: none">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMQuestion5A" runat="server" />
                                                                                            <br />
                                                                                            <br />
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtBIMQuestion5A" runat="server" CssClass="txtboxlarge" TabIndex="49"
                                                                                                    Width="100%" Height="35px" TextMode="MultiLine" MaxLength="500" onFocus="javascript:textCounter(this,500,'yes');"
                                                                                                    onKeyDown="javascript:textCounter(this,500,'yes');" onKeyUp="javascript:textCounter(this,500,'yes');">
                                                                                                </asp:TextBox>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--<tr>
                                                                                        <td colspan="2" style="font-family: Tahoma; font-size: 11px; padding-left: 15px;
                                                                                            width: 80%">
                                                                                            <asp:TextBox ID="txtBIMQuestion5A" runat="server" MaxLength="500" />
                                                                                        </td>
                                                                                    </tr>--%>
                                                                                    <%--G. Vera 02/13/2013 - End--%>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </Content>
                                                            </Ajax:AccordionPane>

                                                            

                                                            <%--START G. Vera 05/29/2014: Added new BIM questions for Design (needed ASAP)--%>
                                                            <Ajax:AccordionPane ID="apnBIMDesign" runat="server" Visible="false">
                                                                <Header>
                                                                    Building Information Modeling Qualifications (BIM)
                                                                </Header>
                                                                <Content>
                                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                        <tr>
                                                                            <td class="exampletext">&nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%">
                                                                                    <tr id="trBIMDesignQuestion1" runat="server" style="display: ">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMDesignQuestion1" runat="server" />
                                                                                            
                                                                                            
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                            <asp:RadioButtonList runat="server" ID="rdoBIMDesignQuestion1" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Text="Yes" Value="Y" />
                                                                                                <asp:ListItem Text="No" Value="N" />
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMDesignQuestion1A" runat="server" style="display: none">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMDesignQuestion1A" runat="server" />
                                                                                            <br />
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtBIMDesignQuestion1A" runat="server" MaxLength="50" Width="100%"
                                                                                                    CssClass="txtboxlarge" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMDesignQuestion1B" runat="server" style="display: none">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMDesignQuestion1B" runat="server" />
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                            <asp:RadioButtonList runat="server" ID="rdoBIMDesignQuestion1B" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Text="Yes" Value="Y" />
                                                                                                <asp:ListItem Text="No" Value="N" />
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMDesignQuestion1C" runat="server" style="display: none">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question" >
                                                                                            <asp:Literal ID="lblBIMDesignQuestion1C" runat="server" />
                                                                                            <br />
                                                                                            <br />
                                                                                            <div >
                                                                                                <asp:TextBox ID="txtBIMDesignQuestion1C" runat="server" Width="70px" MaxLength="50" 
                                                                                                    CssClass="txtboxlarge" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                        </td>
                                                                                                                                                                            
                                                                                    </tr>
                                                                                    <tr id="trBIMDesignQuestion1D" runat="server" style="display: none">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMDesignQuestion1D" runat="server" />
                                                                                            <br />
                                                                                            <br />
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtBIMDesignQuestion1D" runat="server" Width="100%"
                                                                                                    CssClass="txtboxlarge" TabIndex="59" Height="35px" TextMode="MultiLine" MaxLength="500" onFocus="javascript:textCounter(this,500,'yes');"
                                                                                                    onKeyDown="javascript:textCounter(this,500,'yes');" onKeyUp="javascript:textCounter(this,500,'yes');" />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trBIMDesignQuestion1E" runat="server" style="display: ">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMDesignQuestion1E" runat="server" />
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                            <asp:RadioButtonList runat="server" ID="rdoBIMDesignQuestion1E" RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Text="Yes" Value="Y" />
                                                                                                <asp:ListItem Text="No" Value="N" />
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr id="trBIMDesignQuestion1F" runat="server" style="display: ">
                                                                                        <td style="vertical-align: text-top">
                                                                                            <span class="mandatorystar">*</span>
                                                                                        </td>
                                                                                        <td class="bim-question">
                                                                                            <asp:Literal ID="lblBIMDesignQuestion1F" runat="server" />
                                                                                            <br /><br />
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtBIMDesignQuestion1F" runat="server" CssClass="txtboxlarge" TabIndex="49"
                                                                                                    Width="100%" Height="35px" TextMode="MultiLine" MaxLength="500" onFocus="javascript:textCounter(this,500,'yes');"
                                                                                                    onKeyDown="javascript:textCounter(this,500,'yes');" onKeyUp="javascript:textCounter(this,500,'yes');">
                                                                                                </asp:TextBox>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="font-family: Tahoma; font-size: 11px; padding-left: 15px; width: 20%">
                                                                                        </td>
                                                                                    </tr>

                                                                                   
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </Content>
                                                            </Ajax:AccordionPane>
                                                            <%--G. Vera 05/29/2014 - End--%>

                                                        </Panes>
                                                    </Ajax:Accordion>
                                                    <asp:CustomValidator ValidationGroup="BIMQuestions" ID="cusvBIMQuestions" runat="server"
                                                        Display="None" OnServerValidate="cusvBIMQuestions_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td class="bodytextleft">
                                                                <asp:ImageButton ID="imbSaveandClose" CausesValidation="false" runat="server" ImageUrl="~/Images/save.jpg"
                                                                    ToolTip="Save" OnClick="imbSaveandClose_Click" OnClientClick="Display();" />
                                                            </td>
                                                            <td class="bodytextcenter">
                                                            </td>
                                                            <td class="bodytextright">
                                                                <asp:ImageButton ID="imbsubmit" runat="server" ImageUrl="~/Images/save_nextnew.jpg"
                                                                    ToolTip="Save and Next" OnClick="imbsubmit_Click" OnClientClick="Display();" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <%-- </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="LblHidden"
                                            PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="LblHidden" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="modalPopupDiv" runat="server" class="popupdivsmall" Style="display: none;
                                            width: 525px; height: auto">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="Td3">
                                                        <asp:Label ID="Lblheading" runat="server" class="searchhdrbarbold"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        <asp:Label ID="LblErrorMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                        <asp:Label ID="LblMsg" runat="server" CssClass="errormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="popupdivcl">
                                                        <input type="button" title="Ok" value="OK" id="imbOk" class="ModalPopupButton" onclick="$find('modalExtnd').hide();" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hdnVendorStatus" runat="server" />
                            <asp:HiddenField ID="hdnDisplay" runat="server" />
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

    <script language="javascript" type="text/javascript">

        //G. Vera 02/13/2013 - Added
        function DisplayQuestion(controlID, showIt, ofValue) {

            if (showIt == 'yes') {
                document.getElementById(controlID).style.display = "";
            }
            else {
                document.getElementById(controlID).style.display = "none";
                if (ofValue == "-1") {
                    var element1 = document.getElementById(controlID.replace('tr', 'rdo') + "_0");
                    var element2 = document.getElementById(controlID.replace('tr', 'rdo') + "_1");

                    element1.checked = false;
                    element2.checked = false;
                }
                else {
                    var element = document.getElementById(controlID.replace('tr', 'txt'));
                    element.value = "";
                }
            }
        }

        //G. Vera 10/23/2012 - Added
        function SwitchScopePanel(closePanelID, openPanelID, typeIsOpen) {

            var openPanel = document.getElementById(openPanelID);
            var closePanel = document.getElementById(closePanelID);
            var typeOpen = document.getElementById("<%=hdnScopeTypeOpen.ClientID %>");
            openPanel.style.display = '';
            openPanel.visible = true;
            typeOpen.value = typeIsOpen;
            closePanel.style.display = 'none';
            closePanel.visible = false;
            if (typeIsOpen == "PMMI") {
                document.getElementById("ctl06_liPMMI").className = "selected";
                document.getElementById("ctl06_liMasterFormat").className = "";
            }
            if (typeIsOpen == "MasterFormat") {
                document.getElementById("ctl06_liMasterFormat").className = "selected";
                document.getElementById("ctl06_liPMMI").className = "";
            }
        }

        //G. Vera 10/16/2012 - Added
        function ClearTextBox(chkBox) {

            if (!chkBox.checked) {
                document.getElementById("<%=txtChkOther.ClientID%>").value = "";
                document.getElementById("<%=txtChkOther.ClientID%>").style.display = "none";
            }

            if (chkBox.checked) document.getElementById("<%=txtChkOther.ClientID%>").style.display = "";

        }


        function OpenGrid(grd, imgurl, img) {
            if (document.getElementById(imgurl).value == "~/Images/plus.gif") {
                document.getElementById(grd).style.display = "";
                document.getElementById(img).src = "../Images/minus.gif";
                document.getElementById(imgurl).value = "~/Images/minus.gif";
            }
            else {
                document.getElementById(grd).style.display = "none";
                document.getElementById(img).src = "../Images/plus.gif";
                document.getElementById(imgurl).value = "~/Images/plus.gif";
            }
        }
        ////call the following from <body onload="load()">////required so that background blur is dimensioned correctly (with respect to scroll issues with browser)////enable js to be called after ajax postback
        function unCheckCheckALL(CheckState) {
            if (CheckState == false) {
                document.getElementById("<%=chkCheckAll.ClientID%>").checked = false;
            }
        }
        function OpenPrint() {
            window.open("../VQFView/TradeView.aspx?Print=1");
        }
        function GetChecked(chk, chkStatus, ctrl) {
            if (chk == "ALL") {
                if (chkStatus == true) {
                    var ch = document.getElementById(ctrl);

                    var chkBoxCount = ch.getElementsByTagName("input");
                    for (var i = 0; i < chkBoxCount.length; i++) {

                        chkBoxCount[i].checked = true;
                        if (chkBoxCount[i] == "Other") {
                            chkBoxCount[i].checked = false;
                        } 
                    }
                }
                else {
                    var ch = document.getElementById(ctrl);
                    var chkBoxCount = ch.getElementsByTagName("input");
                    for (var i = 0; i < chkBoxCount.length; i++) {
                        chkBoxCount[i].checked = false;
                    }
                }
            }
            else {
                var ch = document.getElementById(ctrl);
                var chkBoxCount = ch.getElementsByTagName("input");
                chkBoxCount[0].checked = false;
            }
            unCheckCheckALL(chkStatus);
        }
        function VisibleRegions(chkid, spnid, rdlid) {
            var chk = document.getElementById(chkid);
            if (chk.checked == true) {
                document.getElementById(spnid).style.display = "";
                document.getElementById(rdlid).focus();
            }
            else {
                document.getElementById(spnid).style.display = "none";
            }
        }
        function DisplayOthers(ddlState, trState, txtState) {
            var ddl = document.getElementById(ddlState);
            var ddltext = ddl.options[ddl.selectedIndex].text;
            if (ddltext == "Other") {
                document.getElementById(trState).style.display = "";
                var TXT = document.getElementById(txtState);
                TXT.value = "";
                TXT.focus();
            }
            else {
                document.getElementById(trState).style.display = "none";
            }
        }
        function Display() {
            document.getElementById("<%=hdnDisplay.ClientID%>").value = 1;
        }
        function DisplayRegions(chec, checkStatus, OtherState) {
            var ch1 = document.getElementById(checkStatus);
            if (ch1.checked == true) {
                var ch = document.getElementById(chec);
                ch.style.display = "";
            }
            else {
                var ch = document.getElementById(chec);
                ch.style.display = "none";
                DisplayLevel2(chec, checkStatus);

                if (OtherState == true) {
                    document.getElementById("<%=txtRegions.ClientID%>").value = "";
                }
            }
            unCheckCheckALL(ch1.checked);
        }
        function DisplayLevel2(chec, checkStatus) {
            var ch1 = document.getElementById(checkStatus);
            if (ch1.checked == true) {
                var ch = document.getElementById(chec);
                ch.style.display = "";
                var chkBoxCount = ch.getElementsByTagName("input");
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = true;
                }
            }
            else {
                var ch = document.getElementById(chec);
                ch.style.display = "none";
                var chkBoxCount = ch.getElementsByTagName("input");
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = false;
                }
            }
        }
        function ShowPerform() {
            if (document.getElementById("<%=rdlyeano.ClientID%>_1").checked) {
                document.getElementById("<%=trexplain.ClientID%>").style.display = "";
            }
            else {
                document.getElementById("<%=trexplain.ClientID%>").style.display = "none";
            }
        }
        function Visibletr() {
            var count = document.getElementById("<%=hdnCount.ClientID %>");
            var count1 = parseInt(count.value) + 1;
            count.value = count1;
            if (count.value == 1)
                document.getElementById("<%=spnmoreadd1.ClientID %>").style.display = "";
            else if (count.value == 2)
                document.getElementById("<%=spnmoreadd2.ClientID %>").style.display = "";
            else if (count.value == 3)
                document.getElementById("<%=spnmoreadd3.ClientID %>").style.display = "";
            else if (count.value == 4)
                document.getElementById("<%=spnmoreadd4.ClientID %>").style.display = "";
            else if (count.value == 5)
                document.getElementById("<%=spnmoreadd5.ClientID %>").style.display = "";
            else if (count.value == 6)
                document.getElementById("<%=spnmoreadd6.ClientID %>").style.display = "";
            else if (count.value == 7)
                document.getElementById("<%=spnmoreadd7.ClientID %>").style.display = "";
            else if (count.value == 8)
                document.getElementById("<%=spnmoreadd8.ClientID %>").style.display = "";
            else if (count.value == 9) {
                document.getElementById("<%=spnmoreadd9.ClientID %>").style.display = "";
                document.getElementById("<%=lnkAddMore.ClientID %>").style.display = "none";
            }
            count.value = count1;
            return false;
        }
        function returnnone(evt) {
            return false;
        }
        function Close() {
            var x = $find("ModalAddDoc");
            if (x) {
                x.hide();
            }
        }
        function CheckAll(chk, checkStatus) {
            var ch1 = document.getElementById(checkStatus);
            if (ch1.checked == true) {
                var ch = document.getElementById(chk);
                var num = ch.childNodes[0].childNodes.length;
                var currentTable = ch.childNodes[0];
                for (var i = 0; i < num; i++) {
                    var innetText = currentTable.childNodes[i].childNodes[0].value;
                    var currentTd = currentTable.childNodes[i].childNodes[0];
                    var listControl = currentTd.childNodes[0];
                    listControl.checked = true;
                }
            }
            else {
                var ch = document.getElementById(chk);
                var num = ch.childNodes[0].childNodes.length;
                var currentTable = ch.childNodes[0];
                for (var i = 0; i < num; i++) {
                    var innetText = currentTable.childNodes[i].childNodes[0].value;
                    var currentTd = currentTable.childNodes[i].childNodes[0];
                    var listControl = currentTd.childNodes[0];
                    listControl.checked = false;
                }
            }
        }
        function CheckAllRegions(Status, chkl, chkl1, chkl2, txt1) {
            // var chk=document.getElementById(Status);
            var container = document.getElementById("<%=divUS.ClientID %>");
            var single = container.getElementsByTagName("input");
            var size = single.length;
            var j = 0;
            if (Status == true) {
                document.getElementById(chkl).style.display = "";
                document.getElementById(chkl1).style.display = "";
                if (document.getElementById(chkl2)!=null)
                document.getElementById(chkl2).style.display = "";
                document.getElementById(txt1).style.display = "";
                for (i = 0; i < size; i++) {
                    if (single[i].value == "51") {
                        single[i].checked = false;
                        j = i + 1;
                        document.getElementById("<%=trRegions.ClientID%>").style.display = "none";
                    }
                    else {
                        single[i].checked = true;
                    }
                }
                single[j].checked = false;
            }
            else {
                document.getElementById(chkl).style.display = "None";
                document.getElementById(chkl1).style.display = "None";
                if (document.getElementById(chkl2) != null)
                document.getElementById(chkl2).style.display = "None";
                document.getElementById(txt1).style.display = "None";
                for (i = 0; i < size; i++) {
                    single[i].checked = false;
                    document.getElementById("<%=txtRegions.ClientID%>").value = "";
                }
            }
        }
        //Sooraj to Add Continent
        function CheckUSRegions(Status, chkl, chkl1, chkl2, txt1, control, controlToSwitch) {
            var container = document.getElementById("<%=divUS.ClientID %>");
            var single = container.getElementsByTagName("input");
            var size = single.length;
            if (Status == false) {
                document.getElementById(chkl).style.display = "None";
                document.getElementById(chkl1).style.display = "None";
                if (document.getElementById(chkl2) != null)
                document.getElementById(chkl2).style.display = "None";
                document.getElementById(txt1).style.display = "None";
                for (i = 0; i < size; i++) {
                    single[i].checked = false;
                    document.getElementById("<%=txtRegions.ClientID%>").value = "";
                }

            }
            SwitchView(control, controlToSwitch);
        }
        //Sooraj to Add Continent
        function SwitchView(control, controlToSwitch) {
            var ch = document.getElementById(control);
            var controlToHide = document.getElementById(controlToSwitch);
            if (controlToHide != null) {
                if (ch.checked == true) {
                    controlToHide.style.display = "";
                }
                else {
                    controlToHide.style.display = "None";
                    UnCheckContinent(controlToHide);
                }
            }

        }

        function UnCheckContinent(container) {

            var single = container.getElementsByTagName("input");
            if (single != null) {
                var size = single.length;

                for (i = 0; i < size; i++) {
                    single[i].checked = false;
                }
            }

        }
    </script>
    <script src="../Script/jquery.js" type="text/javascript"></script>
</body>
</html>
