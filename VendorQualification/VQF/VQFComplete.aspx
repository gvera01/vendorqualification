﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VQFComplete.aspx.cs" Inherits="VQF_VQFComplete" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>VQF Complete Page</title>
    <script language ="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
     <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
            </script>   
    <style type="text/css">
        body
        {
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top:0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server" ScriptMode="Release" LoadScriptsBeforeUI="false">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:VQFTopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0px" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:SideMenu ID="VQFMenu" runat="server" />
                                    </td>
                                    <td class="contenttd">
                                        <table width="100%" border="0px" cellpadding="0" cellspacing="0" class="searchtableclass">
                                            
                                            <tr>
                                                <td style="padding: 5px;">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                        <%--G. Vera 06/02/2014 - message change--%>
                                                            <td class="thankstxt" >
                                                                <asp:Literal ID="lblTopMessage" runat="server" >
                                                                    Thank you for registering with Haskell’s Vendor Management System. Your business will now
                                                                    be eligible for future invitations to bid by any of our project teams.
                                                                </asp:Literal>
                                                                
                                                            </td>
                                                        </tr>
                                                        <tr><td height="5px"></td></tr>
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                    
                                                                    <tr>
                                                                        <td  class="bodytextbold">
                                                                            <table cellpadding="5" cellspacing="0" border="0" width="100%" >
                                                                                <tr>
                                                                                    <td bgcolor="#cfcfcf" class="bodytextbold_right" width="100px" style="padding-left:15px">Company Name: </td>
                                                                                    <td bgcolor="#cfcfcf"><asp:Label ID="lblCompanyname" runat="server"></asp:Label></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                    <td>
                                                                     <table cellpadding="2" cellspacing="0" border="1" width="100%" >
                                                                     <tr>
                                                                        <td class="formtdlt" width="30%">
                                                                            VQF Submitted Date 
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:Label ID="lblSubmitDate" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            VQF Submitted By 
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:Label ID="lblUserName" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            Address 
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:Label ID="lblAddress" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            City 
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:Label ID="lblCity" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            State 
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:Label ID="lblState" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            ZIP/Postal Code 
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:Label ID="lblZIP" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            Phone Number 
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:Label ID="lblPhone" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            Fax Number 
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:Label ID="lblFax" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            E-mail 
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            Tracking Number 
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:Label ID="lblTrackingNumber" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                     </table>
                                                                    </td>
                                                                    </tr>
                                                                    
                                                                    <tr><td height="5px"></td></tr>
                                                                    <tr>
                                                                        <td class="thankstxt">
                                                                            <asp:Literal ID="lblBottomMessage" runat="server">
                                                                                    Thank you Your company information will be valid for one year from date of submission. Near
                                                                                    the time of expiration, the registered contact person will receive an e-mail reminder
                                                                                    to update your company data. If at any time during the year, the structure of your
                                                                                    company changes, please notify our database coordinator at <br /><br /> <a href="mailto:vendorhelpdesk@haskell.com" style="color: white;">vendorhelpdesk@haskell.com</a>.<br />
                                                                                    <br />
                                                                                    Please keep in mind that only one submission per company is necessary.
                                                                            </asp:Literal>
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        
                                                        
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <Haskell:Footer ID="BottomMenu" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>


    </script>
</body>
</html>
