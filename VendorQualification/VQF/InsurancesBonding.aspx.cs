﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using VMSDAL;
#region Change Comments
/*************************************************************************************************************
 * 001 - G. Vera 06/22/2012 - Emergency Hot Fix on Bonding validation
 * 002 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 003 G. Vera 10/19/2012: VMS Stabilization Release 1.3 (see JIRA Issues for more details)
 * 004 Sooraj Sudhakaran.T 09/24/2013: VMS Modification - Added code to hide section that do not apply to design
 * 005 Sooraj Sudhakaran.T 12/10/2013: VMS Modification - Added code to support Internationalization for phone number 
 * consultant
 * 006 G. Vera 06/30/2014: Validate section status upon Save click to get the correct status
 * 007 G. Vera 07/09/2014: Last Modified Date change upon save
 * 008 G. Vera 05/07/2015: Now, the insurenace section needs to be required for Design consultants. Reverting some
 * of the changes done on change comment 003.
 **************************************************************************************************************/
#endregion

public partial class VQF_InsurancesBonding : CommonPage
{

    #region Declaration

    bool? AdditionalInsured;
    bool EditData;
    clsCompany objCompany = new clsCompany();
    Common ObjCommon = new Common();

    long VendorId, InsuranceId;
    DataSet Attch = new DataSet();
    clsInsurance objInsurance = new clsInsurance();
    public DataSet Vendordetails = new DataSet();
    public int CountInsurance;

    Byte InsuranceStatus;


    public VMSDAL.VQFInsurance EditDate;   //003
    public VMSDAL.VQFInsurance UpdateInsuranceDate; //003

    //008
    private static string validationNote = "NOTE: You can use a zero \"0\" for fields that do not apply to your company.<br/>";
    public static string ValidationNote
    {
        get { return validationNote; }
        set { validationNote = value; }
    }
    


    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["VendorId"])) || Convert.ToString(Session["VendorId"]) == "0")
        {
            Response.Redirect("~/Common/Home.aspx?timeout=1");  //002
        }
        //002
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Common/Home.aspx?timeout=1");
        }

        if (!Page.IsPostBack)
        {
            //002
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            CountInsurance = objInsurance.GetInsuranceStatusCount(Convert.ToInt64(Session["VendorId"]));
            VendorId = Convert.ToInt64(Session["VendorId"]);
            Vendordetails = ObjCommon.GetVendorDetails(Convert.ToInt64(Session["VendorId"].ToString()));

            //Show or hide design consultant view based on Type of company - 004-sooraj
            SwitchDesignConsultantView(Vendordetails.Tables[0].Rows[0]["TypeOfCompany"].ConvertToString() == "Design Consultant");
               
            lblwelcome.Text = Vendordetails.Tables[0].Rows[0]["Company"].ToString();

            LoadEditDatas(true);
            //CallScripts();
            ShowHideControls();
            if (objCompany.GetSingleVQFCompanyData(Convert.ToInt64(Session["VendorId"])) != null)
            {
                ViewState["BusinessType"] = objCompany.GetSingleVQFCompanyData(Convert.ToInt64(Session["VendorId"])).TypeOfCompany;
            }
            else
            {
                ViewState["BusinessType"] = null;
            }

            RequiredFieldDisplay(ViewState["BusinessType"] as String);  //003
                
        }

        //003 - added below:
        if (rdoMarineCargoInlandTransit.SelectedValue == "1") trPerConveyanceLimitInlandTrans.Attributes["style"] = "display: ";
        else trPerConveyanceLimitInlandTrans.Attributes["style"] = "display: none";
        if (rdoTransitIns.SelectedValue == "1") trPerConveyanceLimitMarineIns.Attributes["style"] = "display: ";
        else trPerConveyanceLimitMarineIns.Attributes["style"] = "display: none";
        CallScripts();

        VQFMenu.GetVendorStatus();
        hdnVendorStatus.Value = Convert.ToString(VQFMenu.VendorStatus);
        if (VQFMenu.VendorStatus == 1 || VQFMenu.VendorStatus == 2)
        {
            imbSaveandClose.Visible = false;
            apnContact.Enabled =false;
            apnAggregate.Enabled=false;
            apnBonding.Enabled = false; 
            imbSaveNext.Visible = false;
            ImageButton imgAttach = (ImageButton)VQFMenu.FindControl("imbAttch");
            imgAttach.Visible = true;


        }
        if (rblProvideamt.SelectedIndex >= 0)
        {
            trnotes.Style["display"] = rblProvideamt.SelectedIndex == 0 ? "" : "none";
        }

        if (rblUmbrellaExcessliability.Items[0].Selected)
        {
            trExcess.Style["display"] = "";
            trUmbrella.Style["display"] = "";
        }
        else
        {
            trExcess.Style["display"] = "None";
            trUmbrella.Style["display"] = "None";
        }
        SwitchPhoneForKeyContact(null); //005
    }


    /// <summary>
    /// Will hide the required star on UI
    /// 003 - implemented
    /// </summary>
    /// <param name="businessType"></param>
    private void RequiredFieldDisplay(string businessType)
    {
        if (!String.IsNullOrEmpty(businessType))
        {
            //008 - need to show required stars for design consultant
            if (!businessType.Equals(VMSDAL.TypeOfCompany.Subcontractor) && 
                !businessType.Equals(VMSDAL.TypeOfCompany.EquipAndOnsiteSupport) &&
                !businessType.Equals(VMSDAL.TypeOfCompany.DesignConsultant))
            {
                //  hide required field star span
                spnRqdStar1.Attributes["class"] = "hidecontrol";
                spnRqdStar2.Attributes["class"] = "hidecontrol";
                spnRqdStar3.Attributes["class"] = "hidecontrol";
                spnRqdStar4.Attributes["class"] = "hidecontrol";
                spnRqdStar5.Attributes["class"] = "hidecontrol";
                spnRqdStar6.Attributes["class"] = "hidecontrol";
                spnRqdStar7.Attributes["class"] = "hidecontrol";
                spnRqdStar8.Attributes["class"] = "hidecontrol";
                spnRqdStar9.Attributes["class"] = "hidecontrol";
                spnRqdStar10.Attributes["class"] = "hidecontrol";
                spnRqdStar11.Attributes["class"] = "hidecontrol";
                spnRqdStar12.Attributes["class"] = "hidecontrol";
                spnRqdStar13.Attributes["class"] = "hidecontrol";
                spnRqdStar14.Attributes["class"] = "hidecontrol";
                spnRqdStar15.Attributes["class"] = "hidecontrol";
                spnRqdStar16.Attributes["class"] = "hidecontrol";
                spnRqdStar17.Attributes["class"] = "hidecontrol";
                spnRqdStar18.Attributes["class"] = "hidecontrol";
                spnRqdStar19.Attributes["class"] = "hidecontrol";
                spnRqdStar20.Attributes["class"] = "hidecontrol";
                spnRqdStar21.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar1.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar2.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar3.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar4.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar5.Attributes["class"] = "hidecontrol";
                spnConveyanceLimitInlandTrans.Attributes["class"] = "hidecontrol";

                // disable required field validators
                // bonding section
                reqvBondingSuretyCompanyNameone.Enabled = false;
                reqvCompanyContactName.Enabled = false;
                reqvTotalBondingCapacityAmt.Enabled = false;
                reqvCurrentBondingCapacityAmt.Enabled = false;

                // rest of insurance fields
                reqvBrokerorCompanyName.Enabled = false;
                reqvDiseaseEmpAmtone.Enabled = false;
                reqvDiseasePolicyAmt.Enabled = false;
                reqvEachAccidentAmt.Enabled = false;
                reqvEachOccuranceAmt1.Enabled = false;
                reqvEmpLiabilityAmt.Enabled = false;
                reqvGeneralAggregateAmt.Enabled = false;
                reqvGeneralAggregateLimit.Enabled = false;
                reqvInsuranceAgent.Enabled = false;
                reqvInsuranceCarrier.Enabled = false;
                reqvOccurrenceBased.Enabled = false;
                reqvOperationsAggregeteAmt.Enabled = false;
                reqvProvideamt.Enabled = false;
                reqvStatutoryLimits.Enabled = false;
                reqvUmbrellaExcessliability.Enabled = false;

                // marine cargo
                reqvMarineCargoConvLimit.Enabled = false;
                reqvConvLimitInlandTrans.Enabled = false;
                reqvMarineCargoInlandTransit.Enabled = false;
                reqvMarineCargoInsEquip.Enabled = false;
                reqvMarineCargoStorage.Enabled = false;
                reqvTransitIns.Enabled = false;


            }

            //008 - design consultants do not need to fill in bonding section
            // bonding section optional for Equipment and Onsite support as well
            if (businessType.Equals(VMSDAL.TypeOfCompany.EquipAndOnsiteSupport) ||
                businessType.Equals(VMSDAL.TypeOfCompany.DesignConsultant))
            {
                spnRqdStar14.Attributes["class"] = "hidecontrol";
                spnRqdStar15.Attributes["class"] = "hidecontrol";
                spnRqdStar16.Attributes["class"] = "hidecontrol";
                spnRqdStar17.Attributes["class"] = "hidecontrol";
                spnRqdStar18.Attributes["class"] = "hidecontrol";

                reqvBondingSuretyCompanyNameone.Enabled = false;
                reqvCompanyContactName.Enabled = false;
                reqvTotalBondingCapacityAmt.Enabled = false;
                reqvCurrentBondingCapacityAmt.Enabled = false;

            }

            if (businessType.Trim().Equals(VMSDAL.TypeOfCompany.DesignConsultant))
            {
                // hide the marine cargo section
                tdMarineCargoHead.Attributes.Add("style", "display: none");
                tdMarineCargoQuestions.Attributes.Add("style", "display: none");

                // hide marine cargo required stars
                spnMarineCargoStar1.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar2.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar3.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar4.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar5.Attributes["class"] = "hidecontrol";
                spnConveyanceLimitInlandTrans.Attributes["class"] = "hidecontrol";

                // disable validators (marine & cargo)
                reqvMarineCargoConvLimit.Enabled = false;
                reqvConvLimitInlandTrans.Enabled = false;
                reqvMarineCargoInlandTransit.Enabled = false;
                reqvMarineCargoInsEquip.Enabled = false;
                reqvMarineCargoStorage.Enabled = false;
                reqvTransitIns.Enabled = false;

                //008 hide insurance contact info stars
                spnRqdStar1.Attributes["class"] = "hidecontrol";
                spnRqdStar2.Attributes["class"] = "hidecontrol";
                spnRqdStar3.Attributes["class"] = "hidecontrol";
                spnRqdStar19.Attributes["class"] = "hidecontrol";

                //008 disable validators (contact info)
                reqvBrokerorCompanyName.Enabled = false;
                reqvInsuranceAgent.Enabled = false;
                cusphone.Enabled = false;
                reqvInsuranceCarrier.Enabled = false;

            }

            spnProfessional.Style["display"] = Convert.ToString(ViewState["BusinessType"]).Equals("Design Consultant") ? "" : "none";
            cusvEachOccurances.Enabled = Convert.ToString(ViewState["BusinessType"]).Equals("Design Consultant") ? true : false;


        }
    }

    #region Button Events

    /// <summary>
    /// Save and Next
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSaveNext_Click(object sender, ImageClickEventArgs e)
    {
        VendorId = Convert.ToInt64(Session["VendorId"]);
        string AgentTelephoneNumber = ObjCommon.Phoneformat(txtAgentTelephoneNo1.Text.Trim(), txtAgentTelephoneNo2.Text.Trim(), txtAgentTelephoneNo3.Text.Trim(),txtAgentTelephoneNo3);//005
        string ContactTelephoneNumber = ObjCommon.Phoneformat(txtContactTelephoneNo1.Text.Trim(), txtContactTelephoneNo2.Text.Trim(), txtContactTelephoneNo3.Text.Trim(), txtContactTelephoneNo3);//005
        
        //003 - added below
        bool? isMarineCargoIns = (!string.IsNullOrEmpty(rdoTransitIns.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoTransitIns.SelectedValue)) : (bool?)null;
        bool? isInlandTransit = (!string.IsNullOrEmpty(rdoMarineCargoInlandTransit.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoInlandTransit.SelectedValue)) : (bool?)null;
        bool? isInstallEquip = (!string.IsNullOrEmpty(rdoMarineCargoInstEquip.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoInstEquip.SelectedValue)) : (bool?)null;
        bool? isMarinceCargoStorage = (!string.IsNullOrEmpty(rdoMarineCargoStorage.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoStorage.SelectedValue)) : (bool?)null;
        txtMarineCargoConvLimit.Text = (isMarineCargoIns == false || isMarineCargoIns == null) ? "" : txtMarineCargoConvLimit.Text;
        txtPerConveyanceLimitInlandTrans.Text = (isInlandTransit == false || isInlandTransit == null) ? "" : txtPerConveyanceLimitInlandTrans.Text;

        string convLimit = txtMarineCargoConvLimit.Text;
        string convLimitTransitIns = txtPerConveyanceLimitInlandTrans.Text;
        
        reqvMarineCargoConvLimit.Enabled = (isMarineCargoIns != null) ? (bool)isMarineCargoIns : false;
        reqvConvLimitInlandTrans.Enabled = (isInlandTransit != null) ? (bool)isInlandTransit : false;
        //003 - End

        spanPollutionLiabExplainStar.Visible = txtPollutionLiability.Text == "0";
        rqfvPollutionLiabExplain.Enabled = txtPollutionLiability.Text == "0";

        bool Result = false;
        valSummary.ValidationGroup = "ContactInformation";
        valSummary.Visible = true;
        Page.Validate("ContactInformation");
        if (Page.IsValid)
        {
            valSummary.ValidationGroup = "AggregateLimits";
            Page.Validate("AggregateLimits");

            if (Page.IsValid)
            {
                valSummary.ValidationGroup = "Bonding";
                Page.Validate("Bonding");
                if (Page.IsValid)
                {
                    if (!(rblProvideamt.SelectedIndex < 0))
                    {
                        if (rblProvideamt.SelectedIndex == 0)
                        {
                            AdditionalInsured = true;
                        }
                        else if (rblProvideamt.SelectedIndex == 1)
                        {
                            AdditionalInsured = false;
                        }
                    }
                    else
                    {
                        AdditionalInsured = null;
                    }

                    int CountInsurance = objInsurance.GetInsuranceCount(Convert.ToInt64(Session["VendorId"]));
                    if (CountInsurance == 0)
                    {
                        InsuranceStatus = 1;
                        Result = objInsurance.InsertInsurance(txtBrokerorCompanyName.Text.Trim(), txtInsuranceAgent.Text.Trim(), AgentTelephoneNumber, txtInsuranceCarrier.Text.Trim(), 
                                                              txtEachOccuranceAmt1.Text.Trim(), txtOperationsAggregeteAmt.Text.Trim(), txtGeneralAggregateAmt.Text.Trim(), AdditionalInsured, 
                                                              txtAccidentamt.Text.Trim(), txtEmpLiabilityAmt.Text.Trim(), txtDiseaseEmpAmt.Text.Trim(), txtDiseasePolicyAmtone.Text.Trim(), 
                                                              txtEachOccuranceAmt2.Text.Trim(), txtBondingSuretyCompanyNameone.Text.Trim(), txtCompanyContactName.Text.Trim(), 
                                                              ContactTelephoneNumber, txtTotalBondingCapacityAmt.Text.Trim(), txtCurrentBondingCapacityAmt.Text.Trim(), 
                                                              Convert.ToInt64(Session["VendorId"]), InsuranceStatus, rblOccurrenceBased.Items[0].Selected, rblGeneralAggregateLimit.Items[0].Selected,
                                                              rblUmbrellaExcessliability.Items[0].Selected, txtUmbrella.Text, txtExcess.Text, rblStatutoryLimits.Items[0].Selected, 
                                                              isMarineCargoIns, isInlandTransit, isInstallEquip, isMarinceCargoStorage, convLimit, convLimitTransitIns, !chkAgentUSA.Checked,!chkContactUSA.Checked, txtPollutionLiability.Text, txtPollutionLiabExplain.Text);//005
                        Session["Flag"] = 1;
                    }
                    else
                    {
                        InsuranceStatus = 2;
                        if (!string.IsNullOrEmpty(hdnInsuranceId.Value))
                        {
                            InsuranceId = Convert.ToInt64(hdnInsuranceId.Value);
                            
                            Result = objInsurance.UpdateInsurance(txtBrokerorCompanyName.Text.Trim(), txtInsuranceAgent.Text.Trim(), AgentTelephoneNumber, txtInsuranceCarrier.Text.Trim(), 
                                                                  txtEachOccuranceAmt1.Text.Trim(), txtOperationsAggregeteAmt.Text.Trim(), txtGeneralAggregateAmt.Text.Trim(), 
                                                                  AdditionalInsured, txtAccidentamt.Text.Trim(), "", txtEmpLiabilityAmt.Text.Trim(), txtDiseaseEmpAmt.Text.Trim(), 
                                                                  txtDiseasePolicyAmtone.Text.Trim(), txtEachOccuranceAmt2.Text.Trim(), txtBondingSuretyCompanyNameone.Text.Trim(), 
                                                                  txtCompanyContactName.Text.Trim(), ContactTelephoneNumber, txtTotalBondingCapacityAmt.Text.Trim(), 
                                                                  txtCurrentBondingCapacityAmt.Text.Trim(), InsuranceId, InsuranceStatus, rblOccurrenceBased.Items[0].Selected, 
                                                                  rblGeneralAggregateLimit.Items[0].Selected, rblUmbrellaExcessliability.Items[0].Selected, txtUmbrella.Text, txtExcess.Text,
                                                                  rblStatutoryLimits.Items[0].Selected, isMarineCargoIns, isInlandTransit, isInstallEquip, isMarinceCargoStorage, convLimit, convLimitTransitIns, 
                                                                  !chkAgentUSA.Checked, !chkContactUSA.Checked, txtPollutionLiability.Text, txtPollutionLiabExplain.Text);//005

                            UpdateInsuranceDate = objInsurance.GetSingleVQLInsuranceDate(VendorId);

                        
                        }
                    }
                    //Get InsuranceID
                    if (Result == true)
                    {
                        VMSDAL.VQFInsurance EditInsuranceData = objInsurance.GetSingleVQLInsuranceDate(VendorId);   //003
                        hdnInsuranceId.Value = Convert.ToString(EditInsuranceData.PK_InsuranceID);
                        VQFMenu.LastUpdatedDate(Convert.ToInt32(Session["VendorId"]), DateTime.Now);        //007

                        if (!string.IsNullOrEmpty(hdnInsuranceId.Value))
                        {
                            InsuranceId = Convert.ToInt64(hdnInsuranceId.Value);

                            VQFMenu.GetCompleteStatus();
                            VQFMenu.GetIncompleteStatus();
                            VQFMenu.imbSubmitStatus_Click(sender, e); 
                        }
                    }

                    
                }
                else
                {
                    InsuranceStatus = 0;                //001
                    lblheading.Text = "Bonding and Surety - Validation";
                    lblMsg.Text = "";
                    lblError.Text = ValidationNote;     //008
                    modalExtnd.Show();
                    accBonding.SelectedIndex = 2;
                }
            }
            else
            {
                InsuranceStatus = 0;                //001
                lblheading.Text = "Aggregate Limits / Coverage - Validation";
                lblMsg.Text = "";
                lblError.Text = ValidationNote;     //008
                modalExtnd.Show();
                accBonding.SelectedIndex = 1;
            }
        }
        else
        {
            InsuranceStatus = 0;                //001
            lblheading.Text = "Contact Information - Validation";
            lblMsg.Text = "";
            lblError.Text = ValidationNote;     //008
            modalExtnd.Show();
            accBonding.SelectedIndex = 0;
        }

        imbSaveandClose_Click(null, null);  //001
    }

    /// <summary>
    /// Save and close
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSaveandClose_Click(object sender, ImageClickEventArgs e)
    {
        VendorId = Convert.ToInt64(Session["VendorId"]);
        bool? isPolicy = null, isGeneral = null, isUmbrellaExcess = null, isWCPolicy = null;

        string AgentTelephoneNumber = ObjCommon.CheckPhone(txtAgentTelephoneNo1.Text.Trim(), txtAgentTelephoneNo2.Text.Trim(), txtAgentTelephoneNo3.Text.Trim(),txtAgentTelephoneNo3);//005
        string ContactTelephoneNumber = ObjCommon.CheckPhone(txtContactTelephoneNo1.Text.Trim(), txtContactTelephoneNo2.Text.Trim(), txtContactTelephoneNo3.Text.Trim(), txtContactTelephoneNo3);//005
        bool Result = false;
        
        AdditionalInsured = rblProvideamt.SelectedIndex >= 0 ? rblProvideamt.Items[0].Selected : new Nullable<bool>();
        isPolicy = rblOccurrenceBased.SelectedIndex >= 0 ? rblOccurrenceBased.Items[0].Selected : new Nullable<bool>();
        isGeneral = rblGeneralAggregateLimit.SelectedIndex >= 0 ? rblGeneralAggregateLimit.Items[0].Selected : new Nullable<bool>();
        isWCPolicy = rblStatutoryLimits.SelectedIndex >= 0 ? rblStatutoryLimits.Items[0].Selected : new Nullable<bool>();
        isUmbrellaExcess = rblUmbrellaExcessliability.SelectedIndex >= 0 ? rblUmbrellaExcessliability.Items[0].Selected : new Nullable<bool>();
        //003 - added below
        bool? isMarineCargoIns = (!string.IsNullOrEmpty(rdoTransitIns.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoTransitIns.SelectedValue)) : (bool?)null;
        bool? isInlandTransit = (!string.IsNullOrEmpty(rdoMarineCargoInlandTransit.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoInlandTransit.SelectedValue)) : (bool?)null;
        bool? isInstallEquip = (!string.IsNullOrEmpty(rdoMarineCargoInstEquip.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoInstEquip.SelectedValue)) : (bool?)null;
        bool? isMarinceCargoStorage = (!string.IsNullOrEmpty(rdoMarineCargoStorage.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoStorage.SelectedValue)) : (bool?)null;
        txtMarineCargoConvLimit.Text = (isMarineCargoIns == false || isMarineCargoIns == null) ? "" : txtMarineCargoConvLimit.Text;
        txtPerConveyanceLimitInlandTrans.Text = (isInlandTransit == false || isInlandTransit == null) ? "" : txtPerConveyanceLimitInlandTrans.Text;

        string convLimit = txtMarineCargoConvLimit.Text;
        string convLimitTransitIns = txtPerConveyanceLimitInlandTrans.Text;

        reqvMarineCargoConvLimit.Enabled = (isMarineCargoIns != null) ? (bool)isMarineCargoIns : false;
        reqvConvLimitInlandTrans.Enabled = (isInlandTransit != null) ? (bool)isInlandTransit : false;

        //003 - End

        spanPollutionLiabExplainStar.Visible = txtPollutionLiability.Text == "0";
        rqfvPollutionLiabExplain.Enabled = txtPollutionLiability.Text == "0";

        int CountInsurance = objInsurance.GetInsuranceCount(Convert.ToInt64(Session["VendorId"]));
        if (CountInsurance == 0)
        {
            InsuranceStatus = Convert.ToByte(this.IsPageValid());   //006
            //001 - passing new marine/cargo parameters
            Result = objInsurance.InsertInsurance(txtBrokerorCompanyName.Text.Trim(), txtInsuranceAgent.Text.Trim(), AgentTelephoneNumber, 
                                            txtInsuranceCarrier.Text.Trim(), txtEachOccuranceAmt1.Text.Trim(), txtOperationsAggregeteAmt.Text.Trim(), 
                                            txtGeneralAggregateAmt.Text.Trim(), AdditionalInsured, txtAccidentamt.Text.Trim(), txtEmpLiabilityAmt.Text.Trim(), 
                                            txtDiseaseEmpAmt.Text.Trim(), txtDiseasePolicyAmtone.Text.Trim(), txtEachOccuranceAmt2.Text.Trim(), 
                                            txtBondingSuretyCompanyNameone.Text.Trim(), txtCompanyContactName.Text.Trim(), ContactTelephoneNumber, 
                                            txtTotalBondingCapacityAmt.Text.Trim(), txtCurrentBondingCapacityAmt.Text.Trim(), Convert.ToInt64(Session["VendorId"]),
                                            InsuranceStatus, isPolicy, isGeneral, isUmbrellaExcess, txtUmbrella.Text.Trim(), txtExcess.Text.Trim(), isWCPolicy, isMarineCargoIns,
                                            isInlandTransit, isInstallEquip, isMarinceCargoStorage, convLimit, convLimitTransitIns, !chkAgentUSA.Checked, !chkContactUSA.Checked, txtPollutionLiability.Text, txtPollutionLiabExplain.Text);//005
            VQFMenu.GetCompleteStatus();
            VQFMenu.GetIncompleteStatus();
        }
        else
        {
            InsuranceStatus = Convert.ToByte(this.IsPageValid());   //006
            if (!string.IsNullOrEmpty(hdnInsuranceId.Value))
            {
                InsuranceId = Convert.ToInt64(hdnInsuranceId.Value);
                //001 - passing new marine/cargo parameters
                Result = objInsurance.UpdateInsurance(txtBrokerorCompanyName.Text.Trim(), txtInsuranceAgent.Text.Trim(), AgentTelephoneNumber, 
                                            txtInsuranceCarrier.Text.Trim(), txtEachOccuranceAmt1.Text.Trim(), txtOperationsAggregeteAmt.Text.Trim(), 
                                            txtGeneralAggregateAmt.Text.Trim(), AdditionalInsured, txtAccidentamt.Text.Trim(), "", 
                                            txtEmpLiabilityAmt.Text.Trim(), txtDiseaseEmpAmt.Text.Trim(), txtDiseasePolicyAmtone.Text.Trim(), 
                                            txtEachOccuranceAmt2.Text.Trim(), txtBondingSuretyCompanyNameone.Text.Trim(), txtCompanyContactName.Text.Trim(), 
                                            ContactTelephoneNumber, txtTotalBondingCapacityAmt.Text.Trim(), txtCurrentBondingCapacityAmt.Text.Trim(), 
                                            InsuranceId, InsuranceStatus, isPolicy, isGeneral, isUmbrellaExcess, txtUmbrella.Text.Trim(), txtExcess.Text.Trim(),
                                            isWCPolicy, isMarineCargoIns, isInlandTransit, isInstallEquip, isMarinceCargoStorage, convLimit, convLimitTransitIns, !chkAgentUSA.Checked,!chkContactUSA.Checked, txtPollutionLiability.Text, txtPollutionLiabExplain.Text);//005

                VQFMenu.GetCompleteStatus();
                VQFMenu.GetIncompleteStatus();
                
            }
        }
        //Get InsuranceID
        if (Result)
        {
            VMSDAL.VQFInsurance EditInsuranceData = objInsurance.GetSingleVQLInsuranceDate(VendorId);   //002
            hdnInsuranceId.Value = Convert.ToString(EditInsuranceData.PK_InsuranceID);
            InsuranceId = Convert.ToInt64(hdnInsuranceId.Value);
            if (e != null)
            {
                valSummary.Visible = false; //006  need to hide the validation messages on save and close
                lblMsg.Text = "&nbsp;<li>Insurance & bonding details saved successfully</li>";
                lblError.Text = ""; //008
                lblheading.Text = "Result";
                modalExtnd.Show();
            }
                
        }

        VQFMenu.LastUpdatedDate(Convert.ToInt32(Session["VendorId"]), DateTime.Now);        //007
        LoadEditDatas(true);
    }

    #endregion

    #region Validation Controls

    protected void cusvEachOccurances_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;    //003

        if (Convert.ToString(ViewState["BusinessType"]).Equals("Design Consultant"))
        //if (Convert.ToString(ViewState["BusinessType"]).Equals("Subcontractor") || 
        //    Convert.ToString(ViewState["BusinessType"]).Equals("Equip/Onsite Support"))
        {
            if (string.IsNullOrEmpty(txtEachOccuranceAmt2.Text.Trim()))
            {
                cusvEachOccurances.ErrorMessage = "Please enter each occurrence under professional liability";
                args.IsValid = false;
            }
        }
    }

    protected void cusphone_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        args.IsValid = true;    //003

        if (Convert.ToString(ViewState["BusinessType"]).Equals("Subcontractor") ||
            Convert.ToString(ViewState["BusinessType"]).Equals("Equip/Onsite Support"))
        {
            if (((txtAgentTelephoneNo1.Text == "") && ((txtAgentTelephoneNo2.Text == "") && (txtAgentTelephoneNo3.Text == ""))))
            {
                cusphone.ErrorMessage = "Please enter agent phone number under contact information";
                args.IsValid = false;
            }
            else
            {
                bool isValid = ObjCommon.ValidatePhoneformat(txtAgentTelephoneNo1.Text.Trim(), txtAgentTelephoneNo2.Text.Trim(), txtAgentTelephoneNo3.Text.Trim(), txtAgentTelephoneNo3);//005
                if (!isValid)
                {
                    cusphone.ErrorMessage = "Please enter valid agent phone number under contact information";
                    args.IsValid = false;
                }
            } 
        }
    }

    protected void cusContactPhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;    //003

        if (Convert.ToString(ViewState["BusinessType"]).Equals("Subcontractor"))
        {
            if (((txtContactTelephoneNo1.Text == "") && ((txtContactTelephoneNo2.Text == "") && (txtContactTelephoneNo3.Text == ""))))
            {
                cusvContactPhone.ErrorMessage = "Please enter contact telephone number under bonding and surety";
                args.IsValid = false;
            }
            else
            {
                bool isValid = ObjCommon.ValidatePhoneformat(txtContactTelephoneNo1.Text.Trim(), txtContactTelephoneNo2.Text.Trim(), txtContactTelephoneNo3.Text.Trim(), txtContactTelephoneNo3);//005
                if (!isValid)
                {
                    cusvContactPhone.ErrorMessage = "Please enter valid contact telephone number under bonding and surety";
                    args.IsValid = false;
                }
            } 
        }
    }

    protected void custTxtUmbrella_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;    //003

        //008 - validate consultants as well
        if (Convert.ToString(ViewState["BusinessType"]).Equals("Subcontractor") || 
            Convert.ToString(ViewState["BusinessType"]).Equals("Equip/Onsite Support") ||
            Convert.ToString(ViewState["BusinessType"]).Equals("Design Consultant"))
        {
            if ((rblUmbrellaExcessliability.Items[0].Selected) && (string.IsNullOrEmpty(txtUmbrella.Text)))
            {
                custTxtUmbrella.ErrorMessage = "Please enter umbrella each occurence/aggregate";
                args.IsValid = false;
            } 
        }
    }

    protected void cusvTxtExcess_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;    //003
        
        //008 - validate consultants as well
        if (Convert.ToString(ViewState["BusinessType"]).Equals("Subcontractor") || 
            Convert.ToString(ViewState["BusinessType"]).Equals("Equip/Onsite Support") ||
            Convert.ToString(ViewState["BusinessType"]).Equals("Design Consultant"))
        {
            if ((rblUmbrellaExcessliability.Items[0].Selected) && (string.IsNullOrEmpty(txtExcess.Text)))
            {
                cusvTxtExcess.ErrorMessage = "Please enter excess each occurence/aggregate";
                args.IsValid = false;
            } 
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// 006 - added page validation method to be called from the save and close button event
    /// </summary>
    /// <returns></returns>
    private bool IsPageValid()
    {
        valSummary.Visible = true;

        Page.Validate("ContactInformation");
        if (Page.IsValid)
        {
            valSummary.ValidationGroup = "AggregateLimits";
            Page.Validate("AggregateLimits");

            if (Page.IsValid)
            {
                valSummary.ValidationGroup = "Bonding";
                Page.Validate("Bonding");
                return Page.IsValid;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    //004
    /// <summary>
    /// Switch view based on logged in user
    /// </summary>
    /// <param name="IsShow"></param>
    /// <Author>Sooraj Sudhakaran.T</Author>
    ///  <Date>20-SEP-2013</Date>
    private void SwitchDesignConsultantView(bool IsShow)
    {
        //Design Consultant 
        apnBonding.Visible = !IsShow;
    }


    /// <summary>
    /// Display panel based on sub-menu selection.
    /// </summary>
    private void ShowHideControls()
    {
        switch (Convert.ToString(Request.QueryString["submenu"]))
        {
            case "1":
                accBonding.SelectedIndex = 0;
                break;
            case "2":
                accBonding.SelectedIndex = 1;
                break;
            case "3":
                accBonding.SelectedIndex = 2;
                break;
        }
        
        spanPollutionLiabExplainStar.Visible = txtPollutionLiability.Text == "0";
        rqfvPollutionLiabExplain.Enabled = txtPollutionLiability.Text == "0";

    }

    /// <summary>
    /// Append Client-side events to controls.
    /// </summary>
    private void CallScripts()
    {
        txtContactTelephoneNo1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtContactTelephoneNo1.ClientID + "," + txtContactTelephoneNo2.ClientID + ")");
        txtContactTelephoneNo2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtContactTelephoneNo2.ClientID + "," + txtContactTelephoneNo3.ClientID + ")");

        txtAgentTelephoneNo1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtAgentTelephoneNo1.ClientID + "," + txtAgentTelephoneNo2.ClientID + ")");
        txtAgentTelephoneNo2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtAgentTelephoneNo2.ClientID + "," + txtAgentTelephoneNo3.ClientID + ")");

        txtEachOccuranceAmt1.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtEachOccuranceAmt1.ClientID + "')");
        txtEachOccuranceAmt1.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtEachOccuranceAmt1.ClientID + "')");
        txtOperationsAggregeteAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtOperationsAggregeteAmt.ClientID + "')");

        txtGeneralAggregateAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtGeneralAggregateAmt.ClientID + "')");
        txtAccidentamt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAccidentamt.ClientID + "')");

        txtEmpLiabilityAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtEmpLiabilityAmt.ClientID + "')");
        txtDiseaseEmpAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtDiseaseEmpAmt.ClientID + "')");
        txtDiseasePolicyAmtone.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtDiseasePolicyAmtone.ClientID + "')");
        txtEachOccuranceAmt2.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtEachOccuranceAmt2.ClientID + "')");
        txtPollutionLiability.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtPollutionLiability.ClientID + "')");
        txtTotalBondingCapacityAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalBondingCapacityAmt.ClientID + "')");
        //001 - Added line below
        txtCurrentBondingCapacityAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtCurrentBondingCapacityAmt.ClientID + "');validateBondingCapacity();");
        txtCurrentBondingCapacityAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtCurrentBondingCapacityAmt.ClientID + "');validateBondingCapacity();");
        txtOperationsAggregeteAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtOperationsAggregeteAmt.ClientID + "')");
        txtGeneralAggregateAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtGeneralAggregateAmt.ClientID + "')");
        txtAccidentamt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtAccidentamt.ClientID + "')");
        txtEmpLiabilityAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtEmpLiabilityAmt.ClientID + "')");
        txtDiseaseEmpAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtDiseaseEmpAmt.ClientID + "')");
        txtDiseasePolicyAmtone.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtDiseasePolicyAmtone.ClientID + "')");
        txtEachOccuranceAmt2.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtEachOccuranceAmt2.ClientID + "')");
        txtPollutionLiability.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtPollutionLiability.ClientID + "')");
        txtTotalBondingCapacityAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtTotalBondingCapacityAmt.ClientID + "')");
        txtCurrentBondingCapacityAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtCurrentBondingCapacityAmt.ClientID + "')");
        txtUmbrella.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtUmbrella.ClientID + "')");
        txtUmbrella.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtUmbrella.ClientID + "')");
        txtExcess.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtExcess.ClientID + "')");
        txtExcess.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtExcess.ClientID + "')");

        //003 - added below:
        txtMarineCargoConvLimit.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMarineCargoConvLimit.ClientID + "')");
        txtMarineCargoConvLimit.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtMarineCargoConvLimit.ClientID + "')");
        txtPerConveyanceLimitInlandTrans.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtPerConveyanceLimitInlandTrans.ClientID + "')");
        txtPerConveyanceLimitInlandTrans.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtPerConveyanceLimitInlandTrans.ClientID + "')");
        rdoMarineCargoInlandTransit.Items[0].Attributes.Add("onclick", @"javascript:OpenHideControl('" + trPerConveyanceLimitInlandTrans.ClientID + "'" + ",this.value)");
        rdoMarineCargoInlandTransit.Items[1].Attributes.Add("onclick", @"javascript:OpenHideControl('" + trPerConveyanceLimitInlandTrans.ClientID + "'" + ",this.value)");
        rdoTransitIns.Items[0].Attributes.Add("onclick", @"javascript:OpenHideControl('" + trPerConveyanceLimitMarineIns.ClientID + "'" + ",this.value)");
        rdoTransitIns.Items[1].Attributes.Add("onclick", @"javascript:OpenHideControl('" + trPerConveyanceLimitMarineIns.ClientID + "'" + ",this.value)");


        //005 - Sooraj --Internationalization for Phone 
        chkAgentUSA.Attributes.Add("OnClick", "SwitchPhone(this,'" + txtAgentTelephoneNo1.ClientID + "','" + txtAgentTelephoneNo2.ClientID + "','" + txtAgentTelephoneNo3.ClientID + "','" + spnPPhone2.ClientID + "')");
        chkContactUSA.Attributes.Add("OnClick", "SwitchPhone(this,'" + txtContactTelephoneNo1.ClientID + "','" + txtContactTelephoneNo2.ClientID + "','" + txtContactTelephoneNo3.ClientID + "','" + spnCPhone2.ClientID + "')");
     

    }


    /// <summary>
    /// 005
    /// </summary>
    /// <param name="val"></param>
    /// <param name="phone1"></param>
    /// <param name="phone2"></param>
    /// <param name="phone3"></param>
    /// <param name="spnPhone"></param>
    private void SwitchPhone(CheckBox val, TextBox phone1, TextBox phone2, TextBox phone3,  HtmlControl spnPhone)
    {

        if (val.Checked)//Non-US
        {
            val.Text = "Non-US";
            phone3.Style["display"] = "none";

            phone1.MaxLength = 5;
            phone2.MaxLength = 15;
            phone2.CssClass = "txtbox";
            phone3.Text = "";
            spnPhone.Style["display"] = "none";
            phone1.Attributes.Remove("onkeyup");
            phone2.Attributes.Remove("onkeyup");
        }
        else  //US
        {
            phone1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + phone1.ClientID + "," + phone2.ClientID + ")");
            phone2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + phone2.ClientID + "," + phone3.ClientID + ")");
            // val.Text = "US";
            phone3.Style["display"] = "";
            phone2.CssClass = "txtboxsmall";
            phone1.MaxLength = 3;
            phone2.MaxLength = 3;

            spnPhone.Style["display"] = "";

        }

    }


    /// <summary>
    /// 005-Switch phone for Key Contacts 
    /// </summary>
    /// <param name="chk"></param>
    private void SwitchPhoneForKeyContact(CheckBox chk)
    {
        if (chk != null)
        {
            switch (chk.ID)
            {
                case "chkContactUSA":
                    SwitchPhone(chkContactUSA, txtContactTelephoneNo1, txtContactTelephoneNo2, txtContactTelephoneNo3,  spnCPhone2);
                    break;
                case "chkAgentUSA":
                    SwitchPhone(chkAgentUSA, txtAgentTelephoneNo1, txtAgentTelephoneNo2, txtAgentTelephoneNo3,  spnPPhone2);

                    break;

            }
        }
        else
        {
            SwitchPhone(chkContactUSA, txtContactTelephoneNo1, txtContactTelephoneNo2, txtContactTelephoneNo3, spnCPhone2);
            SwitchPhone(chkAgentUSA, txtAgentTelephoneNo1, txtAgentTelephoneNo2, txtAgentTelephoneNo3, spnPPhone2);
        }

    }

    /// <summary>
    /// Populate data from data store.
    /// </summary>
    /// <param name="IsMyProfile"></param>
    private void LoadEditDatas(bool IsMyProfile)
    {
        VendorId = Convert.ToInt64(Session["VendorId"]);
        EditData = true;
        //VendorId =10;
        bool? isPolicy, isGeneral, isUmbrellaExcess, isWCPolicy;
        int CountInsurance = objInsurance.GetInsuranceCount(Convert.ToInt64(Session["VendorId"]));

        if (CountInsurance > 0)
        {
            EditDate = objInsurance.GetSingleVQLInsuranceDate(VendorId);
            if (EditDate != null)
            {
                hdnInsuranceId.Value = Convert.ToString(EditDate.PK_InsuranceID);
                LoadMarineCargoInsurance(EditDate); //003
                long InsuranceId = EditDate.PK_InsuranceID;
                txtBrokerorCompanyName.Text = EditDate.Broker_CompanyName;
                txtInsuranceAgent.Text = EditDate.InsuranceAgent;
                chkAgentUSA.Checked = !EditDate.InsuranceAgentIsBasedInUS.Value;
                SwitchPhoneForKeyContact(chkAgentUSA);
                string AgentTeleNo = EditDate.AgentTelephoneNumber;
                ObjCommon.loadphone(txtAgentTelephoneNo1, txtAgentTelephoneNo2, txtAgentTelephoneNo3, AgentTeleNo);
                txtInsuranceCarrier.Text = EditDate.InsuranceCarrier;
                txtEachOccuranceAmt1.Text = EditDate.GeneralEachOccurrence;
                txtOperationsAggregeteAmt.Text = EditDate.Product_CompletedOperations;
                txtGeneralAggregateAmt.Text = EditDate.GeneralAggregate;
                bool? AdditionalInsured = EditDate.AdditionalInsured;
                if (AdditionalInsured != null)
                {
                    if (AdditionalInsured == true)
                    {
                        rblProvideamt.SelectedIndex = 0;
                        trnotes.Style["display"] = "";
                    }
                    else
                    {
                        rblProvideamt.SelectedIndex = 1;
                        trnotes.Style["display"] = "none";
                    }
                }
                isPolicy = EditDate.IsPolicy;
                if (isPolicy != null)
                {
                    rblOccurrenceBased.SelectedIndex = Convert.ToBoolean(isPolicy) ? 0 : 1;
                }

                isGeneral = EditDate.IsGeneral;
                if (isGeneral != null)
                {
                    rblGeneralAggregateLimit.SelectedIndex = Convert.ToBoolean(isGeneral) ? 0 : 1;
                }
                isUmbrellaExcess = EditDate.Umbrella_Excess;
                if (isUmbrellaExcess != null)
                {
                    if (isUmbrellaExcess == true)
                    {
                        rblUmbrellaExcessliability.SelectedIndex = 0;
                        trUmbrella.Style["Display"] = "";
                        trExcess.Style["Display"] = "";
                    }
                    else
                    {
                        rblUmbrellaExcessliability.SelectedIndex = 1;
                        trUmbrella.Style["Display"] = "None";
                        trExcess.Style["Display"] = "None";
                    }
                }
                else
                {
                    trUmbrella.Style["Display"] = "None";
                    trExcess.Style["Display"] = "None";
                }

                txtUmbrella.Text = EditDate.Umbrella;
                txtExcess.Text = EditDate.Excess;
                isWCPolicy = EditDate.WCPolicy;
                if (isWCPolicy != null)
                {
                    rblStatutoryLimits.SelectedIndex = Convert.ToBoolean(isWCPolicy) ? 0 : 1;
                }
                txtAccidentamt.Text = EditDate.EachAccident;
                //txtStateRequirementsAmt.Text = EditDate.StateRequirements;
                txtEmpLiabilityAmt.Text = EditDate.EmployersLiability;
                txtDiseaseEmpAmt.Text = EditDate.DiseaseEachEmployee;
                txtDiseasePolicyAmtone.Text = EditDate.DiseasePolicyLimit;
                txtEachOccuranceAmt2.Text = EditDate.ProfessionalEachOccurrence;
                txtPollutionLiability.Text = EditDate.PollutionPolicyAmount != default(decimal?) ? EditDate.PollutionPolicyAmount.ToString().Substring(0,EditDate.PollutionPolicyAmount.ToString().Length-3) : string.Empty;
                txtPollutionLiabExplain.Text = EditDate.PollutionPolicyExplain ?? "";
                txtBondingSuretyCompanyNameone.Text = EditDate.Bonding_SuretyCompanyName;
                txtCompanyContactName.Text = EditDate.CompanyContactName;
                chkContactUSA.Checked = !EditDate.ContactIsBasedInUS.Value; //005
                SwitchPhoneForKeyContact(chkContactUSA);
                string ContactTeleNo = EditDate.ContactTelephoneNumber;
                ObjCommon.loadphone(txtContactTelephoneNo1, txtContactTelephoneNo2, txtContactTelephoneNo3, ContactTeleNo);
                txtTotalBondingCapacityAmt.Text = EditDate.TotalBondingCapacity;
                txtCurrentBondingCapacityAmt.Text = EditDate.CurrentBondingCapacity;

            }
        }
    }

    /// <summary>
    /// 003 - implemented
    /// </summary>
    /// <param name="EditDate"></param>
    private void LoadMarineCargoInsurance(VMSDAL.VQFInsurance EditDate)
    {
        rdoMarineCargoInlandTransit.Items[1].Selected = (EditDate.IsInlandTransit != null) ? !(bool)EditDate.IsInlandTransit : false;
        rdoMarineCargoInlandTransit.Items[0].Selected = (EditDate.IsInlandTransit != null) ? (bool)EditDate.IsInlandTransit : false;
        rdoTransitIns.Items[1].Selected = (EditDate.IsMarineCargoInsurance != null) ? !(bool)EditDate.IsMarineCargoInsurance : false;
        rdoTransitIns.Items[0].Selected = (EditDate.IsMarineCargoInsurance != null) ? (bool)EditDate.IsMarineCargoInsurance : false;
        rdoMarineCargoStorage.Items[1].Selected = (EditDate.IsStorage != null) ? !(bool)EditDate.IsStorage : false;
        rdoMarineCargoStorage.Items[0].Selected = (EditDate.IsStorage != null) ? (bool)EditDate.IsStorage : false;
        rdoMarineCargoInstEquip.Items[1].Selected = (EditDate.IsInstallationOfEquip != null) ? !(bool)EditDate.IsInstallationOfEquip : false;
        rdoMarineCargoInstEquip.Items[0].Selected = (EditDate.IsInstallationOfEquip != null) ? (bool)EditDate.IsInstallationOfEquip : false;
        txtMarineCargoConvLimit.Text = (EditDate.ConveyanceLimitMarineInsurance != null) ? EditDate.ConveyanceLimitMarineInsurance : string.Empty;
        txtPerConveyanceLimitInlandTrans.Text = (EditDate.ConveyanceLimitInlandTransit != null) ? EditDate.ConveyanceLimitInlandTransit : string.Empty;

    }

    #endregion

}
