using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using VMSDAL;

#region Change Comments
/****************************************************************************************************************
 * 001 - G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 - G. Vera 10/16/2012: VMS Stabilization Release 1.3 (JIRA:VPI-58)
 * 003 - G. Vera 02/13/2013: New Features version 1.3.1 (JIRA:VPI-93)
 * 004 Sooraj Sudhakaran.T 09/28/2013: VMS Modification -Design consultant scopes
 * 005 Sooraj Sudhakaran.T 09/30/2013: VMS Modification - Added code to hide section that do not apply to design consultant
 * 006 Sooraj Sudhakaran.T 10/11/2013: VMS Modification - Added code to handle continent selection for trade regions 
 * 007 Sooraj Sudhakaran.T 12/14/2013: VMS Modification - Added code to handle Internationalization for License State 
 * 008 - G. Vera 05/29/2014:  New BIM Questions for Design Consultant
 * 009 G. Vera 06/302014: Validate section status upon Save click to get the correct status
 * 010 G. Vera 07/09/2014: Last modifed date change upon save
 * 011 G. Vera 05/06/2016: Add max length validation to the BIM answers in case client script is diabled on their browsers
 ****************************************************************************************************************/
#endregion


public partial class VQF_TradeInformation : CommonPage
{

    #region Declaration

    bool? Forces;
    DateTime ExpirationDate;
    Common objCommon = new Common();
    clsTradeInfomation objTradeInformation = new clsTradeInfomation();
    clsCompany objCompany = new clsCompany();
    static List<VendorTradeQuestions> BIMQuestions = new List<VendorTradeQuestions>();      //003 - added
    static bool IsDesignConsultant = false;     //008
    public int CountTrade;
    long VendorId;

    #endregion

    #region Page Events

    protected void Page_UnLoad(object sender, EventArgs e)
    {

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DAL.HaskellDataContext objContext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);


        //002

       

        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Common/Home.aspx?timeout=1");
        }
        //check for vendor id. if null redirect to home page.
        if (string.IsNullOrEmpty(Convert.ToString(Session["VendorId"])) || Convert.ToString(Session["VendorId"]) == "0")
        {

            Response.Redirect("~/Common/Home.aspx?timeout=1");  //001
        }
        else
        {
            if (!Page.IsPostBack)
            {
                txtChkOther.Attributes.Add("style", "display: none;");      //002

                //001
                Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

                hdnCount.Value = "0";
                // Display panel based on teh sub menu selected
                if (Request.QueryString.Count > 0)
                {
                    switch (Request.QueryString["submenu"])
                    {
                        case "1":
                            accTrade.SelectedIndex = 0;
                            break;
                        case "2":
                            accTrade.SelectedIndex = 1;
                            break;
                        case "3":
                            accTrade.SelectedIndex = 2;
                            break;
                        //003 - added
                        case "4":
                            accTrade.SelectedIndex = 3;
                            break;
                    }
                }

                DataSet Vendordetails = new DataSet();
                chkProject.DataSource = objTradeInformation.GetTradeProjects();
                chkProject.DataTextField = "ProjectName";
                chkProject.DataValueField = "PK_ProjectID";
                chkProject.DataBind();

                BindRegions();
                FillCountry();//007
                BindState();
               
                BindCountries(); //006
                VendorId = Convert.ToInt64(Session["VendorId"]);
                Vendordetails = objCommon.GetVendorDetails(Convert.ToInt64(Session["VendorId"]));
                lblwelcome.Text = Vendordetails.Tables[0].Rows[0]["Company"].ToString();
                CountTrade = objTradeInformation.GetTradeStatusCount(Convert.ToInt32(Session["VendorId"]));
                //Show or hide design consultant view based on Type of company - sooraj
                SwitchDesignConsultantView(Vendordetails.Tables[0].Rows[0]["TypeOfCompany"].ConvertToString() == TypeOfCompany.DesignConsultant);
                BindScopes();// Sooraj - Changed the sequence after the switching the view
                CallScripts();
                BindControls();

            }
            else
            {
                //G. Vera 05/21/2014 - Added
                LoadToolTipOnPostBack();
            }

            

            if (hdnScopeTypeOpen.Value == "PMMI")
            {
                updPnlPMMI.Attributes["style"] = @"display: """;
                updpnlScope.Attributes["style"] = @"display: none";
                updPnlDesign.Attributes["style"] = @"display: none";
                liMasterFormat.Attributes["class"] = "";
                liPMMI.Attributes["class"] = "selected";
                liDesign.Attributes["class"] = "";

            }
            if (hdnScopeTypeOpen.Value == "MasterFormat")
            {
                updPnlPMMI.Attributes["style"] = @"display: none";
                updpnlScope.Attributes["style"] = @"display: """;
                updPnlDesign.Attributes["style"] = @"display: none";
                liMasterFormat.Attributes["class"] = "selected";
                liPMMI.Attributes["class"] = "";
                liDesign.Attributes["class"] = "";
            }
            //006 Sooraj 
            if (hdnScopeTypeOpen.Value == "Design")
            {
                updPnlDesign.Attributes["style"] = @"display: """;
                updpnlScope.Attributes["style"] = @"display: none";
                updPnlPMMI.Attributes["style"] = @"display: none";

                liDesign.Attributes["class"] = "selected";
                liMasterFormat.Attributes["class"] = "";
                liPMMI.Attributes["class"] = "";
            }
        }

        foreach (GridViewRow gr in grdState1.Rows)
        {
            CheckBoxList chk1 = (CheckBoxList)gr.FindControl("chklRegions");
            if (chk1.Items.Count > 0)
            {
                foreach (ListItem item in chk1.Items)
                {
                    item.Attributes.Add("onClick", "GetChecked('" + item.Text.Trim() + "',this.checked,'" + chk1.ClientID + "')");
                }
            }
        }

        foreach (GridViewRow gr in grdState2.Rows)
        {
            CheckBoxList chk1 = (CheckBoxList)gr.FindControl("chklRegions");
            if (chk1.Items.Count > 0)
            {
                foreach (ListItem item in chk1.Items)
                {
                    item.Attributes.Add("onClick", "GetChecked('" + item.Text.Trim() + "',this.checked,'" + chk1.ClientID + "')");
                }
            }
        }

        foreach (GridViewRow gr in grdState3.Rows)
        {
            CheckBoxList chk1 = (CheckBoxList)gr.FindControl("chklRegions");
            if (chk1.Items.Count > 0)
            {
                foreach (ListItem item in chk1.Items)
                {
                    item.Attributes.Add("onClick", "GetChecked('" + item.Text.Trim() + "',this.checked,'" + chk1.ClientID + "')");
                }
            }
        }

        VQFMenu.GetVendorStatus();
        hdnVendorStatus.Value = Convert.ToString(VQFMenu.VendorStatus);
        if (VQFMenu.VendorStatus == 1 || VQFMenu.VendorStatus == 2)
        {
            grdLevel1.Enabled = false;
            grdLevel2.Enabled = false;
            dlPMMICodes1.Enabled = false;   //002
            //grdState1.Enabled = false;
            //grdState2.Enabled = false;
            //grdState3.Enabled = false;
            apnRegions.Enabled = false;
            apnProject.Enabled = false;
            apnBIM.Enabled = false;         //003
            apnBIMDesign.Enabled = false;         //008
            apnScope.Enabled = false;         //003
            imbSaveandClose.Visible = false;
            imbsubmit.Visible = false;
            ImageButton imgAttach = (ImageButton)VQFMenu.FindControl("imbAttch");
            imgAttach.Visible = true;
        }
        VisibleControls();
        CallScripts();      //003
    }

    //006
    private void BindCountries()
    {
        var CountryList = objCommon.GetCountryListWithContinent().ToList();
        var continentList = objCommon.GetContinent();
        CheckBoxList chkList = null;
        CheckBox chk = null;
        grdContinent.DataSource = continentList;
        grdContinent.DataBind();
        int continentID = 0;
        HtmlControl divCountry = null;
        foreach (GridViewRow grd in grdContinent.Rows)
        {
            chk = (CheckBox)grd.FindControl("chkContinent");

            chkList = (CheckBoxList)grd.FindControl("chkContinentCountries");
            divCountry = (HtmlControl)grd.FindControl("divCountries");
            chk.Attributes.Add("onclick", "SwitchView('" + chk.ClientID + "','" + divCountry.ClientID + "');");
            continentID = grdContinent.DataKeys[grd.RowIndex].Value.ConvertToString().ConvertToShortInt();
            if (chkList != null)
                objCommon.BindCountryDropDownByContinent(CountryList, chkList, continentID);

        }

    }



    //private List<Country> CountryList
    //{
    //    get {
    //        return ViewState["CountryList"] as List<Country>;
    //    }
    //    set {
    //        ViewState["CountryList"] = value;
    //    }
    //}


    //003 - added
    //008 - modified
    private void BindBIM(long tradeID)
    {

        if (!IsDesignConsultant)
        {
            BIMQuestions = objTradeInformation.GetTradeBIMQuestions(tradeID);

            //questions:
            lblBIMQuestion1.Text = BIMQuestions.Single(q => q.QuestionOrder == 1).Question;
            lblBIMQuestion1A.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.1).Question;
            lblBIMQuestion1B.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.2).Question;
            lblBIMQuestion1C.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.3).Question;
            lblBIMQuestion1D.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.4).Question;
            lblBIMQuestion2.Text = BIMQuestions.Single(q => q.QuestionOrder == 2).Question;
            lblBIMQuestion3.Text = BIMQuestions.Single(q => q.QuestionOrder == 3).Question;
            lblBIMQuestion3A.Text = BIMQuestions.Single(q => q.QuestionOrder == 3.1).Question;
            lblBIMQuestion4.Text = BIMQuestions.Single(q => q.QuestionOrder == 4).Question;
            lblBIMQuestion4A.Text = BIMQuestions.Single(q => q.QuestionOrder == 4.1).Question;
            lblBIMQuestion5.Text = BIMQuestions.Single(q => q.QuestionOrder == 5).Question;
            lblBIMQuestion5A.Text = BIMQuestions.Single(q => q.QuestionOrder == 5.1).Question;

            //answers:
            //group 1
            switch (BIMQuestions.Single(q => q.QuestionOrder == 1).YesNoAnswer.ToUpper())
            {
                case "Y":
                    rdoBIMQuestion1.SelectedIndex = 0;
                    trBIMQuestion1A.Attributes["Style"] = @"display: ";
                    trBIMQuestion1B.Attributes["Style"] = @"display: ";
                    break;
                case "N":
                    rdoBIMQuestion1.SelectedIndex = 1;
                    trBIMQuestion1A.Attributes["Style"] = @"display: none";
                    trBIMQuestion1B.Attributes["Style"] = @"display: none";
                    break;
                case "":
                    rdoBIMQuestion1.SelectedIndex = -1;
                    trBIMQuestion1A.Attributes["Style"] = @"display: none";
                    trBIMQuestion1B.Attributes["Style"] = @"display: none";
                    break;
                default:
                    break;
            }
            switch (BIMQuestions.Single(q => q.QuestionOrder == 1.2).YesNoAnswer.ToUpper())
            {
                case "Y":
                    rdoBIMQuestion1B.SelectedIndex = 0;
                    trBIMQuestion1C.Attributes["Style"] = @"display: ";
                    trBIMQuestion1D.Attributes["Style"] = @"display: none";
                    break;
                case "N":
                    rdoBIMQuestion1B.SelectedIndex = 1;
                    trBIMQuestion1C.Attributes["Style"] = @"display: none";
                    trBIMQuestion1D.Attributes["Style"] = @"display: ";
                    break;
                case "":
                    rdoBIMQuestion1B.SelectedIndex = -1;
                    trBIMQuestion1C.Attributes["Style"] = @"display: none";
                    trBIMQuestion1D.Attributes["Style"] = @"display: none";
                    break;
                default:
                    break;
            }
            txtBIMQuestion1A.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.1).OtherAnswer.Trim();
            txtBIMQuestion1C.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.3).OtherAnswer.Trim();
            txtBIMQuestion1D.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.4).OtherAnswer.Trim();

            //group 2
            txtBIMQuestion2.Text = BIMQuestions.Single(q => q.QuestionOrder == 2).OtherAnswer.Trim();

            //group 3
            switch (BIMQuestions.Single(q => q.QuestionOrder == 3).YesNoAnswer.ToUpper())
            {
                case "Y":
                    rdoBIMQuestion3.SelectedIndex = 0;
                    trBIMQuestion3A.Attributes["Style"] = @"display: ";
                    break;
                case "N":
                    rdoBIMQuestion3.SelectedIndex = 1;
                    trBIMQuestion3A.Attributes["Style"] = @"display: none";
                    break;
                case "":
                    rdoBIMQuestion3.SelectedIndex = -1;
                    trBIMQuestion3A.Attributes["Style"] = @"display: none";
                    break;
                default:
                    break;
            }
            txtBIMQuestion3A.Text = BIMQuestions.Single(q => q.QuestionOrder == 3.1).OtherAnswer.Trim();

            //group 4
            switch (BIMQuestions.Single(q => q.QuestionOrder == 4).YesNoAnswer.ToUpper())
            {
                case "Y":
                    rdoBIMQuestion4.SelectedIndex = 0;
                    trBIMQuestion4A.Attributes["Style"] = @"display: ";
                    break;
                case "N":
                    rdoBIMQuestion4.SelectedIndex = 1;
                    trBIMQuestion4A.Attributes["Style"] = @"display: none";
                    break;
                case "":
                    rdoBIMQuestion4.SelectedIndex = -1;
                    trBIMQuestion4A.Attributes["Style"] = @"display: none";
                    break;
                default:
                    break;
            }
            txtBIMQuestion4A.Text = BIMQuestions.Single(q => q.QuestionOrder == 4.1).OtherAnswer.Trim();

            //group 5
            switch (BIMQuestions.Single(q => q.QuestionOrder == 5).YesNoAnswer.ToUpper())
            {
                case "Y":
                    rdoBIMQuestion5.SelectedIndex = 0;
                    trBIMQuestion5A.Attributes["Style"] = @"display: ";
                    break;
                case "N":
                    rdoBIMQuestion5.SelectedIndex = 1;
                    trBIMQuestion5A.Attributes["Style"] = @"display: none";
                    break;
                case "":
                    rdoBIMQuestion5.SelectedIndex = -1;
                    trBIMQuestion5A.Attributes["Style"] = @"display: none";
                    break;
                default:
                    break;
            }
            txtBIMQuestion5A.Text = BIMQuestions.Single(q => q.QuestionOrder == 5.1).OtherAnswer.Trim(); 
        }
        else
        {
            BIMQuestions = objTradeInformation.GetTradeBIMQuestions(tradeID, VQFSection.TradeBIMDesign);

            //questions:
            lblBIMDesignQuestion1.Text = BIMQuestions.Single(q => q.QuestionOrder == 1).Question;
            lblBIMDesignQuestion1A.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.1).Question;
            lblBIMDesignQuestion1B.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.2).Question;
            lblBIMDesignQuestion1C.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.3).Question;
            lblBIMDesignQuestion1D.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.4).Question;
            lblBIMDesignQuestion1E.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.5).Question;
            lblBIMDesignQuestion1F.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.6).Question;

            //display
            switch (BIMQuestions.Single(q => q.QuestionOrder == 1).YesNoAnswer.ToUpper())
            {
                case "Y":
                    rdoBIMDesignQuestion1.SelectedIndex = 0;
                    trBIMDesignQuestion1A.Attributes["Style"] = @"display: ";
                    trBIMDesignQuestion1B.Attributes["Style"] = @"display: ";
                    trBIMDesignQuestion1C.Attributes["Style"] = @"display: ";
                    trBIMDesignQuestion1D.Attributes["Style"] = @"display: ";
                    trBIMDesignQuestion1E.Attributes["Style"] = @"display: ";
                    trBIMDesignQuestion1F.Attributes["Style"] = @"display: ";
                    break;
                case "N":
                    rdoBIMDesignQuestion1.SelectedIndex = 1;
                    trBIMDesignQuestion1A.Attributes["Style"] = @"display: none";
                    trBIMDesignQuestion1B.Attributes["Style"] = @"display: none";
                    trBIMDesignQuestion1C.Attributes["Style"] = @"display: none";
                    trBIMDesignQuestion1D.Attributes["Style"] = @"display: none";
                    trBIMDesignQuestion1E.Attributes["Style"] = @"display: none";
                    trBIMDesignQuestion1F.Attributes["Style"] = @"display: none";
                    break;
                default:
                    rdoBIMDesignQuestion1.SelectedIndex = -1;
                    trBIMDesignQuestion1A.Attributes["Style"] = @"display: none";
                    trBIMDesignQuestion1B.Attributes["Style"] = @"display: none";
                    trBIMDesignQuestion1C.Attributes["Style"] = @"display: none";
                    trBIMDesignQuestion1D.Attributes["Style"] = @"display: none";
                    trBIMDesignQuestion1E.Attributes["Style"] = @"display: none";
                    trBIMDesignQuestion1F.Attributes["Style"] = @"display: none";
                    break;
            }

            //answers
            txtBIMDesignQuestion1A.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.1).OtherAnswer.Trim();
            rdoBIMDesignQuestion1B.SelectedIndex = BIMQuestions.Single(q => q.QuestionOrder == 1.2).YesNoAnswer.Trim().ConvertYesNoToSelectedIndex();
            txtBIMDesignQuestion1C.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.3).OtherAnswer.Trim();
            txtBIMDesignQuestion1D.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.4).OtherAnswer.Trim();
            rdoBIMDesignQuestion1E.SelectedIndex = BIMQuestions.Single(q => q.QuestionOrder == 1.5).YesNoAnswer.Trim().ConvertYesNoToSelectedIndex();
            txtBIMDesignQuestion1F.Text = BIMQuestions.Single(q => q.QuestionOrder == 1.6).OtherAnswer.Trim();
        }

    }

    #endregion

    #region Button Events


    //006
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        var ddl = sender as DropDownList;
        BindAssociateState(ddl, true, true);
        ddl.Focus();
    }


    //save and close the trade information
    protected void imbSaveandClose_Click(object sender, ImageClickEventArgs e)
    {
        int CountTrade = objTradeInformation.GetTradeCount(Convert.ToInt32(Session["VendorId"]));
        if (CountTrade == 0)
        {
            long TradeId = InsertTrade(Convert.ToByte(this.IsPageValid()));      //009
            InsertScopes(TradeId);
            InsertProjects(TradeId);
            InsertRegions(TradeId);
            InsertTradeLicenseNumber(TradeId);
            InsertTradeQuestions(TradeId);      //002 - added
            Lblheading.Text = "Result";
            LblErrorMsg.Text = "";
            VQFMenu.GetCompleteStatus();
            VQFMenu.GetIncompleteStatus();
            LblMsg.Text = "&nbsp;<li>Trade information details has been saved successfully</li>";
            modalExtnd.Show();
        }
        else
        {
            long tradeId;
            tradeId = objTradeInformation.GetVQFTrade(Convert.ToInt32(Session["VendorId"]))[0].PK_TradeID;

            objTradeInformation.DeleteAllRelationsofTradeID(tradeId);

            objTradeInformation.TradeID = tradeId;
            Forces = rdlyeano.SelectedIndex == 0 ? true : (rdlyeano.SelectedIndex == 1 ? false : new Nullable<bool>());

            objTradeInformation.UpdateTrade(Forces, txtexplain.Text.Trim(), tradeId, Convert.ToByte(this.IsPageValid()));      //009
            InsertScopes(tradeId);
            InsertProjects(tradeId);
            InsertRegions(tradeId);
            InsertTradeLicenseNumber(tradeId);
            InsertTradeQuestions(tradeId);      //002 - added
            Lblheading.Text = "Result";
            LblErrorMsg.Text = "";
            //imbPrint.Visible = false;
            VQFMenu.GetCompleteStatus();
            VQFMenu.GetIncompleteStatus();
            LblMsg.Text = "&nbsp;<li>Trade information details saved successfully</li>";
            modalExtnd.Show();
        }

        VQFMenu.LastUpdatedDate(Convert.ToInt32(Session["VendorId"]), DateTime.Now);    //010
        BindControls();
    }


    /// <summary>
    /// Redirect to next pages based on logged in user 
    /// </summary>
    /// <Author>Sooraj Sudhakaran.T</Author>
    /// <Date>30-SEP-2013</Date>
    private void RedirectToNextPage()
    {
        if (divDesign.Visible)
            Response.Redirect("References.aspx");
        else
            Response.Redirect("Safety.aspx");
    }

    /// <summary>
    /// 002 - implmented
    /// Insert VQF Trade Question record
    /// </summary>
    /// <param name="TradeId"></param>
    private void InsertTradeQuestions(long TradeId)
    {
        List<VendorTradeQuestions> listVendorQuestions = new List<VendorTradeQuestions>();
        VendorTradeQuestions objVendorQuestions;

        // scope questions
        foreach (DataListItem dlItem in dlTradeQuestions.Items)
        {
            objVendorQuestions = new VendorTradeQuestions();
            HiddenField hdnId = (HiddenField)dlItem.FindControl("hdnQuestionId");
            long questionId = Convert.ToInt64(hdnId.Value);
            RadioButtonList rdoL = (RadioButtonList)dlItem.FindControl("rdoTradeQuestionAnswers");
            string yNAnswer = rdoL.SelectedValue;

            objVendorQuestions.QuestionID = questionId;
            objVendorQuestions.YesNoAnswer = yNAnswer;

            if (rdoL.SelectedValue != string.Empty) listVendorQuestions.Add(objVendorQuestions);
        }

        objTradeInformation.InsertVQFTradeQuestion(listVendorQuestions, TradeId);

        //008
        if (!IsDesignConsultant)
        {
            // 003 bim questions
            BIMQuestions = objTradeInformation.GetTradeBIMQuestions(TradeId).OrderBy(q => q.QuestionOrder).ToList();
            BIMQuestions.Where(q => q.QuestionOrder == 1.0).ToList()[0].YesNoAnswer = rdoBIMQuestion1.SelectedValue;
            BIMQuestions.Where(q => q.QuestionOrder == 1.1).ToList()[0].OtherAnswer = txtBIMQuestion1A.Text;
            BIMQuestions.Where(q => q.QuestionOrder == 1.2).ToList()[0].YesNoAnswer = rdoBIMQuestion1B.SelectedValue;
            BIMQuestions.Where(q => q.QuestionOrder == 1.3).ToList()[0].OtherAnswer = txtBIMQuestion1C.Text;
            BIMQuestions.Where(q => q.QuestionOrder == 1.4).ToList()[0].OtherAnswer = txtBIMQuestion1D.Text;
            BIMQuestions.Where(q => q.QuestionOrder == 2.0).ToList()[0].OtherAnswer = txtBIMQuestion2.Text;
            BIMQuestions.Where(q => q.QuestionOrder == 3.0).ToList()[0].YesNoAnswer = rdoBIMQuestion3.SelectedValue;
            BIMQuestions.Where(q => q.QuestionOrder == 3.1).ToList()[0].OtherAnswer = txtBIMQuestion3A.Text;
            BIMQuestions.Where(q => q.QuestionOrder == 4.0).ToList()[0].YesNoAnswer = rdoBIMQuestion4.SelectedValue;
            BIMQuestions.Where(q => q.QuestionOrder == 4.1).ToList()[0].OtherAnswer = txtBIMQuestion4A.Text;
            BIMQuestions.Where(q => q.QuestionOrder == 5.0).ToList()[0].YesNoAnswer = rdoBIMQuestion5.SelectedValue;
            BIMQuestions.Where(q => q.QuestionOrder == 5.1).ToList()[0].OtherAnswer = txtBIMQuestion5A.Text; 
        }
        else
        {
            BIMQuestions = objTradeInformation.GetTradeBIMQuestions(TradeId, VQFSection.TradeBIMDesign).OrderBy(q => q.QuestionOrder).ToList();
            BIMQuestions.Where(q => q.QuestionOrder == 1.0).ToList()[0].YesNoAnswer = rdoBIMDesignQuestion1.SelectedValue;
            BIMQuestions.Where(q => q.QuestionOrder == 1.1).ToList()[0].OtherAnswer = txtBIMDesignQuestion1A.Text;
            BIMQuestions.Where(q => q.QuestionOrder == 1.2).ToList()[0].YesNoAnswer = rdoBIMDesignQuestion1B.SelectedValue;
            BIMQuestions.Where(q => q.QuestionOrder == 1.3).ToList()[0].OtherAnswer = txtBIMDesignQuestion1C.Text;
            BIMQuestions.Where(q => q.QuestionOrder == 1.4).ToList()[0].OtherAnswer = txtBIMDesignQuestion1D.Text;
            BIMQuestions.Where(q => q.QuestionOrder == 1.5).ToList()[0].YesNoAnswer = rdoBIMDesignQuestion1E.SelectedValue;
            BIMQuestions.Where(q => q.QuestionOrder == 1.6).ToList()[0].OtherAnswer = txtBIMDesignQuestion1F.Text;
        }

        objTradeInformation.InsertVQFTradeQuestion(BIMQuestions, TradeId);

    }


    /// <summary>
    /// 009 - added
    /// </summary>
    /// <returns></returns>
    private bool IsPageValid()
    {
        Page.Validate("TradeQuestions");

        if (Page.IsValid)
        {
            Page.Validate("Regions");

            Page.Validate("BIMQuestions");      //003, 008

            return Page.IsValid;
        }
        else
        {
            return false;
        }
    }

    // save and next  
    protected void imbsubmit_Click(object sender, ImageClickEventArgs e)
    {
        this.imbSaveandClose_Click(sender, null);   //008 - it should save first...was not saving until after validation....

        Page.Validate("TradeQuestions");

        if (Page.IsValid)
        {
            Page.Validate("Regions");

            Page.Validate("BIMQuestions");      //003, 008

            if (Page.IsValid)
            {
                ViewState["Flag"] = "1";
                CountTrade = objTradeInformation.GetTradeCount(Convert.ToInt32(Session["VendorId"]));

                if (CountTrade == 0)
                {
                    long TradeId = InsertTrade(1);
                    InsertScopes(TradeId);
                    InsertProjects(TradeId);
                    InsertRegions(TradeId);
                    InsertTradeLicenseNumber(TradeId);
                    InsertTradeQuestions(TradeId);      //002 - added
                    //Response.Redirect("Safety.aspx"); //005-Sooraj
                    VQFMenu.LastUpdatedDate(Convert.ToInt32(Session["VendorId"]), DateTime.Now);        //010
                    RedirectToNextPage();
                }
                else
                {
                    long tradeId;
                    tradeId = objTradeInformation.GetVQFTrade(Convert.ToInt32(Session["VendorId"]))[0].PK_TradeID;
                    objTradeInformation.DeleteAllRelationsofTradeID(tradeId);


                    objTradeInformation.TradeID = tradeId;
                    if (rdlyeano.SelectedIndex < 0)
                    {
                        Forces = null;
                    }
                    else
                    {
                        if (rdlyeano.SelectedIndex == 0)
                        {
                            Forces = true;
                        }
                        else if (rdlyeano.SelectedIndex == 1)
                        {
                            Forces = false;
                        }

                    }
                    objTradeInformation.UpdateTrade(Forces, txtexplain.Text.Trim(), tradeId, 2);
                    InsertScopes(tradeId);
                    InsertProjects(tradeId);
                    InsertRegions(tradeId);
                    InsertTradeLicenseNumber(tradeId);
                    InsertTradeQuestions(tradeId);      //002 - added
                    //Response.Redirect("Safety.aspx"); --Sooraj
                    VQFMenu.LastUpdatedDate(Convert.ToInt32(Session["VendorId"]), DateTime.Now);    //010
                    RedirectToNextPage();
                }
            }
            else
            {
                LblMsg.Text = "";
                Lblheading.Text = "Trade Information - Validation";
                LblErrorMsg.Text = Errormsg();

                modalExtnd.Show();
                //003 accTrade.SelectedIndex = 0;
            }
        }
        else
        {
            LblMsg.Text = "";
            Lblheading.Text = "Trade Information - Validation";
            LblErrorMsg.Text = Errormsg();

            modalExtnd.Show();
            //003 accTrade.SelectedIndex = 0;
        }
    }

    #endregion

    #region GridView Events

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdn = (HiddenField)e.Row.FindControl("hdnFlag");

            DAL.UspGetTradeScopesResult[] objScopes = objTradeInformation.GetTradeScope(Convert.ToInt32(((HiddenField)e.Row.FindControl("hdnParentScopeId")).Value));

            GridView grd = new GridView();
            grd = (GridView)e.Row.FindControl("grdScopeLevele2");
            grd.DataSource = objScopes;
            grd.DataBind();
            grd.Style["Display"] = "None";
            Image img = new Image();
            img = (Image)e.Row.FindControl("imgStatus");
            hdn.Value = img.ImageUrl;
            if (grd.Rows.Count > 0)
            {
                img.Attributes.Add("onClick", "OpenGrid('" + grd.ClientID + "','" + hdn.ClientID + "','" + img.ClientID + "');");
            }
        }
    }

    /// <summary>
    /// 002
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlTradeQuestionAnswers_DataBound(object sender, DataListItemEventArgs e)
    {
        var obj = e.Item.DataItem as VendorTradeQuestions;
        Literal lbl = e.Item.FindControl("lblTradeQuestion") as Literal;
        RadioButtonList rdoL = e.Item.FindControl("rdoTradeQuestionAnswers") as RadioButtonList;
        RequiredFieldValidator reqValid = e.Item.FindControl("reqvrdoTradeAnswers") as RequiredFieldValidator;

        reqValid.ErrorMessage = "Please answer: " + lbl.Text + "?";
        rdoL.SelectedValue = obj.YesNoAnswer;

    }

    // Display the level two based on the check box selected for al the scopes
    protected void grdGeneralReq_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk = (CheckBox)e.Row.FindControl("chkCheck");

            CheckBoxList chk1 = (CheckBoxList)e.Row.FindControl("chklGeneralReq");
            HiddenField hdnval = (HiddenField)e.Row.FindControl("hdnScopeId");
            chk1.DataSource = objTradeInformation.GetTradeScope(Convert.ToInt32(hdnval.Value));
            chk1.DataTextField = "Name";
            chk1.DataValueField = "PK_ScopeId";
            chk1.DataBind();
            chk1.Style["display"] = "none";
            foreach (ListItem li in chk1.Items)
            {
                li.Selected = true;
            }
            if (chk1.Items.Count > 0)
            {
                chk.Attributes.Add("onclick", "javascript:DisplayLevel2('" + chk1.ClientID + "','" + chk.ClientID + "');");
            }
        }
    }

    //Display regions based on the selected state value
    protected void grdState1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView grd = (GridView)sender;
        if (grd.ID != "grdState3")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chk = (CheckBox)e.Row.FindControl("chkCheck");

                CheckBoxList chk1 = (CheckBoxList)e.Row.FindControl("chklRegions");
                HiddenField hdnval = (HiddenField)e.Row.FindControl("hdnStateID");
                chk1.DataSource = objCommon.getRegion(Convert.ToInt32(hdnval.Value));
                chk1.DataTextField = "StateCode";
                chk1.DataValueField = "PK_StateID";
                chk1.DataBind();
                chk1.Style["display"] = "none";
                trRegions.Style["display"] = "none";
                if (hdnval.Value == "51")
                {
                    chk.Attributes.Add("onclick", "javascript:DisplayRegions('" + trRegions.ClientID + "','" + chk.ClientID + "',true);");
                }
                else
                {
                    if (chk1.Items.Count > 0)
                    {
                        chk.Attributes.Add("onclick", "javascript:DisplayRegions('" + chk1.ClientID + "','" + chk.ClientID + "',false);");
                        foreach (ListItem item in chk1.Items)
                        {
                            item.Attributes.Add("onClick", "GetChecked('" + item.Text.Trim() + "',this.checked,'" + chk1.ClientID + "')");
                        }
                    }

                    else
                    {
                        chk.Attributes.Add("onclick", "javscript:unCheckCheckALL(this.checked);");
                    }
                }
            }
        }
        else
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chk = (CheckBox)e.Row.FindControl("chkCheck");
                CheckBoxList chk1 = (CheckBoxList)e.Row.FindControl("chklRegions");
                HiddenField hdn = (HiddenField)e.Row.FindControl("hdnStateID");
                trRegions.Style["display"] = "none";
                if (hdn.Value == "51")
                {
                    chk.Attributes.Add("onclick", "javascript:DisplayRegions('" + trRegions.ClientID + "','" + chk.ClientID + "',true);");
                }
                else
                {

                    chk1.DataSource = objCommon.getRegion(Convert.ToInt32(hdn.Value));
                    chk1.DataTextField = "StateCode";
                    chk1.DataValueField = "PK_StateID";
                    chk1.DataBind();
                    chk1.Style["display"] = "none";
                    if (chk1.Items.Count > 0)
                    {
                        chk.Attributes.Add("onclick", "javascript:DisplayRegions('" + chk1.ClientID + "','" + chk.ClientID + "',false);");
                        foreach (ListItem item in chk1.Items)
                        {
                            item.Attributes.Add("onClick", "GetChecked('" + item.Text.Trim() + "',this.checked,'" + chk1.ClientID + "')");
                        }
                    }
                    else
                    {
                        chk.Attributes.Add("onclick", "javscript:unCheckCheckALL(this.checked);");
                    }
                }
            }
        }
    }

    #endregion

    #region Validation Control Events

    //validate scope codes
    protected void cusvScopes_ServerValidate(object source, ServerValidateEventArgs args)
    {
        bool isSelected = false;
        for (int i = 0; i < grdLevel1.Rows.Count; i++)
        {
            GridView gv = (GridView)grdLevel1.Rows[i].FindControl("grdScopeLevele2");

            for (int j = 0; j < gv.Rows.Count; j++)
            {
                CheckBox chk = (CheckBox)gv.Rows[j].FindControl("chkCheck");
                bool result = chk.Checked;
                if (result)
                {
                    isSelected = true;
                }
            }
        }

        for (int i = 0; i < grdLevel2.Rows.Count; i++)
        {
            GridView gv = (GridView)grdLevel2.Rows[i].FindControl("grdScopeLevele2");
            for (int j = 0; j < gv.Rows.Count; j++)
            {
                CheckBox chk = (CheckBox)gv.Rows[j].FindControl("chkCheck");
                bool result = chk.Checked;
                if (result)
                {
                    isSelected = true;
                }
            }
        }

        //002 - PMMI should be part of the required validation
        foreach (DataListItem item in dlPMMICodes1.Items)
        {
            HiddenField hdn = ((HiddenField)item.FindControl("hdnPMMIScopeId"));
            CheckBox chbx = ((CheckBox)item.FindControl("chkPMMICode"));
            if (chbx.Checked)
            {
                isSelected = true;
            }
        }

        //004-Sooraj -Design Code should be part of the required validation
        foreach (DataListItem item in dlistDesign.Items)
        {
            HiddenField hdn = ((HiddenField)item.FindControl("hdnDesignScopeId"));
            CheckBox chbx = ((CheckBox)item.FindControl("chkDesignCode"));
            if (chbx.Checked)
            {
                isSelected = true;
                break;//004-Sooraj- we need to check at least one item selected or not ,then ends the loop 
            }
        }

        if (isSelected)
        {
            args.IsValid = true;
        }
        else
        {
            cusvScopes.ErrorMessage = "Please select at least one scope of work";
            args.IsValid = false;
        }
    }

    //validate scope codes
    protected void cusvRegions_ServerValidate(object source, ServerValidateEventArgs args)
    {
        bool isSelected = false;
        bool isContinentSelected = false;
        bool isUSASelected = false;
        string message = "";
        //006
        foreach (GridViewRow row in grdContinent.Rows)
        {
           
            CheckBox chk = (CheckBox)row.FindControl("chkContinent");
            if (chk.Checked)
            {
                isContinentSelected = true;
                CheckBoxList chkList = (CheckBoxList)row.FindControl("chkContinentCountries");
                isSelected = IsCountrySelectedForContinent(chkList);
                if (!isSelected)
                    message = string.Format("{0}, {1}", message, chk.Text);
            }
            
        }
        if (chkUS.Checked)
        {
            foreach (GridViewRow row in grdState1.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chkCheck");
                bool result = chk.Checked;
                if (result)
                {
                    isUSASelected = true;
                   
                }
            }
            foreach (GridViewRow row in grdState2.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chkCheck");
                bool result = chk.Checked;
                if (result)
                {
                    isUSASelected = true;
                   
                }
            }
            foreach (GridViewRow row in grdState3.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chkCheck");
                bool result = chk.Checked;
                if (result)
                {
                    isUSASelected = true;
                 
                }
            }
        }
       
        if ((chkUS.Checked && isUSASelected) || (isContinentSelected && message.Length <= 0))
        {
            args.IsValid = true;

            if (chkUS.Checked && !isUSASelected)
            {
                args.IsValid = false;
            }
            if (isContinentSelected && message.Length > 0)
            {
                args.IsValid = false;
            }

        }
        else
        {
            args.IsValid = false;
            cusvRegions.ErrorMessage = "Please select at least one region." ;
        }
       
       if( !args.IsValid )
        {
             args.IsValid = false;
             if (chkUS.Checked && !isUSASelected)
             {
                 message = message + ", United States of America.";
                 cusvRegions.ErrorMessage = "Please select at least one region for United States of America. ";
             } 
            if (isContinentSelected && message.Length >= 0)
                cusvRegions.ErrorMessage =  "Please select at least one region for continent(s) : " + message.TrimStart(',');
            
        }
    }

    //validate state
    protected void cusvState_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStates.SelectedIndex > 0)
        {
            if (ddlStates.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherState.Text.Trim()))
                {
                    cusvState.ErrorMessage = "Please enter state under license number 1";
                    args.IsValid = false;
                }
            }
        }
    }

    //validate state
    protected void cusvState1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlState1.SelectedIndex > 0)
        {
            if (ddlState1.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOther1.Text.Trim()))
                {
                    cusvState1.ErrorMessage = "Please enter state under license number 2";
                    args.IsValid = false;
                }
            }
        }
    }

    //validate state
    protected void cusvState2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlState2.SelectedIndex > 0)
        {
            if (ddlState2.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOther2.Text.Trim()))
                {
                    cusvState2.ErrorMessage = "Please enter state under license number 3";
                    args.IsValid = false;
                }
            }
        }
    }

    //validate state
    protected void cusvState3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlState3.SelectedIndex > 0)
        {
            if (ddlState3.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOther3.Text.Trim()))
                {
                    cusvState3.ErrorMessage = "Please enter state under license number 4";
                    args.IsValid = false;
                }
            }
        }
    }

    //validate state
    protected void cusvState4_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlState4.SelectedIndex > 0)
        {
            if (ddlState4.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOther4.Text.Trim()))
                {
                    cusvState4.ErrorMessage = "Please enter state under license number 5";
                    args.IsValid = false;
                }
            }
        }
    }

    //validate state
    protected void cusvState5_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlState5.SelectedIndex > 0)
        {
            if (ddlState5.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOther5.Text.Trim()))
                {
                    cusvState5.ErrorMessage = "Please enter state under license number 6";
                    args.IsValid = false;
                }
            }
        }
    }

    //validate state
    protected void cusvState6_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlState6.SelectedIndex > 0)
        {
            if (ddlState6.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOther6.Text.Trim()))
                {
                    cusvState6.ErrorMessage = "Please enter state under license number 7";
                    args.IsValid = false;
                }
            }
        }
    }

    //validate state
    protected void cusvState7_ServerValidate(object surce, ServerValidateEventArgs args)
    {
        if (ddlState7.SelectedIndex > 0)
        {
            if (ddlState7.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOther7.Text.Trim()))
                {
                    cusvState7.ErrorMessage = "Please enter state under license number 8";
                    args.IsValid = false;
                }
            }
        }
    }

    //validate state
    protected void cusvState8_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlState8.SelectedIndex > 0)
        {
            if (ddlState8.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOther8.Text.Trim()))
                {
                    cusvState8.ErrorMessage = "Please enter state under license number 9";
                    args.IsValid = false;
                }
            }
        }
    }

    //validate state
    protected void cusvState9_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlState9.SelectedIndex > 0)
        {
            if (ddlState9.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOther9.Text.Trim()))
                {
                    cusvState9.ErrorMessage = "Please enter state under license number 10";
                    args.IsValid = false;
                }
            }
        }
    }

    //validate explanation radio button
    protected void custRadioYesno_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rdlyeano.SelectedIndex == 1)
        {
            if (string.IsNullOrEmpty(txtexplain.Text.Trim()))
            {
                custRadioYesno.ErrorMessage = "Please enter explanation for performing all work";
                args.IsValid = false;
            }
        }
    }

    //custom validate to validate date
    protected void cusExpirationDate_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt;

        if (!string.IsNullOrEmpty(txtExpiration.Text.Trim()))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txtExpiration.Text.Trim();
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];
                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txtExpiration.Text.Trim(), out dt)))
                    {
                        args.IsValid = false;
                    }
                    else
                    {
                        DateTime Expiration;
                        Expiration = Convert.ToDateTime(txtExpiration.Text.Trim());

                        if (Expiration.Year < DateTime.Now.Year)
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month < DateTime.Now.Month))
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month == DateTime.Now.Month) && (Expiration.Date <= DateTime.Now.Date))
                        {
                            args.IsValid = false;
                        }
                    }
                }
                else
                {
                    args.IsValid = false;
                }
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    //custom validate to validate date
    protected void cusExpirationDate1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt;

        if (!string.IsNullOrEmpty(txtExpiration1.Text.Trim()))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txtExpiration1.Text.Trim();
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];

                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txtExpiration1.Text.Trim(), out dt)))
                    {
                        args.IsValid = false;
                    }
                    else
                    {
                        DateTime Expiration;
                        Expiration = Convert.ToDateTime(txtExpiration1.Text.Trim());

                        if (Expiration.Year < DateTime.Now.Year)
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month < DateTime.Now.Month))
                        {
                            args.IsValid = false;
                        }

                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month == DateTime.Now.Month) && (Expiration.Date <= DateTime.Now.Date))
                        {
                            args.IsValid = false;
                        }
                    }
                }
                else
                {
                    args.IsValid = false;
                }
            }
            else
            {
                //cusExpirationDate1.ErrorMessage = "Please enter valid expiration date (mm/dd/yyyy) under list license 2";
                args.IsValid = false;
            }
        }
    }

    //custom validate to validate date
    protected void cusExpirationDate2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt;
        if (!string.IsNullOrEmpty(txtExpiration2.Text.Trim()))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txtExpiration2.Text.Trim();
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];

                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txtExpiration2.Text.Trim(), out dt)))
                    {
                        args.IsValid = false;
                    }
                    else
                    {
                        DateTime Expiration;
                        Expiration = Convert.ToDateTime(txtExpiration2.Text.Trim());

                        if (Expiration.Year < DateTime.Now.Year)
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month < DateTime.Now.Month))
                        {
                            args.IsValid = false;
                        }

                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month == DateTime.Now.Month) && (Expiration.Date <= DateTime.Now.Date))
                        {
                            args.IsValid = false;
                        }
                    }
                }
                else
                {
                    args.IsValid = false;
                }
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    //custom validate to validate date
    protected void cusExpirationDate3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt;
        if (!string.IsNullOrEmpty(txtExpiration3.Text.Trim()))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txtExpiration3.Text.Trim();
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];

                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txtExpiration3.Text.Trim(), out dt)))
                    {
                        args.IsValid = false;
                    }
                    else
                    {
                        DateTime Expiration;
                        Expiration = Convert.ToDateTime(txtExpiration3.Text.Trim());

                        if (Expiration.Year < DateTime.Now.Year)
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month < DateTime.Now.Month))
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month == DateTime.Now.Month) && (Expiration.Date <= DateTime.Now.Date))
                        {
                            args.IsValid = false;
                        }
                    }
                }
                else
                {
                    args.IsValid = false;
                }
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    //custom validate to validate date
    protected void cusExpirationDate4_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt;
        if (!string.IsNullOrEmpty(txtExpiration4.Text.Trim()))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txtExpiration4.Text.Trim();
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];

                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txtExpiration4.Text.Trim(), out dt)))
                    {
                        args.IsValid = false;
                    }
                    else
                    {
                        DateTime Expiration;
                        Expiration = Convert.ToDateTime(txtExpiration4.Text.Trim());

                        if (Expiration.Year < DateTime.Now.Year)
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month < DateTime.Now.Month))
                        {
                            args.IsValid = false;
                        }

                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month == DateTime.Now.Month) && (Expiration.Date <= DateTime.Now.Date))
                        {
                            args.IsValid = false;
                        }
                    }
                }
                else
                {
                    args.IsValid = false;
                }
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    //custom validate to validate date
    protected void cusExpirationDate5_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt;
        if (!string.IsNullOrEmpty(txtExpiration5.Text.Trim()))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txtExpiration5.Text.Trim();
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];

                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txtExpiration5.Text.Trim(), out dt)))
                    {
                        args.IsValid = false;
                    }
                    else
                    {
                        DateTime Expiration;
                        Expiration = Convert.ToDateTime(txtExpiration5.Text.Trim());

                        if (Expiration.Year < DateTime.Now.Year)
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month < DateTime.Now.Month))
                        {
                            args.IsValid = false;
                        }

                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month == DateTime.Now.Month) && (Expiration.Date <= DateTime.Now.Date))
                        {
                            args.IsValid = false;
                        }
                    }
                }
                else
                {
                    args.IsValid = false;
                }
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    //custom validate to validate date
    protected void cusExpirationDate6_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt;
        if (!string.IsNullOrEmpty(txtExpiration6.Text.Trim()))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txtExpiration6.Text.Trim();
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];
                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txtExpiration6.Text.Trim(), out dt)))
                    {
                        args.IsValid = false;
                    }
                    else
                    {
                        DateTime Expiration;
                        Expiration = Convert.ToDateTime(txtExpiration6.Text.Trim());

                        if (Expiration.Year < DateTime.Now.Year)
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month < DateTime.Now.Month))
                        {
                            args.IsValid = false;
                        }

                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month == DateTime.Now.Month) && (Expiration.Date <= DateTime.Now.Date))
                        {
                            args.IsValid = false;
                        }
                    }
                }
                else
                {
                    args.IsValid = false;
                }
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    //custom validate to validate date
    protected void cusExpirationDate7_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt;
        if (!string.IsNullOrEmpty(txtExpiration7.Text.Trim()))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txtExpiration7.Text.Trim();
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];

                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txtExpiration7.Text.Trim(), out dt)))
                    {
                        args.IsValid = false;
                    }
                    else
                    {
                        DateTime Expiration;
                        Expiration = Convert.ToDateTime(txtExpiration7.Text.Trim());

                        if (Expiration.Year < DateTime.Now.Year)
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month < DateTime.Now.Month))
                        {
                            args.IsValid = false;
                        }

                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month == DateTime.Now.Month) && (Expiration.Date <= DateTime.Now.Date))
                        {
                            args.IsValid = false;
                        }
                    }
                }
                else
                {
                    args.IsValid = false;
                }
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    //custom validate to validate date
    protected void cusExpirationDate8_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt;
        if (!string.IsNullOrEmpty(txtExpiration8.Text.Trim()))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txtExpiration8.Text.Trim();
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];

                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txtExpiration8.Text.Trim(), out dt)))
                    {
                        args.IsValid = false;
                    }
                    else
                    {
                        DateTime Expiration;
                        Expiration = Convert.ToDateTime(txtExpiration8.Text.Trim());

                        if (Expiration.Year < DateTime.Now.Year)
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month < DateTime.Now.Month))
                        {
                            args.IsValid = false;
                        }

                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month == DateTime.Now.Month) && (Expiration.Date <= DateTime.Now.Date))
                        {
                            args.IsValid = false;
                        }
                    }
                }
                else
                {
                    args.IsValid = false;
                }
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    /// <summary>
    /// 003 - Added
    /// 008 - modified
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void cusvBIMQuestions_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (!IsDesignConsultant)
        {
            #region ALL Vendors
            cusvBIMQuestions.ErrorMessage = string.Empty;

            if (rdoBIMQuestion1.SelectedValue == "")
            {
                cusvBIMQuestions.ErrorMessage = "Please answer Yes or No to BIM question";
                args.IsValid = false;
            }

            if (rdoBIMQuestion3.SelectedValue == "")
            {
                cusvBIMQuestions.ErrorMessage = "Please answer Yes or No to BIM question";
                args.IsValid = false;
            }

            if (rdoBIMQuestion4.SelectedValue == "")
            {
                cusvBIMQuestions.ErrorMessage = "Please answer Yes or No to BIM question";
                args.IsValid = false;
            }

            if (rdoBIMQuestion5.SelectedValue == "")
            {
                cusvBIMQuestions.ErrorMessage = "Please answer Yes or No to BIM question";
                args.IsValid = false;
            }

            if (args.IsValid)
            {
                if (rdoBIMQuestion1.SelectedValue == "Y")
                {
                    if (rdoBIMQuestion1B.SelectedValue == "")
                    {
                        cusvBIMQuestions.ErrorMessage = "Please answer Yes or No to BIM question";
                        args.IsValid = false;
                    }
                }
            }

            if (args.IsValid)
            {
                if (rdoBIMQuestion1.SelectedValue == "Y")
                {
                    if (txtBIMQuestion1A.Text == "")
                    {
                        cusvBIMQuestions.ErrorMessage += "Missing answer to BIM question \"What software package(s)...\"<BR/>";
                        args.IsValid = false;
                    }
                }

                if (rdoBIMQuestion1B.SelectedValue == "Y")
                {
                    if (txtBIMQuestion1C.Text == "")
                    {
                        cusvBIMQuestions.ErrorMessage += "Missing answer to BIM question \"How many draftsmen/detailers/coordinators...\"<BR/>";
                        args.IsValid = false;
                    }
                }

                if (rdoBIMQuestion1B.SelectedValue == "N")
                {
                    if (txtBIMQuestion1D.Text == "")
                    {
                        cusvBIMQuestions.ErrorMessage += "Missing answer to BIM question \"What company do you utilize...\"<BR/>";
                        args.IsValid = false;
                    }
                }

                if (txtBIMQuestion2.Text == "")
                {
                    cusvBIMQuestions.ErrorMessage += "Missing answer to BIM question \"Explain the highest level of 3D detail...\"<BR/>";
                    args.IsValid = false;
                }

                if (rdoBIMQuestion3.SelectedValue == "Y")
                {
                    if (txtBIMQuestion3A.Text == "")
                    {
                        cusvBIMQuestions.ErrorMessage += "Missing answer to BIM question \"Explain the extent of your involvement...\"<BR/>";
                        args.IsValid = false;
                    }
                }
                if (rdoBIMQuestion4.SelectedValue == "Y")
                {
                    if (txtBIMQuestion4A.Text == "")
                    {
                        cusvBIMQuestions.ErrorMessage += "Missing answer to BIM question \"Explain the equipment and process...\"<BR/>";
                        args.IsValid = false;
                    }
                }
                if (rdoBIMQuestion5.SelectedValue == "Y")
                {
                    if (txtBIMQuestion5A.Text == "")
                    {
                        cusvBIMQuestions.ErrorMessage += "Missing answer to BIM question \"Explain the extent of what you layout...\"<BR/>";
                        args.IsValid = false;
                    }
                }
            }
  
            //011 - added
            if (args.IsValid)
            {
                //check the max length property on answers and truncate appropriately
                var textboxes = this.Page.AllControlsOfType<TextBox>();
                var bimTextboxes = textboxes.Cast<TextBox>().Where(t => t.ID.ToLower().Contains("bimquestion") && !string.IsNullOrEmpty(t.Text));

                bimTextboxes.ToList<TextBox>().ForEach(bt => {
                    int maxLength = (bt.MaxLength == 0) ? 50 : bt.MaxLength;
                    int textLength = bt.Text.Length;
                    if (textLength > maxLength)
                    {
                        bt.Text = bt.Text.Substring(0, maxLength);
                    }
                });
            }
            #endregion
        }
        else
        {
            #region Design Consultant

            cusvBIMQuestions.ErrorMessage = string.Empty;
            switch (rdoBIMDesignQuestion1.SelectedIndex)
            {
                //yes
                case 0:
                    if (string.IsNullOrEmpty(txtBIMDesignQuestion1A.Text))
                    {
                        args.IsValid = false;
                        cusvBIMQuestions.ErrorMessage = "Please answer BIM question 2<BR/>";
                    }
                    if (string.IsNullOrEmpty(rdoBIMDesignQuestion1B.SelectedValue))
                    {
                        args.IsValid = false;
                        cusvBIMQuestions.ErrorMessage += "<li>Please answer BIM question 3</li>";
                    }
                    if (string.IsNullOrEmpty(txtBIMDesignQuestion1C.Text))
                    {
                        args.IsValid = false;
                        cusvBIMQuestions.ErrorMessage += "<li>Please answer BIM question 4</li>";
                    }
                    if (string.IsNullOrEmpty(txtBIMDesignQuestion1D.Text))
                    {
                        args.IsValid = false;
                        cusvBIMQuestions.ErrorMessage += "<li>Please answer BIM question 5</li>";
                    }
                    if (string.IsNullOrEmpty(rdoBIMDesignQuestion1E.SelectedValue))
                    {
                        args.IsValid = false;
                        cusvBIMQuestions.ErrorMessage += "<li>Please answer BIM question 6</li>";
                    }
                    if (string.IsNullOrEmpty(txtBIMDesignQuestion1F.Text))
                    {
                        args.IsValid = false;
                        cusvBIMQuestions.ErrorMessage += "<li>Please answer BIM question 7</li>";
                    }
                    break;
                //no
                case 1:
                    break;
                default:
                    args.IsValid = false;
                    cusvBIMQuestions.ErrorMessage = "Please answer Yes or No to BIM question";
                    break;
            }

            //011 - added
            if (args.IsValid)
            {
                //check the max length property on answers and truncate appropriately
                var textboxes = this.Page.AllControlsOfType<TextBox>();
                var bimTextboxes = textboxes.Cast<TextBox>().Where(t => t.ID.ToLower().Contains("bimdesignquestion") && !string.IsNullOrEmpty(t.Text));

                bimTextboxes.ToList<TextBox>().ForEach(bt =>
                {
                    int maxLength = (bt.MaxLength == 0) ? 50 : bt.MaxLength;
                    int textLength = bt.Text.Length;
                    if (textLength > maxLength)
                    {
                        bt.Text = bt.Text.Substring(0, maxLength);
                    }
                });
            }

            #endregion
        }
    }

    //custom validate to validate date
    protected void cusExpirationDate9_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt;

        if (!string.IsNullOrEmpty(txtExpiration9.Text.Trim()))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txtExpiration9.Text.Trim();
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];

                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txtExpiration9.Text.Trim(), out dt)))
                    {
                        args.IsValid = false;
                    }
                    else
                    {
                        DateTime Expiration;
                        Expiration = Convert.ToDateTime(txtExpiration9.Text.Trim());

                        if (Expiration.Year < DateTime.Now.Year)
                        {
                            args.IsValid = false;
                        }
                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month < DateTime.Now.Month))
                        {
                            args.IsValid = false;
                        }

                        else if ((Expiration.Year == DateTime.Now.Year) && (Expiration.Month == DateTime.Now.Month) && (Expiration.Date <= DateTime.Now.Date))
                        {
                            args.IsValid = false;
                        }
                    }
                }
                else
                {
                    args.IsValid = false;
                }
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// Switch scope of work view based on logged in user
    /// </summary>
    /// <param name="IsShow"></param>
    /// <Author>Sooraj Sudhakaran.T</Author>
    ///  <Date>17-SEP-2013</Date>
    private void SwitchDesignConsultantView(bool IsShow)
    {
        //Design Consultant 
        divDesign.Visible = IsShow;
        updPnlDesign.Visible = IsShow;
        if (IsShow)
            hdnScopeTypeOpen.Value = "Design";
        //Others 
        divNonDesign.Visible = !IsShow;
        updpnlScope.Visible = !IsShow;

        //Region - Verbage 
        spnRegionDesign.Visible = IsShow;

        //008 - D.C. BIM questions
        apnBIM.Visible = !IsShow;
        apnBIMDesign.Visible = IsShow;
        IsDesignConsultant = IsShow;
    }


    // display data to edit 
    private void BindControls()
    {
        int resCount = objTradeInformation.GetVQFTrade(Convert.ToInt32(Session["VendorId"])).Length;
        long tradeId = 0;       //003 - assigned zero
        if (resCount > 0)
        {
            tradeId = objTradeInformation.GetVQFTrade(Convert.ToInt32(Session["VendorId"]))[0].PK_TradeID;
            bool? OwnForces = objTradeInformation.GetVQFTrade(Convert.ToInt32(Session["VendorId"]))[0].OwnForces;
            if (OwnForces == true)
            {
                rdlyeano.SelectedIndex = 0;
            }
            else if (OwnForces == false)
            {
                rdlyeano.SelectedIndex = 1;
            }

            txtexplain.Text = objTradeInformation.GetVQFTrade(Convert.ToInt32(Session["VendorId"]))[0].Explain;
            trexplain.Style["display"] = rdlyeano.SelectedIndex == 1 ? "" : "none";

            int flag = 0;

            foreach (DAL.UspGetVQFTradeLicenseNumberResult obj in objTradeInformation.GetVQFTradeLicenseNumber(tradeId))
            {
                string Expiration = Convert.ToDateTime(obj.ExpirationDate).ToShortDateString();
                if (Expiration == "1/1/1753")
                {
                    Expiration = "";
                }

                switch (flag)
                {
                    case 0:
                         ddlCountry.SelectedValue = obj.FK_Country.ConvertToString();//007
                        BindAssociateState(ddlCountry);
                        ddlStates.SelectedValue = Convert.ToString(obj.FK_State);
                        txtLicense.Text = obj.LicenseNumber;
                        if (ddlStates.SelectedItem.Text.Trim().Equals("Other"))
                        {
                            trOther.Style["display"] = "";
                            txtOtherState.Text = obj.OtherState;
                        }
                        txtExpiration.Text = Expiration;
                        break;
                    case 1:
                        spnmoreadd1.Style["display"] = "";
                         ddlCountry1.SelectedValue = obj.FK_Country.ConvertToString();//007
                        BindAssociateState(ddlCountry1);
                        ddlState1.SelectedValue = Convert.ToString(obj.FK_State);
                        txtLicense1.Text = obj.LicenseNumber;
                        if (ddlState1.SelectedItem.Text.Trim().Equals("Other"))
                        {
                            trOther1.Style["display"] = "";
                            txtOther1.Text = obj.OtherState;
                        }
                        txtExpiration1.Text = Expiration;
                        break;
                    case 2:
                        spnmoreadd2.Style["display"] = "";
                         ddlCountry2.SelectedValue = obj.FK_Country.ConvertToString();//007
                        BindAssociateState(ddlCountry2);
                        ddlState2.SelectedValue = Convert.ToString(obj.FK_State);
                        txtLicense2.Text = obj.LicenseNumber;
                        if (ddlState2.SelectedItem.Text.Trim().Equals("Other"))
                        {
                            trOther2.Style["display"] = "";
                            txtOther2.Text = obj.OtherState;
                        }
                        txtExpiration2.Text = Expiration;
                        break;
                    case 3:
                        spnmoreadd3.Style["display"] = "";
                         ddlCountry3.SelectedValue = obj.FK_Country.ConvertToString();//007
                        BindAssociateState(ddlCountry3);
                        ddlState3.SelectedValue = Convert.ToString(obj.FK_State);
                        txtLicense3.Text = obj.LicenseNumber;
                        if (ddlState3.SelectedItem.Text.Trim().Equals("Other"))
                        {
                            trOther3.Style["display"] = "";
                            txtOther3.Text = obj.OtherState;
                        }
                        txtExpiration3.Text = Expiration;
                        break;
                    case 4:
                        spnmoreadd4.Style["display"] = "";
                         ddlCountry4.SelectedValue = obj.FK_Country.ConvertToString();//007
                        BindAssociateState(ddlCountry4);
                        ddlState4.SelectedValue = Convert.ToString(obj.FK_State);
                        txtLicense4.Text = obj.LicenseNumber;
                        if (ddlState4.SelectedItem.Text.Trim().Equals("Other"))
                        {
                            trOther4.Style["display"] = "";
                            txtOther4.Text = obj.OtherState;
                        }
                        txtExpiration4.Text = Expiration;
                        break;
                    case 5:
                        spnmoreadd5.Style["display"] = "";
                         ddlCountry5.SelectedValue = obj.FK_Country.ConvertToString();//007
                        BindAssociateState(ddlCountry5);
                        ddlState5.SelectedValue = Convert.ToString(obj.FK_State);
                        txtLicense5.Text = obj.LicenseNumber;
                        if (ddlState5.SelectedItem.Text.Trim().Equals("Other"))
                        {
                            trOther5.Style["display"] = "";
                            txtOther5.Text = obj.OtherState;
                        }
                        txtExpiration5.Text = Expiration;
                        break;
                    case 6:
                        spnmoreadd6.Style["display"] = "";
                         ddlCountry6.SelectedValue = obj.FK_Country.ConvertToString();//007
                        BindAssociateState(ddlCountry6);
                        ddlState6.SelectedValue = Convert.ToString(obj.FK_State);
                        txtLicense6.Text = obj.LicenseNumber;
                        if (ddlState6.SelectedItem.Text.Trim().Equals("Other"))
                        {
                            trOther6.Style["display"] = "";
                            txtOther6.Text = obj.OtherState;
                        }
                        txtExpiration6.Text = Expiration;
                        break;
                    case 7:
                        spnmoreadd7.Style["display"] = "";
                         ddlCountry7.SelectedValue = obj.FK_Country.ConvertToString();//007
                        BindAssociateState(ddlCountry7);
                        ddlState7.SelectedValue = Convert.ToString(obj.FK_State);
                        txtLicense7.Text = obj.LicenseNumber;
                        if (ddlState7.SelectedItem.Text.Trim().Equals("Other"))
                        {
                            trOther7.Style["display"] = "";
                            txtOther7.Text = obj.OtherState;
                        }
                        txtExpiration7.Text = Expiration;
                        break;
                    case 8:
                        spnmoreadd8.Style["display"] = "";
                         ddlCountry8.SelectedValue = obj.FK_Country.ConvertToString();//007
                        BindAssociateState(ddlCountry8);
                        ddlState8.SelectedValue = Convert.ToString(obj.FK_State);
                        txtLicense8.Text = obj.LicenseNumber;
                        if (ddlState8.SelectedItem.Text.Equals("Other"))
                        {
                            trOther8.Style["display"] = "";
                            txtOther8.Text = obj.OtherState;
                        }
                        txtExpiration8.Text = Expiration;
                        break;
                    case 9:
                        spnmoreadd9.Style["display"] = "";
                        ddlCountry9.SelectedValue = obj.FK_Country.ConvertToString();//007
                        BindAssociateState(ddlCountry9);
                        ddlState9.SelectedValue = Convert.ToString(obj.FK_State);

                        txtLicense9.Text = obj.LicenseNumber;
                        if (ddlState9.SelectedItem.Text.Trim().Equals("Other"))
                        {
                            trOther9.Style["display"] = "";
                            txtOther9.Text = obj.OtherState;
                        }
                        txtExpiration9.Text = Expiration;
                        lnkAddMore.Style["display"] = "none";
                        break;
                }
                flag += 1;
            }

            hdnCount.Value = flag > 0 ? (flag - 1).ToString() : "0";
            lnkAddMore.Style["display"] = flag == 9 ? "none" : "";

            foreach (VMSDAL.UspGetVQFTradeProjectsResult obj in objTradeInformation.GetVQFTradeProjects(tradeId))
            {
                foreach (ListItem lst in chkProject.Items)
                {
                    if (Convert.ToInt32(lst.Value) == obj.FK_ProjectID)
                    {
                        lst.Selected = true;
                    }
                }

                //001 - added check below
                if (obj.ProjectName.ToUpper() == "OTHER")
                {
                    chkProjectOther.Checked = true;
                    txtChkOther.Text = obj.OtherProjectName;
                    txtChkOther.Attributes["style"] = @"display: """;
                }
                else
                {
                    chkProjectOther.Checked = false;
                    txtChkOther.Text = string.Empty;
                    txtChkOther.Attributes["style"] = "display: none";

                }
            }

            BindContinents(tradeId); //006-Sooraj
            foreach (DAL.UspGetVQFTradeRegionsResult obj in objTradeInformation.GetVQFTradeRegions(tradeId))
            {
                chkUS.Checked = true;
                divUS.Style["Display"] = "";
                foreach (GridViewRow gr in grdState1.Rows)
                {
                    HiddenField hdn = (HiddenField)gr.FindControl("hdnStateID");
                    if (obj.FK_State == Convert.ToInt16(hdn.Value))
                    {
                        CheckBox chk = (CheckBox)gr.FindControl("chkCheck");
                        chk.Checked = true;
                    }
                    CheckBoxList chkl = (CheckBoxList)gr.FindControl("chklRegions");
                    if (chkl.Items.Count > 0)
                    {
                        foreach (ListItem lst in chkl.Items)
                        {
                            if (Convert.ToInt16(lst.Value) == obj.FK_State)
                            {
                                lst.Selected = true;
                                chkl.Style["Display"] = "";
                            }
                        }
                    }
                }
                foreach (GridViewRow gr in grdState2.Rows)
                {
                    HiddenField hdn = (HiddenField)gr.FindControl("hdnStateID");
                    if (obj.FK_State == Convert.ToInt16(hdn.Value))
                    {
                        CheckBox chk = (CheckBox)gr.FindControl("chkCheck");
                        chk.Checked = true;
                    }
                    CheckBoxList chkl = (CheckBoxList)gr.FindControl("chklRegions");
                    if (chkl.Items.Count > 0)
                    {
                        foreach (ListItem lst in chkl.Items)
                        {
                            if (Convert.ToInt16(lst.Value) == obj.FK_State)
                            {
                                lst.Selected = true;
                                chkl.Style["Display"] = "";
                            }
                        }
                    }
                }
                foreach (GridViewRow gr in grdState3.Rows)
                {
                    HiddenField hdn = (HiddenField)gr.FindControl("hdnStateID");
                    if (obj.FK_State == Convert.ToInt16(hdn.Value))
                    {
                        CheckBox chk = (CheckBox)gr.FindControl("chkCheck");
                        chk.Checked = true;
                    }
                    CheckBoxList chkl = (CheckBoxList)gr.FindControl("chklRegions");
                    if (chkl.Items.Count > 0)
                    {
                        foreach (ListItem lst in chkl.Items)
                        {
                            if (Convert.ToInt16(lst.Value) == obj.FK_State)
                            {
                                lst.Selected = true;
                                chkl.Style["Display"] = "";
                            }
                        }
                    }
                }
                foreach (GridViewRow gr in grdState2.Rows)
                {
                    HiddenField hdn = (HiddenField)gr.FindControl("hdnStateID");
                    if (obj.FK_State == Convert.ToInt16(hdn.Value))
                    {
                        CheckBox chk = (CheckBox)gr.FindControl("chkCheck");
                        chk.Checked = true;
                    }
                    CheckBoxList chkl = (CheckBoxList)gr.FindControl("chklRegions");
                    if (chkl.Items.Count > 0)
                    {
                        foreach (ListItem lst in chkl.Items)
                        {
                            if (Convert.ToInt16(lst.Value) == obj.FK_State)
                            {
                                lst.Selected = true;
                                chkl.Style["Display"] = "";
                            }
                        }
                    }
                }
                foreach (GridViewRow gr in grdState3.Rows)
                {
                    HiddenField hdn = (HiddenField)gr.FindControl("hdnStateID");
                    if (obj.FK_State == Convert.ToInt16(hdn.Value))
                    {
                        CheckBox chk = (CheckBox)gr.FindControl("chkCheck");
                        chk.Checked = true;
                        if (obj.FK_State == 51)
                        {

                            txtRegions.Text = obj.OtherSate;
                            trRegions.Style["Display"] = "";
                        }
                    }
                }
            }

            foreach (DAL.UspGetVQFTradeScopesResult obj in objTradeInformation.GetVQFTradeScopes(tradeId, -1))
            {
                for (int i = 0; i < grdLevel1.Rows.Count; i++)
                {
                    HiddenField hdn = (HiddenField)grdLevel1.Rows[i].FindControl("hdnParentScopeId");
                    if (obj.ScopeId == Convert.ToInt64(hdn.Value))
                    {
                        HiddenField hdnFlag = (HiddenField)grdLevel1.Rows[i].FindControl("hdnFlag");
                        hdnFlag.Value = "~/Images/minus.gif";
                        Image img = (Image)grdLevel1.Rows[i].FindControl("imgStatus");
                        img.ImageUrl = @"~/Images/minus.gif";

                        GridView grd = (GridView)grdLevel1.Rows[i].FindControl("grdScopeLevele2");
                        grd.Style["display"] = "";
                        foreach (DAL.UspGetVQFTradeScopesResult obj1 in objTradeInformation.GetVQFTradeScopes(tradeId, Convert.ToInt64(hdn.Value)))
                        {
                            for (int j = 0; j < grd.Rows.Count; j++)
                            {
                                HiddenField hdn1 = (HiddenField)grd.Rows[j].FindControl("hdnScopeId");
                                if (obj1.ScopeId == Convert.ToInt64(hdn1.Value))
                                {
                                    CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                                    chk.Checked = true;
                                    CheckBoxList chkl = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                                    foreach (ListItem lst in chkl.Items)
                                    {
                                        lst.Selected = false;
                                        foreach (DAL.UspGetVQFTradeScopesResult obj2 in objTradeInformation.GetVQFTradeScopes(tradeId, Convert.ToInt64(hdn1.Value)))
                                        {
                                            if (Convert.ToInt64(lst.Value) == obj2.ScopeId)
                                            {
                                                lst.Selected = true;
                                                chkl.Style["Display"] = "";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                for (int i = 0; i < grdLevel2.Rows.Count; i++)
                {
                    HiddenField hdn = (HiddenField)grdLevel2.Rows[i].FindControl("hdnParentScopeId");
                    if (obj.ScopeId == Convert.ToInt64(hdn.Value))
                    {
                        HiddenField hdnFlag = (HiddenField)grdLevel2.Rows[i].FindControl("hdnFlag");
                        hdnFlag.Value = "~/Images/minus.gif";
                        Image img = (Image)grdLevel2.Rows[i].FindControl("imgStatus");
                        img.ImageUrl = @"~/Images/minus.gif";
                        GridView grd = (GridView)grdLevel2.Rows[i].FindControl("grdScopeLevele2");
                        grd.Style["display"] = "";
                        foreach (DAL.UspGetVQFTradeScopesResult obj1 in objTradeInformation.GetVQFTradeScopes(tradeId, Convert.ToInt64(hdn.Value)))
                        {
                            for (int j = 0; j < grd.Rows.Count; j++)
                            {
                                HiddenField hdn1 = (HiddenField)grd.Rows[j].FindControl("hdnScopeId");
                                if (obj1.ScopeId == Convert.ToInt64(hdn1.Value))
                                {
                                    CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                                    chk.Checked = true;
                                    CheckBoxList chkl = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                                    foreach (ListItem lst in chkl.Items)
                                    {
                                        lst.Selected = false;
                                        foreach (DAL.UspGetVQFTradeScopesResult obj2 in objTradeInformation.GetVQFTradeScopes(tradeId, Convert.ToInt64(hdn1.Value)))
                                        {

                                            if (Convert.ToInt64(lst.Value) == obj2.ScopeId)
                                            {
                                                lst.Selected = true;
                                                chkl.Style["Display"] = "";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //002 - get the selected PMMIs
                foreach (DataListItem dlItem in dlPMMICodes1.Items)
                {
                    HiddenField hdn = ((HiddenField)dlItem.FindControl("hdnPMMIScopeId"));
                    CheckBox chbx = ((CheckBox)dlItem.FindControl("chkPMMICode"));
                    if (obj.ScopeId == Convert.ToInt64(hdn.Value))
                    {
                        chbx.Checked = true;
                    }
                }

                //004-Sooraj - get the selected Design 
                foreach (DataListItem dlItem in dlistDesign.Items)
                {
                    HiddenField hdn = ((HiddenField)dlItem.FindControl("hdnDesignScopeId"));
                    CheckBox chbx = ((CheckBox)dlItem.FindControl("chkDesignCode"));
                    if (obj.ScopeId == Convert.ToInt64(hdn.Value))
                    {
                        chbx.Checked = true;
                    }
                }

              

            }
        }

        //002 - bind the Trade Questions
        var dsTradeQuestions = new clsTradeInfomation().GetTradeScopeQuestions(tradeId);
        dlTradeQuestions.DataSource = dsTradeQuestions;
        dlTradeQuestions.DataBind();

        
        //003
        this.BindBIM(tradeId);
    }

    // bind state //007
    private void BindState()
    {
        BindState(ddlCountry, ddlStates);
        BindState(ddlCountry1, ddlState1);
        BindState(ddlCountry2, ddlState2);
        BindState(ddlCountry3, ddlState3);
        BindState(ddlCountry4, ddlState4);
        BindState(ddlCountry5, ddlState5);
        BindState(ddlCountry6, ddlState6);
        BindState(ddlCountry7, ddlState7);
        BindState(ddlCountry8, ddlState8);
        BindState(ddlCountry9, ddlState9);
    }

    ///007
    /// <summary>
    /// Fill country  dropdown. 
    /// </summary>
    private void FillCountry()
    {
        var countryList = objCommon.GetCountryList();

        objCommon.BindCountryDropDown(countryList, ddlCountry);
        objCommon.BindCountryDropDown(countryList, ddlCountry1);
        objCommon.BindCountryDropDown(countryList, ddlCountry2);
        objCommon.BindCountryDropDown(countryList, ddlCountry3);
        objCommon.BindCountryDropDown(countryList, ddlCountry4);
        objCommon.BindCountryDropDown(countryList, ddlCountry5);
        objCommon.BindCountryDropDown(countryList, ddlCountry6);
        objCommon.BindCountryDropDown(countryList, ddlCountry7);
        objCommon.BindCountryDropDown(countryList, ddlCountry8);
        objCommon.BindCountryDropDown(countryList, ddlCountry9);

    }

    // display controls in postback
    private void VisibleControls()
    {
        if (rdlyeano.SelectedIndex == 1)
        {
            trexplain.Style["display"] = "";
        }
        else
        {
            trexplain.Style["display"] = "none";
        }

        if (((ddlStates.SelectedIndex > 0) && (ddlStates.SelectedItem.Text.Trim().Trim() == "Other")))
        {
            trOther.Style["display"] = "";
        }
        else
        {
            trOther.Style["display"] = "none";
        }
        if (((ddlState1.SelectedIndex > 0) && (ddlState1.SelectedItem.Text.Trim().Trim() == "Other")))
        {
            trOther1.Style["display"] = "";
        }
        else
        {
            trOther1.Style["display"] = "none";
        }
        if (((ddlState2.SelectedIndex > 0) && (ddlState2.SelectedItem.Text.Trim() == "Other")))
        {
            trOther2.Style["display"] = "";
        }
        else
        {
            trOther2.Style["display"] = "none";
        }
        if (((ddlState3.SelectedIndex > 0) && (ddlState3.SelectedItem.Text.Trim() == "Other")))
        {
            trOther3.Style["display"] = "";
        }
        else
        {
            trOther3.Style["display"] = "none";
        }
        if (((ddlState4.SelectedIndex > 0) && (ddlState4.SelectedItem.Text.Trim() == "Other")))
        {
            trOther4.Style["display"] = "";
        }
        else
        {
            trOther4.Style["display"] = "none";
        }
        if (((ddlState5.SelectedIndex > 0) && (ddlState5.SelectedItem.Text.Trim() == "Other")))
        {
            trOther5.Style["display"] = "";
        }
        else
        {
            trOther5.Style["display"] = "none";
        }
        if (((ddlState6.SelectedIndex > 0) && (ddlState6.SelectedItem.Text.Trim() == "Other")))
        {
            trOther6.Style["display"] = "";
        }
        else
        {
            trOther6.Style["display"] = "none";
        }
        if (((ddlState7.SelectedIndex > 0) && (ddlState7.SelectedItem.Text.Trim() == "Other")))
        {
            trOther7.Style["display"] = "";
        }
        else
        {
            trOther7.Style["display"] = "none";
        }
        if (((ddlState8.SelectedIndex > 0) && (ddlState8.SelectedItem.Text.Trim() == "Other")))
        {
            trOther8.Style["display"] = "";
        }
        else
        {
            trOther8.Style["display"] = "none";
        }
        if (((ddlState9.SelectedIndex > 0) && (ddlState9.SelectedItem.Text.Trim() == "Other")))
        {
            trOther9.Style["display"] = "";
        }
        else
        {
            trOther9.Style["display"] = "none";
        }

        if (!string.IsNullOrEmpty(hdnCount.Value))
        {
            for (int i = 1; i <= Convert.ToInt32(hdnCount.Value); i++)
            {
                switch (i)
                {
                    case 1:
                        spnmoreadd1.Style["display"] = "";
                        break;
                    case 2:
                        spnmoreadd2.Style["display"] = "";
                        break;
                    case 3:
                        spnmoreadd3.Style["display"] = "";
                        break;
                    case 4:
                        spnmoreadd4.Style["display"] = "";
                        break;
                    case 5:
                        spnmoreadd5.Style["display"] = "";
                        break;
                    case 6:
                        spnmoreadd6.Style["display"] = "";
                        break;
                    case 7:
                        spnmoreadd7.Style["display"] = "";
                        break;
                    case 8:
                        spnmoreadd8.Style["display"] = "";
                        break;
                    case 9:
                        spnmoreadd9.Style["display"] = "";
                        lnkAddMore.Style["display"] = "none";
                        break;
                }
            }
        }
        //007
        CheckBox chkBox = null;
        HtmlControl divCountry = null;
        foreach (GridViewRow grd in grdContinent.Rows)
        {
            chkBox = (CheckBox)grd.FindControl("chkContinent");
            divCountry = (HtmlControl)grd.FindControl("divCountries");
            if (chkBox.Checked)
            {
                divCountry.Style["display"] = "";
            }
            else
            {
                divCountry.Style["display"] = "none";
            }
        }

        if (chkUS.Checked)
        {
            
                divUS.Style["display"] = "";
            
            foreach (GridViewRow grd in grdState1.Rows)
            {
                CheckBox chk = (CheckBox)grd.FindControl("chkCheck");
                if (chk.Checked)
                {
                    CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chklRegions");
                    if (chk1.Items.Count > 0)
                    {
                        chk1.Style["display"] = "";
                    }
                }
            }
            foreach (GridViewRow grd in grdState2.Rows)
            {
                CheckBox chk = (CheckBox)grd.FindControl("chkCheck");
                if (chk.Checked)
                {
                    CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chklRegions");
                    if (chk1.Items.Count > 0)
                    {
                        chk1.Style["display"] = "";
                    }
                }
            }

            foreach (GridViewRow grd in grdState3.Rows)
            {
                CheckBox chk = (CheckBox)grd.FindControl("chkCheck");
                HiddenField hdnId = (HiddenField)grd.FindControl("hdnStateID");
                if (chk.Checked)
                {
                    if (hdnId.Value == "51")
                    {
                        trRegions.Style["Display"] = "";
                    }
                    else
                    {
                        CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chklRegions");
                        if (chk1.Items.Count > 0)
                        {
                            chk1.Style["display"] = "";
                        }
                    }
                }
            }
        }
        else
        {
            divUS.Style["display"] = "none";
        }

        for (int i = 0; i < grdLevel1.Rows.Count; i++)
        {

            HiddenField hdn = (HiddenField)grdLevel1.Rows[i].FindControl("hdnFlag");
            if (hdn.Value == "~/Images/minus.gif")
            {
                GridView grd = (GridView)grdLevel1.Rows[i].FindControl("grdScopeLevele2");
                Image img = (Image)grdLevel1.Rows[i].FindControl("imgStatus");
                img.ImageUrl = hdn.Value;
                grd.Style["Display"] = "";
                for (int j = 0; j < grd.Rows.Count; j++)
                {
                    CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                    if (chk.Checked)
                    {
                        CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                        foreach (ListItem lst in chklLevele3.Items)
                        {
                            if (lst.Selected)
                            {
                                chklLevele3.Style["Display"] = "";
                            }
                        }
                    }
                }
            }
            else
            {
                GridView grd = (GridView)grdLevel1.Rows[i].FindControl("grdScopeLevele2");
                Image img = (Image)grdLevel1.Rows[i].FindControl("imgStatus");
                img.ImageUrl = hdn.Value;
                for (int j = 0; j < grd.Rows.Count; j++)
                {
                    CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                    grd.Style["Display"] = "None";
                    if (!chk.Checked)
                    {
                        CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                        foreach (ListItem lst in chklLevele3.Items)
                        {
                            if (!lst.Selected)
                            {
                                chklLevele3.Style["Display"] = "None";
                            }
                        }
                    }
                }
            }
        }

        for (int i = 0; i < grdLevel2.Rows.Count; i++)
        {
            HiddenField hdn = (HiddenField)grdLevel2.Rows[i].FindControl("hdnFlag");
            if (hdn.Value == "~/Images/minus.gif")
            {
                GridView grd = (GridView)grdLevel2.Rows[i].FindControl("grdScopeLevele2");
                Image img = (Image)grdLevel2.Rows[i].FindControl("imgStatus");
                img.ImageUrl = hdn.Value;
                grd.Style["Display"] = "";
                for (int j = 0; j < grd.Rows.Count; j++)
                {
                    CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                    if (chk.Checked)
                    {
                        CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                        foreach (ListItem lst in chklLevele3.Items)
                        {
                            if (lst.Selected)
                            {
                                chklLevele3.Style["Display"] = "";
                            }
                        }
                    }
                }
            }
            else
            {
                GridView grd = (GridView)grdLevel2.Rows[i].FindControl("grdScopeLevele2");
                Image img = (Image)grdLevel2.Rows[i].FindControl("imgStatus");
                img.ImageUrl = hdn.Value;
                for (int j = 0; j < grd.Rows.Count; j++)
                {
                    CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                    grd.Style["Display"] = "None";
                    if (!chk.Checked)
                    {
                        CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                        foreach (ListItem lst in chklLevele3.Items)
                        {
                            if (!lst.Selected)
                            {
                                chklLevele3.Style["Display"] = "None";
                            }
                        }
                    }
                }
            }
        }

        #region 003 bim questions
        //answers:
        //group 1
        switch (rdoBIMQuestion1.SelectedValue.ToUpper())
        {
            case "Y":
                rdoBIMQuestion1.SelectedIndex = 0;
                trBIMQuestion1A.Attributes["Style"] = @"display: ";
                trBIMQuestion1B.Attributes["Style"] = @"display: ";
                break;
            case "N":
                rdoBIMQuestion1.SelectedIndex = 1;
                trBIMQuestion1A.Attributes["Style"] = @"display: none";
                trBIMQuestion1B.Attributes["Style"] = @"display: none";
                break;
            case "":
                rdoBIMQuestion1.SelectedIndex = -1;
                trBIMQuestion1A.Attributes["Style"] = @"display: none";
                trBIMQuestion1B.Attributes["Style"] = @"display: none";
                break;
            default:
                break;
        }
        switch (rdoBIMQuestion1B.SelectedValue.ToUpper())
        {
            case "Y":
                rdoBIMQuestion1B.SelectedIndex = 0;
                trBIMQuestion1C.Attributes["Style"] = @"display: ";
                trBIMQuestion1D.Attributes["Style"] = @"display: none";
                break;
            case "N":
                rdoBIMQuestion1B.SelectedIndex = 1;
                trBIMQuestion1C.Attributes["Style"] = @"display: none";
                trBIMQuestion1D.Attributes["Style"] = @"display: ";
                break;
            case "":
                rdoBIMQuestion1B.SelectedIndex = -1;
                trBIMQuestion1C.Attributes["Style"] = @"display: none";
                trBIMQuestion1D.Attributes["Style"] = @"display: none";
                break;
            default:
                break;
        }
        //group 3
        switch (rdoBIMQuestion3.SelectedValue.ToUpper())
        {
            case "Y":
                rdoBIMQuestion3.SelectedIndex = 0;
                trBIMQuestion3A.Attributes["Style"] = @"display: ";
                break;
            case "N":
                rdoBIMQuestion3.SelectedIndex = 1;
                trBIMQuestion3A.Attributes["Style"] = @"display: none";
                break;
            case "":
                rdoBIMQuestion3.SelectedIndex = -1;
                trBIMQuestion3A.Attributes["Style"] = @"display: none";
                break;
            default:
                break;
        }
        //group 4
        switch (rdoBIMQuestion4.SelectedValue.ToUpper())
        {
            case "Y":
                rdoBIMQuestion4.SelectedIndex = 0;
                trBIMQuestion4A.Attributes["Style"] = @"display: ";
                break;
            case "N":
                rdoBIMQuestion4.SelectedIndex = 1;
                trBIMQuestion4A.Attributes["Style"] = @"display: none";
                break;
            case "":
                rdoBIMQuestion4.SelectedIndex = -1;
                trBIMQuestion4A.Attributes["Style"] = @"display: none";
                break;
            default:
                break;
        }
        //group 5
        switch (rdoBIMQuestion5.SelectedValue.ToUpper())
        {
            case "Y":
                rdoBIMQuestion5.SelectedIndex = 0;
                trBIMQuestion5A.Attributes["Style"] = @"display: ";
                break;
            case "N":
                rdoBIMQuestion5.SelectedIndex = 1;
                trBIMQuestion5A.Attributes["Style"] = @"display: none";
                break;
            case "":
                rdoBIMQuestion5.SelectedIndex = -1;
                trBIMQuestion5A.Attributes["Style"] = @"display: none";
                break;
            default:
                break;
        }

        #endregion
    }

    // bind regions for selected state
    private void BindRegions()
    {
        DataTable RegionsTable = new DataTable();
        RegionsTable = objTradeInformation.GetRegions().Tables[0];

        DataView dv = new DataView();
        dv = RegionsTable.DefaultView;
        dv.RowFilter = "PK_StateID >=1 and PK_StateID <=17";
        grdState1.DataSource = dv;
        grdState1.DataBind();

        DataView dv1 = new DataView();
        dv1 = RegionsTable.DefaultView;
        dv1.RowFilter = "PK_StateID >=18 and PK_StateID <=34";
        grdState2.DataSource = dv1;
        grdState2.DataBind();

        DataView dv4 = new DataView();
        dv4 = RegionsTable.DefaultView;
        dv4.RowFilter = "PK_StateID >=35";
        grdState3.DataSource = dv4;
        grdState3.DataBind();
    }

    // scripts for the controls
    private void CallScripts()
    {
        lnkAddMore.Attributes.Add("onClick", "return Visibletr();");
        ddlState1.Attributes.Add("onChange", "return DisplayOthers('" + ddlState1.ClientID + "','" + trOther1.ClientID + "','" + txtOther1.ClientID + "');");
        ddlState2.Attributes.Add("onChange", "return DisplayOthers('" + ddlState2.ClientID + "','" + trOther2.ClientID + "','" + txtOther2.ClientID + "');");
        ddlState3.Attributes.Add("onChange", "return DisplayOthers('" + ddlState3.ClientID + "','" + trOther3.ClientID + "','" + txtOther3.ClientID + "');");
        ddlState4.Attributes.Add("onChange", "return DisplayOthers('" + ddlState4.ClientID + "','" + trOther4.ClientID + "','" + txtOther4.ClientID + "');");
        ddlState5.Attributes.Add("onChange", "return DisplayOthers('" + ddlState5.ClientID + "','" + trOther5.ClientID + "','" + txtOther5.ClientID + "');");
        ddlState6.Attributes.Add("onChange", "return DisplayOthers('" + ddlState6.ClientID + "','" + trOther6.ClientID + "','" + txtOther6.ClientID + "');");
        ddlState7.Attributes.Add("onChange", "return DisplayOthers('" + ddlState7.ClientID + "','" + trOther7.ClientID + "','" + txtOther7.ClientID + "');");
        ddlState8.Attributes.Add("onChange", "return DisplayOthers('" + ddlState8.ClientID + "','" + trOther8.ClientID + "','" + txtOther8.ClientID + "');");
        ddlState9.Attributes.Add("onChange", "return DisplayOthers('" + ddlState9.ClientID + "','" + trOther9.ClientID + "','" + txtOther9.ClientID + "');");
        CheckBoxList chkl, chkl1, chkl2;

        chkl = new CheckBoxList();
        chkl = (CheckBoxList)grdState1.Rows[4].FindControl("chklRegions");

        chkl1 = new CheckBoxList();
        chkl1 = (CheckBoxList)grdState1.Rows[8].FindControl("chklRegions");

        chkl2 = new CheckBoxList();
        chkl2 = (CheckBoxList)grdState3.Rows[8].FindControl("chklRegions");

       

        chkCheckAll.Attributes.Add("onClick", "CheckAllRegions(this.checked,'" + chkl.ClientID + "','" + chkl1.ClientID + "','" + chkl2.ClientID + "','" + trRegions.ClientID + "');");
        ddlStates.Attributes.Add("onChange", "return DisplayOthers('" + ddlStates.ClientID + "','" + trOther.ClientID + "','" + txtOtherState.ClientID + "');");


        //008
        if (!IsDesignConsultant)
        {
            //003 - BIM Questions
            //question 1
            rdoBIMQuestion1.Items[0].Attributes.Add("onclick", "DisplayQuestion('" + trBIMQuestion1A.ClientID + "', 'yes', '');DisplayQuestion('" + trBIMQuestion1B.ClientID + "', 'yes','');");
            rdoBIMQuestion1.Items[1].Attributes.Add("onclick", "DisplayQuestion('" + trBIMQuestion1B.ClientID + "', 'no','-1');DisplayQuestion('" + trBIMQuestion1A.ClientID + "', 'no','');" +
                                                               "DisplayQuestion('" + trBIMQuestion1C.ClientID + "', 'no','');DisplayQuestion('" + trBIMQuestion1D.ClientID + "', 'no','');");

            //question 1B
            rdoBIMQuestion1B.Items[0].Attributes.Add("onclick", "DisplayQuestion('" + trBIMQuestion1C.ClientID + "', 'yes','');DisplayQuestion('" + trBIMQuestion1D.ClientID + "', 'no','');");
            rdoBIMQuestion1B.Items[1].Attributes.Add("onclick", "DisplayQuestion('" + trBIMQuestion1C.ClientID + "', 'no','');DisplayQuestion('" + trBIMQuestion1D.ClientID + "', 'yes','');");

            //question 3
            rdoBIMQuestion3.Items[0].Attributes.Add("onclick", "DisplayQuestion('" + trBIMQuestion3A.ClientID + "', 'yes','');");
            rdoBIMQuestion3.Items[1].Attributes.Add("onclick", "DisplayQuestion('" + trBIMQuestion3A.ClientID + "', 'no','');");

            //question 4
            rdoBIMQuestion4.Items[0].Attributes.Add("onclick", "DisplayQuestion('" + trBIMQuestion4A.ClientID + "', 'yes','');");
            rdoBIMQuestion4.Items[1].Attributes.Add("onclick", "DisplayQuestion('" + trBIMQuestion4A.ClientID + "', 'no','');");

            //question 5
            rdoBIMQuestion5.Items[0].Attributes.Add("onclick", "DisplayQuestion('" + trBIMQuestion5A.ClientID + "', 'yes','');");
            rdoBIMQuestion5.Items[1].Attributes.Add("onclick", "DisplayQuestion('" + trBIMQuestion5A.ClientID + "', 'no','');"); 
        }
        else
        {
            //008 - all below
            rdoBIMDesignQuestion1.Items[0].Attributes.Add("onclick","DisplayQuestion('" + trBIMDesignQuestion1A.ClientID + "', 'yes', '');" +
                                                                    "DisplayQuestion('" + trBIMDesignQuestion1B.ClientID + "', 'yes','-1');" +
                                                                    "DisplayQuestion('" + trBIMDesignQuestion1C.ClientID + "', 'yes','');" +
                                                                    "DisplayQuestion('" + trBIMDesignQuestion1D.ClientID + "', 'yes','');" +
                                                                    "DisplayQuestion('" + trBIMDesignQuestion1E.ClientID + "', 'yes','-1');" +
                                                                    "DisplayQuestion('" + trBIMDesignQuestion1F.ClientID + "', 'yes','');");

            rdoBIMDesignQuestion1.Items[1].Attributes.Add("onclick", "DisplayQuestion('" + trBIMDesignQuestion1A.ClientID + "', 'no', '');" +
                                                                     "DisplayQuestion('" + trBIMDesignQuestion1B.ClientID + "', 'no','-1');" +
                                                                     "DisplayQuestion('" + trBIMDesignQuestion1C.ClientID + "', 'no','');" +
                                                                     "DisplayQuestion('" + trBIMDesignQuestion1D.ClientID + "', 'no','');" +
                                                                     "DisplayQuestion('" + trBIMDesignQuestion1E.ClientID + "', 'no','-1');" +
                                                                     "DisplayQuestion('" + trBIMDesignQuestion1F.ClientID + "', 'no','');");
        }

        //006-Sooraj

        chkUS.Attributes.Add("onClick", "CheckUSRegions(this.checked,'" + chkl.ClientID + "','" + chkl1.ClientID + "','" + chkl2.ClientID + "','" + trRegions.ClientID + "','" + chkUS.ClientID + "','" + divUS.ClientID + "');"); //006-Sooraj


    }

    // bind scopes
    private void BindScopes()
    {
        //004-Sooraj - Bind control based on logged in user
        if (!divDesign.Visible)
        {
            //002 - changed to use new DAL
            VMSDAL.UspGetTradeScopesByTypeResult[] objScopes = objTradeInformation.GetTradeScopeByType(-1, ScopesCodeType.MasterFormat);
            int count = objScopes.Length;
            int count1 = (Convert.ToInt32(Math.Round(Convert.ToDouble(count / 2))));
            grdLevel1.DataSource = objScopes.Take(count1 + 1);
            grdLevel1.DataBind();
            grdLevel2.DataSource = objScopes.Skip(count1 + 1);
            grdLevel2.DataBind();

            //002 - Bind PMMI scopes
            VMSDAL.UspGetTradeScopesByTypeResult[] objPMMIScopes = objTradeInformation.GetTradeScopeByType(-1, ScopesCodeType.PMMI).OrderBy(s => s.RootNode).ToArray();
            dlPMMICodes1.DataSource = objPMMIScopes;
            dlPMMICodes1.DataBind();

        }
        else
        {
            //004-Sooraj - Bind Design Scope 
            VMSDAL.UspGetTradeScopesByTypeResult[] objDesignScopes = objTradeInformation.GetTradeScopeByType(-1, ScopesCodeType.DesignConsultant).OrderBy(s => s.RootNode).ToArray();
            dlistDesign.DataSource = objDesignScopes;
            dlistDesign.DataBind();
        }
        //002 - bind the Trade Questions
        var dsTradeQuestions = new clsTradeInfomation().GetTradeScopeQuestions(0);
        dlTradeQuestions.DataSource = dsTradeQuestions;
        dlTradeQuestions.DataBind();

    }

    // validate page and list the erro messages
    //008
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                if(!validator.ErrorMessage.StartsWith("<li>"))
                    errmsg += "<li>" + validator.ErrorMessage + "</li>";
                else errmsg += validator.ErrorMessage;
                
            }
        }
        return errmsg;
    }

    // extract number from the check box selected
    static string ExtractNumbers(string expr)
    {
        return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
    }

    // Get trade id
    private long InsertTrade(Byte status)
    {
        if (rdlyeano.SelectedIndex < 0)
        {
            Forces = null;
        }
        else
        {
            if (rdlyeano.SelectedIndex == 0)
            {
                Forces = true;
            }
            else if (rdlyeano.SelectedIndex == 1)
            {
                Forces = false;
            }
        }
        long TradeId = objTradeInformation.InsertTrade(Forces, txtexplain.Text.Trim(), Convert.ToInt64(Session["VendorId"]), status);
        return TradeId;
    }



    //006 - Bind Continents
    private void BindContinents(long tradeId)
    {
        CheckBoxList chkList = null;
        CheckBox chk = null;
        HtmlControl divCountry = null;
        int continentID = 0;
        List<VendorTradeContient> vendorContinent = null;
        var regionContinents = objTradeInformation.GetVQFTradeRegionContinents(tradeId);

        foreach (GridViewRow grd in grdContinent.Rows)
        {
            continentID = grdContinent.DataKeys[grd.RowIndex].Value.ConvertToString().ConvertToShortInt();

            vendorContinent = regionContinents.Where(x => x.ContinentID == continentID).ToList();

            if (vendorContinent != null && vendorContinent.Count > 0)
            {
                chk = (CheckBox)grd.FindControl("chkContinent");
                chk.Checked = true;
                chkList = (CheckBoxList)grd.FindControl("chkContinentCountries");
                divCountry = (HtmlControl)grd.FindControl("divCountries");
                divCountry.Style["display"] = "";
                foreach (VendorTradeContient continent in vendorContinent)
                {
                    chkList.Items.FindByValue(continent.CountryID.ConvertToString()).Selected = true;
                }
            }
        }
    }



    //006 Insert Continent
    private void InsertContinent(long tradeId)
    {


        CheckBox chk = null;
        VendorTradeContient checkedItem = null;
        CheckBoxList chkList = null;
        int continentID = 0;

        foreach (GridViewRow grd in grdContinent.Rows)
        {
            continentID = grdContinent.DataKeys[grd.RowIndex].Value.ConvertToString().ConvertToShortInt();
            chk = (CheckBox)grd.FindControl("chkContinent");

            if (chk.Checked)
            {
                chkList = (CheckBoxList)grd.FindControl("chkContinentCountries");

               if(IsCountrySelectedForContinent(chkList)) //006- Fix 
                    foreach (ListItem item in chkList.Items)
                    {
                        if (item.Selected)
                        {
                            checkedItem = new VendorTradeContient
                            {
                                CountryID = item.Value.ConvertToShortInt(),
                                ContinentID = continentID,
                                TradeID = tradeId,
                            };
                            objTradeInformation.InsertVQFTradeRegionContinents(checkedItem);
                        }
                    }


            }
        }

    }

    /// <summary>
    /// 006- Check if any one of the Country selected for Continent
    /// </summary>
    /// <returns></returns>
    private bool IsCountrySelectedForContinent(CheckBoxList chkCountry)
    {
        return chkCountry.Items.Cast<ListItem>().Any(i => i.Selected);
    }

    // Insert Regions
    private void InsertRegions(long TradeId)
    {
        //  chkUS.Checked = CheckIfRegionsSelectedForUS(); //006 -Sooraj


        InsertContinent(TradeId);//006 -Sooraj

        if (chkUS.Checked) //Insert US only if any of the state selected under US selected
        {
            foreach (GridViewRow grd in grdState1.Rows)
            {
                CheckBox chk = (CheckBox)grd.FindControl("chkCheck");
                CheckBoxList chkl = (CheckBoxList)grd.FindControl("chklRegions");
                HiddenField hdn = (HiddenField)grd.FindControl("hdnStateID");
                if (chk.Checked)
                {
                    objTradeInformation.InsertTradeRegions(Convert.ToInt16(hdn.Value), null, TradeId);
                    if (chkl.Items.Count > 0)
                    {
                        foreach (ListItem lst in chkl.Items)
                        {
                            if (lst.Selected)
                            {
                                objTradeInformation.InsertTradeRegions(Convert.ToInt16(lst.Value), null, TradeId);
                            }
                        }
                    }
                }
            }

            foreach (GridViewRow grd in grdState2.Rows)
            {
                CheckBox chk = (CheckBox)grd.FindControl("chkCheck");
                CheckBoxList chkl = (CheckBoxList)grd.FindControl("chklRegions");
                HiddenField hdn = (HiddenField)grd.FindControl("hdnStateID");
                if (chk.Checked)
                {
                    objTradeInformation.InsertTradeRegions(Convert.ToInt16(hdn.Value), null, TradeId);
                }
                if (chkl.Items.Count > 0)
                {
                    foreach (ListItem lst in chkl.Items)
                    {
                        if (lst.Selected)
                        {
                            objTradeInformation.InsertTradeRegions(Convert.ToInt16(lst.Value), null, TradeId);
                        }
                    }
                }
            }
            foreach (GridViewRow grd in grdState3.Rows)
            {
                CheckBox chk = (CheckBox)grd.FindControl("chkCheck");
                //  TextBox txt = (TextBox)grd.FindControl("txtRegions");
                CheckBoxList chkl = (CheckBoxList)grd.FindControl("chklRegions");
                HiddenField hdn = (HiddenField)grd.FindControl("hdnStateID");
                if (chk.Checked)
                {
                    if (chkl.Items.Count > 0)
                    {
                        foreach (ListItem lst in chkl.Items)
                        {
                            if (lst.Selected)
                            {
                                objTradeInformation.InsertTradeRegions(Convert.ToInt16(lst.Value), null, TradeId);
                            }
                        }
                    }
                    if (hdn.Value == "51")
                    {
                        objTradeInformation.InsertTradeRegions(Convert.ToInt16(hdn.Value), txtRegions.Text.Trim(), TradeId);
                    }
                    else
                    {
                        objTradeInformation.InsertTradeRegions(Convert.ToInt16(hdn.Value), null, TradeId);
                    }
                }
            }
        }
    }

    // Insert Projects
    private void InsertProjects(long TradeId)
    {
        foreach (ListItem lst in chkProject.Items)
        {
            if (lst.Selected)
            {
                objTradeInformation.InsertTradeProjects(Convert.ToInt32(lst.Value), TradeId);
            }
        }

        //002 - added
        if (this.chkProjectOther.Checked)
        {
            if (!string.IsNullOrEmpty(txtChkOther.Text.Trim()))
            {
                objTradeInformation.InsertOtherTradeProject(TradeId, txtChkOther.Text.Trim());
            }
        }
    }

    // Insert Scopes
    private void InsertScopes(long TradeId)
    {
        #region Grid Level 1
        for (int i = 0; i < grdLevel1.Rows.Count; i++)
        {
            int flag = 0;
            HiddenField hdnFlag = (HiddenField)grdLevel1.Rows[i].FindControl("hdnFlag");
            //if (hdnFlag.Value == "~/Images/minus.gif")
            //{
            GridView grd = (GridView)grdLevel1.Rows[i].FindControl("grdScopeLevele2");
            for (int j = 0; j < grd.Rows.Count; j++)
            {
                CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                if (chk.Checked)
                {
                    if (flag == 0)
                    {
                        HiddenField hdnScopeId = (HiddenField)grdLevel1.Rows[i].FindControl("hdnParentScopeId");
                        objTradeInformation.InsertTradeScopes(Convert.ToInt32(hdnScopeId.Value), TradeId);
                        flag = 1;
                    }
                    HiddenField hdnLevel2ScopeId = (HiddenField)grd.Rows[j].FindControl("hdnScopeId");
                    objTradeInformation.InsertTradeScopes(Convert.ToInt32(hdnLevel2ScopeId.Value), TradeId);
                    CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");

                    foreach (ListItem lst in chklLevele3.Items)
                    {
                        //Included by SGS for issue no : 4
                        if (lst.Selected)
                        {
                            objTradeInformation.InsertTradeScopes(Convert.ToInt32(lst.Value), TradeId);
                        }
                    }
                }

                // }
            }
        }
        #endregion

        #region Grid Level 2
        for (int i = 0; i < grdLevel2.Rows.Count; i++)
        {
            int flag = 0;
            HiddenField hdnFlag = (HiddenField)grdLevel2.Rows[i].FindControl("hdnFlag");
            //if (hdnFlag.Value == "~/Images/minus.gif")
            //{
            GridView grd = (GridView)grdLevel2.Rows[i].FindControl("grdScopeLevele2");
            for (int j = 0; j < grd.Rows.Count; j++)
            {
                CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                if (chk.Checked)
                {
                    if (flag == 0)
                    {
                        HiddenField hdnScopeId = (HiddenField)grdLevel2.Rows[i].FindControl("hdnParentScopeId");
                        objTradeInformation.InsertTradeScopes(Convert.ToInt32(hdnScopeId.Value), TradeId);
                        flag = 1;
                    }
                    HiddenField hdnLevel2ScopeId = (HiddenField)grd.Rows[j].FindControl("hdnScopeId");
                    objTradeInformation.InsertTradeScopes(Convert.ToInt32(hdnLevel2ScopeId.Value), TradeId);
                    CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");

                    foreach (ListItem lst in chklLevele3.Items)
                    {
                        //Included by SGS for issue no : 4
                        if (lst.Selected)
                        {
                            objTradeInformation.InsertTradeScopes(Convert.ToInt32(lst.Value), TradeId);
                        }
                    }
                }
            }
            // }
        }
        #endregion

        #region PMMI Codes
        foreach (DataListItem item in dlPMMICodes1.Items)
        {
            HiddenField hdn = ((HiddenField)item.FindControl("hdnPMMIScopeId"));
            CheckBox chbx = ((CheckBox)item.FindControl("chkPMMICode"));
            if (chbx.Checked)
            {
                objTradeInformation.InsertTradeScopes(Convert.ToInt32(hdn.Value), TradeId);
            }
        }
        #endregion
        #region Design Codes
        // Sooraj --Insert Design Scope
        foreach (DataListItem item in dlistDesign.Items)
        {
            HiddenField hdnDesign = ((HiddenField)item.FindControl("hdnDesignScopeId"));
            CheckBox chbxDesign = ((CheckBox)item.FindControl("chkDesignCode"));
            if (chbxDesign.Checked)
            {
                objTradeInformation.InsertTradeScopes(Convert.ToInt32(hdnDesign.Value), TradeId);
            }
        }
        #endregion

    }

    // Insert license number
    private void InsertTradeLicenseNumber(long TradeId)
    {
        if (!(string.IsNullOrEmpty(txtExpiration.Text.Trim())) || !(string.IsNullOrEmpty(txtLicense.Text.Trim())) || (ddlStates.SelectedIndex > 0))
        {
            short State = ddlStates.SelectedIndex > 0 ? Convert.ToInt16(ddlStates.SelectedValue) : (byte)0;
            if (string.IsNullOrEmpty(txtExpiration.Text.Trim()))
            {
                ExpirationDate = Convert.ToDateTime("01/01/1753");
            }
            else
            {
                if (DateValidation(txtExpiration))
                {
                    ExpirationDate = Convert.ToDateTime(txtExpiration.Text.Trim());
                }
                else
                {
                    ExpirationDate = Convert.ToDateTime("01/01/1753");
                }
            }
            objTradeInformation.InsertTradeLicenseNumber(ExpirationDate, State, txtLicense.Text.Trim(), txtOtherState.Text.Trim(), TradeId,ddlCountry.SelectedValue.ConvertToShortInt());//007

        }
        ClearTradeInformation(trOther, ddlStates, txtExpiration, txtLicense, txtOtherState);

        if (!(string.IsNullOrEmpty(txtExpiration1.Text.Trim())) || !(string.IsNullOrEmpty(txtLicense1.Text.Trim())) || (ddlState1.SelectedIndex > 0))
        {
            short State = ddlState1.SelectedIndex > 0 ? Convert.ToInt16(ddlState1.SelectedValue) : (byte)0;
            if (string.IsNullOrEmpty(txtExpiration1.Text.Trim()))
            {
                ExpirationDate = Convert.ToDateTime("01/01/1753");
            }
            else
            {
                if (DateValidation(txtExpiration1))
                {
                    ExpirationDate = Convert.ToDateTime(txtExpiration1.Text.Trim());
                }
                else
                {
                    ExpirationDate = Convert.ToDateTime("01/01/1753");
                }
            }
            objTradeInformation.InsertTradeLicenseNumber(ExpirationDate, State, txtLicense1.Text.Trim(), txtOther1.Text.Trim(), TradeId, ddlCountry1.SelectedValue.ConvertToShortInt());//007

        }
        ClearTradeInformation(trOther1, ddlState1, txtExpiration1, txtLicense1, txtOther1);
        if (!(string.IsNullOrEmpty(txtExpiration2.Text.Trim())) || !(string.IsNullOrEmpty(txtLicense2.Text.Trim())) || (ddlState2.SelectedIndex > 0))
        {
            short State = 0;
            if (ddlState2.SelectedIndex > 0)
            {
                State = Convert.ToInt16(ddlState2.SelectedValue);
            }
            if (string.IsNullOrEmpty(txtExpiration2.Text.Trim()))
            {
                ExpirationDate = Convert.ToDateTime("01/01/1753");
            }
            else
            {
                if (DateValidation(txtExpiration2))
                {
                    ExpirationDate = Convert.ToDateTime(txtExpiration2.Text.Trim());
                }
                else
                {
                    ExpirationDate = Convert.ToDateTime("01/01/1753");
                }
            }
            objTradeInformation.InsertTradeLicenseNumber(ExpirationDate, State, txtLicense2.Text.Trim(), txtOther2.Text.Trim(), TradeId, ddlCountry2.SelectedValue.ConvertToShortInt());//007

        }
        ClearTradeInformation(trOther2, ddlState2, txtExpiration2, txtLicense2, txtOther2);
        if (!(string.IsNullOrEmpty(txtExpiration3.Text.Trim())) || !(string.IsNullOrEmpty(txtLicense3.Text.Trim())) || (ddlState3.SelectedIndex > 0))
        {
            short State = 0;
            if (ddlState3.SelectedIndex > 0)
            {
                State = Convert.ToInt16(ddlState3.SelectedValue);
            }
            if (string.IsNullOrEmpty(txtExpiration3.Text.Trim()))
            {
                ExpirationDate = Convert.ToDateTime("01/01/1753");
            }
            else
            {
                if (DateValidation(txtExpiration3))
                {
                    ExpirationDate = Convert.ToDateTime(txtExpiration3.Text.Trim());
                }
                else
                {
                    ExpirationDate = Convert.ToDateTime("01/01/1753");
                }
            }
            objTradeInformation.InsertTradeLicenseNumber(ExpirationDate, State, txtLicense3.Text.Trim(), txtOther3.Text.Trim(), TradeId, ddlCountry3.SelectedValue.ConvertToShortInt());//007

        }
        ClearTradeInformation(trOther3, ddlState3, txtExpiration3, txtLicense3, txtOther3);
        if (!(string.IsNullOrEmpty(txtExpiration4.Text.Trim())) || !(string.IsNullOrEmpty(txtLicense4.Text.Trim())) || (ddlState4.SelectedIndex > 0))
        {
            short State = 0;
            if (ddlState4.SelectedIndex > 0)
            {
                State = Convert.ToInt16(ddlState4.SelectedValue);
            }
            if (string.IsNullOrEmpty(txtExpiration4.Text.Trim()))
            {
                ExpirationDate = Convert.ToDateTime("01/01/1753");
            }
            else
            {
                if (DateValidation(txtExpiration4))
                {
                    ExpirationDate = Convert.ToDateTime(txtExpiration4.Text.Trim());
                }
                else
                {
                    ExpirationDate = Convert.ToDateTime("01/01/1753");
                }
            }
            objTradeInformation.InsertTradeLicenseNumber(ExpirationDate, State, txtLicense4.Text.Trim(), txtOther4.Text.Trim(), TradeId, ddlCountry4.SelectedValue.ConvertToShortInt());//007

        }
        ClearTradeInformation(trOther4, ddlState4, txtExpiration4, txtLicense4, txtOther4);
        if (!(string.IsNullOrEmpty(txtExpiration5.Text.Trim())) || !(string.IsNullOrEmpty(txtLicense5.Text.Trim())) || (ddlState5.SelectedIndex > 0))
        {
            short State = 0;
            if (ddlState5.SelectedIndex > 0)
            {
                State = Convert.ToInt16(ddlState5.SelectedValue);
            }
            if (string.IsNullOrEmpty(txtExpiration5.Text.Trim()))
            {
                ExpirationDate = Convert.ToDateTime("01/01/1753");
            }
            else
            {
                if (DateValidation(txtExpiration5))
                {
                    ExpirationDate = Convert.ToDateTime(txtExpiration5.Text.Trim());
                }
                else
                {
                    ExpirationDate = Convert.ToDateTime("01/01/1753");
                }
            }
            objTradeInformation.InsertTradeLicenseNumber(ExpirationDate, State, txtLicense5.Text.Trim(), txtOther5.Text.Trim(), TradeId, ddlCountry5.SelectedValue.ConvertToShortInt());//007

        }
        ClearTradeInformation(trOther5, ddlState5, txtExpiration5, txtLicense5, txtOther5);
        if (!(string.IsNullOrEmpty(txtExpiration6.Text.Trim())) || !(string.IsNullOrEmpty(txtLicense6.Text.Trim())) || (ddlState6.SelectedIndex > 0))
        {
            short State = 0;
            if (ddlState6.SelectedIndex > 0)
            {
                State = Convert.ToInt16(ddlState6.SelectedValue);
            }
            if (string.IsNullOrEmpty(txtExpiration6.Text.Trim()))
            {
                ExpirationDate = Convert.ToDateTime("01/01/1753");
            }
            else
            {
                if (DateValidation(txtExpiration6))
                {
                    ExpirationDate = Convert.ToDateTime(txtExpiration6.Text.Trim());
                }
                else
                {
                    ExpirationDate = Convert.ToDateTime("01/01/1753");
                }
            }
            objTradeInformation.InsertTradeLicenseNumber(ExpirationDate, State, txtLicense6.Text.Trim(), txtOther6.Text.Trim(), TradeId, ddlCountry6.SelectedValue.ConvertToShortInt());//007

        }
        ClearTradeInformation(trOther6, ddlState6, txtExpiration6, txtLicense6, txtOther6);
        if (!(string.IsNullOrEmpty(txtExpiration7.Text.Trim())) || !(string.IsNullOrEmpty(txtLicense7.Text.Trim())) || (ddlState7.SelectedIndex > 0))
        {
            short State = 0;
            if (ddlState7.SelectedIndex > 0)
            {
                State = Convert.ToInt16(ddlState7.SelectedValue);
            }
            if (string.IsNullOrEmpty(txtExpiration7.Text.Trim()))
            {
                ExpirationDate = Convert.ToDateTime("01/01/1753");
            }
            else
            {
                if (DateValidation(txtExpiration7))
                {
                    ExpirationDate = Convert.ToDateTime(txtExpiration7.Text.Trim());
                }
                else
                {
                    ExpirationDate = Convert.ToDateTime("01/01/1753");
                }
            }
            objTradeInformation.InsertTradeLicenseNumber(ExpirationDate, State, txtLicense7.Text.Trim(), txtOther7.Text.Trim(), TradeId, ddlCountry7.SelectedValue.ConvertToShortInt());//007

        }
        ClearTradeInformation(trOther7, ddlState7, txtExpiration7, txtLicense7, txtOther7);
        if (!(string.IsNullOrEmpty(txtExpiration8.Text.Trim())) || !(string.IsNullOrEmpty(txtLicense8.Text.Trim())) || (ddlState8.SelectedIndex > 0))
        {
            short State = 0;
            if (ddlState8.SelectedIndex > 0)
            {
                State = Convert.ToInt16(ddlState8.SelectedValue);
            }
            if (string.IsNullOrEmpty(txtExpiration8.Text.Trim()))
            {
                ExpirationDate = Convert.ToDateTime("01/01/1753");
            }
            else
            {
                if (DateValidation(txtExpiration8))
                {
                    ExpirationDate = Convert.ToDateTime(txtExpiration8.Text.Trim());
                }
                else
                {
                    ExpirationDate = Convert.ToDateTime("01/01/1753");
                }
            }
            objTradeInformation.InsertTradeLicenseNumber(ExpirationDate, State, txtLicense8.Text.Trim(), txtOther8.Text.Trim(), TradeId, ddlCountry8.SelectedValue.ConvertToShortInt());//007

        }
        ClearTradeInformation(trOther8, ddlState8, txtExpiration8, txtLicense8, txtOther8);
        if (!(string.IsNullOrEmpty(txtExpiration9.Text.Trim())) || !(string.IsNullOrEmpty(txtLicense9.Text.Trim())) || (ddlState9.SelectedIndex > 0))
        {
            short State = 0;
            if (ddlState9.SelectedIndex > 0)
            {
                State = Convert.ToInt16(ddlState9.SelectedValue);
            }
            if (string.IsNullOrEmpty(txtExpiration9.Text.Trim()))
            {
                ExpirationDate = Convert.ToDateTime("01/01/1753");
            }
            else
            {
                if (DateValidation(txtExpiration9))
                {
                    ExpirationDate = Convert.ToDateTime(txtExpiration9.Text.Trim());
                }
                else
                {
                    ExpirationDate = Convert.ToDateTime("01/01/1753");
                }
            }
            objTradeInformation.InsertTradeLicenseNumber(ExpirationDate, State, txtLicense9.Text.Trim(), txtOther9.Text.Trim(), TradeId, ddlCountry9.SelectedValue.ConvertToShortInt());//007

        }
        ClearTradeInformation(trOther9, ddlState9, txtExpiration9, txtLicense9, txtOther9);
    }

    //validate date
    protected bool DateValidation(TextBox txt1)
    {
        bool validDate = false;
        DateTime dt;
        if (!string.IsNullOrEmpty(txt1.Text.Trim()))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txt1.Text.Trim();
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];

                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txt1.Text.Trim(), out dt)))
                    {
                        validDate = false;
                    }
                    else
                    {
                        DateTime Expiration;
                        Expiration = Convert.ToDateTime(txt1.Text.Trim());
                        validDate = true;
                    }
                }
                else
                {
                    validDate = false;
                }
            }
            else
            {
                validDate = false;
            }
        }
        return validDate;
    }

    #endregion

    /// <summary>
    /// Need to handle the tool tip onm post back as it is being removed
    /// </summary>
    private void LoadToolTipOnPostBack()
    {
        objCommon.AddToolTipToItems(ddlCountry);
        objCommon.AddToolTipToItems(ddlCountry1);
        objCommon.AddToolTipToItems(ddlCountry2);
        objCommon.AddToolTipToItems(ddlCountry3);
        objCommon.AddToolTipToItems(ddlCountry4);
        objCommon.AddToolTipToItems(ddlCountry5);
        objCommon.AddToolTipToItems(ddlCountry6);
        objCommon.AddToolTipToItems(ddlCountry7);
        objCommon.AddToolTipToItems(ddlCountry8);
        objCommon.AddToolTipToItems(ddlCountry9);
    }


    /// <summary>
    /// 006 -Sooraj
    /// </summary>
    /// <param name="countryDDL"></param>
    private void BindAssociateState(DropDownList countryDDL, bool IsBind = true, bool IsClear = false)
    {
        switch (countryDDL.ID)
        {
            case "ddlCountry":
                if (IsBind)
                    BindState(countryDDL, ddlStates);
                break;
            case "ddlCountry1":
                if (IsBind)
                    BindState(countryDDL, ddlState1);
                break;
            case "ddlCountry2":
                if (IsBind)
                    BindState(countryDDL, ddlState2);
                break;
            case "ddlCountry3":
                if (IsBind)
                    BindState(countryDDL, ddlState3);
                break;
            case "ddlCountry4":
                if (IsBind)
                    BindState(countryDDL, ddlState4);
                break;
            case "ddlCountry5":
                if (IsBind)
                    BindState(countryDDL, ddlState5);
                break;
            case "ddlCountry6":
                if (IsBind)
                    BindState(countryDDL, ddlState6);
                break;
            case "ddlCountry7":
                if (IsBind)
                    BindState(countryDDL, ddlState7);
                break;
            case "ddlCountry8":
                if (IsBind)
                    BindState(countryDDL, ddlState8);
                break;
            case "ddlCountry9":
                if (IsBind)
                    BindState(countryDDL, ddlState9);
                break;
        }
    }

    //006-Sooraj
    private void BindState(DropDownList countryDDL, DropDownList stateDDL)
    {
        var stateList = objCommon.GetStateByCountry(countryDDL.SelectedValue.ConvertToShortInt());

        objCommon.BindStateDropDownByCountry(stateList, stateDDL);
    }

    // SOC


    private void ClearTradeInformation(HtmlTableRow tableRow, DropDownList dropDownList, TextBox textBox1, TextBox textBox2, TextBox textBox3)
    {
        tableRow.Style["display"] = "none";
        dropDownList.SelectedIndex = 0;
        textBox1.Text = "";
        textBox2.Text = "";
        textBox3.Text = "";
    }

    /// <summary>
    /// 002 - implemented
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlPMMICodes1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (((VMSDAL.UspGetTradeScopesByTypeResult)e.Item.DataItem).Name.StartsWith("provide"))
        {
            CheckBox chk = (e.Item.FindControl("chkPMMICode") as CheckBox);
            chk.Text = "We " + ((VMSDAL.UspGetTradeScopesByTypeResult)e.Item.DataItem).Name;
        }
    }

    // EOC

}

//011 - Added
public static class TradeExtensions
{
    //Returns all controls in all levels:
    public static IEnumerable<Control> AllControls(this Control theStartControl)
    {
        var controlsInThisLevel = theStartControl.Controls.Cast<Control>();
        return controlsInThisLevel.SelectMany(AllControls).Concat(controlsInThisLevel);
    }

    //Returns all controls of a certain type in all levels:
    public static IEnumerable<Control> AllControls<TheControlType>(this Control theStartControl) where TheControlType : Control
    {
        var controlsInThisLevel = theStartControl.Controls.Cast<Control>();
        return controlsInThisLevel.SelectMany(AllControls<TheControlType>).Concat(controlsInThisLevel.OfType<TheControlType>());
    }

    //(Another way) Returns all controls of a certain type in all levels, integrity derivation:
    public static IEnumerable<Control> AllControlsOfType<TheControlType>(this Control theStartControl) where TheControlType : Control
    {
        return theStartControl.AllControls().OfType<TheControlType>();
    }

    //(Another way) Returns all controls with a certain ID in all levels
    public static Control ControlWithID(this Control theStartControl, string ID)
    {
        return theStartControl.AllControls().SingleOrDefault(c => c.ID == ID);
    }
}
