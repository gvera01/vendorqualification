﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Company.aspx.cs" Inherits="VQF_Company"
    EnableEventValidation="false" MaintainScrollPositionOnPostback="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UserControl/uclSafetyAttachmentBox.ascx" TagPrefix="Haskell"
    TagName="uclSafetyAttachmentBox" %>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" onbeforeunload="Javascript:windowOnBeforeUnload()">
    <title>Haskell</title>
    <script language="javascript" type="text/javascript">
        function ErrHndl()
        { return true; }

        window.onHaskell;
        window.onerror = ErrHndl;
          
    </script>
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script src="../Script/jquery.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <%--<link rel="stylesheet" type="text/css" href="demo.css" />--%>
<%--    <script type="text/javascript" src="../Script/jquery.1-4-2.min.js"></script>--%>
<%--    <script type="text/javascript" src="../Script/jquery.ie-select-width.js"></script>--%>
    <!--[if lte ie 6]>
<script type="text/javascript" src="jquery.bgiframe.js"></script>
<![endif]-->
    <script type="text/javascript">
        $(function () {
            // Set the width via the plugin.
            //$('select#fixed-select-no-css').ieSelectWidth
            //({
            //    width: 150,
            //    containerClassName: 'select-container',
            //    overlayClassName: 'select-overlay'
            //});

            // Set the width via CSS.
            //ddlboxState, ddlboxState1
            //var el;

            //$("select").each(function () {
            //    el = $(this);
            //    el.bind("blur change", function () {
            //        el = $(this);
            //        el.ieSelectWidth
            //      ({
            //          containerClassName: 'select-container',
            //          overlayClassName: 'select-overlay'
            //      });
            //    });
            //});
              

            //$('select').ieSelectWidth
            //({
            //    containerClassName: 'select-container',
            //    overlayClassName: 'select-overlay'
            //});



            // Borders and padding etc for Internet Explorer 6/7.
            //$('select').ieSelectWidth
            //({
            //    containerClassName: 'select-container',
            //    overlayClassName: 'select-overlay'
            //});
        
        });
    </script>
    <style type="text/css">
        body
        {
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }

        /*select:focus {
                    width: auto !important;
                    position: relative;
                    }*/
    </style>



</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbsubmit" defaultfocus="txtProjIfavailable">
    <asp:ScriptManager ID="scriptMgr" runat="server" >
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="960px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:VQFTopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:SideMenu ID="VQFMenu" runat="server" />
                                    </td>
                                    <td class="contenttd">
                                        <table id="tblContent" runat="server" border="0" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td class="bodytextbold_right">
                                                    Welcome &nbsp;
                                                    <asp:Label ID="lblwelcome" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    Vendor <span style="letter-spacing: 1px">Qualification</span> Form - Company
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass1">
                                                        <tr>
                                                            <td class="left_border formtdbold_rt" colspan="2">
                                                                Your form is available for 90 days from this start date for submitting to us.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td width="15%">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td class="bodytextbold_rightv" width="18%">
                                                                            Registration Date:
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label CssClass="bodytextbold" ID="lblRegistrationDate" runat="server" Width="80px"></asp:Label>
                                                                        </td>
                                                                        <td width="5%">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td class="bodytextbold_rightv" width="18%">
                                                                            Last Update Date:
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label CssClass="bodytextbold" ID="lblLastUpdateDate" runat="server" Width="80px"></asp:Label>
                                                                        </td>
                                                                        <td width="15%">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdlt">
                                                                Project / If Applicable
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox CssClass="txtbox" ID="txtProjIfavailable" runat="server" MaxLength="100"
                                                                    TabIndex="0"></asp:TextBox>
                                                                <%--     <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtProjIfavailable" TargetControlID="txtProjIfavailable"
                                                                            FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                            InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                   <asp:UpdatePanel ID="updPanel" runat="server">
                                                        <ContentTemplate>
                                                            <Haskell:uclSafetyAttachmentBox runat="server" ID="uclAttachmentBox" />  <%--G. vera 02/27/2017:  Added--%>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <Ajax:Accordion ID="accCompany" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
                                                                    HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                                                                    FadeTransitions="true" FramesPerSecond="40" TransitionDuration="40" AutoSize="none"
                                                                    RequireOpenedPane="true" SuppressHeaderPostbacks="true">
                                                                    <Panes>
                                                                        <Ajax:AccordionPane ID="apnFinacial" runat="server">
                                                                            <Header>
                                                                                Business Section</Header>
                                                                            <Content>
                                                                                <table align="center" border="0" cellpadding="3" cellspacing="0" width="100%" class="searchtableclass">
                                                                                    <tr>
                                                                                        <td class="tdheight">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="padding: 0px 10px 0px 10px">
                                                                                            <table align="center" border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="bodytextbold">
                                                                                                        <span class="mandatorystar">*</span>Legal Business Name
                                                                                                    </td>
                                                                                                    <td class="bodytextbold left_border" colspan="2">
                                                                                                        Other Business Name / DBA
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft left_border">
                                                                                                        <asp:TextBox CssClass="txtboxlarge" ID="txtLegalBusinessName" runat="server" MaxLength="100"
                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                        <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtLegalBusinessName" TargetControlID="txtLegalBusinessName"
                                                                                                                    FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
                                                                                                                    InvalidChars="_-#.$">
                                                                                                                </Ajax:FilteredTextBoxExtender>--%>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvLegalBusinessName"
                                                                                                            runat="server" SetFocusOnError="true" EnableClientScript="true" Display="None"
                                                                                                            ControlToValidate="txtLegalBusinessName" ErrorMessage="Please enter legal business name"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft left_border" colspan="2">
                                                                                                        <asp:TextBox CssClass="txtboxlarge" ID="txtOtherBusinessName" runat="server" MaxLength="100"
                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                        <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherBusinessName" TargetControlID="txtOtherBusinessName"
                                                                                                                    FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
                                                                                                                    InvalidChars="_-#.$">
                                                                                                                </Ajax:FilteredTextBoxExtender>--%>
                                                                                                    </td>
                                                                                                </tr><!--G. Vera 12/09/2016: New Questions-->
                                                                                                <tr>
                                                                                                    <td class="bodytextbold" colspan="3">
                                                                                                        <span class="mandatorystar">*</span>Is your company a subsidiary of any other company?
                                                                                                    </td>      
                                                                                                                                                                                                
                                                                                                    
                                                                                                </tr>
                                                                                                
                                                                                                <tr>
                                                                                                    <td colspan="3">
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft" style="width:25%">
                                                                                                                    <input id="rdoSubsidiaryYes" runat="server" type="radio" name="subsidiary" value="True" onclick="Javascript: SubsidiaryRadioChanged(this.value)" />
                                                                                                                    Yes
                                                                                                                    <input id="rdoSubsidiaryNo" runat="server" type="radio" name="subsidiary" value="False" onclick="Javascript: SubsidiaryRadioChanged(this.value)" />
                                                                                                                    No
                                                                                                                </td>

                                                                                                                <td style="display: none;" id="trSubsidiaryCompanyName" class="bodytextleft" colspan="2" runat="server">
                                                                                                                    <span class="mandatorystar">*</span><span><strong> What is the company name? </strong></span><asp:TextBox ID="txtSubsidiaryCompanyName" runat="server" MaxLength="200" CssClass="txtbox"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                                                                                                                   
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold left_border" style="width: 300px" colspan="3">
                                                                                                        <span class="mandatorystar">*</span>Is your company a publicly held company?
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="text-align: left;" class="bodytextleft" colspan="3">
                                                                                                        <asp:RadioButtonList ID="rdoListPubliclyHeld" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                            <asp:ListItem Value="True">Yes</asp:ListItem>
                                                                                                            <asp:ListItem Value="False">No</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                        <asp:CustomValidator ID="custValidBusinessQuestions" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                            EnableClientScript="false" OnServerValidate="cusBusinessSectioQuestions_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <!--G. Vera 12/09/2016: New Questions END-->
                                                                                                
                                                                                                <tr>
                                                                                                    <td class="bodytextbold">
                                                                                                        <span class="mandatorystar">*</span>Country
                                                                                                    </td>
                                                                                                    <td class="bodytextbold" colspan="2">
                                                                                                        <span class="mandatorystar">*</span> <span id="spnTaxLabel" runat="server">Federal Tax
                                                                                                            ID</span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtdrt">
                                                                                                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="ddlboxCountry"
                                                                                                            OnChange="javascript:ShowHide(this.value,'divUSA','divOther')" Enabled="false"
                                                                                                            Visible="false">
                                                                                                            <asp:ListItem Text="USA" Value="USA"></asp:ListItem>
                                                                                                            <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                                                                                        </asp:DropDownList>
                                                                                                        <asp:Label ID="lblCountry" runat="server" Text="USA"></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="formtdrt left_border" colspan="2">
                                                                                                        <asp:Label ID="txtBusinessTax" runat="server" MaxLength="50" CssClass="txtbox" ReadOnly="true"></asp:Label>
                                                                                                        <%-- <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvFederalTax1"
                                                                                                                    runat="server" SetFocusOnError="true" EnableClientScript="true" Display="None"
                                                                                                                    ControlToValidate="txtFederalTax1" ErrorMessage="Please enter federal tax id 1"></asp:RequiredFieldValidator>
                                                                                                                <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvFederalTax2"
                                                                                                                    runat="server" SetFocusOnError="true" EnableClientScript="true" Display="None"
                                                                                                                    ControlToValidate="txtFederalTax2" ErrorMessage="Please enter federal tax id 2"></asp:RequiredFieldValidator>--%>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="tdheight" colspan="3">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold">
                                                                                                        <span class="mandatorystar">*</span>Type of Company
                                                                                                    </td>
                                                                                                    <td class="bodytextbold" colspan="2">
                                                                                                        D U N S Number
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <%--G. Vera 10/02/2012: VMS Stabilization Release 1.3--%>
                                                                                                        <select id="rdoLegalBusinessName" onchange="Javascript:getCheckedRadio(this.value)"
                                                                                                            runat="server" class="companytypeddl">
                                                                                                            <option value="">Select Type of Company </option>
                                                                                                            <option value="Design Consultant">Design Consultant</option>
                                                                                                            <option value="Subcontractor">Subcontractor</option>
                                                                                                            <option value="Supplier">Supplier Only-Equipment & Materials</option>
                                                                                                            <option value="Equip/Onsite Support">Supplier-Equipment & Onsite Support</option>
                                                                                                        </select>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvLegalBusinessName2"
                                                                                                            runat="server" SetFocusOnError="true" EnableClientScript="true" Display="None"
                                                                                                            ControlToValidate="rdoLegalBusinessName" ErrorMessage="Please select a option in type of Company"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextbold" nowrap="nowrap" align="left" colspan="2">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtDNumber" runat="server" MaxLength="11" TabIndex="0"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtDNumber1" runat="server" TargetControlID="txtDNumber"
                                                                                                            FilterType="Custom,Numbers" ValidChars="-">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="tdheight" colspan="3">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3">
                                                                                                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td class="keyheader" colspan="3">
                                                                                                                    <span class="mandatorystar">*</span>Physical Address
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border" width="45%">
                                                                                                                    Address
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" width="30%">
                                                                                                                    Country
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" width="25%">
                                                                                                                    City
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtPhysicalAddr" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvPhysicalAddr"
                                                                                                                        runat="server" SetFocusOnError="true" EnableClientScript="true" Display="None"
                                                                                                                        ControlToValidate="txtPhysicalAddr" ErrorMessage="Please enter the physical address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:DropDownList ID="ddlPACountry" runat="server" CssClass="ddlboxCountry"
                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged"
                                                                                                                        >
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtPACity" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtPACity" TargetControlID="txtPACity"
                                                                                                                                FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                                InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$,">
                                                                                                                            </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvPACity" runat="server"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtPACity"
                                                                                                                        ErrorMessage="Please enter city under physical address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    State<span class="spnState1" id="trOtherslbl" runat="server" style="display: none">Please
                                                                                                                        Specify State </span>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                    ZIP/Postal Code
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    <asp:DropDownList ID="ddlPAState" runat="server" CssClass="ddlboxState" TabIndex="0" >
                                                                                                                    </asp:DropDownList>
                                                                                                                    <span id="trOthers" runat="server" style="display: None">
                                                                                                                        <asp:TextBox ID="txtOtherRegion" runat="server" CssClass="txtbox" MaxLength="50"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <asp:CustomValidator ID="CustState2" runat="server" ValidationGroup="BusinessSection"
                                                                                                                            Display="None" OnServerValidate="custState2_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherRegion" TargetControlID="txtOtherRegion"
                                                                                                                            FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@,0,1,2,3,4,5,6,7,8,9"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </span>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvPAState" runat="server"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="ddlPAState"
                                                                                                                        ErrorMessage="Please select state under physical address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" style="height: 25px;" colspan="2">
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtPAZip" runat="server" MaxLength="10" TabIndex="0"></asp:TextBox>
                                                                                                                    <asp:RegularExpressionValidator ID="revPAZip" runat="server" ControlToValidate="txtPAZip"
                                                                                                                        ValidationGroup="BusinessSection" Display="None" ErrorMessage="Please enter valid ZIP/Postal Code under physical address"
                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtPAZip" runat="server" FilterType="Custom" TargetControlID="txtPAZip"
                                                                                                                        ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvPAZip" runat="server"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtPAZip"
                                                                                                                        ErrorMessage="Please enter ZIP/Postal Code under physical address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    Main Phone Number
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    Main Fax Number
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtMainPhno1" TabIndex="0" runat="server"
                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                    &nbsp;-
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtMainPhno1" runat="server" TargetControlID="txtMainPhno1"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtMainPhno2" TabIndex="0" runat="server"
                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                    <span id="spnMainPhno2" runat="server">-</span>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtMainPhno2" runat="server" TargetControlID="txtMainPhno2"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtMainPhno3" TabIndex="0" runat="server"
                                                                                                                        MaxLength="4"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtMainPhno3" runat="server" TargetControlID="txtMainPhno3"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="cusphone" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                        EnableClientScript="false" OnServerValidate="cusphone_ServerValidate"></asp:CustomValidator>
                                                                                                                    <div id="divMainPhno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:TextBox ID="txtMainFaxno1" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtMainFaxno1" runat="server" TargetControlID="txtMainFaxno1"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:TextBox ID="txtMainFaxno2" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                                    <span id="spnMainFaxno2" runat="server">-</span>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtMainFaxno2" runat="server" TargetControlID="txtMainFaxno2"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:TextBox ID="txtMainFaxno3" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtMainFaxno3" runat="server" TargetControlID="txtMainFaxno3"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="cusFax" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                        EnableClientScript="false" OnServerValidate="cusFax_ServerValidate"></asp:CustomValidator>
                                                                                                                    <div id="divMainFaxno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="keyheader" colspan="3" title="Addresses for all General Correspondence">
                                                                                                                    <span class="mandatorystar">*</span>Mailing Address&nbsp;
                                                                                                                    <asp:CheckBox CssClass="subHeadtext" runat="server" ID="rdoPhyAddr1" TabIndex="0"
                                                                                                                        Text="Check if the Mailing Address is same as Physical Address" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    Address
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    Country
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    City
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtMailAddr" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvMailAddr" runat="server"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtMailAddr"
                                                                                                                        ErrorMessage="Please enter mailing address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:DropDownList  ID="ddlMACountry" runat="server" CssClass="ddlboxCountry"
                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtMACity" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvMACity" runat="server"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtMACity"
                                                                                                                        ErrorMessage="Please enter city under mailing address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    State<span class="spnState1" id="trMaOtherStatelbl" runat="server" style="display: none">Please
                                                                                                                        Specify State </span>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                    ZIP/Postal Code
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    <asp:DropDownList ID="ddlMAState" runat="server" CssClass="ddlboxState" TabIndex="0">
                                                                                                                    </asp:DropDownList>
                                                                                                                    <asp:HiddenField ID="hdnMAState" runat="server" />
                                                                                                                    <span id="trMaOtherState" runat="server" style="display: none">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOtherState" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                                        <asp:CustomValidator ID="CustState3" runat="server" ValidationGroup="BusinessSection"
                                                                                                                            Display="None" OnServerValidate="custState3_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherState" TargetControlID="txtOtherState"
                                                                                                                            FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@,0,1,2,3,4,5,6,7,8,9"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </span>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvMAState" runat="server"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="ddlMAState"
                                                                                                                        ErrorMessage="Please select a state under mailing address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" style="height: 25px;">
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtMAZip" runat="server" MaxLength="5" TabIndex="0"></asp:TextBox>
                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revMAZip2" ControlToValidate="txtMAZip"
                                                                                                                        ValidationGroup="BusinessSection" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code  under mailing address"
                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtMAZip" runat="server" FilterType="Custom" TargetControlID="txtMAZip"
                                                                                                                        ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvMAZip" runat="server"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtMAZip"
                                                                                                                        ErrorMessage="Please enter ZIP/Postal Code under mailing address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="keyheader" colspan="3" title="Address where you receive all payments">
                                                                                                                    <span class="mandatorystar">*</span>Payment Remittance Address&nbsp;
                                                                                                                    <asp:CheckBox CssClass="subHeadtext" runat="server" ID="rdoPhyAddr2" TabIndex="0"
                                                                                                                        Text="Check if the Payment Remittance Address is same as Physical Address" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    Address
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    Country
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    City
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtPayRemittanceAddr" runat="server" MaxLength="100"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvPayRemittanceAddr"
                                                                                                                        runat="server" SetFocusOnError="true" EnableClientScript="true" Display="None"
                                                                                                                        ControlToValidate="txtPayRemittanceAddr" ErrorMessage="Please enter payment remittance address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:DropDownList  ID="ddlRACountry" runat="server" CssClass="ddlboxCountry"
                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtRACity" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvRACity" runat="server"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtRACity"
                                                                                                                        ErrorMessage="Please enter city under remittance address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    State<span class="spnState1" id="TrRAOtherStatelbl" runat="server" style="display: none">Please
                                                                                                                        Specify State </span>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                    ZIP/Postal Code
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    <asp:DropDownList ID="ddlRAState" runat="server" CssClass="ddlboxState" TabIndex="0">
                                                                                                                    </asp:DropDownList>
                                                                                                                    <asp:HiddenField ID="hdnRAState" runat="server" />
                                                                                                                    <span id="TrRAOtherState" runat="server" style="display: none">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtRaOtherState" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                                        <asp:CustomValidator ID="CustState4" runat="server" ValidationGroup="BusinessSection"
                                                                                                                            Display="None" OnServerValidate="custState4_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtRaOtherState" TargetControlID="txtRaOtherState"
                                                                                                                            FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@,0,1,2,3,4,5,6,7,8,9"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </span>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvRAState" runat="server"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="ddlRAState"
                                                                                                                        ErrorMessage="Please select a state under remittance address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" style="height: 25px;">
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtRAZip" runat="server" TabIndex="0" MaxLength="5"></asp:TextBox>
                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revRAZip" ControlToValidate="txtRAZip"
                                                                                                                        ValidationGroup="BusinessSection" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under remittance address"
                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtRAZip" runat="server" FilterType="Custom" TargetControlID="txtRAZip"
                                                                                                                        ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvRAZip" runat="server"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtRAZip"
                                                                                                                        ErrorMessage="Please enter ZIP/Postal Code under remittance address"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="keyheader" colspan="3" title="Do you have additional branches/locations you would like to include?">
                                                                                                                    Additional Location One
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    Address
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    Country
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    City
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtAddAddress1" TabIndex="0" runat="server"
                                                                                                                        MaxLength="100"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:DropDownList  ID="ddlAddCountry" runat="server" CssClass="ddlboxCountry"
                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtAddCity1" runat="server" MaxLength="50"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    State<span class="spnState1" id="trAddOtherState1lbl" runat="server" style="display: none">Please
                                                                                                                        Specify State </span>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                    ZIP/Postal Code
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    <asp:DropDownList ID="ddlAddState" runat="server" TabIndex="0" CssClass="ddlboxState">
                                                                                                                    </asp:DropDownList>
                                                                                                                    <span id="trAddOtherState1" runat="server" style="display: none">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOtherState1" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                                        <asp:CustomValidator ID="CustState5" runat="server" ValidationGroup="BusinessSection"
                                                                                                                            Display="None" OnServerValidate="custState5_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherState1" TargetControlID="txtOtherState1"
                                                                                                                            FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@,0,1,2,3,4,5,6,7,8,9"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" style="height: 25px;">
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddZip1" runat="server" MaxLength="5"
                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revAddZip1" ControlToValidate="txtAddZip1"
                                                                                                                        ValidationGroup="BusinessSection" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under additional location 1"
                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAddZip1" runat="server" FilterType="Custom"
                                                                                                                        TargetControlID="txtAddZip1" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    Phone Number
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    Fax Number
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddPhno1" TabIndex="0" runat="server"
                                                                                                                        MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtAddPhno1"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddPhno2" TabIndex="0" runat="server"
                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                    <span id="spnAddPhno2" runat="server">-</span>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtAddPhno2"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddPhno3" TabIndex="0" runat="server"
                                                                                                                        MaxLength="4"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtAddPhno3"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="cusAddphone" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                        EnableClientScript="false" OnServerValidate="cusAddphone_ServerValidate"></asp:CustomValidator>
                                                                                                                    <div id="divAddPhno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                </td>
                                                                                                                <td colspan="2" class="bodytextleft">
                                                                                                                    <asp:TextBox ID="txtAddFaxno1" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtAddFaxno1"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:TextBox ID="txtAddFaxno2" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                                    <span id="spnAddFaxno2" runat="server">-</span>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtAddFaxno2"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:TextBox ID="txtAddFaxno3" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtAddFaxno3"
                                                                                                                        FilterType="Numbers">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="cusAddfax" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                        EnableClientScript="false" OnServerValidate="cusAddfax_ServerValidate"></asp:CustomValidator>
                                                                                                                    <div id="divAddFaxno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                </td>
                                                                                                                <td colspan="2">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3">
                                                                                                                    <div id="trAddLocations1" runat="server" style="display: none">
                                                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="keyheader" colspan="3">
                                                                                                                                    Additional Location Two
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Address
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Country
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    City
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtAddress" runat="server" MaxLength="100"
                                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:DropDownList  ID="ddlAddCountry1" runat="server" CssClass="ddlboxCountry"
                                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtAdCity" runat="server" MaxLength="50"
                                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    State<span class="spnState1" id="trSpecifyOtherlbl" runat="server" style="display: none">Please
                                                                                                                                        Specify State </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    ZIP/Postal Code
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:DropDownList ID="ddlAddState1" runat="server" CssClass="ddlboxState" TabIndex="0">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                    <span id="trSpecifyOther" runat="server" style="display: none">
                                                                                                                                        <asp:TextBox ID="txtAdOtherState" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="CustState6" runat="server" ValidationGroup="BusinessSection"
                                                                                                                                            Display="None" OnServerValidate="custState6_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtAdOtherState" TargetControlID="txtAdOtherState"
                                                                                                                                            FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@,0,1,2,3,4,5,6,7,8,9"
                                                                                                                                            InvalidChars="_-#.$">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" style="height: 25px;">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdZip" runat="server" MaxLength="5" TabIndex="0"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAdZip" runat="server" FilterType="Custom" TargetControlID="txtAdZip"
                                                                                                                                        ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revAdZip" ControlToValidate="txtAdZip"
                                                                                                                                        ValidationGroup="BusinessSection" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under additional location 2"
                                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Phone Number
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Fax Number
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd1Phno1" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtAdd1Phno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd1Phno2" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd1Phno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtAdd1Phno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd1Phno3" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtAdd1Phno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd1phone" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd1phone_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd1Phno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox ID="txtAdd1Faxno1" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtAdd1Faxno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd1Faxno2" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd1Faxno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtAdd1Faxno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd1Faxno3" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtAdd1Faxno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd1fax" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd1fax_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd1Faxno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3">
                                                                                                                    <span id="trAddLocations2" runat="server" style="display: none">
                                                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td class="keyheader" colspan="3">
                                                                                                                                    Additional Location Three
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Address
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Country
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    City
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtAddAddress2" runat="server" MaxLength="100"
                                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:DropDownList  ID="ddlAddCountry2" runat="server" CssClass="ddlboxCountry"
                                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtAddCity2" runat="server" MaxLength="50"
                                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    State<span class="spnState1" id="trSpecifyOther1lbl" runat="server" style="display: none">Please
                                                                                                                                        Specify State </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    ZIP/Postal Code
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:DropDownList ID="ddlAddState2" runat="server" CssClass="ddlboxState" TabIndex="0">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                    <span id="trSpecifyOther1" runat="server" style="display: none">
                                                                                                                                        <asp:TextBox ID="txtAdOtherState1" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="CustState7" runat="server" ValidationGroup="BusinessSection"
                                                                                                                                            Display="None" OnServerValidate="custState7_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtAdOtherState1" TargetControlID="txtAdOtherState1"
                                                                                                                                            FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@,0,1,2,3,4,5,6,7,8,9"
                                                                                                                                            InvalidChars="_-#.$">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddZip2" runat="server" MaxLength="5"
                                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtZip2" runat="server" FilterType="Custom" TargetControlID="txtAddZip2"
                                                                                                                                        ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revAddZip2" ControlToValidate="txtAddZip2"
                                                                                                                                        ValidationGroup="BusinessSection" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under additional location 3"
                                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Phone Number
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Fax Number
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd2Phno1" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtAdd2Phno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd2Phno2" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd2Phno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="txtAdd2Phno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd2Phno3" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="txtAdd2Phno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd2phone" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd2phone_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd2Phno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox ID="txtAdd2Faxno1" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="txtAdd2Faxno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd2Faxno2" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd2Faxno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="txtAdd2Faxno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd2Faxno3" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="txtAdd2Faxno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd2fax" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd2fax_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd2Faxno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3">
                                                                                                                    <span id="trAddLocations3" runat="server" style="display: none">
                                                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td class="keyheader" colspan="3">
                                                                                                                                    Additional Location Four
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Address
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Country
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    City
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border ">
                                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtAddAddress3" runat="server" TabIndex="0"
                                                                                                                                        MaxLength="100"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:DropDownList  ID="ddlAddCountry3" runat="server" CssClass="ddlboxCountry"
                                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtAddCity3" runat="server" MaxLength="50"
                                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    State<span class="spnState1" id="trSpecifyOther2lbl" runat="server" style="display: none">Please
                                                                                                                                        Specify State </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    ZIP/Postal Code
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:DropDownList ID="ddlAddState3" runat="server" CssClass="ddlboxState" TabIndex="0">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                    <span id="trSpecifyOther2" runat="server" style="display: none">
                                                                                                                                        <asp:TextBox ID="txtAdOtherState2" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="CustState8" runat="server" ValidationGroup="BusinessSection"
                                                                                                                                            Display="None" OnServerValidate="custState8_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtAdOtherState2" TargetControlID="txtAdOtherState2"
                                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                                            InvalidChars="_-#.$">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddZip3" runat="server" MaxLength="5"
                                                                                                                                        TabIndex="0"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAddZip3" runat="server" FilterType="Custom"
                                                                                                                                        TargetControlID="txtAddZip3" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revAddZip3" ControlToValidate="txtAddZip3"
                                                                                                                                        ValidationGroup="BusinessSection" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under additional location 4"
                                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Phone Number
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Fax Number
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd3Phno1" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="txtAdd3Phno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd3Phno2" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd3Phno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" TargetControlID="txtAdd3Phno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd3Phno3" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" TargetControlID="txtAdd3Phno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd3phone" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd3phone_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd3Phno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox ID="txtAdd3Faxno1" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" TargetControlID="txtAdd3Faxno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd3Faxno2" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd3Faxno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" TargetControlID="txtAdd3Faxno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd3Faxno3" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" TargetControlID="txtAdd3Faxno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd3fax" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd3fax_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd3Faxno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3">
                                                                                                                    <span id="trAddLocations4" runat="server" style="display: none">
                                                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td class="keyheader" colspan="3">
                                                                                                                                    Additional Location Five
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Address
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Country
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    City
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtAddAddress4" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="100"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:DropDownList  ID="ddlAddCountry4" runat="server" CssClass="ddlboxCountry"
                                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtAddCity4" runat="server" TabIndex="0"
                                                                                                                                        MaxLength="50"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    State<span class="spnState1" id="trSpecifyOther3lbl" runat="server" style="display: none">Please
                                                                                                                                        Specify State </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    ZIP/Postal Code
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:DropDownList ID="ddlAddState4" runat="server" TabIndex="0" CssClass="ddlboxState">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                    <span id="trSpecifyOther3" runat="server" style="display: none">
                                                                                                                                        <asp:TextBox ID="txtAdOtherState3" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="CustState9" runat="server" ValidationGroup="BusinessSection"
                                                                                                                                            Display="None" OnServerValidate="custState9_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtAdOtherState3" TargetControlID="txtAdOtherState3"
                                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                                            InvalidChars="_-#.$">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddZip4" TabIndex="0" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAddZip4" runat="server" FilterType="Custom"
                                                                                                                                        TargetControlID="txtAddZip4" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revAddZip4" ControlToValidate="txtAddZip4"
                                                                                                                                        ValidationGroup="BusinessSection" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under additional location 5"
                                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Phone Number
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Fax Number
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd4Phno1" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" TargetControlID="txtAdd4Phno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd4Phno2" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd4Phno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" TargetControlID="txtAdd4Phno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd4Phno3" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" TargetControlID="txtAdd4Phno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd4phone" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd4phone_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd4Phno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox ID="txtAdd4Faxno1" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" TargetControlID="txtAdd4Faxno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd4Faxno2" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd4Faxno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" TargetControlID="txtAdd4Faxno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd4Faxno3" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" TargetControlID="txtAdd4Faxno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd4fax" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd4fax_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd4Faxno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3">
                                                                                                                    <span id="trAddLocations5" runat="server" style="display: none">
                                                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td class="keyheader" colspan="3">
                                                                                                                                    Additional Location Six
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Address
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Country
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    City
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtAddAddress5" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="100"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:DropDownList  ID="ddlAddCountry5" runat="server" CssClass="ddlboxCountry"
                                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtAddCity5" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="50"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    State<span class="spnState1" id="trSpecifyOther4lbl" runat="server" style="display: none">Please
                                                                                                                                        Specify State </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    ZIP/Postal Code
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:DropDownList ID="ddlAddState5" runat="server" TabIndex="0" CssClass="ddlboxState">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                    <span id="trSpecifyOther4" runat="server" style="display: none">
                                                                                                                                        <asp:TextBox ID="txtAdOtherState4" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="CustState10" runat="server" ValidationGroup="BusinessSection"
                                                                                                                                            Display="None" OnServerValidate="custState10_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtAdOtherState4" TargetControlID="txtAdOtherState4"
                                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                                            InvalidChars="_-#.$">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddZip5" TabIndex="0" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAddZip5" runat="server" FilterType="Custom"
                                                                                                                                        TargetControlID="txtAddZip5" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revAddZip5" ControlToValidate="txtAddZip5"
                                                                                                                                        runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under additional location 6"
                                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Phone Number
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Fax Number
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft  keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd5Phno1" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" TargetControlID="txtAdd5Phno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd5Phno2" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd5Phno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" TargetControlID="txtAdd5Phno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd5Phno3" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="txtAdd5Phno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd5phone" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd5phone_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd5Phno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox ID="txtAdd5Faxno1" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="txtAdd5Faxno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd5Faxno2" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd5Faxno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" TargetControlID="txtAdd5Faxno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd5Faxno3" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" TargetControlID="txtAdd5Faxno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd5fax" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd5fax_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd5Faxno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3">
                                                                                                                    <span id="trAddLocations6" runat="server" style="display: none">
                                                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td class="keyheader" colspan="3">
                                                                                                                                    Additional Location Seven
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Address
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Country
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    City
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtAddAddress6" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="100"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:DropDownList  ID="ddlAddCountry6" runat="server" CssClass="ddlboxCountry"
                                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtAddCity6" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="50"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    State<span class="spnState1" id="trSpecifyOther5lbl" runat="server" style="display: none">Please
                                                                                                                                        Specify State </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    ZIP/Postal Code
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:DropDownList ID="ddlAddState6" runat="server" CssClass="ddlboxState" TabIndex="0">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                    <span id="trSpecifyOther5" runat="server" style="display: none">
                                                                                                                                        <asp:TextBox ID="txtAdOtherState5" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="CustState11" runat="server" ValidationGroup="BusinessSection"
                                                                                                                                            Display="None" OnServerValidate="custState11_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtAdOtherState5" TargetControlID="txtAdOtherState5"
                                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                                            InvalidChars="_-#.$">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddZip6" TabIndex="0" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAddZip6" runat="server" FilterType="Custom"
                                                                                                                                        TargetControlID="txtAddZip6" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revAddZip6" ControlToValidate="txtAddZip6"
                                                                                                                                        ValidationGroup="BusinessSection" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under additional location 7"
                                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Phone Number
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Fax Number
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd6Phno1" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" TargetControlID="txtAdd6Phno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd6Phno2" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd6Phno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" TargetControlID="txtAdd6Phno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd6Phno3" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" TargetControlID="txtAdd6Phno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd6phone" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd6phone_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd6Phno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox ID="txtAdd6Faxno1" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" TargetControlID="txtAdd6Faxno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd6Faxno2" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd6Faxno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server" TargetControlID="txtAdd6Faxno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd6Faxno3" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender42" runat="server" TargetControlID="txtAdd6Faxno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd6fax" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd6fax_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd6Faxno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3">
                                                                                                                    <span id="trAddLocations7" runat="server" style="display: none">
                                                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td class="keyheader" colspan="3">
                                                                                                                                    Additional Location Eight
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Address
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Country
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    City
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtAddAddress7" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="100"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:DropDownList  ID="ddlAddCountry7" runat="server" CssClass="ddlboxCountry"
                                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtAddCity7" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="50"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    State<span class="spnState1" id="trSpecifyOther6lbl" runat="server" style="display: none">Please
                                                                                                                                        Specify State </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    ZIP/Postal Code
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:DropDownList ID="ddlAddState7" runat="server" TabIndex="0" CssClass="ddlboxState">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                    <span id="trSpecifyOther6" runat="server" style="display: none">
                                                                                                                                        <asp:TextBox ID="txtAdOtherState6" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="CustState12" runat="server" ValidationGroup="BusinessSection"
                                                                                                                                            Display="None" OnServerValidate="custState12_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtAdOtherState6" TargetControlID="txtAdOtherState6"
                                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                                            InvalidChars="_-#.$">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddZip7" TabIndex="0" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAddZip7" runat="server" FilterType="Custom"
                                                                                                                                        TargetControlID="txtAddZip7" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revAddZip7" ControlToValidate="txtAddZip7"
                                                                                                                                        ValidationGroup="BusinessSection" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under additional location 8"
                                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Phone Number
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Fax Number
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd7Phno1" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender43" runat="server" TargetControlID="txtAdd7Phno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd7Phno2" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd7Phno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender44" runat="server" TargetControlID="txtAdd7Phno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd7Phno3" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender45" runat="server" TargetControlID="txtAdd7Phno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd7phone" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd7phone_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd7Phno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox ID="txtAdd7Faxno1" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender46" runat="server" TargetControlID="txtAdd7Faxno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd7Faxno2" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd7Faxno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender47" runat="server" TargetControlID="txtAdd7Faxno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd7Faxno3" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender48" runat="server" TargetControlID="txtAdd7Faxno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd7fax" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd7fax_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd7Faxno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3">
                                                                                                                    <span id="trAddLocations8" runat="server" style="display: none">
                                                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td class="keyheader" colspan="3">
                                                                                                                                    Additional Location Nine
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Address
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Country
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    City
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border ">
                                                                                                                                    <asp:TextBox CssClass="txtboxlarge" TabIndex="0" ID="txtAddAddress8" runat="server"
                                                                                                                                        MaxLength="100"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:DropDownList  ID="ddlAddCountry8" runat="server" CssClass="ddlboxCountry"
                                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtAddCity8" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="50"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    State<span class="spnState1" id="trSpecifyOther7lbl" runat="server" style="display: none">Please
                                                                                                                                        Specify State </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    ZIP/Postal Code
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:DropDownList ID="ddlAddState8" runat="server" CssClass="ddlboxState" TabIndex="0">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                    <span id="trSpecifyOther7" runat="server" style="display: none">
                                                                                                                                        <asp:TextBox ID="txtAdOtherState7" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="CustState13" runat="server" ValidationGroup="BusinessSection"
                                                                                                                                            Display="None" OnServerValidate="custState13_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtAdOtherState7" TargetControlID="txtAdOtherState7"
                                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                                            InvalidChars="_-#.$">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddZip8" TabIndex="0" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAddZip8" runat="server" FilterType="Custom"
                                                                                                                                        TargetControlID="txtAddZip8" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revAddZip8" ControlToValidate="txtAddZip8"
                                                                                                                                        ValidationGroup="BusinessSection" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under additional location 9"
                                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Phone Number
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Fax Number
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd8Phno1" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender49" runat="server" TargetControlID="txtAdd8Phno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd8Phno2" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd8Phno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender50" runat="server" TargetControlID="txtAdd8Phno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd8Phno3" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender51" runat="server" TargetControlID="txtAdd8Phno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd8phone" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd8phone_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd8Phno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox ID="txtAdd8Faxno1" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender52" runat="server" TargetControlID="txtAdd8Faxno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd8Faxno2" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd8Faxno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender53" runat="server" TargetControlID="txtAdd8Faxno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd8Faxno3" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender54" runat="server" TargetControlID="txtAdd8Faxno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd8fax" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd8fax_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd8Faxno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3">
                                                                                                                    <span id="trAddLocations9" runat="server" style="display: none">
                                                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                            <tr>
                                                                                                                                <td class="keyheader" colspan="3">
                                                                                                                                    Additional Location Ten
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Address
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Country
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    City
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtAddAddress9" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="100"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:DropDownList  ID="ddlAddCountry9" runat="server" CssClass="ddlboxCountry"
                                                                                                                                        TabIndex="0" AutoPostBack="true" OnSelectedIndexChanged="ddlPACountry_SelectedIndexChanged">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxmedium" ID="txtAddCity9" runat="server" TabIndex="0"
                                                                                                                                        MaxLength="50"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    State<span class="spnState1" id="trSpecifyOther8lbl" runat="server" style="display: none">Please
                                                                                                                                        Specify State </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                                    ZIP/Postal Code
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:DropDownList ID="ddlAddState9" runat="server" TabIndex="0" CssClass="ddlboxState">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                    <span id="trSpecifyOther8" runat="server" style="display: none">
                                                                                                                                        <asp:TextBox ID="txtAdOtherState8" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                                                        <asp:CustomValidator ID="CustState14" runat="server" ValidationGroup="BusinessSection"
                                                                                                                                            Display="None" OnServerValidate="custState14_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtAdOtherState8" TargetControlID="txtAdOtherState8"
                                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                                            InvalidChars="_-#.$">
                                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                                    </span>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAddZip9" TabIndex="0" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAddZip19" runat="server" FilterType="Custom"
                                                                                                                                        TargetControlID="txtAddZip9" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:RegularExpressionValidator Display="None" ID="revAddZip9" ControlToValidate="txtAddZip9"
                                                                                                                                        ValidationGroup="BusinessSection" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code under additional location 10"
                                                                                                                                        ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    Phone Number
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    Fax Number
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft keyleft_border">
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd9Phno1" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender55" runat="server" TargetControlID="txtAdd9Phno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd9Phno2" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd9Phno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender56" runat="server" TargetControlID="txtAdd9Phno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtAdd9Phno3" TabIndex="0" runat="server"
                                                                                                                                        MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender57" runat="server" TargetControlID="txtAdd9Phno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd9phone" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd9phone_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd9Phno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft">
                                                                                                                                    <asp:TextBox ID="txtAdd9Faxno1" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender58" runat="server" TargetControlID="txtAdd9Faxno1"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd9Faxno2" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                                                    <span id="spnAdd9Faxno2" runat="server">-</span>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender59" runat="server" TargetControlID="txtAdd9Faxno2"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:TextBox ID="txtAdd9Faxno3" runat="server" TabIndex="0" class="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender60" runat="server" TargetControlID="txtAdd9Faxno3"
                                                                                                                                        FilterType="Numbers">
                                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                                    <asp:CustomValidator ID="cusAdd9fax" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusAdd9fax_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <div id="divAdd9Faxno2" runat="server" class="phoneLabel" style="display: none">
                                                                                                                                        Country Code / Area + Phone</div>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                                    &nbsp;
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3" align="center">
                                                                                                                    <asp:LinkButton ID="lnkAddLocations" runat="server" CssClass="hyplinks" TabIndex="0">Click to Add More Additional Locations</asp:LinkButton>
                                                                                                                    <asp:HiddenField ID="hdnCount" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3" style="border-bottom: 1px solid #cccccc;">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="tdheight" colspan="3">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold" width="15%">
                                                                                                        <%--G. Vera - 06/09/2014 Changed label--%>
                                                                                                        <span class="mandatorystar">*</span>In Business Since
                                                                                                    </td>
                                                                                                    <td class="bodytextbold" width="25%">
                                                                                                        <span class="mandatorystar">*</span>Number of Employees
                                                                                                    </td>
                                                                                                    <td class="bodytextbold" width="60%">
                                                                                                        Company Website Address
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtdrt" width="15%">

                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtYearinBusiness" TabIndex="0" runat="server"
                                                                                                            MaxLength="4"></asp:TextBox>

                                                                                                        <asp:CompareValidator ID="cmpValidYear" runat="server"
                                                                                                            ForeColor="Red"
                                                                                                            ControlToValidate="txtYearinBusiness"                                                                                                            
                                                                                                            Type="Integer"
                                                                                                            
                                                                                                            Operator="LessThanEqual"
                                                                                                            ErrorMessage="Year cannot be in the future"
                                                                                                            ValidationGroup="BusinessSection">
                                                                                                        </asp:CompareValidator>
                                                                                                        <%--G. Vera - 06/09/2014 Added minimum characters validation--%>
                                                                                                        <br />
                                                                                                        <asp:RegularExpressionValidator  ControlToValidate="txtYearinBusiness"
                                                                                                            ID="regexYearValidator" ValidationGroup="BusinessSection" SetFocusOnError="true"
                                                                                                            ValidationExpression="^[\s\S]{4,}$" runat="server" ErrorMessage="A 4 digit year is required."></asp:RegularExpressionValidator>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtYearinBusiness" runat="server" TargetControlID="txtYearinBusiness"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <%--G. Vera - 06/09/2014 Changed error message--%>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvYearinBusiness"
                                                                                                            runat="server" SetFocusOnError="true" EnableClientScript="true" Display="None"
                                                                                                            ControlToValidate="txtYearinBusiness" ErrorMessage="Please enter in business since (year)"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="formtdrt" width="25%">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtNoEmp" TabIndex="0" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtNoEmp" runat="server" TargetControlID="txtNoEmp"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="BusinessSection" ID="rfvNoEmp" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtNoEmp"
                                                                                                            ErrorMessage="Please enter number of employees"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="60%" nowrap="nowrap">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtCWebAddress" TabIndex="0" runat="server" MaxLength="100"></asp:TextBox>
                                                                                                        <asp:RegularExpressionValidator ID="revCWebAddress" runat="server" Display="None"
                                                                                                            ValidationGroup="BusinessSection" ErrorMessage="Please enter valid Company website address"
                                                                                                            ControlToValidate="txtCWebAddress" ValidationExpression="(http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"
                                                                                                            SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="tdheight" colspan="3">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold" colspan="3">
                                                                                                        <span id="trdesigncapab" runat="server">
                                                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold left_border" width="40%">Do you have Design-Build Capabilities?
                                                                                                                    </td>
                                                                                                                    <td colspan="4" width="60%">&nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3">
                                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft left_border" width="15%">
                                                                                                                    <span id="trdesigncapabCtl" runat="server">
                                                                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:RadioButtonList ID="rdoCapabilitiesY" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                                                        <asp:ListItem>Yes</asp:ListItem>
                                                                                                                                        <asp:ListItem>No</asp:ListItem>
                                                                                                                                    </asp:RadioButtonList>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <span id="spnIntExt" runat="server" style="display: none">
                                                                                                                        <asp:CheckBoxList ID="rdoIntExt" runat="server" RepeatDirection="Horizontal" TabIndex="0">
                                                                                                                            <asp:ListItem>External</asp:ListItem>
                                                                                                                            <asp:ListItem>Internal</asp:ListItem>
                                                                                                                        </asp:CheckBoxList>
                                                                                                                        <asp:CustomValidator ID="cusIntExt" runat="server" Display="None" ValidationGroup="BusinessSection"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusIntExt_ServerValidate"></asp:CustomValidator>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="tdheight" colspan="3">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3">
                                                                                                        <span id="salestax" runat="server" style="display: ">
                                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="keyheader" colspan="3">
                                                                                                                        State Sales Tax
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft left_border" width="15%">
                                                                                                                        State
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="25%">
                                                                                                                        State Tax Number
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft left_border" width="15%">
                                                                                                                        <asp:DropDownList ID="ddlState" runat="server" TabIndex="0" CssClass="ddlboxState">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="30%">
                                                                                                                        <asp:TextBox ID="txtStateTaxNo" TabIndex="0" runat="server" CssClass="txtbox" MaxLength="25"></asp:TextBox>&nbsp;
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtStateTaxNo" runat="server" TargetControlID="txtStateTaxNo"
                                                                                                                            FilterType="Custom" ValidChars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-. "
                                                                                                                            InvalidChars="`~!#$%^&*()_+=[]{}:;',<>/?@ ">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trSOther" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        <table width="100%">
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft left_border">
                                                                                                                    Please Specify
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft left_border">
                                                                                                                    <asp:TextBox ID="txtSOtherState" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                                                                    <asp:CustomValidator ID="custState" runat="server" ValidationGroup="BusinessSection"
                                                                                                                        Display="None" OnServerValidate="custState_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtSOtherState" TargetControlID="txtSOtherState"
                                                                                                                        FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                        InvalidChars="_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdheight">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </Content>
                                                                        </Ajax:AccordionPane>
                                                                       
                                                                        

                                                                        <Ajax:AccordionPane ID="apnBusiness" runat="server">
                                                                            <Header>
                                                                                Business Type</Header>
                                                                            <Content>
                                                                                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                    <tr>
                                                                                        <td class="bodytextleft" width="20%">
                                                                                            &nbsp; <span class="mandatorystar">*</span>Select Business Type
                                                                                        </td>
                                                                                        <td class="bodytextleft" valign="top" width="80%">
                                                                                            <asp:RadioButtonList ID="rdoBusinessType" runat="server" RepeatDirection="Horizontal"
                                                                                                onclick="javascript:ShowBussiness();" TabIndex="0">
                                                                                                <asp:ListItem>Corporation</asp:ListItem>
                                                                                                <asp:ListItem>LLC / CP</asp:ListItem>
                                                                                                <asp:ListItem>Sole Proprietor</asp:ListItem>
                                                                                                <asp:ListItem>Partnership</asp:ListItem>
                                                                                                <asp:ListItem>Other</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="OtherBusiness" runat="server" style="display: none;">
                                                                                        <td class="bodytextleft left_border" width="20%">
                                                                                            &nbsp;&nbsp;&nbsp;Please Specify
                                                                                        </td>
                                                                                        <td class="formtdrt" width="80%">
                                                                                            <asp:TextBox CssClass="txtboxmultilarge" ID="txtOther1" TextMode="MultiLine" runat="server"
                                                                                                onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                onKeyUp="javascript:textCounter(this,1000,'yes');"></asp:TextBox>
                                                                                            <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOther1" TargetControlID="txtOther1"
                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                            <asp:CustomValidator ID="cstOthers" runat="server" SetFocusOnError="true" Display="None"
                                                                                                ControlToValidate="txtOther1" EnableClientScript="false" ValidationGroup="BusinessType"
                                                                                                OnServerValidate="cusTINorSSN_ServerValidate" ValidateEmptyText="true"></asp:CustomValidator>
                                                                                            <asp:RequiredFieldValidator ID="rfvBusinessType" ValidationGroup="BusinessType" runat="server"
                                                                                                SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="rdoBusinessType"
                                                                                                ErrorMessage="Please select an option in business type"></asp:RequiredFieldValidator>
                                                                                            <%--<Ajax:ValidatorCalloutExtender ID="vceBusinessType" runat="server" TargetControlID="rfvBusinessType">
                                                                            </Ajax:ValidatorCalloutExtender>--%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--G. Vera 02/24/2017: START--%>
                                                                                    <tr>
                                                                                        
                                                                                        <td colspan="3" class="bodytextleft">
                                                                                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="bodytextleft">
                                                                                                            <span class="mandatorystar">*</span><strong>W9</strong><br />
                                                                                                            <strong>Instructions:</strong>  We require all vendors to upload their W9 form using the latest IRS form.  
                                                                                                            If you are unsure what version of the W9 form you have, you can always click the link below to download the latest version.  
                                                                                                            If W9 does not apply to your company, please check the checkbox below.
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:CheckBox Text="Does not apply to our company" runat="server" Font-Bold="true" ID="chkIsW9Apply" />
                                                                                                    </td>

                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:LinkButton ID="lnkW9Download" OnClick="lnkW9Download_Click" runat="server" Text="Click here to download the latest W9 form" />
                                                                                                    </td>

                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:LinkButton ID="lnkUploadW9" OnClick="lnkUploadW9_Click" runat="server" Text="Upload W9" />
                                                                                                    </td>
                                                                                                    
                                                                                                </tr>

                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--G. Vera 02/24/2017: END--%>
                                                                                </table>
                                                                                
                                                                                
                                                                            </Content>
                                                                        </Ajax:AccordionPane>
                                                                        

                                                                        <Ajax:AccordionPane ID="apnLabor" runat="server" CssClass="visiblepane">
                                                                            <Header>
                                                                                Labor Affiliation
                                                                            </Header>
                                                                            <Content>
                                                                                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                    <tr>
                                                                                        <td class="bodytextleft" width="15%">
                                                                                            &nbsp;<span class="mandatorystar">*</span> Union Shop?
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="20%">
                                                                                            <asp:RadioButtonList ID="rblUnionShop" runat="server" RepeatDirection="Horizontal"
                                                                                                TabIndex="0">
                                                                                                <asp:ListItem>Yes</asp:ListItem>
                                                                                                <asp:ListItem>No</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                            <asp:RequiredFieldValidator ID="reqvUnionShop" ValidationGroup="LabourAffiliation"
                                                                                                runat="server" SetFocusOnError="true" EnableClientScript="true" Display="None"
                                                                                                ControlToValidate="rblUnionShop" ErrorMessage="Please select an option in union shop"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="20%">
                                                                                            <span id="tdUnionAffiliation" runat="server" style="display: none">&nbsp; <span class="mandatorystar">
                                                                                                *</span>Union Affiliations: </span>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="40%">
                                                                                            <span id="tdTxtUnionAffiliation" runat="server" style="display: none">
                                                                                                <asp:TextBox ID="txtUnionAffiliation" runat="server" CssClass="txtboxlarge2" TextMode="MultiLine"
                                                                                                    onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                    onKeyUp="javascript:textCounter(this,1000,'yes');"></asp:TextBox>
                                                                                                <asp:CustomValidator ID="cusvUnionAffiliation" runat="server" OnServerValidate="cusvUnionAffiliation_ServerValidate"
                                                                                                    ValidationGroup="LabourAffiliation" Display="None" EnableClientScript="false"></asp:CustomValidator>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </Content>
                                                                        </Ajax:AccordionPane>
                                                                        <Ajax:AccordionPane ID="apnCompanyCert" runat="server">
                                                                            <Header>
                                                                                Company Certifications
                                                                            </Header>
                                                                            <Content>
                                                                                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                    <tr>
                                                                                        <td class="bodytextleft" width="25%" valign="top">
                                                                                            &nbsp;&nbsp; Company Certifications
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="75%">
                                                                                            <asp:CheckBoxList ID="rdoCompanyCertiY" CssClass="list_displayView" runat="server"
                                                                                                RepeatDirection="Horizontal" RepeatColumns="4" TabIndex="0">
                                                                                            </asp:CheckBoxList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" class="bodytextleft">
                                                                                            <span id="trotherCertification" runat="server" style="display: none">
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextleft" width="25%">
                                                                                                            &nbsp;&nbsp; Please Specify the Certification
                                                                                                        </td>
                                                                                                        <td class="bodytextleft" width="75%">
                                                                                                            &nbsp;
                                                                                                            <asp:TextBox CssClass="txtbox" ID="txtCertiOth" runat="server" MaxLength="100"></asp:TextBox>
                                                                                                            <asp:CustomValidator ID="custOtherCertify" runat="server" OnServerValidate="custOtherCertify_ServerValidate"
                                                                                                                ValidationGroup="CompanyCertify" Display="None" EnableClientScript="false"></asp:CustomValidator>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextleft" width="25%" valign="top">
                                                                                            &nbsp;&nbsp;&nbsp; Federal SB
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="75%">
                                                                                            <asp:CheckBoxList ID="chklFederalSB" runat="server" RepeatDirection="Horizontal"
                                                                                                TabIndex="0" onclick="javascript:ShowCertContents()" RepeatColumns="4">
                                                                                            </asp:CheckBoxList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" class="bodytextleft">
                                                                                            <span id="spnFederalSB" runat="server" style="display: none">
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextleft" width="25%">
                                                                                                            &nbsp;&nbsp; Please Specify the Federal SB
                                                                                                        </td>
                                                                                                        <td class="bodytextleft" width="75%">
                                                                                                            &nbsp;
                                                                                                            <asp:TextBox CssClass="txtbox" ID="txtOtherFederalSB" runat="server" MaxLength="100"></asp:TextBox>
                                                                                                            <asp:CustomValidator ID="cusvFederalSB" CssClass="list_displayView" runat="server"
                                                                                                                OnServerValidate="custOtherFederalSB_ServerValidate" ValidationGroup="CompanyCertify"
                                                                                                                Display="None" EnableClientScript="false"></asp:CustomValidator>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <span id="trCertifyingAgency" runat="server" style="display: ">
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td class="hdrbold">
                                                                                                            &nbsp;Certifying Agency
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formtdbold_rt">
                                                                                                            Please attach copies of any applicable certifying documents.
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <span id="trSpecify" runat="server">
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextleft left_border" width="20%" valign="top" nowrap="nowrap">
                                                                                                            &nbsp;&nbsp;Please Specify&nbsp;&nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextleft" width="80%" valign="top">
                                                                                                            <asp:TextBox ID="txtspecify" runat="server" CssClass="txtboxmultilarge" TextMode="MultiLine"
                                                                                                                onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                onKeyUp="javascript:textCounter(this,1000,'yes');"></asp:TextBox>
                                                                                                            <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtSpecifyCertification" TargetControlID="txtspecify"
                                                                                                                    FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
                                                                                                                    InvalidChars="_-#.$">
                                                                                                                </Ajax:FilteredTextBoxExtender>--%>
                                                                                                            <%--<asp:CustomValidator ID="custAgency" runat="server" OnServerValidate="custAgency_ServerValidate"  ValidationGroup="CompanyCertify" Display="None" EnableClientScript="false" ></asp:CustomValidator>--%>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--G. Vera 10/19/2012 - Added--%>
                                                                                    <tr>
                                                                                        <td class="hdrbold" width="100%" colspan="2">
                                                                                            &nbsp;&nbsp;&nbsp;&nbsp;Industry Certifications:
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextleft" width="20%" colspan="2" style="padding-left: 15px">
                                                                                            <asp:DataList ID="dlIndustryCerts" runat="server">
                                                                                                <ItemTemplate>

                                                                                                    <asp:CheckBox ID="chkIndustryCert" runat="server" Text='<%# Bind("Certification") %>'
                                                                                                        onclick="Javascript:OpenHideTexBox(this)" Width="50" />
                                                                                                    &nbsp;&nbsp;&nbsp;
                                                                                                    <asp:TextBox ID="txtIndustryCert" runat="server" MaxLength="28" CssClass="txtbox" />
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtexIndustryCert" runat="server" TargetControlID="txtIndustryCert"
                                                                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0,:," InvalidChars="A-Z,a-z,-,.,;," />
                                                                                                    <asp:HiddenField ID="hdnCertificationID" runat="server" Value='<%# Bind("Pk_CompanyCertificationID") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:DataList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </Content>
                                                                        </Ajax:AccordionPane>
                                                                        <Ajax:AccordionPane ID="apnContact" runat="server">
                                                                            <Header>
                                                                                Key Personnel Contacts</Header>
                                                                            <Content>
                                                                                <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass left_border">
                                                                                    <tr>
                                                                                        <td style="padding: 10px" colspan="3">
                                                                                            <table align="center" border="0" cellpadding="3" cellspacing="0" width="650px">
                                                                                                <tr>
                                                                                                    <td class="keyheader" colspan="3">
                                                                                                        <span class="mandatorystar">*</span>Principal Contact
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3" class="tdheight">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Title
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Phone Number &nbsp;&nbsp;
                                                                                                        <asp:CheckBox ID="chkPUSA" runat="server" Text="Non-US" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border" valign="top">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtPrincipal" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvPrincipal" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtPrincipal"
                                                                                                            ErrorMessage="Please enter name under principal contact"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtPTitle" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtPTitle" TargetControlID="txtPTitle"
                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvPTitle" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtPTitle"
                                                                                                            ErrorMessage="Please enter title under principal contact">
                                                                                                        </asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPPhone1" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPPhone1" runat="server" TargetControlID="txtPPhone1"
                                                                                                            FilterType="Numbers" >
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPPhone2" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                        <span id="spnPPhone2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPPhone2" runat="server" TargetControlID="txtPPhone2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPPhone3" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPPhone3" runat="server" TargetControlID="txtPPhone3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusPhone2" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                            EnableClientScript="false" OnServerValidate="cusphone2_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                        E-mail
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Fax Number
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtPEmail" TabIndex="0" runat="server" MaxLength="100"></asp:TextBox>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvPEmail" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtPEmail"
                                                                                                            ErrorMessage="Please enter e-mail under principal contact"></asp:RequiredFieldValidator>
                                                                                                        <asp:RegularExpressionValidator ID="revPEmail" runat="server" ValidationGroup="KeyPersonal"
                                                                                                            Display="None" ErrorMessage="Please enter vaild e-mail under Principal contact"
                                                                                                            ControlToValidate="txtPEmail" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPFax1" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPFax1" runat="server" TargetControlID="txtPFax1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPFax2" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                        <span id="spnPFax2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPFax2" runat="server" TargetControlID="txtPFax2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPFax3" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPFax3" runat="server" TargetControlID="txtPFax3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusFax2" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                            EnableClientScript="false" OnServerValidate="cusFax2_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3" class="tdheight">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="keyheader" colspan="3">
                                                                                                        &nbsp; <span class="mandatorystar">*</span>Project Management Contact&nbsp;
                                                                                                        <asp:CheckBox CssClass="subHeadtext" runat="server" ID="chkPrincipalContact" TabIndex="0"
                                                                                                            Text="Check if Project Management Contact is same as Principal Contact" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3" class="tdheight">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Title
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Phone Number &nbsp; &nbsp;<asp:CheckBox ID="chkPMUSA" runat="server" Text="Non-US" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border" valign="top">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtProjectMgmt" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvProjectMgmt" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtProjectMgmt"
                                                                                                            ErrorMessage="Please enter name under project management contact"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtPMTitle" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                        <%--   <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtPMTitle" TargetControlID="txtPMTitle"
                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvPMTitle" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtPMTitle"
                                                                                                            ErrorMessage="Please enter title under project management contact"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPMPhone1" TabIndex="0" runat="server"
                                                                                                            MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPMPhone1" runat="server" TargetControlID="txtPMPhone1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPMPhone2" TabIndex="0" runat="server"
                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                        <span id="spnPMPhone2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPMPhone2" runat="server" TargetControlID="txtPMPhone2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPMPhone3" TabIndex="0" runat="server"
                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPMPhone3" runat="server" TargetControlID="txtPMPhone3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusphone3" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                            EnableClientScript="false" OnServerValidate="cusphone3_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                        E-mail
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Fax Number
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtPMEmail" runat="server" TabIndex="0"
                                                                                                            MaxLength="100"></asp:TextBox>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvPMEmail" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtPMEmail"
                                                                                                            ErrorMessage="Please enter e-mail under project management contact"></asp:RequiredFieldValidator>
                                                                                                        <asp:RegularExpressionValidator Display="None" ControlToValidate="txtPMEmail" ID="revPMEmail"
                                                                                                            runat="server" ErrorMessage="Please enter vaild e-mail under project management contact"
                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="KeyPersonal"
                                                                                                            SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPMFax1" runat="server" TabIndex="0" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPMFax1" runat="server" TargetControlID="txtPMFax1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPMFax2" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                        <span id="spnPMFax2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPMFax2" runat="server" TargetControlID="txtPMFax2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPMFax3" runat="server" TabIndex="0" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPMFax3" runat="server" TargetControlID="txtPMFax3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusFax3" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                            EnableClientScript="false" OnServerValidate="cusFax3_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="keyheader" colspan="3">
                                                                                                        &nbsp; <span class="mandatorystar">*</span>Account Receivable Contact
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3" class="tdheight">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Title
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Phone Number &nbsp; &nbsp;
                                                                                                        <asp:CheckBox ID="chkARUSA" runat="server" Text="Non-US" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border" valign="top">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtAccReceivable" TabIndex="0" runat="server"
                                                                                                            MaxLength="50"></asp:TextBox>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvAccReceivable" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtAccReceivable"
                                                                                                            ErrorMessage="Please enter name under account receivable contact"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtARTitle" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtARTitle" TargetControlID="txtARTitle"
                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvARTitle" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtARTitle"
                                                                                                            ErrorMessage="Please enter title under account receivable contact"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtARPhone1" TabIndex="0" runat="server"
                                                                                                            MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtARPhone1" runat="server" TargetControlID="txtARPhone1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtARPhone2" TabIndex="0" runat="server"
                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                        <span id="spnARPhone2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtARPhone2" runat="server" TargetControlID="txtARPhone2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtARPhone3" TabIndex="0" runat="server"
                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtARPhone3" runat="server" TargetControlID="txtARPhone3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusphone4" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                            EnableClientScript="false" OnServerValidate="cusphone4_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                        E-mail
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Fax Number
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtAREMail" TabIndex="0" runat="server"
                                                                                                            MaxLength="100"></asp:TextBox>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvAREMail" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtAREMail"
                                                                                                            ErrorMessage="Please enter e-mail under account receivable contact"></asp:RequiredFieldValidator>
                                                                                                        <asp:RegularExpressionValidator Display="None" ControlToValidate="txtAREMail" ID="revAREMail"
                                                                                                            ValidationGroup="KeyPersonal" runat="server" ErrorMessage="Please enter vaild e-mail under account receivable contact"
                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtARFax1" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtARFax1" runat="server" TargetControlID="txtARFax1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtARFax2" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                        <span id="spnARFax2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtARFax2" runat="server" TargetControlID="txtARFax2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtARFax3" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtARFax3" runat="server" TargetControlID="txtARFax3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusFax4" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                            EnableClientScript="false" OnServerValidate="cusFax4_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="tdheight" colspan="3">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="keyheader" colspan="3">
                                                                                                        &nbsp; <span class="mandatorystar">*</span>Account Payable Contact
                                                                                                        <asp:CheckBox CssClass="subHeadtext" runat="server" ID="chkReceiveContact" TabIndex="0"
                                                                                                            Text="Check if Account Payable Contact is same as Account Receivable contact" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3" class="tdheight">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Title
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Phone Number&nbsp;&nbsp;
                                                                                                        <asp:CheckBox ID="chkAPUSA" runat="server" Text="Non-US" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border" valign="top">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtAccPayable" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvAccPayable" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtAccPayable"
                                                                                                            ErrorMessage="Please enter name under account payable contact"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtAPTitle" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtAPTitle" TargetControlID="txtAPTitle"
                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvAPTitle" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtAPTitle"
                                                                                                            ErrorMessage="Please enter title under account payable contact"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtAPPhone1" runat="server"
                                                                                                            MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAPPhone1" runat="server" TargetControlID="txtAPPhone1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAPPhone2" TabIndex="0" runat="server"
                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                        <span id="spnAPPhone2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAPPhone2" runat="server" TargetControlID="txtAPPhone2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAPPhone3" TabIndex="0" runat="server"
                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAPPhone3" runat="server" TargetControlID="txtAPPhone3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusphone5" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                            EnableClientScript="false" OnServerValidate="cusphone5_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" class="bodytextleft keyleft_border" valign="top">
                                                                                                        E-mail
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Fax Number
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtAPEMail" TabIndex="0" runat="server"
                                                                                                            MaxLength="100"></asp:TextBox>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="rfvAPEMail" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtAPEMail"
                                                                                                            ErrorMessage="Please enter e-mail under account payable contact"></asp:RequiredFieldValidator>
                                                                                                        <asp:RegularExpressionValidator Display="None" ControlToValidate="txtAPEMail" ID="revAPEMail"
                                                                                                            ValidationGroup="KeyPersonal" runat="server" ErrorMessage="Please enter valid e-mail under account payable contact"
                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAPFax1" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAPFax1" runat="server" TargetControlID="txtAPFax1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAPFax2" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                        <span id="spnAPFax2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAPFax2" runat="server" TargetControlID="txtAPFax2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAPFax3" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAPFax3" runat="server" TargetControlID="txtAPFax3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusFax5" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                            EnableClientScript="false" OnServerValidate="cusFax5_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--G. Vera 11/01/2012 - Added--%>
                                                                                                <tr>
                                                                                                    <td class="tdheight" colspan="3">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--G. Vera 11/01/2012 - Added--%>
                                                                                                <tr id="trEquipSuppOnsiteContact" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="keyheader" colspan="3">
                                                                                                                        &nbsp;Equipment Supplier Onsite Support Contact&nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">&nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Phone Number&nbsp; &nbsp;
                                                                                                                        <asp:CheckBox ID="chkEquipSuppUSA" runat="server" Text="Non-US" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border" valign="top">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtEquipSuppOnsiteContactName" TabIndex="0" runat="server"
                                                                                                                            MaxLength="20"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtEquipSuppOnsiteContactTitle" TabIndex="0" runat="server"
                                                                                                                            MaxLength="50"></asp:TextBox>
                                                                                                                        <%--<asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="RequiredFieldValidator2"
                                                                                                                                runat="server" SetFocusOnError="true" EnableClientScript="true" Display="None"
                                                                                                                                ControlToValidate="txtAPTitle" ErrorMessage="Please enter title under account payable contact"></asp:RequiredFieldValidator>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtEquipSuppOnsiteContactPhone1"
                                                                                                                            runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender61" runat="server" TargetControlID="txtEquipSuppOnsiteContactPhone1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtEquipSuppOnsiteContactPhone2" TabIndex="0"
                                                                                                                            runat="server" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnEquipSuppOnsiteContactPhone2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender62" runat="server" TargetControlID="txtEquipSuppOnsiteContactPhone2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtEquipSuppOnsiteContactPhone3" TabIndex="0"
                                                                                                                            runat="server" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender63" runat="server" TargetControlID="txtEquipSuppOnsiteContactPhone3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border" valign="top">
                                                                                                                        E-mail
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Fax Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtEquipSuppOnsiteContactEmail" TabIndex="0"
                                                                                                                            runat="server" MaxLength="100"></asp:TextBox>
                                                                                                                        <%--<asp:RequiredFieldValidator ValidationGroup="KeyPersonal" ID="RequiredFieldValidator3"
                                                                                                                                runat="server" SetFocusOnError="true" EnableClientScript="true" Display="None"
                                                                                                                                ControlToValidate="txtAPEMail" ErrorMessage="Please enter e-mail under account payable contact"></asp:RequiredFieldValidator>--%>
                                                                                                                        <asp:RegularExpressionValidator Display="None" ControlToValidate="txtEquipSuppOnsiteContactEmail"
                                                                                                                            ID="RegularExpressionValidator2" ValidationGroup="KeyPersonal" runat="server"
                                                                                                                            ErrorMessage="Please enter valid e-mail under onsite support contact" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                                                            SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtEquipSuppOnsiteContactFax1" TabIndex="0"
                                                                                                                            runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender64" runat="server" TargetControlID="txtEquipSuppOnsiteContactFax1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtEquipSuppOnsiteContactFax2" TabIndex="0"
                                                                                                                            runat="server" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnEquipSuppOnsiteContactFax2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender65" runat="server" TargetControlID="txtEquipSuppOnsiteContactFax2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtEquipSuppOnsiteContactFax3" TabIndex="0"
                                                                                                                            runat="server" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender66" runat="server" TargetControlID="txtEquipSuppOnsiteContactFax3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <%--<asp:CustomValidator ID="CustomValidator2" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                                EnableClientScript="false" OnServerValidate="cusFax5_ServerValidate"></asp:CustomValidator>--%>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--G. Vera 11/01/2012 - End--%>
                                                                                                <tr>
                                                                                                    <td class="tdheight" colspan="3">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="keyheader" colspan="3">
                                                                                                        &nbsp;&nbsp;Additional Key Contact One
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3" class="tdheight">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Title
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Phone Number&nbsp; &nbsp;
                                                                                                        <asp:CheckBox ID="chkPUSA1" runat="server" Text="Non-US" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border" valign="top">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtName" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtTitle" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                        <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtTitle" TargetControlID="txtTitle"
                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdPhone1" runat="server" TabIndex="0"
                                                                                                            MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdPhone1" runat="server" TargetControlID="txtAdPhone1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdPhone2" runat="server" TabIndex="0"
                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                        <span id="spnAdPhone2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdPhone2" runat="server" TargetControlID="txtAdPhone2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdPhone3" TabIndex="0" runat="server"
                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdPhone3" runat="server" TargetControlID="txtAdPhone3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusphone6" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                            EnableClientScript="false" OnServerValidate="cusphone6_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                        E-mail
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Fax Number
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtemail" runat="server" TabIndex="0" MaxLength="100"></asp:TextBox>
                                                                                                        <asp:RegularExpressionValidator Display="None" ControlToValidate="txtemail" ID="RegularExpressionValidator1"
                                                                                                            ValidationGroup="KeyPersonal" runat="server" ErrorMessage="Please enter valid e-mail under additional contact 1"
                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdFax1" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdFax1" runat="server" TargetControlID="txtAdFax1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdFax2" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                        <span id="spnAdFax2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdFax2" runat="server" TargetControlID="txtAdFax2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdFax3" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdFax3" runat="server" TargetControlID="txtAdFax3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusFax6" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                            EnableClientScript="false" OnServerValidate="cusFax6_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="tdheight" colspan="3">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trListAccount" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="keyheader" colspan="3">
                                                                                                                        &nbsp;&nbsp;Additional Key Contact Two
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Phone Number&nbsp; &nbsp;
                                                                                                                        <asp:CheckBox ID="chkPUSA2" runat="server" Text="Non-US" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border" valign="top">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtName2" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtTitle2" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                                        <%--   <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtTitle2" TargetControlID="txtTitle2"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdMorePhone1" TabIndex="0" runat="server"
                                                                                                                            MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdMorePhone1" runat="server" TargetControlID="txtAdMorePhone1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdMorePhone2" TabIndex="0" runat="server"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnAdMorePhone2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdMorePhone2" runat="server" TargetControlID="txtAdMorePhone2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdMorePhone3" runat="server" TabIndex="0"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdMorePhone3" runat="server" TargetControlID="txtAdMorePhone3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone7" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone7_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        E-mail
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Fax Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtemail2" TabIndex="0" runat="server" MaxLength="100"></asp:TextBox>
                                                                                                                        <asp:RegularExpressionValidator ValidationGroup="KeyPersonal" Display="None" ControlToValidate="txtemail2"
                                                                                                                            ID="rgeTitle2" runat="server" ErrorMessage="Please enter valid e-mail under additional contact 2"
                                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdMoreFax1" runat="server" MaxLength="3"
                                                                                                                            TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdMoreFax1" runat="server" TargetControlID="txtAdMoreFax1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdMoreFax2" runat="server" MaxLength="3"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <span id="spnAdMoreFax2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdMoreFax2" runat="server" TargetControlID="txtAdMoreFax2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdMoreFax3" runat="server" MaxLength="4"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtAdMoreFax3" runat="server" TargetControlID="txtAdMoreFax3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusFax7" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusFax7_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trListAccount1" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="keyheader" colspan="3">
                                                                                                                        &nbsp;&nbsp;Additional Key Contact Three
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Phone Number&nbsp; &nbsp;
                                                                                                                        <asp:CheckBox ID="chkPUSA3" runat="server" Text="Non-US" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtName3" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtTitle3" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                        <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxttitle3" TargetControlID="txtTitle3"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadphone31" runat="server" MaxLength="3"
                                                                                                                            TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadphone31" runat="server" TargetControlID="txtadphone31"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadphone32" runat="server" MaxLength="3"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <span id="spnadphone32" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadphone32" runat="server" TargetControlID="txtadphone32"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadphone33" runat="server" MaxLength="4"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadphone33" runat="server" TargetControlID="txtadphone33"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone8" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone8_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        E-mail
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Fax Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtemail3" runat="server" MaxLength="100"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <asp:RegularExpressionValidator ValidationGroup="KeyPersonal" Display="None" ControlToValidate="txtemail3"
                                                                                                                            ID="revemail3" runat="server" ErrorMessage="Please enter valid e-mail under additional contact 3"
                                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdfax31" runat="server" MaxLength="3"
                                                                                                                            TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtaddfax31" runat="server" TargetControlID="txtAdfax31"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtAdfax32" runat="server" MaxLength="3"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <span id="spnAdfax32" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtaddfax32" runat="server" TargetControlID="txtadfax32"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadfax33" runat="server" MaxLength="4"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtaddfax33" runat="server" TargetControlID="txtadfax33"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusFax8" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusFax8_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trListAccount2" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="keyheader" colspan="3">
                                                                                                                        &nbsp;&nbsp;Additional Key Contact Four
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Phone Number&nbsp; &nbsp;
                                                                                                                        <asp:CheckBox ID="chkPUSA4" runat="server" Text="Non-US" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtName4" runat="server" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtTitle4" runat="server" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                                                                        <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtTitle4" TargetControlID="txtTitle4"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadphone41" TabIndex="0" runat="server"
                                                                                                                            MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadphone41" runat="server" TargetControlID="txtadphone41"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadphone42" runat="server" TabIndex="0"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnadphone42" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadphone42" runat="server" TargetControlID="txtadphone42"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadphone43" runat="server" TabIndex="0"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadphone43" runat="server" TargetControlID="txtadphone43"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone9" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone9_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        E-mail
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Fax Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtemail4" runat="server" MaxLength="100"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <asp:RegularExpressionValidator ValidationGroup="KeyPersonal" Display="None" ControlToValidate="txtemail4"
                                                                                                                            ID="revemail4" runat="server" ErrorMessage="Please enter valid e-mail under additional contact 4"
                                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadfax41" runat="server" MaxLength="3"
                                                                                                                            TabIndex="0"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadfax41" runat="server" TargetControlID="txtadfax41"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadfax42" runat="server" MaxLength="3"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <span id="spnadfax42" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadfax42" runat="server" TargetControlID="txtadfax42"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadfax43" runat="server" MaxLength="4"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadfax43" runat="server" TargetControlID="txtadfax43"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusFax9" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusFax9_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trListAccount3" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="keyheader" colspan="3">
                                                                                                                        &nbsp;&nbsp;Additional Key Contact Five
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Phone Number&nbsp; &nbsp;<asp:CheckBox ID="chkPUSA5" runat="server" Text="Non-US" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtName5" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtTitle5" runat="server" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                                                                        <%--    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtTitle5" TargetControlID="txtTitle5"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadphone51" runat="server" TabIndex="0"
                                                                                                                            MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadphone51" runat="server" TargetControlID="txtadphone51"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadphone52" runat="server" MaxLength="3"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <span id="spnadphone52" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadphone52" runat="server" TargetControlID="txtadphone52"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadphone53" runat="server" TabIndex="0"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadphone53" runat="server" TargetControlID="txtadphone53"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone10" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone10_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        E-mail
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Fax Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtemail5" runat="server" MaxLength="100"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                        <asp:RegularExpressionValidator ValidationGroup="KeyPersonal" Display="None" ControlToValidate="txtemail5"
                                                                                                                            ID="revemail5" runat="server" ErrorMessage="Please enter valid e-mail under additional contact 5"
                                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadFax51" runat="server" TabIndex="0" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax51" runat="server" TargetControlID="txtadFax51"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadFax52" runat="server" TabIndex="0" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnadFax52" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadFax52" runat="server" TargetControlID="txtadFax52"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtadFax53" runat="server" TabIndex="0" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtadFax53" runat="server" TargetControlID="txtadFax53"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusFax10" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusFax10_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trListAccount4" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="keyheader" colspan="3">
                                                                                                                        &nbsp;&nbsp;Additional Key Contact Six
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Phone Number&nbsp;&nbsp;<asp:CheckBox ID="chkPUSA6" runat="server" Text="Non-US" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtName6" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtTitle6" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                        <%--    <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtTitle6" TargetControlID="txtTitle6"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtphone61" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone61" runat="server" TargetControlID="txtphone61"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPhone62" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnPhone62" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone62" runat="server" TargetControlID="txtPhone62"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPhone63" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone63" runat="server" TargetControlID="txtPhone63"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone11" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone11_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        E-mail
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Fax Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtemail6" runat="server" TabIndex="0" MaxLength="100"></asp:TextBox>
                                                                                                                        <asp:RegularExpressionValidator ValidationGroup="KeyPersonal" Display="None" ControlToValidate="txtemail6"
                                                                                                                            ID="revemail6" runat="server" ErrorMessage="Please enter valid e-mail under additional contact 6"
                                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax61" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax61" runat="server" TargetControlID="txtFax61"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax62" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnFax62" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax62" runat="server" TargetControlID="txtFax62"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax63" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax63" runat="server" TargetControlID="txtFax63"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusFax11" runat="server" TabIndex="0" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusFax11_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trListAccount5" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="keyheader" colspan="3">
                                                                                                                        &nbsp;&nbsp;Additional Key Contact Seven
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Phone Number&nbsp; &nbsp;
                                                                                                                        <asp:CheckBox ID="chkPUSA7" runat="server" Text="Non-US" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtName7" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtTitle7" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                                        <%--     <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtTitle7" TargetControlID="txtTitle7"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPhone71" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone71" runat="server" TargetControlID="txtPhone71"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPhone72" runat="server" TabIndex="0" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnPhone72" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone72" runat="server" TargetControlID="txtPhone72"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPhone73" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone73" runat="server" TargetControlID="txtPhone73"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone12" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone12_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        E-mail
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Fax Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtemail7" TabIndex="0" runat="server" MaxLength="100"></asp:TextBox>
                                                                                                                        <asp:RegularExpressionValidator ValidationGroup="KeyPersonal" Display="None" ControlToValidate="txtemail7"
                                                                                                                            ID="revemail7" runat="server" ErrorMessage="Please enter valid e-mail under additional contact 7"
                                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax71" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax71" runat="server" TargetControlID="txtFax71"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax72" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnFax72" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax72" runat="server" TargetControlID="txtFax72"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax73" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax73" runat="server" TargetControlID="txtFax73"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusFax12" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusFax12_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trListAccount6" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="keyheader" colspan="3">
                                                                                                                        &nbsp;&nbsp;Additional Key Contact Eight
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Phone Number&nbsp; &nbsp;
                                                                                                                        <asp:CheckBox ID="chkPUSA8" runat="server" Text="Non-US" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtName8" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtTitle8" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                                        <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtTitle8" TargetControlID="txtTitle8"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPhone81" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone81" runat="server" TargetControlID="txtPhone81"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPhone82" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnPhone82" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone82" runat="server" TargetControlID="txtPhone82"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPhone83" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone83" runat="server" TargetControlID="txtPhone83"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone13" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone13_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        E-mail
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Fax Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtemail8" TabIndex="0" runat="server" MaxLength="100"></asp:TextBox>
                                                                                                                        <asp:RegularExpressionValidator ValidationGroup="KeyPersonal" Display="None" ControlToValidate="txtemail8"
                                                                                                                            ID="revTitle8" runat="server" ErrorMessage="Please enter valid e-mail under additional contact 8"
                                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtfax81" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtfax81" runat="server" TargetControlID="txtfax81"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtfax82" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnfax82" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtfax82" runat="server" TargetControlID="txtfax82"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtfax83" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtfax83" runat="server" TargetControlID="txtfax83"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusFax13" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusFax13_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trListAccount7" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="keyheader" colspan="3">
                                                                                                                        &nbsp;&nbsp;Additional Key Contact Nine
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Phone Number&nbsp; &nbsp;<asp:CheckBox ID="chkPUSA9" runat="server" Text="Non-US" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtName9" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtTitle9" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                                        <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtTitle9" TargetControlID="txtTitle9"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtphone91" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtphone91" runat="server" TargetControlID="txtphone91"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtphone92" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnphone92" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtphone92" runat="server" TargetControlID="txtphone92"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtphone93" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtphone93" runat="server" TargetControlID="txtphone93"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone14" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone14_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        E-mail
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Fax Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtemail9" TabIndex="0" runat="server" MaxLength="100"></asp:TextBox>
                                                                                                                        <asp:RegularExpressionValidator ValidationGroup="KeyPersonal" Display="None" ControlToValidate="txtemail9"
                                                                                                                            ID="revemail9" runat="server" ErrorMessage="Please enter valid e-mail under additional contact 9"
                                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax91" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax91" runat="server" TargetControlID="txtFax91"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax92" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnFax92" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax92" runat="server" TargetControlID="txtFax92"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax93" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax93" runat="server" TargetControlID="txtFax93"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusFax14" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusFax14_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trListAccount8" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="keyheader" colspan="3">
                                                                                                                        &nbsp;&nbsp;Additional Key Contact Ten
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Phone Number&nbsp; &nbsp;
                                                                                                                        <asp:CheckBox ID="chkPUSA10" runat="server" Text="Non-US" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtName10" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtTitle10" TabIndex="0" MaxLength="50" runat="server"></asp:TextBox>
                                                                                                                        <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtTitle10" TargetControlID="txtTitle10"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPhone101" TabIndex="0" runat="server"
                                                                                                                            MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone101" runat="server" TargetControlID="txtPhone101"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPhone102" TabIndex="0" runat="server"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnPhone102" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone102" runat="server" TargetControlID="txtPhone102"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtPhone103" TabIndex="0" runat="server"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPhone103" runat="server" TargetControlID="txtPhone103"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusPhone15" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone15_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        E-mail
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Fax Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2" class="bodytextleft keyleft_border">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge2" ID="txtemail10" TabIndex="0" MaxLength="100"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                        <asp:RegularExpressionValidator ValidationGroup="KeyPersonal" Display="None" ControlToValidate="txtemail10"
                                                                                                                            ID="revemail10" runat="server" ErrorMessage="Please enter valid e-mail under additional contact 10"
                                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax101" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax101" runat="server" TargetControlID="txtFax101"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax102" TabIndex="0" runat="server" MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnFax102" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax102" runat="server" TargetControlID="txtFax102"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtFax103" TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFax103" runat="server" TargetControlID="txtFax103"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusFax15" runat="server" Display="None" ValidationGroup="KeyPersonal"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusFax15_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="tdheight" colspan="3">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="3" align="center">
                                                                                                        <asp:LinkButton ID="lnkAddMoreInfn" runat="server" TabIndex="0" CssClass="hyplinks">Click to Add More Key Contacts</asp:LinkButton>
                                                                                                        <asp:HiddenField ID="hdnMore" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </Content>
                                                                        </Ajax:AccordionPane>
                                                                        <Ajax:AccordionPane ID="apnOfficers" runat="server">
                                                                            <Header>
                                                                                Officers
                                                                            </Header>
                                                                            <Content>
                                                                                <table cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table border="0" cellpadding="5" cellspacing="0" width="100%" class="">
                                                                                                <tr>
                                                                                                    <td class="formtdbold_rt" colspan="3">
                                                                                                        List the corporate officers, partners or proprietors of your firm
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trMessage" style="display: none;">
                                                                                                    <td colspan="3">
                                                                                                        &nbsp;&nbsp;<asp:Label ID="lblMessage" Text="Sum of all 'Percentage of Ownership' should be less than or equals to 100"
                                                                                                            Style="display: none;" CssClass="summaryerrormsg" runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        <span class="mandatorystar">*</span>Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <span class="mandatorystar">*</span>Title
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Percentage of Ownership
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtName1" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="Officers" ID="rfvName1" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtName1"
                                                                                                            ErrorMessage="Please enter officer name "></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtTitle1" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtTitle1" TargetControlID="txtTitle1"
                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                        <asp:RequiredFieldValidator ValidationGroup="Officers" ID="rfvTitle1" runat="server"
                                                                                                            SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtTitle1"
                                                                                                            ErrorMessage="Please enter officer title"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtPercentage1" TabIndex="0" runat="server" MaxLength="6"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtPercentage1" runat="server" TargetControlID="txtPercentage1"
                                                                                                            FilterType="Numbers,Custom" ValidChars=".">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="custPercentage" OnServerValidate="custPercentage_ServerValidate"
                                                                                                            runat="server" ValidationGroup="Officers" SetFocusOnError="true" EnableClientScript="true"
                                                                                                            Display="None"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Title
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Percentage of Ownership
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwn1" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        <%--<asp:RequiredFieldValidator ValidationGroup="Officers" ID="rfvOwn1" runat="server"
                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtOwn1"
                                                                                                        ErrorMessage="Please enter name under officers 2"></asp:RequiredFieldValidator>--%>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnTitle1" TabIndex="0" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                        <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOwnTitle1" TargetControlID="txtOwnTitle1"
                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                        <%--<asp:RequiredFieldValidator ValidationGroup="Officers" ID="rfbtxtOwnTitle1" runat="server"
                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtOwnTitle1"
                                                                                                        ErrorMessage="Please enter title under officers 2"></asp:RequiredFieldValidator>--%>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnPercentage1" runat="server" TabIndex="0"
                                                                                                            MaxLength="6"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtOwnPercentage1" runat="server" TargetControlID="txtOwnPercentage1"
                                                                                                            FilterType="Numbers,Custom" ValidChars=".">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="custPercentage1" OnServerValidate="custPercentage1_ServerValidate"
                                                                                                            runat="server" ValidationGroup="Officers" SetFocusOnError="true" EnableClientScript="true"
                                                                                                            Display="None"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Title
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        Percentage of Ownership
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnName2" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                        <%--<asp:RequiredFieldValidator ValidationGroup="Officers" ID="rfvOwn2" runat="server"
                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtOwnName2"
                                                                                                        ErrorMessage="Please enter name under officers 3"></asp:RequiredFieldValidator>--%>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnTitle2" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                        <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOwnTitle2" TargetControlID="txtOwnTitle2"
                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                        <%--<asp:RequiredFieldValidator ValidationGroup="Officers" ID="revtxtOwnTitle2" runat="server"
                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtOwnTitle2"
                                                                                                        ErrorMessage="Please enter title under officers 3"></asp:RequiredFieldValidator>--%>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnPercent2" runat="server" TabIndex="0" MaxLength="6"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtOwnPercent2" runat="server" TargetControlID="txtOwnPercent2"
                                                                                                            FilterType="Numbers,Custom" ValidChars=".">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="custPercentage2" OnServerValidate="custPercentage2_ServerValidate"
                                                                                                            runat="server" ValidationGroup="Officers" SetFocusOnError="true" EnableClientScript="true"
                                                                                                            Display="None"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trList2" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        <table width="100%" cellspacing="0" cellpadding="5" border="0">
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft" style="padding-left: 15px; width: 35%">
                                                                                                                    Name
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    Title
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    Percentage of Ownership
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft" style="padding-left: 15px; width: 35%">
                                                                                                                    <asp:TextBox CssClass="txtbox" ID="txtOwnName3" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:TextBox CssClass="txtbox" ID="txtOwnTitle3" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                    <%--   <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOwnTitle3" TargetControlID="txtOwnTitle3"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:TextBox CssClass="txtbox" ID="txtOwnPercent3" runat="server" MaxLength="6" TabIndex="0"></asp:TextBox>
                                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtOwnPercent3" runat="server" TargetControlID="txtOwnPercent3"
                                                                                                                        FilterType="Numbers,Custom" ValidChars=".">
                                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                                    <asp:CustomValidator ID="custPercentage3" OnServerValidate="custPercentage3_ServerValidate"
                                                                                                                        runat="server" ValidationGroup="Officers" SetFocusOnError="true" EnableClientScript="true"
                                                                                                                        Display="None"></asp:CustomValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trList3" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        <span>
                                                                                                            <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" style="padding-left: 15px; width: 35%">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Percentage of Ownership
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" style="padding-left: 15px; width: 35%">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnName4" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnTitle4" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                        <%--   <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOwnTitle4" TargetControlID="txtOwnTitle4"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnPercent4" runat="server" MaxLength="6" TabIndex="0"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtOwnPercent4" runat="server" TargetControlID="txtOwnPercent4"
                                                                                                                            FilterType="Numbers,Custom" ValidChars=".">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="custPercentage4" OnServerValidate="custPercentage4_ServerValidate"
                                                                                                                            runat="server" ValidationGroup="Officers" SetFocusOnError="true" EnableClientScript="true"
                                                                                                                            Display="None"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trList4" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        <span>
                                                                                                            <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" style="padding-left: 15px; width: 35%">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Percentage of Ownership
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" style="padding-left: 15px; width: 35%">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnName5" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnTitle5" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                        <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOenTitle5" TargetControlID="txtOwnTitle5"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnPercent5" runat="server" TabIndex="0" MaxLength="6"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtOwnPercent5" runat="server" TargetControlID="txtOwnPercent5"
                                                                                                                            FilterType="Numbers,Custom" ValidChars=".">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="custPercentage5" OnServerValidate="custPercentage5_ServerValidate"
                                                                                                                            runat="server" ValidationGroup="Officers" SetFocusOnError="true" EnableClientScript="true"
                                                                                                                            Display="None"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trList5" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        <span>
                                                                                                            <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border" style="padding-left: 15px; width: 35%">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Percentage of Ownership
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border" style="padding-left: 15px; width: 35%">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnName6" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnTitle6" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                        <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOwnTitle6" TargetControlID="txtOwnTitle6"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnPercent6" runat="server" TabIndex="0" MaxLength="6"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtOwnPercent6" runat="server" TargetControlID="txtOwnPercent6"
                                                                                                                            FilterType="Numbers,Custom" ValidChars=".">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="custPercentage6" OnServerValidate="custPercentage6_ServerValidate"
                                                                                                                            runat="server" ValidationGroup="Officers" SetFocusOnError="true" EnableClientScript="true"
                                                                                                                            Display="None"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trList6" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        <span>
                                                                                                            <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border" style="padding-left: 15px; width: 35%">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Percentage of Ownership
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border" style="padding-left: 15px; width: 35%">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnName7" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnTitle7" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                        <%--  <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOwnTitle7" TargetControlID="txtOwnTitle7"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnPercent7" runat="server" TabIndex="0" MaxLength="6"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtOwnPercent7" runat="server" TargetControlID="txtOwnPercent7"
                                                                                                                            FilterType="Numbers,Custom" ValidChars=".">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="custPercentage7" OnServerValidate="custPercentage7_ServerValidate"
                                                                                                                            runat="server" ValidationGroup="Officers" SetFocusOnError="true" EnableClientScript="true"
                                                                                                                            Display="None"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trList7" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        <span>
                                                                                                            <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border" style="padding-left: 15px; width: 35%">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Percentage of Ownership
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border" style="padding-left: 15px; width: 35%">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnName8" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnTitle8" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                        <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOwnTitle8" TargetControlID="txtOwnTitle8"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnPercent8" runat="server" TabIndex="0" MaxLength="6"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtOwnPercent" runat="server" TargetControlID="txtOwnPercent8"
                                                                                                                            FilterType="Numbers,Custom" ValidChars=".">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="custPercentage8" OnServerValidate="custPercentage8_ServerValidate"
                                                                                                                            runat="server" ValidationGroup="Officers" SetFocusOnError="true" EnableClientScript="true"
                                                                                                                            Display="None"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trList8" runat="server" style="display: none">
                                                                                                    <td colspan="3">
                                                                                                        <span>
                                                                                                            <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border" style="padding-left: 15px; width: 35%">
                                                                                                                        Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Title
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Percentage of Ownership
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft keyleft_border" style="padding-left: 15px; width: 35%">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnName9" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnTitle9" runat="server" TabIndex="0" MaxLength="50"></asp:TextBox>
                                                                                                                        <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOwnTitle9" TargetControlID="txtOwnTitle9"
                                                                                                                        FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                        InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtbox" ID="txtOwnpercent9" runat="server" TabIndex="0" MaxLength="6"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtOwnPercent9" runat="server" TargetControlID="txtOwnpercent9"
                                                                                                                            FilterType="Numbers,Custom" ValidChars=".">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="custPercentage9" OnServerValidate="custPercentage9_ServerValidate"
                                                                                                                            runat="server" ValidationGroup="Officers" SetFocusOnError="true" EnableClientScript="true"
                                                                                                                            Display="None"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="center" colspan="3">
                                                                                                        <asp:LinkButton ID="imgAddMore" runat="server" CssClass="hyplinks" TabIndex="0">Click to Add More Information</asp:LinkButton>
                                                                                                        <asp:HiddenField ID="hdnMoreInfo" runat="server" />
                                                                                                        <asp:CustomValidator ID="cusvPercentage" OnServerValidate="cusvPercentage_ServerValidate"
                                                                                                            runat="server" Display="None" ValidationGroup="Officers"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="4">
                                                                                                        <table width="100%">
                                                                                                            <tr>
                                                                                                                <td class="bodytextleft keyleft_border" width="75%">
                                                                                                                    <span class="mandatorystar">*</span> Have any of the officers ever done business
                                                                                                                    with Haskell through another company?
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" width="25%" colspan="2">
                                                                                                                    <asp:RadioButtonList ID="rdoPContact" runat="server" RepeatDirection="Horizontal"
                                                                                                                        TabIndex="0" onclick="javascript:Showbusiness()">
                                                                                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                                                        <asp:ListItem Value="2">No</asp:ListItem>
                                                                                                                    </asp:RadioButtonList>
                                                                                                                    <asp:RequiredFieldValidator ValidationGroup="Officers" ID="rfvPContact1" runat="server"
                                                                                                                        SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="rdoPContact"
                                                                                                                        ErrorMessage="please select an option related to business with Haskell"></asp:RequiredFieldValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="3" class="keyleft_border">
                                                                                                                    <span style="display: none" runat="server" id="trbussiness">
                                                                                                                        <table width="100%">
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextleft" width="17%">
                                                                                                                                    <span class="mandatorystar">*</span> Please Explain
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleft" width="83%">
                                                                                                                                    <asp:TextBox CssClass="txtboxmultilarge" ID="txtPContactY" runat="server" TabIndex="0"
                                                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onblur="javascript:textCounter(this,1000,'yes');"
                                                                                                                                        onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                                                                                        TextMode="MultiLine"></asp:TextBox>
                                                                                                                                    <%--<Ajax:FilteredTextBoxExtender runat="server" ID="ftxtPContactY" TargetControlID="txtPContactY"
                                                                                                                                    FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                                    InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                                </Ajax:FilteredTextBoxExtender>--%>
                                                                                                                                    <asp:CustomValidator ID="cusPContactY" runat="server" Display="None" ValidationGroup="Officers"
                                                                                                                                        EnableClientScript="false" OnServerValidate="cusPContactY_ServerValidate"></asp:CustomValidator>
                                                                                                                                    <%--<asp:RequiredFieldValidator ValidationGroup="Officers" ID="rfvPContactY" runat="server"
                                                                                                                                    SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtPContactY"
                                                                                                                                    ErrorMessage="Please enter explanation about another company information"></asp:RequiredFieldValidator>--%>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td colspan="2">
                                                                                                                                    <asp:HiddenField ID="hdnVendorStatus" runat="server" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </Content>
                                                                        </Ajax:AccordionPane>
                                                                    </Panes>
                                                                </Ajax:Accordion>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                 </ContentTemplate>
                                                     
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <asp:HiddenField ID="hdnCompanyId" runat="server" />
                                                    <asp:HiddenField ID="hdnCompanyType" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td class="bodytextleft">
                                                                <asp:ImageButton ID="imbSaveandClose" CausesValidation="false" runat="server" ImageUrl="~/Images/save.jpg"
                                                                    ToolTip="Save" TabIndex="0" OnClick="imbSaveandClose_Click" />
                                                            </td>
                                                            <td class="bodytextcenter">
                                                            </td>
                                                            <td class="bodytextright">
                                                                &nbsp;<asp:ImageButton ID="imbsubmit" runat="server" CausesValidation="false" ImageUrl="~/Images/save_nextnew.jpg"
                                                                    ToolTip="Save and Next" TabIndex="0" OnClick="imbsubmit_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdheight">
                                    </td>
                                </tr>
                                <%--G. Vera 12/04/2012 - new requirement message modal box--%>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalNewReqMessage" runat="server" TargetControlID="hdnField1"
                                            PopupControlID="newRequirementDiv" BackgroundCssClass="popupbg">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="hdnField1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="newRequirementDiv" style="display: none;">
                                            <table cellpadding="0" cellspacing="0" border="0" width="550px">
                                                <tr>
                                                    <td class="searchhdrbarbold" colspan="3">
                                                        <asp:Label ID="lblHeader" runat="server" CssClass="searchhdrbarbold" Text="Message"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; padding: 10px; background-color: #ffffff;">
                                                        <asp:Label ID="lblNewRequirementMsg" runat="server" CssClass="messagebig"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color: #ffffff;">
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; padding: 10px; background-color: #ffffff;">
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/NewReqImage.jpg" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color: #ffffff;">
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="popupdivcl" align="center">
                                                        <asp:ImageButton ID="imbNewReqOK" ImageAlign="AbsMiddle" ToolTip="OK" CausesValidation="false"
                                                            ImageUrl="~/Images/close.jpg" runat="server" OnClick="imbNewReqOK_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                            PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" CancelControlID="imbOk">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden" runat="server" />
                                        <asp:HiddenField ID="hdnInvestigationid" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalPopupDiv" class="popupdivsmall" style="display: none">
                                            <table cellpadding="0" cellspacing="0" border="0" width="450px">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="msgTd" colspan="3">
                                                        <asp:Label ID="lblheading" runat="server" CssClass="searchhdrbarbold"></asp:Label>
                                                    </td>
                                                </tr>
                                                <%--  <tr>
                                        <td class="popupdivcl">&nbsp;
                                        </td>
                                    </tr>--%>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        &nbsp;
                                                        <asp:ValidationSummary CssClass="summaryerrormsg" ValidationGroup="BusinessSection"
                                                            DisplayMode="BulletList" runat="server" ID="valSummary" />
                                                        <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                        <asp:Label ID="lblError" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="popupdivcl" align="center">
                                                        <input type="button" value="OK" id="imbOk" title="Ok" class="ModalPopupButton" onclick="$find('modalExtnd').hide(); $(window).scrollTop(0);" />
                                                        <%--<asp:ImageButton ID="imbOk" ImageAlign="AbsMiddle" ToolTip="OK" CausesValidation="false"
                                                                        ImageUrl="~/Images/ok.jpg" runat="server" />--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        //(function (i, s, o, g, r, a, m) {
        //    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        //        (i[r].q = i[r].q || []).push(arguments)
        //    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        //    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        //})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        //ga('create', 'UA-41184898-1', 'haskell.com');
        //ga('send', 'pageview');

        //G. Vera 12/09/2016
        $(document).ready(function () {

            var isyes = $('#ctl19_rdoSubsidiaryYes').is(':checked');
            (isyes) ? $('#trSubsidiaryCompanyName').css('display', '') : $('#trSubsidiaryCompanyName').css('display', 'none');
            $(window).scrollTop(0);
        });

    </script>

    <script language="javascript" type="text/javascript">

        //G. Vera 10/19/2012 - Added
        function OpenHideTexBox(chkBoxCtrl) {
          

            var isChecked = chkBoxCtrl.checked;
            var textBox = document.getElementById(chkBoxCtrl.id.replace('chk', 'txt'));

            if (isChecked) {
              
                textBox.style.display = "";
            }
            else {
                textBox.style.display = "none"
                textBox.value = "";
            }


        }

        function SetCursorFocus(txtZip) {
            var ziptxt;
            ziptxt = document.getElementById(txtZip);
            ziptxt.focus();
        }

        function Close() {
            var x = $find("ModalAddDoc");
            if (x) {
                x.hide();
            }
        }

        function PrintWindow() {
            window.open("../VQFView/CompanyView.aspx?Print=1");
        }
        function Visibletr() {

            var count = document.getElementById("<%=hdnMoreInfo.ClientID %>");
            var count1 = parseInt(count.value) + 1;
            count.value = count1;
            if (count.value == 1)
                document.getElementById("<%=trList2.ClientID %>").style.display = "";
            else if (count.value == 2)
                document.getElementById("<%=trList3.ClientID %>").style.display = "";
            else if (count.value == 3)
                document.getElementById("<%=trList4.ClientID %>").style.display = "";
            else if (count.value == 4)
                document.getElementById("<%=trList5.ClientID %>").style.display = "";
            else if (count.value == 5)
                document.getElementById("<%=trList6.ClientID %>").style.display = "";
            else if (count.value == 6)
                document.getElementById("<%=trList7.ClientID %>").style.display = "";
            else if (count.value == 7) {
                document.getElementById("<%=trList8.ClientID %>").style.display = "";
                document.getElementById("<%=imgAddMore.ClientID %>").style.display = "none";
            }
            count.value = count1;
            return false;
        }
        function VisibleIntExt(id) {
            //trdesigncapabCtl
            var rdoInt = '<%= rdoCapabilitiesY.ClientID %>'; //Sooraj Modified on 26-DEC-2013 to fix the javascript issues 
           // var rdoInt1=document.getElementById('<%=rdoCapabilitiesY.ClientID %>')+"";
           var Items= '<%= rdoCapabilitiesY.Items.Count %>';
           for (var i = 0; i < Items; i++) {
               if (document.getElementById(rdoInt + "_" + i.toString()).checked) {
                    if (i==0) {
                        document.getElementById("<%= spnIntExt.ClientID %>").style.display = "";
                    }
                    else {
                        var container = document.getElementById("<%= spnIntExt.ClientID %>");
                        var single = container.getElementsByTagName("input");
                        var size = single.length;
                        for (i = 0; i < size; i++) {
                            single[i].checked = false;
                        }
                        document.getElementById("<%= spnIntExt.ClientID %>").style.display = "none";
                    }
                }
            }
        }

        function getCheckedRadio(id) {
            var rdolist = document.getElementById('<%=rdoLegalBusinessName.ClientID %>');
            //  var rdolist = document.getElementsByName("ctl06$rdoLegalBusinessName");
            var AccordionName = "<%=accCompany.ClientID %>" + "_AccordionExtender"
            var accordion = $find(AccordionName);

            // for(var i=0; i< rdolist.length; i++) 
            //{
            //if(rdolist[i].checked)
            //{
            //if (document.getElementById('<%=rdoLegalBusinessName.ClientID %>_0').checked)
            //rdolist[i].value == "Subcontractor" ||
            //G. vera - added: specialized contact type
            if (id == "Equip/Onsite Support") {
                document.getElementById("<%= trEquipSuppOnsiteContact.ClientID %>").style.display = "";
            }
            else {
                document.getElementById("<%= trEquipSuppOnsiteContact.ClientID %>").style.display = "none";
            }
            if (id == "Subcontractor" || id == "Equip/Onsite Support") {

                document.getElementById("<%= salestax.ClientID %>").style.display = "";
                document.getElementById("<%=trCertifyingAgency.ClientID %>").style.display = "";
                document.getElementById("<%= trdesigncapab.ClientID %>").style.display = "";
                document.getElementById("<%= trdesigncapabCtl.ClientID %>").style.display = "";

                //                    accordion.get_Pane(2).header.style.visibility ='visible';
                //                    
                //                    accordion.get_Pane(2).content.style.visibility ='visible'; 
                //                    accordion.get_Pane(3).header.style.visibility ='visible';
                //                    accordion.get_Pane(3).content.style.visibility ='visible'; 

                accordion.get_Pane(2).header.style.display = '';

                // accordion.get_Pane(2).content.style.display = '';
                accordion.get_Pane(3).header.style.display = '';
                // accordion.get_Pane(3).content.style.display = '';
                document.getElementById("VQFMenu_liLabor").style.display = "";
                document.getElementById("VQFMenu_liCertification").style.display = "";


            }
            //            else if (document.getElementById('<%=rdoLegalBusinessName.ClientID %>_1').checked) {
            else if (id == "Supplier") {
                document.getElementById("<%=trdesigncapabCtl.ClientID %>").style.display = "none";
                document.getElementById("<%=rdoCapabilitiesY.ClientID %>_0").checked = false;
                document.getElementById("<%=rdoCapabilitiesY.ClientID %>_1").checked = false;
                document.getElementById("<%=rdoIntExt.ClientID %>_0").checked = false;
                document.getElementById("<%=rdoIntExt.ClientID %>_1").checked = false;
                document.getElementById("<%= salestax.ClientID %>").style.display = "";
                document.getElementById("<%=trCertifyingAgency.ClientID %>").style.display = "";
                document.getElementById("<%= trdesigncapab.ClientID %>").style.display = "none";
                document.getElementById("<%= trdesigncapabCtl.ClientID %>").style.display = "none";
                document.getElementById("<%= spnIntExt.ClientID %>").style.display = "none";

                //                    accordion.get_Pane(2).header.style.visibility ='visible';
                //                    accordion.get_Pane(2).content.style.visibility ='visible'; 
                //                    accordion.get_Pane(3).header.style.visibility ='visible';
                //                    accordion.get_Pane(3).content.style.visibility ='visible';

                accordion.get_Pane(2).header.style.display = '';

                // accordion.get_Pane(2).content.style.display = '';
                accordion.get_Pane(3).header.style.display = '';
                // accordion.get_Pane(3).content.style.display = '';
                document.getElementById("VQFMenu_liLabor").style.display = "";
                document.getElementById("VQFMenu_liCertification").style.display = "";

            }
            else {

                document.getElementById("<%= txtStateTaxNo.ClientID %>").value = "";

                document.getElementById("<%= txtSOtherState.ClientID %>").value = "";
                document.getElementById("<%= ddlState.ClientID %>").selectedIndex = 0;
               
               
                document.getElementById("<%= salestax.ClientID %>").style.display = "none";
                document.getElementById("<%= trdesigncapab.ClientID %>").style.display = "";
                document.getElementById("<%= trdesigncapabCtl.ClientID %>").style.display = "";

                document.getElementById("<%=trCertifyingAgency.ClientID %>").style.display = "none";
                accordion.get_Pane(3).header.style.display = "none";
                accordion.get_Pane(3).content.style.display = "none";
                document.getElementById("VQFMenu_liCertification").style.display = "none";

                //                    accordion.get_Pane(2).header.style.visibility ="hidden";
                //                 
                //                    accordion.get_Pane(2).content.style.visibility ="hidden"; 
                //                 
                //                    accordion.get_Pane(3).header.style.visibility ="hidden";
                //                    accordion.get_Pane(3).content.style.visibility ="hidden";

                accordion.get_Pane(2).header.style.display = "none";
                document.getElementById("VQFMenu_liLabor").style.display = "none";
                accordion.get_Pane(2).content.style.display = "none";


                if (id == "Design Consultant") {
                    document.getElementById("<%=trCertifyingAgency.ClientID %>").style.display = "";
                    accordion.get_Pane(3).header.style.display = "";
                    accordion.get_Pane(3).content.style.display = "";
                    document.getElementById("VQFMenu_liCertification").style.display = "";
                }

            }
        }

        function VisibleOtherstate() {
            var ddl = document.getElementById("<%=ddlState.ClientID %>");
            var ddltext = ddl.options[ddl.selectedIndex].text;
            if (ddltext == "Other") {
                document.getElementById("<%=trSOther.ClientID%>").style.display = "";
                var TXT = document.getElementById("<%=txtSOtherState.ClientID%>");
                TXT.focus();
            }
            else
            { document.getElementById("<%=trSOther.ClientID%>").style.display = "none"; }

        }

        function VisibletrList() {

            var count = document.getElementById("<%=hdnMore.ClientID %>");
            var count1 = parseInt(count.value) + 1;
            count.value = count1;

            if (count.value == 1)

                document.getElementById("<%=trListAccount.ClientID %>").style.display = "";
            else if (count.value == 2)
                document.getElementById("<%=trListAccount1.ClientID %>").style.display = "";
            else if (count.value == 3)
                document.getElementById("<%=trListAccount2.ClientID %>").style.display = "";
            else if (count.value == 4)
                document.getElementById("<%=trListAccount3.ClientID %>").style.display = "";
            else if (count.value == 5)
                document.getElementById("<%=trListAccount4.ClientID %>").style.display = "";
            else if (count.value == 6)
                document.getElementById("<%=trListAccount5.ClientID %>").style.display = "";
            else if (count.value == 7)
                document.getElementById("<%=trListAccount6.ClientID %>").style.display = "";
            else if (count.value == 8)
                document.getElementById("<%=trListAccount7.ClientID %>").style.display = "";
            else if (count.value == 9) {
                document.getElementById("<%=trListAccount8.ClientID %>").style.display = "";
                document.getElementById("<%=lnkAddMoreInfn.ClientID %>").style.display = "none";
            }
            count.value = count1;
            return false;

        }
        function VisibletrLocation() {
            //debugger;
            var count = document.getElementById("<%=hdnCount.ClientID %>");
            var count1 = parseInt(count.value) + 1;
            count.value = count1;
            if (count.value == 1)
                document.getElementById("<%=trAddLocations1.ClientID %>").style.display = "";
            else if (count.value == 2)
                document.getElementById("<%=trAddLocations2.ClientID %>").style.display = "";
            else if (count.value == 3)
                document.getElementById("<%=trAddLocations3.ClientID %>").style.display = "";
            else if (count.value == 4)
                document.getElementById("<%=trAddLocations4.ClientID %>").style.display = "";
            else if (count.value == 5)
                document.getElementById("<%=trAddLocations5.ClientID %>").style.display = "";
            if (count.value == 6)
                document.getElementById("<%=trAddLocations6.ClientID %>").style.display = "";
            else if (count.value == 7)
                document.getElementById("<%=trAddLocations7.ClientID %>").style.display = "";
            else if (count.value == 8)
                document.getElementById("<%=trAddLocations8.ClientID %>").style.display = "";
            else if (count.value == 9) {
                document.getElementById("<%=trAddLocations9.ClientID %>").style.display = "";

                document.getElementById("<%=lnkAddLocations.ClientID %>").style.display = "none";
            }
            count.value = count1;
            return false;
        }
        function ShowBussiness() {
            if (document.getElementById("<%=rdoBusinessType.ClientID %>_4").checked) {
                document.getElementById("<%=OtherBusiness.ClientID %>").style.display = "";
            }
            else {
                document.getElementById("<%=OtherBusiness.ClientID %>").style.display = "none";
            }
        }

        function MailingDisplayDetails() {
            if (document.getElementById("<%=rdoPhyAddr1.ClientID%>").checked) {
                document.getElementById("<%=txtMailAddr.ClientID%>").value = document.getElementById("<%=txtPhysicalAddr.ClientID%>").value;
                document.getElementById("<%=txtMACity.ClientID%>").value = document.getElementById("<%=txtPACity.ClientID%>").value;
                document.getElementById("<%=txtMAZip.ClientID%>").value = document.getElementById("<%=txtPAZip.ClientID%>").value;
                document.getElementById("<%=txtMAZip.ClientID%>").style.width = document.getElementById("<%=txtPAZip.ClientID%>").style.width;
                CopyDropDown(document.getElementById("<%=ddlPAState.ClientID%>"), document.getElementById("<%=ddlMAState.ClientID%>"));

                document.getElementById("<%=ddlMAState.ClientID%>").value = document.getElementById("<%=ddlPAState.ClientID%>").value;
                document.getElementById("<%=hdnMAState.ClientID%>").value = document.getElementById("<%=ddlMAState.ClientID%>").value;
                document.getElementById("<%=ddlMACountry.ClientID%>").value = document.getElementById("<%=ddlPACountry.ClientID%>").value;

                var ddl = document.getElementById("<%=ddlPAState.ClientID %>");
                var ddltext = ddl.options[ddl.selectedIndex].text;
                if (ddltext == "Other") {
                    document.getElementById("<%=trMaOtherState.ClientID%>").style.display = "";
                    document.getElementById("<%=trMaOtherStatelbl.ClientID%>").style.display = "";
                    document.getElementById("<%=txtOtherState.ClientID%>").value = document.getElementById("<%=txtOtherRegion.ClientID%>").value;

                    document.getElementById("<%=revMAZip2.ClientID%>").ValidationGroup = "State";
                    var TextBoxFilter = $find("<%=ftxtMAZip.ClientID%>");
                    if (TextBoxFilter != null) {
                        //TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.Numbers + AjaxControlToolkit.FilterTypes.UppercaseLetters + AjaxControlToolkit.FilterTypes.LowercaseLetters + AjaxControlToolkit.FilterTypes.Custom);
                        // TextBoxFilter.set_ValidChars(" ,@");
                    }
                }
                else {
                    document.getElementById("<%=trMaOtherState.ClientID%>").style.display = "none";
                    document.getElementById("<%=trMaOtherStatelbl.ClientID%>").style.display = "none";
                    document.getElementById("<%=txtOtherState.ClientID%>").value = "";
                    document.getElementById("<%=revMAZip2.ClientID%>").ValidationGroup = "BusinessSection";
                    var TextBoxFilter = $find("<%=ftxtMAZip.ClientID%>");

                    if (TextBoxFilter != null) {
                        //  TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.Numbers);
                    }
                }
            }
            else {
                document.getElementById("<%=txtMailAddr.ClientID%>").value = "";
                document.getElementById("<%=txtMACity.ClientID%>").value = "";
                document.getElementById("<%=txtMAZip.ClientID%>").value = "";
                document.getElementById("<%=txtMAZip.ClientID%>").style.width = "40px";
                document.getElementById("<%=ddlMAState.ClientID%>").value = "0";
                document.getElementById("<%=ddlMACountry.ClientID%>").selectedIndex = 0;
                document.getElementById("<%=trMaOtherState.ClientID%>").style.display = "none";
                document.getElementById("<%=trMaOtherStatelbl.ClientID%>").style.display = "none";
                document.getElementById("<%=hdnMAState.ClientID%>").value = "";
            }
        }

        function RemittanceDisplayDetails() {

            if (document.getElementById("<%=rdoPhyAddr2.ClientID%>").checked) {
                document.getElementById("<%=txtPayRemittanceAddr.ClientID%>").value = document.getElementById("<%=txtPhysicalAddr.ClientID%>").value;
                document.getElementById("<%=txtRACity.ClientID%>").value = document.getElementById("<%=txtPACity.ClientID%>").value;
                document.getElementById("<%=txtRAZip.ClientID%>").value = document.getElementById("<%=txtPAZip.ClientID%>").value;
                document.getElementById("<%=txtRAZip.ClientID%>").style.width = document.getElementById("<%=txtPAZip.ClientID%>").style.width;
                CopyDropDown(document.getElementById("<%=ddlPAState.ClientID%>"), document.getElementById("<%=ddlRAState.ClientID%>"));
                document.getElementById("<%=ddlRAState.ClientID%>").value = document.getElementById("<%=ddlPAState.ClientID%>").value;
                document.getElementById("<%=hdnRAState.ClientID%>").value = document.getElementById("<%=ddlRAState.ClientID%>").value;
                document.getElementById("<%=ddlRACountry.ClientID%>").value = document.getElementById("<%=ddlPACountry.ClientID%>").value;

                var ddl = document.getElementById("<%=ddlPAState.ClientID %>");
                var ddltext = ddl.options[ddl.selectedIndex].text;
                if (ddltext == "Other") {
                    document.getElementById("<%=TrRAOtherState.ClientID%>").style.display = "";
                    document.getElementById("<%=TrRAOtherStatelbl.ClientID%>").style.display = "";
                    document.getElementById("<%=txtRaOtherState.ClientID%>").value = document.getElementById("<%=txtOtherRegion.ClientID%>").value;
                    document.getElementById("<%=revRAZip.ClientID%>").ValidationGroup = "State";
                    var TextBoxFilter = $find("<%=ftxtRAZip.ClientID%>");
                    if (TextBoxFilter != null) {
                        // TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.Numbers + AjaxControlToolkit.FilterTypes.UppercaseLetters + AjaxControlToolkit.FilterTypes.LowercaseLetters + AjaxControlToolkit.FilterTypes.Custom);
                        // TextBoxFilter.set_ValidChars(" ,@");
                    }
                }
                else {
                    document.getElementById("<%=TrRAOtherState.ClientID%>").style.display = "none";
                    document.getElementById("<%=TrRAOtherStatelbl.ClientID%>").style.display = "none";

                    document.getElementById("<%=txtRaOtherState.ClientID%>").value = "";
                    document.getElementById("<%=revRAZip.ClientID%>").ValidationGroup = "BusinessSection";

                    var TextBoxFilter = $find("<%=ftxtRAZip.ClientID%>");
                    if (TextBoxFilter != null) {
                        // TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.Numbers );
                    }
                }
            }
            else {
                document.getElementById("<%=txtPayRemittanceAddr.ClientID%>").value = "";
                document.getElementById("<%=txtRACity.ClientID%>").value = "";
                document.getElementById("<%=txtRAZip.ClientID%>").value = "";
                document.getElementById("<%=txtRAZip.ClientID%>").style.width = "40px";
                document.getElementById("<%=ddlRAState.ClientID%>").value = "0";
                document.getElementById("<%=ddlRACountry.ClientID%>").selectedIndex = 0;
                document.getElementById("<%=TrRAOtherState.ClientID%>").style.display = "none";
                document.getElementById("<%=TrRAOtherStatelbl.ClientID%>").style.display = "none";
                document.getElementById("<%=hdnRAState.ClientID%>").value = "";
            }

        }

        function ShowCertContents(text, ctrl, chk) {

            if (text == "Other") {
                if (chk == true) {
                    document.getElementById("<%=trotherCertification.ClientID%>").style.display = "";
                }
                else {
                    document.getElementById("<%=trotherCertification.ClientID%>").style.display = "None";
                    document.getElementById("<%=txtCertiOth.ClientID%>").value = "";
                }
            }
        }
        function ShowFederalContents(text, ctrl, chk) {

            if (text == "Other") {
                if (chk == true) {
                    document.getElementById("<%=spnFederalSB.ClientID%>").style.display = "";
                }
                else {
                    document.getElementById("<%=spnFederalSB.ClientID%>").style.display = "None";
                    document.getElementById("<%=txtCertiOth.ClientID%>").value = "";
                }
            }
        }

        function Showbusiness() {

            if (document.getElementById("<%=rdoPContact.ClientID%>_0").checked) {
                document.getElementById("<%=trbussiness.ClientID%>").style.display = "";
            }
            else if (document.getElementById("<%=rdoPContact.ClientID%>_1").checked) {
                document.getElementById("<%=trbussiness.ClientID%>").style.display = "none";
                document.getElementById("<%=txtPContactY.ClientID%>").value = "";
            }
        }

        function ProjectContactDetails() {
            if (document.getElementById("<%=chkPrincipalContact.ClientID%>").checked) {
                document.getElementById("<%=txtProjectMgmt.ClientID%>").value = document.getElementById("<%=txtPrincipal.ClientID%>").value;
                document.getElementById("<%=txtPMTitle.ClientID%>").value = document.getElementById("<%=txtPTitle.ClientID%>").value;
                document.getElementById("<%=txtPMEmail.ClientID%>").value = document.getElementById("<%=txtPEmail.ClientID%>").value;
                //Sooraj 
                document.getElementById("<%=chkPMUSA.ClientID%>").checked = document.getElementById("<%=chkPUSA.ClientID%>").checked

                document.getElementById("<%=txtPMPhone1.ClientID%>").value = document.getElementById("<%=txtPPhone1.ClientID%>").value;
                document.getElementById("<%=txtPMPhone2.ClientID%>").value = document.getElementById("<%=txtPPhone2.ClientID%>").value;
                document.getElementById("<%=txtPMPhone3.ClientID%>").value = document.getElementById("<%=txtPPhone3.ClientID%>").value;


                document.getElementById("<%=txtPMFax1.ClientID%>").value = document.getElementById("<%=txtPFax1.ClientID%>").value;
                document.getElementById("<%=txtPMFax2.ClientID%>").value = document.getElementById("<%=txtPFax2.ClientID%>").value;
                document.getElementById("<%=txtPMFax3.ClientID%>").value = document.getElementById("<%=txtPFax3.ClientID%>").value;

            }
            else {
                document.getElementById("<%=txtProjectMgmt.ClientID%>").value = "";
                document.getElementById("<%=txtPMTitle.ClientID%>").value = "";
                document.getElementById("<%=txtPMEmail.ClientID%>").value = "";

                document.getElementById("<%=txtPMPhone1.ClientID%>").value = "";
                document.getElementById("<%=txtPMPhone2.ClientID%>").value = "";
                document.getElementById("<%=txtPMPhone3.ClientID%>").value = "";
                document.getElementById("<%=chkPMUSA.ClientID%>").checked = false;

                document.getElementById("<%=txtPMFax1.ClientID%>").value = "";
                document.getElementById("<%=txtPMFax2.ClientID%>").value = "";
                document.getElementById("<%=txtPMFax3.ClientID%>").value = "";
            }
            SwitchPhoneOnCopy(document.getElementById("<%=chkPMUSA.ClientID%>"), document.getElementById("<%=txtPMPhone1.ClientID%>"), document.getElementById("<%=txtPMPhone2.ClientID%>"), document.getElementById("<%=txtPMPhone3.ClientID%>"), document.getElementById("<%=txtPMFax1.ClientID%>"), document.getElementById("<%=txtPMFax2.ClientID%>"), document.getElementById("<%=txtPMFax3.ClientID%>"), document.getElementById("<%=spnPMPhone2.ClientID%>"), document.getElementById("<%=spnPMFax2.ClientID%>"));
        }

        function ReceivableContactDetails() {
            if (document.getElementById("<%=chkReceiveContact.ClientID%>").checked) {
                document.getElementById("<%=txtAccPayable.ClientID%>").value = document.getElementById("<%=txtAccReceivable.ClientID%>").value;
                document.getElementById("<%=txtAPTitle.ClientID%>").value = document.getElementById("<%=txtARTitle.ClientID%>").value;
                document.getElementById("<%=txtAPEMail.ClientID%>").value = document.getElementById("<%=txtAREMail.ClientID%>").value;


                //Sooraj 
                document.getElementById("<%=chkAPUSA.ClientID%>").checked = document.getElementById("<%=chkARUSA.ClientID%>").checked


                document.getElementById("<%=txtAPPhone1.ClientID%>").value = document.getElementById("<%=txtARPhone1.ClientID%>").value;
                document.getElementById("<%=txtAPPhone2.ClientID%>").value = document.getElementById("<%=txtARPhone2.ClientID%>").value;
                document.getElementById("<%=txtAPPhone3.ClientID%>").value = document.getElementById("<%=txtARPhone3.ClientID%>").value;


                document.getElementById("<%=txtAPFax1.ClientID%>").value = document.getElementById("<%=txtARFax1.ClientID%>").value;
                document.getElementById("<%=txtAPFax2.ClientID%>").value = document.getElementById("<%=txtARFax2.ClientID%>").value;
                document.getElementById("<%=txtAPFax3.ClientID%>").value = document.getElementById("<%=txtARFax3.ClientID%>").value;

            }
            else {
                document.getElementById("<%=txtAccPayable.ClientID%>").value = "";
                document.getElementById("<%=txtAPTitle.ClientID%>").value = "";
                document.getElementById("<%=txtAPEMail.ClientID%>").value = "";
                document.getElementById("<%=chkAPUSA.ClientID%>").checked = false;

                document.getElementById("<%=txtAPPhone1.ClientID%>").value = "";
                document.getElementById("<%=txtAPPhone2.ClientID%>").value = "";
                document.getElementById("<%=txtAPPhone3.ClientID%>").value = "";

                document.getElementById("<%=txtAPFax1.ClientID%>").value = "";
                document.getElementById("<%=txtAPFax2.ClientID%>").value = "";
                document.getElementById("<%=txtAPFax3.ClientID%>").value = "";
            }

            SwitchPhoneOnCopy(document.getElementById("<%=chkAPUSA.ClientID%>"), document.getElementById("<%=txtAPPhone1.ClientID%>"), document.getElementById("<%=txtAPPhone2.ClientID%>"), document.getElementById("<%=txtAPPhone3.ClientID%>"), document.getElementById("<%=txtAPFax1.ClientID%>"), document.getElementById("<%=txtAPFax2.ClientID%>"), document.getElementById("<%=txtAPFax3.ClientID%>"), document.getElementById("<%=spnAPPhone2.ClientID%>"), document.getElementById("<%=spnAPFax2.ClientID%>"));
        }
        /*Included by SGS for Task No: 12 */
        function NumberFormat(nStr, ctrl) {
            if (nStr != '') {
                document.getElementById(ctrl).value = (Math.round(nStr * 100) / 100);
            }
        }

        function VisibleLabor() {
            if (document.getElementById("<%= rblUnionShop.ClientID %>_0").checked == true) {
                document.getElementById("<%= tdTxtUnionAffiliation.ClientID %>").style["display"] = "";
                document.getElementById("<%= tdUnionAffiliation.ClientID %>").style["display"] = "";
            }
            else {
                document.getElementById("<%= tdTxtUnionAffiliation.ClientID %>").style["display"] = "None";
                document.getElementById("<%= tdUnionAffiliation.ClientID %>").style["display"] = "None";
                document.getElementById("<%= txtUnionAffiliation.ClientID%>").value = "";

            }
        }
        function validateOfficerPercentage() {
            var percentage = 0;
            if (document.getElementById("<%=txtPercentage1.ClientID%>").value != "")
                percentage = percentage + parseFloat(document.getElementById("<%=txtPercentage1.ClientID%>").value);
            if (document.getElementById("<%=txtOwnPercentage1.ClientID%>").value != "")
                percentage = percentage + parseFloat(document.getElementById("<%=txtOwnPercentage1.ClientID%>").value);
            if (document.getElementById("<%=txtOwnPercent2.ClientID%>").value != "")
                percentage = percentage + parseFloat(document.getElementById("<%=txtOwnPercent2.ClientID%>").value);
            //
            if (document.getElementById("<%=trList2.ClientID%>").style.display == "" && document.getElementById("<%=txtOwnPercent3.ClientID%>").value != "")
                percentage = percentage + parseFloat(document.getElementById("<%=txtOwnPercent3.ClientID%>").value);
            if (document.getElementById("<%=trList3.ClientID%>").style.display == "" && document.getElementById("<%=txtOwnPercent4.ClientID%>").value != "")
                percentage = percentage + parseFloat(document.getElementById("<%=txtOwnPercent4.ClientID%>").value);
            if (document.getElementById("<%=trList4.ClientID%>").style.display == "" && document.getElementById("<%=txtOwnPercent5.ClientID%>").value != "")
                percentage = percentage + parseFloat(document.getElementById("<%=txtOwnPercent5.ClientID%>").value);
            if (document.getElementById("<%=trList5.ClientID%>").style.display == "" && document.getElementById("<%=txtOwnPercent6.ClientID%>").value != "")
                percentage = percentage + parseFloat(document.getElementById("<%=txtOwnPercent6.ClientID%>").value);
            if (document.getElementById("<%=trList6.ClientID%>").style.display == "" && document.getElementById("<%=txtOwnPercent7.ClientID%>").value != "")
                percentage = percentage + parseFloat(document.getElementById("<%=txtOwnPercent7.ClientID%>").value);
            if (document.getElementById("<%=trList7.ClientID%>").style.display == "" && document.getElementById("<%=txtOwnPercent8.ClientID%>").value != "")
                percentage = percentage + parseFloat(document.getElementById("<%=txtOwnPercent8.ClientID%>").value);
            if (document.getElementById("<%=trList8.ClientID%>").style.display == "" && document.getElementById("<%=txtOwnpercent9.ClientID%>").value != "")
                percentage = percentage + parseFloat(document.getElementById("<%=txtOwnpercent9.ClientID%>").value);

            //alert(percentage);
            if (percentage > 100) {
                document.getElementById("<%=lblMessage.ClientID%>").style.display = "";
                document.getElementById("trMessage").style.display = "";
            }
            else {
                document.getElementById("<%=lblMessage.ClientID%>").style.display = "none";
                document.getElementById("trMessage").style.display = "none";
            }
        }


        function visibleControl(dropDownList, textBox, tr, filterExtender, regularExprValidator, otherTextBox) {
            var TextBoxFilter = $find(filterExtender);
            var ddl = document.getElementById(dropDownList);
            var ddltext = ddl.options[ddl.selectedIndex].text;
            var zipText = document.getElementById(textBox);
            var TXT = document.getElementById(otherTextBox);
            var stateOtherlabel = document.getElementById(tr + "lbl");

            zipText.value = "";
            TXT.value = "";
            if (ddltext == "Other") {
                if (stateOtherlabel != null)
                    stateOtherlabel.style.display = "";
                document.getElementById(tr).style.display = "";
                document.getElementById(regularExprValidator).ValidationGroup = "State";
                zipText.maxLength = 10;
                zipText.style.width = "80px";
                TXT.focus();
                if (TextBoxFilter != null) {
                    //   TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.Numbers + AjaxControlToolkit.FilterTypes.UppercaseLetters + AjaxControlToolkit.FilterTypes.LowercaseLetters + AjaxControlToolkit.FilterTypes.Custom); 
                    //   TextBoxFilter.set_ValidChars(" ,@"); //003-Sooraj commented
                }
            }
            else {
                if (stateOtherlabel != null)
                    stateOtherlabel.style.display = "none";
                document.getElementById(tr).style.display = "none";
                document.getElementById(regularExprValidator).ValidationGroup = "BusinessSection";
                if (TextBoxFilter != null) {
                    // TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.Numbers);  //003-Sooraj commented
                }
                zipText.maxLength = 5;
                zipText.style.width = "40px";

                if (ddl != null && ddl.value > 51) {
                    zipText.maxLength = 10;
                    zipText.style.width = "80px";
                }

            }
        }
        //005 -Sooraj
        function ShowHide(val, usaPnl, otherPnl) {

            var spnTax = document.getElementById("<%=spnTaxLabel.ClientID%>");
            if (val == 'Other') {

                spnTax.innerHTML = "Business ID";
            }
            else {

                spnTax.innerHTML = "Federal Tax ID";
            }
        }

        function SwitchPhone(val, phone1, phone2, phone3, fax1, fax2, fax3, spnPhone, spnFax) {

            
            var p1 = document.getElementById(phone1);
            var p2 = document.getElementById(phone2);
            var p3 = document.getElementById(phone3);
            
            var f1 = document.getElementById(fax1);
            var f2 = document.getElementById(fax2);
            var f3 = document.getElementById(fax3);

            var s1 = document.getElementById(spnPhone);
            var s2 = document.getElementById(spnFax);


            if (val.checked) {
                //val.nextSibling.innerHTML = "Non-US";
                p3.style.display = "none";
                p1.maxLength = 5;
                p2.maxLength = 15;
                p2.className = "txtbox";

                s1.style.display = "none";
                s2.style.display = "none";

                f3.style.display = "none";
                f1.maxLength = 5;
                f2.maxLength = 15;
                f2.className = "txtbox";
                
                // G. Vera 05/12/2014:  Changed the use of this JS function
                // since it is not working properly on IE9 and greater
                //.attributes.removeNamedItem("onkeyup");
                p1.onkeyup = "";
                p2.onkeyup = "";
                f1.onkeyup = "";
                f2.onkeyup = "";

            }
            else {
                //val.nextSibling.innerHTML = "Non-US";
                p3.style.display = "";
                p2.className = "txtboxsmall";
                p1.maxLength = 3;
                p2.maxLength = 3;

                s1.style.display = "";
                s2.style.display = "";


                f3.style.display = "";

                f2.className = "txtboxsmall";
                f1.maxLength = 3;
                f2.maxLength = 3;
                
                p1.setAttribute("onkeyup", "SetFocusOnPhone(" + phone1 + "," + phone2 + ")");
                p2.setAttribute("onkeyup", "SetFocusOnPhone(" + phone2 + "," + phone3 + ")");
                f1.setAttribute("onkeyup", "SetFocusOnPhone(" + fax1 + "," + fax2 + ")");
                f2.setAttribute("onkeyup", "SetFocusOnPhone(" + fax2 + "," + fax3 + ")");
            }

            p3.value = "";
            f3.value = "";
            p2.value = "";
            f2.value = "";
            p1.value = "";
            f1.value = "";

        }

        function SwitchPhoneOnCopy(val, p1, p2, p3, f1, f2, f3, s1, s2) {

            
            
            if (val.checked) {
                //val.nextSibling.innerHTML = "Non-US";
                p3.style.display = "none";
                p1.maxLength = 5;
                p2.maxLength = 15;
                p2.className = "txtbox";

                s1.style.display = "none";
                s2.style.display = "none";

                f3.style.display = "none";
                f1.maxLength = 5;
                f2.maxLength = 15;
                f2.className = "txtbox";

                p1.onkeyup = "";
                p2.onkeyup = "";
                f1.onkeyup = "";
                f2.onkeyup = "";

            }
            else {
                //val.nextSibling.innerHTML = "Non-US";
                p3.style.display = "";
                p2.className = "txtboxsmall";
                p1.maxLength = 3;
                p2.maxLength = 3;

                s1.style.display = "";
                s2.style.display = "";


                f3.style.display = "";

                f2.className = "txtboxsmall";
                f1.maxLength = 3;
                f2.maxLength = 3;

                p1.setAttribute("onkeyup", "SetFocusOnPhone(" + p1 + "," + p2 + ")");
                p2.setAttribute("onkeyup", "SetFocusOnPhone(" + p2 + "," + p3 + ")");
                f1.setAttribute("onkeyup", "SetFocusOnPhone(" + f1 + "," + f2 + ")");
                f2.setAttribute("onkeyup", "SetFocusOnPhone(" + f2 + "," + f3 + ")");
            }

        }

        //G. Vera 12/09/2016: Added function
        function SubsidiaryRadioChanged(isyes, toggleElement) {
            $('#hdnSubsidiarySelction').val(isyes);
            var companyNameID = '#' + "<%= this.txtSubsidiaryCompanyName.ClientID %>";
            var companyNameTR = '#' + "<%= this.trSubsidiaryCompanyName.ClientID %>";
            (isyes == "True") ? $(companyNameTR).css('display', '') : $(companyNameTR).css('display', 'none');
            (isyes == "True") ? "nothing" : $(companyNameID).val('');
        }


        //G. Vera 02/27/2017: Added
        function ShowHideElements(isChecked, csvElements) {
            
            var elements = csvElements.toString().split(",");
            for (var i = 0; i < elements.length; i++) {
                var id = elements[i];
                var objElement = document.getElementById(id);
                if (isChecked) {
                    $('#' + id).hide();
                }
                else {
                    $('#' + id).show();
                }
            }
        }
        
        
        
    
    </script>

</body>
</html>
