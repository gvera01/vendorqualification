﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;
using VMSDAL;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 Sooraj Sudhakaran.T 09/28/2013: VMS Modification - Added code to hide section that do not apply to design consultant
 * 003 Sooraj Sudhakaran.T 09/24/2013: VMS Modification - Added code to handle International Zip/Postal Code
 * 004 Sooraj Sudhakaran.T 09/24/2013: VMS Modification - Added code to handle project reference type for design consultant
 * 005 Sooraj Sudhakaran.T 10/10/2013: VMS Modification - Added code to handle International Country ,State or province
 * 006 Sooraj Sudhakaran.T 10/10/2013: VMS Modification - Added code to handle International Phone Number
 * 007 Sooraj Sudhakaran.T 21/10/2013: VMS Modification - Added code to handle International Language Question
 * 008 G. Vera 06/302014: Validate section status upon Save click to get the correct status
 * 009 G. Vera 07/09/2014: Last modified date change upon save
 * 010 G. Vera 07/08/2015: Validation on General COntractor, Owner, or Prime COnsultant change
 ****************************************************************************************************************/
#endregion

public partial class VQF_References : CommonPage
{

    #region Declaration

    Common objCommon = new Common();
    clsReferences objReferences = new clsReferences();
    clsCompany objCompany = new clsCompany();
    long returnRefId = 0;
    long existingRefId = 0;
    bool IsBasedInUS;
    string completionDateOne;
    public DataSet VendorDetails = new DataSet();
    bool? projectsCompleted = null;
    DateTime curdt = Convert.ToDateTime(DateTime.Now.ToShortDateString());
    DataSet Attch = new DataSet();



    public DAL.VQFReference Moddata;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["VendorId"])) || Convert.ToString(Session["VendorId"]) == "0")
        {
            Response.Redirect("~/Common/Home.aspx?timeout=1");  //001
        }
        //001
        else if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Common/Home.aspx?timeout=1");
        }
        else
        {
            CallScripts();
            if (!Page.IsPostBack)
            {

                //001
                Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

                ShowHideControls();
                VendorDetails = objCommon.GetVendorDetails(Convert.ToInt64(Session["VendorId"].ToString()));

                //Show or hide design consultant view based on Type of company - //002-sooraj
                bool IsDesignConsultant = (VendorDetails.Tables[0].Rows[0]["TypeOfCompany"].ConvertToString() == TypeOfCompany.DesignConsultant);
                SwitchDesignConsultantView(IsDesignConsultant);
                IsBasedInUS = VendorDetails.Tables[0].Rows[0]["IsBasedInUS"].ConvertToString().ConvertToBool(); //007
                ShowHideReferenceSupplierQuestion(IsBasedInUS);
                lblwelcome.Text = VendorDetails.Tables[0].Rows[0]["Company"].ToString();
                int refCount = objReferences.GetReferenceStatusCount(Convert.ToInt32(Session["VendorId"]));
                hdnCount.Value = "0";
                hdnaddProjectcount.Value = "0";
                hdnAdditionalReferencesCount.Value = "3";
                FillYear(ddlYear1);
                FillYear(ddlYear2);
                FillYear(ddlYear3);
                FillYear(ddlYear4);
                FillYear(ddlYear5);
                FillYear(ddlYear6);
                FillYear(ddlYear7);
                FillYear(ddlYear8);
                FillYear(ddlYear9);
                FillYear(ddlYear10);
                FillMonth(ddlMonth1);
                FillMonth(ddlMonth2);
                FillMonth(ddlMonth3);
                FillMonth(ddlMonth4);
                FillMonth(ddlMonth5);
                FillMonth(ddlMonth6);
                FillMonth(ddlMonth7);
                FillMonth(ddlMonth8);
                FillMonth(ddlMonth9);
                FillMonth(ddlMonth10);
                FillProjectReferenceType(IsDesignConsultant);

                FillCountry();//005-Sooraj
                FillState();//005-Sooraj
                LoadEditDatas(true);

            }
            else
            {
                //G. Vera 05/21/2014 - Added
                LoadToolTipOnPostBack();
            }
        }

        imgAddMore.Attributes.Add("onClick", "return Visibletr();");
        lnladdProject.Attributes.Add("onClick", "return VisibleProject();");
        ControlVisiblity();
        VQFMenu.GetVendorStatus();
        hdnVendorStatus.Value = Convert.ToString(VQFMenu.VendorStatus);
        if (VQFMenu.VendorStatus == 1 || VQFMenu.VendorStatus == 2)
        {
            imbSaveandClose.Visible = false;
            imbsubmit.Visible = false;
            apnProject.Enabled = false;
            apnSupplier.Enabled = false;
            ImageButton imgAttach = (ImageButton)VQFMenu.FindControl("imbAttch");
            imgAttach.Visible = true;
        }

      
    }


    /// <summary>
    /// Need to handle the tool tip onm post back as it is being removed
    /// </summary>
    private void LoadToolTipOnPostBack()
    {
        //Supplier 
        objCommon.AddToolTipToItems(ddlCountryFour);
        objCommon.AddToolTipToItems(ddlCountryFive);
        objCommon.AddToolTipToItems(ddlCountrySix);
        objCommon.AddToolTipToItems(ddlCountrySeven);
        objCommon.AddToolTipToItems(ddlCountryEight);
        objCommon.AddToolTipToItems(ddlCountryNine);
        objCommon.AddToolTipToItems(ddlCountryTen);
        objCommon.AddToolTipToItems(ddlCountryEleven);
        objCommon.AddToolTipToItems(ddlCountryTwelve);
        objCommon.AddToolTipToItems(ddlCountryThirteen);

        //Reference
        objCommon.AddToolTipToItems(ddlCountryOne);
        objCommon.AddToolTipToItems(ddlCountryTwo);
        objCommon.AddToolTipToItems(ddlCountryThree);
        objCommon.AddToolTipToItems(ddlCountryReferencesFour);
        objCommon.AddToolTipToItems(ddlCountryReferencesFive);
        objCommon.AddToolTipToItems(ddlCountryReferencesSix);
        objCommon.AddToolTipToItems(ddlCountryReferencesSeven);
        objCommon.AddToolTipToItems(ddlCountryReferencesEight);
        objCommon.AddToolTipToItems(ddlCountryReferencesNine);
        objCommon.AddToolTipToItems(ddlCountryReferencesTen);


        objCommon.AddToolTipToItems(ddlStateFour);
        objCommon.AddToolTipToItems(ddlStateFive);
        objCommon.AddToolTipToItems(ddlStateSix);
        objCommon.AddToolTipToItems(ddlStateSeven);
        objCommon.AddToolTipToItems(ddlStateEight);
        objCommon.AddToolTipToItems(ddlStateNine);
        objCommon.AddToolTipToItems(ddlStateTen);
        objCommon.AddToolTipToItems(ddlStateEleven);
        objCommon.AddToolTipToItems(ddlStateTwelve);
        objCommon.AddToolTipToItems(ddlStateThirteen);


        objCommon.AddToolTipToItems(ddlStateOne);
        objCommon.AddToolTipToItems(ddlStateTwo);
        objCommon.AddToolTipToItems(ddlStateThree);
        objCommon.AddToolTipToItems(ddlStateReferencesFour);
        objCommon.AddToolTipToItems(ddlStateReferencesFive);
        objCommon.AddToolTipToItems(ddlStateReferencesSix);
        objCommon.AddToolTipToItems(ddlStateReferencesSeven);
        objCommon.AddToolTipToItems(ddlStateReferencesEight);
        objCommon.AddToolTipToItems(ddlStateReferencesNine);
        objCommon.AddToolTipToItems(ddlStateReferencesTen);
        
    }

    #region Button Events

    //005-Sooraj
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        var ddl = sender as DropDownList;
        BindAssociateState(ddl, true, true);
        ddl.Focus();
    }
    //005-Sooraj
    protected void ddlRefCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        var ddl = sender as DropDownList;
        BindAssociateStateRefernce(ddl, true, true);
        ddl.Focus();
    }
    // save and close details - Its update the exisiting datas
    protected void imbSaveandClose_Click(object sender, ImageClickEventArgs e)
    {
        int refCount = objReferences.GetRefCount(Convert.ToInt32(Session["VendorId"]));
        projectsCompleted = rdoHaskellProjectsCompleted.SelectedValue.Equals("True") ? true : (rdoHaskellProjectsCompleted.SelectedValue.Equals("False") ? false : new Nullable<bool>());

        if (refCount == 0)
        {
            int status = Convert.ToByte(this.IsPageValid());        //008
            returnRefId = InsertProjectReferencesForSaveAndClose(status);
            InsertReferencesProject(returnRefId);
            InsertAddionalProjects(returnRefId);
            InsertSupplierReferences(returnRefId);
            LblMsg.Text = "&nbsp;<li>References details saved successfully</li>";
        }
        else
        {
            existingRefId = objReferences.GetVQFReferences(Convert.ToInt32(Session["VendorId"]))[0].PK_ReferencesID;
            string mainPhone = null, secondPhone = null, thirdPhone = null;
            if ((txtMainPhnoOne.Text + txtMainPhnoTwo.Text + txtMainPhnoThree.Text).Length>0)
            {
                mainPhone = objCommon.Phoneformat(txtMainPhnoOne.Text.Trim(), txtMainPhnoTwo.Text.Trim(), txtMainPhnoThree.Text.Trim(), txtMainPhnoThree);//006
            }
            if ((txtSecondPhnoOne.Text + txtSecondPhnoTwo.Text + txtSecondPhnoThree.Text).Length > 0)
            {
                secondPhone = objCommon.Phoneformat(txtSecondPhnoOne.Text.Trim(), txtSecondPhnoTwo.Text.Trim(), txtSecondPhnoThree.Text.Trim(), txtSecondPhnoThree);//006
            }
            if ((txtThirdPhnoOne.Text + txtThirdPhnoTwo.Text + txtThirdPhnoThree.Text).Length > 0)
            {
                thirdPhone = objCommon.Phoneformat(txtThirdPhnoOne.Text.Trim(), txtThirdPhnoTwo.Text.Trim(), txtThirdPhnoThree.Text.Trim(), txtThirdPhnoThree);//006
            }
            bool updRefResults = objReferences.UpdateReferences(projectsCompleted, existingRefId, Convert.ToByte(this.IsPageValid()));      //008
            objReferences.DeleteAllRelationsofRefId(existingRefId);
            InsertReferencesProject(existingRefId);
            InsertAddionalProjects(existingRefId);
            InsertSupplierReferences(existingRefId);
            LblErrorMsg.Text = "";
            LblMsg.Text = "&nbsp;<li>References details has been saved successfully</li>";
        }
        VQFMenu.GetCompleteStatus();
        VQFMenu.GetIncompleteStatus();
        VQFMenu.LastUpdatedDate(Convert.ToInt32(Session["VendorId"]), DateTime.Now);        //009
        Lblheading.Text = "Result";
        LblErrorMsg.Text = "";

        modalExtnd.Show();

        LoadEditDatas(true);
        ControlVisiblity();
    }

    /// <summary>
    /// 008 - added to be called by the save and close button event
    /// </summary>
    /// <returns></returns>
    private bool IsPageValid()
    {
        Page.Validate("ProjectReferences");
        if (Page.IsValid)
        {
            Page.Validate("Supplier");
            return Page.IsValid;
        }
        else
        {
            return false;
        }

    }

    protected void imgComp1_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void imbsubmit_Click(object sender, ImageClickEventArgs e)
    {
        Page.Validate("ProjectReferences");
        if (hdnprjreferenceid.Value != "")
            returnRefId = Convert.ToInt64(hdnprjreferenceid.Value);

        if (Page.IsValid)
        {
            projectsCompleted = rdoHaskellProjectsCompleted.SelectedValue.Equals("True") ? true : (rdoHaskellProjectsCompleted.SelectedValue.Equals("False") ? false : new Nullable<bool>());

            Page.Validate("Supplier");
            if (Page.IsValid)
            {
                int refCount = objReferences.GetRefCount(Convert.ToInt32(Session["VendorId"]));
                if (refCount == 0)
                {
                    int status = 1;
                    returnRefId = InsertProjectReferences(status);
                    InsertReferencesProject(returnRefId);
                    InsertAddionalProjects(returnRefId);
                    InsertSupplierReferences(returnRefId);
                    VQFMenu.LastUpdatedDate(Convert.ToInt32(Session["VendorId"]), DateTime.Now);        //009
                    Response.Redirect("InsurancesBonding.aspx");
                }
                else
                {
                    existingRefId = objReferences.GetVQFReferences(Convert.ToInt32(Session["VendorId"]))[0].PK_ReferencesID;
                    bool updRefResults = objReferences.UpdateReferences(projectsCompleted, existingRefId, 2);
                    objReferences.DeleteAllRelationsofRefId(existingRefId);
                    InsertReferencesProject(existingRefId);
                    InsertAddionalProjects(existingRefId);
                    InsertSupplierReferences(existingRefId);
                    VQFMenu.LastUpdatedDate(Convert.ToInt32(Session["VendorId"]), DateTime.Now);        //009
                    Response.Redirect("InsurancesBonding.aspx");
                }
            }
            else
            {
                Lblheading.Text = "Supplier - Validation";
                accReferences.SelectedIndex = 1;
                LblMsg.Text = "";
                if (!string.IsNullOrEmpty(Errormsg()))
                {
                    string[] txt = Errormsg().Split('|');
                    if (txt.Length == 2)
                    {
                        LblErrorMsg.Text = txt[0];
                        if (Convert.ToInt32(txt[1]) > 20)
                        {
                            modalPopupDiv.Style["height"] = "550px";
                            modalPopupDiv.ScrollBars = ScrollBars.Both;
                        }
                        else
                        {
                            modalPopupDiv.Style["height"] = "auto";
                            modalPopupDiv.ScrollBars = ScrollBars.None;
                        }
                    }
                    modalExtnd.Show();
                }
            }
        }
        else
        {
            Lblheading.Text = "Project References - Validation";
            accReferences.SelectedIndex = 0;
            LblMsg.Text = "";
            if (!string.IsNullOrEmpty(Errormsg()))
            {
                string[] txt = Errormsg().Split('|');
                if (txt.Length == 2)
                {
                    LblErrorMsg.Text = txt[0];
                    if (Convert.ToInt32(txt[1]) > 20)
                    {
                        modalPopupDiv.Style["height"] = "550px";
                        modalPopupDiv.ScrollBars = ScrollBars.Both;
                    }
                    else
                    {
                        modalPopupDiv.Style["height"] = "auto";
                        modalPopupDiv.ScrollBars = ScrollBars.None;
                    }
                }
            }
            modalExtnd.Show();
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// 006-Phone number changes
    /// </summary>
    /// <param name="phone1"></param>
    /// <param name="phone2"></param>
    /// <param name="phone3"></param>
    /// <param name="spnHyphen"></param>
    /// <param name="IsClear"></param>
    private void SwitchPhoneNumber(DropDownList countryDDL, TextBox phone1, TextBox phone2, TextBox phone3, HtmlControl spnHyphen, HtmlControl divLabel, bool IsClear)
    {
        objCommon.SwitchPhoneNumber(countryDDL, phone1, phone2, phone3, spnHyphen, divLabel, IsClear);
    }



    /// <summary>
    /// 006 -Sooraj
    /// </summary>
    /// <param name="countryDDL"></param>
    /// <param name="IsBind"></param>
    /// <param name="IsClear"></param>
    private void BindAssociateState(DropDownList countryDDL, bool IsBind = true, bool IsClear = false)
    {

        switch (countryDDL.ID)
        {
            case "ddlCountryFour":
                if (IsBind)
                    BindState(ddlCountryFour, ddlStateFour);
                SwitchPhoneNumber(countryDDL, txtSuplMainPhoneOne, txtSuplMainPhoneTwo, txtSuplMainPhoneThree, spnSuplMainPhoneTwo,divSuplMainPhoneTwo, IsClear);
                SetValidCharsAndLength(ddlStateFour, txtSuplZipOne, ftxtSuplZipOne, regvsuplzipone, trOther4, trOther4lbl);
                break;
            case "ddlCountryFive":
                if (IsBind)
                    BindState(ddlCountryFive, ddlStateFive);
                SwitchPhoneNumber(countryDDL, txtSuplSecondPhoneOne, txtSuplSecondPhoneTwo, txtSuplSecondPhoneThree, spnSuplSecondPhoneTwo, divSuplSecondPhoneTwo, IsClear);
                SetValidCharsAndLength(ddlStateFive, txtSuplZipTwo, ftxtSuplZipTwo, regsuplziptwo, trOther5, trOther5lbl);
                break;
            case "ddlCountrySix":
                if (IsBind)
                    BindState(ddlCountrySix, ddlStateSix);
                SwitchPhoneNumber(countryDDL, txtSuplThirdPhoneOne, txtSuplThirdPhoneTwo, txtSuplThirdPhoneThree, spnSuplThirdPhoneTwo, divSuplThirdPhoneTwo, IsClear);
                SetValidCharsAndLength(ddlStateSix, txtSuplZipThree, ftxtSuplZipThree, regvSuplZipThree, trOther6, trOther6lbl);
                break;
            case "ddlCountrySeven":
                if (IsBind)
                    BindState(ddlCountrySeven, ddlStateSeven);
                SwitchPhoneNumber(countryDDL, txtSuplFourthPhoneOne, txtSuplFourthPhoneTwo, txtSuplFourthPhoneThree, spnSuplFourthPhoneTwo, divSuplFourthPhoneTwo, IsClear);
                SetValidCharsAndLength(ddlStateSeven, txtSuplZipFour, ftxtSuplZipFour, regvSuplZipFour, trOther7, trOther7lbl);
                break;
            case "ddlCountryEight":
                if (IsBind)
                    BindState(ddlCountryEight, ddlStateEight);
                SwitchPhoneNumber(countryDDL, txtSuplFivePhoneOne, txtSuplFivePhoneTwo, txtSuplFivePhoneThree, spnSuplFivePhoneTwo,divSuplFivePhoneTwo, IsClear);
                SetValidCharsAndLength(ddlStateEight, txtSuplZipFive, ftxtSuplZipFive, regvSuplZipFive, trOther8, trOther8lbl);
                break;
            case "ddlCountryNine":
                if (IsBind)
                    BindState(ddlCountryNine, ddlStateNine);
                SwitchPhoneNumber(countryDDL, txtSuplSixPhoneOne, txtSuplSixPhoneTwo, txtSuplSixPhoneThree, spnSuplSixPhoneTwo, divSuplSixPhoneTwo, IsClear);
                SetValidCharsAndLength(ddlStateNine, txtSuplZipSix, ftxtSuplZipSix, regvSuplZipSix, trOther9, trOther9lbl);
                break;
            case "ddlCountryTen":
                if (IsBind)
                    BindState(ddlCountryTen, ddlStateTen);
                SwitchPhoneNumber(countryDDL, txtSuplSevenPhoneOne, txtSuplSevenPhoneTwo, txtSuplSevenPhoneThree, spnSuplSevenPhoneTwo, divSuplSevenPhoneTwo, IsClear);
                SetValidCharsAndLength(ddlStateTen, txtSuplZipSeven, ftxtSuplZipSeven, regvSuplZipSeven, trOther10, trOther10lbl);
                break;
            case "ddlCountryEleven":
                if (IsBind)
                    BindState(ddlCountryEleven, ddlStateEleven);
                SwitchPhoneNumber(countryDDL, txtSuplEightPhoneOne, txtSuplEightPhoneTwo, txtSuplEightPhoneThree, spnSuplEightPhoneTwo, divSuplEightPhoneTwo, IsClear);
                SetValidCharsAndLength(ddlStateEleven, txtSuplZipEight, ftxtSuplZipEight, regvSuplZipEight, trOther11, trOther11lbl);
                break;
            case "ddlCountryTwelve":
                if (IsBind)
                    BindState(ddlCountryTwelve, ddlStateTwelve);
                SwitchPhoneNumber(countryDDL, txtSuplNinePhoneOne, txtSuplNinePhoneTwo, txtSuplNinePhoneThree, spnSuplNinePhoneTwo, divSuplNinePhoneTwo, IsClear);
                SetValidCharsAndLength(ddlStateTwelve, txtSuplZipNine, ftxtSuplZipNine, regvSuplZipNine, trOther12, trOther12lbl);
                break;
            case "ddlCountryThirteen":
                if (IsBind)
                    BindState(ddlCountryThirteen, ddlStateThirteen);
                SwitchPhoneNumber(countryDDL, txtSuplTenPhoneOne, txtSuplTenPhoneTwo, txtSuplTenPhoneThree, spnSuplTenPhoneTwo, divSuplTenPhoneTwo, IsClear);
                SetValidCharsAndLength(ddlStateThirteen, txtSuplZipTen, ftxtSuplZipTen, regvSuplZipTen, trOther13, trOther13lbl);
                break;


        }
    }



    /// <summary>
    /// 006 -Sooraj
    /// </summary>
    /// <param name="countryDDL"></param>
    /// <param name="IsBind"></param>
    /// <param name="IsClear"></param>
    private void BindAssociateStateRefernce(DropDownList countryDDL, bool IsBind = true, bool IsClear = false)
    {

        switch (countryDDL.ID)
        {
            case "ddlCountryOne":
                if (IsBind)
                    BindState(ddlCountryOne, ddlStateOne);
                SwitchPhoneNumber(countryDDL, txtMainPhnoOne, txtMainPhnoTwo, txtMainPhnoThree, spnMainPhnoTwo,divMainPhnoTwo, IsClear); //To Be change span 
                break;
            case "ddlCountryTwo":
                if (IsBind)
                    BindState(ddlCountryTwo, ddlStateTwo);
                SwitchPhoneNumber(countryDDL, txtSecondPhnoOne, txtSecondPhnoTwo, txtSecondPhnoThree, spnSecondPhnoTwo, divSecondPhnoTwo, IsClear);
                break;
            case "ddlCountryThree":
                if (IsBind)
                    BindState(ddlCountryThree, ddlStateThree);
                SwitchPhoneNumber(countryDDL, txtThirdPhnoOne, txtThirdPhnoTwo, txtThirdPhnoThree, spnThirdPhnoTwo, divThirdPhnoTwo, IsClear);
                break;
            case "ddlCountryReferencesFour":
                if (IsBind)
                    BindState(ddlCountryReferencesFour, ddlStateReferencesFour);
                SwitchPhoneNumber(countryDDL, txtFourthPhnoOne, txtFourthPhnoTwo, txtFourthPhnoThree, spnFourthPhnoTwo, divFourthPhnoTwo, IsClear);
                break;
            case "ddlCountryReferencesFive":
                if (IsBind)
                    BindState(ddlCountryReferencesFive, ddlStateReferencesFive);
                SwitchPhoneNumber(countryDDL, txtFifthPhnoOne, txtFifthPhnoTwo, txtFifthPhnoThree, spnFifthPhnoTwo, divFifthPhnoTwo, IsClear);
                break;
            case "ddlCountryReferencesSix":
                if (IsBind)
                    BindState(ddlCountryReferencesSix, ddlStateReferencesSix);
                SwitchPhoneNumber(countryDDL, txtSixthPhnoOne, txtSixthPhnoTwo, txtSixthPhnoThree, spnSixthPhnoTwo, divSixthPhnoTwo, IsClear);

                break;
            case "ddlCountryReferencesSeven":
                if (IsBind)
                    BindState(ddlCountryReferencesSeven, ddlStateReferencesSeven);
                SwitchPhoneNumber(countryDDL, txtSeventhPhnoOne, txtSeventhPhnoTwo, txtSeventhPhnoThree, spnSeventhPhnoTwo, divSeventhPhnoTwo, IsClear);

                break;
            case "ddlCountryReferencesEight":
                if (IsBind)
                    BindState(ddlCountryReferencesEight, ddlStateReferencesEight);
                SwitchPhoneNumber(countryDDL, txtEighthPhnoOne, txtEighthPhnoTwo, txtEighthPhnoThree, spnEighthPhnoTwo, divEighthPhnoTwo, IsClear);

                break;
            case "ddlCountryReferencesNine":
                if (IsBind)
                    BindState(ddlCountryReferencesNine, ddlStateReferencesNine);
                SwitchPhoneNumber(countryDDL, txtNinthPhnoOne, txtNinthPhnoTwo, txtNinthPhnoThree, spnNinthPhnoTwo, divNinthPhnoTwo, IsClear);

                break;
            case "ddlCountryReferencesTen":
                if (IsBind)
                    BindState(ddlCountryReferencesTen, ddlStateReferencesTen);
                SwitchPhoneNumber(countryDDL, txtTenthPhnoOne, txtTenthPhnoTwo, txtTenthPhnoThree, spnTenthPhnoTwo, divTenthPhnoTwo, IsClear);

                break;


        }
    }

    //005-Sooraj
    private void BindState(DropDownList countryDDL, DropDownList stateDDL)
    {
        var stateList = objCommon.GetStateByCountry(countryDDL.SelectedValue.ConvertToShortInt());

        objCommon.BindStateDropDownByCountry(stateList, stateDDL);
    }


    //003
    /// <summary>
    /// Fill Project Reference Type based Logged in User 
    /// </summary>
    /// <Author>Sooraj Sudhakaran.T</Author>
    ///  <Date>23-SEP-2013</Date>
    private void FillProjectReferenceType(bool IsDesignConsultant)
    {
        if (IsDesignConsultant)
        {
            var masterList = objCommon.GetMasterInformationByType(MasterType.ProjectReference);
            objCommon.BindMasterDropDown(masterList, ddlProjectReferenceTypeOne);
            objCommon.BindMasterDropDown(masterList, ddlProjectReferenceTypeTwo);
            objCommon.BindMasterDropDown(masterList, ddlProjectReferenceTypeThree);
            objCommon.BindMasterDropDown(masterList, ddlProjectReferenceTypeFour);
            objCommon.BindMasterDropDown(masterList, ddlProjectReferenceTypeFive);
            objCommon.BindMasterDropDown(masterList, ddlProjectReferenceTypeSix);
            objCommon.BindMasterDropDown(masterList, ddlProjectReferenceTypeSeven);
            objCommon.BindMasterDropDown(masterList, ddlProjectReferenceTypeEight);
            objCommon.BindMasterDropDown(masterList, ddlProjectReferenceTypeNine);
            objCommon.BindMasterDropDown(masterList, ddlProjectReferenceTypeTen);
        }
    }

    /// <summary>
    /// 007 - Show hide question based on Vendor
    /// </summary>
    /// <param name="IsBasedInUS"></param>
    private void ShowHideReferenceSupplierQuestion(bool IsBasedInUS)
    {
        if (IsBasedInUS)
        {
            ShowHideView("none");
            ShowHideViewSupplier("none");
        }
        else
        {
            ShowHideView("");
            ShowHideViewSupplier("");
        }
    }
    /// <summary>
    /// 007
    /// </summary>
    /// <param name="visibleProperty"></param>
    private void ShowHideView(string visibleProperty)
    {
        trRefSpeaksEnglishOne.Style["display"] = visibleProperty;
        trRefSpeaksEnglishTwo.Style["display"] = visibleProperty;
        trRefSpeaksEnglishThree.Style["display"] = visibleProperty;
        trRefSpeaksEnglishFour.Style["display"] = visibleProperty;
        trRefSpeaksEnglishFive.Style["display"] = visibleProperty;
        trRefSpeaksEnglishSix.Style["display"] = visibleProperty;
        trRefSpeaksEnglishSeven.Style["display"] = visibleProperty;
        trRefSpeaksEnglishEight.Style["display"] = visibleProperty;
        trRefSpeaksEnglishNine.Style["display"] = visibleProperty;
        trRefSpeaksEnglishTen.Style["display"] = visibleProperty;
    }

    /// <summary>
    /// 007
    /// </summary>
    /// <param name="visibleProperty"></param>
    private void ShowHideViewSupplier(string visibleProperty)
    {
        divSpeaksEnglishOne.Style["display"] = visibleProperty;
        divSpeaksEnglishTwo.Style["display"] = visibleProperty;
        divSpeaksEnglishThree.Style["display"] = visibleProperty;
        divSpeaksEnglishFour.Style["display"] = visibleProperty;
        divSpeaksEnglishFive.Style["display"] = visibleProperty;
        divSpeaksEnglishSix.Style["display"] = visibleProperty;
        divSpeaksEnglishSeven.Style["display"] = visibleProperty;
        divSpeaksEnglishEight.Style["display"] = visibleProperty;
        divSpeaksEnglishNine.Style["display"] = visibleProperty;
        divSpeaksEnglishTen.Style["display"] = visibleProperty;
    }

    //002
    /// <summary>
    /// Switch view based on logged in user
    /// </summary>
    /// <param name="IsShow"></param>
    /// <Author>Sooraj Sudhakaran.T</Author>
    ///  <Date>20-SEP-2013</Date>
    private void SwitchDesignConsultantView(bool IsShow)
    {
        //Design Consultant view
        apnSupplier.Visible = !IsShow;

        spnProjectReferenceTypeOne.Visible = !IsShow;
        spnProjectReferenceTypeTwo.Visible = !IsShow;
        spnProjectReferenceTypeThree.Visible = !IsShow;
        spnProjectReferenceTypeFour.Visible = !IsShow;
        spnProjectReferenceTypeFive.Visible = !IsShow;
        spnProjectReferenceTypeSix.Visible = !IsShow;
        spnProjectReferenceTypeSeven.Visible = !IsShow;
        spnProjectReferenceTypeEight.Visible = !IsShow;
        spnProjectReferenceTypeNine.Visible = !IsShow;
        spnProjectReferenceTypeTen.Visible = !IsShow;

        ddlProjectReferenceTypeOne.Visible = IsShow;
        ddlProjectReferenceTypeTwo.Visible = IsShow;
        ddlProjectReferenceTypeThree.Visible = IsShow;
        ddlProjectReferenceTypeFour.Visible = IsShow;
        ddlProjectReferenceTypeFive.Visible = IsShow;
        ddlProjectReferenceTypeSix.Visible = IsShow;
        ddlProjectReferenceTypeSeven.Visible = IsShow;
        ddlProjectReferenceTypeEight.Visible = IsShow;
        ddlProjectReferenceTypeNine.Visible = IsShow;
        ddlProjectReferenceTypeTen.Visible = IsShow;

    }


    private void ShowHideControls()
    {
        switch (Convert.ToString(Request.QueryString["submenu"]))
        {
            case "1":
                accReferences.SelectedIndex = 0;
                break;
            case "2":
                accReferences.SelectedIndex = 1;
                break;
        }
    }

    //Load State details
    private void FillState()
    {
        //objCommon.DisplayddlData(ddlStateOne);
        //objCommon.DisplayddlData(ddlStateTwo);
        //objCommon.DisplayddlData(ddlStateThree);

        //objCommon.DisplayddlData(ddlStateFour);
        //objCommon.DisplayddlData(ddlStateFive);
        //objCommon.DisplayddlData(ddlStateSix);
        //objCommon.DisplayddlData(ddlStateSeven);
        //objCommon.DisplayddlData(ddlStateEight);
        //objCommon.DisplayddlData(ddlStateNine);
        //objCommon.DisplayddlData(ddlStateTen);
        //objCommon.DisplayddlData(ddlStateEleven);
        //objCommon.DisplayddlData(ddlStateTwelve);
        //objCommon.DisplayddlData(ddlStateThirteen);

        //objCommon.DisplayddlData(ddlStateReferencesFour);
        //objCommon.DisplayddlData(ddlStateReferencesFive);
        //objCommon.DisplayddlData(ddlStateReferencesSix);
        //objCommon.DisplayddlData(ddlStateReferencesSeven);
        //objCommon.DisplayddlData(ddlStateReferencesEight);
        //objCommon.DisplayddlData(ddlStateReferencesNine);
        //objCommon.DisplayddlData(ddlStateReferencesTen);



        //005-Sooraj  
        BindState(ddlCountryFour, ddlStateFour);
        BindState(ddlCountryFive, ddlStateFive);
        BindState(ddlCountrySix, ddlStateSix);
        BindState(ddlCountrySeven, ddlStateSeven);
        BindState(ddlCountryEight, ddlStateEight);
        BindState(ddlCountryNine, ddlStateNine);
        BindState(ddlCountryTen, ddlStateTen);
        BindState(ddlCountryEleven, ddlStateEleven);
        BindState(ddlCountryTwelve, ddlStateTwelve);
        BindState(ddlCountryThirteen, ddlStateThirteen);

        //005-Sooraj  
        BindState(ddlCountryOne, ddlStateOne);
        BindState(ddlCountryTwo, ddlStateTwo);
        BindState(ddlCountryThree, ddlStateThree);
        BindState(ddlCountryReferencesFour, ddlStateReferencesFour);
        BindState(ddlCountryReferencesFive, ddlStateReferencesFive);
        BindState(ddlCountryReferencesSix, ddlStateReferencesSix);
        BindState(ddlCountryReferencesSeven, ddlStateReferencesSeven);
        BindState(ddlCountryReferencesEight, ddlStateReferencesEight);
        BindState(ddlCountryReferencesNine, ddlStateReferencesNine);
        BindState(ddlCountryReferencesTen, ddlStateReferencesTen);


        if (string.IsNullOrEmpty(Convert.ToString(Session["VendorId"])) || Convert.ToString(Session["VendorId"]) == "0")
            rdoHaskellProjectsCompleted.SelectedValue = "False";
    }

    //005
    //Load Country details
    private void FillCountry()
    {
        var countryList = objCommon.GetCountryList();
        //Supplier 
        objCommon.BindCountryDropDown(countryList, ddlCountryFour);
        objCommon.BindCountryDropDown(countryList, ddlCountryFive);
        objCommon.BindCountryDropDown(countryList, ddlCountrySix);
        objCommon.BindCountryDropDown(countryList, ddlCountrySeven);
        objCommon.BindCountryDropDown(countryList, ddlCountryEight);
        objCommon.BindCountryDropDown(countryList, ddlCountryNine);
        objCommon.BindCountryDropDown(countryList, ddlCountryTen);
        objCommon.BindCountryDropDown(countryList, ddlCountryEleven);
        objCommon.BindCountryDropDown(countryList, ddlCountryTwelve);
        objCommon.BindCountryDropDown(countryList, ddlCountryThirteen);

        //Reference

        objCommon.BindCountryDropDown(countryList, ddlCountryOne);
        objCommon.BindCountryDropDown(countryList, ddlCountryTwo);
        objCommon.BindCountryDropDown(countryList, ddlCountryThree);
        objCommon.BindCountryDropDown(countryList, ddlCountryReferencesFour);
        objCommon.BindCountryDropDown(countryList, ddlCountryReferencesFive);
        objCommon.BindCountryDropDown(countryList, ddlCountryReferencesSix);
        objCommon.BindCountryDropDown(countryList, ddlCountryReferencesSeven);
        objCommon.BindCountryDropDown(countryList, ddlCountryReferencesEight);
        objCommon.BindCountryDropDown(countryList, ddlCountryReferencesNine);
        objCommon.BindCountryDropDown(countryList, ddlCountryReferencesTen);


    }

    //Fill month
    public void FillMonth(DropDownList ddlMonths)
    {
        ListItem lst;
        for (int i = 0; i <= 12; i++)
        {
            if (i == 0)
            {
                lst = new ListItem();
                lst.Text = "Month";
                lst.Value = "0";
                ddlMonths.Items.Add(lst);
            }
            else if (i < 10)
            {
                lst = new ListItem();
                lst.Text = "0" + Convert.ToString(i);
                lst.Value = "0" + Convert.ToString(i);
                ddlMonths.Items.Add(lst);
            }
            else
            {
                lst = new ListItem();
                lst.Text = Convert.ToString(i);
                lst.Value = Convert.ToString(i);
                ddlMonths.Items.Add(lst);
            }
        }
    }

    //Fill Year
    public void FillYear(DropDownList ddlYears)
    {
        ListItem lst;
        lst = new ListItem();
        lst.Text = "Year";
        lst.Value = "0";
        ddlYears.Items.Add(lst);
        for (int i = DateTime.Now.Year; i >= 1950; i--)
        {
            lst = new ListItem();
            lst.Text = Convert.ToString(i);
            lst.Value = Convert.ToString(i);
            ddlYears.Items.Add(lst);
        }
    }

    //Check the State is other,it shows another textbox
    private void ScriptForState()
    {
        ddlStateOne.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateOne.ClientID + "','" + trOther1.ClientID + "','" + txtOtherStateOne.ClientID + "','','');");
        ddlStateTwo.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateTwo.ClientID + "','" + trOther2.ClientID + "','" + txtOtherStateTwo.ClientID + "','','');");
        ddlStateThree.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateThree.ClientID + "','" + trOther3.ClientID + "','" + txtOtherStateThree.ClientID + "','','');");

        ddlStateFour.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateFour.ClientID + "','" + trOther4.ClientID + "','" + txtOtherStateFour.ClientID + "','" + ftxtSuplZipOne.ClientID + "','" + regvsuplzipone.ClientID + "','" + txtSuplZipOne.ClientID + "');");
        ddlStateFive.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateFive.ClientID + "','" + trOther5.ClientID + "','" + txtOtherStateFive.ClientID + "','" + ftxtSuplZipTwo.ClientID + "','" + regsuplziptwo.ClientID + "','" + txtSuplZipTwo.ClientID + "');");
        ddlStateSix.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateSix.ClientID + "','" + trOther6.ClientID + "','" + txtOtherStateSix.ClientID + "','" + ftxtSuplZipThree.ClientID + "','" + regvSuplZipThree.ClientID + "','" + txtSuplZipThree.ClientID + "');");
        ddlStateSeven.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateSeven.ClientID + "','" + trOther7.ClientID + "','" + txtOtherStateSeven.ClientID + "','" + ftxtSuplZipFour.ClientID + "','" + regvSuplZipFour.ClientID + "','" + txtSuplZipFour.ClientID + "');");
        ddlStateEight.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateEight.ClientID + "','" + trOther8.ClientID + "','" + txtOtherStateEight.ClientID + "','" + ftxtSuplZipFive.ClientID + "','" + regvSuplZipFive.ClientID + "','" + txtSuplZipFive.ClientID + "');");
        ddlStateNine.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateNine.ClientID + "','" + trOther9.ClientID + "','" + txtOtherStateNine.ClientID + "','" + ftxtSuplZipSix.ClientID + "','" + regvSuplZipSix.ClientID + "','" + txtSuplZipSix.ClientID + "');");
        ddlStateTen.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateTen.ClientID + "','" + trOther10.ClientID + "','" + txtOtherStateTen.ClientID + "','" + ftxtSuplZipSeven.ClientID + "','" + regvSuplZipSeven.ClientID + "','" + txtSuplZipSeven.ClientID + "');");
        ddlStateEleven.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateEleven.ClientID + "','" + trOther11.ClientID + "','" + txtOtherStateEleven.ClientID + "','" + ftxtSuplZipEight.ClientID + "','" + regvSuplZipEight.ClientID + "','" + txtSuplZipEight.ClientID + "');");
        //ddlStateTwelve.Attributes.Add("onchange", "return DisplayOthers1('" + ddlStateTwelve.ClientID + "','" + trOther12.ClientID + "','" + txtOtherStateTwelve.ClientID + "','" + ftxtSuplZipNine.ClientID + ",'" + regvSuplZipNine.ClientID + "','" + txtSuplZipNine.ClientID + "')");
        ddlStateThirteen.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateThirteen.ClientID + "','" + trOther13.ClientID + "','" + txtOtherStateThirteen.ClientID + "','" + ftxtSuplZipTen.ClientID + "','" + regvSuplZipTen.ClientID + "','" + txtSuplZipTen.ClientID + "');");

        ddlStateTwelve.Attributes.Add("onchange", "DisplayOthers1()");

        ddlStateReferencesFour.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateReferencesFour.ClientID + "','" + trOtherReferences4.ClientID + "','" + txtStateReferencesFour.ClientID + "','','');");
        ddlStateReferencesFive.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateReferencesFive.ClientID + "','" + trOtherReferences5.ClientID + "','" + txtStateReferencesFive.ClientID + "','','');");
        ddlStateReferencesSix.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateReferencesSix.ClientID + "','" + trOtherReferences6.ClientID + "','" + txtStateReferencesSix.ClientID + "','','');");
        ddlStateReferencesSeven.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateReferencesSeven.ClientID + "','" + trOtherReferences7.ClientID + "','" + txtStateReferencesSeven.ClientID + "','','');");
        ddlStateReferencesEight.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateReferencesEight.ClientID + "','" + trOtherReferences8.ClientID + "','" + txtStateRefencesEight.ClientID + "','','');");
        ddlStateReferencesNine.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateReferencesNine.ClientID + "','" + trOtherReferences9.ClientID + "','" + txtStateReferencesNine.ClientID + "','','');");
        ddlStateReferencesTen.Attributes.Add("onChange", "return DisplayOthers('" + ddlStateReferencesTen.ClientID + "','" + trOtherReferences10.ClientID + "','" + txtStateReferencesTen.ClientID + "','','');");
    }

    //Show Addional s 
    private void ControlVisiblity()
    {
        // SOC
        DisplayReferenceNA();

        trOther1.Style["display"] = (ddlStateOne.SelectedIndex > 0 && ddlStateOne.SelectedItem.Text.Equals("Other")) ? "" : "none";
        trOther2.Style["display"] = (ddlStateTwo.SelectedIndex > 0 && ddlStateTwo.SelectedItem.Text.Equals("Other")) ? "" : "none";
        trOther3.Style["display"] = (ddlStateThree.SelectedIndex > 0 && ddlStateThree.SelectedItem.Text.Equals("Other")) ? "" : "none";

        trOther1lbl.Style["display"] = trOther1.Style["display"];
        trOther2lbl.Style["display"] = trOther2.Style["display"];
        trOther3lbl.Style["display"] = trOther3.Style["display"];

        SetValidCharsAndLength(ddlStateFour, txtSuplZipOne, ftxtSuplZipOne, regvsuplzipone, trOther4, trOther4lbl);
        SetValidCharsAndLength(ddlStateFive, txtSuplZipTwo, ftxtSuplZipTwo, regsuplziptwo, trOther5, trOther5lbl);
        SetValidCharsAndLength(ddlStateSix, txtSuplZipThree, ftxtSuplZipThree, regvSuplZipThree, trOther6, trOther6lbl);
        SetValidCharsAndLength(ddlStateSeven, txtSuplZipFour, ftxtSuplZipFour, regvSuplZipFour, trOther7, trOther7lbl);
        SetValidCharsAndLength(ddlStateEight, txtSuplZipFive, ftxtSuplZipFive, regvSuplZipFive, trOther8, trOther8lbl);
        SetValidCharsAndLength(ddlStateNine, txtSuplZipSix, ftxtSuplZipSix, regvSuplZipSix, trOther9, trOther9lbl);
        SetValidCharsAndLength(ddlStateTen, txtSuplZipSeven, ftxtSuplZipSeven, regvSuplZipSeven, trOther10, trOther10lbl);
        SetValidCharsAndLength(ddlStateEleven, txtSuplZipEight, ftxtSuplZipEight, regvSuplZipEight, trOther11, trOther11lbl);
        SetValidCharsAndLength(ddlStateTwelve, txtSuplZipNine, ftxtSuplZipNine, regvSuplZipNine, trOther12, trOther12lbl);
        SetValidCharsAndLength(ddlStateThirteen, txtSuplZipTen, ftxtSuplZipTen, regvSuplZipTen, trOther13, trOther13lbl);

        trOtherReferences4.Style["display"] = (ddlStateReferencesFour.SelectedIndex > 0 && ddlStateReferencesFour.SelectedItem.Text.Equals("Other")) ? "" : "none";
        trOtherReferences5.Style["display"] = (ddlStateReferencesFive.SelectedIndex > 0 && ddlStateReferencesFive.SelectedItem.Text.Equals("Other")) ? "" : "none";
        trOtherReferences6.Style["display"] = (ddlStateReferencesSix.SelectedIndex > 0 && ddlStateReferencesSix.SelectedItem.Text.Equals("Other")) ? "" : "none";
        trOtherReferences7.Style["display"] = (ddlStateReferencesSeven.SelectedIndex > 0 && ddlStateReferencesSeven.SelectedItem.Text.Equals("Other")) ? "" : "none";
        trOtherReferences8.Style["display"] = (ddlStateReferencesEight.SelectedIndex > 0 && ddlStateReferencesEight.SelectedItem.Text.Equals("Other")) ? "" : "none";
        trOtherReferences9.Style["display"] = (ddlStateReferencesNine.SelectedIndex > 0 && ddlStateReferencesNine.SelectedItem.Text.Equals("Other")) ? "" : "none";
        trOtherReferences10.Style["display"] = (ddlStateReferencesTen.SelectedIndex > 0 && ddlStateReferencesTen.SelectedItem.Text.Equals("Other")) ? "" : "none";


        trOtherReferences4lbl.Style["display"] = trOtherReferences4.Style["display"];
        trOtherReferences5lbl.Style["display"] = trOtherReferences5.Style["display"];
        trOtherReferences6lbl.Style["display"] = trOtherReferences6.Style["display"];
        trOtherReferences7lbl.Style["display"] = trOtherReferences7.Style["display"];
        trOtherReferences8lbl.Style["display"] = trOtherReferences8.Style["display"];
        trOtherReferences9lbl.Style["display"] = trOtherReferences9.Style["display"];
        trOtherReferences10lbl.Style["display"] = trOtherReferences10.Style["display"];

        // EOC

        if (rdoHaskellProjectsCompleted.SelectedIndex == 0)
        {
            tdProject.Style["display"] = "";
            lnladdProject.Style["display"] = "";
            for (int i = 1; i <= Convert.ToInt32(hdnaddProjectcount.Value); i++)
            {
                switch (i)
                {
                    case 1:
                        trAddProject01.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        break;
                    case 2:
                        trAddProject02.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        break;
                    case 3:
                        trAddProject03.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        break;
                    case 4:
                        trAddProject04.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        break;
                    case 5:
                        trAddProject05.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        break;
                    case 6:
                        trAddProject06.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        break;
                    case 7:
                        trAddProject07.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        break;
                    case 8:
                        trAddProject08.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        break;
                    case 9:
                        trAddProject09.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        break;
                    case 10:
                        trAddProject10.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "none";
                        //lnladdProject.Visible = false;
                        break;
                }
            }
        }
        else
        {
            tdProject.Style["display"] = "none";
        }
        lnladdProject.Style["display"] = hdnaddProjectcount.Value.Equals("10") ? "none" : "";
        // 

        for (int i = 1; i <= Convert.ToInt32(hdnCount.Value); i++)
        {
            switch (i)
            {
                case 1:
                    spnmore1.Style["display"] = "";
                    break;
                case 2:
                    spnmore2.Style["display"] = "";
                    break;
                case 3:
                    spnmore3.Style["display"] = "";
                    break;
                case 4:
                    spnmore4.Style["display"] = "";
                    break;
                case 5:
                    spnmore5.Style["display"] = "";
                    break;
                case 6:
                    spnmore6.Style["display"] = "";
                    break;
                case 7:
                    spnmore7.Style["display"] = "";
                    imgAddMore.Style["display"] = "none";
                    break;
            }
        }

        for (int i = 4; i <= Convert.ToInt32(hdnAdditionalReferencesCount.Value); i++)
        {
            switch (i)
            {
                case 4:
                    trReferencesFour.Style["display"] = "";
                    break;
                case 5:
                    trReferencesFive.Style["display"] = "";
                    break;
                case 6:
                    trReferencesSix.Style["display"] = "";
                    break;
                case 7:
                    trReferencesSeven.Style["display"] = "";
                    break;
                case 8:
                    trReferencesEight.Style["display"] = "";
                    break;
                case 9:
                    trReferencesNine.Style["display"] = "";

                    break;
                case 10:
                    lnkAddMoreReference.Style["display"] = "none";
                    trReferencesTen.Style["display"] = "";
                    break;
            }
        }
        lnkAddMoreReference.Style["display"] = hdnAdditionalReferencesCount.Value.Equals("10") ? "none" : "";
    }

    private void DisplayReferenceNA()
    {
        // First
        txtProjectNameOne.Enabled = !chkNA1.Checked;
        txtCity.Enabled = !chkNA1.Checked;
        ddlStateOne.Enabled = !chkNA1.Checked;
        ddlCountryOne.Enabled = !chkNA1.Checked; //006
        ddlMonth1.Enabled = !chkNA1.Checked;
        ddlYear1.Enabled = !chkNA1.Checked;
        txtApproximateContractAmtOne.Enabled = !chkNA1.Checked;
        txtContractorNameOne.Enabled = !chkNA1.Checked;
        txtCtrContactNameOne.Enabled = !chkNA1.Checked;
        txtMainPhnoOne.Enabled = !chkNA1.Checked;
        txtMainPhnoTwo.Enabled = !chkNA1.Checked;
        txtMainPhnoThree.Enabled = !chkNA1.Checked;
        txtWorkCompletedOne.Enabled = !chkNA1.Checked;
        trExplain1.Style["Display"] = chkNA1.Checked ? "" : "none";
        trtxtExplain1.Style["Display"] = chkNA1.Checked ? "" : "none";
        ddlRefSpeaksEnglishOne.Enabled = !chkNA1.Checked; //007

        // Second
        txtProjectNameTwo.Enabled = !chkNA2.Checked;
        txtCityTwo.Enabled = !chkNA2.Checked;
        ddlStateTwo.Enabled = !chkNA2.Checked;
        ddlCountryTwo.Enabled = !chkNA2.Checked; //006
        ddlMonth2.Enabled = !chkNA2.Checked;
        ddlYear2.Enabled = !chkNA2.Checked;
        txtApproximateContractAmtTwo.Enabled = !chkNA2.Checked;
        txtContractorNameTwo.Enabled = !chkNA2.Checked;
        txtCtrContactNameTwo.Enabled = !chkNA2.Checked;
        txtSecondPhnoOne.Enabled = !chkNA2.Checked;
        txtSecondPhnoTwo.Enabled = !chkNA2.Checked;
        txtSecondPhnoThree.Enabled = !chkNA2.Checked;
        txtWorkCompletedTwo.Enabled = !chkNA2.Checked;
        trExplain2.Style["Display"] = chkNA2.Checked ? "" : "none";
        trtxtExplain2.Style["Display"] = chkNA2.Checked ? "" : "none";
        ddlRefSpeaksEnglishTwo.Enabled = !chkNA2.Checked; //007

        // Third
        txtProjectNameThree.Enabled = !chkNA3.Checked;
        txtCityThree.Enabled = !chkNA3.Checked;
        ddlStateThree.Enabled = !chkNA3.Checked;
        ddlCountryThree.Enabled = !chkNA3.Checked; //006
        ddlMonth3.Enabled = !chkNA3.Checked;
        ddlYear3.Enabled = !chkNA3.Checked;
        txtApproximateContractAmtThree.Enabled = !chkNA3.Checked;
        txtContractorNameThree.Enabled = !chkNA3.Checked;
        txtCtrContactNameThree.Enabled = !chkNA3.Checked;
        txtThirdPhnoOne.Enabled = !chkNA3.Checked;
        txtThirdPhnoTwo.Enabled = !chkNA3.Checked;
        txtThirdPhnoThree.Enabled = !chkNA3.Checked;
        txtWorkCompletedThree.Enabled = !chkNA3.Checked;
        trExplain3.Style["Display"] = chkNA3.Checked ? "" : "none";
        trtxtExplain3.Style["Display"] = chkNA3.Checked ? "" : "none";
        ddlRefSpeaksEnglishThree.Enabled = !chkNA3.Checked; //007
    }


    //phone number textbox focus Change to another text box 
    private void CallScripts()
    {
        chkNA1.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + trExplain1.ClientID + "','" + trtxtExplain1.ClientID + "','" + txtExplain1.ClientID + "','" + txtProjectNameOne.ClientID + "','" + txtCity.ClientID + "','" + ddlStateOne.ClientID + "' ,'" + ddlMonth1.ClientID + "','" + ddlYear1.ClientID + "' ,'" + txtOtherStateOne.ClientID + "','" + txtApproximateContractAmtOne.ClientID + "','" + txtContractorNameOne.ClientID + "','" + txtCtrContactNameOne.ClientID + "','" + txtMainPhnoOne.ClientID + "','" + txtMainPhnoTwo.ClientID + "','" + txtMainPhnoThree.ClientID + "','" + txtWorkCompletedOne.ClientID + "','" + ddlProjectReferenceTypeOne.ClientID + "','" + ddlRefSpeaksEnglishOne.ClientID + "','" + ddlCountryOne.ClientID + "');");  //005
        chkNA2.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + trExplain2.ClientID + "','" + trtxtExplain2.ClientID + "','" + txtExplain2.ClientID + "','" + txtProjectNameTwo.ClientID + "','" + txtCityTwo.ClientID + "','" + ddlStateTwo.ClientID + "' ,'" + ddlMonth2.ClientID + "','" + ddlYear2.ClientID + "' ,'" + txtOtherStateTwo.ClientID + "','" + txtApproximateContractAmtTwo.ClientID + "','" + txtContractorNameTwo.ClientID + "','" + txtCtrContactNameTwo.ClientID + "','" + txtSecondPhnoOne.ClientID + "','" + txtSecondPhnoTwo.ClientID + "','" + txtSecondPhnoThree.ClientID + "','" + txtWorkCompletedTwo.ClientID + "','" + ddlProjectReferenceTypeTwo.ClientID + "','" + ddlRefSpeaksEnglishTwo.ClientID + "','" + ddlCountryTwo.ClientID + "');");//005
        chkNA3.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + trExplain3.ClientID + "','" + trtxtExplain3.ClientID + "','" + txtExplain3.ClientID + "','" + txtProjectNameThree.ClientID + "','" + txtCityThree.ClientID + "','" + ddlStateThree.ClientID + "' ,'" + ddlMonth3.ClientID + "','" + ddlYear3.ClientID + "' ,'" + txtOtherStateThree.ClientID + "','" + txtApproximateContractAmtThree.ClientID + "','" + txtContractorNameThree.ClientID + "','" + txtCtrContactNameThree.ClientID + "','" + txtThirdPhnoOne.ClientID + "','" + txtThirdPhnoTwo.ClientID + "','" + txtThirdPhnoThree.ClientID + "','" + txtWorkCompletedThree.ClientID + "','" + ddlProjectReferenceTypeThree.ClientID + "','" + ddlRefSpeaksEnglishThree.ClientID + "','" + ddlCountryThree.ClientID + "');");//005

        txtMainPhnoOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtMainPhnoOne.ClientID + "," + txtMainPhnoTwo.ClientID + ")");
        txtMainPhnoTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtMainPhnoTwo.ClientID + "," + txtMainPhnoThree.ClientID + ")");

        txtSecondPhnoOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSecondPhnoOne.ClientID + "," + txtSecondPhnoTwo.ClientID + ")");
        txtSecondPhnoTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSecondPhnoTwo.ClientID + "," + txtSecondPhnoThree.ClientID + ")");

        txtThirdPhnoOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtThirdPhnoOne.ClientID + "," + txtThirdPhnoTwo.ClientID + ")");
        txtThirdPhnoTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtThirdPhnoTwo.ClientID + "," + txtThirdPhnoThree.ClientID + ")");

        txtFourthPhnoOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtFourthPhnoOne.ClientID + "," + txtFourthPhnoTwo.ClientID + ")");
        txtFourthPhnoTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtFourthPhnoTwo.ClientID + "," + txtFourthPhnoThree.ClientID + ")");

        txtFifthPhnoOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtFifthPhnoOne.ClientID + "," + txtFifthPhnoTwo.ClientID + ")");
        txtFifthPhnoTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtFifthPhnoTwo.ClientID + "," + txtFifthPhnoThree.ClientID + ")");

        txtSixthPhnoOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSixthPhnoOne.ClientID + "," + txtSixthPhnoTwo.ClientID + ")");
        txtSixthPhnoTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSixthPhnoTwo.ClientID + "," + txtSixthPhnoThree.ClientID + ")");

        txtSeventhPhnoOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSeventhPhnoOne.ClientID + "," + txtSeventhPhnoTwo.ClientID + ")");
        txtSeventhPhnoTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSeventhPhnoTwo.ClientID + "," + txtSeventhPhnoThree.ClientID + ")");

        txtEighthPhnoOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtEighthPhnoOne.ClientID + "," + txtEighthPhnoTwo.ClientID + ")");
        txtEighthPhnoTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtEighthPhnoTwo.ClientID + "," + txtEighthPhnoThree.ClientID + ")");

        txtNinthPhnoOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtNinthPhnoOne.ClientID + "," + txtNinthPhnoTwo.ClientID + ")");
        txtNinthPhnoTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtNinthPhnoTwo.ClientID + "," + txtNinthPhnoThree.ClientID + ")");

        txtTenthPhnoOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtTenthPhnoOne.ClientID + "," + txtTenthPhnoTwo.ClientID + ")");
        txtTenthPhnoTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtTenthPhnoTwo.ClientID + "," + txtTenthPhnoThree.ClientID + ")");

        txtSuplMainPhoneOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplMainPhoneOne.ClientID + "," + txtSuplMainPhoneTwo.ClientID + ")");
        txtSuplMainPhoneTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplMainPhoneTwo.ClientID + "," + txtSuplMainPhoneThree.ClientID + ")");

        txtSuplSecondPhoneOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplSecondPhoneOne.ClientID + "," + txtSuplSecondPhoneTwo.ClientID + ")");
        txtSuplSecondPhoneTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplSecondPhoneTwo.ClientID + "," + txtSuplSecondPhoneThree.ClientID + ")");

        txtSuplThirdPhoneOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplThirdPhoneOne.ClientID + "," + txtSuplThirdPhoneTwo.ClientID + ")");
        txtSuplThirdPhoneTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplThirdPhoneTwo.ClientID + "," + txtSuplThirdPhoneThree.ClientID + ")");

        txtSuplFourthPhoneOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplFourthPhoneOne.ClientID + "," + txtSuplFourthPhoneTwo.ClientID + ")");
        txtSuplFourthPhoneTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplFourthPhoneTwo.ClientID + "," + txtSuplFourthPhoneThree.ClientID + ")");

        txtSuplFivePhoneOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplFivePhoneOne.ClientID + "," + txtSuplFivePhoneTwo.ClientID + ")");
        txtSuplFivePhoneTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplFivePhoneTwo.ClientID + "," + txtSuplFivePhoneThree.ClientID + ")");

        txtSuplSixPhoneOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplSixPhoneOne.ClientID + "," + txtSuplSixPhoneTwo.ClientID + ")");
        txtSuplSixPhoneTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplSixPhoneTwo.ClientID + "," + txtSuplSixPhoneThree.ClientID + ")");

        txtSuplSevenPhoneOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplSevenPhoneOne.ClientID + "," + txtSuplSevenPhoneTwo.ClientID + ")");
        txtSuplSevenPhoneTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplSevenPhoneTwo.ClientID + "," + txtSuplSevenPhoneThree.ClientID + ")");

        txtSuplEightPhoneOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplEightPhoneOne.ClientID + "," + txtSuplEightPhoneTwo.ClientID + ")");
        txtSuplEightPhoneTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplEightPhoneTwo.ClientID + "," + txtSuplEightPhoneThree.ClientID + ")");

        txtSuplNinePhoneOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplNinePhoneOne.ClientID + "," + txtSuplNinePhoneTwo.ClientID + ")");
        txtSuplNinePhoneTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplNinePhoneTwo.ClientID + "," + txtSuplNinePhoneThree.ClientID + ")");

        txtSuplTenPhoneOne.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplTenPhoneOne.ClientID + "," + txtSuplTenPhoneTwo.ClientID + ")");
        txtSuplTenPhoneTwo.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtSuplTenPhoneTwo.ClientID + "," + txtSuplTenPhoneThree.ClientID + ")");

        txtApproximateContractAmtOne.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtOne.ClientID + "')");

        txtApproximateContractAmtTwo.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtTwo.ClientID + "')");

        txtApproximateContractAmtThree.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtThree.ClientID + "')");

        txtApproximateContractAmtFour.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtFour.ClientID + "')");
        txtApproximateContractAmtFive.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtFive.ClientID + "')");
        txtApproximateContractAmtSix.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtSix.ClientID + "')");
        txtApproximateContractAmtSeven.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtSeven.ClientID + "')");
        txtApproximateContractAmtEight.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtEight.ClientID + "')");
        txtApproximateContractAmtnine.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtnine.ClientID + "')");
        txtApproximateContractAmtTen.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtTen.ClientID + "')");

        txtApproximateContractAmtOne.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtOne.ClientID + "')");
        txtApproximateContractAmtTwo.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtTwo.ClientID + "')");
        txtApproximateContractAmtThree.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtThree.ClientID + "')");
        txtApproximateContractAmtFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtFour.ClientID + "')");
        txtApproximateContractAmtFive.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtFive.ClientID + "')");
        txtApproximateContractAmtSix.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtSix.ClientID + "')");
        txtApproximateContractAmtSeven.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtSeven.ClientID + "')");
        txtApproximateContractAmtEight.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtEight.ClientID + "')");
        txtApproximateContractAmtnine.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtnine.ClientID + "')");
        txtApproximateContractAmtTen.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtApproximateContractAmtTen.ClientID + "')");

        lnkAddMoreReference.Attributes.Add("onClick", "return VisibleReferencestr();");

        rdoHaskellProjectsCompleted.Attributes.Add("onClick", "return ShowProject();");
        ScriptForState();


        BindAssociateState(ddlCountryFour, false,true); //006-Sooraj
        BindAssociateState(ddlCountryFive, false, true); //006-Sooraj 
        BindAssociateState(ddlCountrySix, false, true); //006-Sooraj 
        BindAssociateState(ddlCountrySeven, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryEight, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryNine, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryTen, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryEleven, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryTwelve, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryThirteen, false, true); //006-Sooraj 

        BindAssociateStateRefernce(ddlCountryOne, false, true); //006-Sooraj
        BindAssociateStateRefernce(ddlCountryTwo, false, true); //006-Sooraj 
        BindAssociateStateRefernce(ddlCountryThree, false, true); //006-Sooraj 
        BindAssociateStateRefernce(ddlCountryReferencesFour, false, true); //006-Sooraj 
        BindAssociateStateRefernce(ddlCountryReferencesFive, false, true); //006-Sooraj 
        BindAssociateStateRefernce(ddlCountryReferencesSix, false, true); //006-Sooraj 
        BindAssociateStateRefernce(ddlCountryReferencesSeven, false, true); //006-Sooraj 
        BindAssociateStateRefernce(ddlCountryReferencesEight, false, true); //006-Sooraj 
        BindAssociateStateRefernce(ddlCountryReferencesNine, false, true); //006-Sooraj 
        BindAssociateStateRefernce(ddlCountryReferencesTen, false, true); //006-Sooraj 
    }

    //Insert Project Reference details
    public long InsertProjectReferences(int status)
    {
        returnRefId = objReferences.InsertReferences(projectsCompleted, Convert.ToInt64(Session["VendorId"]), Convert.ToByte(status));
        return returnRefId;
    }

    public void InsertReferencesProject(long References)
    {
        short State;
        State = ddlStateOne.SelectedIndex > 0 ? Convert.ToInt16(ddlStateOne.SelectedItem.Value) : (byte)0;

        string Phone = null;
        if (!((string.IsNullOrEmpty(txtMainPhnoOne.Text)) && (string.IsNullOrEmpty(txtMainPhnoTwo.Text)) && (string.IsNullOrEmpty(txtMainPhnoThree.Text))))
        {
            if (txtMainPhnoOne.Text.Length + txtMainPhnoTwo.Text.Length + txtMainPhnoThree.Text.Length > 0)
            {
                Phone = objCommon.Phoneformat(txtMainPhnoOne.Text, txtMainPhnoTwo.Text, txtMainPhnoThree.Text, txtMainPhnoThree);//006
            }
        }
        if ((ddlMonth1.SelectedIndex > 0) && (ddlYear1.SelectedIndex > 0))
        {
            completionDateOne = ddlMonth1.SelectedValue + "/" + ddlYear1.SelectedValue;
        }
        else
        {
            completionDateOne = null;
        }
        if (!chkNA1.Checked)
        {
            if (!(string.IsNullOrEmpty(txtProjectNameOne.Text) && string.IsNullOrEmpty(txtCity.Text) && State == 0 && string.IsNullOrEmpty(completionDateOne) && string.IsNullOrEmpty(txtApproximateContractAmtOne.Text) && string.IsNullOrEmpty(txtCtrContactNameOne.Text) && string.IsNullOrEmpty(Phone) && string.IsNullOrEmpty(txtWorkCompletedOne.Text)))
            {
                objReferences.insertProjectReferences(References, txtProjectNameOne.Text, txtCity.Text, State, completionDateOne, txtOtherStateOne.Text, txtApproximateContractAmtOne.Text, ddlProjectReferenceTypeOne.SelectedValue.ConvertToShortInt(), txtContractorNameOne.Text, txtCtrContactNameOne.Text, Phone, txtWorkCompletedOne.Text, null, chkNA1.Checked, txtExplain1.Text, ddlRefSpeaksEnglishOne.SelectedValue.ConvertToBool(), ddlCountryOne.SelectedValue.ConvertToShortInt());//006 //007
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(txtExplain1.Text))
            {
                objReferences.insertProjectReferences(References, txtProjectNameOne.Text, txtCity.Text, State, completionDateOne, txtOtherStateOne.Text, txtApproximateContractAmtOne.Text, ddlProjectReferenceTypeTwo.SelectedValue.ConvertToShortInt(), txtContractorNameOne.Text, txtCtrContactNameOne.Text, Phone, txtWorkCompletedOne.Text, null, chkNA1.Checked, txtExplain1.Text, ddlRefSpeaksEnglishOne.SelectedValue.ConvertToBool(), ddlCountryOne.SelectedValue.ConvertToShortInt());//006 //007
            }
        }
        State = ddlStateTwo.SelectedIndex > 0 ? Convert.ToInt16(ddlStateTwo.SelectedItem.Value) : (byte)0;

        Phone = null;
        if (!((string.IsNullOrEmpty(txtSecondPhnoOne.Text)) && (string.IsNullOrEmpty(txtSecondPhnoTwo.Text)) && (string.IsNullOrEmpty(txtSecondPhnoThree.Text))))
        {
            if (txtSecondPhnoOne.Text.Length + txtSecondPhnoTwo.Text.Length + txtSecondPhnoThree.Text.Length > 0)
            {
                Phone = objCommon.Phoneformat(txtSecondPhnoOne.Text, txtSecondPhnoTwo.Text, txtSecondPhnoThree.Text, txtSecondPhnoThree);//006
            }
        }

        string CompletionDate = null;
        if (ddlYear2.SelectedIndex > 0 && ddlMonth2.SelectedIndex > 0)
        {
            CompletionDate = ddlMonth2.SelectedItem.Text + "/" + ddlYear2.SelectedItem.Text;
        }

        if (!chkNA2.Checked)
        {
            if (!(string.IsNullOrEmpty(txtProjectNameTwo.Text) && string.IsNullOrEmpty(txtCityTwo.Text) && State == 0 && string.IsNullOrEmpty(CompletionDate) && string.IsNullOrEmpty(txtApproximateContractAmtTwo.Text) && string.IsNullOrEmpty(txtCtrContactNameTwo.Text) && string.IsNullOrEmpty(Phone) && string.IsNullOrEmpty(txtWorkCompletedTwo.Text)))
            {
                objReferences.insertProjectReferences(References, txtProjectNameTwo.Text, txtCityTwo.Text, State, CompletionDate, txtOtherStateTwo.Text, txtApproximateContractAmtTwo.Text, ddlProjectReferenceTypeTwo.SelectedValue.ConvertToShortInt(), txtContractorNameTwo.Text, txtCtrContactNameTwo.Text, Phone, txtWorkCompletedTwo.Text, null, chkNA2.Checked, txtExplain2.Text, ddlRefSpeaksEnglishTwo.SelectedValue.ConvertToBool(), ddlCountryTwo.SelectedValue.ConvertToShortInt());//006 //007
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(txtExplain2.Text))
            {
                objReferences.insertProjectReferences(References, txtProjectNameTwo.Text, txtCityTwo.Text, State, CompletionDate, txtOtherStateTwo.Text, txtApproximateContractAmtTwo.Text, ddlProjectReferenceTypeTwo.SelectedValue.ConvertToShortInt(), txtContractorNameTwo.Text, txtCtrContactNameTwo.Text, Phone, txtWorkCompletedTwo.Text, null, chkNA2.Checked, txtExplain2.Text, ddlRefSpeaksEnglishTwo.SelectedValue.ConvertToBool(), ddlCountryTwo.SelectedValue.ConvertToShortInt());//006 //007
            }
        }
        State = ddlStateThree.SelectedIndex > 0 ? Convert.ToInt16(ddlStateThree.SelectedItem.Value) : (byte)0;

        Phone = null;
        if (!((string.IsNullOrEmpty(txtThirdPhnoOne.Text)) && (string.IsNullOrEmpty(txtThirdPhnoTwo.Text)) && (string.IsNullOrEmpty(txtThirdPhnoThree.Text))))
        {
            if (txtThirdPhnoOne.Text.Length + txtThirdPhnoTwo.Text.Length + txtThirdPhnoThree.Text.Length > 0)
            {
                Phone = objCommon.Phoneformat(txtThirdPhnoOne.Text, txtThirdPhnoTwo.Text, txtThirdPhnoThree.Text, txtThirdPhnoThree);//006
            }
        }

        CompletionDate = null;
        if (ddlYear3.SelectedIndex > 0 && ddlMonth3.SelectedIndex > 0)
        {
            CompletionDate = ddlMonth3.SelectedItem.Text + "/" + ddlYear3.SelectedItem.Text;
        }

        if (!chkNA3.Checked)
        {
            if (!(string.IsNullOrEmpty(txtProjectNameThree.Text) && string.IsNullOrEmpty(txtCityThree.Text) && State == 0 && string.IsNullOrEmpty(CompletionDate) && string.IsNullOrEmpty(txtApproximateContractAmtThree.Text) && string.IsNullOrEmpty(txtCtrContactNameThree.Text) && string.IsNullOrEmpty(Phone) && string.IsNullOrEmpty(txtWorkCompletedThree.Text)))
            {
                objReferences.insertProjectReferences(References, txtProjectNameThree.Text, txtCityThree.Text, State, CompletionDate, txtOtherStateThree.Text, txtApproximateContractAmtThree.Text, ddlProjectReferenceTypeThree.SelectedValue.ConvertToShortInt(), txtContractorNameThree.Text, txtCtrContactNameThree.Text, Phone, txtWorkCompletedThree.Text, null, chkNA3.Checked, txtExplain3.Text, ddlRefSpeaksEnglishThree.SelectedValue.ConvertToBool(), ddlCountryThree.SelectedValue.ConvertToShortInt());//006 //007
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(txtExplain3.Text))
            {
                objReferences.insertProjectReferences(References, txtProjectNameThree.Text, txtCityThree.Text, State, CompletionDate, txtOtherStateThree.Text, txtApproximateContractAmtThree.Text, ddlProjectReferenceTypeThree.SelectedValue.ConvertToShortInt(), txtContractorNameThree.Text, txtCtrContactNameThree.Text, Phone, txtWorkCompletedThree.Text, null, chkNA3.Checked, txtExplain3.Text, ddlRefSpeaksEnglishThree.SelectedValue.ConvertToBool(), ddlCountryThree.SelectedValue.ConvertToShortInt());//006 //007
            }
        }

        if (trReferencesFour.Style["display"] == "")
        {
            State = ddlStateReferencesFour.SelectedIndex > 0 ? Convert.ToInt16(ddlStateReferencesFour.SelectedItem.Value) : (byte)0;
            Phone = null;
            if (!((string.IsNullOrEmpty(txtFourthPhnoOne.Text)) && (string.IsNullOrEmpty(txtFourthPhnoTwo.Text)) && (string.IsNullOrEmpty(txtFourthPhnoThree.Text))))
            {
                if (txtFourthPhnoOne.Text.Length + txtFourthPhnoTwo.Text.Length + txtFourthPhnoThree.Text.Length > 0)
                {
                    Phone = objCommon.Phoneformat(txtFourthPhnoOne.Text, txtFourthPhnoTwo.Text, txtFourthPhnoThree.Text, txtFourthPhnoThree);//006
                }
            }
            CompletionDate = null;
            if (ddlYear4.SelectedIndex > 0 && ddlMonth4.SelectedIndex > 0)
            {
                CompletionDate = ddlMonth4.SelectedItem.Text + "/" + ddlYear4.SelectedItem.Text;
            }
            if (!(string.IsNullOrEmpty(txtProjectNameFour.Text) && string.IsNullOrEmpty(txtCityFour.Text) && State == 0 && string.IsNullOrEmpty(CompletionDate) && string.IsNullOrEmpty(txtApproximateContractAmtFour.Text) && string.IsNullOrEmpty(txtCtrContactNameFour.Text) && string.IsNullOrEmpty(Phone) && string.IsNullOrEmpty(txtWorkCompletedFour.Text)))
            {
                objReferences.insertProjectReferences(References, txtProjectNameFour.Text, txtCityFour.Text, State, CompletionDate, txtStateReferencesFour.Text, txtApproximateContractAmtFour.Text, ddlProjectReferenceTypeFour.SelectedValue.ConvertToShortInt(), txtContractorNameFour.Text, txtCtrContactNameFour.Text, Phone, txtWorkCompletedFour.Text, null, false, null, ddlRefSpeaksEnglishFour.SelectedValue.ConvertToBool(), ddlCountryReferencesFour.SelectedValue.ConvertToShortInt());//006 //007
            }
            ResetProjectReferenceValues(trReferencesFour, txtProjectNameFour, txtCityFour, txtStateReferencesFour, txtApproximateContractAmtFour, ddlProjectReferenceTypeFour, txtContractorNameFour, txtWorkCompletedFour, txtFourthPhnoOne, txtFourthPhnoTwo, txtFourthPhnoThree, ddlStateReferencesFour, ddlYear4, ddlMonth4, null, null, txtCtrContactNameFour);
        }

        if (trReferencesFive.Style["display"] == "")
        {
            State = ddlStateReferencesFive.SelectedIndex > 0 ? Convert.ToInt16(ddlStateReferencesFive.SelectedItem.Value) : (byte)0;

            Phone = null;
            CompletionDate = null;
            if (ddlYear5.SelectedIndex > 0 && ddlMonth5.SelectedIndex > 0)
            {
                CompletionDate = ddlMonth5.SelectedItem.Text + "/" + ddlYear5.SelectedItem.Text;
            }
            if (!((string.IsNullOrEmpty(txtFifthPhnoOne.Text)) && (string.IsNullOrEmpty(txtFifthPhnoTwo.Text)) && (string.IsNullOrEmpty(txtFifthPhnoThree.Text))))
            {
                if (txtFifthPhnoOne.Text.Length + txtFifthPhnoTwo.Text.Length + txtFifthPhnoThree.Text.Length > 0)
                {
                    Phone = objCommon.Phoneformat(txtFifthPhnoOne.Text, txtFifthPhnoTwo.Text, txtFifthPhnoThree.Text, txtFifthPhnoThree);//006
                }
            }
            if (!(string.IsNullOrEmpty(txtProjectNameFive.Text) && string.IsNullOrEmpty(txtCityFive.Text) && State == 0 && string.IsNullOrEmpty(CompletionDate) && string.IsNullOrEmpty(txtApproximateContractAmtFive.Text) && string.IsNullOrEmpty(txtCtrContactNameFive.Text) && string.IsNullOrEmpty(Phone) && string.IsNullOrEmpty(txtWorkCompletedFive.Text)))
            {
                objReferences.insertProjectReferences(References, txtProjectNameFive.Text, txtCityFive.Text, State, CompletionDate, txtStateReferencesFive.Text, txtApproximateContractAmtFive.Text, ddlProjectReferenceTypeFive.SelectedValue.ConvertToShortInt(), txtContractorNameFive.Text, txtCtrContactNameFive.Text, Phone, txtWorkCompletedFive.Text, null, false, null, ddlRefSpeaksEnglishFive.SelectedValue.ConvertToBool(), ddlCountryReferencesFive.SelectedValue.ConvertToShortInt());//006 //007
            }
            ResetProjectReferenceValues(trReferencesFive, txtProjectNameFive, txtCityFive, txtStateReferencesFive, txtApproximateContractAmtFive, ddlProjectReferenceTypeFive, txtContractorNameFive, txtWorkCompletedFive, txtFifthPhnoOne, txtFifthPhnoTwo, txtFifthPhnoThree, ddlStateReferencesFive, ddlYear5, ddlMonth5, null, null, txtCtrContactNameFive);
        }

        if (trReferencesSix.Style["display"] == "")
        {
            State = ddlStateReferencesSix.SelectedIndex > 0 ? Convert.ToInt16(ddlStateReferencesSix.SelectedItem.Value) : (byte)0;

            Phone = null;
            if (!((string.IsNullOrEmpty(txtSixthPhnoOne.Text)) && (string.IsNullOrEmpty(txtSixthPhnoTwo.Text)) && (string.IsNullOrEmpty(txtSixthPhnoThree.Text))))
            {
                if (txtSixthPhnoOne.Text.Length + txtSixthPhnoTwo.Text.Length + txtSixthPhnoThree.Text.Length > 0)
                {
                    Phone = objCommon.Phoneformat(txtSixthPhnoOne.Text, txtSixthPhnoTwo.Text, txtSixthPhnoThree.Text, txtSixthPhnoThree);//006
                }
            }
            CompletionDate = null;
            if (ddlYear6.SelectedIndex > 0 && ddlMonth6.SelectedIndex > 0)
            {
                CompletionDate = ddlMonth6.SelectedItem.Text + "/" + ddlYear6.SelectedItem.Text;
            }
            if (!(string.IsNullOrEmpty(txtProjectNameSix.Text) && string.IsNullOrEmpty(txtCitySix.Text) && State == 0 && string.IsNullOrEmpty(CompletionDate) && string.IsNullOrEmpty(txtApproximateContractAmtSix.Text) && string.IsNullOrEmpty(txtCtrContactNameSix.Text) && string.IsNullOrEmpty(Phone) && string.IsNullOrEmpty(txtWorkCompletedSix.Text)))
            {
                objReferences.insertProjectReferences(References, txtProjectNameSix.Text, txtCitySix.Text, State, CompletionDate, txtStateReferencesSix.Text, txtApproximateContractAmtSix.Text, ddlProjectReferenceTypeSix.SelectedValue.ConvertToShortInt(), txtContractorNameSix.Text, txtCtrContactNameSix.Text, Phone, txtWorkCompletedSix.Text, null, false, null, ddlRefSpeaksEnglishSix.SelectedValue.ConvertToBool(), ddlCountryReferencesSix.SelectedValue.ConvertToShortInt());//006 //007
            }
            ResetProjectReferenceValues(trReferencesSix, txtProjectNameSix, txtCitySix, txtStateReferencesSix, txtApproximateContractAmtSix, ddlProjectReferenceTypeSix, txtContractorNameSix, txtCtrContactNameSix, txtWorkCompletedSix, txtSixthPhnoOne, txtSixthPhnoTwo, ddlStateReferencesSix, ddlYear6, ddlMonth6, null, null, txtSixthPhnoThree);
        }
        if (trReferencesSeven.Style["display"] == "")
        {
            State = ddlStateReferencesSeven.SelectedIndex > 0 ? Convert.ToInt16(ddlStateReferencesSeven.SelectedItem.Value) : (byte)0;
            Phone = null;
            if (!((string.IsNullOrEmpty(txtSeventhPhnoOne.Text)) && (string.IsNullOrEmpty(txtSeventhPhnoTwo.Text)) && (string.IsNullOrEmpty(txtSeventhPhnoThree.Text))))
            {
                if (txtSeventhPhnoOne.Text.Length + txtSeventhPhnoTwo.Text.Length + txtSeventhPhnoThree.Text.Length > 0)
                {
                    Phone = objCommon.Phoneformat(txtSeventhPhnoOne.Text, txtSeventhPhnoTwo.Text, txtSeventhPhnoThree.Text, txtSeventhPhnoThree);//006
                }
            }
            CompletionDate = null;
            if (ddlYear6.SelectedIndex > 0 && ddlMonth6.SelectedIndex > 0)
            {
                CompletionDate = ddlMonth6.SelectedItem.Text + "/" + ddlYear6.SelectedItem.Text;
            }
            if (!(string.IsNullOrEmpty(txtProjectNameSeven.Text) && string.IsNullOrEmpty(txtCitySeven.Text) && State == 0 && string.IsNullOrEmpty(CompletionDate) && string.IsNullOrEmpty(txtApproximateContractAmtSeven.Text) && string.IsNullOrEmpty(txtCtrContactNameSeven.Text) && string.IsNullOrEmpty(Phone) && string.IsNullOrEmpty(txtWorkCompletedSeven.Text)))
            {
                objReferences.insertProjectReferences(References, txtProjectNameSeven.Text, txtCitySeven.Text, State, CompletionDate, txtStateReferencesSeven.Text, txtApproximateContractAmtSeven.Text, ddlProjectReferenceTypeSeven.SelectedValue.ConvertToShortInt(), txtContractorNameSeven.Text, txtCtrContactNameSeven.Text, Phone, txtWorkCompletedSeven.Text, null, false, null, ddlRefSpeaksEnglishSeven.SelectedValue.ConvertToBool(), ddlCountryReferencesSeven.SelectedValue.ConvertToShortInt());//006 //007
            }
            ResetProjectReferenceValues(trReferencesSeven, txtProjectNameSeven, txtCitySeven, txtStateReferencesSeven, txtApproximateContractAmtSeven, ddlProjectReferenceTypeSeven, txtContractorNameSeven, txtCtrContactNameSeven, txtWorkCompletedSeven, txtSeventhPhnoOne, txtSeventhPhnoTwo, ddlStateReferencesSeven, ddlYear7, ddlMonth7, null, null, txtSeventhPhnoThree);
        }
        if (trReferencesEight.Style["display"] == "")
        {
            State = ddlStateReferencesEight.SelectedIndex > 0 ? Convert.ToInt16(ddlStateReferencesEight.SelectedItem.Value) : (byte)0;
            Phone = null;
            if (!((string.IsNullOrEmpty(txtEighthPhnoOne.Text)) && (string.IsNullOrEmpty(txtEighthPhnoTwo.Text)) && (string.IsNullOrEmpty(txtEighthPhnoThree.Text))))
            {
                if (txtEighthPhnoOne.Text.Length + txtEighthPhnoTwo.Text.Length + txtEighthPhnoThree.Text.Length > 0)
                {
                    Phone = objCommon.Phoneformat(txtEighthPhnoOne.Text, txtEighthPhnoTwo.Text, txtEighthPhnoThree.Text, txtEighthPhnoThree);//006
                }
            }
            CompletionDate = null;
            if (ddlYear8.SelectedIndex > 0 && ddlMonth8.SelectedIndex > 0)
            {
                CompletionDate = ddlMonth8.SelectedItem.Text + "/" + ddlYear8.SelectedItem.Text;
            }
            if (!(string.IsNullOrEmpty(txtProjectNameEight.Text) && string.IsNullOrEmpty(txtCityEight.Text) && State == 0 && string.IsNullOrEmpty(CompletionDate) && string.IsNullOrEmpty(txtApproximateContractAmtEight.Text) && string.IsNullOrEmpty(txtCtrContactNameEight.Text) && string.IsNullOrEmpty(Phone) && string.IsNullOrEmpty(txtWorkCompletedEight.Text)))
            {
                objReferences.insertProjectReferences(References, txtProjectNameEight.Text, txtCityEight.Text, State, CompletionDate, txtStateRefencesEight.Text, txtApproximateContractAmtEight.Text, ddlProjectReferenceTypeEight.SelectedValue.ConvertToShortInt(), txtContractorNameEight.Text, txtCtrContactNameEight.Text, Phone, txtWorkCompletedEight.Text, null, false, null, ddlRefSpeaksEnglishEight.SelectedValue.ConvertToBool(), ddlCountryReferencesEight.SelectedValue.ConvertToShortInt());//006 //007
            }
            ResetProjectReferenceValues(trReferencesEight, txtProjectNameEight, txtCityEight, txtStateRefencesEight, txtApproximateContractAmtEight, ddlProjectReferenceTypeEight, txtContractorNameEight, txtCtrContactNameEight, txtWorkCompletedEight, txtEighthPhnoOne, txtEighthPhnoTwo, ddlStateReferencesEight, ddlYear8, ddlMonth8, null, null, txtEighthPhnoThree);
        }
        if (trReferencesNine.Style["display"] == "")
        {
            State = ddlStateReferencesNine.SelectedIndex > 0 ? Convert.ToInt16(ddlStateReferencesNine.SelectedItem.Value) : (byte)0;
            Phone = null;
            if (!((string.IsNullOrEmpty(txtNinthPhnoOne.Text)) && (string.IsNullOrEmpty(txtNinthPhnoTwo.Text)) && (string.IsNullOrEmpty(txtNinthPhnoThree.Text))))
            {
                if (txtNinthPhnoOne.Text.Length + txtNinthPhnoTwo.Text.Length + txtNinthPhnoThree.Text.Length > 0)
                {
                    Phone = objCommon.Phoneformat(txtNinthPhnoOne.Text, txtNinthPhnoTwo.Text, txtNinthPhnoThree.Text, txtNinthPhnoThree);//006
                }
            }
            CompletionDate = null;
            if (ddlYear9.SelectedIndex > 0 && ddlMonth9.SelectedIndex > 0)
            {
                CompletionDate = ddlMonth9.SelectedItem.Text + "/" + ddlYear9.SelectedItem.Text;
            }
            if (!(string.IsNullOrEmpty(txtProjectNameNine.Text) && string.IsNullOrEmpty(txtCityNine.Text) && State == 0 && string.IsNullOrEmpty(CompletionDate) && string.IsNullOrEmpty(txtApproximateContractAmtnine.Text) && string.IsNullOrEmpty(txtContractorNameNine.Text) && string.IsNullOrEmpty(Phone) && string.IsNullOrEmpty(txtWorkCompletedNinth.Text)))
            {
                objReferences.insertProjectReferences(References, txtProjectNameNine.Text, txtCityNine.Text, State, CompletionDate, txtStateReferencesNine.Text, txtApproximateContractAmtnine.Text, ddlProjectReferenceTypeNine.SelectedValue.ConvertToShortInt(), txtContractorNameNine.Text, txtCtrContactNameNinth.Text, Phone, txtWorkCompletedNinth.Text, null, false, null, ddlRefSpeaksEnglishNine.SelectedValue.ConvertToBool(), ddlCountryReferencesNine.SelectedValue.ConvertToShortInt());//006 //007
            }
            ResetProjectReferenceValues(trReferencesNine, txtProjectNameNine, txtCityNine, txtStateReferencesNine, txtApproximateContractAmtnine, ddlProjectReferenceTypeNine, txtContractorNameNine, txtCtrContactNameNinth, txtWorkCompletedNinth, txtNinthPhnoOne, txtNinthPhnoTwo, ddlStateReferencesNine, ddlYear9, ddlMonth9, null, null, txtNinthPhnoThree);
        }
        if (trReferencesTen.Style["display"] == "")
        {
            State = ddlStateReferencesTen.SelectedIndex > 0 ? Convert.ToInt16(ddlStateReferencesTen.SelectedItem.Value) : (byte)0;
            Phone = null;
            if (!((string.IsNullOrEmpty(txtTenthPhnoOne.Text)) && (string.IsNullOrEmpty(txtTenthPhnoTwo.Text)) && (string.IsNullOrEmpty(txtTenthPhnoThree.Text))))
            {
                if (txtTenthPhnoOne.Text.Length + txtTenthPhnoTwo.Text.Length + txtTenthPhnoThree.Text.Length > 0)
                {
                    Phone = objCommon.Phoneformat(txtTenthPhnoOne.Text, txtTenthPhnoTwo.Text, txtTenthPhnoThree.Text, txtTenthPhnoThree);//006
                }
            }
            CompletionDate = null;
            if (ddlYear10.SelectedIndex > 0 && ddlMonth10.SelectedIndex > 0)
            {
                CompletionDate = ddlMonth10.SelectedItem.Text + "/" + ddlYear10.SelectedItem.Text;
            }
            if (!(string.IsNullOrEmpty(txtProjectNameTen.Text) && string.IsNullOrEmpty(txtCityTen.Text) && State == 0 && string.IsNullOrEmpty(CompletionDate) && string.IsNullOrEmpty(txtApproximateContractAmtTen.Text) && string.IsNullOrEmpty(txtContractorNameTen.Text) && string.IsNullOrEmpty(Phone) && string.IsNullOrEmpty(txtWorkCompletedTen.Text)))
            {
                objReferences.insertProjectReferences(References, txtProjectNameTen.Text, txtCityTen.Text, State, CompletionDate, txtStateReferencesTen.Text, txtApproximateContractAmtTen.Text, ddlProjectReferenceTypeTen.SelectedValue.ConvertToShortInt(), txtContractorNameTen.Text, txtCtrContactNameTen.Text, Phone, txtWorkCompletedTen.Text, null, false, null, ddlRefSpeaksEnglishTen.SelectedValue.ConvertToBool(), ddlCountryReferencesTen.SelectedValue.ConvertToShortInt());//006 //007
            }
            ResetProjectReferenceValues(trReferencesTen, txtProjectNameTen, txtCityTen, txtStateReferencesTen, txtApproximateContractAmtTen, ddlProjectReferenceTypeTen, txtContractorNameTen, txtCtrContactNameTen, txtWorkCompletedTen, txtTenthPhnoOne, txtTenthPhnoTwo, ddlStateReferencesNine, ddlYear10, ddlMonth10, null, null, txtTenthPhnoThree);
        }
    }


    //Insert Addional Project References details
    public void InsertAddionalProjects(long PrimaryId)
    {
        if (trAddProject01.Style["display"] != "none" && rdoHaskellProjectsCompleted.SelectedValue.Equals("True"))
        {
            if (!(string.IsNullOrEmpty(txtHaskellProjectNameOne.Text) && string.IsNullOrEmpty(txtHaskellProjectManagerOne.Text) && string.IsNullOrEmpty(txtYearOne.Text)))
            {
                objReferences.InsertVQFReferencesProject(txtHaskellProjectNameOne.Text.Trim(), txtHaskellProjectManagerOne.Text.Trim(), txtYearOne.Text.Trim(), PrimaryId);
            }
            ResetAdditionalInformation(trAddProject01, txtHaskellProjectNameOne, txtHaskellProjectManagerOne, txtYearOne);
        }
        if (trAddProject02.Style["display"] != "none" && rdoHaskellProjectsCompleted.SelectedValue.Equals("True"))
        {
            if ((!(string.IsNullOrEmpty(txtHaskellProjectNameTwo.Text.Trim()))) || (!(string.IsNullOrEmpty(txtHaskellProjectManagerTwo.Text.Trim()))) || (!(string.IsNullOrEmpty(txtYearTwo.Text.Trim()))))
            {
                objReferences.InsertVQFReferencesProject(txtHaskellProjectNameTwo.Text.Trim(), txtHaskellProjectManagerTwo.Text.Trim(), txtYearTwo.Text.Trim(), PrimaryId);
            }
            ResetAdditionalInformation(trAddProject02, txtHaskellProjectNameTwo, txtHaskellProjectManagerTwo, txtYearTwo);
        }
        if (trAddProject03.Style["display"] != "none" && rdoHaskellProjectsCompleted.SelectedValue.Equals("True"))
        {
            if ((!(string.IsNullOrEmpty(txtHaskellProjectNameThree.Text.Trim()))) || (!(string.IsNullOrEmpty(txtHaskellProjectManagerThree.Text.Trim()))) || (!(string.IsNullOrEmpty(txtYearThree.Text.Trim()))))
            {
                objReferences.InsertVQFReferencesProject(txtHaskellProjectNameThree.Text.Trim(), txtHaskellProjectManagerThree.Text.Trim(), txtYearThree.Text.Trim(), PrimaryId);
            }
            ResetAdditionalInformation(trAddProject03, txtHaskellProjectNameThree, txtHaskellProjectManagerThree, txtYearThree);
        }
        if (trAddProject04.Style["display"] != "none" && rdoHaskellProjectsCompleted.SelectedValue.Equals("True"))
        {
            if ((!(string.IsNullOrEmpty(txtHaskellProjectNameFour.Text.Trim()))) || (!(string.IsNullOrEmpty(txtHaskellProjectManagerFour.Text.Trim()))) || (!(string.IsNullOrEmpty(txtYearFour.Text.Trim()))))
            {
                objReferences.InsertVQFReferencesProject(txtHaskellProjectNameFour.Text.Trim(), txtHaskellProjectManagerFour.Text.Trim(), txtYearFour.Text.Trim(), PrimaryId);
            }
            ResetAdditionalInformation(trAddProject04, txtHaskellProjectNameFour, txtHaskellProjectManagerFour, txtYearFour);
        }
        if (trAddProject05.Style["display"] != "none" && rdoHaskellProjectsCompleted.SelectedValue.Equals("True"))
        {
            if ((!(string.IsNullOrEmpty(txtHaskellProjectNameFive.Text.Trim()))) || (!(string.IsNullOrEmpty(txtHaskellProjectManagerFive.Text.Trim()))) || (!(string.IsNullOrEmpty(txtYearFive.Text.Trim()))))
            {
                objReferences.InsertVQFReferencesProject(txtHaskellProjectNameFive.Text.Trim(), txtHaskellProjectManagerFive.Text.Trim(), txtYearFive.Text.Trim(), PrimaryId);
            }
            ResetAdditionalInformation(trAddProject05, txtHaskellProjectNameFive, txtHaskellProjectManagerFive, txtYearFive);
        }
        if (trAddProject06.Style["display"] != "none" && rdoHaskellProjectsCompleted.SelectedValue.Equals("True"))
        {
            if ((!(string.IsNullOrEmpty(txtHaskellProjectNameSix.Text.Trim()))) || (!(string.IsNullOrEmpty(txtHaskellProjectManagerSix.Text.Trim()))) || (!(string.IsNullOrEmpty(txtYearSix.Text.Trim()))))
            {
                objReferences.InsertVQFReferencesProject(txtHaskellProjectNameSix.Text.Trim(), txtHaskellProjectManagerSix.Text.Trim(), txtYearSix.Text.Trim(), PrimaryId);
            }
            ResetAdditionalInformation(trAddProject06, txtHaskellProjectNameSix, txtHaskellProjectManagerSix, txtYearSix);
        }
        if (trAddProject07.Style["display"] != "none" && rdoHaskellProjectsCompleted.SelectedValue.Equals("True"))
        {
            if ((!(string.IsNullOrEmpty(txtHaskellProjectNameSeven.Text.Trim()))) || (!(string.IsNullOrEmpty(txtHaskellProjectManagerSeven.Text.Trim()))) || (!(string.IsNullOrEmpty(txtYearSeven.Text.Trim()))))
            {
                objReferences.InsertVQFReferencesProject(txtHaskellProjectNameSeven.Text.Trim(), txtHaskellProjectManagerSeven.Text.Trim(), txtYearSeven.Text.Trim(), PrimaryId);
            }
            ResetAdditionalInformation(trAddProject07, txtHaskellProjectNameSeven, txtHaskellProjectManagerSeven, txtYearSeven);
        }
        if (trAddProject08.Style["display"] != "none" && rdoHaskellProjectsCompleted.SelectedValue.Equals("True"))
        {
            if ((!(string.IsNullOrEmpty(txtHaskellProjectNameEight.Text.Trim()))) || (!(string.IsNullOrEmpty(txtHaskellProjectManagerEight.Text.Trim()))) || (!(string.IsNullOrEmpty(txtYearEight.Text.Trim()))))
            {
                objReferences.InsertVQFReferencesProject(txtHaskellProjectNameEight.Text.Trim(), txtHaskellProjectManagerEight.Text.Trim(), txtYearEight.Text.Trim(), PrimaryId);
            }
            ResetAdditionalInformation(trAddProject08, txtHaskellProjectNameEight, txtHaskellProjectManagerEight, txtYearEight);
        }
        if (trAddProject09.Style["display"] != "none" && rdoHaskellProjectsCompleted.SelectedValue.Equals("True"))
        {
            if ((!(string.IsNullOrEmpty(txtHaskellProjectNameNine.Text.Trim()))) || (!(string.IsNullOrEmpty(txtHaskellProjectManagerNine.Text.Trim()))) || (!(string.IsNullOrEmpty(txtYearNine.Text.Trim()))))
            {
                objReferences.InsertVQFReferencesProject(txtHaskellProjectNameNine.Text.Trim(), txtHaskellProjectManagerNine.Text.Trim(), txtYearNine.Text.Trim(), PrimaryId);
            }
            ResetAdditionalInformation(trAddProject09, txtHaskellProjectNameNine, txtHaskellProjectManagerNine, txtYearNine);
        }
        if (trAddProject10.Style["display"] != "none" && rdoHaskellProjectsCompleted.SelectedValue.Equals("True"))
        {
            if ((!(string.IsNullOrEmpty(txtHaskellProjectNameTen.Text.Trim()))) || (!(string.IsNullOrEmpty(txtHaskellProjectManagerTen.Text.Trim()))) || (!(string.IsNullOrEmpty(txtYearTen.Text.Trim()))))
            {
                objReferences.InsertVQFReferencesProject(txtHaskellProjectNameTen.Text.Trim(), txtHaskellProjectManagerTen.Text.Trim(), txtYearTen.Text.Trim(), PrimaryId);
            }
            ResetAdditionalInformation(trAddProject10, txtHaskellProjectNameTen, txtHaskellProjectManagerTen, txtYearTen);
        }
    }

    //To check the ZIP length
    public string CheckZIP(string ZIP)
    {
        return ZIP;

        //string ZIP1 = string.Empty;
        //if (ZIP.Length == 5)
        //{
        //    ZIP1 = ZIP;
        //}
        //else
        //{
        //    ZIP1 = "";
        //}
        //return ZIP1;
    }

    //Insert Supplier References details
    public void InsertSupplierReferences(long PrimaryId)
    {
        if (ddlStateFour.SelectedIndex > 0)
        {
            ViewState["StateSelectedValue1"] = ddlStateFour.SelectedValue;
        }
        string mainPhoneOne = objCommon.CheckPhone(txtSuplMainPhoneOne.Text.Trim(), txtSuplMainPhoneTwo.Text.Trim(), txtSuplMainPhoneThree.Text.Trim(), txtSuplMainPhoneThree); //006

        if ((!string.IsNullOrEmpty(txtCompanyNameOne.Text.Trim())) || (!string.IsNullOrEmpty(txtContactNameOne.Text.Trim()))
                        || (!string.IsNullOrEmpty(txtSuplCityFour.Text.Trim())) || (ddlStateFour.SelectedIndex > 0)
                        || (!string.IsNullOrEmpty(txtCompanyAddressOne.Text.Trim())) || (!string.IsNullOrEmpty(txtSuplZipOne.Text.Trim()))
                        || (!string.IsNullOrEmpty(mainPhoneOne)))
        {
            objReferences.InsertVQFReferencesSupplier(txtCompanyNameOne.Text.Trim(), txtContactNameOne.Text.Trim(), txtSuplCityFour.Text.Trim(),
                          Convert.ToInt16(ddlStateFour.SelectedValue), txtOtherStateFour.Text.Trim(), txtCompanyAddressOne.Text.Trim(),
                          CheckZIP(txtSuplZipOne.Text.Trim()), mainPhoneOne, PrimaryId, ddlCountryFour.SelectedValue.ConvertToShortInt(), ddlSupSpeaksEnglishOne.SelectedValue.ConvertToBool()); //005-Sooraj , //007
        }

        string mainPhoneTwo = objCommon.CheckPhone(txtSuplSecondPhoneOne.Text.Trim(), txtSuplSecondPhoneTwo.Text.Trim(), txtSuplSecondPhoneThree.Text.Trim(), txtSuplSecondPhoneThree); //006
        if ((!string.IsNullOrEmpty(txtCompanyNameTwo.Text.Trim())) || (!string.IsNullOrEmpty(txtContactNameTwo.Text.Trim()))
                    || (!string.IsNullOrEmpty(txtSuplCityFive.Text.Trim())) || (ddlStateFive.SelectedIndex > 0)
                    || (!string.IsNullOrEmpty(txtCompanyAddressTwo.Text.Trim())) || (!string.IsNullOrEmpty(txtSuplZipTwo.Text.Trim()))
                    || (!string.IsNullOrEmpty(mainPhoneTwo)))
        {
            objReferences.InsertVQFReferencesSupplier(txtCompanyNameTwo.Text.Trim(), txtContactNameTwo.Text.Trim(), txtSuplCityFive.Text.Trim(),
                                                      Convert.ToInt16(ddlStateFive.SelectedValue), txtOtherStateFive.Text.Trim(), txtCompanyAddressTwo.Text.Trim(),
                                                      CheckZIP(txtSuplZipTwo.Text.Trim()), mainPhoneTwo, PrimaryId, ddlCountryFive.SelectedValue.ConvertToShortInt(), ddlSupSpeaksEnglishTwo.SelectedValue.ConvertToBool()); //005-Sooraj , //007
        }

        string mainPhoneThree = objCommon.CheckPhone(txtSuplThirdPhoneOne.Text.Trim(), txtSuplThirdPhoneTwo.Text.Trim(), txtSuplThirdPhoneThree.Text.Trim(), txtSuplThirdPhoneThree); //006
        if ((!string.IsNullOrEmpty(txtCompanyNameThree.Text.Trim())) || (!string.IsNullOrEmpty(txtContactNameThree.Text.Trim()))
                    || (!string.IsNullOrEmpty(txtSuplCitySix.Text.Trim())) || (ddlStateSix.SelectedIndex > 0)
                    || (!string.IsNullOrEmpty(txtCompanyAddressThree.Text.Trim())) || (!string.IsNullOrEmpty(txtSuplZipThree.Text.Trim()))
                    || (!string.IsNullOrEmpty(mainPhoneThree)))
        {
            objReferences.InsertVQFReferencesSupplier(txtCompanyNameThree.Text.Trim(), txtContactNameThree.Text.Trim(), txtSuplCitySix.Text.Trim(),
                                                      Convert.ToInt16(ddlStateSix.SelectedValue), txtOtherStateSix.Text.Trim(), txtCompanyAddressThree.Text.Trim(),
                                                      CheckZIP(txtSuplZipThree.Text.Trim()), mainPhoneThree, PrimaryId, ddlCountrySix.SelectedValue.ConvertToShortInt(), ddlSupSpeaksEnglishThree.SelectedValue.ConvertToBool()); //005-Sooraj , //007
        }

        //link1
        if (spnmore1.Style["display"] == "")
        {
            string mainPhoneFour = objCommon.CheckPhone(txtSuplFourthPhoneOne.Text.Trim(), txtSuplFourthPhoneTwo.Text.Trim(), txtSuplFourthPhoneThree.Text.Trim(), txtSuplFourthPhoneThree); //006
            if ((!string.IsNullOrEmpty(txtCompanyNameFour.Text.Trim())) || (!string.IsNullOrEmpty(txtContactNameFour.Text.Trim()))
                        || (!string.IsNullOrEmpty(txtSuplCitySeven.Text.Trim())) || (ddlStateSeven.SelectedIndex > 0)
                        || (!string.IsNullOrEmpty(txtCompanyAddressFour.Text.Trim())) || (!string.IsNullOrEmpty(txtSuplZipFour.Text.Trim()))
                        || (!string.IsNullOrEmpty(mainPhoneFour)))
            {
                objReferences.InsertVQFReferencesSupplier(txtCompanyNameFour.Text.Trim(), txtContactNameFour.Text.Trim(), txtSuplCitySeven.Text.Trim(),
                                                    Convert.ToInt16(ddlStateSeven.SelectedValue), txtOtherStateSeven.Text.Trim(), txtCompanyAddressFour.Text.Trim(),
                                                    CheckZIP(txtSuplZipFour.Text.Trim()), mainPhoneFour, PrimaryId, ddlCountrySeven.SelectedValue.ConvertToShortInt(), ddlSupSpeaksEnglishFour.SelectedValue.ConvertToBool()); //005-Sooraj , //007
            }
            ResetSupplierReferenceValues(spnmore1, txtCompanyNameFour, txtCompanyAddressFour, txtSuplCitySeven, ddlStateSeven, txtSuplZipFour, txtContactNameFour,
                                    txtSuplFourthPhoneOne, txtSuplFourthPhoneTwo, txtSuplFourthPhoneThree, txtOtherStateSeven, trOther7, regvSuplZipFour, ftxtSuplZipFour);
        }
        //link2
        if (spnmore2.Style["display"] == "")
        {
            string mainPhoneFive = objCommon.CheckPhone(txtSuplFivePhoneOne.Text.Trim(), txtSuplFivePhoneTwo.Text.Trim(), txtSuplFivePhoneThree.Text.Trim(), txtSuplFivePhoneThree); //006
            if ((!string.IsNullOrEmpty(txtCompanyNameFive.Text.Trim())) || (!string.IsNullOrEmpty(txtContactNameFive.Text.Trim()))
                        || (!string.IsNullOrEmpty(txtSuplCityEight.Text.Trim())) || (ddlStateEight.SelectedIndex > 0)
                        || (!string.IsNullOrEmpty(txtCompanyAddressFive.Text.Trim())) || (!string.IsNullOrEmpty(txtSuplZipFive.Text.Trim()))
                        || (!string.IsNullOrEmpty(mainPhoneFive)))
            {
                objReferences.InsertVQFReferencesSupplier(txtCompanyNameFive.Text.Trim(), txtContactNameFive.Text.Trim(), txtSuplCityEight.Text.Trim(),
                                                    Convert.ToInt16(ddlStateEight.SelectedValue), txtOtherStateEight.Text.Trim(), txtCompanyAddressFive.Text.Trim(),
                                                    CheckZIP(txtSuplZipFive.Text.Trim()), mainPhoneFive, PrimaryId, ddlCountryEight.SelectedValue.ConvertToShortInt(), ddlSupSpeaksEnglishFive.SelectedValue.ConvertToBool()); //005-Sooraj , //007
            }
            ResetSupplierReferenceValues(spnmore2, txtCompanyNameFive, txtCompanyAddressFive, txtSuplCityEight, ddlStateEight, txtSuplZipFive, txtContactNameFive,
                                    txtSuplFivePhoneOne, txtSuplFivePhoneTwo, txtSuplFivePhoneThree, txtOtherStateEight, trOther8, regvSuplZipFive, ftxtSuplZipFive);
        }
        //link3
        if (spnmore3.Style["display"] == "")
        {
            string mainPhoneSix = objCommon.CheckPhone(txtSuplSixPhoneOne.Text.Trim(), txtSuplSixPhoneTwo.Text.Trim(), txtSuplSixPhoneThree.Text.Trim(), txtSuplSixPhoneThree); //006
            if ((!string.IsNullOrEmpty(txtCompanyNameSix.Text.Trim())) || (!string.IsNullOrEmpty(txtContactNameSix.Text.Trim()))
                        || (!string.IsNullOrEmpty(txtSuplCityNine.Text.Trim())) || (ddlStateNine.SelectedIndex > 0)
                        || (!string.IsNullOrEmpty(txtCompanyAddressSix.Text.Trim())) || (!string.IsNullOrEmpty(txtSuplZipSix.Text.Trim()))
                        || (!string.IsNullOrEmpty(mainPhoneSix)))
            {
                objReferences.InsertVQFReferencesSupplier(txtCompanyNameSix.Text.Trim(), txtContactNameSix.Text.Trim(), txtSuplCityNine.Text.Trim(),
                                                    Convert.ToInt16(ddlStateNine.SelectedValue), txtOtherStateNine.Text.Trim(), txtCompanyAddressSix.Text.Trim(),
                                                    CheckZIP(txtSuplZipSix.Text.Trim()), mainPhoneSix, PrimaryId, ddlCountryNine.SelectedValue.ConvertToShortInt(), ddlSupSpeaksEnglishSix.SelectedValue.ConvertToBool()); //005-Sooraj , //007
            }
            ResetSupplierReferenceValues(spnmore3, txtCompanyNameSix, txtCompanyAddressSix, txtSuplCityNine, ddlStateNine, txtSuplZipSix, txtContactNameSix,
                                    txtSuplSixPhoneOne, txtSuplSixPhoneTwo, txtSuplSixPhoneThree, txtOtherStateNine, trOther9, regvSuplZipSix, ftxtSuplZipSix);
        }
        //link4
        if (spnmore4.Style["display"] == "")
        {
            string mainPhoneSeven = objCommon.CheckPhone(txtSuplSevenPhoneOne.Text.Trim(), txtSuplSevenPhoneTwo.Text.Trim(), txtSuplSevenPhoneThree.Text.Trim(), txtSuplSevenPhoneThree); //006
            if ((!string.IsNullOrEmpty(txtCompanyNameSeven.Text.Trim())) || (!string.IsNullOrEmpty(txtContactNameSeven.Text.Trim()))
                        || (!string.IsNullOrEmpty(txtSuplCityTen.Text.Trim())) || (ddlStateTen.SelectedIndex > 0)
                        || (!string.IsNullOrEmpty(txtCompanyAddressSeven.Text.Trim())) || (!string.IsNullOrEmpty(txtSuplZipSeven.Text.Trim()))
                        || (!string.IsNullOrEmpty(mainPhoneSeven)))
            {
                objReferences.InsertVQFReferencesSupplier(txtCompanyNameSeven.Text.Trim(), txtContactNameSeven.Text.Trim(), txtSuplCityTen.Text.Trim(),
                                                     Convert.ToInt16(ddlStateTen.SelectedValue), txtOtherStateTen.Text.Trim(), txtCompanyAddressSeven.Text.Trim(),
                                                     CheckZIP(txtSuplZipSeven.Text.Trim()), mainPhoneSeven, PrimaryId, ddlCountryTen.SelectedValue.ConvertToShortInt(), ddlSupSpeaksEnglishSeven.SelectedValue.ConvertToBool()); //005-Sooraj , //007
            }
            ResetSupplierReferenceValues(spnmore4, txtCompanyNameSeven, txtCompanyAddressSeven, txtSuplCityTen, ddlStateTen, txtSuplZipSeven, txtContactNameSeven,
                                    txtSuplSevenPhoneOne, txtSuplSevenPhoneTwo, txtSuplSevenPhoneThree, txtOtherStateTen, trOther10, regvSuplZipSeven, ftxtSuplZipSeven);
        }
        //link5
        if (spnmore5.Style["display"] == "")
        {
            string mainPhoneEight = objCommon.CheckPhone(txtSuplEightPhoneOne.Text.Trim(), txtSuplEightPhoneTwo.Text.Trim(), txtSuplEightPhoneThree.Text.Trim(), txtSuplEightPhoneThree); //006
            if ((!string.IsNullOrEmpty(txtCompanyNameEight.Text.Trim())) || (!string.IsNullOrEmpty(txtContactNameEight.Text.Trim()))
                        || (!string.IsNullOrEmpty(txtSuplCityEleven.Text.Trim())) || (ddlStateEleven.SelectedIndex > 0)
                        || (!string.IsNullOrEmpty(txtCompanyAddressEight.Text.Trim())) || (!string.IsNullOrEmpty(txtSuplZipEight.Text.Trim()))
                        || (!string.IsNullOrEmpty(mainPhoneEight)))
            {
                objReferences.InsertVQFReferencesSupplier(txtCompanyNameEight.Text.Trim(), txtContactNameEight.Text.Trim(), txtSuplCityEleven.Text.Trim(),
                                                    Convert.ToInt16(ddlStateEleven.SelectedValue), txtOtherStateEleven.Text.Trim(), txtCompanyAddressEight.Text.Trim(),
                                                    CheckZIP(txtSuplZipEight.Text.Trim()), mainPhoneEight, PrimaryId, ddlCountryEleven.SelectedValue.ConvertToShortInt(), ddlSupSpeaksEnglishEight.SelectedValue.ConvertToBool()); //005-Sooraj , //007
            }
            ResetSupplierReferenceValues(spnmore5, txtCompanyNameEight, txtCompanyAddressEight, txtSuplCityEleven, ddlStateEleven, txtSuplZipEight, txtContactNameEight,
                                    txtSuplEightPhoneOne, txtSuplEightPhoneTwo, txtSuplEightPhoneThree, txtOtherStateEleven, trOther11, regvSuplZipEight, ftxtSuplZipEight);
        }
        //link6
        if (spnmore6.Style["display"] == "")
        {
            string mainPhoneNine = objCommon.CheckPhone(txtSuplNinePhoneOne.Text.Trim(), txtSuplNinePhoneTwo.Text.Trim(), txtSuplNinePhoneThree.Text.Trim(), txtSuplNinePhoneThree); //006
            if ((!string.IsNullOrEmpty(txtCompanyNameNine.Text.Trim())) || (!string.IsNullOrEmpty(txtContactNameNine.Text.Trim()))
                        || (!string.IsNullOrEmpty(txtSuplCityTwelve.Text.Trim())) || (ddlStateTwelve.SelectedIndex > 0)
                        || (!string.IsNullOrEmpty(txtCompanyAddressNine.Text.Trim())) || (!string.IsNullOrEmpty(txtSuplZipNine.Text.Trim()))
                        || (!string.IsNullOrEmpty(mainPhoneNine)))
            {
                objReferences.InsertVQFReferencesSupplier(txtCompanyNameNine.Text.Trim(), txtContactNameNine.Text.Trim(), txtSuplCityTwelve.Text.Trim(),
                               Convert.ToInt16(ddlStateTwelve.SelectedValue), txtOtherStateTwelve.Text.Trim(), txtCompanyAddressNine.Text.Trim(),
                               CheckZIP(txtSuplZipNine.Text.Trim()), mainPhoneNine, PrimaryId, ddlCountryTwelve.SelectedValue.ConvertToShortInt(), ddlSupSpeaksEnglishNine.SelectedValue.ConvertToBool()); //005-Sooraj , //007
            }
            ResetSupplierReferenceValues(spnmore6, txtCompanyNameNine, txtCompanyAddressNine, txtSuplCityTwelve, ddlStateTwelve, txtSuplZipNine, txtContactNameNine,
                                     txtSuplNinePhoneOne, txtSuplNinePhoneTwo, txtSuplNinePhoneThree, txtOtherStateTwelve, trOther12, regvSuplZipNine, ftxtSuplZipNine);
        }
        //link7
        if (spnmore7.Style["display"] == "")
        {
            string mainPhoneTen = objCommon.CheckPhone(txtSuplTenPhoneOne.Text.Trim(), txtSuplTenPhoneTwo.Text.Trim(), txtSuplTenPhoneThree.Text.Trim(), txtSuplTenPhoneThree); //006
            if ((!string.IsNullOrEmpty(txtCompanyNameTen.Text.Trim())) || (!string.IsNullOrEmpty(txtContactNameTen.Text.Trim()))
                        || (!string.IsNullOrEmpty(txtSuplCityThirteen.Text.Trim())) || (ddlStateThirteen.SelectedIndex > 0)
                        || (!string.IsNullOrEmpty(txtCompanyAddressTen.Text.Trim())) || (!string.IsNullOrEmpty(txtSuplZipTen.Text.Trim()))
                        || (!string.IsNullOrEmpty(mainPhoneTen)))
            {
                objReferences.InsertVQFReferencesSupplier(txtCompanyNameTen.Text.Trim(), txtContactNameTen.Text.Trim(), txtSuplCityThirteen.Text.Trim(),
                              Convert.ToInt16(ddlStateThirteen.SelectedValue), txtOtherStateThirteen.Text.Trim(), txtCompanyAddressTen.Text.Trim(),
                              CheckZIP(txtSuplZipTen.Text.Trim()), mainPhoneTen, PrimaryId, ddlCountryThirteen.SelectedValue.ConvertToShortInt(), ddlSupSpeaksEnglishTen.SelectedValue.ConvertToBool()); //005-Sooraj , //007
            }
            ResetSupplierReferenceValues(spnmore7, txtCompanyNameTen, txtCompanyAddressTen, txtSuplCityThirteen, ddlStateThirteen, txtSuplZipTen, txtContactNameTen,
                                    txtSuplTenPhoneOne, txtSuplTenPhoneTwo, txtSuplTenPhoneThree, txtOtherStateThirteen, trOther12, regvSuplZipTen, ftxtSuplZipTen);
        }
    }

    //To Show Validation's  Message
    private string Errormsg()
    {
        string errorMsg = String.Empty;
        int count = 0;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                count++;
                errorMsg += "<li>" + validator.ErrorMessage + "</li>";
            }
        }
        return errorMsg + "|" + count;
    }

    //To check valid date
    protected bool DateValidation(TextBox txt1)
    {
        bool validDate = false;
        DateTime dt;
        if (!string.IsNullOrEmpty(txt1.Text))
        {
            char[] splitter = { '/' };
            string[] arInfo = new string[3];
            string info = txt1.Text;
            arInfo = info.Split(splitter);
            if (!(arInfo.Length < 3))
            {
                string test = arInfo[2];
                if (!(test.Length < 4 || test.Length > 4))
                {
                    if (!(DateTime.TryParse(txt1.Text, out dt)))
                    {
                        validDate = false;
                    }
                    else
                    {
                        validDate = true;
                    }
                }
                else
                {
                    validDate = false;
                }
            }
            else
            {
                validDate = false;
            }
        }
        return validDate;
    }

    // To load editable datas
    private void LoadEditDatas(bool IsReferences)
    {
        int refCount = objReferences.GetRefCount(Convert.ToInt32(Session["VendorId"]));
        if (refCount > 0)
        {
            Moddata = objReferences.GetSingleVQFReferenceData(Convert.ToInt64(Session["VendorId"]));
            hdnprjreferenceid.Value = Convert.ToString(Moddata.PK_ReferencesID);
            long RefId = Moddata.PK_ReferencesID;

            if (!string.IsNullOrEmpty(Convert.ToString(Moddata.CompletedProjects)))
            {
                rdoHaskellProjectsCompleted.SelectedValue = Convert.ToString(Moddata.CompletedProjects);
            }
            if (Convert.ToString(rdoHaskellProjectsCompleted.SelectedValue).Equals("True"))
            {
                tdlnkbtnaddproject.Style["display"] = "";
                lnladdProject.Style["display"] = "";
            }
            else
            {
                tdlnkbtnaddproject.Style["display"] = "none";
                lnladdProject.Style["display"] = "none";
            }




            #region "Commented"
            /* //003-sooraj commented : same DB call for each column its unwanted 
          
          int countReferences = objReferences.GetProjectReferences(RefId).Count();
             if (countReferences > 0)
            {
                string CompletionDate;
                hdnAdditionalReferencesCount.Value = Convert.ToString(countReferences);
                for (int i = 1; i <= countReferences; i++)
                {
                    switch (i)
                    {
                        case 1:
                            txtProjectNameOne.Text = objReferences.GetProjectReferences(RefId)[i - 1].ProjectName;
                            txtWorkCompletedOne.Text = objReferences.GetProjectReferences(RefId)[i - 1].WorkCompleted;
                            txtCity.Text = objReferences.GetProjectReferences(RefId)[i - 1].City;
                            ddlStateOne.SelectedValue = Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State);
                            if (Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State).Equals("51"))
                            {
                                trOther1.Style["display"] = "";
                                txtOtherStateOne.Text = objReferences.GetProjectReferences(RefId)[i - 1].OtherState;
                            }
                            CompletionDate = objReferences.GetProjectReferences(RefId)[i - 1].CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear1.SelectedValue = Month[1];
                                    ddlMonth1.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtOne.Text = objReferences.GetProjectReferences(RefId)[i - 1].ApproximateContract;
                            txtContractorNameOne.Text = objReferences.GetProjectReferences(RefId)[i - 1].GeneralContractor;
                            txtCtrContactNameOne.Text = objReferences.GetProjectReferences(RefId)[i - 1].ContactName;
                            chkNA1.Checked = Convert.ToBoolean(objReferences.GetProjectReferences(RefId)[i - 1].NotAvailable);
                            trExplain1.Style["Display"] = chkNA1.Checked ? "" : "none";

                            txtExplain1.Text = objReferences.GetProjectReferences(RefId)[i - 1].Explanation;
                            objCommon.loadphone(txtMainPhnoOne, txtMainPhnoTwo, txtMainPhnoThree, objReferences.GetProjectReferences(RefId)[i - 1].ContactPhoneNumber);
                            break;
                        case 2:
                            txtProjectNameTwo.Text = objReferences.GetProjectReferences(RefId)[i - 1].ProjectName;
                            txtWorkCompletedTwo.Text = objReferences.GetProjectReferences(RefId)[i - 1].WorkCompleted;
                            txtCityTwo.Text = objReferences.GetProjectReferences(RefId)[i - 1].City;
                            ddlStateTwo.SelectedValue = Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State);
                            CompletionDate = objReferences.GetProjectReferences(RefId)[i - 1].CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear2.SelectedValue = Month[1];
                                    ddlMonth2.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtTwo.Text = objReferences.GetProjectReferences(RefId)[i - 1].ApproximateContract;
                            txtContractorNameTwo.Text = objReferences.GetProjectReferences(RefId)[i - 1].GeneralContractor;
                            txtCtrContactNameTwo.Text = objReferences.GetProjectReferences(RefId)[i - 1].ContactName;
                            chkNA2.Checked = Convert.ToBoolean(objReferences.GetProjectReferences(RefId)[i - 1].NotAvailable);
                            trExplain2.Style["display"] = chkNA2.Checked ? "" : "none";
                            
                            txtExplain2.Text = objReferences.GetProjectReferences(RefId)[i - 1].Explanation;
                            objCommon.loadphone(txtSecondPhnoOne, txtSecondPhnoTwo, txtSecondPhnoThree, objReferences.GetProjectReferences(RefId)[i - 1].ContactPhoneNumber);
                            if (Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State).Equals("51"))
                            {
                                trOther2.Style["display"] = "";
                                txtOtherStateTwo.Text = objReferences.GetProjectReferences(RefId)[i - 1].OtherState;
                            }
                            break;
                        case 3:
                            txtProjectNameThree.Text = objReferences.GetProjectReferences(RefId)[i - 1].ProjectName;
                            txtWorkCompletedThree.Text = objReferences.GetProjectReferences(RefId)[i - 1].WorkCompleted;
                            txtCityThree.Text = objReferences.GetProjectReferences(RefId)[i - 1].City;
                            ddlStateThree.SelectedValue = Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State);
                            CompletionDate = objReferences.GetProjectReferences(RefId)[i - 1].CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear3.SelectedValue = Month[1];
                                    ddlMonth3.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtThree.Text = objReferences.GetProjectReferences(RefId)[i - 1].ApproximateContract;
                            txtContractorNameThree.Text = objReferences.GetProjectReferences(RefId)[i - 1].GeneralContractor;
                            txtCtrContactNameThree.Text = objReferences.GetProjectReferences(RefId)[i - 1].ContactName;
                            chkNA3.Checked = Convert.ToBoolean(objReferences.GetProjectReferences(RefId)[i - 1].NotAvailable);
                            trExplain3.Style["Display"] = chkNA3.Checked ? "" : "none";
                            
                            txtExplain3.Text = objReferences.GetProjectReferences(RefId)[i - 1].Explanation;
                            objCommon.loadphone(txtThirdPhnoOne, txtThirdPhnoTwo, txtThirdPhnoThree, objReferences.GetProjectReferences(RefId)[i - 1].ContactPhoneNumber);
                            if (Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State).Equals("51"))
                            {
                                trOther3.Style["display"] = "";
                                txtOtherStateThree.Text = objReferences.GetProjectReferences(RefId)[i - 1].OtherState;
                            }
                            break;
                        case 4:
                            trReferencesFour.Style["display"] = "";
                            txtProjectNameFour.Text = objReferences.GetProjectReferences(RefId)[i - 1].ProjectName;
                            txtWorkCompletedFour.Text = objReferences.GetProjectReferences(RefId)[i - 1].WorkCompleted;
                            txtCityFour.Text = objReferences.GetProjectReferences(RefId)[i - 1].City;
                            ddlStateReferencesFour.SelectedValue = Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State);
                            CompletionDate = objReferences.GetProjectReferences(RefId)[i - 1].CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear4.SelectedValue = Month[1];
                                    ddlMonth4.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtFour.Text = objReferences.GetProjectReferences(RefId)[i - 1].ApproximateContract;
                            txtContractorNameFour.Text = objReferences.GetProjectReferences(RefId)[i - 1].GeneralContractor;
                            txtCtrContactNameFour.Text = objReferences.GetProjectReferences(RefId)[i - 1].ContactName;
                            objCommon.loadphone(txtFourthPhnoOne, txtFourthPhnoTwo, txtFourthPhnoThree, objReferences.GetProjectReferences(RefId)[i - 1].ContactPhoneNumber);
                            if (Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State).Equals("51"))
                            {
                                trReferencesFour.Style["display"] = "";
                                txtStateReferencesFour.Text = objReferences.GetProjectReferences(RefId)[i - 1].OtherState;
                            }
                            break;
                        case 5:
                            trReferencesFive.Style["display"] = "";
                            txtProjectNameFive.Text = objReferences.GetProjectReferences(RefId)[i - 1].ProjectName;
                            txtWorkCompletedFive.Text = objReferences.GetProjectReferences(RefId)[i - 1].WorkCompleted;
                            txtCityFive.Text = objReferences.GetProjectReferences(RefId)[i - 1].City;
                            ddlStateReferencesFive.SelectedValue = Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State);
                            CompletionDate = objReferences.GetProjectReferences(RefId)[i - 1].CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear5.SelectedValue = Month[1];
                                    ddlMonth5.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtFive.Text = objReferences.GetProjectReferences(RefId)[i - 1].ApproximateContract;
                            txtContractorNameFive.Text = objReferences.GetProjectReferences(RefId)[i - 1].GeneralContractor;
                            txtCtrContactNameFive.Text = objReferences.GetProjectReferences(RefId)[i - 1].ContactName;
                            objCommon.loadphone(txtFifthPhnoOne, txtFifthPhnoTwo, txtFifthPhnoThree, objReferences.GetProjectReferences(RefId)[i - 1].ContactPhoneNumber);
                            if (Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State).Equals("51"))
                            {
                                trReferencesFive.Style["display"] = "";
                                txtStateReferencesFive.Text = objReferences.GetProjectReferences(RefId)[i - 1].OtherState;
                            }
                            break;
                        case 6:
                            trReferencesSix.Style["display"] = "";
                            txtProjectNameSix.Text = objReferences.GetProjectReferences(RefId)[i - 1].ProjectName;
                            txtWorkCompletedSix.Text = objReferences.GetProjectReferences(RefId)[i - 1].WorkCompleted;
                            txtCitySix.Text = objReferences.GetProjectReferences(RefId)[i - 1].City;
                            ddlStateReferencesSix.SelectedValue = Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State);
                            CompletionDate = objReferences.GetProjectReferences(RefId)[i - 1].CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear6.SelectedValue = Month[1];
                                    ddlMonth6.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtSix.Text = objReferences.GetProjectReferences(RefId)[i - 1].ApproximateContract;
                            txtContractorNameSix.Text = objReferences.GetProjectReferences(RefId)[i - 1].GeneralContractor;
                            txtCtrContactNameSix.Text = objReferences.GetProjectReferences(RefId)[i - 1].ContactName;
                            objCommon.loadphone(txtSixthPhnoOne, txtSixthPhnoTwo, txtSixthPhnoThree, objReferences.GetProjectReferences(RefId)[i - 1].ContactPhoneNumber);
                            if (Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State).Equals("51"))
                            {
                                trReferencesSix.Style["display"] = "";
                                txtStateReferencesSix.Text = objReferences.GetProjectReferences(RefId)[i - 1].OtherState;
                            }
                            break;
                        case 7:
                            trReferencesSeven.Style["display"] = "";
                            txtProjectNameSeven.Text = objReferences.GetProjectReferences(RefId)[i - 1].ProjectName;
                            txtWorkCompletedSeven.Text = objReferences.GetProjectReferences(RefId)[i - 1].WorkCompleted;
                            txtCitySeven.Text = objReferences.GetProjectReferences(RefId)[i - 1].City;
                            ddlStateReferencesSeven.SelectedValue = Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State);
                            CompletionDate = objReferences.GetProjectReferences(RefId)[i - 1].CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear7.SelectedValue = Month[1];
                                    ddlMonth7.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtSeven.Text = objReferences.GetProjectReferences(RefId)[i - 1].ApproximateContract;
                            txtContractorNameSeven.Text = objReferences.GetProjectReferences(RefId)[i - 1].GeneralContractor;
                            txtCtrContactNameSeven.Text = objReferences.GetProjectReferences(RefId)[i - 1].ContactName;
                            objCommon.loadphone(txtSeventhPhnoOne, txtSeventhPhnoTwo, txtSeventhPhnoThree, objReferences.GetProjectReferences(RefId)[i - 1].ContactPhoneNumber);
                            if (Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State).Equals("51"))
                            {
                                trReferencesSeven.Style["display"] = "";
                                txtStateReferencesSeven.Text = objReferences.GetProjectReferences(RefId)[i - 1].OtherState;
                            }
                            break;
                        case 8:
                            trReferencesEight.Style["display"] = "";
                             txtProjectNameEight.Text = objReferences.GetProjectReferences(RefId)[i - 1].ProjectName;
                             txtWorkCompletedEight.Text = objReferences.GetProjectReferences(RefId)[i - 1].WorkCompleted;
                             txtCityEight.Text = objReferences.GetProjectReferences(RefId)[i - 1].City;
                             ddlStateReferencesEight.SelectedValue = Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State);
                             CompletionDate = objReferences.GetProjectReferences(RefId)[i - 1].CompletionDT;
                             if (!string.IsNullOrEmpty(CompletionDate))
                             {
                                 string[] Month = CompletionDate.Split('/');
                                 if (Month.Length == 2)
                                 {
                                     ddlYear8.SelectedValue = Month[1];
                                     ddlMonth8.SelectedValue = Month[0];
                                 }
                             }
                             txtApproximateContractAmtEight.Text = objReferences.GetProjectReferences(RefId)[i - 1].ApproximateContract;
                         
                             txtContractorNameEight.Text = objReferences.GetProjectReferences(RefId)[i - 1].GeneralContractor;
                             txtCtrContactNameEight.Text = objReferences.GetProjectReferences(RefId)[i - 1].ContactName;
                              objCommon.loadphone(txtEighthPhnoOne, txtEighthPhnoTwo, txtEighthPhnoThree, objReferences.GetProjectReferences(RefId)[i - 1].ContactPhoneNumber);
                         
                               if (Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State).Equals("51"))
                            {
                                trReferencesEight.Style["display"] = "";
                                txtStateRefencesEight.Text = objReferences.GetProjectReferences(RefId)[i - 1].OtherState;
                            }
                            break;
                        case 9:
                            trReferencesNine.Style["display"] = "";
                            txtProjectNameNine.Text = objReferences.GetProjectReferences(RefId)[i - 1].ProjectName;
                            txtWorkCompletedNinth.Text = objReferences.GetProjectReferences(RefId)[i - 1].WorkCompleted;
                            txtCityNine.Text = objReferences.GetProjectReferences(RefId)[i - 1].City;
                            ddlStateReferencesNine.SelectedValue = Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State);
                            CompletionDate = objReferences.GetProjectReferences(RefId)[i - 1].CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear9.SelectedValue = Month[1];
                                    ddlMonth9.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtnine.Text = objReferences.GetProjectReferences(RefId)[i - 1].ApproximateContract;
                            txtContractorNameNine.Text = objReferences.GetProjectReferences(RefId)[i - 1].GeneralContractor;
                            txtCtrContactNameNinth.Text = objReferences.GetProjectReferences(RefId)[i - 1].ContactName;
                            objCommon.loadphone(txtNinthPhnoOne, txtNinthPhnoTwo, txtNinthPhnoThree, objReferences.GetProjectReferences(RefId)[i - 1].ContactPhoneNumber);
                            if (Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State).Equals("51"))
                            {
                                trReferencesNine.Style["display"] = "";
                                txtStateReferencesNine.Text = objReferences.GetProjectReferences(RefId)[i - 1].OtherState;
                            }
                            break;
                        case 10:
                            trReferencesTen.Style["display"] = "";
                            txtProjectNameTen.Text = objReferences.GetProjectReferences(RefId)[i - 1].ProjectName;
                            txtWorkCompletedTen.Text = objReferences.GetProjectReferences(RefId)[i - 1].WorkCompleted;
                            txtCityTen.Text = objReferences.GetProjectReferences(RefId)[i - 1].City;
                            ddlStateReferencesTen.SelectedValue = Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State);
                            CompletionDate = objReferences.GetProjectReferences(RefId)[i - 1].CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear10.SelectedValue = Month[1];
                                    ddlMonth10.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtTen.Text = objReferences.GetProjectReferences(RefId)[i - 1].ApproximateContract;
                            txtContractorNameTen.Text = objReferences.GetProjectReferences(RefId)[i - 1].GeneralContractor;
                            txtCtrContactNameTen.Text = objReferences.GetProjectReferences(RefId)[i - 1].ContactName;
                            objCommon.loadphone(txtTenthPhnoOne, txtTenthPhnoTwo, txtTenthPhnoThree, objReferences.GetProjectReferences(RefId)[i - 1].ContactPhoneNumber);
                            if (Convert.ToString(objReferences.GetProjectReferences(RefId)[i - 1].FK_State).Equals("51"))
                            {
                                trReferencesTen.Style["display"] = "";
                                txtStateReferencesTen.Text = objReferences.GetProjectReferences(RefId)[i - 1].OtherState;
                            }
                            break;
                    }
                }
                DisplayReferenceNA();
            }
          */
            #endregion

            //002-Sooraj  modified for performance 
            #region "Project Reference"

            var referenceList = objReferences.GetProjectReferences(RefId);
            int countReferences = referenceList.Count();
            DAL.UspGetProjectReferencesResult reference;
            if (countReferences > 0)
            {
                string CompletionDate;
                hdnAdditionalReferencesCount.Value = Convert.ToString(countReferences);
                for (int i = 1; i <= countReferences; i++)
                {
                    reference = referenceList[i - 1];
                    switch (i)
                    {

                        case 1:
                            txtProjectNameOne.Text = reference.ProjectName;
                            txtWorkCompletedOne.Text = reference.WorkCompleted;
                            txtCity.Text = reference.City;

                            ddlCountryOne.SelectedValue = reference.FK_Country.ConvertToString();//005-Sooraj
                            BindAssociateStateRefernce(ddlCountryOne);//005-Sooraj
                            ddlStateOne.SelectedValue = Convert.ToString(reference.FK_State);//005-Sooraj

                            ddlStateOne.SelectedValue = Convert.ToString(reference.FK_State);
                            if (Convert.ToString(reference.FK_State).Equals("51"))
                            {
                                trOther1.Style["display"] = "";
                                txtOtherStateOne.Text = reference.OtherState;
                            }
                            CompletionDate = reference.CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear1.SelectedValue = Month[1];
                                    ddlMonth1.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtOne.Text = reference.ApproximateContract;
                            txtContractorNameOne.Text = reference.GeneralContractor;
                            txtCtrContactNameOne.Text = reference.ContactName;
                            chkNA1.Checked = Convert.ToBoolean(reference.NotAvailable);
                            trExplain1.Style["Display"] = chkNA1.Checked ? "" : "none";

                            txtExplain1.Text = reference.Explanation;
                            objCommon.loadphone(txtMainPhnoOne, txtMainPhnoTwo, txtMainPhnoThree, reference.ContactPhoneNumber);
                            ddlProjectReferenceTypeOne.SelectedValue = reference.FK_ProjectReferenceType.ConvertToString();  //004-sooraj
                            ddlRefSpeaksEnglishOne.SelectedValue = reference.IsSpeaksEnglish.ConvertToString(); //007
                            break;
                        case 2:
                            txtProjectNameTwo.Text = reference.ProjectName;
                            txtWorkCompletedTwo.Text = reference.WorkCompleted;
                            txtCityTwo.Text = reference.City;
                            ddlCountryTwo.SelectedValue = reference.FK_Country.ConvertToString();//005-Sooraj
                            BindAssociateStateRefernce(ddlCountryTwo);//005-Sooraj
                            ddlStateTwo.SelectedValue = Convert.ToString(reference.FK_State);//005-Sooraj
                            CompletionDate = reference.CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear2.SelectedValue = Month[1];
                                    ddlMonth2.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtTwo.Text = reference.ApproximateContract;
                            txtContractorNameTwo.Text = reference.GeneralContractor;
                            txtCtrContactNameTwo.Text = reference.ContactName;
                            chkNA2.Checked = Convert.ToBoolean(reference.NotAvailable);
                            trExplain2.Style["display"] = chkNA2.Checked ? "" : "none";

                            txtExplain2.Text = reference.Explanation;
                            objCommon.loadphone(txtSecondPhnoOne, txtSecondPhnoTwo, txtSecondPhnoThree, reference.ContactPhoneNumber);

                            ddlProjectReferenceTypeTwo.SelectedValue = reference.FK_ProjectReferenceType.ConvertToString();  //004-sooraj
                            ddlRefSpeaksEnglishTwo.SelectedValue = reference.IsSpeaksEnglish.ConvertToString(); //007
                            if (Convert.ToString(reference.FK_State).Equals("51"))
                            {
                                trOther2.Style["display"] = "";
                                txtOtherStateTwo.Text = reference.OtherState;
                            }
                            break;
                        case 3:
                            txtProjectNameThree.Text = reference.ProjectName;
                            txtWorkCompletedThree.Text = reference.WorkCompleted;
                            txtCityThree.Text = reference.City;
                            ddlCountryThree.SelectedValue = reference.FK_Country.ConvertToString();//005-Sooraj
                            BindAssociateStateRefernce(ddlCountryThree);//005-Sooraj
                            ddlStateThree.SelectedValue = Convert.ToString(reference.FK_State);//005-Sooraj
                            CompletionDate = reference.CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear3.SelectedValue = Month[1];
                                    ddlMonth3.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtThree.Text = reference.ApproximateContract;
                            txtContractorNameThree.Text = reference.GeneralContractor;
                            txtCtrContactNameThree.Text = reference.ContactName;
                            chkNA3.Checked = Convert.ToBoolean(reference.NotAvailable);
                            trExplain3.Style["Display"] = chkNA3.Checked ? "" : "none";

                            txtExplain3.Text = reference.Explanation;
                            objCommon.loadphone(txtThirdPhnoOne, txtThirdPhnoTwo, txtThirdPhnoThree, reference.ContactPhoneNumber);
                            ddlProjectReferenceTypeThree.SelectedValue = reference.FK_ProjectReferenceType.ConvertToString();  //004-sooraj
                            ddlRefSpeaksEnglishThree.SelectedValue = reference.IsSpeaksEnglish.ConvertToString(); //007

                            if (Convert.ToString(reference.FK_State).Equals("51"))
                            {
                                trOther3.Style["display"] = "";
                                txtOtherStateThree.Text = reference.OtherState;
                            }
                            break;
                        case 4:
                            trReferencesFour.Style["display"] = "";
                            txtProjectNameFour.Text = reference.ProjectName;
                            txtWorkCompletedFour.Text = reference.WorkCompleted;
                            txtCityFour.Text = reference.City;
                            ddlCountryReferencesFour.SelectedValue = reference.FK_Country.ConvertToString();//005-Sooraj
                            BindAssociateStateRefernce(ddlCountryReferencesFour);//005-Sooraj
                            ddlStateReferencesFour.SelectedValue = Convert.ToString(reference.FK_State);//005-Sooraj
                            CompletionDate = reference.CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear4.SelectedValue = Month[1];
                                    ddlMonth4.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtFour.Text = reference.ApproximateContract;
                            txtContractorNameFour.Text = reference.GeneralContractor;
                            txtCtrContactNameFour.Text = reference.ContactName;
                            objCommon.loadphone(txtFourthPhnoOne, txtFourthPhnoTwo, txtFourthPhnoThree, reference.ContactPhoneNumber);
                            ddlProjectReferenceTypeFour.SelectedValue = reference.FK_ProjectReferenceType.ConvertToString();  //004-sooraj
                            ddlRefSpeaksEnglishFour.SelectedValue = reference.IsSpeaksEnglish.ConvertToString(); //007

                            if (Convert.ToString(reference.FK_State).Equals("51"))
                            {
                                trReferencesFour.Style["display"] = "";
                                txtStateReferencesFour.Text = reference.OtherState;
                            }
                            break;
                        case 5:
                            trReferencesFive.Style["display"] = "";
                            txtProjectNameFive.Text = reference.ProjectName;
                            txtWorkCompletedFive.Text = reference.WorkCompleted;
                            txtCityFive.Text = reference.City;
                            ddlCountryReferencesFive.SelectedValue = reference.FK_Country.ConvertToString();//005-Sooraj
                            BindAssociateStateRefernce(ddlCountryReferencesFive);//005-Sooraj
                            ddlStateReferencesFive.SelectedValue = Convert.ToString(reference.FK_State);//005-Sooraj
                            CompletionDate = reference.CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear5.SelectedValue = Month[1];
                                    ddlMonth5.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtFive.Text = reference.ApproximateContract;
                            txtContractorNameFive.Text = reference.GeneralContractor;
                            txtCtrContactNameFive.Text = reference.ContactName;
                            objCommon.loadphone(txtFifthPhnoOne, txtFifthPhnoTwo, txtFifthPhnoThree, reference.ContactPhoneNumber);
                            ddlProjectReferenceTypeFive.SelectedValue = reference.FK_ProjectReferenceType.ConvertToString();  //004-sooraj
                            ddlRefSpeaksEnglishFive.SelectedValue = reference.IsSpeaksEnglish.ConvertToString(); //007

                            if (Convert.ToString(reference.FK_State).Equals("51"))
                            {
                                trReferencesFive.Style["display"] = "";
                                txtStateReferencesFive.Text = reference.OtherState;
                            }
                            break;
                        case 6:
                            trReferencesSix.Style["display"] = "";
                            txtProjectNameSix.Text = reference.ProjectName;
                            txtWorkCompletedSix.Text = reference.WorkCompleted;
                            txtCitySix.Text = reference.City;
                            ddlCountryReferencesSix.SelectedValue = reference.FK_Country.ConvertToString();//005-Sooraj
                            BindAssociateStateRefernce(ddlCountryReferencesSix);//005-Sooraj
                            ddlStateReferencesSix.SelectedValue = Convert.ToString(reference.FK_State);//005-Sooraj
                            CompletionDate = reference.CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear6.SelectedValue = Month[1];
                                    ddlMonth6.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtSix.Text = reference.ApproximateContract;
                            txtContractorNameSix.Text = reference.GeneralContractor;
                            txtCtrContactNameSix.Text = reference.ContactName;
                            objCommon.loadphone(txtSixthPhnoOne, txtSixthPhnoTwo, txtSixthPhnoThree, reference.ContactPhoneNumber);
                            ddlProjectReferenceTypeSix.SelectedValue = reference.FK_ProjectReferenceType.ConvertToString();  //004-sooraj
                            ddlRefSpeaksEnglishSix.SelectedValue = reference.IsSpeaksEnglish.ConvertToString(); //007

                            if (Convert.ToString(reference.FK_State).Equals("51"))
                            {
                                trReferencesSix.Style["display"] = "";
                                txtStateReferencesSix.Text = reference.OtherState;
                            }
                            break;
                        case 7:
                            trReferencesSeven.Style["display"] = "";
                            txtProjectNameSeven.Text = reference.ProjectName;
                            txtWorkCompletedSeven.Text = reference.WorkCompleted;
                            txtCitySeven.Text = reference.City;
                            ddlCountryReferencesSeven.SelectedValue = reference.FK_Country.ConvertToString();//005-Sooraj
                            BindAssociateStateRefernce(ddlCountryReferencesSeven);//005-Sooraj
                            ddlStateReferencesSeven.SelectedValue = Convert.ToString(reference.FK_State);//005-Sooraj
                            CompletionDate = reference.CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear7.SelectedValue = Month[1];
                                    ddlMonth7.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtSeven.Text = reference.ApproximateContract;
                            txtContractorNameSeven.Text = reference.GeneralContractor;
                            txtCtrContactNameSeven.Text = reference.ContactName;
                            objCommon.loadphone(txtSeventhPhnoOne, txtSeventhPhnoTwo, txtSeventhPhnoThree, reference.ContactPhoneNumber);
                            ddlProjectReferenceTypeSeven.SelectedValue = reference.FK_ProjectReferenceType.ConvertToString();  //004-sooraj
                            ddlRefSpeaksEnglishSeven.SelectedValue = reference.IsSpeaksEnglish.ConvertToString(); //007

                            if (Convert.ToString(reference.FK_State).Equals("51"))
                            {
                                trReferencesSeven.Style["display"] = "";
                                txtStateReferencesSeven.Text = reference.OtherState;
                            }
                            break;
                        case 8:
                            trReferencesEight.Style["display"] = "";

                            txtProjectNameEight.Text = reference.ProjectName;
                            txtWorkCompletedEight.Text = reference.WorkCompleted;
                            txtCityEight.Text = reference.City;
                            ddlCountryReferencesEight.SelectedValue = reference.FK_Country.ConvertToString();//005-Sooraj
                            BindAssociateStateRefernce(ddlCountryReferencesEight);//005-Sooraj
                            ddlStateReferencesEight.SelectedValue = Convert.ToString(reference.FK_State);//005-Sooraj
                            CompletionDate = reference.CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear8.SelectedValue = Month[1];
                                    ddlMonth8.SelectedValue = Month[0];
                                }
                            }

                            txtApproximateContractAmtEight.Text = reference.ApproximateContract;

                            txtContractorNameEight.Text = reference.GeneralContractor;
                            txtCtrContactNameEight.Text = reference.ContactName;
                            objCommon.loadphone(txtEighthPhnoOne, txtEighthPhnoTwo, txtEighthPhnoThree, reference.ContactPhoneNumber);
                            ddlProjectReferenceTypeEight.SelectedValue = reference.FK_ProjectReferenceType.ConvertToString(); //004-sooraj
                            ddlRefSpeaksEnglishEight.SelectedValue = reference.IsSpeaksEnglish.ConvertToString(); //007

                            if (Convert.ToString(reference.FK_State).Equals("51"))
                            {
                                trReferencesEight.Style["display"] = "";
                                txtStateRefencesEight.Text = reference.OtherState;
                            }
                            break;
                        case 9:
                            trReferencesNine.Style["display"] = "";
                            txtProjectNameNine.Text = reference.ProjectName;
                            txtWorkCompletedNinth.Text = reference.WorkCompleted;
                            txtCityNine.Text = reference.City;
                            ddlCountryReferencesNine.SelectedValue = reference.FK_Country.ConvertToString();//005-Sooraj
                            BindAssociateStateRefernce(ddlCountryReferencesNine);//005-Sooraj
                            ddlStateReferencesNine.SelectedValue = Convert.ToString(reference.FK_State);//005-Sooraj
                            CompletionDate = reference.CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear9.SelectedValue = Month[1];
                                    ddlMonth9.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtnine.Text = reference.ApproximateContract;
                            txtContractorNameNine.Text = reference.GeneralContractor;
                            txtCtrContactNameNinth.Text = reference.ContactName;
                            objCommon.loadphone(txtNinthPhnoOne, txtNinthPhnoTwo, txtNinthPhnoThree, reference.ContactPhoneNumber);
                            ddlProjectReferenceTypeNine.SelectedValue = reference.FK_ProjectReferenceType.ConvertToString();  //004-sooraj
                            ddlRefSpeaksEnglishNine.SelectedValue = reference.IsSpeaksEnglish.ConvertToString(); //007

                            if (Convert.ToString(reference.FK_State).Equals("51"))
                            {
                                trReferencesNine.Style["display"] = "";
                                txtStateReferencesNine.Text = reference.OtherState;
                            }
                            break;
                        case 10:
                            trReferencesTen.Style["display"] = "";
                            txtProjectNameTen.Text = reference.ProjectName;
                            txtWorkCompletedTen.Text = reference.WorkCompleted;
                            txtCityTen.Text = reference.City;
                            ddlCountryReferencesTen.SelectedValue = reference.FK_Country.ConvertToString();//005-Sooraj
                            BindAssociateState(ddlCountryReferencesTen);//005-Sooraj
                            ddlStateReferencesTen.SelectedValue = Convert.ToString(reference.FK_State);//005-Sooraj
                            CompletionDate = reference.CompletionDT;
                            if (!string.IsNullOrEmpty(CompletionDate))
                            {
                                string[] Month = CompletionDate.Split('/');
                                if (Month.Length == 2)
                                {
                                    ddlYear10.SelectedValue = Month[1];
                                    ddlMonth10.SelectedValue = Month[0];
                                }
                            }
                            txtApproximateContractAmtTen.Text = reference.ApproximateContract;
                            txtContractorNameTen.Text = reference.GeneralContractor;
                            txtCtrContactNameTen.Text = reference.ContactName;

                            objCommon.loadphone(txtTenthPhnoOne, txtTenthPhnoTwo, txtTenthPhnoThree, reference.ContactPhoneNumber);
                            ddlProjectReferenceTypeTen.SelectedValue = reference.FK_ProjectReferenceType.ConvertToString();  //004-sooraj
                            ddlRefSpeaksEnglishTen.SelectedValue = reference.IsSpeaksEnglish.ConvertToString(); //007

                            if (Convert.ToString(reference.FK_State).Equals("51"))
                            {
                                trReferencesTen.Style["display"] = "";
                                txtStateReferencesTen.Text = reference.OtherState;
                            }
                            break;
                    }
                }
                DisplayReferenceNA();
            }

            #endregion

            //004-sooraj Commented for performance
            #region "Commented"
            /* Sooraj Commented to avoid unwanted db call 
            #region Load Additional Information

            int Count1 = objReferences.GetReferencesProject(RefId).Count();
            hdnaddProjectcount.Value = Convert.ToString(Count1);
            for (int i = 1; i <= Count1; i++)
            {
                switch (i)
                {
                    case 1:
                        trAddProject01.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameOne.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectName;
                        txtHaskellProjectManagerOne.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectManager;
                        txtYearOne.Text = objReferences.GetReferencesProject(RefId)[i - 1].Year;
                        hdnaddProjectcount.Value = "1";
                        break;
                    case 2:
                        trAddProject02.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameTwo.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectName;
                        txtHaskellProjectManagerTwo.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectManager;
                        txtYearTwo.Text = objReferences.GetReferencesProject(RefId)[i - 1].Year;
                        hdnaddProjectcount.Value = "2";
                        break;
                    case 3:
                        trAddProject03.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameThree.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectName;
                        txtHaskellProjectManagerThree.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectManager;
                        txtYearThree.Text = objReferences.GetReferencesProject(RefId)[i - 1].Year;
                        hdnaddProjectcount.Value = "3";
                        break;
                    case 4:

                        trAddProject04.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameFour.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectName;
                        txtHaskellProjectManagerFour.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectManager;
                        txtYearFour.Text = objReferences.GetReferencesProject(RefId)[i - 1].Year;
                        hdnaddProjectcount.Value = "4";
                        break;
                    case 5:
                        trAddProject05.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameFive.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectName;
                        txtHaskellProjectManagerFive.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectManager;
                        txtYearFive.Text = objReferences.GetReferencesProject(RefId)[i - 1].Year;
                        hdnaddProjectcount.Value = "5";
                        break;
                    case 6:
                        trAddProject06.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameSix.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectName;
                        txtHaskellProjectManagerSix.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectManager;
                        txtYearSix.Text = objReferences.GetReferencesProject(RefId)[i - 1].Year;
                        hdnaddProjectcount.Value = "6";
                        break;
                    case 7:
                        trAddProject07.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameSeven.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectName;
                        txtHaskellProjectManagerSeven.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectManager;
                        txtYearSeven.Text = objReferences.GetReferencesProject(RefId)[i - 1].Year;
                        hdnaddProjectcount.Value = "7";
                        break;
                    case 8:
                        trAddProject08.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameEight.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectName;
                        txtHaskellProjectManagerEight.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectManager;
                        txtYearEight.Text = objReferences.GetReferencesProject(RefId)[i - 1].Year;
                        hdnaddProjectcount.Value = "8";
                        break;
                    case 9:
                        trAddProject09.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameNine.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectName;
                        txtHaskellProjectManagerNine.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectManager;
                        txtYearNine.Text = objReferences.GetReferencesProject(RefId)[i - 1].Year;
                        break;
                    case 10:
                        trAddProject10.Style["display"] = "";
                        txtHaskellProjectNameTen.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectName;
                        txtHaskellProjectManagerTen.Text = objReferences.GetReferencesProject(RefId)[i - 1].ProjectManager;
                        txtYearTen.Text = objReferences.GetReferencesProject(RefId)[i - 1].Year;
                        tdlnkbtnaddproject.Style["display"] = "none";
                        lnladdProject.Style["display"] = "none";
                        break;
                }
            }

            #endregion

            #region Load Supplier

            int SuplCount = objReferences.GetReferencesSupplier(RefId).Length;
            if (SuplCount > 3)
            {
                hdnCount.Value = (SuplCount - 3).ToString();
            }
            else
            {
                hdnCount.Value = "0";
            }
            for (int i = 0; i < SuplCount; i++)
            {
                switch (i)
                {
                    case 0:
                        txtCompanyNameOne.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyName;
                        txtCompanyAddressOne.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyAddress;
                        txtSuplCityFour.Text = objReferences.GetReferencesSupplier(RefId)[i].City;
                        ddlStateFour.SelectedValue = Convert.ToString(objReferences.GetReferencesSupplier(RefId)[i].FK_State);
                        txtSuplZipOne.Text = objReferences.GetReferencesSupplier(RefId)[i].ZipCode;
                        txtOtherStateFour.Text = objReferences.GetReferencesSupplier(RefId)[i].OtherState;
                        txtContactNameOne.Text = objReferences.GetReferencesSupplier(RefId)[i].ContactName;
                        string SuplMainphnolength = objReferences.GetReferencesSupplier(RefId)[i].Phone;
                        objCommon.loadphone(txtSuplMainPhoneOne, txtSuplMainPhoneTwo, txtSuplMainPhoneThree, SuplMainphnolength);
                        break;
                    case 1:
                        txtCompanyNameTwo.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyName;
                        txtCompanyAddressTwo.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyAddress;
                        txtSuplCityFive.Text = objReferences.GetReferencesSupplier(RefId)[i].City;
                        ddlStateFive.SelectedValue = Convert.ToString(objReferences.GetReferencesSupplier(RefId)[i].FK_State);
                        txtSuplZipTwo.Text = objReferences.GetReferencesSupplier(RefId)[i].ZipCode;
                        txtOtherStateFive.Text = objReferences.GetReferencesSupplier(RefId)[i].OtherState;
                        txtContactNameTwo.Text = objReferences.GetReferencesSupplier(RefId)[i].ContactName;
                        string SuplSecondphnolength = objReferences.GetReferencesSupplier(RefId)[i].Phone;
                        objCommon.loadphone(txtSuplSecondPhoneOne, txtSuplSecondPhoneTwo, txtSuplSecondPhoneThree, SuplSecondphnolength);
                        break;
                    case 2:
                        txtCompanyNameThree.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyName;
                        txtCompanyAddressThree.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyAddress;
                        txtSuplCitySix.Text = objReferences.GetReferencesSupplier(RefId)[i].City;
                        ddlStateSix.SelectedValue = Convert.ToString(objReferences.GetReferencesSupplier(RefId)[i].FK_State);
                        txtSuplZipThree.Text = objReferences.GetReferencesSupplier(RefId)[i].ZipCode;
                        txtOtherStateSix.Text = objReferences.GetReferencesSupplier(RefId)[i].OtherState;
                        txtContactNameThree.Text = objReferences.GetReferencesSupplier(RefId)[i].ContactName;
                        string SuplThirdphnolength = objReferences.GetReferencesSupplier(RefId)[i].Phone;
                        objCommon.loadphone(txtSuplThirdPhoneOne, txtSuplThirdPhoneTwo, txtSuplThirdPhoneThree, SuplThirdphnolength);
                        break;
                    case 3:
                        spnmore1.Style["display"] = "";
                        txtCompanyNameFour.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyName;
                        txtCompanyAddressFour.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyAddress;
                        txtSuplCitySeven.Text = objReferences.GetReferencesSupplier(RefId)[i].City;
                        ddlStateSeven.SelectedValue = Convert.ToString(objReferences.GetReferencesSupplier(RefId)[i].FK_State);
                        txtSuplZipFour.Text = objReferences.GetReferencesSupplier(RefId)[i].ZipCode;
                        txtOtherStateSeven.Text = objReferences.GetReferencesSupplier(RefId)[i].OtherState;
                        txtContactNameFour.Text = objReferences.GetReferencesSupplier(RefId)[i].ContactName;
                        string SuplFouthphnolength = objReferences.GetReferencesSupplier(RefId)[i].Phone;
                        objCommon.loadphone(txtSuplFourthPhoneOne, txtSuplFourthPhoneTwo, txtSuplFourthPhoneThree, SuplFouthphnolength);
                        break;
                    case 4:
                        spnmore2.Style["display"] = "";
                        txtCompanyNameFive.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyName;
                        txtCompanyAddressFive.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyAddress;
                        txtSuplCityEight.Text = objReferences.GetReferencesSupplier(RefId)[i].City;
                        ddlStateEight.SelectedValue = Convert.ToString(objReferences.GetReferencesSupplier(RefId)[i].FK_State);
                        txtSuplZipFive.Text = objReferences.GetReferencesSupplier(RefId)[i].ZipCode;
                        txtOtherStateEight.Text = objReferences.GetReferencesSupplier(RefId)[i].OtherState;
                        txtContactNameFive.Text = objReferences.GetReferencesSupplier(RefId)[i].ContactName;
                        string SuplFifthphnolength = objReferences.GetReferencesSupplier(RefId)[i].Phone;
                        objCommon.loadphone(txtSuplFivePhoneOne, txtSuplFivePhoneTwo, txtSuplFivePhoneThree, SuplFifthphnolength);
                        break;
                    case 5:
                        spnmore3.Style["display"] = "";
                        txtCompanyNameSix.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyName;
                        txtCompanyAddressSix.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyAddress;
                        txtSuplCityNine.Text = objReferences.GetReferencesSupplier(RefId)[i].City;
                        ddlStateNine.SelectedValue = Convert.ToString(objReferences.GetReferencesSupplier(RefId)[i].FK_State);
                        txtSuplZipSix.Text = objReferences.GetReferencesSupplier(RefId)[i].ZipCode;
                        txtOtherStateNine.Text = objReferences.GetReferencesSupplier(RefId)[i].OtherState;
                        txtContactNameSix.Text = objReferences.GetReferencesSupplier(RefId)[i].ContactName;
                        string SuplSixphnolength = objReferences.GetReferencesSupplier(RefId)[i].Phone;
                        objCommon.loadphone(txtSuplSixPhoneOne, txtSuplSixPhoneTwo, txtSuplSixPhoneThree, SuplSixphnolength);
                        break;
                    case 6:
                        spnmore4.Style["display"] = "";
                        txtCompanyNameSeven.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyName;
                        txtCompanyAddressSeven.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyAddress;
                        txtSuplCityTen.Text = objReferences.GetReferencesSupplier(RefId)[i].City;
                        ddlStateTen.SelectedValue = Convert.ToString(objReferences.GetReferencesSupplier(RefId)[i].FK_State);
                        txtSuplZipSeven.Text = objReferences.GetReferencesSupplier(RefId)[i].ZipCode;
                        txtOtherStateTen.Text = objReferences.GetReferencesSupplier(RefId)[i].OtherState;
                        txtContactNameSeven.Text = objReferences.GetReferencesSupplier(RefId)[i].ContactName;
                        string SuplSeventhphnolength = objReferences.GetReferencesSupplier(RefId)[i].Phone;
                        objCommon.loadphone(txtSuplSevenPhoneOne, txtSuplSevenPhoneTwo, txtSuplSevenPhoneThree, SuplSeventhphnolength);
                        break;
                    case 7:
                        spnmore5.Style["display"] = "";
                        txtCompanyNameEight.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyName;
                        txtCompanyAddressEight.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyAddress;
                        txtSuplCityEleven.Text = objReferences.GetReferencesSupplier(RefId)[i].City;
                        ddlStateEleven.SelectedValue = Convert.ToString(objReferences.GetReferencesSupplier(RefId)[i].FK_State);
                        txtSuplZipEight.Text = objReferences.GetReferencesSupplier(RefId)[i].ZipCode;
                        txtOtherStateEleven.Text = objReferences.GetReferencesSupplier(RefId)[i].OtherState;
                        txtContactNameEight.Text = objReferences.GetReferencesSupplier(RefId)[i].ContactName;
                        string SuplEightphnolength = objReferences.GetReferencesSupplier(RefId)[i].Phone;
                        objCommon.loadphone(txtSuplEightPhoneOne, txtSuplEightPhoneTwo, txtSuplEightPhoneThree, SuplEightphnolength);
                        break;
                    case 8:
                        spnmore6.Style["display"] = "";
                        txtCompanyNameNine.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyName;
                        txtCompanyAddressNine.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyAddress;
                        txtSuplCityTwelve.Text = objReferences.GetReferencesSupplier(RefId)[i].City;
                        ddlStateTwelve.SelectedValue = Convert.ToString(objReferences.GetReferencesSupplier(RefId)[i].FK_State);
                        txtSuplZipNine.Text = objReferences.GetReferencesSupplier(RefId)[i].ZipCode;
                        txtOtherStateTwelve.Text = objReferences.GetReferencesSupplier(RefId)[i].OtherState;
                        txtContactNameNine.Text = objReferences.GetReferencesSupplier(RefId)[i].ContactName;
                        string SuplNinephnolength = objReferences.GetReferencesSupplier(RefId)[i].Phone;
                        objCommon.loadphone(txtSuplNinePhoneOne, txtSuplNinePhoneTwo, txtSuplNinePhoneThree, SuplNinephnolength);
                        break;
                    case 9:
                        spnmore7.Style["display"] = "";
                        txtCompanyNameTen.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyName;
                        txtCompanyAddressTen.Text = objReferences.GetReferencesSupplier(RefId)[i].CompanyAddress;
                        txtSuplCityThirteen.Text = objReferences.GetReferencesSupplier(RefId)[i].City;
                        ddlStateThirteen.SelectedValue = Convert.ToString(objReferences.GetReferencesSupplier(RefId)[i].FK_State);
                        txtSuplZipTen.Text = objReferences.GetReferencesSupplier(RefId)[i].ZipCode;
                        txtOtherStateThirteen.Text = objReferences.GetReferencesSupplier(RefId)[i].OtherState;
                        txtContactNameTen.Text = objReferences.GetReferencesSupplier(RefId)[i].ContactName;
                        string SuplTenphnolength = objReferences.GetReferencesSupplier(RefId)[i].Phone;
                        objCommon.loadphone(txtSuplTenPhoneOne, txtSuplTenPhoneTwo, txtSuplTenPhoneThree, SuplTenphnolength);
                        break;
                }
            }

            #endregion
            */
            #endregion


            //004-sooraj Modified for performance 
            #region Load Additional Information
            var refereceProjectList = objReferences.GetReferencesProject(RefId);
            int Count1 = refereceProjectList.Count();
            hdnaddProjectcount.Value = Convert.ToString(Count1);
            DAL.USPGetReferencesProjectResult referenceProject;

            for (int i = 1; i <= Count1; i++)
            {
                referenceProject = refereceProjectList[i - 1];
                switch (i)
                {
                    case 1:
                        trAddProject01.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameOne.Text = referenceProject.ProjectName;
                        txtHaskellProjectManagerOne.Text = referenceProject.ProjectManager;
                        txtYearOne.Text = referenceProject.Year;
                        hdnaddProjectcount.Value = "1";
                        break;
                    case 2:
                        trAddProject02.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameTwo.Text = referenceProject.ProjectName;
                        txtHaskellProjectManagerTwo.Text = referenceProject.ProjectManager;
                        txtYearTwo.Text = referenceProject.Year;
                        hdnaddProjectcount.Value = "2";
                        break;
                    case 3:
                        trAddProject03.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameThree.Text = referenceProject.ProjectName;
                        txtHaskellProjectManagerThree.Text = referenceProject.ProjectManager;
                        txtYearThree.Text = referenceProject.Year;
                        hdnaddProjectcount.Value = "3";
                        break;
                    case 4:

                        trAddProject04.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameFour.Text = referenceProject.ProjectName;
                        txtHaskellProjectManagerFour.Text = referenceProject.ProjectManager;
                        txtYearFour.Text = referenceProject.Year;
                        hdnaddProjectcount.Value = "4";
                        break;
                    case 5:
                        trAddProject05.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameFive.Text = referenceProject.ProjectName;
                        txtHaskellProjectManagerFive.Text = referenceProject.ProjectManager;
                        txtYearFive.Text = referenceProject.Year;
                        hdnaddProjectcount.Value = "5";
                        break;
                    case 6:
                        trAddProject06.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameSix.Text = referenceProject.ProjectName;
                        txtHaskellProjectManagerSix.Text = referenceProject.ProjectManager;
                        txtYearSix.Text = referenceProject.Year;
                        hdnaddProjectcount.Value = "6";
                        break;
                    case 7:
                        trAddProject07.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameSeven.Text = referenceProject.ProjectName;
                        txtHaskellProjectManagerSeven.Text = referenceProject.ProjectManager;
                        txtYearSeven.Text = referenceProject.Year;
                        hdnaddProjectcount.Value = "7";
                        break;
                    case 8:
                        trAddProject08.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameEight.Text = referenceProject.ProjectName;
                        txtHaskellProjectManagerEight.Text = referenceProject.ProjectManager;
                        txtYearEight.Text = referenceProject.Year;
                        hdnaddProjectcount.Value = "8";
                        break;
                    case 9:
                        trAddProject09.Style["display"] = "";
                        tdlnkbtnaddproject.Style["display"] = "";
                        lnladdProject.Style["display"] = "";
                        txtHaskellProjectNameNine.Text = referenceProject.ProjectName;
                        txtHaskellProjectManagerNine.Text = referenceProject.ProjectManager;
                        txtYearNine.Text = referenceProject.Year;
                        break;
                    case 10:
                        trAddProject10.Style["display"] = "";
                        txtHaskellProjectNameTen.Text = referenceProject.ProjectName;
                        txtHaskellProjectManagerTen.Text = referenceProject.ProjectManager;
                        txtYearTen.Text = referenceProject.Year;
                        tdlnkbtnaddproject.Style["display"] = "none";
                        lnladdProject.Style["display"] = "none";
                        break;
                }
            }

            #endregion

            #region Load Supplier
            var referenceSupplierList = objReferences.GetReferencesSupplier(RefId);
            int SuplCount = referenceSupplierList.Length;
            DAL.USPGetReferencesSupplierResult referenceSupplier;

            if (SuplCount > 3)
            {
                hdnCount.Value = (SuplCount - 3).ToString();
            }
            else
            {
                hdnCount.Value = "0";
            }
            for (int i = 0; i < SuplCount; i++)
            {
                referenceSupplier = referenceSupplierList[i];
                switch (i)
                {
                    case 0:
                        txtCompanyNameOne.Text = referenceSupplier.CompanyName;
                        txtCompanyAddressOne.Text = referenceSupplier.CompanyAddress;
                        txtSuplCityFour.Text = referenceSupplier.City;

                        ddlCountryFour.SelectedValue = referenceSupplier.FK_Country.ConvertToString();//005-Sooraj
                        BindAssociateState(ddlCountryFour);//005-Sooraj
                        ddlStateFour.SelectedValue = Convert.ToString(referenceSupplier.FK_State);//005-Sooraj
                        ddlSupSpeaksEnglishOne.SelectedValue = referenceSupplier.IsSpeaksEnglish.ConvertToString(); //007

                        txtSuplZipOne.Text = referenceSupplier.ZipCode;
                        txtOtherStateFour.Text = referenceSupplier.OtherState;
                        txtContactNameOne.Text = referenceSupplier.ContactName;
                        string SuplMainphnolength = referenceSupplier.Phone;
                        objCommon.loadphone(txtSuplMainPhoneOne, txtSuplMainPhoneTwo, txtSuplMainPhoneThree, SuplMainphnolength);
                        break;
                    case 1:
                        txtCompanyNameTwo.Text = referenceSupplier.CompanyName;
                        txtCompanyAddressTwo.Text = referenceSupplier.CompanyAddress;
                        txtSuplCityFive.Text = referenceSupplier.City;


                        ddlCountryFive.SelectedValue = referenceSupplier.FK_Country.ConvertToString();//005-Sooraj
                        BindAssociateState(ddlCountryFive);//005-Sooraj
                        ddlStateFive.SelectedValue = Convert.ToString(referenceSupplier.FK_State);//005-Sooraj
                        ddlSupSpeaksEnglishTwo.SelectedValue = referenceSupplier.IsSpeaksEnglish.ConvertToString(); //007


                        txtSuplZipTwo.Text = referenceSupplier.ZipCode;
                        txtOtherStateFive.Text = referenceSupplier.OtherState;
                        txtContactNameTwo.Text = referenceSupplier.ContactName;
                        string SuplSecondphnolength = referenceSupplier.Phone;
                        objCommon.loadphone(txtSuplSecondPhoneOne, txtSuplSecondPhoneTwo, txtSuplSecondPhoneThree, SuplSecondphnolength);
                        break;
                    case 2:
                        txtCompanyNameThree.Text = referenceSupplier.CompanyName;
                        txtCompanyAddressThree.Text = referenceSupplier.CompanyAddress;
                        txtSuplCitySix.Text = referenceSupplier.City;

                        ddlCountrySix.SelectedValue = referenceSupplier.FK_Country.ConvertToString();//005-Sooraj
                        BindAssociateState(ddlCountrySix); //005-Sooraj
                        ddlStateSix.SelectedValue = Convert.ToString(referenceSupplier.FK_State);//005-Sooraj
                        ddlSupSpeaksEnglishThree.SelectedValue = referenceSupplier.IsSpeaksEnglish.ConvertToString(); //007


                        txtSuplZipThree.Text = referenceSupplier.ZipCode;
                        txtOtherStateSix.Text = referenceSupplier.OtherState;
                        txtContactNameThree.Text = referenceSupplier.ContactName;
                        string SuplThirdphnolength = referenceSupplier.Phone;
                        objCommon.loadphone(txtSuplThirdPhoneOne, txtSuplThirdPhoneTwo, txtSuplThirdPhoneThree, SuplThirdphnolength);
                        break;
                    case 3:
                        spnmore1.Style["display"] = "";
                        txtCompanyNameFour.Text = referenceSupplier.CompanyName;
                        txtCompanyAddressFour.Text = referenceSupplier.CompanyAddress;
                        txtSuplCitySeven.Text = referenceSupplier.City;

                        ddlCountrySeven.SelectedValue = referenceSupplier.FK_Country.ConvertToString();//005-Sooraj
                        BindAssociateState(ddlCountrySeven); //005-Sooraj
                        ddlStateSeven.SelectedValue = Convert.ToString(referenceSupplier.FK_State);//005-Sooraj
                        ddlSupSpeaksEnglishFour.SelectedValue = referenceSupplier.IsSpeaksEnglish.ConvertToString(); //007


                        txtSuplZipFour.Text = referenceSupplier.ZipCode;
                        txtOtherStateSeven.Text = referenceSupplier.OtherState;
                        txtContactNameFour.Text = referenceSupplier.ContactName;
                        string SuplFouthphnolength = referenceSupplier.Phone;
                        objCommon.loadphone(txtSuplFourthPhoneOne, txtSuplFourthPhoneTwo, txtSuplFourthPhoneThree, SuplFouthphnolength);
                        break;
                    case 4:
                        spnmore2.Style["display"] = "";
                        txtCompanyNameFive.Text = referenceSupplier.CompanyName;
                        txtCompanyAddressFive.Text = referenceSupplier.CompanyAddress;
                        txtSuplCityEight.Text = referenceSupplier.City;

                        ddlCountryEight.SelectedValue = referenceSupplier.FK_Country.ConvertToString();//005-Sooraj
                        BindAssociateState(ddlCountryEight); //005-Sooraj
                        ddlStateEight.SelectedValue = Convert.ToString(referenceSupplier.FK_State);//005-Sooraj
                        ddlSupSpeaksEnglishFive.SelectedValue = referenceSupplier.IsSpeaksEnglish.ConvertToString(); //007

                        txtSuplZipFive.Text = referenceSupplier.ZipCode;
                        txtOtherStateEight.Text = referenceSupplier.OtherState;
                        txtContactNameFive.Text = referenceSupplier.ContactName;
                        string SuplFifthphnolength = referenceSupplier.Phone;
                        objCommon.loadphone(txtSuplFivePhoneOne, txtSuplFivePhoneTwo, txtSuplFivePhoneThree, SuplFifthphnolength);
                        break;
                    case 5:
                        spnmore3.Style["display"] = "";
                        txtCompanyNameSix.Text = referenceSupplier.CompanyName;
                        txtCompanyAddressSix.Text = referenceSupplier.CompanyAddress;
                        txtSuplCityNine.Text = referenceSupplier.City;

                        ddlCountryNine.SelectedValue = referenceSupplier.FK_Country.ConvertToString();//005-Sooraj
                        BindAssociateState(ddlCountryNine); //005-Sooraj
                        ddlStateNine.SelectedValue = Convert.ToString(referenceSupplier.FK_State);//005-Sooraj
                        ddlSupSpeaksEnglishSix.SelectedValue = referenceSupplier.IsSpeaksEnglish.ConvertToString(); //007

                        txtSuplZipSix.Text = referenceSupplier.ZipCode;
                        txtOtherStateNine.Text = referenceSupplier.OtherState;
                        txtContactNameSix.Text = referenceSupplier.ContactName;
                        string SuplSixphnolength = referenceSupplier.Phone;
                        objCommon.loadphone(txtSuplSixPhoneOne, txtSuplSixPhoneTwo, txtSuplSixPhoneThree, SuplSixphnolength);
                        break;
                    case 6:
                        spnmore4.Style["display"] = "";
                        txtCompanyNameSeven.Text = referenceSupplier.CompanyName;
                        txtCompanyAddressSeven.Text = referenceSupplier.CompanyAddress;
                        txtSuplCityTen.Text = referenceSupplier.City;

                        ddlCountryTen.SelectedValue = referenceSupplier.FK_Country.ConvertToString();//005-Sooraj
                        BindAssociateState(ddlCountryTen); //005-Sooraj
                        ddlStateTen.SelectedValue = Convert.ToString(referenceSupplier.FK_State);//005-Sooraj
                        ddlSupSpeaksEnglishSeven.SelectedValue = referenceSupplier.IsSpeaksEnglish.ConvertToString(); //007

                        txtSuplZipSeven.Text = referenceSupplier.ZipCode;
                        txtOtherStateTen.Text = referenceSupplier.OtherState;
                        txtContactNameSeven.Text = referenceSupplier.ContactName;
                        string SuplSeventhphnolength = referenceSupplier.Phone;
                        objCommon.loadphone(txtSuplSevenPhoneOne, txtSuplSevenPhoneTwo, txtSuplSevenPhoneThree, SuplSeventhphnolength);
                        break;
                    case 7:
                        spnmore5.Style["display"] = "";
                        txtCompanyNameEight.Text = referenceSupplier.CompanyName;
                        txtCompanyAddressEight.Text = referenceSupplier.CompanyAddress;
                        txtSuplCityEleven.Text = referenceSupplier.City;

                        ddlCountryEleven.SelectedValue = referenceSupplier.FK_Country.ConvertToString();//005-Sooraj
                        BindAssociateState(ddlCountryEleven); //005-Sooraj
                        ddlStateEleven.SelectedValue = Convert.ToString(referenceSupplier.FK_State);//005-Sooraj
                        ddlSupSpeaksEnglishEight.SelectedValue = referenceSupplier.IsSpeaksEnglish.ConvertToString(); //007

                        txtSuplZipEight.Text = referenceSupplier.ZipCode;
                        txtOtherStateEleven.Text = referenceSupplier.OtherState;
                        txtContactNameEight.Text = referenceSupplier.ContactName;
                        string SuplEightphnolength = referenceSupplier.Phone;
                        objCommon.loadphone(txtSuplEightPhoneOne, txtSuplEightPhoneTwo, txtSuplEightPhoneThree, SuplEightphnolength);
                        break;
                    case 8:
                        spnmore6.Style["display"] = "";
                        txtCompanyNameNine.Text = referenceSupplier.CompanyName;
                        txtCompanyAddressNine.Text = referenceSupplier.CompanyAddress;
                        txtSuplCityTwelve.Text = referenceSupplier.City;

                        ddlCountryTwelve.SelectedValue = referenceSupplier.FK_Country.ConvertToString();//005-Sooraj
                        BindAssociateState(ddlCountryTwelve); //005-Sooraj
                        ddlStateTwelve.SelectedValue = Convert.ToString(referenceSupplier.FK_State);//005-Sooraj
                        ddlSupSpeaksEnglishNine.SelectedValue = referenceSupplier.IsSpeaksEnglish.ConvertToString(); //007


                        txtSuplZipNine.Text = referenceSupplier.ZipCode;
                        txtOtherStateTwelve.Text = referenceSupplier.OtherState;
                        txtContactNameNine.Text = referenceSupplier.ContactName;
                        string SuplNinephnolength = referenceSupplier.Phone;
                        objCommon.loadphone(txtSuplNinePhoneOne, txtSuplNinePhoneTwo, txtSuplNinePhoneThree, SuplNinephnolength);
                        break;
                    case 9:
                        spnmore7.Style["display"] = "";
                        txtCompanyNameTen.Text = referenceSupplier.CompanyName;
                        txtCompanyAddressTen.Text = referenceSupplier.CompanyAddress;
                        txtSuplCityThirteen.Text = referenceSupplier.City;

                        ddlCountryThirteen.SelectedValue = referenceSupplier.FK_Country.ConvertToString();//005-Sooraj
                        BindAssociateState(ddlCountryThirteen); //005-Sooraj
                        ddlStateThirteen.SelectedValue = Convert.ToString(referenceSupplier.FK_State);//005-Sooraj
                        ddlSupSpeaksEnglishTen.SelectedValue = referenceSupplier.IsSpeaksEnglish.ConvertToString(); //007

                        txtSuplZipTen.Text = referenceSupplier.ZipCode;
                        txtOtherStateThirteen.Text = referenceSupplier.OtherState;
                        txtContactNameTen.Text = referenceSupplier.ContactName;
                        string SuplTenphnolength = referenceSupplier.Phone;
                        objCommon.loadphone(txtSuplTenPhoneOne, txtSuplTenPhoneTwo, txtSuplTenPhoneThree, SuplTenphnolength);
                        break;
                }
            }

            #endregion

        }
    }

    //To Check phone validations and update it
    public long InsertProjectReferencesForSaveAndClose(int status)
    {
        string mainPhone, secondPhone, thirdPhone;
        if ((txtMainPhnoOne.Text + txtMainPhnoTwo.Text + txtMainPhnoThree.Text).Length > 0)
        {
            mainPhone = objCommon.Phoneformat(txtMainPhnoOne.Text.Trim(), txtMainPhnoTwo.Text.Trim(), txtMainPhnoThree.Text.Trim(), txtMainPhnoThree);//006
        }
        else
        {
            mainPhone = null;
        }
        if ((txtSecondPhnoOne.Text + txtSecondPhnoTwo.Text + txtSecondPhnoThree.Text).Length > 0)
        {
            secondPhone = objCommon.Phoneformat(txtSecondPhnoOne.Text.Trim(), txtSecondPhnoTwo.Text.Trim(), txtSecondPhnoThree.Text.Trim(), txtSecondPhnoThree);//006
        }
        else
        {
            secondPhone = null;
        }
        if ((txtThirdPhnoOne.Text + txtThirdPhnoTwo.Text + txtThirdPhnoThree.Text).Length > 0)
        {
            thirdPhone = objCommon.Phoneformat(txtThirdPhnoOne.Text.Trim(), txtThirdPhnoTwo.Text.Trim(), txtThirdPhnoThree.Text.Trim(), txtThirdPhnoThree);//006
        }
        else
        {
            thirdPhone = null;
        }
        returnRefId = objReferences.InsertReferences(projectsCompleted, Convert.ToInt64(Session["VendorId"]), Convert.ToByte(status));
        return returnRefId;
    }

    // SOC

    // Set Valid Chars and MaxLength for ZipCode textbox
    private void SetValidCharsAndLength(DropDownList dropDownList, TextBox textBox, AjaxControlToolkit.FilteredTextBoxExtender filteredTextBoxExtender, RegularExpressionValidator regvValidator, HtmlControl tableRow, HtmlControl tableRowlbl)
    {
        //003-sooraj commented to apply Internationalization
        //if (dropDownList.SelectedIndex > 0)
        //{
        //    if (dropDownList.SelectedItem.Text.Trim().Equals("Other"))
        //    {
        //        tableRow.Style["display"] = "";
        //        regvValidator.ValidationGroup = "State";
        //        textBox.MaxLength = 10;
        //        textBox.Width = Unit.Pixel(80);
        //        filteredTextBoxExtender.ValidChars = " ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0";
        //    }
        //    else
        //    {
        //        tableRow.Style["display"] = "None";
        //        regvValidator.ValidationGroup = "Supplier";
        //        filteredTextBoxExtender.ValidChars = "0,1,2,3,4,5,6,7,8,9";
        //        textBox.MaxLength = 5;
        //        textBox.Width = Unit.Pixel(40);
        //    }
        //}

        //003-sooraj added 

        objCommon.SetValidCharsAndLength(dropDownList, textBox, filteredTextBoxExtender, regvValidator, tableRow, "Supplier", tableRowlbl);
    }

    // Clear Control values
    private void ResetSupplierReferenceValues(HtmlTableRow tableRow, TextBox companyNameText, TextBox companyAddress, TextBox cityText, DropDownList stateDropDown, TextBox zipText,
                                TextBox contactNameText, TextBox phone1, TextBox phone2, TextBox phone3, TextBox otherStateText, HtmlControl otherStateRow,
                                RegularExpressionValidator regularExprValidator, AjaxControlToolkit.FilteredTextBoxExtender filteredTextBoxExtender)
    {
        tableRow.Style["display"] = "none";
        companyNameText.Text = "";
        companyAddress.Text = "";
        cityText.Text = "";
        stateDropDown.SelectedIndex = 0;
        zipText.Text = "";
        contactNameText.Text = "";
        phone1.Text = "";
        phone2.Text = "";
        phone3.Text = "";
        otherStateText.Text = "";
        otherStateRow.Style["display"] = "none";

        //
        otherStateRow.Style["display"] = "None";
        regularExprValidator.ValidationGroup = "Supplier";
        //  filteredTextBoxExtender.ValidChars = "0,1,2,3,4,5,6,7,8,9";
        filteredTextBoxExtender.ValidChars = objCommon.ZipCodeValidCharacters;//003-sooraj
        zipText.MaxLength = 5;
        zipText.Width = Unit.Pixel(40);

    }

    private void ResetProjectReferenceValues(HtmlTableRow tableRow1, TextBox textBox1, TextBox textBox2, TextBox textBox3, TextBox textBox4, DropDownList dropDown4, TextBox textBox5,
                        TextBox textBox6, TextBox textBox7, TextBox textBox8, TextBox textBox9, DropDownList dropDown1, DropDownList dropDown2,
                                    DropDownList dropDown3, HtmlTableRow tableRow2, CheckBox checkBox, TextBox textBox10)
    {
        tableRow1.Style["display"] = "none";
        textBox1.Text = "";
        textBox2.Text = "";
        textBox3.Text = "";
        textBox4.Text = "";
        textBox5.Text = "";
        textBox6.Text = "";
        textBox7.Text = "";
        textBox8.Text = "";
        textBox9.Text = "";
        dropDown1.SelectedIndex = 0;
        dropDown2.SelectedIndex = 0;
        dropDown3.SelectedIndex = 0;
        if (dropDown4.SelectedIndex > 0)
            dropDown4.SelectedIndex = 0;
    }

    private void ResetAdditionalInformation(HtmlGenericControl tableRow, TextBox textBox1, TextBox textBox2, TextBox textBox3)
    {
        tableRow.Style["display"] = "none";
        textBox1.Text = "";
        textBox2.Text = "";
        textBox3.Text = "";
    }
    // EOC

    #endregion

    #region Validation Control Events

    protected void CusvOtherStateOne_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA1.Checked)
        {
            if (ddlStateOne.SelectedIndex <= 0)
            {
                cusvOtherStateOne.ErrorMessage = "Please select state under reference one";
                args.IsValid = false;
            }
            else if (ddlStateOne.SelectedIndex > 0)
            {
                if (ddlStateOne.SelectedItem.Text == "Other")
                {
                    if (string.IsNullOrEmpty(txtOtherStateOne.Text.Trim()))
                    {
                        cusvOtherStateOne.ErrorMessage = "Please select state under reference one";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void cusvStateReferencesFour_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateReferencesFour.SelectedIndex > 0)
        {
            if (ddlStateReferencesFour.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtStateReferencesFour.Text.Trim()))
                {
                    cusvStateReferencesFour.ErrorMessage = "Please select state under reference four";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cusvOtherReferencesFive_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateReferencesFive.SelectedIndex > 0)
        {
            if (ddlStateReferencesFive.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtStateReferencesFive.Text.Trim()))
                {
                    cusvOtherReferencesFive.ErrorMessage = "Please select state under reference five";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cusvStateReferencesSix_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateReferencesSix.SelectedIndex > 0)
        {
            if (ddlStateReferencesSix.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtStateReferencesSix.Text.Trim()))
                {
                    cusvStateReferencesSix.ErrorMessage = "Please select state under reference six";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cusvStateReferencesSeven_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateReferencesSeven.SelectedIndex > 0)
        {
            if (ddlStateReferencesSeven.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtStateReferencesSeven.Text.Trim()))
                {
                    cusvStateReferencesSeven.ErrorMessage = "Please select state under reference seven";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cusvstateReferencesEight_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateReferencesEight.SelectedIndex > 0)
        {
            if (ddlStateReferencesEight.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtStateRefencesEight.Text.Trim()))
                {

                    cusvstateReferencesEight.ErrorMessage = "Please select state under reference eight";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void custStateReferencesNine_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateReferencesNine.SelectedIndex > 0)
        {
            if (ddlStateReferencesNine.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtStateReferencesNine.Text.Trim()))
                {
                    custStateReferencesNine.ErrorMessage = "Please select state under reference nine";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void cusvStateReferencesTen_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateReferencesTen.SelectedIndex > 0)
        {
            if (ddlStateReferencesTen.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtStateReferencesTen.Text.Trim()))
                {
                    cusvStateReferencesTen.ErrorMessage = "Please select state under reference ten";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void CusvOtherStateTwo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA2.Checked)
        {
            if (ddlStateTwo.SelectedIndex <= 0)
            {
                cusvOtherStateTwo.ErrorMessage = "Please select state under reference two";
                args.IsValid = false;
            }
            else if (ddlStateTwo.SelectedIndex > 0)
            {
                if (ddlStateTwo.SelectedItem.Text.Equals("Other"))
                {
                    if (string.IsNullOrEmpty(txtOtherStateTwo.Text.Trim()))
                    {
                        cusvOtherStateTwo.ErrorMessage = "Please select state under reference two";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void CusvOtherStateThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA3.Checked)
        {
            if (ddlStateThree.SelectedIndex <= 0)
            {
                cusvOtherStateThree.ErrorMessage = "Please select state under reference three";
                args.IsValid = false;
            }
            else if (ddlStateThree.SelectedIndex > 0)
            {
                if (ddlStateThree.SelectedItem.Text.Equals("Other"))
                {
                    if (string.IsNullOrEmpty(txtOtherStateThree.Text.Trim()))
                    {
                        cusvOtherStateThree.ErrorMessage = "Please select state under reference three";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void CusvOtherStateFour_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateFour.SelectedIndex > 0)
        {
            if (ddlStateFour.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateFour.Text.Trim()))
                {
                    cusvOtherStateFour.ErrorMessage = "Please select state under supplier/credit one";
                    args.IsValid = false;
                }
            }
        }
        else if (ddlStateFour.SelectedIndex <= 0)
        {
            cusvOtherStateFour.ErrorMessage = "Please select state under supplier/credit one";
            args.IsValid = false;
        }
    }

    protected void CusvOtherStateFive_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateFive.SelectedIndex > 0)
        {
            if (ddlStateFive.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateFive.Text.Trim()))
                {
                    cusvOtherStateFive.ErrorMessage = "Please select state under supplier/credit two";
                    args.IsValid = false;
                }
            }
        }
        else if (ddlStateFive.SelectedIndex <= 0)
        {
            cusvOtherStateFive.ErrorMessage = "Please select state under supplier/credit two";
            args.IsValid = false;
        }
    }

    protected void CusvOtherStateSix_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateSix.SelectedIndex > 0)
        {
            if (ddlStateSix.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateSix.Text.Trim()))
                {
                    cusvOtherStateSix.ErrorMessage = "Please select state under supplier/credit three";
                    args.IsValid = false;
                }
            }
        }
        else if (ddlStateSix.SelectedIndex <= 0)
        {
            cusvOtherStateSix.ErrorMessage = "Please select state under supplier/credit three";
            args.IsValid = false;
        }
    }

    protected void CusvOtherStateSeven_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateSeven.SelectedIndex > 0 && spnmore1.Style["display"] == "")
        {
            if (ddlStateSeven.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateSeven.Text.Trim()))
                {
                    cusvOtherStateSeven.ErrorMessage = "Please select state under supplier/credit four";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void CusvOtherStateEight_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateEight.SelectedIndex > 0 && spnmore2.Style["display"] == "")
        {
            if (ddlStateEight.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateEight.Text.Trim()))
                {
                    cusvOtherStateEight.ErrorMessage = "Please select state under supplier/credit five";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void CusvOtherStateNine_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateNine.SelectedIndex > 0 && spnmore3.Style["display"] == "")
        {
            if (ddlStateNine.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateNine.Text.Trim()))
                {
                    cusvOtherStateNine.ErrorMessage = "Please select state under supplier/credit six ";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void CusvOtherStateTen_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateTen.SelectedIndex > 0 && spnmore4.Style["display"] == "")
        {
            if (ddlStateTen.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateTen.Text.Trim()))
                {
                    cusvOtherStateTen.ErrorMessage = "Please select state under supplier/credit seven";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void CusvOtherStateEleven_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateEleven.SelectedIndex > 0 && spnmore5.Style["display"] == "")
        {
            if (ddlStateEleven.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateEleven.Text.Trim()))
                {
                    cusvOtherStateEleven.ErrorMessage = "Please select state under supplier/credit eight";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void CusvOtherStateTwelve_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateTwelve.SelectedIndex > 0 && spnmore6.Style["display"] == "")
        {
            if (ddlStateTwelve.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateTwelve.Text.Trim()))
                {
                    cusvOtherStateTwelve.ErrorMessage = "Please select state under supplier/credit nine";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void CusvOtherStateThirteen_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateThirteen.SelectedIndex > 0 && spnmore7.Style["display"] == "")
        {
            if (ddlStateThirteen.SelectedItem.Text.Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateThirteen.Text.Trim()))
                {
                    cusvOtherStateThirteen.ErrorMessage = "Please select state under supplier/credit ten";
                    args.IsValid = false;
                }
            }
        }
    }

    protected void CusvrefMainPhno_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA1.Checked)
        {
            if (string.IsNullOrEmpty(txtMainPhnoOne.Text) && string.IsNullOrEmpty(txtMainPhnoTwo.Text) && string.IsNullOrEmpty(txtMainPhnoThree.Text))
            {
                cusvrefMainPhno.ErrorMessage = "Please enter contact phone number under reference one";
                args.IsValid = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtMainPhnoOne.Text) || !string.IsNullOrEmpty(txtMainPhnoTwo.Text) || !string.IsNullOrEmpty(txtMainPhnoThree.Text))
                {
                    bool isValid = objCommon.ValidatePhoneformat(txtMainPhnoOne.Text, txtMainPhnoTwo.Text, txtMainPhnoThree.Text, txtMainPhnoThree); //006
                    if (!isValid)
                    {
                        cusvrefMainPhno.ErrorMessage = "Please enter contact phone number in valid format under reference one";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void cusvFourthPhnoThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtFourthPhnoOne.Text) || !string.IsNullOrEmpty(txtFourthPhnoTwo.Text) || !string.IsNullOrEmpty(txtFourthPhnoThree.Text))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtFourthPhnoOne.Text, txtFourthPhnoTwo.Text, txtFourthPhnoThree.Text, txtFourthPhnoThree);//006
            if (!isValid)
            {
                cusvFourthPhnoThree.ErrorMessage = "Please enter contact phone number in valid format under reference four";
                args.IsValid = false;
            }
        }
    }

    protected void CusvFifthPhnoThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtFifthPhnoOne.Text) || !string.IsNullOrEmpty(txtFifthPhnoTwo.Text) || !string.IsNullOrEmpty(txtFifthPhnoThree.Text))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtFifthPhnoOne.Text, txtFifthPhnoTwo.Text, txtFifthPhnoThree.Text, txtFifthPhnoThree);//006
            if (!isValid)
            {
                cusvFifthPhnoThree.ErrorMessage = "Please enter contact phone number in valid format under reference five";
                args.IsValid = false;
            }
        }
    }

    protected void cusvSixthPhnoThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtSixthPhnoOne.Text) || !string.IsNullOrEmpty(txtSixthPhnoTwo.Text) || !string.IsNullOrEmpty(txtSixthPhnoThree.Text))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtSixthPhnoOne.Text, txtSixthPhnoTwo.Text, txtSixthPhnoThree.Text, txtSixthPhnoThree);//006
            if (!isValid)
            {
                cusvSixthPhnoThree.ErrorMessage = "Please enter contact phone number in valid format under reference six";
                args.IsValid = false;
            }
        }
    }

    protected void CusvSevethPhnoThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtSeventhPhnoOne.Text) || !string.IsNullOrEmpty(txtSeventhPhnoTwo.Text) || !string.IsNullOrEmpty(txtSeventhPhnoThree.Text))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtSeventhPhnoOne.Text, txtSeventhPhnoTwo.Text, txtSeventhPhnoThree.Text, txtSeventhPhnoThree);//006
            if (!isValid)
            {
                custSeventhPhnoThree.ErrorMessage = "Please enter contact phone number in valid format under reference seven";
                args.IsValid = false;
            }
        }
    }

    protected void cusvEightPhnoThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtEighthPhnoOne.Text) || !string.IsNullOrEmpty(txtEighthPhnoTwo.Text) || !string.IsNullOrEmpty(txtEighthPhnoThree.Text))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtEighthPhnoOne.Text, txtEighthPhnoTwo.Text, txtEighthPhnoThree.Text, txtEighthPhnoThree);//006
            if (!isValid)
            {
                cusvEightPhnoThree.ErrorMessage = "Please enter contact phone number in valid format under reference eight";
                args.IsValid = false;
            }
        }
    }

    protected void cusvninthPhnoThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtNinthPhnoOne.Text) || !string.IsNullOrEmpty(txtNinthPhnoTwo.Text) || !string.IsNullOrEmpty(txtNinthPhnoThree.Text))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtNinthPhnoOne.Text, txtNinthPhnoTwo.Text, txtNinthPhnoThree.Text, txtNinthPhnoThree);//006
            if (!isValid)
            {
                cusvninthPhnoThree.ErrorMessage = "Please enter contact phone number in valid format under reference nine";
                args.IsValid = false;
            }
        }
    }

    protected void custTenthPhnoThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtTenthPhnoOne.Text.Trim()) || !string.IsNullOrEmpty(txtTenthPhnoTwo.Text.Trim()) || !string.IsNullOrEmpty(txtTenthPhnoThree.Text.Trim()))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtTenthPhnoOne.Text.Trim(), txtTenthPhnoTwo.Text.Trim(), txtTenthPhnoThree.Text.Trim(), txtTenthPhnoThree);//006
            if (!isValid)
            {
                custTenthPhnoThree.ErrorMessage = "Please enter contact phone number in valid format under reference ten";
                args.IsValid = false;
            }
        }
    }

    protected void CusvrefSecondPhnoTwo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA2.Checked)
        {
            if (string.IsNullOrEmpty(txtSecondPhnoOne.Text) && string.IsNullOrEmpty(txtSecondPhnoTwo.Text) && string.IsNullOrEmpty(txtSecondPhnoThree.Text))
            {
                cusvrefSecondPhnoTwo.ErrorMessage = "Please enter contact phone number under reference two";
                args.IsValid = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtSecondPhnoOne.Text) || !string.IsNullOrEmpty(txtSecondPhnoTwo.Text) || !string.IsNullOrEmpty(txtSecondPhnoThree.Text))
                {
                    bool isValid = objCommon.ValidatePhoneformat(txtSecondPhnoOne.Text, txtSecondPhnoTwo.Text, txtSecondPhnoThree.Text, txtSecondPhnoThree);//006
                    if (!isValid)
                    {
                        cusvrefSecondPhnoTwo.ErrorMessage = "Please enter contact phone number in valid format under reference two";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void CusvhirdPhnoThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA3.Checked)
        {
            if (string.IsNullOrEmpty(txtThirdPhnoOne.Text) && string.IsNullOrEmpty(txtThirdPhnoTwo.Text) && string.IsNullOrEmpty(txtThirdPhnoThree.Text))
            {
                cusvhirdPhnoThree.ErrorMessage = "Please enter contact phone number under reference three";
                args.IsValid = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtThirdPhnoOne.Text) || !string.IsNullOrEmpty(txtThirdPhnoTwo.Text) || !string.IsNullOrEmpty(txtThirdPhnoThree.Text))
                {
                    bool isValid = objCommon.ValidatePhoneformat(txtThirdPhnoOne.Text, txtThirdPhnoTwo.Text, txtThirdPhnoThree.Text, txtThirdPhnoThree);//006
                    if (!isValid)
                    {
                        cusvhirdPhnoThree.ErrorMessage = "Please enter contact phone number in valid format under reference three";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void cusvWorkComplated1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA1.Checked)
        {
            if (string.IsNullOrEmpty(txtWorkCompletedOne.Text))
            {
                cusvWorkComplated1.ErrorMessage = "Please enter description of work completed under reference one";
                args.IsValid = false;
            }
        }
    }

    protected void cusvContactName1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA1.Checked)
        {
            if (string.IsNullOrEmpty(txtCtrContactNameOne.Text))
            {
                cusvContactName1.ErrorMessage = "Please enter contact name under reference one";
                args.IsValid = false;
            }
        }
    }

    protected void cusvContractorNameOne_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA1.Checked)
        {
            if (string.IsNullOrEmpty(txtContractorNameOne.Text))
            {
                var selectedItem = ddlProjectReferenceTypeOne.SelectedItem;

                cusvContractorNameOne.ErrorMessage = (selectedItem == null) ? "Please enter  general contractor under reference one" : "Please enter " + selectedItem.Text + " under reference one";
                args.IsValid = false;
            }
        }
    }

    protected void cusvApproximate1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA1.Checked)
        {
            if (string.IsNullOrEmpty(txtApproximateContractAmtOne.Text))
            {
                cusvApproximate1.ErrorMessage = "Please enter approximate contract value under reference one";
                args.IsValid = false;
            }
        }
    }

    protected void cusvCity1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA1.Checked)
        {
            if (string.IsNullOrEmpty(txtCity.Text))
            {
                cusvCity1.ErrorMessage = "Please enter city under reference one";
                args.IsValid = false;
            }
        }
    }

    protected void cusvProjectNameOne_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA1.Checked)
        {
            if (string.IsNullOrEmpty(txtProjectNameOne.Text))
            {
                cusvProjectNameOne.ErrorMessage = "Please enter project name under reference one";
                args.IsValid = false;
            }
        }
    }

    protected void cusvWorkComplated2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA2.Checked)
        {
            if (string.IsNullOrEmpty(txtWorkCompletedTwo.Text))
            {
                cusvWorkComplated2.ErrorMessage = "Please enter description of work completed under reference two";
                args.IsValid = false;
            }
        }
    }

    protected void cusvContactName2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA2.Checked)
        {
            if (string.IsNullOrEmpty(txtCtrContactNameTwo.Text))
            {
                cusvContactName2.ErrorMessage = "Please enter contact name under reference two";
                args.IsValid = false;
            }
        }
    }

    protected void cusvContractorNameTwo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA2.Checked)
        {
            if (string.IsNullOrEmpty(txtContractorNameTwo.Text))
            {
                var selectedItem = ddlProjectReferenceTypeTwo.SelectedItem;

                cusvContractorNameTwo.ErrorMessage = (selectedItem == null) ? "Please enter  general contractor under reference two" : "Please enter " + selectedItem.Text + " under reference two";
                args.IsValid = false;
            }
        }
    }

    protected void cusvApproximate2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA2.Checked)
        {
            if (string.IsNullOrEmpty(txtApproximateContractAmtTwo.Text))
            {
                cusvApproximate2.ErrorMessage = "Please enter approximate contract value under reference two";
                args.IsValid = false;
            }
        }
    }

    protected void cusvCity2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA2.Checked)
        {
            if (string.IsNullOrEmpty(txtCityTwo.Text))
            {
                cusvCity2.ErrorMessage = "Please enter city under reference two";
                args.IsValid = false;
            }
        }
    }

    protected void cusvProjectNameTwo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA2.Checked)
        {
            if (string.IsNullOrEmpty(txtProjectNameTwo.Text))
            {
                cusvProjectNameTwo.ErrorMessage = "Please enter project name under reference two";
                args.IsValid = false;
            }
        }
    }

    protected void cusvWorkComplated3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA3.Checked)
        {
            if (string.IsNullOrEmpty(txtWorkCompletedThree.Text))
            {
                cusvWorkComplated3.ErrorMessage = "Please enter description of work completed under reference three";
                args.IsValid = false;
            }
        }
    }

    protected void cusvContactName3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA3.Checked)
        {
            if (string.IsNullOrEmpty(txtCtrContactNameThree.Text))
            {
                cusvContactName3.ErrorMessage = "Please enter contact name under reference three";
                args.IsValid = false;
            }
        }
    }

    protected void cusvContractorNameThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA3.Checked)
        {
            if (string.IsNullOrEmpty(txtContractorNameThree.Text))
            {
                var selectedItem = ddlProjectReferenceTypeThree.SelectedItem;

                cusvContractorNameThree.ErrorMessage = (selectedItem == null) ? "Please enter  general contractor under reference three" : "Please enter " + selectedItem.Text + " under reference three";

                args.IsValid = false;
            }
        }
    }

    protected void cusvApproximate3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA3.Checked)
        {
            if (string.IsNullOrEmpty(txtApproximateContractAmtThree.Text))
            {
                cusvApproximate3.ErrorMessage = "Please enter approximate contract value under reference three";
                args.IsValid = false;
            }
        }
    }

    protected void cusvCity3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA3.Checked)
        {
            if (string.IsNullOrEmpty(txtCityThree.Text))
            {
                cusvCity3.ErrorMessage = "Please enter city under reference three";
                args.IsValid = false;
            }
        }
    }

    protected void cusvProjectNameThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA3.Checked)
        {
            if (string.IsNullOrEmpty(txtProjectNameThree.Text))
            {
                cusvProjectNameThree.ErrorMessage = "Please enter project name under reference three";
                args.IsValid = false;
            }
        }
    }

    protected void CusvSuplMainPhon_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (((string.IsNullOrEmpty(txtSuplMainPhoneOne.Text)) && (string.IsNullOrEmpty(txtSuplMainPhoneTwo.Text)) && (string.IsNullOrEmpty(txtSuplMainPhoneThree.Text))))
        {
            cusvSuplMainPhon.ErrorMessage = "Please enter phone number under supplier/credit one";
            args.IsValid = false;
        }
        else
        {
            bool isValid = objCommon.ValidatePhoneformat(txtSuplMainPhoneOne.Text, txtSuplMainPhoneTwo.Text, txtSuplMainPhoneThree.Text, txtSuplMainPhoneThree);//006
            if (!isValid)
            {
                cusvSuplMainPhon.ErrorMessage = "Please enter phone number in valid format under supplier/credit one";
                args.IsValid = false;
            }
        }
    }

    protected void CusvSuplSecondPhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((string.IsNullOrEmpty(txtSuplSecondPhoneOne.Text)) && (string.IsNullOrEmpty(txtSuplSecondPhoneTwo.Text)) && (string.IsNullOrEmpty(txtSuplSecondPhoneThree.Text)))
        {
            cusvSuplSecondPhone.ErrorMessage = "Please enter phone number under supplier/credit two";
            args.IsValid = false;
        }
        else
        {
            bool isValid = objCommon.ValidatePhoneformat(txtSuplSecondPhoneOne.Text, txtSuplSecondPhoneTwo.Text, txtSuplSecondPhoneThree.Text, txtSuplSecondPhoneThree);//006
            if (!isValid)
            {
                cusvSuplSecondPhone.ErrorMessage = "Please enter phone number in valid format under supplier/credit two";
                args.IsValid = false;
            }
        }
    }

    protected void CusvSuplThirdPhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((string.IsNullOrEmpty(txtSuplThirdPhoneOne.Text)) && (string.IsNullOrEmpty(txtSuplThirdPhoneTwo.Text)) && (string.IsNullOrEmpty(txtSuplThirdPhoneThree.Text)))
        {
            cusvSuplThirdPhone.ErrorMessage = "Please enter phone number under supplier/credit three";
            args.IsValid = false;
        }
        else
        {
            bool isValid = objCommon.ValidatePhoneformat(txtSuplThirdPhoneOne.Text, txtSuplThirdPhoneTwo.Text, txtSuplThirdPhoneThree.Text, txtSuplThirdPhoneThree);//006
            if (!isValid)
            {
                cusvSuplThirdPhone.ErrorMessage = "Please enter phone number in valid format under supplier/credit three";
                args.IsValid = false;
            }
        }
    }

    protected void cusSuplFourthPhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (spnmore1.Style["display"] != "none")
        {
            if (string.IsNullOrEmpty(txtSuplFourthPhoneOne.Text) && string.IsNullOrEmpty(txtSuplFourthPhoneTwo.Text) && string.IsNullOrEmpty(txtSuplFourthPhoneThree.Text))
            {
                args.IsValid = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtSuplFourthPhoneOne.Text) && !string.IsNullOrEmpty(txtSuplFourthPhoneTwo.Text) && !string.IsNullOrEmpty(txtSuplFourthPhoneThree.Text))
                {
                    bool isValid = objCommon.ValidatePhoneformat(txtSuplFourthPhoneOne.Text, txtSuplFourthPhoneTwo.Text, txtSuplFourthPhoneThree.Text, txtSuplFourthPhoneThree);//006
                    if (!isValid)
                    {
                        cusvSuplFourthPhone.ErrorMessage = "Please enter phone number in valid format for additional section one";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void CusvSuplFivePhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (spnmore2.Style["display"] != "none")
        {
            if (string.IsNullOrEmpty(txtSuplFivePhoneOne.Text) && string.IsNullOrEmpty(txtSuplFivePhoneTwo.Text) && string.IsNullOrEmpty(txtSuplFivePhoneThree.Text))
            {
                args.IsValid = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtSuplFivePhoneOne.Text) && !string.IsNullOrEmpty(txtSuplFivePhoneTwo.Text) && !string.IsNullOrEmpty(txtSuplFivePhoneThree.Text))
                {
                    bool isValid = objCommon.ValidatePhoneformat(txtSuplFivePhoneOne.Text, txtSuplFivePhoneTwo.Text, txtSuplFivePhoneThree.Text, txtSuplFivePhoneThree);//006
                    if (!isValid)
                    {
                        cusvSuplFivePhone.ErrorMessage = "Please enter phone number in valid format under supplier/credit two";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void CusvSuplSixPhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (spnmore3.Style["display"] != "none")
        {
            if (string.IsNullOrEmpty(txtSuplSixPhoneOne.Text) && string.IsNullOrEmpty(txtSuplSixPhoneTwo.Text) && string.IsNullOrEmpty(txtSuplSixPhoneThree.Text))
            {
                args.IsValid = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtSuplSixPhoneOne.Text) && !string.IsNullOrEmpty(txtSuplSixPhoneTwo.Text) && !string.IsNullOrEmpty(txtSuplSixPhoneThree.Text))
                {
                    bool isValid = objCommon.ValidatePhoneformat(txtSuplSixPhoneOne.Text, txtSuplSixPhoneTwo.Text, txtSuplSixPhoneThree.Text, txtSuplSixPhoneThree);//006
                    if (!isValid)
                    {
                        cusvSuplSixPhone.ErrorMessage = "Please enter phone number in valid format under supplier/credit three";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void CusvSuplSevenPhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (spnmore4.Style["display"] != "none")
        {
            if (string.IsNullOrEmpty(txtSuplSevenPhoneOne.Text) && string.IsNullOrEmpty(txtSuplSixPhoneTwo.Text) && string.IsNullOrEmpty(txtSuplSevenPhoneThree.Text))
            {
                args.IsValid = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtSuplSevenPhoneOne.Text) && !string.IsNullOrEmpty(txtSuplSixPhoneTwo.Text) && !string.IsNullOrEmpty(txtSuplSevenPhoneThree.Text))
                {
                    bool isValid = objCommon.ValidatePhoneformat(txtSuplSevenPhoneOne.Text, txtSuplSevenPhoneTwo.Text, txtSuplSevenPhoneThree.Text, txtSuplSevenPhoneThree);//006
                    if (!isValid)
                    {
                        cusvSuplSevenPhone.ErrorMessage = "Please enter phone number in valid format for additional section four";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void CusvSuplEightPhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (spnmore5.Style["display"] != "none")
        {
            if (string.IsNullOrEmpty(txtSuplEightPhoneOne.Text) && string.IsNullOrEmpty(txtSuplEightPhoneTwo.Text) && string.IsNullOrEmpty(txtSuplEightPhoneThree.Text))
            {
                args.IsValid = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtSuplEightPhoneOne.Text) && !string.IsNullOrEmpty(txtSuplEightPhoneTwo.Text) && !string.IsNullOrEmpty(txtSuplEightPhoneThree.Text))
                {
                    bool isValid = objCommon.ValidatePhoneformat(txtSuplEightPhoneOne.Text, txtSuplEightPhoneTwo.Text, txtSuplEightPhoneThree.Text, txtSuplEightPhoneThree);//006
                    if (!isValid)
                    {
                        cusvSuplEightPhone.ErrorMessage = "Please enter phone number in valid format for additional section five";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void CusvSuplNinePhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (spnmore6.Style["display"] != "none")
        {
            if (string.IsNullOrEmpty(txtSuplNinePhoneOne.Text) && string.IsNullOrEmpty(txtSuplNinePhoneTwo.Text) && string.IsNullOrEmpty(txtSuplNinePhoneThree.Text))
            {
                args.IsValid = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtSuplNinePhoneOne.Text) && !string.IsNullOrEmpty(txtSuplNinePhoneTwo.Text) && !string.IsNullOrEmpty(txtSuplNinePhoneThree.Text))
                {
                    bool isValid = objCommon.ValidatePhoneformat(txtSuplNinePhoneOne.Text, txtSuplNinePhoneTwo.Text, txtSuplNinePhoneThree.Text, txtSuplNinePhoneThree);//006
                    if (!isValid)
                    {
                        cusvSuplNinePhone.ErrorMessage = "Please enter phone number in valid format for additional section six";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void CusvSuplTenPhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (spnmore7.Style["display"] != "none")
        {
            if (string.IsNullOrEmpty(txtSuplTenPhoneOne.Text) && string.IsNullOrEmpty(txtSuplTenPhoneTwo.Text) && string.IsNullOrEmpty(txtSuplTenPhoneThree.Text))
            {
                args.IsValid = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtSuplTenPhoneOne.Text) && !string.IsNullOrEmpty(txtSuplTenPhoneTwo.Text) && !string.IsNullOrEmpty(txtSuplTenPhoneThree.Text))
                {
                    bool isValid = objCommon.ValidatePhoneformat(txtSuplTenPhoneOne.Text, txtSuplTenPhoneTwo.Text, txtSuplTenPhoneThree.Text, txtSuplTenPhoneThree);//006
                    if (!isValid)
                    {
                        cusvSuplTenPhone.ErrorMessage = "Please enter phone number in valid format for additional section seven";
                        args.IsValid = false;
                    }
                }
            }
        }
    }

    protected void cusvExplain1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA1.Checked)
        {
            if (string.IsNullOrEmpty(txtExplain1.Text))
            {
                cusvExplain1.ErrorMessage = "Please enter explanation under references one";
                args.IsValid = false;
            }
        }
    }

    protected void cusvExplain2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA2.Checked)
        {
            if (string.IsNullOrEmpty(txtExplain2.Text))
            {
                cusvExplain2.ErrorMessage = "Please enter explanation under references two";
                args.IsValid = false;
            }
        }
    }

    protected void cusvExplain3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA3.Checked)
        {
            if (string.IsNullOrEmpty(txtExplain3.Text))
            {
                cusvExplain3.ErrorMessage = "Please enter explanation under references three";
                args.IsValid = false;
            }
        }
    }

    protected void cusvMonth1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA1.Checked)
        {
            if (ddlMonth1.SelectedIndex <= 0 && ddlYear1.SelectedIndex <= 0)
            {
                cusvMonth1.ErrorMessage = "Please select completion date under references one";
                args.IsValid = false;
            }
            else if (ddlMonth1.SelectedIndex <= 0 || ddlYear1.SelectedIndex <= 0)
            {
                cusvMonth1.ErrorMessage = "Please select valid completion date under references one";
                args.IsValid = false;
            }
        }
    }

    protected void cusvMonth2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA2.Checked)
        {
            if (ddlMonth2.SelectedIndex <= 0 && ddlYear2.SelectedIndex <= 0)
            {
                cusvMonth2.ErrorMessage = "Please select completion date under references two";
                args.IsValid = false;
            }
            else if (ddlMonth2.SelectedIndex <= 0 || ddlYear2.SelectedIndex <= 0)
            {
                cusvMonth2.ErrorMessage = "Please select valid completion date under references two";
                args.IsValid = false;
            }
        }
    }

    protected void cusvMonth3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!chkNA3.Checked)
        {
            if (ddlMonth3.SelectedIndex <= 0 && ddlYear3.SelectedIndex <= 0)
            {
                cusvMonth3.ErrorMessage = "Please select completion date under references three";
                args.IsValid = false;
            }
            else if (ddlMonth3.SelectedIndex <= 0 || ddlYear3.SelectedIndex <= 0)
            {

                cusvMonth3.ErrorMessage = "Please select valid completion date under references three";
                args.IsValid = false;
            }
        }
    }

    protected void cusvMonth4_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!((ddlMonth4.SelectedIndex <= 0) && (ddlYear4.SelectedIndex <= 0)))
        {
            if (ddlMonth4.SelectedIndex <= 0 || ddlYear4.SelectedIndex <= 0)
            {

                cusvMonth4.ErrorMessage = "Please select valid completion date under references four";
                args.IsValid = false;
            }
        }
    }

    protected void cusvMonth5_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!((ddlMonth5.SelectedIndex <= 0) && (ddlYear5.SelectedIndex <= 0)))
        {
            if (ddlMonth5.SelectedIndex <= 0 || ddlYear5.SelectedIndex <= 0)
            {
                cusvMonth5.ErrorMessage = "Please select valid completion date under references five";
                args.IsValid = false;
            }
        }
    }

    protected void cusvMonth6_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!((ddlMonth6.SelectedIndex <= 0) && (ddlYear6.SelectedIndex <= 0)))
        {
            if (ddlMonth6.SelectedIndex <= 0 || ddlYear6.SelectedIndex <= 0)
            {
                cusvMonth6.ErrorMessage = "Please select valid completion date under references six";
                args.IsValid = false;
            }
        }
    }

    protected void cusvMonth7_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!((ddlMonth7.SelectedIndex <= 0) && (ddlYear7.SelectedIndex <= 0)))
        {
            if (ddlMonth7.SelectedIndex <= 0 || ddlYear7.SelectedIndex <= 0)
            {
                cusvMonth7.ErrorMessage = "Please select valid completion date under references seven";
                args.IsValid = false;
            }
        }
    }

    protected void cusvMonth8_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!((ddlMonth8.SelectedIndex <= 0) && (ddlYear8.SelectedIndex <= 0)))
        {
            if (ddlMonth8.SelectedIndex <= 0 || ddlYear8.SelectedIndex <= 0)
            {
                cusvMonth8.ErrorMessage = "Please select valid completion date under references eight";
                args.IsValid = false;
            }
        }
    }

    protected void cusvMonth9_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!((ddlMonth9.SelectedIndex <= 0) && (ddlYear9.SelectedIndex <= 0)))
        {
            if (ddlMonth9.SelectedIndex <= 0 || ddlYear9.SelectedIndex <= 0)
            {
                cusvMonth9.ErrorMessage = "Please select valid completion date under references nine";
                args.IsValid = false;
            }
        }
    }

    protected void cusvMonth10_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!((ddlMonth10.SelectedIndex <= 0) && (ddlYear10.SelectedIndex <= 0)))
        {
            if (ddlMonth10.SelectedIndex <= 0 || ddlYear10.SelectedIndex <= 0)
            {
                cusvMonth10.ErrorMessage = "Please select valid completion date under references ten";
                args.IsValid = false;
            }
        }
    }

    #endregion

}
