﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SessionOut.aspx.cs" Inherits="Common_SessionOut" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="contenttd">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="center">
                                                    <table width="50%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td height="50px">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div align="justify" class="sessiontextleft">
                                                                    <br />
                                                                    <span class="sessiontextbold">Sessions can expire for the following reasons:</span>
                                                                    <br />
                                                                    <br />
                                                                    1. A link must be clicked within 45 minutes or the session will expire.
                                                                    <br />
                                                                    <br />
                                                                    2. Browser "cookies" must be enabled or the session may expire.
                                                                    <br />
                                                                    <br />
                                                                    3. If for some reason, the server must be restarted, the session will expire.
                                                                    <br />
                                                                    <br />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
