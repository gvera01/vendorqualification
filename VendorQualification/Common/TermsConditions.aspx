﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TermsConditions.aspx.cs" Inherits="Common_TermsConditions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
        .style3
        {
            font-family: Arial;
            font-size: 14px;
            color: black;
            text-align: left;
        }
        .style4
        {
            font-family: Arial;
            font-size: 11px;
            color: black;
            text-align: left;
            width: 408px;
        }
    </style>
    
    <script type="text/javascript">
        function enableSubmit() {

            var check = window.document.getElementById("chkTermsConditions");
            
            if (document.form1.chkTermsConditions.checked == false)
                {
                    alert('Please agree to Terms & Conditions in order to continue.');
                    return false;
                }
            else 
                {
                    return true;
                }
        }
    
    </script>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbSubmit">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:VQFTopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="formhead">
                                                    Vendor - Terms and Conditions</td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="95%">
                                                        <tr>
                                                            <td class="style3" align="center">
                                                                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: small; font-weight: normal; font-family: arial; color: #000080">
                                                                <asp:Label ID="lblAgreement" runat="server" ForeColor="#003366"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;</td>
                                                            <%--<td class="formtdrt">
                                                                        <span class="mandatorystar">*</span>Comments
                                                                    </td>--%>
                                                        </tr>
                                                        </table>
                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="style4">
                                                                <asp:CheckBox ID="chkTermsConditions" runat="server" 
                                                                    Text="I hereby agree to these terms and conditions." />
                                                                &nbsp;
                                                                </td>
                                                            <td style="text-align: right">
                                                                <asp:ImageButton ID="imbSubmit" runat="server" ImageUrl="~/images/Submit.jpg" OnClick="imbSubmit_Click"
                                                                    CausesValidation="false" ToolTip="Submit" onclientclick="enableSubmit()" />&nbsp;<asp:ImageButton 
                                                                    ID="imbClose" CausesValidation="false" runat="server" ImageUrl="~/Images/close.jpg"
                                                                    ToolTip="Close" OnClick="imbClose_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
