﻿using System;
using System.Configuration;
using System.Text;
using System.Web.UI;


#region Comments
/*******************************************************************************************************************************************
 * 001 G. Vera 07/08/2014: Update the InProgress vendors that have expired to InComplete status
 * 002 G. vera 05/01/2015:  Removing the vendor status checks and allowing vendor to log in normally.  The status will be handled upon
 *     Vendor clicking the Update VQF red button.  This is an effort to eliminate activation calls to Dianne.
 * 
 * 
 ********************************************************************************************************************************************/
#endregion

public partial class Common_Home : CommonPage
{
    Common objCommon = new Common();
    clsRegistration objRegistration = new clsRegistration();

    protected void Page_Load(object sender, EventArgs e)
    {
        //G. Vera 07/03/2012 - Added code below
        if (!String.IsNullOrEmpty(Request.QueryString["timeout"]))
        {
            lblmsg.Text = "<li>" + "Your session has expired!" + "</li>";
            modlextndError.Show();
        }

        if (!Page.IsPostBack)
        {
            Session["VendorId"] = null;
            try
            {
                if (Request.UrlReferrer != null)
                {
                    string strPreviousURL = Request.UrlReferrer.ToString();
                    if (strPreviousURL.Contains("admin"))
                    {
                        Session["EmployeeId"] = null;
                        Session["EmployeeName"] = null;
                        Session["EmployeeTitle"] = null;
                        Session["EmployeeNumber"] = null;
                        Session["Access"] = null;
                    } 
                }
            }
            catch (Exception ex)
            { }
            //Check Querystring is greater than zero and assign the query string value to email page and forgot email page
            //G. Vera 07/3/2012 - Change from checking the count to checking the actual query string.
            if (!String.IsNullOrEmpty(Request.QueryString["emailId"]))
            {
                txtemail.ReadOnly = true;
                txtemail.Text = objCommon.decode(Request.QueryString["emailId"]);
                txtforgotemail.ReadOnly = true;
                txtforgotemail.Text = objCommon.decode(Request.QueryString["emailId"]);
            }
        }
    }

    protected void imbsubmit_Click(object sender, ImageClickEventArgs e)
    {
        valSummary.ValidationGroup = "NewCompany";
        Page.Validate("NewCompany");
        //page is valid
        if (Page.IsValid)
        {
            Session["Username"] = txtemail.Text.Trim();
            // Get vendor Id and asign in session
            Session["VendorId"] = Convert.ToString(objRegistration.GetVendorId(txtemail.Text.Trim(), objCommon.encode(txtPassword.Text)));
            if (Convert.ToInt32(Session["VendorId"]) == 0)
            {
                lblmsg.Text = "<li>" + "Incorrect e-mail or password" + "</li>";
                modlextndError.Show();
            }
            else
            {
                DAL.UspGetVendorDetailsByIdResult[] obj = objRegistration.GetVendor(Convert.ToInt64(Session["VendorId"]));
                if (obj.Length > 0)
                {
                    //002 - Taking out the expiration logic and allowing vendor to log in normally.
                    //if ((obj[0].VendorStatus == 0) && ((Convert.ToDateTime(obj[0].RegisteredDate).AddDays(90)) < DateTime.Now))
                    //{
                    //    lblmsg.Text = "<li>" + "Your Vendor Qualification Form has expired.&nbsp;If you would like to activate the account, please email the vendor help desk at vendorhelpdesk@haskell.com or by calling 904-357-4998." + "</li>";
                    //    objRegistration.UpdateVendorBitStatus(obj[0].PK_VendorID, 5);       //001
                    //    modlextndError.Show();
                    //}
                    //else if (obj[0].VendorStatus == 4)
                    //{
                    //    lblmsg.Text = "<li>" + "Your VQF is rejected by the Haskell Administrator.&nbsp;Kindly contact Haskell administrator for further details (904) 357-4998" + "</li>";
                    //    modlextndError.Show();
                    //}
                    //else if ((Convert.ToDateTime(obj[0].ExpiryDT)) <= DateTime.Now || (obj[0].VendorStatus == 5))
                    //{
                    //    lblmsg.Text = "<li>" + "Your Vendor Qualification Form has expired.&nbsp;If you would like to activate the account, please email the vendor help desk at vendorhelpdesk@haskell.com or by calling 904-357-4998." + "</li>";
                    //    if (obj[0].VendorStatus != 5 && obj[0].VendorStatus != 6)   //G. Vera 12/01/2014 - Vendor status issue fix
                    //        //SGS changes On 08/26/2011
                    //         //GV Took out per Dianne D:  if ((obj[0].ExpiryStatus !=3) &&  (obj[0].ExpiryStatus !=2)) 
                    //        objRegistration.ExpiredActivateVendor(Convert.ToInt64(Session["VendorId"]), 6, obj[0].VendorStatus);

                    //    modlextndError.Show();
                    //}
                    
                    if (obj[0].PasswordStatus == true)
                    {
                        Response.Redirect("../Common/ChangePassword.aspx");
                    }
                    else if (obj[0].VendorStatus == 4)
                    {
                        lblmsg.Text = "<li>" + "Your VQF is rejected by the Haskell Administrator.&nbsp;Kindly contact Haskell administrator for further details (904) 357-4998" + "</li>";
                        modlextndError.Show();
                    }
                    else //002 - if (obj[0].VendorStatus.Equals(0) || obj[0].VendorStatus.Equals(1) || obj[0].VendorStatus.Equals(2) || obj[0].VendorStatus.Equals(3))
                    {
                        if (objRegistration.HasAcceptedTermsAgreement(Convert.ToInt64(Session["VendorId"])) == true)
                        {
                            Response.Redirect("../VQF/Company.aspx");
                        }
                        else
                        {
                            Response.Redirect("../Common/TermsConditions.aspx");
                        }
                    }
                }
                else
                {
                    //Modified by N Schoenberger on 01/04/2012
                    if (objRegistration.HasAcceptedTermsAgreement(Convert.ToInt64(Session["VendorId"])) == true)
                    {
                        Response.Redirect("../VQF/Company.aspx");
                    }
                    else
                    {
                        Response.Redirect("../Common/TermsConditions.aspx");
                    }
                }
            }
        }
        else
        {
            lblmsg.Text = "";
            modlextndError.Show();
        }
    }

    // validate page and list the errors in page.
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                errmsg = errmsg + "<br>" + "<li>" + validator.ErrorMessage + "</li>";
            }
        }
        return errmsg;
    }

    protected void imbOK_Click(object sender, ImageClickEventArgs e)
    {
        // validate page
        valSummary.ValidationGroup = "ForgotEmail";
        Page.Validate("ForgotEmail");
        //page is valid
        lblmsg.Text = "";
        if (Page.IsValid)
        {
            if (string.IsNullOrEmpty(txtforgotemail.Text.Trim()))
            {
                lblmsg.Text = "<li>Please enter e-mail</li>";
                modlextndError.Show();
                modalExtnd.Show();
                txtforgotemail.Focus();
            }
            else
            {
                DAL.UspGetPasswordResult[] objResult = objRegistration.GetPassword(txtforgotemail.Text.Trim());
                string adminemail = ConfigurationManager.AppSettings["adminemail"].ToString();
                //billtompkins@comcast.com
                if (objResult.Length > 0)
                {
                    StringBuilder Body = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='70%'>");
                    Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hi&nbsp;" + objResult[0].FirstName + " " + objResult[0].LastName + ", <br></font></td></tr>");
                    Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Welcome to Haskell!</font></td></tr>");
                    Body.Append("<tr><td><font  face='verdana' size='2'>Please use the password “" + objCommon.decode(objResult[0].password) + "“ to login into the account.  <br></font></td></tr>");                    Body.Append("<tr><td>&nbsp;</td></tr>");
                    Body.Append("<tr><td><font  face='verdana' size='2'>Respectfully,</font><br/><br/></td></tr>");
                    Body.Append("<tr><td><font  face='verdana' size='2'>Haskell</font><br></td></tr></table>");
                    objCommon.SendMail(adminemail, txtforgotemail.Text.Trim(), "Your Password from Haskell" + " (" + objResult[0].Company + ") ", Body); //Modified by N Schoenberger 02/03/2012 [VMS Phase II Enhancements] {Company name in email subject}
                    modalExtnd.Hide();
                    lblmsg.Text = "&nbsp; <li>Password has been sent to your e-mail</li>";
                    txtforgotemail.Text = "";
                    modlextndError.Show();
                }
                else
                {
                    lblmsg.Text = "&nbsp; <li>E-mail does not exist</li>";
                    modlextndError.Show();
                    txtforgotemail.Text = "";
                    modalExtnd.Show();
                    txtforgotemail.Focus();
                }
            }
        }
        else
        {
            lblmsg.Text = "&nbsp; <li>Please enter valid e-mail</li>";
            txtforgotemail.Focus();
            modalExtnd.Show();
            txtforgotemail.Text = "";
            modlextndError.Show();
        }
    }

    protected void imbCancel_Click(object sender, ImageClickEventArgs e)
    {
        //G. Vera txtforgotemail.Text = "";
    }

    protected void lnkForgot_Click(object sender, EventArgs e)
    {
        //G. Vera txtforgotemail.Text = "";
    }

}