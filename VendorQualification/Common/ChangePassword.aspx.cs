﻿using System;
using System.Web.UI;
#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 
 *
 ****************************************************************************************************************/
#endregion
public partial class Admin_ChangePassword : CommonPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Common/Home.aspx?timeout=1");
        }
        txtPassword.Focus();
        if (!IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            if (string.IsNullOrEmpty(Convert.ToString(Session["VendorId"])) || Convert.ToString(Session["VendorId"]) == "0")
            {
                Response.Redirect("~/Common/Home.aspx?timeout=1");  //001
            }
            else
            {
                ControlTopMenuVisibility(TopMenu, VQFTopMenu);
            }
            trPassword.Style["Display"] = Request.QueryString.Count > 0 ? "none" : "";
        }
    }

    protected void imbsubmit_Click(object sender, ImageClickEventArgs e)
    {
        //validate the page
        Page.Validate();
        //Check if page is valid
        if (Page.IsValid)
        {
            string Password = "";
            clsRegistration objRegistration = new clsRegistration();
            Common objCommon = new Common();

            //Get vendor details
            DAL.UspGetVendorDetailsByIdResult[] objDetails = objRegistration.GetVendor(Convert.ToInt64(Session["VendorId"]));
            if (objDetails.Length > 0)
            {
                //Decode teh password
                Password = objCommon.decode(objDetails[0].Password);
            }
            if (Password == txtPassword.Text)
            {
                //update password
                objRegistration.ChangePasswordStatus(Convert.ToInt64(Session["VendorId"]));
                objRegistration.UpdateVendorPassword(Convert.ToInt64(Session["VendorId"]), objCommon.encode(txtNewPassword.Text));
                lblheading.Text = "Result";
                lblerrorMsg.Text = "<li>Password is changed successfully</li>";
                modalExtnd.Show();

                VisibleTopMenu(TopMenu, VQFTopMenu);
                trPassword.Style["Display"] = "None";
            }
            else
            {
                lblheading.Text = "Message";
                lblerrorMsg.Text = "<li>Current password is incorrect";
                modalExtnd.Show();
            }
        }
        else
        {
            //if page is not valid, send the validation message
            string errmsg = String.Empty;
            foreach (IValidator validator in Page.Validators)
            {
                if (!validator.IsValid)
                {
                    errmsg += "<li>" + validator.ErrorMessage + "</li>";
                }
            }
            lblheading.Text = "Validation";
            lblerrorMsg.Text = errmsg;
            modalExtnd.Show();
        }
    }

    //Validate the password should contain at least 6 characters
    protected void cusPassword_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        string Password;
        string ConPassword;
        if (!string.IsNullOrEmpty(txtNewPassword.Text))
        {
            Password = txtNewPassword.Text;
            if (Password.Length < 6)
            {
                cusPassword.ErrorMessage = "New password should contain at least six characters";
                args.IsValid = false;
            }
            else
            {
                ConPassword = txtCPassword.Text;
                if (Password.Equals(ConPassword))
                {
                    args.IsValid = true;
                }
                else
                {
                    cusPassword.ErrorMessage = "Password and confirm password should be same";
                    args.IsValid = false;
                }
            }
        }
    }

    //Validate the page and return the message
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                errmsg += "<li>" + validator.ErrorMessage + "</li>";
            }
        }
        lblerrorMsg.Text = errmsg;
        return errmsg;
    }
}