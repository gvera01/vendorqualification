﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="Admin_ChangePassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>

    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultfocus="txtPassword" defaultbutton="imbsubmit">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" Visible="false" />
                            <Haskell:VQFTopMenu ID="VQFTopMenu" runat="server" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="false" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="80%" class="searchtableclass">
                                                        <tr>
                                                            <td class="searchhdrbarbold" colspan="2">
                                                                Change My Password
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight" colspan="2">
                                                            </td>
                                                        </tr>
                                                        <tr id="trPassword" runat="server" style="display: none">
                                                            <td colspan="2" align="center" class="summaryerrormsg">
                                                                Since your password has been changed by the Administrator, you are requested to
                                                                change the password now
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdlt">
                                                                <span class="mandatorystar">*</span>Current Password
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtPassword" CssClass="txtbox" runat="server" TextMode="Password"
                                                                    MaxLength="20"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="reqPassword" ControlToValidate="txtPassword" Display="none"
                                                                    runat="server" ErrorMessage="Please enter current password">
                                                                </asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdlt">
                                                                <span class="mandatorystar">*</span>New Password
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtNewPassword" CssClass="txtbox" runat="server" TextMode="Password"
                                                                    MaxLength="20"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="reqNewPassword" ControlToValidate="txtNewPassword"
                                                                    Display="none" runat="server" ErrorMessage="Please enter new password">
                                                                </asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdlt">
                                                                <span class="mandatorystar">*</span>Confirm Password
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtCPassword" CssClass="txtbox" runat="server" TextMode="Password"
                                                                    MaxLength="20"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="reqCPassword" ControlToValidate="txtCPassword" Display="none"
                                                                    runat="server" ErrorMessage="Please enter confirm password">
                                                                </asp:RequiredFieldValidator>
                                                                <asp:CustomValidator ID="cusPassword" runat="server" Display="None" ValidationGroup="VendorReg"
                                                                    EnableClientScript="false" OnServerValidate="cusPassword_ServerValidate"></asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:ImageButton ID="imbsubmit" runat="server" ToolTip="Submit" ImageUrl="~/images/submit.jpg"
                                                                    CausesValidation="false" OnClick="imbsubmit_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                            PopupControlID="modalPopupDiv" CancelControlID="imbOK" BackgroundCssClass="popupbg"
                                            RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalPopupDiv" runat="server" class="popupdivsmall" style="display: none;
                                            width: 300px;">
                                            <table cellpadding="5" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                        <asp:Label ID="lblheading" runat="server" CssClass="searchhdrbarbold"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        <asp:Label ID="lblerrorMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl" align="center">
                                                        <asp:ImageButton ID="imbOK" runat="server" ToolTip="Ok" ImageUrl="~/Images/ok.jpg" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
