﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Common_Home" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>

    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultfocus="txtemail" defaultbutton="imbsubmit">
      
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>

    <script type="text/javascript">
            function pageLoad(){
                $find("modalPopupDiv").add_shown(onModalPopupShown);
            }
            function onModalPopupShown(){
                $get("<%=txtforgotemail.ClientID%>").focus();
            }
    </script>

    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:VQFRegistrationMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top" height="273" background="../images/main_head.jpg" class="pad_left">
                                                    <!--Added by G. Vera on 03/23/2012 - Need some kind of notification on login for users-->
                                                    <table border="0" cellpadding="0" cellspacing="0" align="left" 
                                                        style="margin: 10px 10px 10px 75px">
                                                        <tr>
                                                            <td class="style2" style="color: #800000; font-size: 14px;" valign="top">
                                                                The VMS (Vendor Management System) will be temporarily <br />
                                                                unavailable starting Sunday, April 2 9:30 a.m. (ET) and ending <br />
                                                                Sunday, April 2 10:30 a.m. (ET).<br /><br />

                                                                Thank you for your continued patience while we improve our system.
                                                            </td>
                                                        </tr>
                                                    </table>
                                                     <table width="300px" border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td class="login_heads">
                                                                VENDOR LOGIN
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="even_border">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt" width="30%">
                                                                            <b>E-mail</b>
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:TextBox ID="txtemail" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="reqvemail" ControlToValidate="txtemail" Display="None"
                                                                                ValidationGroup="NewCompany" runat="server" SetFocusOnError="true" ErrorMessage="Please enter e-mail"
                                                                                EnableClientScript="false">
                                                             
                                                                            </asp:RequiredFieldValidator>
                                                                            <asp:RegularExpressionValidator ID="reqvemail1" runat="server" Display="None" ErrorMessage="Please enter valid e-mail"
                                                                                ControlToValidate="txtemail" SetFocusOnError="True" ValidationGroup="NewCompany"
                                                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            <b>Password</b>
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:TextBox ID="txtPassword" runat="server" CssClass="txtbox" TextMode="Password"
                                                                                MaxLength="20"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="reqvpass" ControlToValidate="txtPassword" Display="None"
                                                                                ValidationGroup="NewCompany" runat="server" SetFocusOnError="true" ErrorMessage="Please enter password"
                                                                                EnableClientScript="false">
                                                             
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:ImageButton ID="imbsubmit" runat="server" ImageUrl="~/images/login.jpg" ToolTip="Login"
                                                                                OnClick="imbsubmit_Click" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;&nbsp;
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            &nbsp;&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="bodytextcenter" style="padding-bottom: 10px">
                                                                            <a href="VendorReg.aspx" class="home_link">New Vendor Register Here</a> |
                                                                            <asp:LinkButton ID="lnkForgot" runat="server" Text="Forgot Password?" CssClass="home_link"
                                                                                align="middle" OnClick="lnkForgot_Click" ></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- Added by N Schoenberger 1/27/2012 to suggest browser compatability *** -->
                                                                    <tr>
                                                                        <td colspan="2" class="bodytextcenter" 
                                                                            style="padding-bottom: 10px; color: #800000;">
                                                                            <br />
                                                                            FOR BEST RESULTS, PLEASE USE INTERNET EXPLORER 9 OR LATER.&nbsp; OTHER BROWSERS MAY 
                                                                            EXPERIENCE PROBLEMS WHILE USING THIS SITE.</td>
                                                                    </tr>
                                                                    <!-- End added by N Schoenberger 1/27/2012 to suggest browser compatability *** -->
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <img src="../images/HomeText.jpg" height="92" width="900" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" align="center">
                                                    <img src="../images/middle.jpg" width="458" height="28" align="middle" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lnkForgot"
                                            BehaviorID="modalPopupDiv" PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%--G. Vera 09/16/2015: Surround with Panel to use default button behavior--%>
                                        <asp:Panel runat="server" ID="pnlForgotPassword" DefaultButton="imbOK">
                                            <div id="modalPopupDiv" class="popupdivsmall1" style="display: none">
                                                <table cellpadding="5" cellspacing="0" border="0" width="275" align="center">
                                                    <tr>
                                                        <td class="searchhdrbarbold" runat="server" id="msgTd" colspan="2">Forgot Password
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="tdheight"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="formtdlt" width="27%">
                                                            <span class="mandatorystar">*</span> E-mail
                                                        </td>
                                                        <td class="formtdrt">
                                                            <asp:TextBox ID="txtforgotemail" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox>
                                                            <asp:RegularExpressionValidator Display="None" ControlToValidate="txtforgotemail"
                                                                ID="revforgotemail" ValidationGroup="ForgotEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td class="formtdrt">
                                                            <asp:ImageButton ID="imbOK" runat="server" ToolTip="Submit" ImageUrl="~/Images/submit.jpg"
                                                                OnClick="imbOK_Click" />
                                                            <asp:ImageButton ID="imbCancel" runat="server" ToolTip="Cancel" ImageUrl="~/Images/cancel.jpg"
                                                                OnClientClick="closediv();" OnClick="imbCancel_Click" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="tdheight"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modlextndError" runat="server" TargetControlID="HiddenField1"
                                            PopupControlID="Div1" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="Div1" class="popupdivsmall" style="display: none; width: 525px">
                                            <table cellpadding="5" cellspacing="0" border="0" width="525px" align="center">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="Td1" colspan="3">
                                                        Message
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        &nbsp;
                                                        <asp:Label ID="lblmsg" runat="server" CssClass="errormsg"></asp:Label>
                                                        <asp:ValidationSummary CssClass="summaryerrormsg" ValidationGroup="ForgotEmail" DisplayMode="BulletList"
                                                            runat="server" ID="valSummary" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="popupdivcl" align="center">
                                                        <input type="button" value="OK" title="Ok" class="ModalPopupButton" onclick="closediv();" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="popupdivcl">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <style type="text/css">
        .body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
        }
        .style2
        {
            text-align: left;
        }
    </style>

    <script type="text/javascript" language="javascript">

        // G. Vera - Added the redirect.
        function closediv() {
            $find('modlextndError').hide();
            window.location = "../Common/Home.aspx";
        }
        
    </script>

</body>
</html>
