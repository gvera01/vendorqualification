﻿using System;
using System.Configuration;
using System.Text;
using System.Web.UI;

public partial class Common_ContactUs : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtMessage.Focus();
    }

    //Send comments to the admin through e-mail
    protected void imbSubmit_Click(object sender, ImageClickEventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            if (!(string.IsNullOrEmpty(txtMessage.Text)))
            {
                clsRegistration objRegistration = new clsRegistration();
                DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(Session["VendorId"]));
                StringBuilder Body = new StringBuilder();
                if (objVendor.Length > 0)
                {
                    Common objCommon = new Common();
                    Body.Append(txtMessage.Text.Trim());
                    //call method to send e-mail
                    objCommon.SendMail(objVendor[0].Email, ConfigurationManager.AppSettings["adminemail"].ToString(), "Contact Us - " + objVendor[0].Company.ToString(), Body);
                    lblMsg.Text = "Your comments has been sent to the Haskell administrator";
                    modalExtnd.Show();
                    txtMessage.Text = string.Empty;
                }
                txtMessage.Focus();
            }
        }
        else
        {
            lblMsg.Text = ErrorMsg();
            modalExtnd.Show();
        }
    }

    //validate the page and send the error message
    private string ErrorMsg()
    {
        string errmsg = string.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                errmsg += "<li>" + validator.ErrorMessage + "</li><br>";
            }
        }
        return errmsg;
    }

    //Clear the text
    protected void imbCancel_Click(object sender, ImageClickEventArgs e)
    {
        txtMessage.Text = "";
    }

}