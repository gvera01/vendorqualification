﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 
 *
 ****************************************************************************************************************/
#endregion

public partial class Common_TermsConditions : System.Web.UI.Page
{
    #region Declarations

    clsRegistration oRegistration = new clsRegistration();
    static Int32 messageId;
    #endregion

    #region Event Handlers
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["VendorId"])) || Convert.ToString(Session["VendorId"]) == "0")
        {
            Response.Redirect("~/Common/Home.aspx?timeout=1");  //001

        }
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Common/Home.aspx?timeout=1");
        }

        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");
            
            //Load terms and contitions
            DataSet ds = new DataSet();

            ds = oRegistration.GetTermsAndConditionsAgreement("TermsAndConditions");

            messageId = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0]);
            lblTitle.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
            lblAgreement.Text = ReplaceWithBR(ds.Tables[0].Rows[0].ItemArray[2].ToString());

            //enable accept functionality only if NOT yet accepted
            if (oRegistration.HasAcceptedTermsAgreement(Convert.ToInt64(Session["VendorId"])) == true)
            {
                chkTermsConditions.Enabled = false;
                imbSubmit.Visible = false;
                imbClose.Visible = true;
            }
            else
            {
                chkTermsConditions.Enabled = true;
                imbSubmit.Visible = true;
                imbClose.Visible = false;
            }
        }
    }
    protected void imbSubmit_Click(object sender, ImageClickEventArgs e)
    {
        if (chkTermsConditions.Checked == true)
        {
            //update accepted
            bool bUpdated = oRegistration.UpdateHasAcceptedTermsAgreement(Convert.ToInt64(Session["VendorId"]), messageId);
            Response.Redirect("../VQF/Company.aspx");
        }
    }
    protected void imbClose_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../VQF/Company.aspx");
    }
    #endregion

    #region Methods

    protected string ReplaceWithBR(string agreement)
    {
        Regex regex = new Regex(@"(\r\n|\r|\n)+"); 
        string newText = regex.Replace(agreement, "<br /><br />");

        return newText;
    }
    #endregion
}
