﻿using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMSDAL;
#region Change Comments
/****************************************************************************************************************
 * 001 Sooraj Sudhakaran.T 10/04/2013: VMS Modification - Added code to handle International Business Id
 * 002 G. Vera 06/02/2014: Changing MailContent() message
 * 003 G. Vera 03/29/2017: Allow company field to be edited if they made a mistake
 ****************************************************************************************************************/
#endregion 
public partial class Common_VendorReg : CommonPage
{
    Common objCom = new Common();
    clsCompany objVendor = new clsCompany();
    clsRegistration objRegistration = new clsRegistration();
    string Company, TaxId;

    protected void Page_Load(object sender, EventArgs e)
    {
        CallScripts();

        ShowHideTaxControls();

        if (!IsPostBack)
        {
            ShowHideControls();
        }
    }

    /// <summary>
    /// Display the panel to enter company name or tin/ss number
    /// </summary>
    private void ShowHideControls()
    {
        pnlReg.Visible = true;
        pnlVendorDB.Visible = false;
        pnlNewCompany.Visible = false;
      

    }
    //001
    /// <summary>
    /// Display the tax control based on selected Country
    /// </summary>
    /// <Author>Sooraj Sudhakaran.T</Author>
    /// <Date>3-OCT-2013</Date>
    private void ShowHideTaxControls()
    {
        if (ddlCountry.SelectedIndex == 0)//USA
        {
            txtBusinessTax.Text = string.Empty;
            trUSA.Style["display"] = "";
            trOther.Style["display"] = "none";
        }
        else
        {
            txtTax.Text = string.Empty;
            txtTax1.Text = string.Empty;
            trOther.Style["display"] = "";
            trUSA.Style["display"] = "none";
        }

        if (ddlCountryReg.SelectedIndex == 0) //USA
        {
            txtBusinessTaxId.Text = string.Empty;
            trOtherReg.Style["display"] = "none";
            trUSAReg.Style["display"] = "";
        }
        else
        {
            txtTaxId.Text = string.Empty;
            txtTaxId1.Text = string.Empty;
          
            trOtherReg.Style["display"] = "";
            trUSAReg.Style["display"] = "none";
        }

    }

    // redirects to another panel based on the company and tin/ss number entered
    protected void imgSubmit_Click(object sender, ImageClickEventArgs e)
    {
        // validate page
        Page.Validate("NewCompany");
        txtCompanyName.Focus();
        if (Page.IsValid)
        {
          
            // Assign values to the company and tax number entered in the previous page 
            if (string.IsNullOrEmpty(txtCompany.Text))
            {
                Company = null;
                hdnCompany.Value = null;
            }
            else
            {
                Company = txtCompany.Text.Trim();
                hdnCompany.Value = txtCompany.Text.Trim();
            }

            if (ddlCountry.SelectedIndex == 0)  //001-Sooraj modified for International TaxID 
            {
                hdnCountry.Value = "0";
                if (string.IsNullOrEmpty(txtTax.Text.Trim()) && string.IsNullOrEmpty(txtTax1.Text.Trim()))
                {
                    TaxId = null;
                    hdnTax.Value = null;
                }
                else
                {
                    TaxId = txtTax.Text.Trim() + txtTax1.Text.Trim();
                    hdnTax.Value = txtTax.Text.Trim() + txtTax1.Text.Trim();
                }
            }
            else //001-Sooraj modified for International TaxID 
            {
                hdnCountry.Value = "1";
                if (string.IsNullOrEmpty(txtBusinessTax.Text.Trim()))
                {
                    TaxId = null;
                    hdnTax.Value = null;
                }
                else
                {
                    TaxId = txtBusinessTax.Text;
                    hdnTax.Value = txtBusinessTax.Text.Trim();
                }
            }

            //var count = objRegistration.GetCompany(Company, TaxId).Length; //001-Sooraj Commented code as same Query invoked multiple times 

            var  companyData = objRegistration.GetCompany(Company, TaxId,ddlCountry.SelectedIndex==0);    
            var count = companyData.Length;
            // check company already exists, if yes display the company details,
            //else redirects to the panel to enter the company details
            if (count > 0)
            {
               // grdVendorDB.DataSource = objRegistration.GetCompany(Company, TaxId);  //001-Sooraj commented 
                grdVendorDB.DataSource = companyData;
                grdVendorDB.DataBind();
            
                if (!string.IsNullOrEmpty(hdnTax.Value))
                {
                    grdVendorDB.SelectedIndex = 0;
                    grdVendorDB_SelectedIndexChanged(null, null);
                }
                if (TaxId != null)
                {
                    lnkNew.Visible = false;
                }
                pnlReg.Visible = false;
                //pnlJDE.Visible = false;
                pnlVendorDB.Visible = true;
                pnlNewCompany.Visible = false;
            }
            else if (count == 0)
            {
                DAL.Vendor[] objVendor = objRegistration.GetVendorDetails();
                int Companycount = 0,Taxidcount = 0;
            
                if (((!string.IsNullOrEmpty(Company)) && (!string.IsNullOrEmpty(TaxId))))
                {
                    var IsUSA=ddlCountry.SelectedIndex==0;
                    Companycount = objVendor.Count(m => m.Company == Company );
                    Taxidcount = objVendor.Count(m => m.TaxID == TaxId && m.IsBasedInUS == IsUSA);
                }
                if (Companycount == 0 && Taxidcount == 0) 
                {
                    pnlReg.Visible = false;
                    //   pnlJDE.Visible = false;
                    pnlVendorDB.Visible = false;
                    pnlNewCompany.Visible = true;


                    //  pnlTaxInfo.Visible = false;
                    txtCompanyName.Text = hdnCompany.Value;

                    ddlCountryReg.SelectedIndex = hdnCountry.Value.ConvertToShortInt();

                    ShowHideTaxControls();

                    string TaxIdMax = hdnTax.Value;
                    if (ddlCountryReg.SelectedIndex == 0) //001-Sooraj
                    {
                        if (TaxIdMax.Length >= 9)
                        {
                            txtTaxId.Text = TaxIdMax.Substring(0, 2);
                            txtTaxId1.Text = TaxIdMax.Substring(2, 7);
                        }
                    }
                    else
                    {
                        txtBusinessTaxId.Text = TaxIdMax;//001-Sooraj
                    }

                    //003 txtCompanyName.ReadOnly = !string.IsNullOrEmpty(txtCompanyName.Text.Trim());

                    if (ddlCountryReg.SelectedIndex == 0) //001-Sooraj
                    {

                        if (!string.IsNullOrEmpty(txtTaxId.Text.Trim()))
                        {
                            //003 txtTaxId.ReadOnly = true;
                            //003 txtTaxId1.ReadOnly = true;
                        }
                        else
                        {
                            txtTaxId.ReadOnly = false;
                            txtTaxId1.ReadOnly = false;
                        }
                    }
                    else  
                    {
                        if (!string.IsNullOrEmpty(txtBusinessTaxId.Text.Trim())) //001-Sooraj
                        {
                            txtBusinessTaxId.ReadOnly = true;
                        }
                        else
                        {
                            txtBusinessTaxId.ReadOnly = false;
                        }
                    }
                }
                else
                {
                    if (((!string.IsNullOrEmpty(Company)) && (!string.IsNullOrEmpty(TaxId))))
                    {
                        string mailBody = "Company " + Company.ToString().ToUpper() + " has requested a Tax Id that already exists in the system for Tax Id " + TaxId.ConvertToTaxId(ddlCountryReg.SelectedIndex == 0) + ".";
                        if (Taxidcount > 0)
                            lblErrorMsg.Text = "You are attempting to enter a tax id that has been associated with another company. If you believe this to be in error or need further assistance, please contact <a href='mailto:vendorhelpdesk@haskell.com?subject=Duplicate Tax Id Request" + " (" + Company.ToString().ToUpper() + ") " + "&body=" + mailBody + "'>vendorhelpdesk@haskell.com</a>"; //Modified by N Schoenberger 02/03/2012 [VMS Phase II Enhancements] {Company name in email subject}
                        else if (Companycount > 0)
                            lblErrorMsg.Text = "<li>Company name already exists";
                        modalExtnd.Show();
                    }
                }
            }
        }
        else
        {
            lblErrorMsg.Text = Errormsg();
            modalExtnd.Show();
        }
    }
    
    /// <summary>
    /// Method to cal Javascript
    /// </summary>
    private void CallScripts()
    {
        txtTax.Attributes.Add("onkeyup", "javascript:SetFocusOnTax(" + txtTax.ClientID + "," + txtTax1.ClientID + ")");
        txtTaxId.Attributes.Add("onkeyup", "javascript:SetFocusOnTax(" + txtTaxId.ClientID + "," + txtTaxId1.ClientID + ")");

      
    }

    /// <summary>
    /// validate page and display the validation error in the list
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                errmsg = errmsg + "<li> " + validator.ErrorMessage + "</li>";
            }
        }
        return errmsg;
    }

    /// <summary>
    /// Registration completed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        lblComplete.Text = "&nbsp;Registration has been completed sucessfully <br/ > &nbsp;please check your email for confirmation.";
        msgTd.InnerText = "Result";
        modalComplate.Show();
    }

    /// <summary>
    /// Company name is correct
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imdCorrect_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("Home.aspx?emailId=" +  objCom.encode(lblEmail.Text));
    }

    /// <summary>
    /// Company name is incorrent
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbIncorrect_Click(object sender, ImageClickEventArgs e)
    {
        lblErrorMsg.Text = "";
        lblMsg.Text = "";
        lblMsg.Text = "&nbsp;Please contact " + lblConfirmFirstName.Text.Trim() + "  " + lblConfirmLastName.Text.Trim() + " of " + lblCompanyName.Text.Trim() + " or <BR/> &nbsp;Haskell administrator (904) 791 - 4500 for assistance";
        msgTd.InnerText = "Confirm";
        modalExtnd.Show();
    }

    /// <summary>
    /// If the entered information is correct
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rblCorrectOpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblCorrectOpt.SelectedValue.Equals("0"))
        {
            lblMsg.Text = "&nbsp;Login information has been send to the e-mail";
            msgTd.InnerText = "Result";
            modalExtnd.Show();
        }
        else if (rblCorrectOpt.SelectedValue.Equals("1"))
        {
            lblMsg.Text = "&nbsp;Call up this nubmer for assistance (904) 357-4998";
            msgTd.InnerText = "Result";
            modalExtnd.Show();
        }
    }

    /// <summary>
    /// If company name displayed in the list is incorrect the user can use this method to move to another panel to insert new company details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkNew_Click(object sender, EventArgs e)
    {
        pnlReg.Visible = false;
        pnlVendorDB.Visible = false;
        pnlNewCompany.Visible = true;
        txtCompanyName.Text = hdnCompany.Value;
        txtTaxId.Text = hdnTax.Value;
        txtBusinessTaxId.Text = hdnTax.Value;
        txtCompanyName.ReadOnly = false;
        txtTaxId.ReadOnly = false;
        txtBusinessTaxId.ReadOnly = false;
    }

    /// <summary>
    /// Insert vendor details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgSave_Click(object sender, ImageClickEventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            string count =string.Empty;
            if(ddlCountryReg.SelectedIndex==0)
             count = objRegistration.CheckRedundancy(txtCompanyName.Text.Trim(), (txtTaxId.Text.Trim() + txtTaxId1.Text.Trim()), txtEmailNew.Text.Trim(),true);
            else
                count = objRegistration.CheckRedundancy(txtCompanyName.Text.Trim(), txtBusinessTaxId.Text, txtEmailNew.Text.Trim(),false);

            //Check taxId already exists. If already exists throw a msg stating taxid already exists
            if (count == "Correct")
            {
                long VendorId = 0;
                if (ddlCountryReg.SelectedIndex == 0)
                    VendorId = objVendor.InsertVendor(txtCompanyName.Text.Trim().ToUpper(), (txtTaxId.Text.Trim() + txtTaxId1.Text.Trim()), txtFirstName.Text.Trim(), txtLastName.Text.Trim(), txtEmailNew.Text.Trim(), objCom.encode(txtPasswordNew.Text.Trim()),false,true);
                else
                    VendorId = objVendor.InsertVendor(txtCompanyName.Text.Trim().ToUpper(), txtBusinessTaxId.Text, txtFirstName.Text.Trim(), txtLastName.Text.Trim(), txtEmailNew.Text.Trim(), objCom.encode(txtPasswordNew.Text.Trim()), false,false);
                MailContant();
                Session["VendorId"] = Convert.ToString(VendorId);
                Response.Redirect("../VQF/Company.aspx?VendorId=" + objCom.encode(Convert.ToString(VendorId)));
            }
            else
            {   
                txtCompanyName.ReadOnly = false;
                txtTaxId.ReadOnly = false;
                txtTaxId1.ReadOnly = false;
                txtBusinessTaxId.ReadOnly = false;
                lblErrorMsg.Text = count;
                modalExtnd.Show();
            }
        }
        else
        {
            lblErrorMsg.Text = Errormsg();
            modalExtnd.Show();
        }
    }

    //method to send email
    private void MailContant()
    {
        StringBuilder Body = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
        Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hi " + " " + txtFirstName.Text + " " + txtLastName.Text + ", <br></font></td></tr>");
        Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Welcome to Haskell!</font></td></tr>");
        Body.Append("<tr><td><font  face='verdana' size='2'>You have successfully registered with us.  <br></font></td></tr>");
        Body.Append("<tr><td><font  face='verdana' size='2'>Kindly complete and submit the Vendor Qualification Form within 90 days from today.<br></font></td></tr>");
        Body.Append("<tr><td><font  face='verdana' size='2'>Respectfully,</font><br/><br/></td></tr>");
        Body.Append("<tr><td><font  face='verdana' size='2'>Haskell</font><br></td></tr></table>");
        Common objCommon = new Common();
        string adminemail = ConfigurationManager.AppSettings["adminemail"].ToString();
        objCommon.SendMail(adminemail, txtEmailNew.Text.ToString().Trim(), "Successful registration with Haskell", Body);

        StringBuilder AdminBody = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='70%'>");
        AdminBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hi, <br></font></td></tr>");
        AdminBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>This is to inform you that " + txtFirstName.Text.ToString().Trim() + " " + txtLastName.Text.ToString().Trim() + " From " + txtCompanyName.Text.ToString().Trim() + " has successfully registered.</font></td></tr>");
        Body.Append("<tr><td><font  face='verdana' size='2'>Respectfully,</font><br/><br/></td></tr>");
        Body.Append("<tr><td><font  face='verdana' size='2'>Haskell</font><br></td></tr></table>");
        objCommon.SendMail(txtEmailNew.Text, adminemail, "Successful registration with Haskell - " + txtCompanyName.Text.ToString().Trim() + "", AdminBody);
    }

    /// <summary>
    /// validate page to enter either company or tin / ss number
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void cusTINorSSN_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((string.IsNullOrEmpty(txtCompany.Text)) && ((string.IsNullOrEmpty(txtTax.Text) && string.IsNullOrEmpty(txtTax1.Text) && string.IsNullOrEmpty(txtBusinessTax.Text))))
        {
            cusvTINorSSN.ErrorMessage = "Please enter company name or tax ID";
            args.IsValid = false;
        }
        else
        {
            if (string.IsNullOrEmpty(txtCompany.Text))
            {
                cusvTINorSSN.ErrorMessage = "Please enter company name";
                args.IsValid = false;
            }

            if ((string.IsNullOrEmpty(txtBusinessTax.Text)) && ((string.IsNullOrEmpty(txtTax.Text)) && ((string.IsNullOrEmpty(txtTax1.Text)))))
            {
                cusvTINorSSN.ErrorMessage = "Please enter tax ID";
                args.IsValid = false;
            }
        }
    }

    //validate the tax id
    protected void Custax_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if (((string.IsNullOrEmpty(txtTaxId.Text)) && ((string.IsNullOrEmpty(txtTaxId1.Text)))) && string.IsNullOrEmpty(txtBusinessTaxId.Text))
        {
            Custax.ErrorMessage = "Please enter tax ID";
            args.IsValid = false;
        }
        else
        {
            if (ddlCountryReg.SelectedIndex == 0)
            {
                bool b = objCom.Validatefederaltaxformat(txtTaxId.Text, txtTaxId1.Text);
                if (b == false)
                {
                    Custax.ErrorMessage = "Please enter tax ID in valid format";
                    args.IsValid = false;
                }
            }
        }
    }

    /// <summary>
    /// selected a company from the list to display the company details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rdbSelect_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow oldrow in grdVendorDB.Rows)
        {
            ((RadioButton)oldrow.FindControl("rdbSelect")).Checked = false;
        }
        //Set the new selected row
        RadioButton rb = (RadioButton)sender;
        GridViewRow row = (GridViewRow)rb.NamingContainer;
        ((RadioButton)row.FindControl("rdbSelect")).Checked = true;
        CompDetailstr.Style["display"] = "";
        grdVendorDB.SelectedIndex = row.RowIndex;
        grdVendorDB_SelectedIndexChanged(null, null);
    }

    /// <summary>
    /// display the content based on the inputs given
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendorDB_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblState = new Label();
            lblState = (Label)e.Row.FindControl("lblState");
            Label lblOtherState = new Label();
            lblOtherState = (Label)e.Row.FindControl("lblOtherState");
           
            if ((lblState.Text == "0") || string.IsNullOrEmpty(lblState.Text))
            {
                lblState.Text = string.Empty;
                lblOtherState.Visible = false;
            }
            else if (lblState.Text == "51")
            {
                lblState.Visible = false;
                lblOtherState.Visible = true;
            }
            else if (Convert.ToInt32(lblState.Text) > 1)
            {
                lblState.Text = Convert.ToString(objCom.GetStateCode(Convert.ToInt16(lblState.Text)).Tables[0].Rows[0][1]);
                lblOtherState.Visible = false;
            }
        }

        if (!string.IsNullOrEmpty(hdnTax.Value))
        {
            e.Row.Cells[0].Visible = false;
            e.Row.Cells[1].Visible = true;
            e.Row.Cells[2].Visible = true;
            e.Row.Cells[3].Visible = true;
            e.Row.Cells[4].Visible = true;
            e.Row.Cells[5].Visible = true;
            e.Row.Cells[6].Visible = true;

            string TaxID = e.Row.Cells[6].Text.ToString();
           
            e.Row.Cells[6].Text = TaxID.ConvertToTaxId(ddlCountry.SelectedIndex==0);

            CompDetailstr.Style["display"] = "";
        }
        else
        {
            e.Row.Cells[0].Visible = true;
            e.Row.Cells[1].Visible = true;
            e.Row.Cells[2].Visible = true;
            e.Row.Cells[3].Visible = true;
            e.Row.Cells[4].Visible = true;
            e.Row.Cells[5].Visible = true;
            e.Row.Cells[6].Visible = false;
            CompDetailstr.Style["display"] = "none";
        }
    }

    /// <summary>
    /// Display the vendor details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendorDB_SelectedIndexChanged(object sender, EventArgs e)
    {
        long value = Convert.ToInt64(grdVendorDB.SelectedDataKey.Value);
        if (Convert.ToInt32(grdVendorDB.SelectedDataKey.Value) > 0)
        {
            lblCompanyName.Text = Convert.ToString(objRegistration.GetVendor(value)[0].Company);
            lblConfirmFirstName.Text = Convert.ToString(objRegistration.GetVendor(value)[0].FirstName);
            lblFirstName.Text = Convert.ToString(objRegistration.GetVendor(value)[0].FirstName);
            lblLastName.Text = Convert.ToString(objRegistration.GetVendor(value)[0].LastName);
            lblConfirmLastName.Text = Convert.ToString(objRegistration.GetVendor(value)[0].LastName);
            lblEmail.Text = Convert.ToString(objRegistration.GetVendor(value)[0].Email);
        }
    }

    //Validate password contain at least 6 characters
    protected void cusPassword_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        string Password;
        string ConPassword;
        if (!string.IsNullOrEmpty(txtPasswordNew.Text))
        {
            Password = txtPasswordNew.Text;
            if (Password.Length < 6)
            {
                cusPassword.ErrorMessage = "Password should contain at least six characters";
                args.IsValid = false;
            }
            else
            {
                ConPassword = txtCPasswordNew.Text;
                if (!Password.Equals(ConPassword))
                {
                    cusPassword.ErrorMessage = "Password and confirm password should be same";
                    args.IsValid = false;
                }
            }
        }
    }

    // ***** Added by N Schoenberger on 01/05/2012 [Phase II Enhancement] ***** //
    protected void cusEmail_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        string Email;
        string ConEmail;
        if (!string.IsNullOrEmpty(this.txtEmailNew.Text))
        {
            Email = txtEmailNew.Text;
            ConEmail = txtCEmailNew.Text;

            if (!Email.Equals(ConEmail))
            {
                cusEmail.ErrorMessage = "Email and confirm email must be the same";
                args.IsValid = false;
            }
        }
    }
    // ***** End sdded by N Schoenberger on 01/05/2012 [Phase II Enhancement] ***** //

    //validate federal tax id

    protected void Cusfederaltax_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if(ddlCountry.SelectedIndex==0)
        if ((!string.IsNullOrEmpty(txtTax.Text)) || ((!string.IsNullOrEmpty(txtTax1.Text))))
        {
            bool isValidFormat = objCom.Validatefederaltaxformat(txtTax.Text, txtTax1.Text);
            if (!isValidFormat)
            {
                Cusfederaltax.ErrorMessage = "Please enter tax ID in valid format";
                args.IsValid = false;
            }
        }
    }
}