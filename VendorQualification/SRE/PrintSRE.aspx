﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintSRE.aspx.cs" Inherits="SRE_PrintSRE" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <style type="text/css">
        body
        {
            font-family: Tahoma;
            font-size: 11px;
            color: #333333;
            /*background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;*/
             margin-top: 0px;
        }
       
    </style>

    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">

        //function printfn() {

            //window.setInterval("window.close()", 10);
        }
    </script>

    <style>
        @media print
        {
            body
            {
                zoom: 100%;
            }
        }
        .style2
        {
            font-family: Tahoma;
            font-size: 11px;
            color: black;
            text-align: left;
            font-weight: bold;
            height: 28px;
        }
        .style3
        {
            width: 87px;
        }
        .style4
        {
            width: 59px;
        }
        .style5
        {
            width: 94px;
        }
        .style6
        {
            width: 44px;
        }
        .style7
        {
            font-family: Tahoma;
            font-size: 11px;
            color: black;
            text-align: right;
            height: 25px;
        }
        .style8
        {
            height: 25px;
        }
    </style>
    
    <style type="text/css" media="print">
        .hide_print
        {
            display: none;
        }
        div.page
        {
            page-break-before: always;
        }
    </style>

    <script language="javascript" type="text/javascript">
        if (navigator.appVersion.indexOf("MSIE 6") != -1) {
            document.styleSheets[document.styleSheets.length - 1].rules[0].style.zoom = "74%";
        }
        else if (navigator.appVersion.indexOf("MSIE 8") != -1) {


            document.styleSheets[document.styleSheets.length - 1].rules[0].style.zoom;
        }
        else
        { document.styleSheets[document.styleSheets.length - 1].rules[0].style.zoom; }

    </script>

</head>
<body style="margin: 0px">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="clscpmgrRiskEvaluation" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" style="border-bottom-width: 0px">
                                <tr>
                               
                                    <td class="contenttd">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="0">
                                            <tr>
                                                <td colspan="2" class="formhead">
                                                    Subcontractor Risk Evaluation
                                                </td>
                                            </tr>
                                           
                                            <tr>
                                                <td colspan="2" class="sectionpad">
                                                    <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass">
                                                        <tr>
                                                            <td class="formtdrt">
                                                                Project Name/Location
                                                            </td>
                                                            <td class="formtdrt">
                                                                Project Number
                                                            </td>
                                                            <td class="formtdrt">
                                                                Date
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtProjectName" CssClass="txtboxlarge" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtProjectNumber" CssClass="txtboxlarge" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtDate" CssClass="txtbox" runat="server" MaxLength="10"></asp:TextBox>
                                                                <%--<Ajax:FilteredTextBoxExtender ID="ftxtDate" TargetControlID="txtDate" FilterType="Numbers,Custom"
                                                                    ValidChars="/" runat="server">
                                                                </Ajax:FilteredTextBoxExtender>
                                                                <Ajax:CalendarExtender ID="caleDate" TargetControlID="txtDate" PopupButtonID="imgDate"
                                                                    runat="server" Format="MM/dd/yyyy">
                                                                </Ajax:CalendarExtender>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdrt">
                                                                Subcontractor
                                                            </td>
                                                            <td class="formtdrt">
                                                                Subcontractor Contact Name
                                                            </td>
                                                            <td class="formtdrt">
                                                                Date of Vendor Qualification Form
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtVendorName" CssClass="txtboxlarge" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtVendorContactName" CssClass="txtboxlarge" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtVendorQualificationDate" CssClass="txtboxmedium" MaxLength="10"
                                                                    runat="server"></asp:TextBox>
                                                                <Ajax:FilteredTextBoxExtender ID="ftxtVendorQualificationDate" TargetControlID="txtVendorQualificationDate"
                                                                    FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                </Ajax:FilteredTextBoxExtender>
                                                                <Ajax:CalendarExtender ID="caleVendorQualificationDate" TargetControlID="txtVendorQualificationDate"
                                                                    PopupButtonID="imgVendorQualificationDate" runat="server" Format="MM/dd/yyyy">
                                                                </Ajax:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdrt">
                                                                Approximate Subcontract Value
                                                            </td>
                                                            <td class="formtdrt">
                                                                Bid Package Description
                                                            </td>
                                                            <td class="formtdrt">
                                                                Bid Package Number
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdrt">
                                                            <span class="cell2">&#36;&nbsp;</span>
                                                                <asp:TextBox ID="txtAppSubContractorValue" CssClass="txtboxlargeNum" runat="server"></asp:TextBox>
                                                                <Ajax:FilteredTextBoxExtender ID="ftxtAppSubContractorValue" runat="server" TargetControlID="txtAppSubContractorValue"
                                                                    FilterType="Numbers">
                                                                </Ajax:FilteredTextBoxExtender>
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtBidpackageDesc" CssClass="txtboxlarge" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:TextBox ID="txtBidpackageNumber" CssClass="txtbox" runat="server"></asp:TextBox>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="bodytextleft sectionpad">
                                                    <%--G. Vera 09/21/2012 - Added--%>
                                                    <asp:Label ID="lblOriginator" runat="server" Font-Bold="true" />
                                                    <br />
                                                    <br />
                                                    The PM must answer each question from the data supplied by the Subcontractor’s VQF,
                                                    and the PM must obtain appropriate references. If the Subcontractor supplied incomplete
                                                    data, the deficiency should be promptly resolved by the PM.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="sectionpad">
                                                    
                                                                 
                                                                    <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass ">
                                                                     <tr>
                                                            <td colspan="4" class="Header">
                                                                Business / Legal Section
                                                            </td>
                                                        </tr>
                                                              <tr>
                                                            <td colspan="4" >
                                                                 <table cellpadding="0" cellspacing="0" width="100%" class="even_pad">
                                                          
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                1.
                                                                            </td>
                                                                            <td id="BusinessYear" class="cell2" runat="server">
                                                                                
                                                                            </td>
                                                                            <td class="cell3">
                                                                                <asp:RadioButton ID="rdoBusiness1Yes" GroupName="Business1" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoBusiness1No" GroupName="Business1" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                2.
                                                                            </td>
                                                                            <td class="cell2">
                                                                                Has the Subcontractor failed to complete awarded work or terminated for cause?
                                                                            </td>
                                                                            <td class="cell3">
                                                                                <asp:RadioButton ID="rdoBusiness2Yes" GroupName="Business2" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoBusiness2No" GroupName="Business2" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                         <%--G. Vera 12/28/2016:  Add the other VQF legal questions that was left out--%>
                                                                         <tr>
                                                                             <td class="cell1">3.
                                                                             </td>
                                                                             <td class="cell2">Any of its Owners, Officers or Major Stockholders currently involved in any arbitration, litigation, suits or liens?
                                                                             </td>
                                                                             <td class="cell3">
                                                                                 <asp:RadioButton ID="rdoBusinessLitigationYes" GroupName="Business5" runat="server" Text="&nbsp;Yes"
                                                                                     TabIndex="21" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoBusinessLitigationNo" GroupName="Business5" runat="server" Text="&nbsp;No"
                                                                                    TabIndex="22" />
                                                                             </td>
                                                                             <td>&nbsp;
                                                                             </td>
                                                                         </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                4.
                                                                            </td>
                                                                            <td class="cell2">
                                                                                Has the Subcontractor had any judgments, bankruptcies, or reorganizations?
                                                                            </td>
                                                                            <td class="cell3">
                                                                                <asp:RadioButton ID="rdoBusiness3Yes" GroupName="Business3" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoBusiness3No" GroupName="Business3" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                5.
                                                                            </td>
                                                                            <td class="cell2">
                                                                                Any indictments or convictions of felony or other criminal conduct for key personnel?
                                                                            </td>
                                                                            <td class="cell3">
                                                                                <asp:RadioButton ID="rdoBusiness4Yes" GroupName="Business4" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoBusiness4No" GroupName="Business4" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4" class="tdheight">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4" class="bodytextright">
                                                                                <span class="riskCaption">High Risk Indicator:</span><span class="riskCell"><asp:CheckBox ID="chkBusinessRiskIndicator" runat="server" /></span> 
                                                                                <br />
                                                                                <%--G. Vera 10/08/2012 - Added--%>
                                                                                <span style="padding-right: 70px;font-style:italic" >TWO or more YES</span>
                                                                            </td>
                                                                           
                                                                        </tr>                                                                   
                                                                        <tr>
                                                      <td width="100%" colspan="4" style="border-bottom:1px solid #cccccc"; >&nbsp;</td>
                                                                      </tr>
                                                                             <tr>
                                                                                                <td colspan="4" class="style2">
                                                                                                    Comments:
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                           
                                                                                                <td colspan="4">
                                                                                                <table cellpadding="3" cellspacing="1" width="100%"><tr>
                                                                                                <td class="bodytextleft">
                                                                                              
                                                                                               <span style="word-wrap:break-word; vertical-align:top;  float:left; width: 99%;"  id="lblBComments" runat="server" ></span>
                                                                                              
                                                                                                </td></tr></table>
                                                                                                </td>
                                                                                            </tr>
                                                                        
                                                                        </table> 
                                                                          </td>
                                                        </tr>
                                                                    </table>
                                                                  
                                                              
                                                                    <br />
                                                                    <table cellpadding="3" cellspacing="0" class="searchtableclass" width="100%" style="page-break-after: always;">
                                                                     <tr>
                                                            <td colspan="4" class="Header">
                                                                Financial Section
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td colspan="4">
                                                                
                                                            </td>
                                                        </tr>
                                                 
                                                                  <tr>
                                                                            <td class="cell1">
                                                                                1.
                                                                            </td>
                                                                            <td id="LargestMaxPercentage" class="cell2" runat="server">
                                                                                
                                                                            </td>
                                                                            <td class="cell3">
                                                                                <asp:RadioButton ID="rdoFinancial1Yes" GroupName="Financial1" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoFinancial1No" GroupName="Financial1" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                            </td>
                                                                            <td class="cell2" align="right">
                                                                                Subcontractor&#39;s Current Bid:&nbsp;&nbsp;&nbsp; &#36;&nbsp;<asp:TextBox ID="txtSubcontractorCurrentBid"
                                                                                    CssClass="txtboxNum" runat="server"  MaxLength="13"></asp:TextBox>
                                                                               <Ajax:FilteredTextBoxExtender ID="ftxtSubcontractorCurrentBid" runat="server" TargetControlID="txtSubcontractorCurrentBid"
                                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                    </Ajax:FilteredTextBoxExtender>
                                                                            </td>
                                                                            <td class="cell3">
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                            </td>
                                                                            <td class="cell2" align="right">
                                                                            <span id="LargestMaxPastYear" runat="server">
                                                                                </span>&nbsp;&nbsp;&nbsp; &#36;&nbsp;<asp:TextBox
                                                                                    ID="txtMaxContract3Year" CssClass="txtboxNum" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td class="cell3">
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                2.
                                                                            </td>
                                                                            <td id="AannualRevenuePastYear" class="cell2" runat="server">
                                                                               
                                                                            </td>
                                                                            <td class="cell3">
                                                                                <asp:RadioButton ID="rdoFinancial2Yes" GroupName="Financial2" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoFinancial2No" GroupName="Financial2" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                            </td>
                                                                            <td class="cell2" align="right">
                                                                                Current Total Backlog:&nbsp;&nbsp;&nbsp; &#36;&nbsp;<asp:TextBox ID="txtCurrentTotalBacklog"
                                                                                    CssClass="txtboxNum" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td class="cell3">
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                            </td>
                                                                            <td class="cell2" align="right">
                                                                            <span id="AannualRevenuePastYear1" runat="server">
                                                                                </span>
                                                                                &nbsp;&nbsp;&nbsp; &#36;&nbsp;<asp:TextBox ID="txtAverageAnnualRevenue"
                                                                                    CssClass="txtboxNum" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td class="cell3">
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                3.
                                                                            </td>
                                                                            <td id="BidAannualRevenuePercentage" class="cell2" runat="server">
                                                                                
                                                                            </td>
                                                                            <td class="cell3">
                                                                                <asp:RadioButton ID="rdoFinancial3Yes" GroupName="Financial3" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoFinancial3No" GroupName="Financial3" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                4.
                                                                            </td>
                                                                            <td id="LowestbidPercentage" class="cell2" runat="server">
                                                                                
                                                                            </td>
                                                                            <td class="cell3">
                                                                                <asp:RadioButton ID="rdoFinancial4Yes" GroupName="Financial4" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoFinancial4No" GroupName="Financial4" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                            </td>
                                                                            <td class="cell2" align="right">
                                                                                Next lowest bid value:&nbsp;&nbsp;&nbsp; &#36;&nbsp;<asp:TextBox ID="txtNextLowestBid"
                                                                                    CssClass="txtboxNum" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td class="cell3">
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1" valign="top">
                                                                                5.
                                                                            </td>
                                                                            <td class="cell2">
                                                                                Are the Subcontractor&#39;s typical scope of work, type & geographical area substantially
                                                                                DIFFERENT from the proposed project?
                                                                            </td>
                                                                            <td class="cell3">
                                                                                <asp:RadioButton ID="rdoFinancial5Yes" GroupName="Financial5" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoFinancial5No" GroupName="Financial5" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4" class="tdheight">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4" class="bodytextright">
                                                                                <span class="riskCaption">High Risk Indicator:</span><span class="riskCell"><asp:CheckBox ID="chkFinancialRiskIndicator" runat="server" /></span> 
                                                                                <%--G. Vera 10/08/2012 - Added--%>
                                                                                <br />
                                                                                
                                                                                <span style="padding-right: 65px; font-style: italic">TWO or more YES</span>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                          <td width="100%" colspan="4" style="border-bottom:1px solid #cccccc"; >&nbsp;</td>
                                                                      </tr>
                                                               
                                                                            <tr>
                                                                                                <td colspan="4" class="bodytextbold">
                                                                                                    Comments:
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                           
                                                                                                <td colspan="4">
                                                                                                <table cellpadding="3" cellspacing="1" width="100%"><tr>
                                                                                                <td class="bodytextleft">
                                                                                              
                                                                                               <span style="word-wrap:break-word; vertical-align:top;  float:left; width: 99%;"  id="lblFComments" runat="server" ></span>
                                                                                              
                                                                                                </td></tr></table>
                                                                                                </td>
                                                                                            </tr>
                                                                    </table>
                                                                    
                                                                    <br />
                                                                    <br />
                                                                    <table cellpadding="5" cellspacing="0" width="100%" class="searchtableclass" >
                                                                        <tr>
                                                                            <td colspan="3" class="Header">
                                                                                Licensing Section
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                &nbsp;1.
                                                                            </td>
                                                                            <td class="cell2">
                                                                                The Subcontractor holds current required licenses for the project location.
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButton ID="rdoLicense1Yes" GroupName="License" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoLicense1No" GroupName="License" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                        </tr>
                                                                        <%--G. Vera 10/08/2012 - Added--%>
                                                                        <tr>
                                                                            <td colspan="3" style="font-style: italic; font-weight: bold; padding-left: 30px;">
                                                                                NOTE: If contract license is not required, check YES.
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                            </td>
                                                                            <td class="cell2" colspan="2">
                                                                                <table cellpadding="3" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="style3">
                                                                                            PM Verification:
                                                                                        </td>
                                                                                        <td align="right" class="style4">
                                                                                            Date:
                                                                                        </td>
                                                                                        <td class="style5">
                                                                                            <asp:TextBox ID="txtVerificationDate" CssClass="txtboxmedium" MaxLength="10" runat="server"></asp:TextBox>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtVerificationDate" TargetControlID="txtVerificationDate"
                                                                                                FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                            <Ajax:CalendarExtender ID="caleVerficationDate" Format="MM/dd/yyyy" TargetControlID="txtVerificationDate"
                                                                                                PopupButtonID="imgVerficationDate" runat="server">
                                                                                            </Ajax:CalendarExtender>
                                                                                        </td>
                                                                                        <td align="right" class="style6">
                                                                                            Notes:
                                                                                        </td>
                                                                                        <td height="auto">
                                                                                            <!-- *** Modified by N Schoenberger 11/16/2011 - replace textbox with <span>*** -->
                                                                                            <!--
                                                                                             <asp:TextBox ID="txtPMNotes" runat="server" CssClass="txtboxlarge" TabIndex="49"
                                                                                    TextMode="MultiLine" MaxLength="150" onFocus="javascript:textCounter(this,150,'yes');"
                                                                                    onKeyDown="javascript:textCounter(this,150,'yes');" 
                                                                                                 onKeyUp="javascript:textCounter(this,150,'yes');" Height="100%"></asp:TextBox>
                                                                                             -->
                                                                                            <!-- *** End Modification by N Schoenberger 11/16/2011 - replace textbox with <span>*** -->
                                                                                               <span style="word-wrap:break-word; float:left; width: 100%;"  
                                                                                                id="lblPMVerificationNotes" runat="server" ></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--<tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td colspan="4" class="bodytextright">
                                                                                            <asp:ImageButton ID="imbUpload1" runat="server" ImageUrl="~/Images/attach_document.jpg"
                                                                                                ImageAlign="AbsMiddle" OnClientClick="return showAttachment('1')" OnClick="imbUpload_Click" />
                                                                                        </td>
                                                                                    </tr>--%>
                                                                                    <tr>
                                                                                        <td colspan="5" class="bodytextleft">
                                                                                            <i>(Attach printout from website)</i>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                        <%--G. Vera 10/08/2012 - Added--%>
                                                                                        <td style="font-weight: bold; font-style: italic; padding-right: 20px" align="right">
                                                                                            If NO, Subcontractor is DISQUALIFIED.
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td width="100%" colspan="4" style="border-bottom:1px solid #cccccc"; >&nbsp;</td>
                                                                      </tr>
                                                                             <tr>
                                                                                                <td colspan="4" class="bodytextbold">
                                                                                                    Comments:
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                           
                                                                                                <td colspan="4">
                                                                                                <table cellpadding="3" cellspacing="1" width="100%"><tr>
                                                                                                <td class="bodytextleft">
                                                                                              
                                                                                               <span style="word-wrap:break-word; vertical-align:top;  float:left; width: 99%;"  id="lblLComments" runat="server" ></span>
                                                                                              
                                                                                                </td></tr></table>
                                                                                                </td>
                                                                                            </tr>
                                                                    </table>
                                                              
                                                      <br />
                                                                    <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass">
                                                                             
                                                        <tr>
                                                            <td colspan="4" class="Header">
                                                                Safety Section
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <table cellpadding="0" cellspacing="0" width="100%" class="even_pad">
                                                                   <tr>
                                                                            <td class="cell1">
                                                                                1.
                                                                            </td>
                                                                             <td id="EMRPastYear" class="cell2" runat="server">
                                                                                
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButton ID="rdoSafety1Yes" GroupName="Safety1" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoSafety1No" GroupName="Safety1" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trNonUSVendor" runat="server" style="display: none;">
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td colspan="3" class="redtextleft">
                                                                            *** International vendor. Exempt from EMR calculation. ***
                                                                        </td>
                                                                    </tr>
                                                                          <tr>
                                                                <td colspan="4" class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                            <td colspan="4" > <table cellpadding="3" cellspacing="0" width="100%" >
                                                            <tr>
                                                            <td class="bodytextbold_right" >
                                                                <asp:Literal ID="lblEMRYear1" runat="server" Text="EMR Year 1:" /></td>
                                                            <td><asp:TextBox ID="txtRate1" CssClass="txtboxsmall"  runat="server"></asp:TextBox></td>
                                                            <td class="bodytextbold_right">
                                                                <asp:Literal ID="lblEMRYear2" runat="server" Text="EMR Year 2:" /></td>
                                                            <td><asp:TextBox ID="txtRate2" CssClass="txtboxsmall" runat="server"></asp:TextBox></td>
                                                            <td class="bodytextbold_right">
                                                                <asp:Literal ID="lblEMRYear3" runat="server" Text="EMR Year 3:" /></td>
                                                            <td><asp:TextBox ID="txtRate3" CssClass="txtboxsmall" runat="server"></asp:TextBox></td></tr>
                                                                <%--G. Vera 01/31/2013 - New Row to display the EMR year we are using for calculation--%>
                                                                <tr>
                                                                    <td class="bodytextbold_right" style="font-size: smaller; font-weight: normal; padding-right: 15px;">
                                                                        <asp:Literal ID="lblYearDisp1" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td class="bodytextbold_right" style="font-size: smaller; font-weight: normal; padding-right: 15px;">
                                                                        <asp:Literal ID="lblYearDisp2" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td class="bodytextbold_right" style="font-size: smaller; font-weight: normal; padding-right: 15px;">
                                                                        <asp:Literal ID="lblYearDisp3" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                         </table> 
                                                          </td> 
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" class="tdheight">
                                                                </td>
                                                            </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                2.
                                                                            </td>
                                                                            <td class="cell2">
                                                                                Did Subcontractor answer &quot;NO&quot; to any safety question on the VQF?
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButton ID="rdoSafety2Yes" GroupName="Safety2" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoSafety2No" GroupName="Safety2" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4" class="tdheight">
                                                                            </td>
                                                                        </tr>
                                                                    <!--G. Vera 01/09/2017: START-->
                                                                    <tr>
                                                                        <td class="cell1">3.
                                                                        </td>
                                                                        <td class="cell2">Has company been cited by an Occupational Safety&nbsp;&amp;&nbsp;Health or
                                                                            Environmental Enforcement Agency in the last 3 years?
                                                                        </td>
                                                                        <td class="cell3">
                                                                            <asp:RadioButton ID="rdoSafetyCitationYes" GroupName="Safety3" runat="server" Text="&nbsp;Yes"
                                                                                TabIndex="55" onclick="Javascript:SetSafetyRiskIndicator();" />&nbsp;&nbsp;
                                                                            <asp:RadioButton ID="rdoSafetyCitationNo" GroupName="Safety3" runat="server" Text="&nbsp;No"
                                                                                TabIndex="56" onclick="Javascript:SetSafetyRiskIndicator();" />
                                                                        </td>
                                                                        <td></td>
                                                                        
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="bodytextright">&nbsp;&nbsp;
                                                                    
                                                                        </td>
                                                                        <td class="bodytextleft">
                                                                            <asp:Literal ID="ltrlHazardAnalysis" runat="server" />
                                                                        </td>
                                                                        <td></td>
                                                                        <td class="bodytextright">&nbsp;
                                                                        </td>
                                                                        <td>&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <%--<tr>
                                                                        <td class="cell1">4.
                                                                        </td>
                                                                        <td class="cell2">Is this company utilizing tiered subs on this project to complete their scope of work?
                                                                        </td>
                                                                        <td class="cell3">
                                                                            <asp:RadioButton ID="rdoSafetyUseTieredSubsYes" GroupName="Safety4" runat="server" Text="&nbsp;Yes"
                                                                                TabIndex="57" onclick="Javascript:SetSafetyRiskIndicator();" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoSafetyUseTieredSubsNo" GroupName="Safety4" runat="server" Text="&nbsp;No"
                                                                        TabIndex="58" onclick="Javascript:SetSafetyRiskIndicator();" />
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>--%>
                                                                    <!--G. Vera 01/09/2017: END-->
                                                                        <tr>
                                                                            <td colspan="4" class="bodytextright">
                                                                                <span class="riskCaption">High Risk Indicator:</span><span class="riskCell"><asp:CheckBox ID="chkSafetyRiskIndicator" runat="server" /></span> 
                                                                                <br />
                                                                                
                                                                                <%--G. Vera 10/08/2012 - added--%>
                                                                                <%--G. vera 12/28/2016 - modified--%>
                                                                                <span style="padding-right: 65px; font-style: italic">ONE or more YES</span>
                                                                            </td>                                                                          
                                                                        </tr>
                                                                        <tr>
                                                                          <td width="100%" colspan="4" style="border-bottom:1px solid #cccccc"; >&nbsp;</td>
                                                                      </tr>
                                                                             <tr>
                                                                                                <td colspan="4" class="bodytextbold">
                                                                                                    Comments:
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                           
                                                                                                <td colspan="4">
                                                                                                <table cellpadding="3" cellspacing="1" width="100%"><tr>
                                                                                                <td class="bodytextleft">
                                                                                              
                                                                                               <span style="word-wrap:break-word; vertical-align:top;  float:left; width: 99%;"  id="lblSComments" runat="server" ></span>
                                                                                              
                                                                                                </td></tr></table>
                                                                                                </td>
                                                                                            </tr>
                                                                </table> 
                                                            </td>
                                                        </tr>
                                                                     
                                                                    </table>
                                                                   <br />
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr >
                                                            <td>
                                                                    <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass" style="page-break-after: always;">
                                                                    <tr>
                                                            <td colspan="4" class="Header">
                                                                 Insurance &amp; Bonding Section
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td colspan="4">
                                                                 <table cellpadding="0" cellspacing="0" width="100%" class="even_pad">
                                                                 <tr>
                                                                            <td class="cell1">
                                                                                1.
                                                                            </td>
                                                                            <td class="cell2">
                                                                                Does Subcontractor meet all Haskell insurance level requirements?
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButton ID="rdoInsurance1Yes" GroupName="Insurance1" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoInsurance1No" GroupName="Insurance1" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                2.
                                                                            </td>
                                                                            <td class="cell2">
                                                                                If NO for #1, can Subcontractor obtain adequate insurance to meet requirements?
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButton ID="rdoInsurance2Yes" GroupName="Insurance2" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoInsurance2No" GroupName="Insurance2" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                3.
                                                                            </td>
                                                                            <td class="cell2">
                                                                                Does Subcontractor have a bonding company rated A or better by A.M. Best?
                                                                                <br />
                                                                                <div style="">
                                                                                    (Attach printout from www.ambest.com)</div>
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButton ID="rdoInsurance3Yes" GroupName="Insurance3" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoInsurance3No" GroupName="Insurance3" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                4.
                                                                            </td>
                                                                            <td class="cell2">
                                                                                Is available bonding capacity &gt; value of proposed subcontract?
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButton ID="rdoInsurance4Yes" GroupName="Insurance4" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoInsurance4No" GroupName="Insurance4" runat="server" Text="&nbsp;No" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="cell1">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td class="cell2" align="right">
                                                                                Available bonding capacity:&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtAvailBondingCapacity"
                                                                                    runat="server" CssClass="txtboxNum"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <%--<tr>
                                                                            <td colspan="4" class="bodytextright">
                                                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/attach_document.jpg"
                                                                                    ImageAlign="AbsMiddle" OnClientClick="return showAttachment('2')" />
                                                                            </td>
                                                                        </tr>--%>
                                                                        <tr>
                                                                            <td colspan="4" class="tdheight">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3" class="bodytextright">
                                                                                <span class="riskCaption">High Risk Indicator:</span><span class="riskCell"><asp:CheckBox ID="chkInsuranceRiskIndicator" runat="server" /></span> 
                                                                                <br />
                                                                               
                                                                                <%--G. Vera 10/08/2012 - Added--%>
                                                                                <span style="padding-right: 65px; font-style: italic">TWO or more NO</span>
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                          <td width="100%" colspan="4" style="border-bottom:1px solid #cccccc"; >&nbsp;</td>
                                                                      </tr>
                                                                             <tr>
                                                                                                <td colspan="4" class="bodytextbold">
                                                                                                    Comments:
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                           
                                                                                                <td colspan="4">
                                                                                                <table cellpadding="3" cellspacing="1" width="100%"><tr>
                                                                                                <td class="bodytextleft">
                                                                                              
                                                                                               <span style="word-wrap:break-word; vertical-align:top;  float:left; width: 99%;"  id="lblIComments" runat="server" ></span>
                                                                                              
                                                                                                </td></tr></table>
                                                                                                </td>
                                                                                            </tr>
                                                                 </table> 
                                                            </td>
                                                        </tr>
                                                                        
                                                                    </table>
                                                              </td></tr></table>   
                                                               
                                                                    
                                                    
                                                   
                                                    
                                                    <br />
                                                    <br />
                                                 <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass" style="page-break-after: always" >
                                                                    <tr>
                                                            <td colspan="4" class="Header">
                                                               Reference Section
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                
                                                            </td>
                                                        </tr>
                                                                  <tr>
                                                                            <td class="bodytextright" style="padding-left: 10px;">
                                                                                Project Reference 1:
                                                                            </td>
                                                                            <td style="padding-top: 10px;">
                                                                                <asp:TextBox ID="txtProjectReference1" CssClass="txtboxlarge" runat="server"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtProjectReference1" runat="server" TargetControlID="txtProjectReference1" FilterType="LowercaseLetters,UppercaseLetters"></Ajax:FilteredTextBoxExtender>
                                                                            </td>
                                                                            <td class="bodytextright" style="padding-top: 10px;">
                                                                                Date Called:
                                                                            </td>
                                                                            <td style="padding-top: 10px;">
                                                                                <asp:TextBox ID="txtReferenceDate1" CssClass="txtboxmedium" runat="server" MaxLength="10"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtReferenceDate1" TargetControlID="txtReferenceDate1"
                                                                                    FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <Ajax:CalendarExtender ID="caleReferenceDate1" TargetControlID="txtReferenceDate1"
                                                                                    PopupButtonID="imgReferenceDate1" runat="server" Format="MM/dd/yyyy">
                                                                                </Ajax:CalendarExtender>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextright" style="padding-left: 10px;">
                                                                                Project Reference 2:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtProjectReference2" CssClass="txtboxlarge" runat="server"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtProjectReference2" runat="server" TargetControlID="txtProjectReference2"
                                                                                    FilterType="LowercaseLetters,UppercaseLetters">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                            </td>
                                                                            <td class="bodytextright">
                                                                                Date Called:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtReferenceDate2" CssClass="txtboxmedium" MaxLength="10" runat="server"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtReferenceDate2" TargetControlID="txtReferenceDate2"
                                                                                    FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <Ajax:CalendarExtender ID="caleReferenceDate2" TargetControlID="txtReferenceDate2"
                                                                                    PopupButtonID="imgReferenceDate2" runat="server" Format="MM/dd/yyyy">
                                                                                </Ajax:CalendarExtender>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextright" style="padding-left: 10px;">
                                                                                Project Reference 3:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtProjectReference3" CssClass="txtboxlarge" runat="server"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectReference3"
                                                                                    FilterType="LowercaseLetters,UppercaseLetters">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                            </td>
                                                                            <td class="bodytextright">
                                                                                Date Called:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtReferenceDate3" CssClass="txtboxmedium" MaxLength="10" runat="server"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtReferenceDate3" TargetControlID="txtReferenceDate3"
                                                                                    FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <Ajax:CalendarExtender ID="caleReferenceDate3" TargetControlID="txtReferenceDate3"
                                                                                    PopupButtonID="imgReferenceDate3" runat="server" Format="MM/dd/yyyy">
                                                                                </Ajax:CalendarExtender>
                                                                            </td>
                                                                        </tr>
                                                                          <tr>
                                                                          <td width="100%" colspan="7" style="border-bottom:1px solid #cccccc"; >&nbsp;</td>
                                                                      </tr>
                                                                      <tr><td>&nbsp;</td></tr>
                                                                        <tr>
                                                                            <td class="bodytextright" style="padding-left: 10px;">
                                                                                Supplier Reference 1:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSupplierReference1" CssClass="txtboxlarge" runat="server"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSupplierReference1" runat="server" TargetControlID="txtSupplierReference1"
                                                                                    FilterType="LowercaseLetters,UppercaseLetters">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                            </td>
                                                                            <td class="bodytextright">
                                                                                Date Called:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSupplierDate1" CssClass="txtboxmedium" MaxLength="10" runat="server"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSupplierDate1" TargetControlID="txtSupplierDate1"
                                                                                    FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <Ajax:CalendarExtender ID="caleSupplierDate1" TargetControlID="txtSupplierDate1"
                                                                                    PopupButtonID="imgSupplierDate1" runat="server" Format="MM/dd/yyyy">
                                                                                </Ajax:CalendarExtender>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextright" style="padding-left: 10px;">
                                                                                Supplier Reference 2:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSupplierReference2" CssClass="txtboxlarge" runat="server"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSupplierReference2" runat="server" TargetControlID="txtSupplierReference2"
                                                                                    FilterType="LowercaseLetters,UppercaseLetters">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                            </td>
                                                                            <td class="bodytextright">
                                                                                Date Called:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSupplierDate2" CssClass="txtboxmedium" MaxLength="10" runat="server"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSupplierDate2" TargetControlID="txtSupplierDate2"
                                                                                    FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <Ajax:CalendarExtender ID="caleSupplierDate2" TargetControlID="txtSupplierDate2"
                                                                                    PopupButtonID="imgSupplierDate2" runat="server" Format="MM/dd/yyyy">
                                                                                </Ajax:CalendarExtender>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="style7" style="padding-left: 10px;">
                                                                                Supplier Reference 3:
                                                                            </td>
                                                                            <td class="style8">
                                                                                <asp:TextBox ID="txtSupplierReference3" CssClass="txtboxlarge" runat="server"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSupplierReference3" runat="server" TargetControlID="txtSupplierReference3"
                                                                                    FilterType="LowercaseLetters,UppercaseLetters">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                            </td>
                                                                            <td class="style7">
                                                                                Date Called:
                                                                            </td>
                                                                            <td class="style8">
                                                                                <asp:TextBox ID="txtSupplierDate3" CssClass="txtboxmedium" MaxLength="10" runat="server"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtSupplierDate3" TargetControlID="txtSupplierDate3"
                                                                                    FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <Ajax:CalendarExtender ID="CalendarExtender3" TargetControlID="txtSupplierDate3"
                                                                                    PopupButtonID="imgSupplierDate3" runat="server" Format="MM/dd/yyyy">
                                                                                </Ajax:CalendarExtender>
                                                                            </td>
                                                                        </tr>
                                                                          <%--<tr>
                                                                          <td width="100%" colspan="7" style="border-bottom:1px solid #cccccc"; >&nbsp;</td>
                                                                      </tr>--%>
                                                                      <tr><td>&nbsp;</td></tr>
                                                                        <tr>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td width="100%" colspan="7" style="border-bottom:1px solid #cccccc"; >&nbsp;</td>
                                                                      </tr>
                                                                        <%--<tr>
                                                                            <td>
                                                                            </td>
                                                                            <td colspan="3" align="right">
                                                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/attach_document.jpg"
                                                                                    ImageAlign="AbsMiddle" OnClientClick="return showAttachment('3')" />
                                                                            </td>
                                                                        </tr>--%>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                <table cellpadding="3" cellspacing="0" width="100%" class="even_pad" >
                                                                                    <tr>
                                                                                        <td class="cell1">
                                                                                            1.
                                                                                        </td>
                                                                                        <td class="cell2">
                                                                                            Have the project references indicated a satisfactory and timely performance?
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:RadioButton ID="rdoReference1Yes" GroupName="Reference1" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;&nbsp;
                                                                                            <asp:RadioButton ID="rdoReference1No" GroupName="Reference1" runat="server" Text="&nbsp;No" />
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="cell1">
                                                                                            2.
                                                                                        </td>
                                                                                        <td class="cell2">
                                                                                            Does Subcontractor pay vendors on time?
                                                                                            <br />
                                                                                            (Attach report from Prequalification Analyst)
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:RadioButton ID="rdoReference2Yes" GroupName="Reference2" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                            <asp:RadioButton ID="rdoReference2No" GroupName="Reference2" runat="server" Text="&nbsp;No" />
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="cell1">
                                                                                            3.
                                                                                        </td>
                                                                                        <td class="cell2">
                                                                                            Are Haskell internal references positive (if applicable)?
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:RadioButton ID="rdoReference3Yes" GroupName="Reference3" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                                            <asp:RadioButton ID="rdoReference3No" GroupName="Reference3" runat="server" Text="&nbsp;No" />&nbsp;&nbsp;
                                                                                            <asp:RadioButton ID="rdoReference3NA" GroupName="Reference3" runat="server" Text="&nbsp;NA" />
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3" class="tdheight">
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" class="bodytextright">
                                                                                            <span class="riskCaption">High Risk Indicator:</span><span class="riskCell"><asp:CheckBox ID="chkReferenceRiskIndicator" runat="server" /></span>
                                                                                            <br />
                                                                                         
                                                                                            <%--G. Vera 10/08/2012 - Added--%>
                                                                                            <span style="padding-right: 70px; font-style: italic">TWO or more NO</span>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                          <td width="100%" colspan="4" style="border-bottom:1px solid #cccccc"; >&nbsp;</td>
                                                                           </tr>
                                                                                         <tr>
                                                                                                <td colspan="4" class="bodytextbold">
                                                                                                    Comments:
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                           
                                                                                                <td colspan="4">
                                                                                                <table cellpadding="3" cellspacing="1" width="100%"><tr>
                                                                                                <td class="bodytextleft">
                                                                                              
                                                                                               <span style="word-wrap:break-word; vertical-align:top;  float:left; width: 99%;"  id="lblRComments" runat="server" ></span>
                                                                                              
                                                                                                </td></tr></table>
                                                                                                </td>
                                                                                            </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                   
                                                              
                                                                    <br />
                                                                    <table cellpadding="3" cellspacing="1" width="100%" class="searchtableclass" style="page-break-after: always;">
                                                                       <tr>
                                                            <td class="Header" colspan="3">
                                                                Risk Determination
                                                            </td>
                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3" class="bodytextbold">
                                                                                <%--G. Vera 12/29/2016: Add new notes--%>
                                                                                <span class="mandatorystar">* </span>Please note: SRE's for subcontract values over <strong>$<asp:Label runat="server" ID="docApprovalTreshold" Visible="true"><%= ConfigurationManager.AppSettings["DocApprovalTresholdDisplay"] %></asp:Label>.00</strong> or for subcontractors / vendors 
                                                                    deemed as High Risk on the SRE must be reviewed by the Director of Construction.
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3" class="tdheight">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextleft" style="width: 55%; vertical-align: middle;">
                                                                                <asp:CheckBox ID="chkAverageRisk" Text="&nbsp;Average Risk (ZERO to ONE section High Risk Indicators)"
                                                                                    runat="server" Font-Bold="true" />
                                                                            </td>
                                                                            <td style="width: 25%;">
                                                                                <asp:TextBox ID="txtProjectManagerName" runat="server"  CssClass="txtbox"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 20%;">
                                                                                <asp:TextBox ID="txtRiskDate1" runat="server"  CssClass="txtboxmedium"></asp:TextBox>
                                                                                <Ajax:CalendarExtender ID="caleRiskDate1" TargetControlID="txtRiskDate1" PopupButtonID="imgRiskDate1"
                                                                                    Format="MM/dd/yyyy" runat="server">
                                                                                </Ajax:CalendarExtender>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <ul class="sresublist">
                                                                                    <li>Implement controls identified in Section A below.</li>
                                                                                    <%--G. Vera 10/08/2012 - Added--%>
                                                                                    <%--G. Vera 06/25/2020--%>
                                                                                    <asp:CheckBoxList ID="chkListAvgRiskDet" runat="server" Enabled="true" RepeatDirection="Horizontal"
                                                                                           RepeatColumns="1" RepeatLayout="Table" CssClass="chkbox" CellPadding="3"/>
                                                                                </ul>
                                                                                <%--G. Vera 06/25/2020--%>
                                                                                <ul style="list-style-type: none">
                                                                                    <li>
                                                                                        <asp:TextBox runat="server" ID="txtAvgRiskDetOther" CssClass="txtboxarea"  
                                                                                                     Width="100%" TextMode="MultiLine" Wrap="true" MaxLength="500" hidden="hidden"/>

                                                                                    </li>
                                                                                </ul>
                                                                            </td>
                                                                            <td class="bodytextleft" valign="top">
                                                                                Project Manager Name / Signature
                                                                            </td>
                                                                            <td class="bodytextleft" valign="top">
                                                                                &nbsp;Date
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextleft">
                                                                                <asp:CheckBox ID="chkHighRisk" Text="&nbsp;High Risk (TWO sections are High Risk)"
                                                                                    runat="server" Font-Bold="true" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtGroupName1" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtRiskDate2" runat="server" CssClass="txtboxmedium"></asp:TextBox>
                                                                                <Ajax:CalendarExtender ID="caleRiskDate2" TargetControlID="txtRiskDate2" PopupButtonID="imgRiskDate2"
                                                                                    Format="MM/dd/yyyy" runat="server">
                                                                                </Ajax:CalendarExtender>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <ul class="sresublist">
                                                                                    <li>DOC to select one of the following options:</li>
                                                                                </ul>
                                                                                <%--G. Vera 10/26/2012 - Added--%>
                                                                                <ul style="list-style-type: none">
                                                                                    <li>
                                                                                        <asp:CheckBoxList ID="chkListHighRiskDet" runat="server" Enabled="true" CellPadding="3" /></li>
                                                                                        
                                                                                        <%--<input ID="chkListHighRiskDet" runat="server" type="checkbox" />&nbsp;&nbsp;<label id="lblListHighRiskDet" runat="server" /></li>
                                                                                        <li><input id="chkListHighRiskDet2" runat="server" type="checkbox" />&nbsp;&nbsp;<label
                                                                                                id="lblListHighRiskDet2" runat="server" /></li>
                                                                                        <li><input id="chkListHighRiskDet3" runat="server" type="checkbox" />&nbsp;&nbsp;<label
                                                                                                id="lblListHighRiskDet3" runat="server" /></li>
                                                                                        <li><input id="chkListHighRiskDet4" runat="server" type="checkbox" />&nbsp;&nbsp;<label
                                                                                                id="lblListHighRiskDet4" runat="server" /></li>--%>
                                                                                        
                                                                                </ul>
                                                                                <ul style="list-style-type: none"><li><asp:TextBox runat="server" ID="txtHighRiskDetOther" CssClass="txtboxarea"  Width="100%" TextMode="MultiLine" Wrap="true" MaxLength="500"/></li></ul>
                                                                            </td>
                                                                            <td class="bodytextleft" valign="top">
                                                                                DOC Name / Signature
                                                                            </td>
                                                                            <td class="bodytextleft" valign="top">
                                                                                &nbsp;Date
                                                                            </td>
                                                                        </tr>
                                                                       <tr><td></td></tr>
                                                                        <tr>
                                                                            <td class="bodytextleft">
                                                                                <asp:CheckBox ID="chkDisapproved" Text="&nbsp;Disapproved – DO NOT USE" runat="server"
                                                                                    Font-Bold="true" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtGroupName2" runat="server"  CssClass="txtbox"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtRiskDate3" runat="server" MaxLength="10" CssClass="txtboxmedium"></asp:TextBox>
                                                                                <Ajax:CalendarExtender ID="caleRiskDate3" TargetControlID="txtRiskDate3" PopupButtonID="imgRiskDate3"
                                                                                    Format="MM/dd/yyyy" runat="server">
                                                                                </Ajax:CalendarExtender>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                            <td class="bodytextleft" valign="top">
                                                                                DOC Name / Signature
                                                                            </td>
                                                                            <td class="bodytextleft" valign="top">
                                                                                &nbsp;Date
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td width="100%" colspan="4" style="border-bottom:1px solid #cccccc"; >&nbsp;</td>
                                                                       </tr>
                                                                      <tr>
                                                                            <td colspan="3" class="bodytextbold">
                                                                                Comments:
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                                           
                                                                            <td colspan="3">
                                                                            <table cellpadding="3" cellspacing="1" width="100%"><tr>
                                                                            <td class="bodytextleft">
                                                                                              
                                                                            <div style="word-wrap:break-word; vertical-align:top;  float:left; width: 99%; height: 100px"  id="lblRiskComments" runat="server" ></div>
                                                                                              
                                                                            </td></tr></table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <br />
                                                                    <br />
                                                                    <%--G. Vera 10/24/2012 - Added--%> 
                                                                    <table cellpadding="3" cellspacing="1" width="100%" class="searchtableclass" >
                                                                        <tr>
                                                                            <td class="Header">
                                                                                Enrollment (DOC to select one of the following)
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div style="margin-top: 10px; padding-left: 5px;">
                                                                                    <div>
                                                                                        <div class="bodytextleft">
                                                                                            <asp:CheckBoxList ID="chklstEnrollment" runat="server" Enabled="true" /><br />
                                                                                            <asp:TextBox runat="server" ID="txtEnrollmentOther" CssClass="txtboxarea"  Width="100%" TextMode="MultiLine" Wrap="true" MaxLength="500"/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    
                                                                    <table cellpadding="3" cellspacing="1" class="searchtableclass" width="100%">
                                                                        <tr>
                                                                            <td class="Header">
                                                                                Required Risk Management Controls of Subcontractors
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div style="margin-top: 10px; padding-left: 5px;">
                                                                                    <div>
                                                                                        <strong style="font-family: Tahoma; font-size: 12px;">A.&nbsp;&nbsp;&nbsp;&nbsp;Average
                                                                                            Risk Management</strong>
                                                                                            <%--G. Vera 10/08/2012 - Modified--%>
                                                                                            <ul class="sresublist">
                                                                                                <asp:CheckBoxList ID="chkListAverageRisk" runat="server" Enabled="true" />
                                                                                                
                                                                                            </ul>
                                                                                        <ul style="list-style-type: none"><li><asp:TextBox runat="server" ID="txtAverageRiskControlsOther" CssClass="txtboxarea"  Width="100%" TextMode="MultiLine" Wrap="true" MaxLength="500" /></li></ul>

                                                                                    </div>
                                                                                    <div>
                                                                                        <strong style="font-family: Tahoma; font-size: 12px;">B.&nbsp;&nbsp;&nbsp;&nbsp;High
                                                                                            Risk Management&nbsp;(Requires approval of the DOC)</strong>
                                                                                        <ul class="sresublist">
                                                                                            <asp:CheckBoxList ID="chkListHighRisk" runat="server" Enabled="true" />
                                                                                            
                                                                                        </ul>
                                                                                        <ul style="list-style-type: none"><li><asp:TextBox runat="server" ID="txtHighRiskControlsOther" CssClass="txtboxarea"  Width="100%" TextMode="MultiLine" Wrap="true" MaxLength="500" /></li></ul>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td width="100%" colspan="4" style="border-bottom:1px solid #cccccc"; >&nbsp;</td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td colspan="3" class="bodytextbold">
                                                                            Comments:
                                                                            </td>
                                                                        </tr>
                                                                                       
                                                                        <tr>
                                                                            <td class="tdheight">
                                                                                <span style="word-wrap: break-word; vertical-align: top; float: left; width: 99%;
                                                                                    height: 100px" id="txtRiskMgmntComments" runat="server"></span>
                                                                                <%--<asp:TextBox ID="txtRiskMgmntComments" runat="server" Height="100px" 
                                                                                    Width="99%" MaxLength="1500" TextMode="MultiLine" CssClass="txtboxmultilarge" />--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                             
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                           
                                           
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
  
    </form>
</body>
</html>


