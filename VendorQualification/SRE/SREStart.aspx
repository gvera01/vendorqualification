﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SREStart.aspx.cs" Inherits="SREStart" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Haskell</title>
    <link href="../css/Haskell.css" rel="Stylesheet" />
    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>

    <script type="text/javascript">
            function ClientItemSelected(sender, args) {
                document.getElementById("<%=hdnVendorId.ClientID%>").value = args.get_value();

            }
//             paging method
             function GetValue(i) {
        var str = document.getElementById("<%=hdnPage.ClientID%>");
        str.value = i;
    }
    function isbtnclick() 
    {
    if(document.getElementById("<%=hdnVendorIds.ClientID%>").value!="")
    {
        document.getElementById("<%=hdnbtnclick.ClientID%>").value="click";
        }
    }
    function selectVendor(sender, id) {
        var ids = document.getElementById("<%=hdnVendorIds.ClientID%>").value;
        if (sender.checked) {
            ids = id + ",";
        }
        else {
            if (ids.indexOf(id + ",") != -1) {
                ids = ids.replace(id + ",", "");
            }
        }
        document.getElementById("<%=hdnVendorIds.ClientID%>").value = ids;
    }
  
    </script>

</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbSearch">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="bottom">
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="formhead">
                                                    Subcontractor Risk Evaluation
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="tdButtons" runat="server" style="display: none">
                                                    <asp:ImageButton ID="imbAdd" runat="server" ImageUrl="~/Images/addnew.jpg" OnClick="imbAdd_Click"
                                                        ToolTip="Add New Subcontractor Risk Evaluation" TabIndex="4" />
                                                    &nbsp;
                                                    <asp:ImageButton ID="imbExists" runat="server" ImageUrl="~/Images/viewexist.jpg"
                                                        ToolTip="View Existing Subcontractor Risk Evaluation" OnClick="imbExists_Click"
                                                        TabIndex="5" />
                                                    <asp:HiddenField ID="hdnClick" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="Vendortd" runat="server" style="display: none" class="even_pad">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                    <tr align="right">
                                                    <td>
                                                                                                        </td>
                                                    
                                                <td >
                                                <asp:ImageButton ID="imgback" runat="server" ImageUrl="~/Images/back.jpg" ToolTip="Back"
                                                        OnClick="imgback_Click" TabIndex="6" />
                                                        <br />
                                                        <br />
                                                </td>
                                                    </tr>
                                                        <tr>
                                                            <td class="formtdlt" width="41%" valign="top">
                                                                &nbsp;Vendor Name from Vendor Database
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtVendor" runat="server" CssClass="ajaxtxtbox" MaxLength="60"></asp:TextBox>
                                                                <%--<div id="listPlacement" style="height: 100px; overflow: scroll;">
                                                                </div>--%>
                                                                <Ajax:AutoCompleteExtender ID="autoComplete1" runat="server" BehaviorID="AutoCompleteEx"
                                                                    CompletionListElementID="listPlacement" EnableCaching="false" FirstRowSelected="True"
                                                                    MinimumPrefixLength="1" ServiceMethod="GetVendorList" ServicePath="~/AutoComplete.asmx"
                                                                    TargetControlID="txtVendor" CompletionSetCount="10" OnClientItemSelected="ClientItemSelected"
                                                                    CompletionInterval="500">
                                                                    <%-- <Animations>
                                                            <OnShow>
                                                              <Sequence>
                                                                <OpacityAction Opacity="0" />
                                                                <HideAction Visible="true" />
                                                                  <ScriptAction Script="
                                                                    var behavior = $find('AutoCompleteEx');
                                                                    if (!behavior._height) {
                                                                        var target = behavior.get_completionList();
                                                                        behavior._height = target.offsetHeight - 2;
                                                                        target.style.height = '0px';
                                                                    }" />
                                                                <Parallel Duration=".4">
                                                                <FadeIn />
                                                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                                                                </Parallel>
                                                                </Sequence>
                                                                 </OnShow>
                                                                  <OnHide>
                                                                 <Parallel Duration=".4">
                                                                 <FadeOut />
                                                                 <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                                                                 </Parallel>
                                                                 </OnHide>
                                                                    </Animations>--%>
                                                                </Ajax:AutoCompleteExtender>
                                                                <asp:HiddenField ID="hdnMatch" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr style="display: none">
                                                            <td class="formtdlt">
                                                                &nbsp;Project Number
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:DropDownList ID="ddlProjectNumber" runat="Server" CssClass="ddlbox" Width="367px"
                                                                    TabIndex="1">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr style="display: none">
                                                            <td class="formtdlt">
                                                                Bid Package No
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtBidPackage" runat="server" CssClass="ajaxtxtbox" MaxLength="15"
                                                                    TabIndex="2"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdlt">
                                                                Vendor Number from E1 / JDE
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtVNo" runat="server" CssClass="ajaxtxtbox" MaxLength="100" TabIndex="3"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td class="formtdrt">
                                                                <br />
                                                                <asp:ImageButton ID="imbSearch" ImageUrl="~/Images/search.jpg" ImageAlign="AbsMiddle"
                                                                    ToolTip="Search Vendor List" runat="server" OnClick="imbSearch_Click" TabIndex="4" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                                    <td >
                                                                        <table width="100%" cellpadding="3" cellspacing="0" runat="server">
                                            <tr id="start" runat="server" style="display: none">
                                                <td class="tdheight">
                                                    <asp:HiddenField ID="hdnCount" runat="server" />
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="bodytextleft" valign="middle">
                                                                Total Number of Records:&nbsp;
                                                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                            </td>
                                                            <td class="bodytextright" valign="middle">
                                                                Number of Records Per Page:&nbsp;&nbsp;
                                                            </td>
                                                            <td width="5%">
                                                                <asp:DropDownList ID="ddlNumber" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlNumber_SelectedIndexChanged"
                                                                    CssClass="ddlbox">
                                                                    <asp:ListItem Value="10" >10</asp:ListItem>
                                                                    <asp:ListItem Value="20">20</asp:ListItem>
                                                                    <asp:ListItem Value="30">30</asp:ListItem>
                                                                    <asp:ListItem Value="40"> 40</asp:ListItem>
                                                                    <asp:ListItem Value="50">50</asp:ListItem>
                                                                    <asp:ListItem Value="100">100</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="start1" runat="server" style="display: none">
                                                <td width="5%">
                                                    <asp:GridView ID="grvRiskEvaluation" runat="server" CellPadding="3" DataKeyNames="Email,Company,VQFStatus,FK_VendorId"
                                                        GridLines="none" Width="100%" AllowSorting="True" CellSpacing="1" AutoGenerateColumns="False"
                                                        OnRowDataBound="grvRiskEvaluation_RowDataBound" OnSorting="grvRiskEvaluation_Sorting">
                                                        <RowStyle CssClass="listone" />
                                                        <AlternatingRowStyle CssClass="listtwo" />
                                                        <HeaderStyle CssClass="listheading" />
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White"
                                                                ItemStyle-Width="15px">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkVendor" runat="server" />
                                                                    <Ajax:MutuallyExclusiveCheckBoxExtender ID="MutuallyExclusiveCheckBoxExtender1" runat="server"
                                                                        TargetControlID="chkVendor" Key="chkVendor">
                                                                    </Ajax:MutuallyExclusiveCheckBoxExtender>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Vendor Name" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                ItemStyle-CssClass="gridpad" SortExpression="Company">
                                                                <ItemTemplate>
                                                                    <%#Eval("Company") %>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gridpad" ForeColor="White"></HeaderStyle>
                                                                <ItemStyle CssClass="gridpad"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="VQF Status" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                ItemStyle-CssClass="gridpad" SortExpression="VQFStatus">
                                                                <ItemTemplate>
                                                                    <%#Eval("VQFStatus")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gridpad" ForeColor="White"></HeaderStyle>
                                                                <ItemStyle CssClass="gridpad"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Vendor Number" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                ItemStyle-CssClass="gridpad" SortExpression="VendorNumber">
                                                                <ItemTemplate>
                                                                    <%#Eval("VendorNumber")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gridpad" ForeColor="White"></HeaderStyle>
                                                                <ItemStyle CssClass="gridpad"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField HeaderText="SRE Status" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                ItemStyle-CssClass="gridpad" SortExpression="SREStatus">
                                                                <ItemTemplate>
                                                                    <%#Eval("SREStatus")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gridpad" ForeColor="White"></HeaderStyle>
                                                                <ItemStyle CssClass="gridpad"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Project Number" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                ItemStyle-CssClass="gridpad" SortExpression="ProjectNumber" HeaderStyle-Width="200px">
                                                                <ItemTemplate>
                                                                    <%#Eval("ProjectNumber")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gridpad" ForeColor="White" Width="200px"></HeaderStyle>
                                                                <ItemStyle CssClass="gridpad"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Bid Package #" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                ItemStyle-CssClass="gridpad" SortExpression="BidPackageNumber">
                                                                <ItemTemplate>
                                                                    <%#Eval("BidPackageNumber")%>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gridpad" ForeColor="White"></HeaderStyle>
                                                                <ItemStyle CssClass="gridpad"></ItemStyle>
                                                            </asp:TemplateField>--%>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr id="start2" runat="server" style="display: none;">
                                                <td class="bodytextcenter">
                                                    Page&nbsp;&nbsp;
                                                    <asp:ImageButton ID="imbPrevious" runat="server" ImageUrl="~/Images/pre.jpg" OnClick="imbPrevious_Click" />&nbsp;&nbsp;
                                                    <asp:PlaceHolder ID="plcCrntPage" runat="server"></asp:PlaceHolder>
                                                    of
                                                    <asp:Label ID="lblTotalPage" runat="server"></asp:Label>
                                                    &nbsp;&nbsp;<asp:ImageButton ID="imbNext" runat="server" ImageUrl="~/Images/next1.jpg"
                                                        OnClick="imbNext_Click" Style="height: 7px; width: 7px" />
                                                    <asp:HiddenField ID="hdnPage" runat="server" />
                                                    <asp:HiddenField ID="hdnCurrentPage" runat="server" />
                                                    <asp:HiddenField ID="hdnSort" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <br />
                                                </td>
                                            </tr></table></td></tr>   
                                            <tr id="start3" runat="server" style="display: none;">
                                                <td align="center">
                                                    <asp:ImageButton ID="imbNotify" ImageUrl="~/Images/pmfinal.jpg" ImageAlign="AbsMiddle"
                                                        ToolTip="PM Mail Notification" runat="server" TabIndex="3" OnClientClick="isbtnclick();"
                                                        OnClick="imbNotify_Click" />&nbsp;&nbsp;
                                                    <asp:ImageButton ID="imbNew" ImageUrl="~/Images/startsre.jpg" ImageAlign="AbsMiddle"
                                                        ToolTip="Start SRE" runat="server" TabIndex="3" OnClick="imbNew_Click"
                                                        OnClientClick="isbtnclick();" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Ajax:ModalPopupExtender ID="mpopAlert" runat="server" BackgroundCssClass="popupbg"
                                TargetControlID="hdnAlert" PopupControlID="divAlert" OkControlID="imbOK">
                            </Ajax:ModalPopupExtender>
                            <div id="divAlert" class="popupdivbg1" style="display: none">
                                <table cellspacing="1" cellpadding="3" width="100%">
                                    <tr>
                                        <td class="Messageheading">
                                            Message
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdheight">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl" style="padding-right: 0px;">
                                            <asp:Label ID="lblMessage" CssClass="errormsg" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdheight">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;<asp:ImageButton ID="imbOK" ImageUrl="~/Images/ok.jpg" ImageAlign="AbsMiddle"
                                                runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdheight">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:HiddenField ID="hdnVendorId" runat="server" />
                            <asp:HiddenField ID="hdnAlert" runat="server" />
                            <asp:HiddenField ID="hdnbtnclick" runat="server" />
                            <asp:HiddenField ID="hdnVendorIds" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
