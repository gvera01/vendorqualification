﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SubContratorRiskEvaluation.aspx.cs" EnableViewState="true"
    Inherits="SubContratorRiskEvaluation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />

    <script src="../Script/jquery.js" type="text/javascript"></script>

    <meta http-equiv="Pragma" content="no-cache" />
    <style type="text/css">
        body
        {
            font-family: Tahoma;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
        .cell1
        {
            width: 3%;
            font-family: tahoma;
            font-size: 11px;
        }
        .cell2
        {
            width: 76%;
            font-family: tahoma;
            font-size: 11px;
        }
        .cell3
        {
            width: 16%;
            font-family: tahoma;
            font-size: 11px;
        }
        .sresublist li
        {
            list-style-type: katakana-iroha;
            line-height: 17px;
            font-family: Tahoma;
            font-size: 11px;
        }
        .riskCell
        {
            text-align: right;
            padding-right: 5px;
        }
        .riskCaption
        {
            font-family: Tahoma;
            font-size: 13px;
            font-weight: bold;
        }
        .style2
        {
            font-family: Tahoma;
            text-align: left;
            color: black;
            font-size: 11px;
            height: 21px;
            padding-left: 10px;
            padding-bottom: 5px;
        }
    </style>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        var clicked = false;
        var clicked = false; function CheckBrowser() {
            if (clicked == false) {
            }
            else {
                clicked = false;
            }
        }
        function bodyUnload() {
            if (clicked == false) {
                var request = GetRequest();
                request.open("GET", "../Admin/LOGOUT.aspx", true);
                request.send();
            }
        }
        function GetRequest() {
            var xmlHttp = null;
            try {
                xmlHttp = new XMLHttpRequest();
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch (e) {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
            }
            return xmlHttp;
        } 
    </script>

</head>
<body onunload="bodyUnload();" onclick="clicked=true;">
    <form id="form1" runat="server" defaultbutton="imbSave">
    <asp:ScriptManager ID="clscpmgrRiskEvaluation" runat="server" EnablePageMethods="true" >
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                    </td>
                                    <td class="contenttd">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="0">
                                            <tr>
                                                <td colspan="2" class="formhead" align="right">
                                                    Subcontractor Risk Evaluation
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td colspan="2" class="sectionpad">
                                                    <table cellpadding="3" cellspacing="1" width="100%" class="searchtableclass">
                                            </tr>
                                            <tr class="ContentHeader">
                                                <td>
                                                    Set Alert
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt" id="tdAlertComment" runat="server" style="display: none;">
                                                    <asp:TextBox ID="txtAlertComments" CssClass="txtboxlarge" TextMode="MultiLine" Width="290"
                                                        Height="50" MaxLength="2000" runat="server" TabIndex="0"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;<asp:CheckBox ID="chkVendor" runat="server" TabIndex="1" />
                                                    <asp:Image ID="imgVendor" runat="server" ImageUrl="~/Images/V.gif" ImageAlign="AbsMiddle" />
                                                    &nbsp;&nbsp;
                                                    <asp:RadioButton ID="rdoRed" runat="server" GroupName="Alert" TabIndex="2" />
                                                    <asp:Image ID="imgRadAlert" ImageUrl="~/Images/red.png" ImageAlign="AbsMiddle" AlternateText="Red Alert"
                                                        runat="server" />
                                                    &nbsp;&nbsp;
                                                    <asp:RadioButton ID="rdoYellow" runat="server" GroupName="Alert" TabIndex="3" />
                                                    <asp:Image ID="imgYellowAlert" ImageUrl="~/Images/yellow.png" ImageAlign="AbsMiddle"
                                                        AlternateText="Yellow Alert" runat="server" />
                                                    &nbsp;&nbsp;
                                                    <asp:RadioButton ID="rdoNoWarning" runat="server" GroupName="Alert" TabIndex="4" />
                                                    <asp:Image ID="imgNoWarning" ImageUrl="~/Images/nowarning.gif" ImageAlign="AbsMiddle"
                                                        AlternateText="Yellow Alert" runat="server" />
                                                    &nbsp;&nbsp;
                                                    <asp:ImageButton ID="imbAlertSubmit" ImageAlign="AbsMiddle" ImageUrl="~/Images/submit.jpg"
                                                        runat="server" TabIndex="5" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight" colspan="2">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="sectionpad">
                                        <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass">
                                            <tr>
                                                <td align="right">
                                                </td>
                                                <td align="right">
                                                </td>
                                                <td align="right">
                                                    <asp:ImageButton ID="imgback" runat="server" ImageUrl="~/Images/back.jpg" ToolTip="Back"
                                                        OnClick="imgback_Click" TabIndex="6" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style2">
                                                    Project Name / Location
                                                </td>
                                                <td class="style2">
                                                    Project Number
                                                </td>
                                                <td class="style2">
                                                    Date
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="txtProjectName" CssClass="txtboxlarge" runat="server" TabIndex="7"></asp:TextBox>
                                                </td>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="txtProjectNumber" CssClass="txtboxlarge" runat="server" TabIndex="8">
                                                    </asp:TextBox>
                                                </td>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="txtDate" CssClass="txtbox" runat="server" MaxLength="10" TabIndex="9"
                                                        Enabled="False"></asp:TextBox>
                                                    <%--<Ajax:FilteredTextBoxExtender ID="ftxtDate" TargetControlID="txtDate" FilterType="Numbers,Custom"
                                                        ValidChars="/" runat="server">
                                                    </Ajax:FilteredTextBoxExtender>--%>
                                                    <%--<asp:Image ID="imgVendorQualificationDate" AlternateText="" ImageAlign="AbsMiddle"
                                                                    ImageUrl="~/Images/cal.gif" runat="server" />--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt">
                                                    Subcontractor
                                                </td>
                                                <td class="formtdrt">
                                                    Subcontractor Contact Name
                                                </td>
                                                <td class="formtdrt">
                                                    Date of Vendor Qualification Form
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="txtVendorName" CssClass="txtboxlarge" runat="server" TabIndex="10"></asp:TextBox>
                                                </td>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="txtVendorContactName" CssClass="txtboxlarge" runat="server" TabIndex="11"></asp:TextBox>
                                                </td>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="txtVendorQualificationDate" CssClass="txtboxmedium" MaxLength="10"
                                                        runat="server" TabIndex="12"></asp:TextBox>
                                                    <%--<Ajax:CalendarExtender ID="caleVendorQualificationDate" TargetControlID="txtVendorQualificationDate"
                                                                    PopupButtonID="imgVendorQualificationDate" runat="server" Format="MM/dd/yyyy">
                                                                </Ajax:CalendarExtender>--%>
                                                    <Ajax:FilteredTextBoxExtender ID="ftxtVendorQualificationDate" TargetControlID="txtVendorQualificationDate"
                                                        FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                    </Ajax:FilteredTextBoxExtender>
                                                    <%-- <table cellpadding="3" cellspacing="1" width="100%" class="searchtableclass">
                                                                        <tr>
                                                            <td colspan="4" class="Header">
                                                                Business Section
                                                            </td>
                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                
                                                                            </td>
                                                                        </tr>
                                                                    </table>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt">
                                                    <span class="mandatorystar">*</span> Approximate Subcontract Value
                                                </td>
                                                <td class="formtdrt">
                                                    <span class="mandatorystar">*</span> Bid Package Description
                                                </td>
                                                <td class="formtdrt">
                                                    Bid Package #
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt">
                                                    <span class="cell2">&#36;&nbsp;</span>
                                                    <asp:TextBox ID="txtAppSubContractorValue" CssClass="txtboxlargeNum" runat="server"
                                                        TabIndex="13" MaxLength="13"></asp:TextBox>
                                                    <%--  <table cellpadding="3" cellspacing="1" width="100%" class="searchtableclass">
                                                                        <tr>
                                                            <td colspan="4" class="Header">
                                                                Business Section
                                                            </td>
                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                
                                                                            </td>
                                                                        </tr>
                                                                    </table>--%>
                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAppSubContractorValue" runat="server" TargetControlID="txtAppSubContractorValue"
                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                    </Ajax:FilteredTextBoxExtender>
                                                </td>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="txtBidpackageDesc" CssClass="txtboxlarge" runat="server" TabIndex="14"
                                                        MaxLength="200"></asp:TextBox>
                                                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtBidpackageDesc"
                                                        FilterType="Numbers,Custom,lowercaseLetters,uppercaseLetters" ValidChars=".,',',-,' ',/,(,),:,"
                                                        InvalidChars="">
                                                    </Ajax:FilteredTextBoxExtender>
                                                </td>
                                                <td class="formtdrt">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtBidpackageNumber" CssClass="txtbox" runat="server" TabIndex="15"></asp:TextBox>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="bodytextleft sectionpad">
                                        <%--G. Vera 09/21/2012 - Added--%>
                                        <asp:Label ID="lblOriginator" runat="server" Font-Bold="true"/>
                                        <br/>
                                        <br />
                                        The PM must answer each question from the data supplied by the Subcontractor’s VQF,
                                        and the PM must obtain appropriate references. If the Subcontractor supplied incomplete
                                        data, the deficiency should be promptly resolved by the PM.
                                </tr>
                                <tr>
                                    <td colspan="2" class="sectionpad">
                                        <Ajax:Accordion ID="acdnBusiness" runat="server" HeaderCssClass="accordionHeader"
                                            HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                                            FadeTransitions="true" FramesPerSecond="40" TransitionDuration="40" AutoSize="none"
                                            RequireOpenedPane="true" SuppressHeaderPostbacks="true">
                                            <Panes>
                                                <Ajax:AccordionPane ID="AccordionPane1" runat="server" TabIndex="16">
                                                    <Header>
                                                        Business / Legal Section</Header>
                                                    <Content>
                                                        <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass">
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    &nbsp;&nbsp;&nbsp;1.
                                                                </td>
                                                                <td id="BusinessYear" class="bodytextleft" runat="server">
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoBusiness1Yes" GroupName="Business1" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="17" Enabled="false" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoBusiness1No" GroupName="Business1" runat="server" Text="&nbsp;No"
                                                                        TabIndex="18" Enabled="false" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    &nbsp;&nbsp;&nbsp;2.
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    Has the Subcontractor failed to complete awarded work or terminated for cause?
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoBusiness2Yes" GroupName="Business2" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="19" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoBusiness2No" GroupName="Business2" runat="server" Text="&nbsp;No"
                                                                        TabIndex="20" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 12/28/2016:  Add the other VQF legal questions that was left out--%>
                                                            <tr>
                                                                <td class="bodytextleft">&nbsp;&nbsp;&nbsp;3.
                                                                </td>
                                                                <td class="bodytextleft">Any of its Owners, Officers or Major Stockholders currently involved in any arbitration, litigation, suits or liens?
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoBusinessLitigationYes" GroupName="Business5" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="21" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoBusinessLitigationNo" GroupName="Business5" runat="server" Text="&nbsp;No"
                                                                        TabIndex="22" />
                                                                </td>
                                                                <td>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    &nbsp;&nbsp;&nbsp;4.
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    Has the Subcontractor had any judgments, bankruptcies, or reorganizations?
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoBusiness3Yes" GroupName="Business3" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="23" Enabled="false" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoBusiness3No" GroupName="Business3" runat="server" Text="&nbsp;No"
                                                                        TabIndex="24" Enabled="false" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    &nbsp;&nbsp;&nbsp;5.
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    Any indictments or convictions of felony or other criminal conduct for key personnel?
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoBusiness4Yes" GroupName="Business4" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="25" Enabled="false" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoBusiness4No" GroupName="Business4" runat="server" Text="&nbsp;No"
                                                                        TabIndex="26" Enabled="false" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" class="bodytextright">
                                                                    <span class="riskCaption">High Risk Indicator:</span> <span class="riskCell">
                                                                        <asp:CheckBox ID="chkBusinessRiskIndicator" runat="server" Enabled="false" TabIndex="27" /></span>
                                                                    <br />
                                                                    <%--G. Vera 10/08/2012 - added--%>
                                                                    <div style="padding-right: 65px;font-style:italic">TWO or more YES</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" class="bodytextbold">
                                                                    Comments
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:TextBox ID="txtBcomments" runat="server" onFocus="javascript:textCounter(this,1500,'yes');"
                                                                        onKeyDown="javascript:textCounter(this,1500,'yes');" onKeyUp="javascript:textCounter(this,1500,'yes');"
                                                                        Height="125px" TextMode="MultiLine" TabIndex="24" Width="99%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <%-- <table cellpadding="3" cellspacing="1" width="100%" class="searchtableclass">
                                                                        <tr>
                                                            <td colspan="4" class="Header">
                                                                Business Section
                                                            </td>
                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                
                                                                            </td>
                                                                        </tr>
                                                                    </table>--%>
                                                    </Content>
                                                </Ajax:AccordionPane>
                                                <Ajax:AccordionPane ID="AccordionPane2" runat="server" TabIndex="26">
                                                    <Header>
                                                        Financial Section</Header>
                                                    <Content>
                                                        <table cellpadding="3" cellspacing="0" class="searchtableclass" width="100%">
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    &nbsp;&nbsp;&nbsp;1.
                                                                </td>
                                                                <td id="LargestMaxPercentage" class="bodytextleft" runat="server">
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoFinancial1Yes" GroupName="Financial1" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="27" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoFinancial1No" GroupName="Financial1" runat="server" Text="&nbsp;No"
                                                                        TabIndex="28" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                </td>
                                                                <td class="bodytextright">
                                                                    <span class="mandatorystar">*</span> Subcontractor's Current Bid:&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&#36;&nbsp;<asp:TextBox
                                                                        ID="txtSubcontractorCurrentBid" CssClass="txtboxNum" runat="server" TabIndex="29"
                                                                        MaxLength="13"></asp:TextBox>
                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSubcontractorCurrentBid" runat="server" TargetControlID="txtSubcontractorCurrentBid"
                                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                    </Ajax:FilteredTextBoxExtender>
                                                                </td>
                                                                <td class="bodytextleft">
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td class="bodytextright">
                                                                    <span id="LargestMaxPastYear" runat="server"></span>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&#36;&nbsp;<asp:TextBox
                                                                        ID="txtMaxContract3Year" CssClass="txtboxNum" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td class="cell3">
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    &nbsp;&nbsp;&nbsp;2.
                                                                </td>
                                                                <td id="AannualRevenuePastYear" class="bodytextleft" runat="server">
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoFinancial2Yes" GroupName="Financial2" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoFinancial2No" GroupName="Financial2" runat="server" Text="&nbsp;No" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td class="bodytextright">
                                                                    Current Total Backlog:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &#36;&nbsp;<asp:TextBox ID="txtCurrentTotalBacklog"
                                                                        CssClass="txtboxNum" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td class="bodytextleft">
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td class="bodytextright">
                                                                    <span id="AannualRevenuePastYear1" runat="server"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    &#36;&nbsp;<asp:TextBox ID="txtAverageAnnualRevenue" CssClass="txtboxNum" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td class="cell3">
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    &nbsp;&nbsp;&nbsp;3.
                                                                </td>
                                                                <td id="BidAannualRevenuePercentage" class="bodytextleft" runat="server">
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoFinancial3Yes" GroupName="Financial3" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoFinancial3No" GroupName="Financial3" runat="server" Text="&nbsp;No" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    &nbsp;&nbsp;&nbsp;4.
                                                                </td>
                                                                <td id="LowestbidPercentage" class="bodytextleft" runat="server">
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoFinancial4Yes" GroupName="Financial4" runat="server" Text="&nbsp;Yes" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoFinancial4No" GroupName="Financial4" runat="server" Text="&nbsp;No" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cell1">
                                                                </td>
                                                                <td class="bodytextright" align="right">
                                                                    <span class="mandatorystar">*</span> Next lowest bid value:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    &#36;&nbsp;<asp:TextBox ID="txtNextLowestBid" CssClass="txtboxNum" runat="server"
                                                                        TabIndex="39" MaxLength="13"></asp:TextBox>
                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtlowestBid" runat="server" TargetControlID="txtNextLowestBid"
                                                                        FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                    </Ajax:FilteredTextBoxExtender>
                                                                </td>
                                                                <td class="cell3">
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft" valign="top">
                                                                    <span class="mandatorystar">*</span>5.
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    Are the Subcontractor&#39;s typical scope of work, type & geographical area substantially
                                                                    DIFFERENT from the proposed project?
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoFinancial5Yes" GroupName="Financial5" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="40" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoFinancial5No" GroupName="Financial5" runat="server" Text="&nbsp;No"
                                                                        TabIndex="41" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" class="tdheight">
                                                                    <asp:ImageButton ID="imbViewLegal" runat="server" OnClick="imbViewLegal_Click" ImageUrl="~/Images/vieweditleg.jpg"
                                                                        TabIndex="42" ToolTip="View / Edit Legal" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" class="bodytextright">
                                                                    <span class="riskCaption">High Risk Indicator:</span><span class="riskCell"><asp:CheckBox
                                                                        ID="chkFinancialRiskIndicator" runat="server" Enabled="false" TabIndex="43" /></span>
                                                                    <br />
                                                                    <%--G. Vera 10/08/2012 - added--%>
                                                                    <span style="padding-right: 65px; font-style: italic">TWO or more YES</span>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td colspan="4" class="bodytextbold">
                                                                    Comments
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:TextBox ID="txtFcomments" runat="server" onFocus="javascript:textCounter(this,1500,'yes');"
                                                                        onKeyDown="javascript:textCounter(this,1500,'yes');" onKeyUp="javascript:textCounter(this,1500,'yes');"
                                                                        Height="125px" TextMode="MultiLine" TabIndex="41" Width="99%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <%--<table cellpadding="3" cellspacing="1" width="100%" class="searchtableclass">
                                                        <tr>
                                                            <td colspan="4" class="Header">
                                                                Financial Section
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                    </Content>
                                                </Ajax:AccordionPane>
                                                <Ajax:AccordionPane ID="AccordionPane3" runat="server" TabIndex="44">
                                                    <Header>
                                                        Licensing Section</Header>
                                                    <Content>
                                                        <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass">
                                       
                                                            <tr>
                                                                <td class="cell1">
                                                                    <span class="mandatorystar">*</span>1.
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    The Subcontractor holds current required licenses for the project location.
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoLicense1Yes" GroupName="License" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="45" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoLicense1No" GroupName="License" runat="server" Text="&nbsp;No"
                                                                        TabIndex="46" />
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 10/08/2012 - Added--%>
                                                            <tr><td colspan="3" style="font-style: italic;font-weight:bold;padding-left: 30px;">NOTE: If contract license is not required, check YES.</td></tr>
                                                            <tr>
                                                                <td class="cell1">
                                                                </td>
                                                                <td class="cell2" colspan="2">
                                                               
                                                                    <table cellpadding="3" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td class="bodytextleft">
                                                                                PM Verification:
                                                                                <br />
                                                                                
                                                                            </td>
                                                                            <td class="bodytextright">
                                                                                <span class="mandatorystar">*</span> Date:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtVerificationDate" CssClass="txtboxmedium" MaxLength="10" runat="server"
                                                                                    TabIndex="47"></asp:TextBox>
                                                                                <asp:ImageButton ID="imgVerficationDate" AlternateText="" ImageAlign="AbsMiddle"
                                                                                    ImageUrl="~/Images/cal.gif" runat="server" />
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtVerificationDate" TargetControlID="txtVerificationDate"
                                                                                    FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <Ajax:CalendarExtender ID="caleVerficationDate" Format="MM/dd/yyyy" TargetControlID="txtVerificationDate"
                                                                                    PopupButtonID="imgVerficationDate" runat="server">
                                                                                </Ajax:CalendarExtender>
                                                                            </td>
                                                                            <td class="bodytextright">
                                                                                <span class="mandatorystar">*</span> Notes:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtPMNotes" runat="server" CssClass="txtboxlarge" TabIndex="49"
                                                                                    TextMode="MultiLine" MaxLength="150" onFocus="javascript:textCounter(this,150,'yes');"
                                                                                    onKeyDown="javascript:textCounter(this,150,'yes');" onKeyUp="javascript:textCounter(this,150,'yes');"></asp:TextBox>
                                                                                <%--<Ajax:FilteredTextBoxExtender ID="ftxtPMNotes" runat="server" TargetControlID="txtPMNotes"
                                                                                                FilterType="LowercaseLetters,UppercaseLetters">
                                                                                            </Ajax:FilteredTextBoxExtender>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" class="bodytextleft">
                                                                                <i>(Attach printout from website)</i>
                                                                            </td>
                                                                        </tr>

                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    
                                                                </td>
                                                                <td>
                                                                    
                                                                </td>
                                                                <%--G. Vera 10/08/2012 - Added--%>
                                                                <td >
                                                                    <div style="font-weight: bold; font-style: italic; padding-right: 70px">
                                                                        If NO, Subcontractor is DISQUALIFIED.
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" class="bodytextbold">
                                                                    Comments
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="txtLcomments" runat="server" onFocus="javascript:textCounter(this,1500,'yes');"
                                                                        onKeyDown="javascript:textCounter(this,1500,'yes');" onKeyUp="javascript:textCounter(this,1500,'yes');"
                                                                        Height="125px" TextMode="MultiLine" TabIndex="49" Width="99%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Content>
                                                </Ajax:AccordionPane>
                                                <Ajax:AccordionPane ID="AccordionPane4" runat="server" TabIndex="50">
                                                    <Header>
                                                        Safety Section</Header>
                                                    <Content>
                                                        <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass">
                                                        
                                                            <tr>
                                                                <td class="bodytextright">
                                                                    &nbsp;&nbsp;1.
                                                                </td>
                                                                <td id="EMRPastYear" class="bodytextleft" runat="server">
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td class="bodytextright">
                                                                    <asp:RadioButton ID="rdoSafety1Yes" GroupName="Safety1" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="51" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoSafety1No" GroupName="Safety1" runat="server" Text="&nbsp;No"
                                                                        TabIndex="52" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr id="trInternationalVendor" runat="server" style="display:none">
                                                            <td>&nbsp;</td>
                                                                <td colspan="4" class="redtextleft">
                                                                *** International vendor. Exempt from EMR calculation. ***
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5">
                                                                    <table cellpadding="3" cellspacing="0" width="100%">
                                                                        
                                                                        <tr>
                                                                            <td class="bodytextbold_right">
                                                                                <asp:Literal ID="lblEMRYear1" runat="server" Text="EMR Year 1: " />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtRate1" CssClass="txtboxsmall" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td class="bodytextbold_right">
                                                                                <asp:Literal ID="lblEMRYear2" runat="server" Text="EMR Year 2: " />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtRate2" CssClass="txtboxsmall" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td class="bodytextbold_right">
                                                                                <asp:Literal ID="lblEMRYear3" runat="server" Text="EMR Year 3: " />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtRate3" CssClass="txtboxsmall" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>

                                                                        <%--G. Vera 01/31/2013 - New Row to display the EMR year we are using for calculation--%>
                                                                        <tr>
                                                                            <td class="bodytextbold_right" style="font-size: smaller; font-weight: normal;padding-right: 15px;">
                                                                                <asp:Literal ID="lblYearDisp1" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                            <td class="bodytextbold_right" style="font-size: smaller; font-weight: normal; padding-right: 15px;">
                                                                                <asp:Literal ID="lblYearDisp2" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                            <td class="bodytextbold_right" style="font-size: smaller; font-weight: normal; padding-right: 15px;">
                                                                                <asp:Literal ID="lblYearDisp3" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextright">
                                                                    &nbsp;&nbsp;2.
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    Did Subcontractor answer &quot;NO&quot; to any safety question on the VQF?
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td class="bodytextright">
                                                                    <asp:RadioButton ID="rdoSafety2Yes" GroupName="Safety2" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="53" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoSafety2No" GroupName="Safety2" runat="server" Text="&nbsp;No"
                                                                        TabIndex="54" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <!--G. Vera 01/09/2017: START-->
                                                            <tr>
                                                                <td class="bodytextright">&nbsp;&nbsp;
                                                                    3.
                                                                </td>
                                                                <td class="bodytextleft">Has company been cited by an Occupational Safety&nbsp;&amp;&nbsp;Health or <br />Environmental Enforcement Agency in the last 3 years?
                                                                </td>
                                                                <td></td>
                                                                <td class="bodytextright">
                                                                    <asp:RadioButton ID="rdoSafetyCitationYes" GroupName="Safety3" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="55" Enabled="false"/>&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoSafetyCitationNo" GroupName="Safety3" runat="server" Text="&nbsp;No"
                                                                        TabIndex="56" Enabled="false"/>
                                                                </td>
                                                                <td>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">&nbsp;</td>
                                                            </tr>
                                                            <%--<tr>
                                                                <td class="bodytextright">&nbsp;&nbsp;<span class="mandatorystar">* </span>4.
                                                                </td>
                                                                <td class="bodytextleft">Is this company utilizing tiered subs on this project to complete their scope of work?
                                                                </td>
                                                                <td></td>
                                                                <td class="bodytextright">
                                                                    <asp:RadioButton ID="rdoSafetyUseTieredSubsYes" GroupName="Safety4" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="57" onclick="Javascript:SetSafetyRiskIndicator();" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoSafetyUseTieredSubsNo" GroupName="Safety4" runat="server" Text="&nbsp;No"
                                                                        TabIndex="58" onclick="Javascript:SetSafetyRiskIndicator();" />
                                                                </td>
                                                                <td>&nbsp;
                                                                </td>
                                                            </tr>--%>
                                                            <!--G. Vera 01/09/2017: END-->
                                                            <tr>
                                                                <td colspan="5" class="tdheight">
                                                                    <asp:ImageButton ID="imbViewEMR" runat="server" OnClick="imbViewEMR_Click" ImageUrl="~/Images/vieweditemr.jpg"
                                                                        TabIndex="55" ToolTip="View Edit EMR" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5">
                                                                    <table cellpadding="3" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td class="bodytextright" width="95%">
                                                                                <span class="riskCaption">High Risk Indicator:</span><span class="riskCell"><asp:CheckBox
                                                                                    ID="chkSafetyRiskIndicator" runat="server" Enabled="false" TabIndex="56" /></span>
                                                                                <br />
                                                                                <%--G. Vera 10/08/2012 - added--%>
                                                                                <%--G. vera 12/28/2016 - modified--%>
                                                                                <span style="padding-right: 65px; font-style: italic">ONE or more YES</span>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td class="bodytextright">&nbsp;&nbsp;
                                                                    
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:Literal ID="ltrlHazardAnalysis" runat="server" />
                                                                </td>
                                                                <td></td>
                                                                <td class="bodytextright">&nbsp;
                                                                </td>
                                                                <td>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">&nbsp;</td>
                                                            </tr>
                                                            <tr>

                                                                <td colspan="5" class="bodytextbold">
                                                                    Comments
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5">
                                                                    <asp:TextBox ID="txtScomments" runat="server" onFocus="javascript:textCounter(this,1500,'yes');"
                                                                        onKeyDown="javascript:textCounter(this,1500,'yes');" onKeyUp="javascript:textCounter(this,1500,'yes');"
                                                                        Height="125px" TextMode="MultiLine" TabIndex="55" Width="99%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <%-- <table cellpadding="3" cellspacing="1" width="100%" class="searchtableclass">
                                                        <tr>
                                                            <td colspan="4" class="Header">
                                                                Safety Section
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                    </Content>
                                                </Ajax:AccordionPane>
                                                <Ajax:AccordionPane ID="AccordionPane5" runat="server" TabIndex="57">
                                                    <Header>
                                                        Insurance &amp; Bonding Section</Header>
                                                    <Content>
                                                        <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass">
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    <span class="mandatorystar">*</span>1.
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    Does Subcontractor meet all Haskell insurance level requirements?
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoInsurance1Yes" GroupName="Insurance1" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="58" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoInsurance1No" GroupName="Insurance1" runat="server" Text="&nbsp;No"
                                                                        TabIndex="59" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    &nbsp;&nbsp;&nbsp;2.
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    If NO for #1, can Subcontractor obtain adequate insurance to meet requirements?
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoInsurance2Yes" GroupName="Insurance2" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="60" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoInsurance2No" GroupName="Insurance2" runat="server" Text="&nbsp;No"
                                                                        TabIndex="61" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    <span class="mandatorystar">*</span>3.
                                                                </td>
                                                                <%--G. Vera - changed--%>
                                                                <td class="bodytextleft">
                                                                    Does Subcontractor have a bonding company rated A or better by A.M. Best?
                                                                    <br />
                                                                    <div style="">(Attach printout from www.ambest.com)</div>
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoInsurance3Yes" GroupName="Insurance3" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="62" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoInsurance3No" GroupName="Insurance3" runat="server" Text="&nbsp;No"
                                                                        TabIndex="63" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    &nbsp;&nbsp;&nbsp;4.
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    Is available bonding capacity &gt; value of proposed subcontract?
                                                                </td>
                                                                <td class="bodytextleft">
                                                                    <asp:RadioButton ID="rdoInsurance4Yes" GroupName="Insurance4" runat="server" Text="&nbsp;Yes"
                                                                        TabIndex="64" />&nbsp;&nbsp;
                                                                    <asp:RadioButton ID="rdoInsurance4No" GroupName="Insurance4" runat="server" Text="&nbsp;No"
                                                                        TabIndex="65" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    &nbsp;
                                                                </td>
                                                                <td class="bodytextright">
                                                                    Available bonding capacity:&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtAvailBondingCapacity"
                                                                        runat="server" CssClass="txtboxNum" TabIndex="66" ReadOnly="true"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <%--<tr>
                                                                            <td colspan="4" class="bodytextright">
                                                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/attach_document.jpg"
                                                                                    ImageAlign="AbsMiddle" OnClientClick="return showAttachment('2')" />
                                                                            </td>
                                                                        </tr>--%>
                                                            <tr>
                                                                <td colspan="4" class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>

                                                                <td colspan="4" class="bodytextright">
                                                                    <span class="riskCaption">High Risk Indicator:</span>&nbsp;&nbsp;<span class="riskCell">
                                                                        <asp:CheckBox ID="chkInsuranceRiskIndicator" runat="server" Enabled="false" TabIndex="67" /></span>
                                                                    <br />
                                                                    <%--G. Vera 10/08/2012 - added--%>
                                                                    <span style="padding-right: 75px; font-style: italic">TWO or more NO</span>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td colspan="4" class="bodytextbold">
                                                                    Comments
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:TextBox ID="txtIcomments" runat="server" onFocus="javascript:textCounter(this,1500,'yes');"
                                                                        onKeyDown="javascript:textCounter(this,1500,'yes');" onKeyUp="javascript:textCounter(this,1500,'yes');"
                                                                        Height="125px" TextMode="MultiLine" TabIndex="66" Width="99%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Content>
                                                </Ajax:AccordionPane>
                                                <Ajax:AccordionPane ID="AccordionPane6" runat="server" TabIndex="68">
                                                    <Header>
                                                        Reference Section</Header>
                                                    <Content>
                                                        <table cellpadding="3" cellspacing="0" width="100%" class="searchtableclass">
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td class="bodytextright" >
                                                                    <span class="mandatorystar">*</span> Project Reference 1:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtProjectReference1" CssClass="txtboxlarge2" runat="server" TabIndex="69"
                                                                        MaxLength="100"></asp:TextBox>
                                                                    <%--<Ajax:FilteredTextBoxExtender ID="ftxtProjectReference1" runat="server" TargetControlID="txtProjectReference1" FilterType="LowercaseLetters,UppercaseLetters,Numbers" ></Ajax:FilteredTextBoxExtender>--%>
                                                                </td>
                                                                <td class="bodytextright">
                                                                    <span class="mandatorystar">*</span> Date Called:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtReferenceDate1" CssClass="txtboxmedium" runat="server" MaxLength="10"
                                                                        TabIndex="70"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgReferenceDate1" AlternateText="" ImageAlign="AbsMiddle" ImageUrl="~/Images/cal.gif"
                                                                        runat="server" TabIndex="71" />
                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtReferenceDate1" TargetControlID="txtReferenceDate1"
                                                                        FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                    </Ajax:FilteredTextBoxExtender>
                                                                <Ajax:CalendarExtender ID="caleReferenceDate1" TargetControlID="txtReferenceDate1"
                                                                        PopupButtonID="imgReferenceDate1" runat="server" Format="MM/dd/yyyy">
                                                                    </Ajax:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextright">
                                                                    <span class="mandatorystar">*</span> Project Reference 2:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtProjectReference2" CssClass="txtboxlarge2" runat="server" TabIndex="72"
                                                                        MaxLength="100"></asp:TextBox>
                                                                    <%--<Ajax:FilteredTextBoxExtender ID="ftxtProjectReference2" runat="server" TargetControlID="txtProjectReference2"
                                                                                    FilterType="LowercaseLetters,UppercaseLetters">--%>
                                                                    <%-- </Ajax:FilteredTextBoxExtender>--%>
                                                                </td>
                                                                <td class="bodytextright">
                                                                    <span class="mandatorystar">*</span> Date Called:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtReferenceDate2" CssClass="txtboxmedium" MaxLength="10" runat="server"
                                                                        TabIndex="73"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgReferenceDate2" AlternateText="" ImageAlign="AbsMiddle" ImageUrl="~/Images/cal.gif"
                                                                        runat="server" TabIndex="74" />
                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtReferenceDate2" TargetControlID="txtReferenceDate2"
                                                                        FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                    </Ajax:FilteredTextBoxExtender>
                                                                    <Ajax:CalendarExtender ID="caleReferenceDate2" TargetControlID="txtReferenceDate2"
                                                                        PopupButtonID="imgReferenceDate2" runat="server" Format="MM/dd/yyyy">
                                                                    </Ajax:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextright">
                                                                    <span class="mandatorystar">*</span> Project Reference 3:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtProjectReference3" CssClass="txtboxlarge2" runat="server" TabIndex="75"
                                                                        MaxLength="100"></asp:TextBox>
                                                                    <%--<Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProjectReference3"
                                                                                    FilterType="LowercaseLetters,UppercaseLetters">
                                                                                </Ajax:FilteredTextBoxExtender>--%>
                                                                </td>
                                                                <td class="bodytextright">
                                                                    <span class="mandatorystar">*</span> Date Called:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtReferenceDate3" CssClass="txtboxmedium" MaxLength="10" runat="server"
                                                                        TabIndex="76"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgReferenceDate3" AlternateText="" ImageAlign="AbsMiddle" ImageUrl="~/Images/cal.gif"
                                                                        runat="server" TabIndex="77" />
                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtReferenceDate3" TargetControlID="txtReferenceDate3"
                                                                        FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                    </Ajax:FilteredTextBoxExtender>
                                                                    <Ajax:CalendarExtender ID="caleReferenceDate3" TargetControlID="txtReferenceDate3"
                                                                        PopupButtonID="imgReferenceDate3" runat="server" Format="MM/dd/yyyy">
                                                                    </Ajax:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="100%" colspan="7" style="border-bottom: 1px solid #cccccc;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextright">
                                                                    Supplier Reference 1:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSupplierReference1" CssClass="txtboxlarge2" runat="server" TabIndex="78"
                                                                        MaxLength="100"></asp:TextBox>
                                                                    <%--<Ajax:FilteredTextBoxExtender ID="ftxtSupplierReference1" runat="server" TargetControlID="txtSupplierReference1"
                                                                                    FilterType="LowercaseLetters,UppercaseLetters">
                                                                                </Ajax:FilteredTextBoxExtender>--%>
                                                                </td>
                                                                <td class="bodytextright">
                                                                    Date Called:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSupplierDate1" CssClass="txtboxmedium" MaxLength="10" runat="server"
                                                                        TabIndex="79"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgSupplierDate1" AlternateText="" ImageAlign="AbsMiddle" ImageUrl="~/Images/cal.gif"
                                                                        runat="server" TabIndex="80" />
                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSupplierDate1" TargetControlID="txtSupplierDate1"
                                                                        FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                    </Ajax:FilteredTextBoxExtender>
                                                                    <Ajax:CalendarExtender ID="caleSupplierDate1" TargetControlID="txtSupplierDate1"
                                                                        PopupButtonID="imgSupplierDate1" runat="server" Format="MM/dd/yyyy">
                                                                    </Ajax:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextright">
                                                                    Supplier Reference 2:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSupplierReference2" CssClass="txtboxlarge2" runat="server" TabIndex="81"
                                                                        MaxLength="100"></asp:TextBox>
                                                                    <%-- <Ajax:FilteredTextBoxExtender ID="ftxtSupplierReference2" runat="server" TargetControlID="txtSupplierReference2"
                                                                                    FilterType="LowercaseLetters,UppercaseLetters">
                                                                                </Ajax:FilteredTextBoxExtender>--%>
                                                                </td>
                                                                <td class="bodytextright">
                                                                    Date Called:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSupplierDate2" CssClass="txtboxmedium" MaxLength="10" runat="server"
                                                                        TabIndex="82"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgSupplierDate2" AlternateText="" ImageAlign="AbsMiddle" ImageUrl="~/Images/cal.gif"
                                                                        runat="server" TabIndex="83" />
                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSupplierDate2" TargetControlID="txtSupplierDate2"
                                                                        FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                    </Ajax:FilteredTextBoxExtender>
                                                                    <Ajax:CalendarExtender ID="caleSupplierDate2" TargetControlID="txtSupplierDate2"
                                                                        PopupButtonID="imgSupplierDate2" runat="server" Format="MM/dd/yyyy">
                                                                    </Ajax:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextright">
                                                                    Supplier Reference 3:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSupplierReference3" CssClass="txtboxlarge2" runat="server" TabIndex="84"
                                                                        MaxLength="100"></asp:TextBox>
                                                                    <%--<Ajax:FilteredTextBoxExtender ID="ftxtSupplierReference3" runat="server" TargetControlID="txtSupplierReference3"
                                                                                    FilterType="LowercaseLetters,UppercaseLetters">
                                                                                </Ajax:FilteredTextBoxExtender>--%>
                                                                </td>
                                                                <td class="bodytextright">
                                                                    Date Called:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSupplierDate3" CssClass="txtboxmedium" MaxLength="10" runat="server"
                                                                        TabIndex="85"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgSupplierDate3" AlternateText="" ImageAlign="AbsMiddle" ImageUrl="~/Images/cal.gif"
                                                                        runat="server" TabIndex="86" />
                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtSupplierDate3" TargetControlID="txtSupplierDate3"
                                                                        FilterType="Numbers,Custom" ValidChars="/" runat="server">
                                                                    </Ajax:FilteredTextBoxExtender>
                                                                    <Ajax:CalendarExtender ID="CalendarExtender3" TargetControlID="txtSupplierDate3"
                                                                        PopupButtonID="imgSupplierDate3" runat="server" Format="MM/dd/yyyy">
                                                                    </Ajax:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="100%" colspan="7" style="border-bottom: 1px solid #cccccc;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <%--<tr>
                                                                            <td>
                                                                            </td>
                                                                            <td colspan="3" align="right">
                                                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/attach_document.jpg"
                                                                                    ImageAlign="AbsMiddle" OnClientClick="return showAttachment('3')" />
                                                                            </td>
                                                                        </tr>--%>
                                                            <tr>
                                                                <td colspan="7">
                                                                    <table cellpadding="3" cellspacing="0" width="100%" >
                                                                        <tr>
                                                                            <td class="bodytextright">
                                                                                <span class="mandatorystar">*</span>1.
                                                                            </td>
                                                                            <td class="bodytextleft">
                                                                                Have the project references indicated a satisfactory and timely performance?
                                                                            </td>
                                                                            <td class="bodytextleft">
                                                                                <asp:RadioButton ID="rdoReference1Yes" GroupName="Reference1" runat="server" Text="&nbsp;Yes"
                                                                                    TabIndex="87" />&nbsp;&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoReference1No" GroupName="Reference1" runat="server" Text="&nbsp;No"
                                                                                    TabIndex="88" />
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextright">
                                                                                <span class="mandatorystar">*</span>2.
                                                                            </td>
                                                                            <td class="bodytextleft">
                                                                                Does Subcontractor pay vendors on time?
                                                                                <br />
                                                                                (Attach report from Prequalification Analyst)
                                                                            </td>
                                                                            <td class="bodytextleft">
                                                                                <asp:RadioButton ID="rdoReference2Yes" GroupName="Reference2" runat="server" Text="&nbsp;Yes"
                                                                                    TabIndex="89" />&nbsp;&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoReference2No" GroupName="Reference2" runat="server" Text="&nbsp;No"
                                                                                    TabIndex="90" />
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextright">
                                                                                <span class="mandatorystar">*</span>3.
                                                                            </td>
                                                                            <td class="bodytextleft">
                                                                                Are Haskell internal references positive (if applicable)?
                                                                            </td>
                                                                            <td class="bodytextleft">
                                                                                <asp:RadioButton ID="rdoReference3Yes" GroupName="Reference3" runat="server" Text="&nbsp;Yes"
                                                                                    TabIndex="91" />&nbsp;&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoReference3No" GroupName="Reference3" runat="server" Text="&nbsp;No"
                                                                                    TabIndex="92" />&nbsp;&nbsp;&nbsp;
                                                                                <asp:RadioButton ID="rdoReference3NA" GroupName="Reference3" runat="server" Text="&nbsp;NA"
                                                                                    TabIndex="92" />
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3" class="tdheight">
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4" class="bodytextright">
                                                                                <span class="riskCaption">High Risk Indicator:</span><span class="riskCell">
                                                                                    <asp:CheckBox ID="chkReferenceRiskIndicator" runat="server" Enabled="false" TabIndex="93" /></span>
                                                                                <br />
                                                                                <%--G. Vera 10/08/2012 - added--%>
                                                                                <span style="padding-right: 70px; font-style: italic">TWO or more NO</span>
                                                                            </td>
                                                                        </tr>                                                                        
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td colspan="4" class="bodytextbold">
                                                                    Comments
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:TextBox ID="txtRcomments" runat="server" onFocus="javascript:textCounter(this,1500,'yes');"
                                                                        onKeyDown="javascript:textCounter(this,1500,'yes');" onKeyUp="javascript:textCounter(this,1500,'yes');"
                                                                        Height="125px" TextMode="MultiLine" TabIndex="92" Width="99%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <%--<table cellpadding="3" cellspacing="1" width="100%" class="searchtableclass">
                                                        <tr>
                                                            <td colspan="4" class="Header">
                                                                Insurance &amp; Bonding Section
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                    </Content>
                                                </Ajax:AccordionPane>
                                                <Ajax:AccordionPane ID="AccordionPane7" runat="server" TabIndex="94">
                                                    <Header>
                                                        Risk Determination</Header>
                                                    <Content>
                                                        <table cellpadding="3" cellspacing="1" width="100%" class="searchtableclass">
                                                           
                                                            <tr>
                                                                <td colspan="3" class="bodytextbold">
                                                                    <%--G. Vera 12/29/2016: Add new notes--%>
                                                                    <span class="mandatorystar">* </span>Please note: SRE's for subcontract values over <strong>$<asp:Label runat="server" ID="docApprovalTreshold" Visible="true"><%= ConfigurationManager.AppSettings["DocApprovalTresholdDisplay"] %></asp:Label>.00</strong> or for subcontractors / vendors 
                                                                    deemed as High Risk on the SRE must be reviewed by the Director of Construction.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft" style="width: 55%; vertical-align: middle;">
                                                                    <asp:CheckBox ID="chkAverageRisk" Enabled="false" runat="server" TabIndex="95" />&nbsp;
                                                                    <strong>Average Risk (ZERO to ONE section High Risk Indicators)</strong>
                                                                </td>
                                                                <td style="width: 25%;">
                                                                    <asp:TextBox ID="txtProjectManagerName" runat="server" Enabled="false" CssClass="txtbox"
                                                                        TabIndex="96"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 20%;">
                                                                    <asp:TextBox ID="txtRiskDate1" runat="server" Enabled="false" CssClass="txtboxmedium"
                                                                        TabIndex="97"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgRiskDate1" ImageUrl="~/Images/cal.gif" ImageAlign="AbsMiddle"
                                                                        runat="server" />
                                                                    <Ajax:CalendarExtender ID="caleRiskDate1" TargetControlID="txtRiskDate1" PopupButtonID="imgRiskDate1"
                                                                        Format="MM/dd/yyyy" runat="server">
                                                                    </Ajax:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    <%--G. Vera 10/08/2012 - Added--%>
                                                                    <ul >
                                                                        <li>Implement controls identified in Section A below.</li>
                                                                        <%--G. Vera 06/25/2020--%>
                                                                        <asp:CheckBoxList ID="chkListAvgRiskDet" runat="server" Enabled="true" RepeatDirection="Horizontal"
                                                                                           RepeatColumns="1" RepeatLayout="Table" CssClass="chkbox" CellPadding="3"/>
                                                                        
                                                                        <%--<li style="margin-right: 10px">PM shall implement risk management controls identified in section A 
                                                                            of the Risk Management Controls section below.</li> --%>    
                                                                   </ul>
                                                                    <%--G. Vera 06/25/2020--%>
                                                                    <ul style="list-style-type: none">
                                                                        <li>
                                                                            <asp:TextBox runat="server" ID="txtAvgRiskDetOther" CssClass="txtboxarea"  
                                                                                         Width="100%" TextMode="MultiLine" Wrap="true" MaxLength="500" hidden="hidden"/>

                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <td class="bodytextleft" valign="top">
                                                                    <%--Modified 11/19/2018 - Refer to change comment 015 in code file--%>
                                                                    Completed By:
                                                                </td>
                                                                <td class="bodytextleft" valign="top">
                                                                    &nbsp;Date
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    <asp:CheckBox ID="chkHighRisk" Enabled="false" runat="server" TabIndex="99" />&nbsp;
                                                                    <strong>High Risk (TWO sections are High Risk)</strong>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtGroupName1" runat="server" CssClass="txtbox"
                                                                        TabIndex="100"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtRiskDate2" runat="server" CssClass="txtboxmedium" Enabled="true"
                                                                        TabIndex="101"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgRiskDate2" ImageUrl="~/Images/cal.gif" ImageAlign="AbsMiddle"
                                                                        runat="server" />
                                                                    <Ajax:CalendarExtender ID="caleRiskDate2" TargetControlID="txtRiskDate2" PopupButtonID="imgRiskDate2"
                                                                        Format="MM/dd/yyyy" runat="server">
                                                                    </Ajax:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    <ul>
                                                                        <li>DOC to select one of the following options:</li>
                                                                        <%--<li>Provide DOC with certified financial statement and D&amp;B report.</li>--%>
                                                                        <%--G. Vera 10/08/2012 - Added--%>
                                                                        <asp:CheckBoxList ID="chkListHighRiskDet" runat="server" Enabled="true" RepeatDirection="Horizontal"
                                                                                           RepeatColumns="1" RepeatLayout="Table" CssClass="chkbox" CellPadding="3"/>
                                                                        
                                                                            
                                                                    </ul>
                                                                    <ul style="list-style-type: none">
                                                                        <li>
                                                                            <asp:TextBox runat="server" ID="txtHighRiskDetOther" CssClass="txtboxarea"  
                                                                                         Width="100%" TextMode="MultiLine" Wrap="true" MaxLength="500" hidden="hidden"/>

                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <td class="bodytextleft" valign="top">
                                                                    DOC Name / Signature
                                                                </td>
                                                                <td class="bodytextleft" valign="top">
                                                                    &nbsp;Date
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr><td colspan="3"></td></tr>
                                                            <tr>
                                                                <td class="bodytextleft" valign="top">
                                                                    <asp:CheckBox ID="chkDisapproved" runat="server" Enabled="false" TabIndex="103" />&nbsp;
                                                                    <strong>Disapproved – DO NOT USE</strong>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtGroupName2" runat="server" CssClass="txtbox" TabIndex="104"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtRiskDate3" runat="server" MaxLength="10" CssClass="txtboxmedium"
                                                                        TabIndex="105"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgRiskDate3" ImageUrl="~/Images/cal.gif" ImageAlign="AbsMiddle"
                                                                        runat="server" />
                                                                    <Ajax:CalendarExtender ID="caleRiskDate3" TargetControlID="txtRiskDate3" PopupButtonID="imgRiskDate3"
                                                                        Format="MM/dd/yyyy" runat="server">
                                                                    </Ajax:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextleft">
                                                                    <%--<ul>
                                                                        <li>Forward copy of this form and VQF to the Vendor Database Coordinator.</li>
                                                                    </ul>--%>
                                                                </td>
                                                                <td class="bodytextleft" valign="top">
                                                                    DOC Name / Signature
                                                                </td>
                                                                <td class="bodytextleft" valign="top">
                                                                    &nbsp;Date
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" class="bodytextbold">
                                                                    Comments
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:TextBox ID="txtRiskcomments" runat="server" onFocus="javascript:textCounter(this,1500,'yes');"
                                                                        onKeyDown="javascript:textCounter(this,1500,'yes');" onKeyUp="javascript:textCounter(this,1500,'yes');"
                                                                        Height="125px" TextMode="MultiLine" TabIndex="92" Width="99%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <%--G. Vera 10/24/2012 - Added--%>
                                                        <table cellpadding="3" cellspacing="1" class="searchtableclass" width="100%">
                                                            <tr>
                                                                <td class="Header">
                                                                    Enrollment (DOC to select one of the following)
                                                                </td>

                                                            </tr>
                                                            <%--G. Vera 06/25/2020 - Added row--%>
                                                            <%--<tr>
                                                                <td colspan="3" class="bodytextbold"><b>  <span class="mandatorystar">* </span>Policy change effective April 1, 2020, some selections below are no longer available and have been disabled.</td>
                                                            </tr>--%>
                                                            
                                                            <tr>
                                                                <td>
                                                                    <div style="margin-top: 10px; padding-left: 5px;">
                                                                        <div>
                                                                            <div class="bodytextleft">
                                                                                    <asp:CheckBoxList ID="chklstEnrollment" runat="server" Enabled="true" />
                                                                                    <asp:TextBox runat="server" ID="txtEnrollmentOther" CssClass="txtboxarea"  Width="100%" TextMode="MultiLine" Wrap="true" MaxLength="500" hidden="hidden"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <table cellpadding="3" cellspacing="1" class="searchtableclass" width="100%">
                                                            <tr>
                                                                <td class="Header">
                                                                    Required Risk Management Controls of Subcontractors
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="100%">
                                                                    <div style="margin-top: 10px; padding-left: 5px;">
                                                                        <div>
                                                                            <strong style="font-family: Tahoma; font-size: 12px;">A.&nbsp;&nbsp;&nbsp;&nbsp;Average
                                                                                Risk Management</strong>
                                                                            <div class="bodytextleft">
                                                                                <ul>
                                                                                    <asp:CheckBoxList ID="chkListAverageRisk" runat="server" Enabled="true" /> 
                                                                                    
                                                                                </ul>
                                                                                <ul style="list-style-type: none">
                                                                                    <li>
                                                                                        <asp:TextBox runat="server" ID="txtAverageRiskControlsOther" CssClass="txtboxarea"  Width="100%" TextMode="MultiLine" Wrap="true" MaxLength="500" hidden="hidden" />

                                                                                    </li>

                                                                                </ul>
                                                                                <%--G. Vera 09/21/2012: Modification (JIRA:VPI-69, VPI-71, & VPI-70)--%>
                                                                      </div>
                                                                        </div>
                                                                        <div>
                                                                            <strong style="font-family: Tahoma; font-size: 12px;">B.&nbsp;&nbsp;&nbsp;&nbsp;High
                                                                                Risk Management&nbsp;&nbsp;&nbsp;(Requires approval of the DOC)</strong>
                                                                            <div class="bodytextleft">
                                                                                <ul>
                                                                                    <asp:CheckBoxList ID="chkListHighRisk" runat="server" Enabled="true" /> 
                                                                                    
                                                                                </ul>
                                                                                <ul style="list-style-type: none"><li><asp:TextBox runat="server" ID="txtHighRiskControlsOther" CssClass="txtboxarea"  Width="100%" TextMode="MultiLine" Wrap="true" MaxLength="500" hidden="hidden"/></li></ul>
                                                                                <%--G. Vera 09/21/2012: Modification (JIRA:VPI-69, VPI-71, & VPI-70)--%>
                                                                           </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight" style="font-weight: bold;">
                                                                    Comments:
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                    <asp:TextBox ID="txtRiskMgmntComments" runat="server" Height="100px" Width="99%"
                                                                        MaxLength="1500" TextMode="MultiLine" CssClass="txtboxmultilarge" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Content>
                                                </Ajax:AccordionPane>
                                            </Panes>
                                        </Ajax:Accordion>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdheight">
                                    </td>
                                </tr>
                                <tr id="trEmployeeAction" runat="server" style="display: none;">
                                    <td align="left">
                                        &nbsp;&nbsp;&nbsp;<asp:ImageButton 
                                            ID="imbSave" ToolTip="Save and Close SRE" ImageUrl="~/Images/save_close.jpg"
                                            ImageAlign="AbsMiddle" runat="server" Visible="false" 
                                            OnClick="imbSave_Click" OnClientClick="javascript:validateInsurance();bondingSection();validateFinance();validateReference();"
                                            TabIndex="107" Style="height: 21px" />  <%--G. Vera 06/21/2012 Added client click property--%>
                                        <asp:ImageButton ID="imbSubmit" ToolTip="Submit SRE" ImageUrl="~/Images/submit.jpg"
                                            ImageAlign="AbsMiddle" runat="server" Visible="false" OnClick="imbSubmit_Click"
                                            TabIndex="108" OnClientClick="javascript:validateInsurance();bondingSection();"/>   <%--G. Vera 06/25/2012 Added client click property--%>
                                        <asp:ImageButton ID="imbComplete" ToolTip="Complete SRE" ImageUrl="~/Images/complete.jpg"
                                            ImageAlign="AbsMiddle" runat="server" Visible="false" OnClick="imbComplete_Click"
                                            TabIndex="109" OnClientClick="javascript:validateInsurance();bondingSection();validateFinance();validateReference();"/>   <%--G. Vera 06/25/2012 Added client click property--%>
                                        <asp:ImageButton ID="imbUpdate" ToolTip="Update SRE" ImageUrl="~/Images/update.jpg"
                                            ImageAlign="AbsMiddle" runat="server" Visible="false" OnClick="imbUpdate_Click"
                                            TabIndex="110" />
                                        <asp:ImageButton ID="imbAwarded" ImageUrl="~/Images/award.jpg" ImageAlign="AbsMiddle"
                                            runat="server" OnClick="imbAwarded_Click" ToolTip="Awarded" TabIndex="111" />
                                        <asp:ImageButton ID="imbNotAwarded" ImageUrl="~/Images/notaward.jpg" ImageAlign="AbsMiddle"
                                            runat="server" OnClick="imbNotAwarded_Click" ToolTip="Not award" TabIndex="112" />
                                        <%--G. Vera - 10/01/2012 - Moved the Delete SRE button to admin menu--%>
                                        <%--G. Vera 01/13/2016:  If Admin has a role of Admin_SRE, then should be able to delete and unlock an SRE as well--%>
                                        <asp:ImageButton
                                            ID="imbUnlockAdminSRE" runat="server" ImageUrl="~/Images/unlock_sre.jpg"
                                            Visible="false" OnClientClick="javascript:return showAlert()" ToolTip="Unlock SRE"
                                            ImageAlign="AbsMiddle" TabIndex="117"/>
                                        <asp:ImageButton ID="imbDeleteAdminSRE" runat="server" ImageUrl="~/Images/delete.jpg"
                                            ImageAlign="AbsMiddle" OnClientClick="javascript:return getSREConfirmation()"
                                            ToolTip="Delete SRE" TabIndex="113" OnClick="imbDeleteSRE_Click" Visible="false"/>
                                        <asp:ImageButton ID="imbPrintSRE" runat="server" ImageUrl="~/Images/print.jpg" ImageAlign="AbsMiddle"
                                            ToolTip="Print SRE" OnClick="imbPrintSRE_Click" TabIndex="114" Visible="false"/>
                                        <asp:ImageButton ID="imgPDF" runat="server" ImageUrl="~/Images/ex2pdf.jpg" ImageAlign="AbsMiddle"
                                            ToolTip="Export PDF" OnClick="imgPDF_Click" 
                                            OnClientClick="javascript:validateInsurance();bondingSection();validateFinance();validateReference();" />     <%--G. Vera 06/25/2012 Added client click property--%>
                                    </td>
                                    <td align="right">
                                        <asp:ImageButton ID="imbUpload1" runat="server" ImageUrl="~/Images/attach_documentnew.jpg"
                                            ImageAlign="AbsMiddle" OnClientClick="return showAttachment('1')" OnClick="imbUpload_Click"
                                            TabIndex="115" />
                                            <%--G. Vera 06/25/2012 Added client click property--%>
                                        <asp:ImageButton ID="imbCalculate" ImageUrl="~/Images/recal.jpg" ImageAlign="AbsMiddle"
                                            runat="server" Visible="false" OnClick="imbCalculate_Click" ToolTip="Recalculate SRE"
                                            TabIndex="116" 
                                            OnClientClick="javascript:validateInsurance();bondingSection();validateFinance();validateReference();" />&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr id="trAdminAction" runat="server" style="display: none;">
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;<asp:ImageButton 
                                            ID="imbUnlockSRE" runat="server" ImageUrl="~/Images/unlock_sre.jpg"
                                            Visible="false" OnClientClick="javascript:return showAlert()" ToolTip="Unlock SRE"
                                            ImageAlign="AbsMiddle" TabIndex="117" />
                                        <asp:ImageButton ID="imbDeleteSRE" runat="server" ImageUrl="~/Images/delete.jpg"
                                            ImageAlign="AbsMiddle" OnClientClick="javascript:return getSREConfirmation()"
                                            ToolTip="Delete SRE" TabIndex="113" OnClick="imbDeleteSRE_Click" />
                                        <asp:ImageButton ID="imbUpload2" runat="server" ImageUrl="~/Images/attach_documentnew.jpg"
                                            ImageAlign="AbsMiddle" OnClientClick="return showAttachment('2')" OnClick="imbUpload_Click"
                                            TabIndex="118" ToolTip="Attach Document" />
                                        <asp:ImageButton ID="imbPrintSRE1" runat="server" ImageUrl="~/Images/print.jpg" ImageAlign="AbsMiddle"
                                            ToolTip="Print SRE" OnClick="imbPrintSRE1_Click" TabIndex="119" OnClientClick="javascript:validateInsurance();bondingSection();" Visible="false"/> <%--G. Vera 06/25/2012 Added client click property--%>
                                        <asp:ImageButton ID="imbPDF1" runat="server" ImageUrl="~/Images/ex2pdf.jpg" ImageAlign="AbsMiddle"
                                            ToolTip="Export PDF" OnClick="imgPDF_Click" 
                                            OnClientClick="javascript:validateInsurance();bondingSection();validateFinance();validateReference();" />     <%--G. Vera 06/25/2012 Added client click property--%>
                                        <asp:HiddenField ID="hdnrisk" runat="server" />
                                        <asp:HiddenField ID="hdnFinancial1" runat="server" />
                                        <asp:HiddenField ID="hdnFinancial3" runat="server" />
                                        <asp:HiddenField ID="hdnFinancial4" runat="server" />
                                        <asp:HiddenField ID="hdnInsurance" runat="server" />
                                        <asp:HiddenField ID="hdnFinancialRiskIndicator" runat="server" />
                                        <asp:HiddenField ID="hdnInsuranceRiskIndicator" runat="server" />
                                        <asp:HiddenField ID="hdnReferenceRiskIndicator" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <Haskell:Footer ID="BottomMenu" runat="server" />
            </td>
        </tr>
    </table>
    <Ajax:ModalPopupExtender ID="mpopSaveAlert" runat="server" BackgroundCssClass="popupbg"
        PopupControlID="divAlert" TargetControlID="hdnAlert">
    </Ajax:ModalPopupExtender>
    <asp:HiddenField ID="hdnAlert" runat="server"  />
    <div id="divAlert" runat="server" class="popupdiv" style="display: none;">
        <table cellpadding="3" cellspacing="0" width="100%">
            <tr>
                <td class="searchhdrbarbold">
                    Alert
                </td>
            </tr>
            <tr>
                <td class="tdheight popupdivcl">
                </td>
            </tr>
            <tr>
                <td class="popupdivcl">
                    Delete existing snapshot of VQF?
                </td>
            </tr>
            <tr>
                <td class="tdheight popupdivcl">
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" align="center">
                    <asp:ImageButton ID="imbYes" ImageUrl="~/Images/yes.jpg" ImageAlign="AbsMiddle" runat="server"
                        OnClick="imbYes_Click" />&nbsp;&nbsp;
                    <asp:ImageButton ID="imbVQFCancel" runat="server" OnClick="imbVQFCancel_Click" ImageAlign="AbsMiddle"
                        ImageUrl="~/Images/no.jpg" />
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td class="tdheight popupdivcl">
                </td>
            </tr>
        </table>
    </div>
    <Ajax:ModalPopupExtender ID="mpopSaveAlertSRE" runat="server" BackgroundCssClass="popupbg"
        PopupControlID="divAlertSRE" TargetControlID="hdnAlert1">
    </Ajax:ModalPopupExtender>
    <asp:HiddenField ID="hdnAlertSRE" runat="server" />
    <div id="divAlertSRE" runat="server" class="popupdiv" style="display: none;">
        <table cellpadding="3" cellspacing="0" width="100%">
            <tr>
                <td class="searchhdrbarbold">
                    Alert
                </td>
            </tr>
            <tr>
                <td class="tdheight popupdivcl">
                </td>
            </tr>
            <tr>
                <td class="popupdivcl">
                    Delete existing snapshot of SRE?
                </td>
            </tr>
            <tr>
                <td class="tdheight popupdivcl">
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" align="center">
                    <asp:ImageButton ID="imbYesSRE" ImageUrl="~/Images/yes.jpg" ImageAlign="AbsMiddle"
                        runat="server" OnClick="imbYesSRE_Click" />&nbsp;&nbsp;
                    <asp:ImageButton ID="imbVQFCancelSRE" runat="server" OnClick="imbVQFCancelSRE_Click"
                        ImageAlign="AbsMiddle" ImageUrl="~/Images/no.jpg" />
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td class="tdheight popupdivcl">
                </td>
            </tr>
        </table>
    </div>
    <Ajax:ModalPopupExtender ID="mpopAddDocument" runat="server" TargetControlID="hdnAddDoc"
        PopupControlID="divAddDoc" BackgroundCssClass="popupbg">
    </Ajax:ModalPopupExtender>
    <Ajax:ModalPopupExtender ID="mpopAlert" runat="server" TargetControlID="hdnAlert1"
        PopupControlID="divAlert1" BackgroundCssClass="popupbg">
    </Ajax:ModalPopupExtender>
    <Ajax:ModalPopupExtender ID="mpopAlert1" runat="server" TargetControlID="hdnAlert1"
        PopupControlID="divAlert2" BackgroundCssClass="popupbg">
    </Ajax:ModalPopupExtender>
    <asp:HiddenField ID="hdnAlert1" runat="server" />
    <asp:HiddenField ID="hdnAddDoc" runat="server" />
    <asp:HiddenField ID="hdnGroupRisk" runat="server" />
    <div id="divAddDoc" class="popupdivbg1" style="display: none;">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                        <tr>
                            <td class="Messageheading" runat="server" id="Td1">
                                Upload Document
                            </td>
                        </tr>
                        <tr id="trAttachDocument" runat="server">
                            <td style="padding: 0px 5px">
                                <table width="100%">
                                    <tr>
                                        <td class="popupdivcl" align="left" colspan="2">
                                            <asp:Label ID="lblattach" runat="server" CssClass="errormsg"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt" colspan="2" style="padding: 10px 7px; border: 1px solid #000">
                                            <b>NOTE:</b><br />
                                            <br />
                                            Each file should be less than or equal to 8 MB in size.<br />
                                            Please upload files only in the format gif, jpg, jpeg, xls, xlsx, txt, pdf, doc,
                                            docx
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bodytextcenter">
                                            <b>Description</b>
                                        </td>
                                        <td class="formtdrt">
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>Upload Path</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt">
                                            <asp:TextBox ID="txtFileName1" runat="server" CssClass="txtbox" MaxLength="200" TabIndex="0"></asp:TextBox>
                                            <%--G. Vera 10/04/2012 - Not needed--%>
                                            <%--<Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtFileName1"
                                                FilterType="Numbers,UppercaseLetters,LowercaseLetters,Custom" ValidChars=","
                                                runat="server">
                                            </Ajax:FilteredTextBoxExtender>--%>
                                        </td>
                                        <td class="formtdrt">
                                            <asp:FileUpload ID="fluUpload1" runat="server" CssClass="fileUpload" TabIndex="0" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt">
                                            <asp:TextBox ID="txtFileName2" runat="server" CssClass="txtbox" MaxLength="200" TabIndex="0"></asp:TextBox>
                                            <%--G. Vera 10/04/2012 - Not needed--%>
                                            <%--<Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtFileName2"
                                                FilterType="Numbers,UppercaseLetters,LowercaseLetters,Custom" ValidChars=","
                                                runat="server">
                                            </Ajax:FilteredTextBoxExtender>--%>
                                        </td>
                                        <td class="formtdrt">
                                            <asp:FileUpload ID="fluUpload2" runat="server" CssClass="fileUpload" TabIndex="0" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt">
                                            <asp:TextBox ID="txtFileName3" runat="server" CssClass="txtbox" MaxLength="200" TabIndex="0"></asp:TextBox>
                                            <%--G. Vera 10/04/2012 - Not needed--%>
                                            <%--<Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="txtFileName3"
                                                FilterType="Numbers,UppercaseLetters,LowercaseLetters,Custom" ValidChars=","
                                                runat="server">
                                            </Ajax:FilteredTextBoxExtender>--%>
                                        </td>
                                        <td class="formtdrt">
                                            <asp:FileUpload ID="fluUpload3" runat="server" CssClass="fileUpload" TabIndex="0" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt">
                                            <asp:TextBox ID="txtFileName4" runat="server" CssClass="txtbox" MaxLength="200" TabIndex="0"></asp:TextBox>
                                            <%--G. Vera 10/04/2012 - Not needed--%>
                                            <%--<Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="txtFileName4"
                                                FilterType="Numbers,UppercaseLetters,LowercaseLetters,Custom" ValidChars=","
                                                runat="server">
                                            </Ajax:FilteredTextBoxExtender>--%>
                                        </td>
                                        <td class="formtdrt">
                                            <asp:FileUpload ID="fluUpload4" runat="server" CssClass="fileUpload" TabIndex="0" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt">
                                            <asp:TextBox ID="txtFileName5" runat="server" CssClass="txtbox" MaxLength="200" TabIndex="0"></asp:TextBox>
                                            <%--G. Vera 10/04/2012 - Not needed--%>
                                            <%--<Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" TargetControlID="txtFileName5"
                                                FilterType="Numbers,UppercaseLetters,LowercaseLetters,Custom" ValidChars=","
                                                runat="server">
                                            </Ajax:FilteredTextBoxExtender>--%>
                                        </td>
                                        <td class="formtdrt">
                                            <asp:FileUpload ID="fluUpload5" runat="server" CssClass="fileUpload" TabIndex="0" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ModalPopupTd" colspan="2">
                                            <asp:ImageButton ID="imbAddDoc" runat="server" ImageUrl="~/Images/upload.jpg" ToolTip="Upload"
                                                TabIndex="0" CausesValidation="false" OnClientClick="javascript:return validateDoc()"
                                                OnClick="imbAddDoc_Click" Height="21px" />
                                            &nbsp;
                                            <asp:ImageButton ID="imbAttachClose" runat="server" ImageUrl="~/Images/close.jpg"
                                                ToolTip="Close" CausesValidation="false" TabIndex="0" OnClientClick="javascript:return closePopup();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="DocumentTd" runat="server">
                            <td>
                                <asp:Panel ID="PanAttach" runat="server">
                                    <table cellpadding="3" cellspacing="1" align="center" width="99%" border="0">
                                        <tr>
                                            <td class="Messageheading" runat="server" id="tdattach">
                                                Attached Document files
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formhead2">
                                                Attached by PM
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="left_border" align="center">
                                                <asp:Repeater ID="rptDocument" runat="server" OnItemCommand="rptDocument_ItemCommand">
                                                    <HeaderTemplate>
                                                        <table cellpadding="5" cellspacing="1" width="100%">
                                                            <tr class="listheading">
                                                                <td style="width: 3%;">
                                                                    #
                                                                </td>
                                                                <td style="width: 80%;">
                                                                    File Name
                                                                </td>
                                                                <td style="width: 17%;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr class="listone">
                                                            <td>
                                                                <%#Container.ItemIndex + 1%>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="lnkFileName" runat="server" CommandName="View" CommandArgument='<%#bind("FilePath")%>'
                                                                    Text='<%#bind("FileName")%>'></asp:LinkButton>
                                                            </td>
                                                            <td align="center">
                                                                <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                                    ImageAlign="AbsMiddle" CommandName="Delete" OnClientClick="javascript:return getConfirmation()"
                                                                    CommandArgument='<%#Bind("PK_DocumentId")%>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <tr class="listtwo">
                                                            <td>
                                                                <%#Container.ItemIndex + 1%>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="lnkFileName" runat="server" CommandName="View" CommandArgument='<%#Eval("FilePath")%>'
                                                                    Text='<%#Eval("FileName")%>'></asp:LinkButton>
                                                            </td>
                                                            <td align="center">
                                                                <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                                    ImageAlign="AbsMiddle" CommandName="Delete" OnClientClick="javascript:return getConfirmation()"
                                                                    CommandArgument='<%#Eval("PK_DocumentId") + "^" + Eval("FilePath")%>' />
                                                            </td>
                                                        </tr>
                                                    </AlternatingItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                        <asp:Label ID="lblEmptyData" Text="There are no Documents to Display" runat="server"
                                                            Visible="false">
                                                        </asp:Label>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="left_border" align="center">
                                                <asp:Label ID="rptPM" runat="server" Text="" CssClass="errormsg"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formhead2">
                                                Attached by Admin
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="left_border" align="center">
                                                <asp:Repeater ID="rptDocumentAdmin" runat="server" OnItemCommand="rptDocumentAdmin_ItemCommand">
                                                    <HeaderTemplate>
                                                        <table cellpadding="5" cellspacing="1" width="100%">
                                                            <tr class="listheading">
                                                                <td style="width: 3%;">
                                                                    #
                                                                </td>
                                                                <td style="width: 80%;">
                                                                    File Name
                                                                </td>
                                                                <td style="width: 17%;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr class="listone">
                                                            <td>
                                                                <%#Container.ItemIndex + 1%>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="lnkFileName1" runat="server" CommandName="View" CommandArgument='<%#Eval("FilePath")%>'
                                                                    Text='<%#Eval("FileName")%>'></asp:LinkButton>
                                                            </td>
                                                            <td align="center">
                                                                <asp:ImageButton ID="imbDelete1" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                                    ImageAlign="AbsMiddle" CommandName="Delete" OnClientClick="javascript:return getConfirmation()"
                                                                    CommandArgument='<%#Eval("PK_DocumentId")%>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <tr class="listtwo">
                                                            <td>
                                                                <%#Container.ItemIndex + 1%>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="lnkFileName1" runat="server" CommandName="View" CommandArgument='<%#Eval("FilePath")%>'
                                                                    Text='<%#Eval("FileName")%>'></asp:LinkButton>
                                                            </td>
                                                            <td align="center">
                                                                <asp:ImageButton ID="imbDelete1" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                                    ImageAlign="AbsMiddle" CommandName="Delete" OnClientClick="javascript:return getConfirmation()"
                                                                    CommandArgument='<%#Eval("PK_DocumentId") + "^" + Eval("FilePath")%>' />
                                                            </td>
                                                        </tr>
                                                    </AlternatingItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                        <asp:Label ID="lblEmptyData" Text="There are no Documents to Display" runat="server"
                                                            Visible="false">
                                                        </asp:Label>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="left_border" align="center">
                                                <asp:Label ID="rptadmin" runat="server" Text="" CssClass="errormsg"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr class="ModalPopupTd" colspan="2">
                            <td>
                                <asp:ImageButton ID="imbAward" runat="server" ImageUrl="~/Images/close.jpg" ToolTip="Close"
                                    CausesValidation="false" TabIndex="0" OnClientClick="javascript:return closePopup();"
                                    Visible="false" />
                            </td>
                        </tr>
                        <tr class="ModalPopupTd" colspan="2">
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="hdnAttachmentType" runat="server" />
    <asp:HiddenField ID="hdnEmpName" runat="server" />
    <asp:HiddenField ID="hdnDate" runat="server" />
    <%--G. Vera 09/21/2012 - Added hidden fields below--%>
    <asp:HiddenField ID="hdnOriginator" runat="server" />
    <asp:HiddenField ID="hdnIsOriginator" runat="server" />
    <div id="divAlert1" class="popupdivbg1" style="display: none;">
        <table cellpadding="3" cellspacing="1" width="100%">
            <tr>
                <td class="Messageheading">
                    Message
                </td>
            </tr>
            <tr>
                <td class="popupdivcl tdHeight">
                </td>
            </tr>
            <tr>
                <td id="align1" runat="server" class="popupdivcl" style="padding-right: 0px;">
                    <%--     <%#Container.ItemIndex + 1%>--%>
                    <br />
                    <asp:Label ID="lblMessage" CssClass="errormsg" runat="server">1</asp:Label>
                    <br />
                    <%--  <div style="max-height: 500px; overflow: auto; width: 100%;" >--%>
                </td>
            </tr>
            <tr>
                <td class="popupdivcl tdHeight">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" align="center">
                    <asp:ImageButton ID="imbOK" runat="server" ImageUrl="~/Images/ok.jpg" ImageAlign="AbsMiddle"
                        Visible="false" ToolTip="OK" />
                    <%--OnClientClick="javascript:window.location.href = 'ViewSRE.aspx';return false;" />--%>
                    <asp:ImageButton ID="imbOK1" runat="server" ImageUrl="~/Images/ok.jpg" ImageAlign="AbsMiddle"
                        Visible="false" ToolTip="OK" OnClientClick="javascript:$find('mpopAlert').hide();return false;" />
                    <asp:ImageButton ID="imbDeleteOk" runat="server" ImageUrl="~/Images/ok.jpg" ImageAlign="AbsMiddle"
                        Visible="false" ToolTip="OK" Height="21px" OnClick="imbDeleteOk_Click" />
                    <asp:ImageButton ID="imbNotify" runat="server" ImageUrl="~/Images/pmfinal.jpg" ImageAlign="AbsMiddle"
                        Visible="false" ToolTip="PM Mail Notification" OnClick="imbNotify_Click" />
                    <asp:ImageButton ID="imb" runat="server" ImageUrl="~/Images/cancel.jpg" ImageAlign="AbsMiddle"
                        Visible="false" ToolTip="Cancel" Height="21px" OnClick="imb_Click" />
                        <!-- Modified by G. Vera 04/13/2012 - Changing the image to Close instead-->
                    <asp:ImageButton ID="imbOKSave" runat="server" ImageUrl="~/Images/close.jpg" ImageAlign="AbsMiddle"
                        Visible="false" ToolTip="OK" OnClick="imbOKSave_Click" />
                    <!-- Modified by G. Vera 04/13/2012 - Testing new button "Continue"-->
                    <asp:ImageButton ID="imbContSave" runat="server" ImageUrl="~/Images/ok.jpg" ImageAlign="AbsMiddle"
                        Visible="false" ToolTip="OK" OnClick="imbContSave_Click" />
                </td>
            </tr>
            <tr class="popupdivcl tdHeight">
                <td>
                </td>
            </tr>
        </table>
    </div>
    <div id="divAlert2" class="popupdivsmall1" style="display: none;">
        <table cellpadding="3" cellspacing="1" width="100%">
            <tr>
                <td class="Messageheading" runat="server">
                    Message
                </td>
            </tr>
            <tr>
                <td class="tdHeight">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" style="padding-right: 0px;" align="center">
                    <%-- </div>--%>
                    <asp:Label ID="lblCompareMessage" CssClass="errormsg" runat="server">2</asp:Label>
                    <%--  <%#Container.ItemIndex + 1%>--%>
                </td>
            </tr>
            <tr>
                <td class="popupdivcl tdHeight">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" align="center">
                    <asp:ImageButton ID="imbOK2" runat="server" ImageUrl="~/Images/ok.jpg" ImageAlign="AbsMiddle"
                        ToolTip="OK" OnClick="imbOK2_Click" OnClientClick="javascript:validateInsurance();bondingSection();"/>  <%--G. Vera 06/25/2012 - Added on client click event--%>
                    <asp:ImageButton ID="imbCancel2" runat="server" ImageUrl="~/Images/cancel.jpg" ImageAlign="AbsMiddle"
                        ToolTip="Cancel" OnClick="imbCancel2_Click" />
                    <asp:ImageButton ID="imbCompletedOK" runat="server" ImageUrl="~/Images/ok.jpg" ImageAlign="AbsMiddle"
                        ToolTip="ok" Visible="false" OnClick="imbCompletedOK_Click" />
                </td>
            </tr>
            <tr class="popupdivcl tdHeight">
                <td>
                </td>
            </tr>
        </table>
    </div>
    <Ajax:ModalPopupExtender ID="mpopAlertAward" runat="server" TargetControlID="hdnAlert1"
        CancelControlID="imbAwardCancel" PopupControlID="divAlertAvard" BackgroundCssClass="popupbg">
    </Ajax:ModalPopupExtender>
    <div id="divAlertAvard" class="popupdivsmall" style="display: none;">
        <table cellpadding="3" cellspacing="1" width="100%">
            <tr>
                <td id="Td2" class="Messageheading" runat="server" colspan="2">
                    Message
                </td>
            </tr>
            <tr>
                <td class="tdheight" colspan="2">
                </td>
            </tr>
            <tr>
                <td class="bodytextcenter" style="padding-right: 0px;" colspan="2">
                    <asp:Label ID="lblAwardMessage" CssClass="errormsg" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="trContract" runat="server">
                <td class="bodytextright" width="40%" valign="top">
                    <span class="errormsg">*</span> Contract Number
                </td>
                <td class="bodytextleft">
                    <asp:TextBox ID="txtAlertAward" runat="server" CssClass="txtbox" MaxLength="15"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftxtContractNumber" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                        InvalidChars="" TargetControlID="txtAlertAward" ValidChars="-">
                    </Ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr class="popupdivcl tdHeight">
                <td>
                </td>
                <td class="bodytextleft">
                    <asp:ImageButton ID="imbSubmitAward" runat="server" ImageUrl="~/Images/submit.jpg"
                        ImageAlign="AbsMiddle" ToolTip="Submit" OnClick="imbSubmitAward_Click" />
                    <asp:ImageButton ID="imbAwardCancel" runat="server" ImageUrl="~/Images/cancel.jpg"
                        ImageAlign="AbsMiddle" ToolTip="Cancel" />
                </td>
            </tr>
            <tr>
                <td class="popupdivcl">
                    &nbsp;&nbsp;
                </td>
            </tr>
        </table>
    </div>
    <Ajax:ModalPopupExtender ID="mpopAlertNotAward" runat="server" TargetControlID="hdnAlert1"
        CancelControlID="imbNotAwardCancel" PopupControlID="divAlertNotAvard" BackgroundCssClass="popupbg">
    </Ajax:ModalPopupExtender>
    <div id="divAlertNotAvard" class="popupdivsmall" style="display: none;">
        <table cellpadding="3" cellspacing="1" width="350px">
            <tr>
                <td id="Td3" class="Messageheading" runat="server" colspan="2">
                    Message
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" style="padding-right: 0px;" colspan="2">
                    <asp:Label ID="lblNotAwardMessage" CssClass="errormsg" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="trReason" runat="server">
                <td class="bodytextright" width="20%" valign="top">
                    <span class="errormsg">*</span> Reason
                </td>
                <td class="bodytextleft">
                    <asp:TextBox ID="txtAlertNotAward" runat="server" CssClass="txtboxlarge1" TextMode="MultiLine"
                        onFocus="javascript:textCounter(this,500,'yes');" onKeyDown="javascript:textCounter(this,500,'yes');"
                        onKeyUp="javascript:textCounter(this,500,'yes');"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr class="popupdivcl tdHeight">
                <td>
                </td>
                <td>
                    <asp:ImageButton ID="imbSubmitNotAward" runat="server" ImageUrl="~/Images/submit.jpg"
                        ImageAlign="AbsMiddle" ToolTip="Submit" OnClick="imbSubmitNotAward_Click" />
                    <asp:ImageButton ID="imbNotAwardCancel" runat="server" ImageUrl="~/Images/cancel.jpg"
                        ImageAlign="AbsMiddle" ToolTip="Cancel" />
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" colspan="2">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <Ajax:ModalPopupExtender ID="mpopEMRAlert" runat="server" TargetControlID="hdnAlert1"
        PopupControlID="divEMRAlert" BackgroundCssClass="popupbg">
    </Ajax:ModalPopupExtender>
    <div id="divEMRAlert" class="popupdivsmall" style="display: none;" align="center">
        <table cellpadding="3" cellspacing="1" width="100%" align="center">
            <tr>
                <td id="Td4" class="Messageheading" runat="server" colspan="2">
                    EMR Rates
                </td>
            </tr>
            <tr>
                <td class="popupdivcl tdHeight" colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="searchtableclass" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="sectionpad">
                                <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                    <tr id="trMessage" runat="server" style="display: none;">
                                        <td colspan="5">
                                            <asp:Label ID="lblemrmsg" runat="server" CssClass="summaryerrormsg" Style="display: none;"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bodytextleft" valign="top" width="8%">
                                            <span class="mandatorystar">*</span>Year
                                        </td>
                                        <td class="formtdrt" valign="top" width="12%">
                                            <asp:TextBox CssClass="txtboxsmall" ID="txtYearOne" ReadOnly="true" runat="server"
                                                MaxLength="4"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftxtYearOne" runat="server" TargetControlID="txtYearOne"
                                                FilterType="Numbers">
                                            </Ajax:FilteredTextBoxExtender>
                                        </td>
                                        <td class="bodytextleft" valign="top" width="8%">
                                            <span class="mandatorystar">*</span>Rate
                                        </td>
                                        <td class="bodytextleft" valign="top" width="22%">
                                            <asp:TextBox CssClass="txtboxmedium1" ID="txtRateOne" TabIndex="0" runat="server"
                                                ReadOnly="true" MaxLength="4"></asp:TextBox>&nbsp;&nbsp;<Ajax:FilteredTextBoxExtender
                                                    ID="ftxtRateOne" runat="server" TargetControlID="txtRateOne" FilterMode="ValidChars"
                                                    FilterType="Custom" ValidChars="0123456789.">
                                                </Ajax:FilteredTextBoxExtender>
                                            <asp:CustomValidator ID="cusvYearOne" runat="server" Display="None" ValidationGroup="Current"
                                                OnServerValidate="cusvYearOne_ServerValidate"></asp:CustomValidator>
                                            <%-- <asp:RangeValidator ID="rangRateOne" runat="server" ControlToValidate="txtRateOne"
                                                Type="Double" ErrorMessage="Please enter rate under year one range between 0 to 3 "
                                                Display="None" ValidationGroup="Current" MaximumValue="3" MinimumValue="0"></asp:RangeValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bodytextleft" valign="top">
                                            <span class="mandatorystar">*</span>Year
                                        </td>
                                        <td class="formtdrt" valign="top">
                                            <asp:TextBox CssClass="txtboxsmall" ID="txtYearTwo" ReadOnly="true" TabIndex="0"
                                                runat="server" MaxLength="4"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftxtYearTwo" runat="server" TargetControlID="txtYearTwo"
                                                FilterType="Numbers">
                                            </Ajax:FilteredTextBoxExtender>
                                        </td>
                                        <td class="bodytextleft" valign="top">
                                            <span class="mandatorystar">*</span>Rate
                                        </td>
                                        <td class="bodytextleft" valign="top" width="22%">
                                            <asp:TextBox CssClass="txtboxmedium1" ID="txtRateTwo" runat="server" TabIndex="0"
                                                ReadOnly="true" MaxLength="4"></asp:TextBox>&nbsp;&nbsp;
                                            <Ajax:FilteredTextBoxExtender ID="ftxtRateTwo" runat="server" TargetControlID="txtRateTwo"
                                                FilterMode="ValidChars" FilterType="Custom" ValidChars="0123456789.">
                                            </Ajax:FilteredTextBoxExtender>
                                            <asp:CustomValidator ID="cusvYearTwo" runat="server" Display="None" ValidationGroup="Current"
                                                OnServerValidate="cusvYearTwo_ServerValidate"></asp:CustomValidator>
                                            <%-- <asp:RangeValidator ID="rangRateTwo" runat="server" ControlToValidate="txtRateTwo"
                                                Type="Double" ErrorMessage="Please enter rate under year two range between 0 to 3"
                                                Display="None" ValidationGroup="Current" MaximumValue="3" MinimumValue="0"></asp:RangeValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bodytextleft" valign="top" valign="top">
                                            <span class="mandatorystar">*</span>Year
                                        </td>
                                        <td class="formtdrt" valign="top">
                                            <asp:TextBox CssClass="txtboxsmall" ID="txtYearThree" ReadOnly="true" TabIndex="0"
                                                runat="server" MaxLength="4"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftxtYearThree" runat="server" TargetControlID="txtYearThree"
                                                FilterType="Numbers">
                                            </Ajax:FilteredTextBoxExtender>
                                        </td>
                                        <td class="bodytextleft" valign="top">
                                            <span class="mandatorystar">*</span>Rate
                                        </td>
                                        <td class="bodytextleft" valign="top">
                                            <asp:TextBox CssClass="txtboxmedium1" ID="txtRateThree" runat="server" TabIndex="0"
                                                ReadOnly="true" MaxLength="4"></asp:TextBox>&nbsp;&nbsp;
                                            <Ajax:FilteredTextBoxExtender ID="ftxtRateThree" runat="server" TargetControlID="txtRateThree"
                                                FilterMode="ValidChars" FilterType="Custom" ValidChars="0123456789.">
                                            </Ajax:FilteredTextBoxExtender>
                                            <asp:CustomValidator ID="cusvYearThree" runat="server" Display="None" ValidationGroup="Current"
                                                OnServerValidate="cusvYearThree_ServerValidate"></asp:CustomValidator>
                                            <%-- <asp:RangeValidator ID="rangRateThree" runat="server" ControlToValidate="txtRateThree"
                                                Type="Double" ErrorMessage="Please enter rate under year three range between 0 to 3"
                                                Display="None" ValidationGroup="Current" MaximumValue="3" MinimumValue="0"></asp:RangeValidator>--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="popupdivcl bodytextcenter">
                <td>
                    <asp:ImageButton ID="imbEMRSave" runat="server" ImageUrl="~/Images/update.jpg" ImageAlign="AbsMiddle"
                        ToolTip="Update" OnClick="imbEMRSave_Click" />
                    <asp:ImageButton ID="imbcancel" runat="server" ImageUrl="~/Images/cancel.jpg" ImageAlign="AbsMiddle"
                        ToolTip="Cancel" OnClientClick="javascript:return emrclosePopup();" />
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" colspan="2">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <Ajax:ModalPopupExtender ID="mpopLegalAlert" runat="server" TargetControlID="hdnAlert1"
        PopupControlID="divLegalAlert" BackgroundCssClass="popupbg">
    </Ajax:ModalPopupExtender>
    <div id="divLegalAlert" class="popuplegdiv" style="display: none;">
        <table cellpadding="3" cellspacing="1" width="100%" align="center">
            <tr>
                <td id="Td5" class="Messageheading " runat="server" colspan="2">
                    Financial
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table border="0" cellpadding="5" cellspacing="0" width="100%">
                        <tr id="trLegalMessage" runat="server">
                            <td colspan="4" class="sectionpad">
                                <asp:Label ID="lblLegalmsg" CssClass="summaryerrormsg" runat="server" Style="display: none;"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="bodytextbold sectionpad">
                                Year One
                            </td>
                            <td class="bodytextleft">
                                <asp:TextBox CssClass="txtboxsmall" ID="txtLegalYearOne" TabIndex="0" runat="server"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                            <td class="bodytextleft">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="bodytextleft sectionpad" width="40%" nowrap="nowrap">
                                <span class="mandatorystar">*</span>Maximum Contract Value Completed $
                            </td>
                            <td class="bodytextleft">
                                <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="13" ID="txtMaxContractValueYearOne"
                                    ReadOnly="true" runat="server"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftxtMaxContract" runat="server" TargetControlID="txtMaxContractValueYearOne"
                                    FilterType="Numbers,Custom" ValidChars=",">
                                </Ajax:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="ReqvMax1" runat="server" ErrorMessage="Please enter maximum contract value completed under year one"
                                    ValidationGroup="Legal" Display="None" ControlToValidate="txtMaxContractValueYearOne"></asp:RequiredFieldValidator>
                            </td>
                            <td class="bodytextleft" width="40%">
                                &nbsp; <span class="mandatorystar">*</span>Annual Company Revenue $
                            </td>
                            <td class="bodytextleft">
                                <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="13" ID="txtAnuualCompRevenueYearOne"
                                    ReadOnly="true" runat="server"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftxtAnnualRevenue" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="," TargetControlID="txtAnuualCompRevenueYearOne">
                                </Ajax:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="ReqvAr1" runat="server" ErrorMessage="Please enter annual company revenue under year one"
                                    ValidationGroup="Legal" Display="None" ControlToValidate="txtAnuualCompRevenueYearOne"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tdheight">
                            </td>
                        </tr>
                        <tr>
                            <td class="bodytextbold sectionpad">
                                Year Two
                            </td>
                            <td class="bodytextleft">
                                <asp:TextBox CssClass="txtboxsmall" TabIndex="0" MaxLength="4" ID="txtLegalYearTwo"
                                    runat="server" ReadOnly="true"></asp:TextBox>&nbsp;&nbsp;
                            </td>
                            <td class="bodytextleft">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="bodytextleft sectionpad">
                                <span class="mandatorystar">*</span>Maximum Contract Value Completed $
                            </td>
                            <td class="bodytextleft">
                                <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="13" ID="txtMaxContractValueYearTwo"
                                    ReadOnly="true" runat="server"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftxtMaxContractvalue" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="," TargetControlID="txtMaxContractValueYearTwo">
                                </Ajax:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="ReqvMax2" runat="server" ErrorMessage="Please enter maximum contract value completed under year two"
                                    ValidationGroup="Legal" Display="None" ControlToValidate="txtMaxContractValueYearTwo"></asp:RequiredFieldValidator>
                            </td>
                            <td class="bodytextleft">
                                &nbsp; <span class="mandatorystar">*</span>Annual Company Revenue $
                            </td>
                            <td class="bodytextleft">
                                <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="13" ID="txtAnuualCompRevenueYearTwo"
                                    ReadOnly="true" runat="server"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftxtAnnualCompRevenue" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="," TargetControlID="txtAnuualCompRevenueYearTwo">
                                </Ajax:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="ReqvAr2" runat="server" ErrorMessage="Please enter annual company revenue under year two"
                                    ValidationGroup="Legal" Display="None" ControlToValidate="txtAnuualCompRevenueYearTwo"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tdheight">
                            </td>
                        </tr>
                        <tr>
                            <td class="bodytextbold sectionpad">
                                Year Three
                            </td>
                            <td class="bodytextleft">
                                <asp:TextBox CssClass="txtboxsmall" TabIndex="0" MaxLength="4" ID="txtLegalYearThree"
                                    runat="server" ReadOnly="true"></asp:TextBox>&nbsp;&nbsp;
                            </td>
                            <td class="bodytextleft">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="bodytextleft sectionpad">
                                <span class="mandatorystar">*</span>Maximum Contract Value Completed $
                            </td>
                            <td class="bodytextleft">
                                <asp:TextBox CssClass="txtboxmedium1" MaxLength="13" TabIndex="0" ID="txtMaxContractValueYearThree"
                                    ReadOnly="true" runat="server"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftxtMaxContractValue2" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="," TargetControlID="txtMaxContractValueYearThree">
                                </Ajax:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="ReqvMax3" runat="server" ErrorMessage="Please enter annual company revenue under year three"
                                    ValidationGroup="Legal" Display="None" ControlToValidate="txtMaxContractValueYearthree"></asp:RequiredFieldValidator>
                            </td>
                            <td class="bodytextleft">
                                &nbsp; <span class="mandatorystar">*</span>Annual Company Revenue $
                            </td>
                            <td class="bodytextleft">
                                <asp:TextBox CssClass="txtboxmedium1" MaxLength="13" TabIndex="0" ID="txtAnuualCompRevenueYearThree"
                                    ReadOnly="true" runat="server"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftxtAnnualRevenue2" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="," TargetControlID="txtAnuualCompRevenueYearThree">
                                </Ajax:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="ReqvAr3" runat="server" ErrorMessage="Please enter annual company revenue under year three"
                                    ValidationGroup="Legal" Display="None" ControlToValidate="txtAnuualCompRevenueYearThree"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td class="tdheight" colspan="4">
                            </td>
                        </tr>
                        <tr class="popupdivcl bodytextcenter">
                            <td colspan="4">
                                <asp:ImageButton ID="imbLegalSave" runat="server" ImageUrl="~/Images/update.jpg"
                                    ImageAlign="AbsMiddle" ToolTip="Update" OnClick="imbLegalSave_Click" />
                                <asp:ImageButton ID="imbLegalCancel" runat="server" ImageUrl="~/Images/cancel.jpg"
                                    ImageAlign="AbsMiddle" ToolTip="Cancel" OnClientClick="javascript:return legalclosePopup();" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="popupdivcl" colspan="2">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<script src="../Script/jquery.js" type="text/javascript"></script>

<script type="text/javascript">

    function ToggleDOCSignature(value) {
        var contractValueStr = removeCommas(value);//.replace(',', '');
        var contractValue = parseInt(contractValueStr);
        var docApprovalTreshold = "<%= Convert.ToInt64(ConfigurationManager.AppSettings["DocApprovalTreshold"]) %>"

        if (document.getElementById("<%= chkAverageRisk.ClientID%>").checked) {

            if (contractValue >= docApprovalTreshold) {
                
                $('#' + "<%=txtGroupName1.ClientID%>").removeAttr('disabled');
                $('#' + "<%=txtRiskDate2.ClientID%>").removeAttr('disabled');
                document.getElementById("<%=txtRiskDate2.ClientID%>").disabled = false;
            }
            else {
                $('#' + "<%=txtGroupName1.ClientID%>").attr('disabled', 'disabled');
                $('#' + "<%=txtRiskDate2.ClientID%>").attr('disabled', 'disabled');
                document.getElementById("<%=txtRiskDate2.ClientID%>").disabled = true;
            }
        }
    }

    //G. Vera 12/28/2016: Added
    function SetSafetyRiskIndicator() {

        var safetyCnt = 0;
        //debugger;
        if ($("#" + "<%= rdoSafety2Yes.ClientID %>").is(":checked"))
        {
            safetyCnt++;
        }
        if ($("#" + "<%= rdoSafety1Yes.ClientID%>").is(":checked"))
        {
            safetyCnt++;
        }
        if ($("#" + "<%= rdoSafetyCitationYes.ClientID%>").is(":checked"))
        {
            safetyCnt++;
        }

        if (safetyCnt >= 1) { $("#" + "<%=chkSafetyRiskIndicator.ClientID%>").attr("checked", "checked"); }
        else $("#" + "<%=chkSafetyRiskIndicator.ClientID%>").removeAttr('checked');
    }

    function closePopup() {
        $find("mpopAddDocument").hide();
        return false;
    }
    function emrclosePopup() {
        $find("mpopEMRAlert").hide();
        document.getElementById("<%=txtRateOne.ClientID%>").value = "";
        document.getElementById("<%=txtRateTwo.ClientID%>").value = "";
        document.getElementById("<%=txtRateThree.ClientID%>").value = "";
        return false;
    }

    function legalclosePopup() {
        $find("mpopLegalAlert").hide();
        document.getElementById("<%=lblLegalmsg.ClientID%>").value = "";

    }
    function currencyFormat(src) {
        var num = src.value.replace(",", "");
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));

        src.value = num;
    }
    function removeComma(src) {
        var str = src.value.split(",");
        src.value = str.join("");
    }
    function removeCommas(val) {
        var str = val.split(",");
        return str.join("");
    }
    function showAlert() {
        $find('<%=mpopSaveAlert.ClientID%>').show();
        document.getElementById("<%=lblMessage.ClientID%>").value = "Test";
        return false;
    }
    function financialSection1(LargestMaxPercentage, BidAannualRevenuePercentage) {
        //debugger; 
        var currentBid = removeCommas(document.getElementById("<%=txtSubcontractorCurrentBid.ClientID%>").value);

        var annualRevenue = removeCommas(document.getElementById("<%=txtAverageAnnualRevenue.ClientID%>").value);

        var maxContract = removeCommas(document.getElementById("<%=txtMaxContract3Year.ClientID%>").value);
        if (annualRevenue != "") {
            var perc = (BidAannualRevenuePercentage * parseFloat(annualRevenue)) / 100;

            // if (parseFloat(currentBid) > (parseFloat(annualRevenue) + perc)) {
            if (parseFloat(currentBid) > perc) {
                document.getElementById("<%=rdoFinancial3Yes.ClientID%>").checked = true;
                document.getElementById("<%=rdoFinancial3No.ClientID%>").checked = false;
                document.getElementById("<%=hdnFinancial3.ClientID%>").value = "1";

            }
            else {
                document.getElementById("<%=rdoFinancial3Yes.ClientID%>").checked = false;
                document.getElementById("<%=rdoFinancial3No.ClientID%>").checked = true;
                document.getElementById("<%=hdnFinancial3.ClientID%>").value = "0";

            }
        }
        else {
            document.getElementById("<%=rdoFinancial3Yes.ClientID%>").checked = false;
            document.getElementById("<%=rdoFinancial3No.ClientID%>").checked = true;
            document.getElementById("<%=hdnFinancial3.ClientID%>").value = "0";
        }
        if (maxContract != "") {

            // NEW CODE BY N SCHOENBERGER 07/27/2011
            var perc = 1 + (LargestMaxPercentage / 100);

            if (parseFloat(currentBid) > (parseFloat(maxContract) * perc)) {
                document.getElementById("<%=rdoFinancial1Yes.ClientID%>").checked = true;
                document.getElementById("<%=rdoFinancial1No.ClientID%>").checked = false;
                document.getElementById("<%=hdnFinancial1.ClientID%>").value = "1";

            }
            else {
                document.getElementById("<%=rdoFinancial1No.ClientID%>").checked = true;
                document.getElementById("<%=rdoFinancial1Yes.ClientID%>").checked = false;
                document.getElementById("<%=hdnFinancial1.ClientID%>").value = "0";
            }
        }
        else {
            document.getElementById("<%=rdoFinancial1No.ClientID%>").checked = true;
            document.getElementById("<%=rdoFinancial1Yes.ClientID%>").checked = false;
            document.getElementById("<%=hdnFinancial1.ClientID%>").value = "0";

        }


        validateFinance();
    }

    //Calculation provided by Haskell 
    function financialSection2(LowestbidPercentage) {
        //debugger; 
        var currentBid = removeCommas(document.getElementById("<%=txtSubcontractorCurrentBid.ClientID%>").value);
        var nextLowestBid = removeCommas(document.getElementById("<%=txtNextLowestBid.ClientID%>").value);
        //if (currentBid > nextLowestBid) {
        if (parseInt(currentBid) > parseInt(nextLowestBid)) {
            document.getElementById("<%=rdoFinancial4No.ClientID%>").checked = true;
            document.getElementById("<%=hdnFinancial4.ClientID%>").value = "0";
        }
        else {
            //var perc = ((nextLowestBid - currentBid) / parseFloat(currentBid)) / 100;
            //BEGIN - G. Vera 08/02/2012: Hot Fix (JIRA:VPI-49)
            var perc = 0;
            if (nextLowestBid != 0)
                                  {
                perc = ((nextLowestBid - currentBid) / parseFloat(nextLowestBid)) * 100;
            }
            //END - G. Vera 08/02/2012: Hot Fix (JIRA:VPI-49)

            //var perc = (LowestbidPercentage * parseFloat(currentBid)) / 100;
            // parseFloat(currentBid) > (parseFloat(nextLowestBid) + perc)
            if (perc >= LowestbidPercentage) {
                document.getElementById("<%=rdoFinancial4Yes.ClientID%>").checked = true;
                document.getElementById("<%=hdnFinancial4.ClientID%>").value = "1";
            }
            else {
                document.getElementById("<%=rdoFinancial4No.ClientID%>").checked = true;
                document.getElementById("<%=hdnFinancial4.ClientID%>").value = "0";
            }
        }
        validateFinance();
    }

    function bondingSection() {
        var proposedValue = removeCommas(document.getElementById("<%=txtAppSubContractorValue.ClientID%>").value);
        var availableBond = removeCommas(document.getElementById("<%=txtAvailBondingCapacity.ClientID%>").value);
        if (proposedValue != "" && availableBond != "") {
            if (parseFloat(availableBond) > parseFloat(proposedValue)) {
                document.getElementById("<%=rdoInsurance4Yes.ClientID%>").checked = true;
                document.getElementById("<%=rdoInsurance4No.ClientID%>").checked = false;
                document.getElementById("<%=hdnInsurance.ClientID%>").value = "1";



            }
            else {
                document.getElementById("<%=rdoInsurance4No.ClientID%>").checked = true;
                document.getElementById("<%=rdoInsurance4Yes.ClientID%>").checked = false;
                document.getElementById("<%=hdnInsurance.ClientID%>").value = "0";

            }
        }
        else {
            document.getElementById("<%=rdoInsurance4No.ClientID%>").checked = true;
            document.getElementById("<%=rdoInsurance4Yes.ClientID%>").checked = false;
            document.getElementById("<%=hdnInsurance.ClientID%>").value = "0";

        }
        checkRisk();
    }
    function validateFinance() {
        var count = 0;
        if (document.getElementById("<%=rdoFinancial1Yes.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=rdoFinancial2Yes.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=rdoFinancial3Yes.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=rdoFinancial4Yes.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=rdoFinancial5Yes.ClientID%>").checked)
            count = count + 1;
        if (count >= 2) {
            document.getElementById("<%=chkFinancialRiskIndicator.ClientID%>").checked = true;
            document.getElementById("<%=hdnFinancialRiskIndicator.ClientID%>").value = 1;
        }
        else {
            document.getElementById("<%=chkFinancialRiskIndicator.ClientID%>").checked = false;
            document.getElementById("<%=hdnFinancialRiskIndicator.ClientID%>").value = 0;
        }
        checkRisk();
    }
    function numberRateFormat(nStr, prefix, ctrl) {
        var txt = document.getElementById(ctrl);
        if (txt.value != "0") {
            var prefix = prefix || '';
            nStr += '';
            nStr = nStr.replace(/^[0]+/g, "");

            y = nStr.split(',');
            var y1 = '';
            for (var i = 0; i < y.length; i++) {
                y1 += y[i];
            }
            if (y.length > 0) {
                nStr = y1;
            }
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1))
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            var txt = document.getElementById(ctrl);
            txt.value = x1 + x2;
        }
        if (txt.value.length != 0) {
            txt.value = roundNumber(txt.value, 2);
        }
    }
    function numberEMRFormat(nStr, prefix, ctrl) {
        var txt = document.getElementById(ctrl);
        if (txt.value != "0") {
            var prefix = prefix || '';
            nStr += '';
            nStr = nStr.replace(/^[0]+/g, "");
            y = nStr.split(',');
            var y1 = '';
            for (var i = 0; i < y.length; i++) {
                y1 += y[i];
            }
            if (y.length > 0) {
                nStr = y1;
            }
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1))
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            var txt = document.getElementById(ctrl);
            txt.value = x1 + x2;
        }
    }
    function validateRate(srcElem, year) {

        var msg = "";
        if (document.getElementById("<%=txtRateOne.ClientID%>").value == "") {
            msg = msg + "<li>Please enter rate under year one</li>";
        }

        if (document.getElementById("<%=txtRateTwo.ClientID%>").value == "") {
            msg = msg + "<li>Please enter rate under year two</li>";
        }

        if (document.getElementById("<%=txtRateThree.ClientID%>").value == "") {
            msg = msg + "<li>Please enter rate under year three</li>";
        }

        if (msg != "") {
            document.getElementById("<%=lblemrmsg.ClientID%>").innerHTML = "";
            document.getElementById("<%=lblemrmsg.ClientID%>").style.display = "";
            document.getElementById("<%=lblemrmsg.ClientID%>").innerHTML = msg;
            document.getElementById("trMessage").style.display = "";
        }
        else {
            document.getElementById("<%=lblemrmsg.ClientID%>").style.display = "none";
            document.getElementById("<%=lblemrmsg.ClientID%>").innerHTML = "";
            document.getElementById("trMessage").style.display = "none";
        }
    }
    function validateRateRange(srcElem, year) {
        //debugger; 
        var msg = "";
        if (document.getElementById("<%=txtRateOne.ClientID%>").value != "") {
            if (!(parseFloat(document.getElementById("<%=txtRateOne.ClientID%>").value) >= 0 && parseFloat(document.getElementById("<%=txtRateOne.ClientID%>").value) <= 3)) {
                msg = msg + "<li>Please enter rate under year one range between 0 to 3</li>";
            }
        }
        if (document.getElementById("<%=txtRateTwo.ClientID%>").value != "") {
            if (!(parseFloat(document.getElementById("<%=txtRateTwo.ClientID%>").value) >= 0 && parseFloat(document.getElementById("<%=txtRateTwo.ClientID%>").value) <= 3)) {
                msg = msg + "<li>Please enter rate under year two range between 0 to 3</li>";
            }
        }
        if (document.getElementById("<%=txtRateThree.ClientID%>").value != "") {
            if (!(parseFloat(document.getElementById("<%=txtRateThree.ClientID%>").value) >= 0 && parseFloat(document.getElementById("<%=txtRateThree.ClientID%>").value) <= 3)) {
                msg = msg + "<li>Please enter rate under year three range between 0 to 3</li>";
            }
        }
        if (msg != "") {

            document.getElementById("<%=lblemrmsg.ClientID%>").style.display = "";
            document.getElementById("<%=lblemrmsg.ClientID%>").innerHTML = msg;
            document.getElementById("trMessage").style.display = "";
        }
        else {
            document.getElementById("<%=lblemrmsg.ClientID%>").style.display = "none";
            document.getElementById("<%=lblemrmsg.ClientID%>").innerHTML = "";
            document.getElementById("trMessage").style.display = "none";
        }
    }
    function checkRisk() {
        //        //
        //        debugger; 
        
        document.getElementById("<%=hdnGroupRisk.ClientID%>").value = "1";

        document.getElementById("<%=txtProjectManagerName.ClientID%>").disabled = true;

        document.getElementById("<%=txtRiskDate1.ClientID%>").disabled = true;

       


        document.getElementById("<%=txtRiskDate3.ClientID%>").disabled = true;
        
        if (document.getElementById("<%=rdoLicense1No.ClientID%>").checked) {
            document.getElementById("<%=chkDisapproved.ClientID%>").checked = true;
            document.getElementById("<%=txtGroupName2.ClientID%>").disabled = false;
            document.getElementById("<%=txtRiskDate3.ClientID%>").disabled = false;
            document.getElementById("<%=chkAverageRisk.ClientID%>").checked = false;
            document.getElementById("<%=hdnrisk.ClientID%>").value = 3;
            return;
        }
        var count = 0;
        if (document.getElementById("<%=chkBusinessRiskIndicator.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=chkFinancialRiskIndicator.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=chkInsuranceRiskIndicator.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=chkReferenceRiskIndicator.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=chkSafetyRiskIndicator.ClientID%>").checked)
            count = count + 1;
        var subContractValue = removeCommas(document.getElementById("<%=txtAppSubContractorValue.ClientID%>").value);
        //debugger;
        var docApprovalTreshold = "<%= Convert.ToInt64(ConfigurationManager.AppSettings["DocApprovalTreshold"]) %>"
        if (count <= 1) {
            document.getElementById("<%=chkAverageRisk.ClientID%>").checked = true;
            document.getElementById("<%=chkHighRisk.ClientID%>").checked = false;
            document.getElementById("<%=chkDisapproved.ClientID%>").checked = false;
            document.getElementById("<%=txtProjectManagerName.ClientID%>").disabled = false;
            document.getElementById("<%=txtRiskDate1.ClientID%>").disabled = false;
            document.getElementById("<%=hdnrisk.ClientID%>").value = 1;
            //G. Vera 01/10/2018:
            //debugger;
            //var groupName1 = document.getElementById("<%=txtGroupName1.ClientID%>").value;
            //document.getElementById("<%=txtGroupName1.ClientID%>").disabled = (subContractValue < docApprovalTreshold);
            //document.getElementById("<%=txtRiskDate2.ClientID%>").disabled = (subContractValue < docApprovalTreshold);
        }
        else {
            //debugger;
            document.getElementById("<%=chkHighRisk.ClientID%>").checked = true;
            document.getElementById("<%=chkAverageRisk.ClientID%>").checked = false;
            document.getElementById("<%=chkDisapproved.ClientID%>").checked = false;
            //document.getElementById("<%=txtGroupName1.ClientID%>").disabled = false;
            //document.getElementById("<%=txtRiskDate2.ClientID%>").disabled = false;
            document.getElementById("<%=hdnrisk.ClientID%>").value = 2;
        }
        //        if(document.getElementById("<%=chkAverageRisk.ClientID%>").checked)
        //        {
        if (document.getElementById("<%=hdnEmpName.ClientID%>").value != "") {
            document.getElementById("<%=txtProjectManagerName.ClientID%>").value = document.getElementById("<%=hdnEmpName.ClientID%>").value; //"UMA";
        }

        //            document.getElementById("<%=txtRiskDate1.ClientID%>").value=document.getElementById("<%=hdnDate.ClientID%>").value;

        //        }
    }
    function checkRiskIndicator(src, chk1, chk2, chk3) {
        var status = src.checked;
        //document.getElementById("<%=txtProjectManagerName.ClientID%>").value = "";
        document.getElementById("<%=txtProjectManagerName.ClientID%>").disabled = true;
        //document.getElementById("<%=txtRiskDate1.ClientID%>").value = "";
        document.getElementById("<%=txtRiskDate1.ClientID%>").disabled = true;
        debugger;
        
       // document.getElementById("<%=txtGroupName1.ClientID%>").disabled = true;
        //document.getElementById("<%=txtRiskDate2.ClientID%>").value = "";
        //document.getElementById("<%=txtRiskDate2.ClientID%>").disabled = true; //Remarked out by N Schoenberger 10-06-2011 date validation problem
        //document.getElementById("<%=txtGroupName2.ClientID%>").value = "";
        //document.getElementById("<%=txtGroupName2.ClientID%>").disabled = true; //Remarked out by N Schoenberger 10-06-2011 date validation problem
        //document.getElementById("<%=txtRiskDate3.ClientID%>").value = "";
        document.getElementById("<%=txtRiskDate3.ClientID%>").disabled = true;
        if (src.id.indexOf("chkAverageRisk") != -1 && src.checked) {
            document.getElementById("<%=txtProjectManagerName.ClientID%>").disabled = false;
            document.getElementById("<%=txtRiskDate1.ClientID%>").disabled = false;
            var subContractValue = removeCommas(document.getElementById("<%=txtAppSubContractorValue.ClientID%>").value);
            //debugger;
            //var docApprovalTreshold = "<%= Convert.ToInt64(ConfigurationManager.AppSettings["DocApprovalTreshold"]) %>"
            //document.getElementById("<%=txtGroupName1.ClientID%>").disabled = (subContractValue < docApprovalTreshold);
        }
        if (src.id.indexOf("chkHighRisk") != -1 && src.checked) {
            //debugger;
            //document.getElementById("<%=txtGroupName1.ClientID%>").disabled = false;
            //document.getElementById("<%=txtRiskDate2.ClientID%>").disabled = false;
        }
        if (src.id.indexOf("chkDisapproved") != -1 && src.checked) {
            document.getElementById("<%=txtGroupName2.ClientID%>").disabled = false;
            document.getElementById("<%=txtRiskDate3.ClientID%>").disabled = false;
        }
        document.getElementById(chk1).checked = false;
        document.getElementById(chk2).checked = false;
        document.getElementById(chk3).checked = false;
        src.checked = status;
    }
    function validateInsurance() {
        var count = 0;
        if (document.getElementById("<%=rdoInsurance1Yes.ClientID%>").checked) {
            document.getElementById("<%=rdoInsurance2Yes.ClientID%>").disabled = true;
            document.getElementById("<%=rdoInsurance2No.ClientID%>").disabled = true;
            document.getElementById("<%=rdoInsurance2Yes.ClientID%>").checked = false;
            document.getElementById("<%=rdoInsurance2No.ClientID%>").checked = false;
        }
        else if (document.getElementById("<%=rdoInsurance1No.ClientID%>").checked) {
            document.getElementById("<%=rdoInsurance2Yes.ClientID%>").disabled = false;
            document.getElementById("<%=rdoInsurance2No.ClientID%>").disabled = false;
        }

        if (document.getElementById("<%=rdoInsurance1No.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=rdoInsurance2No.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=rdoInsurance3No.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=rdoInsurance4No.ClientID%>").checked)
            count = count + 1;
        if (count >= 2) {
            document.getElementById("<%=chkInsuranceRiskIndicator.ClientID%>").checked = true;
            document.getElementById("<%=hdnInsuranceRiskIndicator.ClientID%>").value = 1;
        }
        else {
            document.getElementById("<%=chkInsuranceRiskIndicator.ClientID%>").checked = false;
            document.getElementById("<%=hdnInsuranceRiskIndicator.ClientID%>").value = 0;
        }
        checkRisk();
    }
    function showComment(param) {
        if (param == "1") {
            document.getElementById("<%=tdAlertComment.ClientID%>").style.display = "";
        }
        else {
            document.getElementById("<%=tdAlertComment.ClientID%>").style.display = "none";
        }
    }
    function showAttachment(type) {
        //document.getElementById("<%=hdnAttachmentType.ClientID%>").value = type;
        return true;
    }
    function validateDoc() {
        var msg = "";
        msg = msg + validateDocument(document.getElementById("<%=txtFileName1.ClientID%>"), document.getElementById("<%=fluUpload1.ClientID%>"), "one");
        msg = msg + validateDocument(document.getElementById("<%=txtFileName2.ClientID%>"), document.getElementById("<%=fluUpload2.ClientID%>"), "two");
        msg = msg + validateDocument(document.getElementById("<%=txtFileName3.ClientID%>"), document.getElementById("<%=fluUpload3.ClientID%>"), "three");
        msg = msg + validateDocument(document.getElementById("<%=txtFileName4.ClientID%>"), document.getElementById("<%=fluUpload4.ClientID%>"), "four");
        msg = msg + validateDocument(document.getElementById("<%=txtFileName5.ClientID%>"), document.getElementById("<%=fluUpload5.ClientID%>"), "five");
        if (msg != "") {
            document.getElementById("<%=lblattach.ClientID%>").value = msg;
            alert(msg);
            return false;
        }
        return true;
    }
    function validateDocument(txtBox, fileUpload, num) {
        var msg = "";
        if (trimAll(txtBox.value) == "" && trimAll(fileUpload.value) != "") {
            msg = msg + "Please enter the description " + num + "\n";
        }
        if (trimAll(txtBox.value) != "" && trimAll(fileUpload.value) == "") {
            msg = msg + "Please upload the document " + num + "\n";
        }
        if (trimAll(fileUpload.value) != "") {
            var ext = fileUpload.value;
            var len = fileUpload.value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();
                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file " + num + "\n";
                }
            }
        }
        return msg;
    }
    function getConfirmation() {
        if (confirm("Are you sure you want to delete the selected document?"))
            return true;
        return false;
    }

    function getSREConfirmation() {
        var isOriginator = document.getElementById("<%=hdnIsOriginator.ClientID%>").value;
        var orginatorsName = document.getElementById("<%=hdnOriginator.ClientID%>").value;
        if (confirm("Are you sure you want to delete the selected SRE?")) return true;
        else return false;

//        G. Vera 09/25/2012 - Added
//        if (isOriginator == "true") {
//            if (confirm("Are you sure you want to delete the selected SRE?")) return true;
//            else return false;
//        }
//        else {
//            alert("Cannot delete SRE as it belongs to " + orginatorsName + ".\nPlease contact originator for assistance.");
//            return false;
//        }
        
    }
    function numberFormat(nStr, prefix, ctrl) {
        //debugger; 
        var txt = document.getElementById(ctrl);
        if (txt.value != "0") {

            var prefix = prefix || '';
            nStr += '';
            nStr = nStr.replace(/^[0]+/g, "");

            y = nStr.split(',');
            var y1 = '';
            for (var i = 0; i < y.length; i++) {
                y1 += y[i];

            }
            if (y.length > 0) {
                nStr = y1;
            }
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1))
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            var txt = document.getElementById(ctrl);

            txt.value = prefix + x1 + x2;
            // alert(txt.value);  
        }
    }

    function validateReference() {
        var count = 0;
        if (document.getElementById("<%=rdoReference1No.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=rdoReference2No.ClientID%>").checked)
            count = count + 1;
        if (document.getElementById("<%=rdoReference3No.ClientID%>").checked)
            count = count + 1;

        if (count > 1) {
            document.getElementById("<%=chkReferenceRiskIndicator.ClientID%>").checked = true;
            document.getElementById("<%=hdnReferenceRiskIndicator.ClientID%>").value = 1;
        }
        else {
            document.getElementById("<%=chkReferenceRiskIndicator.ClientID%>").checked = false;
            document.getElementById("<%=hdnReferenceRiskIndicator.ClientID%>").value = 0;
        }
        checkRisk();
    }

    function Uncheck(chkBox) {
        //GVERA: Not sure why: chkBox.checked = false;
    }

    function ToggleElement(id, isChecked) {
        
        if (!isChecked) {
            document.getElementById(id).value = "";
            document.getElementById(id).hidden = "hidden"
        }
        else {
            document.getElementById(id).hidden = ""
        };

    }

</script>

<script type="text/javascript">
    function validateBidPackageNo() {

    }
</script>

