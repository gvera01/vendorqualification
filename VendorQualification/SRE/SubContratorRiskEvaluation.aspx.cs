﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web;
using VMSDAL;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 08/02/2012: Hot Fix (JIRA:VPI-49)
 * 003 G. Vera 09/18/2012: Hot Fix (JIRA:VPI-48)
 * 004 G. Vera 09/21/2012: Modification (JIRA:VPI-69, VPI-71, VPI-70, VPI-73)
 * 005 G. Vera 11/07/2012: Modification to increase max file size validation\
 * 006 G. Vera 01/29/2013: Hot Fix 1.3.1 (JIRA:VPI-90)
 * 007 Sooraj Sudhakaran.T 23/09/2013: - Change SRE Calculation for Non-US vendors and display label . 
 * 008 G. Vera 06/20/2014: New Financial year if first year is marked not available
 * 009 G. Vera 03/09/2015: Correction on value for business question 3.
 * 010 G. Vera 04/25/2015: Correction on Safety EMR rates calculation
 * 011 G. Vera 10/22/2015:  Correct Annaual Revenue 3 year Avg calculation, correct Contract Value calculation
 * 012 - G. Vera 11/18/2015: Change sending vendor ids in URL as it could reach URL length limits
 * 013 - G. Vera 11/20/2015: Mitigate the "NaN" result from divison of zero to eliminate error on converting string to decimal
 * 014 - G. Vera 12/28/2016: Add new questions and add new SRE admin role.
 * 015 - G. Vera 11/19/2018: Change DOC signature logic to be enabled on Average Risk when subcontract value is >= $100,000
 * 016 - G. Vera 05/18/2020: Disable SSDI Program checklist item in the CheckBoxListItemsScripts() method, change the 
 *                           DOC signature requirement treshold to $250,000
 ****************************************************************************************************************/
#endregion

public partial class SubContratorRiskEvaluation : System.Web.UI.Page
{

    #region Instantiate the class and declare varaibles

    private Int64 vendorId = 0;
    private Int64 riskId = 0;
    private Int32 statusId = 0;
    private Int64 userId = 0;
    private byte editStatus = 0;
    private Int64? lockedBy = null;

    private string projectNumber = string.Empty;
    private const string TAG_ERROR_MSG = "<li>{}</li>";
    //005 - max file size to come from configuration
    private static int MAXFILESIZE = Convert.ToInt32(ConfigurationManager.AppSettings["MaxFileSizeMBs"].ToString()) * 1024 * 1024;
    string DOCUMENT_LOCATION = string.Empty;
    public DataSet Lookup = new DataSet();

    RiskEvaluation riskEvaluation = new RiskEvaluation();
    private bool IsNonUSVendor = false; //007
    public static DataSet SREDataSet;
    Common objCommon = new Common();
    string accrodinOpen = string.Empty;
    bool isContractNeedDOCReview;
    //016
    private int DocApprovalTreshold = ConfigurationManager.AppSettings["DocApprovalTreshold"].ConvertToInteger();
    private string DocApprovalTresholdDisplay = ConfigurationManager.AppSettings["DocApprovalTresholdDisplay"];
    //016
    #endregion

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]).Equals("0"))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
        }
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (Session["RISKIDLOGOUT"] != null && Session["LOCKEDBYLOGOUT"] != null)
        {
            Int64 Lockedby = riskEvaluation.LockebByUser(Convert.ToInt64(Session["RISKIDLOGOUT"]));
            if (Lockedby == Convert.ToInt64(Session["LOCKEDBYLOGOUT"]))
            {
                riskEvaluation.UpdateStatus(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                Session["RISKIDLOGOUT"] = null;
            }
        }

        DOCUMENT_LOCATION = ConfigurationManager.AppSettings["SREDocument"].ToString();
        riskId = Convert.ToInt64(objCommon.decode(Request.QueryString["riskId"]));
        Session["RISKIDLOGOUT"] = Convert.ToInt64(objCommon.decode(Request.QueryString["riskId"]));
        userId = Convert.ToInt64(Session["EmployeeId"]);
        Session["EMPLOYEEIDLOGOUT"] = Convert.ToInt64(Session["EmployeeId"]);
        Session["LOCKEDBYLOGOUT"] = Convert.ToInt64(Session["EmployeeId"]);
        

        GetSREDetail();
        if (statusId == 2 || statusId == 4 || statusId == 5)
            Lookup = riskEvaluation.GetSRELookUpData(riskId);
        else
        {
            Lookup = riskEvaluation.GetSRELookUpData(-1);
        }
        BusinessYear.InnerText = "Has the Subcontractor been in business less than " + Lookup.Tables[0].Rows[0]["BusinessYear"].ToString() + " years?";
        LargestMaxPercentage.InnerText = "Does Subcontractor's bid exceed their largest maximum contract value by " + Lookup.Tables[0].Rows[0]["LargestMaxPercentage"].ToString() + "%?";
        LargestMaxPastYear.InnerText = "Largest Max Contract (over past " + (Convert.ToInt32(Lookup.Tables[0].Rows[0]["LargestMaxPastYear"])-1).ToString() + " yrs):";                              //008
        AannualRevenuePastYear.InnerText = "Is Subcontractor's current total backlog > the " + Lookup.Tables[0].Rows[0]["AannualRevenuePastYear"].ToString() + " year average annual revenue?";
        AannualRevenuePastYear1.InnerText = Lookup.Tables[0].Rows[0]["AannualRevenuePastYear"].ToString() + " Year Average Annual Revenue:";
        BidAannualRevenuePercentage.InnerText = "Is Subcontractor's bid > " + Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"].ToString() + "% of " + Lookup.Tables[0].Rows[0]["BidAannualRevenuePastYear"].ToString() + " year average annual revenue?";
        LowestbidPercentage.InnerText = "Is the Subcontractor's bid " + Lookup.Tables[0].Rows[0]["LowestbidPercentage"].ToString() + "% or more lower than next bidder?";
        EMRPastYear.InnerText = "Is EMR history in any of past " + (Convert.ToInt32(Lookup.Tables[0].Rows[0]["EMRPastYear"]) - 1).ToString() + " years greater than " + Lookup.Tables[0].Rows[0]["EMRValue"].ToString() + "?";      //006

        if (!IsPostBack)
        {
            //004 - on load only
            LoadRiskManagmentChecklist();

            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");
            LoadProjectNumber();
            hdnGroupRisk.Value = "0";
            AppendClientScript();
            BindData();
            imbUpload1.ToolTip = "Attach Document";
            ShowButton();
            HighRisk();
            if (Request.QueryString.Count > 1)
            {
                imgback.Visible = true;
            }
            else
            {
                imgback.Visible = false;
            }
            txtAppSubContractorValue.Focus();
        }
        //004
        this.CheckBoxListItemsScripts();

        ShowActionButton();
        string role = Session["EmployeeRole"].ConvertToString();    //014
        AdminMenu.DisplayMenu(Convert.ToString(Session["Access"]).Equals("1") ? "Admin" : "Employee");
        AdminView(Convert.ToString(Session["Access"]));

        if (imbComplete.Visible && (Session["Access"].ToString() != "1" || role == clsAdminLogin.EmployeeRole.Admin_SRE.ToString()))
        {

            hdnEmpName.Value = Session["EmployeeName"].ToString();
            txtRiskDate1.Text = DateTime.Now.ToString("MM/dd/yyyy");

        }
        else
        {
            hdnEmpName.Value = txtProjectManagerName.Text;

        }
        rdoFinancial1Yes.Enabled = rdoFinancial1No.Enabled = false;
        rdoFinancial3Yes.Enabled = rdoFinancial3No.Enabled = false;
        rdoFinancial4Yes.Enabled = rdoFinancial4No.Enabled = false;
        rdoInsurance4No.Enabled = rdoInsurance4Yes.Enabled = false;

        txtRate1.Enabled = txtRate2.Enabled = txtRate3.Enabled = false;

        //004 - changed
        //if (!imbComplete.Visible)
        if (Convert.ToString(Session["Access"]).Equals("1"))  //&& statusId != 4)  014 allow awarded to be deleted as well
            imbDeleteSRE.Visible = true;
        else
            imbDeleteSRE.Visible = false;


        if (rdoInsurance1Yes.Checked)
        {
            rdoInsurance2Yes.Checked = false;
            rdoInsurance2No.Checked = false;
            rdoInsurance2Yes.InputAttributes.Add("disabled", "disabled");
            rdoInsurance2No.InputAttributes.Add("disabled", "disabled");
        }

        //DataSet vendorDataSet = riskEvaluation.GetSelectedRiskDetail(riskId);
        //if (hdnrisk.Value == "2")
        //{

        //    txtGroupName1.Text = vendorDataSet.Tables[0].Rows[0]["GroupDOCName"].ToString();
        //    txtRiskDate2.Text = vendorDataSet.Tables[0].Rows[0]["SREDate"].ToString();
        //}
        //else if (hdnrisk.Value == "3")
        //{

        //    txtGroupName2.Text = vendorDataSet.Tables[0].Rows[0]["GroupDOCName"].ToString();
        //    txtRiskDate3.Text = vendorDataSet.Tables[0].Rows[0]["SREDate"].ToString();
        //}
        //vendorDataSet.Dispose();  

    }

    #endregion

    #region Button Events

    #region Save and close the SRE Process Events
    /// <summary>
    /// Save and Close the SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSave_Click(object sender, ImageClickEventArgs e)
    {

        txtProjectManagerName.Text = Session["EmployeeName"].ToString();

        if (txtAppSubContractorValue.Text == "0" || txtAppSubContractorValue.Text.Length == 0)
            txtAppSubContractorValue.Text = "";

        string message = string.Empty;
        accrodinOpen = string.Empty;

        if (ValidateSREDate(ref message))
        {
            //bool a = rdoFinancial1Yes.Checked;
            //bool b = rdoFinancial1No.Checked;
            // AppendClientScript();
            imbSaveSubmit();
            GetSREDetail();
            BindData();
            RefreshSRE();   // G. Vera 06/21/2012 - Added
            align1.Align = "Center";
            lblMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "SRESave").ToString();
            imbOK.Visible = false;
            if (imbOK1.Visible)
                imbOK1.Visible = false;
            imbOKSave.Visible = true;
            //Modified by G. Vera 04/13/2012 - Added code below
            imbContSave.Visible = true;
            mpopAlert.Show();

        }
        else
        {

            lblMessage.Text = message;
            imbOK1.Visible = true;
            imbOK.Visible = false;
            mpopAlert.Show();
            ShowButton();
            AccrodineValidateFocus();
        }

        if (hdnrisk.Value == "1")
        {
            chkAverageRisk.Checked = true;
            chkHighRisk.Checked = false;
            chkDisapproved.Checked = false;

        }
        else if (hdnrisk.Value == "2")
        {
            chkHighRisk.Checked = true;
            chkDisapproved.Checked = false;
            chkAverageRisk.Checked = false;

        }
        else if (hdnrisk.Value == "3")
        {
            chkDisapproved.Checked = true;
            chkHighRisk.Checked = false;
            chkAverageRisk.Checked = false;

        }

    }
    protected void imbOKSave_Click(object sender, ImageClickEventArgs e)
    {
        imgback_Click(sender, e);
    }

    //  Modified by G. Vera 04/13/2012 - Continue button click event
    protected void imbContSave_Click(object sender, ImageClickEventArgs e)
    {
        //imgback_Click(sender, e);
        imbContSave.Visible = false;
    }
    #endregion

    #region Complete the SRE Process Events
    /// <summary>
    /// Complete the SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbComplete_Click(object sender, ImageClickEventArgs e)
    {
        txtProjectManagerName.Text = Session["EmployeeName"].ToString();
        string message = string.Empty;
        accrodinOpen = string.Empty;
        if (txtAppSubContractorValue.Text == "0" || txtAppSubContractorValue.Text.Length == 0)
            txtAppSubContractorValue.Text = "";
        if (ValidateSREDate(ref message))
        {
            imbSaveSubmit();
            GetSREDetail();
            BindData();

            if (ValidateSRE(ref message))
            {
                if (ValidateSRE1(ref message))
                {

                    byte vendorStatus = riskEvaluation.GetVendorStatus(vendorId);
                    if (riskEvaluation.GetVendorStatus(vendorId).Equals(2))
                    {
                        AppendClientScript();
                        imbSaveSubmit();
                        DataSet VQFDataSet = riskEvaluation.GetVendorDetailsForSRE(vendorId, Convert.ToInt16(Lookup.Tables[0].Rows[0]["LargestMaxPastYear"]), Convert.ToInt16(Lookup.Tables[0].Rows[0]["EMRPastYear"]));
                       
                        lblCompareMessage.Text = string.Empty;
                        lblCompareMessage.Text = CompareSREVQF(SREDataSet, VQFDataSet);
                     

                        //if (lblCompareMessage.Text == string.Empty)
                            lblCompareMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "Complete").ToString();
                        imbOK2.Visible = true;
                        mpopAlert1.Show();
                    }
                    else
                    {
                        if (vendorStatus == 0)
                        {
                            lblMessage.Text = "VQF is currently in inprogress status";
                            imbOK1.Visible = true;
                            imbCompletedOK.Visible = false;
                            imbOK.Visible = false;
                            imbDeleteOk.Visible = false;
                            imbNotify.Visible = false;
                            imb.Visible = false;
                        }
                        else if (vendorStatus == 1)
                        {
                            lblMessage.Text = "VQF is currently in pending Status";
                            imbOK1.Visible = false;
                            imbCompletedOK.Visible = false;
                            imbOK.Visible = false;
                            imbDeleteOk.Visible = false;
                            imbNotify.Visible = true;
                            imb.Visible = true;

                        }
                        else if (vendorStatus == 3)
                        {
                            lblMessage.Text = "VQF is currently in revision status";
                            imbOK1.Visible = false;
                            imbCompletedOK.Visible = false;
                            imbOK.Visible = false;
                            imbDeleteOk.Visible = false;
                            imbNotify.Visible = true;
                            imb.Visible = true;
                        }
                        else if (vendorStatus == 4)
                        {
                            lblMessage.Text = "VQF is currently in rejected status";
                            imbOK1.Visible = true;
                            imbCompletedOK.Visible = false;
                            imbOK.Visible = false;
                            imbDeleteOk.Visible = false;
                            imbNotify.Visible = false;
                            imb.Visible = false;
                        }
                        else if (vendorStatus == 5)
                        {
                            lblMessage.Text = "VQF is currently in incomplete status";
                            imbOK1.Visible = true;
                            imbCompletedOK.Visible = false;
                            imbOK.Visible = false;
                            imbDeleteOk.Visible = false;
                            imbNotify.Visible = false;
                            imb.Visible = false;
                        }
                        else if (vendorStatus == 6)
                        {
                            lblMessage.Text = "VQF is currently in expired status";
                            //imbOK1.Visible = true;
                            //imbCompletedOK.Visible = false;
                            //imbOK.Visible = false;
                            //imbDeleteOk.Visible = false;
                            //imbNotify.Visible = true;
                            //imb.Visible = false;

                            imbOK1.Visible = false;
                            imbCompletedOK.Visible = false;
                            imbOK.Visible = false;
                            imbDeleteOk.Visible = false;
                            imbNotify.Visible = true;
                            imb.Visible = true;



                        }
                        align1.Align = "Center";


                        mpopAlert.Show();

                    }

                }
                else
                {
                    align1.Align = "Left";
                    lblMessage.Text = message;
                    imbOK1.Visible = true;
                    imbOK.Visible = false;
                    mpopAlert.Show();
                    ShowButton();
                    AccrodineValidateFocus();

                }
            }
            else
            {

                lblMessage.Text = message;
                align1.Align = "Left";
                imbOK1.Visible = true;
                imbOK.Visible = false;
                mpopAlert.Show();
                ShowButton();
                AccrodineValidateFocus();
                txtGroupName2.Enabled = true;
                txtRiskDate2.Enabled = true;
            }
        }
        else
        {
            align1.Align = "Left";
            lblMessage.Text = message;
            imbOK1.Visible = true;
            imbOK.Visible = false;
            mpopAlert.Show();
            ShowButton();
            AccrodineValidateFocus();

        }

        if (hdnrisk.Value == "1")
        {
            chkAverageRisk.Checked = true;
            chkHighRisk.Checked = false;
            chkDisapproved.Checked = false;

        }
        else if (hdnrisk.Value == "2")
        {
            chkHighRisk.Checked = true;
            chkDisapproved.Checked = false;
            chkAverageRisk.Checked = false;

        }
        else if (hdnrisk.Value == "3")
        {
            chkDisapproved.Checked = true;
            chkHighRisk.Checked = false;
            chkAverageRisk.Checked = false;

        }
    }
    protected void imbNotify_Click(object sender, ImageClickEventArgs e)
    {
        riskEvaluation.AddMailNotification(vendorId, txtVendorName.Text.Trim(), Session["VendorNameEmailNotification"].ToString(), "Pending", Session["EmployeeName"].ToString(), Session["EmailUser"].ToString(), 0);
        imbNotify.Visible = false;
        imb.Visible = false;
        align1.Align = "Center";
        lblMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "EmailNotification").ToString();
        hdnFinancial3.Value = "";
        hdnFinancial1.Value = "";
        hdnFinancial4.Value = "";

        imbOK1.Visible = true;
        imbOK.Visible = false;
        imbOK2.Visible = false;
        mpopAlert.Show();
    }
    /// <summary>
    /// Confirm to complete the SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbOK2_Click(object sender, ImageClickEventArgs e)
    {
        string fileName = txtVendorName.Text.Trim() + txtBidpackageNumber.Text.Trim() + DateTime.Now.ToString();
        fileName = fileName.Replace("-", "").Replace(" ", "").Replace(":", "").Replace("AM", "").Replace("PM", "").Replace("/", "-").Replace(",","").Replace(".","_");   //GV

        //GV: not needed riskEvaluation.UpdateSREDocument(riskId);
        bool isCreated = new Common().SaveVQFSnapshot(vendorId, Convert.ToString(Session["Access"]).Equals("1") ? "admin" : "user", fileName);


        bool isCreated1 = new Common().SaveSRESnapshot(riskId, fileName, statusId);
        if (isCreated && isCreated1)
        {
            GetSREDetail();
            imbSaveSubmit();
            BindData();
            imbSaveSubmit();
            riskEvaluation.UpdateBidPackage(riskId, string.Empty, 2, 0, userId);// new Nullable<Int64>()); 
            statusId = 2;
            ShowButton();
            riskEvaluation.AddNewCompletedSRE(Lookup.Tables[0].Rows[0]["BusinessYear"].ToString(), Lookup.Tables[0].Rows[0]["LargestMaxPercentage"].ToString(), Lookup.Tables[0].Rows[0]["LargestMaxPastYear"].ToString(), Lookup.Tables[0].Rows[0]["AannualRevenuePastYear"].ToString(), Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"].ToString(), Lookup.Tables[0].Rows[0]["BidAannualRevenuePastYear"].ToString(), Lookup.Tables[0].Rows[0]["LowestbidPercentage"].ToString(), Lookup.Tables[0].Rows[0]["EMRPastYear"].ToString(), Lookup.Tables[0].Rows[0]["EMRValue"].ToString(), riskId);
            HighRisk();
            try
            {
                riskEvaluation.SaveSREDocument(riskId, 1, fileName, fileName + ".pdf", userId);
            }
            catch (Exception ex)
            {
                string sr = ex.StackTrace.ToString();
                DeleteSREDocument(fileName);
            }
            try
            {
                riskEvaluation.SaveVQFSnapshot(riskId, fileName, userId);
            }
            catch
            {
            }
            align1.Align = "Center";

            lblCompareMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "SREcompleted").ToString();
            imbOK2.Visible = false;
            imbCancel2.Visible = false;
            imbCompletedOK.Visible = true;
            imbDeleteSRE.Visible = false;
            StringBuilder MailBody = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hello,<br></font></td></tr>");
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
            //003
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>SRE with Vendor Name - " + txtVendorName.Text.Trim() + ", Project No - " + txtProjectNumber.Text.ToString() + ", Bid Package # - " + txtBidpackageNumber.Text.Trim() + " has been completed.<br></font></td></tr>");
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Regards,<br></font></td></tr>");
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + Session["EmployeeName"].ToString() + "<br></font></td></tr>");
            MailBody.Append("</table>");
            //DataSet dsEmailID = riskEvaluation.GetAdminEmailId();
            //string[] EmailId = new string[dsEmailID.Tables[0].Rows.Count];
            //for (int i = 0; i < dsEmailID.Tables[0].Rows.Count; i++)
            //{
            //    EmailId[i] = dsEmailID.Tables[0].Rows[i][0].ToString();
            //}
            //objCommon.SendMailToAdmin(Session["EmailUser"].ToString(), EmailId, "SRE Completed", MailBody);

            mpopAlert1.Show();
        }
        else
        {
            GetSREDetail();
            imbSaveSubmit();
            BindData();
            imbSaveSubmit();
            align1.Align = "Center";
            lblMessage.Text = "Failed to generate snapshot";
            imbOK1.Visible = true;
            imbOK.Visible = false;
            imbOK2.Visible = false;
            mpopAlert.Show();
        }

    }
    /// <summary>
    /// Cancel the completion of SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCancel2_Click(object sender, ImageClickEventArgs e)
    {
        GetSREDetail();
        BindData();
        imbSaveSubmit();
        ShowButton();
        mpopAlert1.Hide();
        HighRisk();
    }


    /// <summary>
    /// Cancel the completion of SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCompletedOK_Click(object sender, ImageClickEventArgs e)
    {
        //Response.Redirect("SREStart.aspx");
    }
    #endregion

    #region Award the SRE Process Events
    /// <summary>
    /// Award the SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAwarded_Click(object sender, ImageClickEventArgs e)
    {
        lblAwardMessage.Text = "";
        mpopAlertAward.Show();
    }
    /// <summary>
    /// Save the Contract number while Awarding the SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSubmitAward_Click(object sender, ImageClickEventArgs e)
    {
        if (string.IsNullOrEmpty(txtAlertAward.Text.Trim().Trim()))
        {
            lblAwardMessage.Text = TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ContractNumber").ToString());
            mpopAlertAward.Show();
        }
        else
        {
            riskEvaluation.UpdateBidPackage(riskId, string.Empty, 4, 0, userId);// new Nullable<Int64>()); 
            riskEvaluation.UpdateSREAward(riskId, txtAlertAward.Text.Trim(), "");
            mpopAlertAward.Hide();
            GetSREDetail();
            ShowButton();
            align1.Align = "Center";
            lblMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "SREAwarded").ToString();
            trContract.Style["display"] = "none";
            StringBuilder MailBody = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hello,<br></font></td></tr>");
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
            //003
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>SRE with Vendor Name - " + txtVendorName.Text.Trim() + ", Project No - " + txtProjectNumber.Text + ", Bid Package # - " + txtBidpackageNumber.Text.Trim() + " has been awarded.<br></font></td></tr>");
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Regards,<br></font></td></tr>");
            MailBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + Session["EmployeeName"].ToString() + "<br></font></td></tr>");
            MailBody.Append("</table>");
            string adminemail = ConfigurationManager.AppSettings["adminemail"].ToString();
            //DataSet dsEmailID = riskEvaluation.GetAdminEmailId();
            //string[] EmailId = new string[dsEmailID.Tables[0].Rows.Count];
            //for (int i = 0; i < dsEmailID.Tables[0].Rows.Count; i++)
            //{
            //    EmailId[i] = dsEmailID.Tables[0].Rows[i][0].ToString();
            //}
            objCommon.SendMailToAdmin(Session["EmailUser"].ToString(), adminemail, "SRE Awarded", MailBody);
            mpopAlert.Show();
            imbOK.Visible = true;
            imbOK1.Visible = false;
        }
        
        if(Session["Access"].ToString() != "1") imbDeleteSRE.Visible = false;       //014
    }
    #endregion

    #region Not Award the SRE Process Events
    /// <summary>
    /// Not Awarding the SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbNotAwarded_Click(object sender, ImageClickEventArgs e)
    {
        lblNotAwardMessage.Text = "";
        mpopAlertNotAward.Show();

    }
    /// <summary>
    /// Save the reason for not awarding the SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSubmitNotAward_Click(object sender, ImageClickEventArgs e)
    {
        if (string.IsNullOrEmpty(txtAlertNotAward.Text.Trim().Trim()))
        {
            lblNotAwardMessage.Text = TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "Reason").ToString());
            mpopAlertNotAward.Show();
        }
        else
        {
            riskEvaluation.UpdateBidPackage(riskId, string.Empty, 5, 0, userId);// new Nullable<Int64>()); 
            riskEvaluation.UpdateSREAward(riskId, "", txtAlertNotAward.Text.Trim());
            //004
            if (statusId == 0)
                riskEvaluation.AddNewCompletedSRE(Lookup.Tables[0].Rows[0]["BusinessYear"].ToString(), Lookup.Tables[0].Rows[0]["LargestMaxPercentage"].ToString(), Lookup.Tables[0].Rows[0]["LargestMaxPastYear"].ToString(), Lookup.Tables[0].Rows[0]["AannualRevenuePastYear"].ToString(), Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"].ToString(), Lookup.Tables[0].Rows[0]["BidAannualRevenuePastYear"].ToString(), Lookup.Tables[0].Rows[0]["LowestbidPercentage"].ToString(), Lookup.Tables[0].Rows[0]["EMRPastYear"].ToString(), Lookup.Tables[0].Rows[0]["EMRValue"].ToString(), riskId);
            mpopAlertNotAward.Hide();
            GetSREDetail();
            ShowButton();
            align1.Align = "Center";
            lblMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "SRENOTAwarded").ToString();
            trReason.Style["display"] = "none";
            mpopAlert.Show();
            imbOK.Visible = true;
            imbOK1.Visible = false;
        }
        imbDeleteSRE.Visible = false;
    }
    #endregion

    #region ReCalculate the SRE Process Events
    /// <summary>
    /// ReCalculate the SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCalculate_Click(object sender, ImageClickEventArgs e)
    {

        txtProjectManagerName.Text = Session["EmployeeName"].ToString();
        txtRiskDate1.Text = DateTime.Now.ToString("MM/dd/yyyy");
        DataSet VQFDataSet = new DataSet();
        //GV imbSaveSubmit();
        if (Lookup != null && Lookup.Tables.Count > 0 && Lookup.Tables[0].Rows.Count > 0)
        {
             VQFDataSet = riskEvaluation.GetVendorDetailsForSRE(vendorId, Convert.ToInt16(Lookup.Tables[0].Rows[0]["LargestMaxPastYear"]), Convert.ToInt16(Lookup.Tables[0].Rows[0]["EMRPastYear"]));
            lblMessage.Text = string.Empty;
            lblMessage.Text = CompareSREVQF(SREDataSet, VQFDataSet);
            if (lblMessage.Text == string.Empty)
            {
                imbOK1.Visible = true;
                imbOK.Visible = false;
                align1.Align = "Center";
                lblMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "SRERecalculated").ToString();
                BindData();
                RefreshSRE();
                HighRisk();
                mpopAlert.Show();
            }
            else
            {
                imbOK1.Visible = true;
                imbOK.Visible = false;
                GetSREDetail();
                BindData();
                imbSaveSubmit();
                RefreshSRE();
                HighRisk();
                mpopAlert.Show();
            }
        }



    }
    #endregion

    #region Print the SRE Process Events
    /// <summary>
    /// Print the SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrintSRE_Click(object sender, ImageClickEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(@"<script language='javascript'>");
        sb.Append(@"window.open('PrintSRE.aspx','PrintMe','height=300px,width=300px,scrollbars=1');");

        sb.Append(@"</script>");
        Session["riskId"] = riskId;
        Session["statusidprint"] = statusId;
        ScriptManager.RegisterStartupScript(imbPrintSRE, this.GetType(), "JSCR", sb.ToString(), false);
    }

    /// <summary>
    /// Print the SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrintSRE1_Click(object sender, ImageClickEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder(); ;
        sb.Append(@"<script language='javascript'>");
        sb.Append(@"window.open('PrintSRE.aspx','PrintMe','height=300px,width=300px,scrollbars=1');");

        sb.Append(@"</script>");
        Session["riskId"] = riskId;
        Session["statusidprint"] = statusId;
        ScriptManager.RegisterStartupScript(imbPrintSRE, this.GetType(), "JSCR", sb.ToString(), false);
    }

    protected void imgPDF_Click(object sender, ImageClickEventArgs e)
    {
        Session["riskId"] = riskId;
        Session["statusidprint"] = statusId;

        #region For Dev Only
        //Response.Redirect("~/SRE/PrintSRE.aspx?RiskId=" + Session["riskId"].ToString() + "&statusidprint=" + Session["statusidprint"].ToString()); 
        #endregion

        objCommon.GenerateSREPDF();
    }
    #endregion

    #region Upload the Documents Process Events
    /// <summary>
    /// Show the modal popup to upload the Documents for SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbUpload_Click(object sender, ImageClickEventArgs e)
    {
        BindDocument(0);
        ClearDocumentValue();
        mpopAddDocument.Show();
    }
    /// <summary>
    /// Upload Documents for SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAddDoc_Click(object sender, ImageClickEventArgs e)
    {
        if (ValidateDoc())
        {
            SaveSREDoc(txtFileName1, fluUpload1);
            SaveSREDoc(txtFileName2, fluUpload2);
            SaveSREDoc(txtFileName3, fluUpload3);
            SaveSREDoc(txtFileName4, fluUpload4);
            SaveSREDoc(txtFileName5, fluUpload5);
            BindDocument(0);
            ClearDocumentValue();
        }
        mpopAddDocument.Show();
    }
    #endregion

    #region Unlock the SRE Process Events
    /// <summary>
    /// Confirm to Archive the Snapshot
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbYes_Click(object sender, ImageClickEventArgs e)
    {
        riskEvaluation.ArchiveVQFSnapshot(riskId, userId);
        riskEvaluation.UpdateBidPackage(riskId, string.Empty, 3, 0, userId);// new Nullable<Int64>()); 
        statusId = 3;
        if (statusId == 3)
            riskEvaluation.DeleteCompletedSRE(riskId);
        ShowButton();
        mpopSaveAlertSRE.Show();

    }

    /// <summary>
    /// Cancel to archieve the snapshot
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbVQFCancel_Click(object sender, ImageClickEventArgs e)
    {
        riskEvaluation.UpdateBidPackage(riskId, string.Empty, 3, 0, userId);// new Nullable<Int64>()); 
        statusId = 3;
        if (statusId == 3)
            riskEvaluation.DeleteCompletedSRE(riskId);
        mpopSaveAlertSRE.Show();
    }


    protected void imbYesSRE_Click(object sender, ImageClickEventArgs e)
    {
        riskEvaluation.ArchiveSRESnapshot(riskId, userId);
        align1.Align = "Center";
        lblMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "SREUnlocked").ToString();
        imbOK.Visible = true;
        imbOK1.Visible = false;
        mpopAlert.Show();
    }
    protected void imbVQFCancelSRE_Click(object sender, ImageClickEventArgs e)
    {
        imbUnlockSRE.Visible = false;
        imbUnlockAdminSRE.Visible = false;  //014
        align1.Align = "Center";
        lblMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "SREUnlocked").ToString();
        imbOK.Visible = true;
        imbOK1.Visible = false;
        mpopAlert.Show();
    }
    #endregion

    #region Navigate back to Search page Events
    /// <summary>
    /// Navigate back to to the Search page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgback_Click(object sender, ImageClickEventArgs e)
    {

        if (Request.QueryString.Count == 8) //006
        {
            //012: Response.Redirect("~/SRE/ViewSRE.aspx?VendorID=" + Request.QueryString["VendorId"].ToString() + "&PageRecords=" + objCommon.encode(Request.QueryString["PageRecords"].ToString()) + "&CurrentPage=" + objCommon.encode(Request.QueryString["CurrentPage"].ToString()) + "&PageRecords1=" + Request.QueryString["PageRecords1"].ToString() + "&CurrentPage1=" + Request.QueryString["CurrentPage1"].ToString() + "&VendorName=" + Request.QueryString["VendorName"].ToString() + "&ProjectN0=" + Request.QueryString["ProjectN0"].ToString() + "&BidPackNO=" + Request.QueryString["BidPackNO"].ToString());
            //012
            Response.Redirect("~/SRE/ViewSRE.aspx?PageRecords=" + objCommon.encode(Request.QueryString["PageRecords"].ToString()) + "&CurrentPage=" + objCommon.encode(Request.QueryString["CurrentPage"].ToString()) + "&PageRecords1=" + Request.QueryString["PageRecords1"].ToString() + "&CurrentPage1=" + Request.QueryString["CurrentPage1"].ToString() + "&VendorName=" + Request.QueryString["VendorName"].ToString() + "&ProjectN0=" + Request.QueryString["ProjectN0"].ToString() + "&BidPackNO=" + Request.QueryString["BidPackNO"].ToString());
        }
        else if (Request.QueryString.Count > 1)
        {

            Response.Redirect("~/SRE/SREStart.aspx?VNameStart=" + Request.QueryString["VNameStart"].ToString() + "&VNoStart=" + Request.QueryString["VNoStart"].ToString() + Session["SREStratPage"] + "&Pagenumber=1" + "&pageSize=");

            // Response.Redirect("~/SRE/SRE.aspx?riskId=" +  Request.QueryString["riskId"].ToString() + "&VendorName=" +Request.QueryString["VendorName"].ToString() + "&BNo=" + Request.QueryString["BNo"].ToString() + "&PNo=" + Request.QueryString["PNo"].ToString() + "&NewSRE=NEW"+"&VNameStart=" + Request.QueryString["VNameStart"].ToString() + "&VNoStart=" + Request.QueryString["VNoStart"].ToString());
        }
    }
    #endregion

    #region Legal Events
    /// <summary>
    /// Save Legal for SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbLegalSave_Click(object sender, ImageClickEventArgs e)
    {
        Page.Validate("Legal");
        if (Page.IsValid)
        {

            riskEvaluation.UpdateLegal(riskId, vendorId, txtLegalYearOne.Text, txtMaxContractValueYearOne.Text, txtAnuualCompRevenueYearOne.Text);
            riskEvaluation.UpdateLegal(riskId, vendorId, txtLegalYearTwo.Text, txtMaxContractValueYearTwo.Text, txtAnuualCompRevenueYearTwo.Text);
            riskEvaluation.UpdateLegal(riskId, vendorId, txtLegalYearThree.Text, txtMaxContractValueYearThree.Text, txtAnuualCompRevenueYearThree.Text);
            mpopLegalAlert.Hide();
            imbSaveSubmit();
            DataSet VQFDataSet = riskEvaluation.GetVendorDetailsForSRE(vendorId, Convert.ToInt16(Lookup.Tables[0].Rows[0]["LargestMaxPastYear"]), Convert.ToInt16(Lookup.Tables[0].Rows[0]["EMRPastYear"]));
            GetSREDetail();
            BindData();
            RefreshSRE();
            HighRisk();
            ShowButton();

            lblMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "VendorLegal").ToString();
            imbOK1.Visible = true;
            imbOK.Visible = false;
            imbOK2.Visible = false;
            mpopAlert.Show();
        }
        else
        {
            trLegalMessage.Style["display"] = "";
            lblLegalmsg.Style["display"] = "";
            lblLegalmsg.Text = Errormsg();
            mpopLegalAlert.Show();

        }
    }
    /// <summary>
    /// View Legal for SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbViewLegal_Click(object sender, ImageClickEventArgs e)
    {
        BindData();
        lblLegalmsg.Text = "";
        if (txtMaxContractValueYearOne.Text != "" && txtMaxContractValueYearTwo.Text != "" && txtMaxContractValueYearThree.Text != "" && txtAnuualCompRevenueYearOne.Text != "" && txtAnuualCompRevenueYearTwo.Text != "" && txtMaxContractValueYearThree.Text != "")
        {
            imbLegalSave.Visible = false;
        }
        else
        {
            imbLegalSave.Visible = true;
        }


        if (txtMaxContractValueYearOne.Text == "")
            txtMaxContractValueYearOne.ReadOnly = false;
        if (txtMaxContractValueYearTwo.Text == "")
            txtMaxContractValueYearTwo.ReadOnly = false;
        if (txtMaxContractValueYearThree.Text == "")
            txtMaxContractValueYearThree.ReadOnly = false;



        if (txtAnuualCompRevenueYearOne.Text == "")
            txtAnuualCompRevenueYearOne.ReadOnly = false;
        if (txtAnuualCompRevenueYearTwo.Text == "")
            txtAnuualCompRevenueYearTwo.ReadOnly = false;
        if (txtAnuualCompRevenueYearThree.Text == "")
            txtAnuualCompRevenueYearThree.ReadOnly = false;

        mpopLegalAlert.Show();
    }
    #endregion

    #region EMR Events
    /// <summary>
    /// View EMR for SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbViewEMR_Click(object sender, ImageClickEventArgs e)
    {
        BindData();
        if (txtRateOne.Text != "" && txtRateTwo.Text != "" && txtRateThree.Text != "")
        {
            imbEMRSave.Visible = false;
        }
        else
        {
            imbEMRSave.Visible = true;
        }
        lblemrmsg.Style["display"] = "none";
        mpopEMRAlert.Show();
    }
    /// <summary>
    /// Save EMR for SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbEMRSave_Click(object sender, ImageClickEventArgs e)
    {
        lblMessage.Text = "";
        Page.Validate("Current");
        if (Page.IsValid)
        {

            riskEvaluation.UpdateEMR(riskId, vendorId, txtYearOne.Text, txtRateOne.Text);
            riskEvaluation.UpdateEMR(riskId, vendorId, txtYearTwo.Text, txtRateTwo.Text);
            riskEvaluation.UpdateEMR(riskId, vendorId, txtYearThree.Text, txtRateThree.Text);
            mpopEMRAlert.Hide();
            imbSaveSubmit();
            DataSet VQFDataSet = riskEvaluation.GetVendorDetailsForSRE(vendorId, Convert.ToInt16(Lookup.Tables[0].Rows[0]["LargestMaxPastYear"]), Convert.ToInt16(Lookup.Tables[0].Rows[0]["EMRPastYear"]));
            GetSREDetail();
            BindData();
            RefreshSRE();
            HighRisk();
            ShowButton();

            lblMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "VendorEMR").ToString();
            imbOK1.Visible = true;
            imbOK.Visible = false;
            imbOK2.Visible = false;
            mpopAlert.Show();


        }
        else
        {
            trMessage.Style["display"] = "";
            lblemrmsg.Style["display"] = "";
            lblemrmsg.Text = Errormsg();
            mpopEMRAlert.Show();



        }
    }
    #endregion

    #region SRE Delete
    /// <summary>
    /// Delete SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbDeleteSRE_Click(object sender, ImageClickEventArgs e)
    {
        //if (Convert.ToBoolean(hdnIsOriginator.Value))
        //{
        riskEvaluation.DeleteSRE(riskId);
        align1.Align = "Center";
        lblMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "SREDeleted").ToString();
        if (imbOK.Visible)
            imbOK.Visible = false;
        if (imbOK1.Visible)
            imbOK1.Visible = false;
        if (!imbDeleteOk.Visible)
            imbDeleteOk.Visible = true;
        mpopAlert.Show();
        //}
    }
    protected void imbDeleteOk_Click(object sender, ImageClickEventArgs e)
    {
        imgback_Click(sender, e);
    }
    protected void imb_Click(object sender, ImageClickEventArgs e)
    {
        imbNotify.Visible = false;
        imb.Visible = false;
    }
    #endregion

    #endregion

    #region Gridview Events
    protected void rptDocument_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        switch (e.CommandName)
        {
            case "View":
                ShowDocument(e.CommandArgument.ToString());
                break;
            case "Delete":
                string[] value = e.CommandArgument.ToString().Split('^');

                riskEvaluation.DeleteSREDocument(Convert.ToInt32(value[0]));
                DeleteSREDocument(value[0]);
                BindDocument(0);
                break;
        }
        mpopAddDocument.Show();
    }

    protected void rptDocumentAdmin_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "View":
                ShowDocument(e.CommandArgument.ToString());
                break;
            case "Delete":
                string[] value = e.CommandArgument.ToString().Split('^');

                riskEvaluation.DeleteSREDocument(Convert.ToInt32(value[0]));
                DeleteSREDocument(value[0]);
                BindDocument(0);
                break;
        }
        mpopAddDocument.Show();
    }
    #endregion

    #region Other Methods
    /// <summary>
    /// loading project number
    /// </summary>
    private void LoadProjectNumber()
    {
        //txtProjectNumber.DataSource = new clsRating().GetProject();
        //txtProjectNumber.DataTextField = "ProjectNumber";
        //txtProjectNumber.();
    }
    /// <summary>
    /// Show/hide Action buttons
    /// </summary>
    private void ShowActionButton()
    {
        string role = Session["EmployeeRole"].ConvertToString();    //014
        switch (Convert.ToString(Session["Access"]))
        {
            case "1":
                if (role != clsAdminLogin.EmployeeRole.Admin_SRE.ToString())
                {
                    trAdminAction.Style["display"] = "";
                    trEmployeeAction.Style["display"] = "none";
                    
                }
                else
                {
                    trAdminAction.Style["display"] = "none";
                    trEmployeeAction.Style["display"] = "";
                    imbDeleteAdminSRE.Visible = true;   //014
                }

                break;

            default:
                trAdminAction.Style["display"] = "none";
                trEmployeeAction.Style["display"] = "";
                imbDeleteAdminSRE.Visible = false;  //014
                break;
        }
    }
    /// <summary>
    /// javascript for SRE
    /// </summary>
    private void AppendClientScript()
    {

        txtBidpackageNumber.Attributes.Add("onblur", "validateBidPackageNo()");
        //
        rdoRed.Attributes.Add("onclick", "javascript:showComment('1');");
        rdoYellow.Attributes.Add("onclick", "javascript:showComment('1')");
        rdoNoWarning.Attributes.Add("onclick", "javascript:showComment('2')");
        //
        //txtSubcontractorCurrentBid.Attributes.Add("onblur", "javascript:financialSection()");

        txtSubcontractorCurrentBid.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtSubcontractorCurrentBid.ClientID + "')");
        txtSubcontractorCurrentBid.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtSubcontractorCurrentBid.ClientID + "')");
        txtSubcontractorCurrentBid.Attributes.Add("onblur", "javascript:numberFormat(this.value,'','" + txtSubcontractorCurrentBid.ClientID + "');financialSection1('" + Lookup.Tables[0].Rows[0]["LargestMaxPercentage"].ToString() + "','" + Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"].ToString() + "');financialSection2('" + Lookup.Tables[0].Rows[0]["LowestbidPercentage"].ToString() + "');");



        txtNextLowestBid.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtNextLowestBid.ClientID + "')");
        txtNextLowestBid.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNextLowestBid.ClientID + "')");
        txtNextLowestBid.Attributes.Add("onblur", "javascript:numberFormat(this.value,'','" + txtNextLowestBid.ClientID + "');financialSection1('" + Lookup.Tables[0].Rows[0]["LargestMaxPercentage"].ToString() + "','" + Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"].ToString() + "');financialSection2('" + Lookup.Tables[0].Rows[0]["LowestbidPercentage"].ToString() + "');");


        rdoFinancial1Yes.Attributes.Add("onclick", "javascript:validateFinance();");
        rdoFinancial1No.Attributes.Add("onclick", "javascript:validateFinance();");
        rdoFinancial2Yes.Attributes.Add("onclick", "javascript:validateFinance();");
        rdoFinancial2No.Attributes.Add("onclick", "javascript:validateFinance();");
        rdoFinancial3Yes.Attributes.Add("onclick", "javascript:validateFinance();");
        rdoFinancial3No.Attributes.Add("onclick", "javascript:validateFinance();");
        rdoFinancial4Yes.Attributes.Add("onclick", "javascript:validateFinance();");
        rdoFinancial4No.Attributes.Add("onclick", "javascript:validateFinance();");
        rdoFinancial5Yes.Attributes.Add("onclick", "javascript:financialSection1('" + Lookup.Tables[0].Rows[0]["LargestMaxPercentage"].ToString() + "','" + Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"].ToString() + "');financialSection2('" + Lookup.Tables[0].Rows[0]["LowestbidPercentage"].ToString() + "');validateFinance();");
        rdoFinancial5No.Attributes.Add("onclick", "javascript:financialSection1('" + Lookup.Tables[0].Rows[0]["LargestMaxPercentage"].ToString() + "','" + Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"].ToString() + "');financialSection2('" + Lookup.Tables[0].Rows[0]["LowestbidPercentage"].ToString() + "');validateFinance();");




        //rdoFinancial5Yes.Attributes.Add("onclick", "javascript:validateFinance();");
        //rdoFinancial5No.Attributes.Add("onclick", "javascript:validateFinance();");
        //
        rdoInsurance1Yes.Attributes.Add("onclick", "javascript:validateInsurance();");
        rdoInsurance1No.Attributes.Add("onclick", "javascript:validateInsurance();");
        rdoInsurance2Yes.Attributes.Add("onclick", "javascript:validateInsurance();");
        rdoInsurance2No.Attributes.Add("onclick", "javascript:validateInsurance();");
        rdoInsurance3Yes.Attributes.Add("onclick", "javascript:validateInsurance();");
        rdoInsurance3No.Attributes.Add("onclick", "javascript:validateInsurance();");
        rdoInsurance4Yes.Attributes.Add("onclick", "javascript:validateInsurance();");
        rdoInsurance4No.Attributes.Add("onclick", "javascript:validateInsurance();");

        rdoReference1Yes.Attributes.Add("onclick", "javascript:validateReference();");
        rdoReference1No.Attributes.Add("onclick", "javascript:validateReference();");
        rdoReference2Yes.Attributes.Add("onclick", "javascript:validateReference();");
        rdoReference2No.Attributes.Add("onclick", "javascript:validateReference();");
        rdoReference3Yes.Attributes.Add("onclick", "javascript:validateReference();");
        rdoReference3No.Attributes.Add("onclick", "javascript:validateReference();");
        rdoReference3NA.Attributes.Add("onclick", "javascript:validateReference();");

        //
        rdoLicense1Yes.Attributes.Add("onclick", "javascript:checkRisk();");
        rdoLicense1No.Attributes.Add("onclick", "javascript:checkRisk();");
        //
        //chkHighRisk.Attributes.Add("onclick", "javascript:checkRiskIndicator(this,'" + chkHighRisk.ClientID + "','" + chkAverageRisk.ClientID + "','" + chkDisapproved.ClientID + "');");
        //chkAverageRisk.Attributes.Add("onclick", "javascript:checkRiskIndicator(this,'" + chkHighRisk.ClientID + "','" + chkAverageRisk.ClientID + "','" + chkDisapproved.ClientID + "');");
        //chkDisapproved.Attributes.Add("onclick", "javascript:checkRiskIndicator(this,'" + chkHighRisk.ClientID + "','" + chkAverageRisk.ClientID + "','" + chkDisapproved.ClientID + "');");

        txtAppSubContractorValue.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAppSubContractorValue.ClientID + "')");
        txtAppSubContractorValue.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtAppSubContractorValue.ClientID + "')");
        txtAppSubContractorValue.Attributes.Add("onblur", "javascript:numberFormat(this.value,'','" + txtAppSubContractorValue.ClientID + "');bondingSection();financialSection1('" + Lookup.Tables[0].Rows[0]["LargestMaxPercentage"].ToString() + "','" + Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"].ToString() + "');financialSection2('" + Lookup.Tables[0].Rows[0]["LowestbidPercentage"].ToString() + "');validateFinance();ToggleDOCSignature(this.value);");   //015
        //txtAppSubContractorValue.Attributes.Add("onkeypress", "javascript:financialSection1('" + Lookup.Tables[0].Rows[0]["LargestMaxPercentage"].ToString() + "','" + Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"].ToString() + "');financialSection2('" + Lookup.Tables[0].Rows[0]["LowestbidPercentage"].ToString() + "');validateFinance();");




        txtRateOne.Attributes.Add("onBlur", "javascript:numberRateFormat(this.value,'','" + txtRateOne.ClientID + "');validateRateRange('" + txtRateOne.ClientID + "','one')");
        txtRateTwo.Attributes.Add("onBlur", "javascript:numberRateFormat(this.value,'','" + txtRateTwo.ClientID + "');validateRateRange('" + txtRateTwo.ClientID + "','two')");
        txtRateThree.Attributes.Add("onBlur", "javascript:numberRateFormat(this.value,'','" + txtRateThree.ClientID + "');validateRateRange('" + txtRateThree.ClientID + "','three')");


        txtRateOne.Attributes.Add("onkeypress", "javascript:numberEMRFormat(this.value,'','" + txtRateOne.ClientID + "')");
        txtRateTwo.Attributes.Add("onkeypress", "javascript:numberEMRFormat(this.value,'','" + txtRateTwo.ClientID + "')");
        txtRateThree.Attributes.Add("onkeypress", "javascript:numberEMRFormat(this.value,'','" + txtRateThree.ClientID + "')");



        txtMaxContractValueYearOne.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearOne.ClientID + "')");
        txtMaxContractValueYearOne.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearOne.ClientID + "')");

        txtAnuualCompRevenueYearOne.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearOne.ClientID + "')");
        txtAnuualCompRevenueYearOne.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearOne.ClientID + "')");

        txtMaxContractValueYearTwo.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearTwo.ClientID + "')");
        txtMaxContractValueYearTwo.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearTwo.ClientID + "')");

        txtAnuualCompRevenueYearTwo.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearTwo.ClientID + "')");
        txtAnuualCompRevenueYearTwo.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearTwo.ClientID + "')");

        txtMaxContractValueYearThree.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearThree.ClientID + "')");
        txtMaxContractValueYearThree.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearThree.ClientID + "')");

        txtAnuualCompRevenueYearThree.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearThree.ClientID + "')");
        txtAnuualCompRevenueYearThree.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearThree.ClientID + "')");


    }
    /// <summary>
    /// Bind the SRE 
    /// </summary>
    private void BindData()
    {
        string yearsInBusiness = string.Empty;
        bool? legal1 = null, legal2 = null, legal3 = null, legalLitigation = null;
        bool? safetyProgram = null, safetyRequirement = null, companySafety = null;
        if (vendorId.Equals(0))
            return;
        SREDataSet = riskEvaluation.GetSelectedRiskDetail(riskId);
        Boolean isEdit = IsEdittable();
        if (isEdit)
        {
            txtDate.Text = DateTime.Now.ToString();
            SaveVendorDetailForSRE();
        }
        var vendor = riskEvaluation.GetVendorDetail(vendorId);
        //014 - START
        string conductsHazardAnalysis = string.Empty; 
        var safety = new clsSafety().GetSingleVQFSafetyData(vendor.PK_VendorID);
        if (safety != null)
        {
            conductsHazardAnalysis = (safety.IsConductHazardAnalysis != null) ?
                (((bool)safety.IsConductHazardAnalysis) ? "Vendor conducts Job Hazard Analysis or Task Hazard Analysis on a " +
                                                          safety.ConductHazardAnalysisPeriods.Replace("|", ", ").Replace("?", "") + " basis.<br/><i>(Not included in SRE calculation)</i>"
                                                          : "Vendor does not conduct Job Hazard Analysis or Task Hazard Analysis.<br/><i>(Not included in SRE calculation)</i>") : string.Empty; 
        }

        ltrlHazardAnalysis.Text = conductsHazardAnalysis;
        //014 - END

        IsNonUSVendor = !vendor.IsBasedInUS;//007
        txtVendorName.Text = vendor.Company;
        Session["VendorNameEmailNotification"] = vendor.Email;
        txtVendorContactName.Text = vendor.FirstName + " " + vendor.LastName;
        DataSet vendorDataSet = riskEvaluation.GetSelectedRiskDetail(riskId);

        if (vendorDataSet.Tables.Count > 0)
        {
            LoadRiskDetails(vendorDataSet);
            txtVendorName.Enabled = false;

            if (vendorDataSet.Tables[0].Rows[0]["SREModifiedDate"].ToString() == "")
                txtDate.Text = DateTime.Now.ToString();
            else
                txtDate.Text = vendorDataSet.Tables[0].Rows[0]["SREModifiedDate"].ToString();

            txtVendorQualificationDate.Text = vendorDataSet.Tables[0].Rows[0]["VendorQualificationDate"].ToString();
            txtVendorQualificationDate.Enabled = false;

            if ((!vendorDataSet.Tables[0].Rows[0].IsNull("YearsInBusiness")) && (!string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[0].Rows[0]["YearsInBusiness"]))))
            {
                yearsInBusiness = Convert.ToString(vendorDataSet.Tables[0].Rows[0]["YearsInBusiness"]);
                if (Convert.ToInt32(vendorDataSet.Tables[0].Rows[0]["YearsInBusiness"]) < Convert.ToInt16(Lookup.Tables[0].Rows[0]["BusinessYear"]))
                {
                    rdoBusiness1Yes.Checked = true;
                    rdoBusiness1No.Checked = false;
                }
                else
                {
                    rdoBusiness1No.Checked = true;
                    rdoBusiness1Yes.Checked = false;
                }
            }

            txtVendorContactName.Text = vendorDataSet.Tables[0].Rows[0]["contactName"].ToString();
            if (txtVendorContactName.Text == "")
            {
                txtVendorContactName.Text = vendor.FirstName + " " + vendor.LastName;
            }

            legal1 = vendorDataSet.Tables[0].Rows[0].IsNull("VQFLegal1") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[0].Rows[0]["VQFLegal1"]);
            legal2 = vendorDataSet.Tables[0].Rows[0].IsNull("VQFLegal2") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[0].Rows[0]["VQFLegal2"]);
            legal3 = vendorDataSet.Tables[0].Rows[0].IsNull("VQFLegal3") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[0].Rows[0]["VQFLegal3"]);
            //014
            legalLitigation = vendorDataSet.Tables[0].Rows[0].IsNull("VQFLegalAnyLitigation") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[0].Rows[0]["VQFLegalAnyLitigation"]);


            /*** EMR Rates ***/
            //006
            if (vendorDataSet.Tables[2] != null && vendorDataSet.Tables[2].Rows.Count>0)
            {
               bool skipCurrentEMR = (vendorDataSet.Tables[2].Rows[0]["NotAvailable"].ToString().ToUpper() == "TRUE" ||
                                        string.IsNullOrEmpty(vendorDataSet.Tables[2].Rows[0]["EMRRate"] as string)) ? true : false;

                if (!skipCurrentEMR)
                {
                    txtRateOne.Text = vendorDataSet.Tables[2].Rows[0]["EMRRate"].ToString();
                    txtRateTwo.Text = vendorDataSet.Tables[2].Rows[1]["EMRRate"].ToString();
                    txtRateThree.Text = vendorDataSet.Tables[2].Rows[2]["EMRRate"].ToString();

                    txtRate1.Text = vendorDataSet.Tables[2].Rows[0]["EMRRate"].ToString();
                    txtRate2.Text = vendorDataSet.Tables[2].Rows[1]["EMRRate"].ToString();
                    txtRate3.Text = vendorDataSet.Tables[2].Rows[2]["EMRRate"].ToString();



                    lblYearDisp1.Text = "(" + vendorDataSet.Tables[2].Rows[0]["EMRYear"].ToString() + ")";
                    lblYearDisp2.Text = "(" + vendorDataSet.Tables[2].Rows[1]["EMRYear"].ToString() + ")";
                    lblYearDisp3.Text = "(" + vendorDataSet.Tables[2].Rows[2]["EMRYear"].ToString() + ")";

                    //010 - fix - remove the unnecesary row since the safety calculation method uses the dataset instead of the text box.
                    if(vendorDataSet.Tables[2].Rows.Count == 4) vendorDataSet.Tables[2].Rows.RemoveAt(3);
                }
                else
                {
                    txtRateOne.Text = vendorDataSet.Tables[2].Rows[1]["EMRRate"].ToString();
                    txtRateTwo.Text = vendorDataSet.Tables[2].Rows[2]["EMRRate"].ToString();
                    txtRateThree.Text = (vendorDataSet.Tables[2].Rows.Count == 4) ? vendorDataSet.Tables[2].Rows[3]["EMRRate"].ToString() : "";

                    txtRate1.Text = vendorDataSet.Tables[2].Rows[1]["EMRRate"].ToString();
                    txtRate2.Text = vendorDataSet.Tables[2].Rows[2]["EMRRate"].ToString();
                    txtRate3.Text = (vendorDataSet.Tables[2].Rows.Count == 4) ? vendorDataSet.Tables[2].Rows[3]["EMRRate"].ToString() : "";

                    lblYearDisp1.Text = "(" + vendorDataSet.Tables[2].Rows[1]["EMRYear"].ToString() + ")";
                    lblYearDisp2.Text = "(" + vendorDataSet.Tables[2].Rows[2]["EMRYear"].ToString() + ")";
                    lblYearDisp3.Text = "(" + ((vendorDataSet.Tables[2].Rows.Count == 4) ? vendorDataSet.Tables[2].Rows[3]["EMRYear"].ToString() : (Convert.ToInt32(vendorDataSet.Tables[2].Rows[2]["EMRYear"].ToString()) - 1).ToString()) + ")";

                    //010 - fix - remove the unnecesary row since the safety calculation method uses the dataset instead of the text box.
                    vendorDataSet.Tables[2].Rows.RemoveAt(0);
                }
            }

            //006 - modified this
            if (txtRate1.Text == "")
            {
                txtRate1.Text = "N/A";
            }
            if (txtRate2.Text == "")
            {
                txtRate2.Text = "N/A";
            }
            if (txtRate3.Text == "")
            {
                txtRate3.Text = "N/A";
            }

            //End for 006

            
            //008 - year 2 - 4 if 1 is not available
            bool useFinancialYear4 = Convert.ToBoolean(vendorDataSet.Tables[1].Rows[0]["NotAvailable"].ToString().ToLower() == "true" || string.IsNullOrEmpty(vendorDataSet.Tables[1].Rows[0]["AnnualRevenue"] as string));
            if (!useFinancialYear4)
            {
                txtLegalYearOne.Text = vendorDataSet.Tables[1].Rows[0]["Year"].ToString();
                txtLegalYearTwo.Text = vendorDataSet.Tables[1].Rows[1]["Year"].ToString();
                txtLegalYearThree.Text = vendorDataSet.Tables[1].Rows[2]["Year"].ToString();

                txtAnuualCompRevenueYearOne.Text = vendorDataSet.Tables[1].Rows[0]["AnnualRevenue"].ToString();
                txtAnuualCompRevenueYearTwo.Text = vendorDataSet.Tables[1].Rows[1]["AnnualRevenue"].ToString();
                txtAnuualCompRevenueYearThree.Text = vendorDataSet.Tables[1].Rows[2]["AnnualRevenue"].ToString();
            }
            else
            {
                txtLegalYearOne.Text = vendorDataSet.Tables[1].Rows[1]["Year"].ToString();
                txtLegalYearTwo.Text = vendorDataSet.Tables[1].Rows[2]["Year"].ToString();
                txtLegalYearThree.Text = (vendorDataSet.Tables[1].Rows.Count == 4) ? vendorDataSet.Tables[1].Rows[3]["Year"].ToString() : "";

                txtAnuualCompRevenueYearOne.Text = vendorDataSet.Tables[1].Rows[1]["AnnualRevenue"].ToString();
                txtAnuualCompRevenueYearTwo.Text = vendorDataSet.Tables[1].Rows[2]["AnnualRevenue"].ToString();
                txtAnuualCompRevenueYearThree.Text = (vendorDataSet.Tables[1].Rows.Count == 4) ? vendorDataSet.Tables[1].Rows[3]["AnnualRevenue"].ToString() : "";
            }
            //008 - END


            txtMaxContractValueYearOne.Text = vendorDataSet.Tables[1].Rows[0]["ContractValue"].ToString();
            //011 - incorrect: txtMaxContractValueYearTwo.Text = vendorDataSet.Tables[1].Rows[2]["ContractValue"].ToString();
            txtMaxContractValueYearTwo.Text = vendorDataSet.Tables[1].Rows[1]["ContractValue"].ToString();
            txtMaxContractValueYearThree.Text = vendorDataSet.Tables[1].Rows[2]["ContractValue"].ToString();


            if (txtMaxContractValueYearOne.Text != "" && txtMaxContractValueYearTwo.Text != "" && txtMaxContractValueYearThree.Text != "" &&
                txtAnuualCompRevenueYearOne.Text != "" && txtAnuualCompRevenueYearTwo.Text != "" && txtAnuualCompRevenueYearThree.Text != "")
                imbViewLegal.Visible = false;
            else
                imbViewLegal.Visible = false;


            if (txtRateOne.Text != "" && txtRateTwo.Text != "" && txtRateThree.Text != "")
                imbViewEMR.Visible = false;
            else
                imbViewEMR.Visible = false;

            if (txtRateOne.Text == "")
                txtRateOne.ReadOnly = false;
            if (txtRateTwo.Text == "")
                txtRateTwo.ReadOnly = false;
            if (txtRateThree.Text == "")
                txtRateThree.ReadOnly = false;



            SetBusiness(rdoBusiness2Yes, rdoBusiness2No, vendorDataSet, "VQFLegal1");
            SetBusiness(rdoBusiness3Yes, rdoBusiness3No, vendorDataSet, "VQFLegal2");
            SetBusiness(rdoBusiness4Yes, rdoBusiness4No, vendorDataSet, "VQFLegal3");
            //014 - START
            SetBusiness(rdoBusinessLitigationYes, rdoBusinessLitigationNo, vendorDataSet, "VQFLegalAnyLitigation");
            //GV 05242018: Added this to handle Design Consultants (Dianne states that this was throwing DCs as high risk in Safety before the
            //new questions.
            if (safety != null) //only Design consultants will have a null Safety object.
            { 
                SetBusiness(rdoSafetyCitationYes, rdoSafetyCitationNo, vendorDataSet, "SafetyCitation");
            }
            else
            {
                rdoSafetyCitationNo.Checked = true;
                rdoSafety2Yes.Checked = true;
                rdoSafety1No.Checked = true;
            }
            //014 - END




            txtCurrentTotalBacklog.Text = Convert.ToString(vendorDataSet.Tables[0].Rows[0]["CurrentBacklog"]);
            txtCurrentTotalBacklog.Enabled = false;

            double annualRevenue = 0;

            //011 - new way of calculating Max Contract
            double contract1 = 0.00;
            double.TryParse(txtMaxContractValueYearOne.Text.Replace(",", ""), out contract1);
            double contract2 = 0.00;
            double.TryParse(txtMaxContractValueYearTwo.Text.Replace(",", ""), out contract2);
            double contract3 = 0.00;
            double.TryParse(txtMaxContractValueYearThree.Text.Replace(",", ""), out contract3);
            double[] contracts = { contract1, contract2, contract3 };
            double maxContract = contracts.Max();
            //011 - END

            #region 011 - commented unneeded code for future reference
            //int start = (useFinancialYear4) ? 1: 0;     //008
            //for (int count = start; count < vendorDataSet.Tables[1].Rows.Count; count++)    //008
            //{
            //    if ((!vendorDataSet.Tables[1].Rows[count].IsNull("AnnualRevenue")) && (!string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[1].Rows[count]["AnnualRevenue"]))))
            //    {
            //        annualRevenue = annualRevenue + Convert.ToDouble(vendorDataSet.Tables[1].Rows[count]["AnnualRevenue"]);
            //        countRevenue = countRevenue + 1;
            //    }
            //    if ((!vendorDataSet.Tables[1].Rows[count].IsNull("ContractValue")) && (!string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[1].Rows[count]["ContractValue"]))))
            //    {
            //        if (maxContract <= Convert.ToDouble(vendorDataSet.Tables[1].Rows[count]["ContractValue"]))
            //        {
            //            maxContract = Convert.ToDouble(vendorDataSet.Tables[1].Rows[count]["ContractValue"]);
            //        }
            //        // maxContract = maxContract + Convert.ToDouble(vendorDataSet.Tables[1].Rows[count]["ContractValue"]);
            //    }
            //}

            //if (countRevenue == 0)
            //{
            //    countRevenue = 1;
            //}
            #endregion

            //011 - new way of calculating
            var q = vendorDataSet.Tables[1].AsEnumerable().Where<DataRow>(dr => dr["AnnualRevenue"] != "");
            int countRevenue = (q.Count() > 3) ? 3 : q.Count();

            //int yr1 = txtAnuualCompRevenueYearOne.Text.Replace(",", "").ConvertToInteger();
            //int yr2 = txtAnuualCompRevenueYearTwo.Text.Replace(",", "").ToString().ConvertToInteger();
            //int yr3 = txtAnuualCompRevenueYearThree.Text.Replace(",", "").ToString().ConvertToInteger();
            
            //013 - need to use double instead of int since we will get large numbers
            double yr1 = 0;
            double.TryParse(txtAnuualCompRevenueYearOne.Text.Replace(",", ""), out yr1);
            double yr2 = 0;
            double.TryParse(txtAnuualCompRevenueYearTwo.Text.Replace(",", ""), out yr2);
            double yr3 = 0;
            double.TryParse(txtAnuualCompRevenueYearThree.Text.Replace(",", ""), out yr3);
            annualRevenue = (yr1 + yr2 + yr3);
            
            //011 - not needed: txtAverageAnnualRevenue.Text = (annualRevenue / countRevenue).ToString();  
            //013 do not divide 0 by 0 and round
            txtAverageAnnualRevenue.Text = (countRevenue != 0 && annualRevenue != 0) ? string.Format("{0:n0}", Math.Round(annualRevenue / countRevenue)) : "0";
            txtAverageAnnualRevenue.Enabled = false;
            //011 - not needed: txtMaxContract3Year.Text = maxContract.ToString();
            txtMaxContract3Year.Text = string.Format("{0:n0}", maxContract);

            txtMaxContract3Year.Enabled = false;

            if(vendorDataSet.Tables[0].Rows.Count>0) //007 -fixes
                SafetySection(vendorDataSet);

           

            safetyProgram = vendorDataSet.Tables[0].Rows[0].IsNull("SafetyProgram") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[0].Rows[0]["SafetyProgram"]);
            safetyRequirement = vendorDataSet.Tables[0].Rows[0].IsNull("SafetyRequirements") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[0].Rows[0]["SafetyRequirements"]);
            companySafety = vendorDataSet.Tables[0].Rows[0].IsNull("CompanySafety") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[0].Rows[0]["CompanySafety"]);

            if ((!vendorDataSet.Tables[0].Rows[0].IsNull("AvailableBondingCapacity")) && (!string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[0].Rows[0]["AvailableBondingCapacity"]))))
            {
                txtAvailBondingCapacity.Text = vendorDataSet.Tables[0].Rows[0]["AvailableBondingCapacity"].ToString();
                txtAvailBondingCapacity.Enabled = false;
            }

            txtBcomments.Text = vendorDataSet.Tables[0].Rows[0]["BusinessComments"].ToString();

            txtFcomments.Text = vendorDataSet.Tables[0].Rows[0]["FinanceComments"].ToString();
            //SGS Changes on 08/11/2011
            txtLcomments.Text = vendorDataSet.Tables[0].Rows[0]["LicenseComments"].ToString();
            //
            txtScomments.Text = vendorDataSet.Tables[0].Rows[0]["SaftyComments"].ToString();
            //SGS Changes on 08/11/2011
            txtIcomments.Text = vendorDataSet.Tables[0].Rows[0]["InsuranceComments"].ToString();
            //
            txtRcomments.Text = vendorDataSet.Tables[0].Rows[0]["ReferenceComments"].ToString();
            //SGS Changes on 08/11/2011
            txtRiskcomments.Text = vendorDataSet.Tables[0].Rows[0]["Comments"].ToString();
            ////
            //004
            txtRiskMgmntComments.Text = vendorDataSet.Tables[0].Rows[0]["RiskManagementComments"].ToString();

            vendorDataSet.Dispose();

        }

        if ((!string.IsNullOrEmpty(txtCurrentTotalBacklog.Text)) && (!string.IsNullOrEmpty(txtAverageAnnualRevenue.Text)))
        {
            if (Convert.ToDecimal(txtCurrentTotalBacklog.Text) > Convert.ToDecimal(txtAverageAnnualRevenue.Text))
            {
                rdoFinancial2Yes.Checked = true;
                rdoFinancial2No.Checked = false;
            }
            else
            {
                rdoFinancial2No.Checked = true;
                rdoFinancial2Yes.Checked = false;
            }
        }
        else
        {
            rdoFinancial2No.Checked = true;
            rdoFinancial2Yes.Checked = false;
        }
        rdoFinancial2No.Enabled = rdoFinancial2Yes.Enabled = false;

        if (isEdit)
        {
            riskEvaluation.UpdateSRE(riskId, txtBidpackageNumber.Text, 2, userId, (byte)statusId, string.Empty);
        }
    }

    /// <summary>
    /// 007 - Display label for Non US Vendors that have rate as N/A
    /// </summary>
    private void CheckIfNonUSAndApplyLabel()
    {
        if (IsNonUSVendor)
            if (txtRate1.Text == "N/A" && txtRate2.Text == "N/A" && txtRate3.Text == "N/A")
            {
                trInternationalVendor.Style["display"] = "";
                rdoSafety1No.Checked = true;
                rdoSafety1Yes.Checked = false;
            }
    }



    /// <summary>
    /// Bind the business section Radio buttons
    /// </summary>
    /// <param name="radioButton1"></param>
    /// <param name="radioButton2"></param>
    /// <param name="dataSet"></param>
    /// <param name="columnName"></param>
    /// 014 - Added optional parameter
    private void SetBusiness(RadioButton radioButton1, RadioButton radioButton2, DataSet dataSet, string columnName, bool enabled = false)
    {
        if ((!dataSet.Tables[0].Rows[0].IsNull(columnName)) && (!string.IsNullOrEmpty(Convert.ToString(dataSet.Tables[0].Rows[0][columnName]))))
        {
            radioButton1.Checked = Convert.ToBoolean(dataSet.Tables[0].Rows[0][columnName]);
            radioButton2.Checked = !Convert.ToBoolean(dataSet.Tables[0].Rows[0][columnName]);
        }
        else
        {
            //014 - not right: radioButton2.Checked = true;
        }
        radioButton1.Enabled = enabled;
        radioButton2.Enabled = enabled;
    }
    /// <summary>
    /// Bind the Safety section
    /// </summary>
    /// <param name="vendorDataSet"></param>
    private void SafetySection(DataSet vendorDataSet)
    {
        for (int count = 0; count < vendorDataSet.Tables[2].Rows.Count; count++)
        {
            if ((!vendorDataSet.Tables[2].Rows[count].IsNull("EMRRate")) && (!string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[2].Rows[count]["EMRRate"]))) && (Convert.ToDecimal(vendorDataSet.Tables[2].Rows[count]["EMRRate"]) > Convert.ToDecimal(Lookup.Tables[0].Rows[0]["EMRValue"])))
            {
                rdoSafety1Yes.Checked = true;
                rdoSafety1No.Checked = false;
                break;
            }
            else
            {
                rdoSafety1No.Checked = true;
                rdoSafety1Yes.Checked = false;
            }

         
            // if ((vendorDataSet.Tables[2].Rows[0].IsNull("EMRRate").ToString() == "N/A") || (vendorDataSet.Tables[2].Rows[1].IsNull("EMRRate").ToString() == "N/A") || (vendorDataSet.Tables[2].Rows[2].IsNull("EMRRate").ToString() == "N/A"))

            if (txtRate1.Text == "N/A" || txtRate2.Text == "N/A" || txtRate3.Text == "N/A")
            {
                rdoSafety1No.Checked = false;
                rdoSafety1Yes.Checked = true;
            }
           
        }
        rdoSafety1Yes.Enabled = rdoSafety1No.Enabled = false;

        //014 - not sure why this is here since these radio boxes have been handled already in the LoadRiskDetail() method.
        //if (vendorDataSet.Tables[0].Rows.Count > 0)
        //{
        //    if ((!vendorDataSet.Tables[0].Rows[0].IsNull("SafetyProgram") && !vendorDataSet.Tables[0].Rows[0].IsNull("SafetyRequirements") 
        //        && !vendorDataSet.Tables[0].Rows[0].IsNull("CompanySafety")) && ((!vendorDataSet.Tables[0].Rows[0].IsNull("SafetyRequirements") 
        //        && !Convert.ToBoolean(vendorDataSet.Tables[0].Rows[0]["SafetyRequirements"])) || (!vendorDataSet.Tables[0].Rows[0].IsNull("SafetyProgram") 
        //        && !Convert.ToBoolean(vendorDataSet.Tables[0].Rows[0]["SafetyProgram"])) ||(!vendorDataSet.Tables[0].Rows[0].IsNull("CompanySafety") 
        //        && !Convert.ToBoolean(vendorDataSet.Tables[0].Rows[0]["CompanySafety"]))))
        //    {
        //        rdoSafety2Yes.Checked = true;
        //        rdoSafety2No.Checked = false;
        //    }
        //    else
        //    {
        //        rdoSafety2No.Checked = true;
        //        rdoSafety2Yes.Checked = false;
        //    }            
        //}
        //014 - END


        rdoSafety2Yes.Enabled = rdoSafety2No.Enabled = false;

        CheckIfNonUSAndApplyLabel(); //007 Sooraj To modify

        //014 - change high risk flag for safety section
        SetSafetyRiskIndicator();
    }

    /// <summary>
    /// 014 - Created
    /// </summary>
    private void SetSafetyRiskIndicator()
    {
        int safetyCnt = 0;
        if (rdoSafety2Yes.Checked)
        {
            safetyCnt++;
        }
        if (rdoSafety1Yes.Checked)
        {
            safetyCnt++;
        }
        if (rdoSafetyCitationYes.Checked)
        {
            safetyCnt++;
        }

        chkSafetyRiskIndicator.Checked = (safetyCnt >= 1);
        chkSafetyRiskIndicator.Enabled = false;
    }
    /// <summary>
    /// Display the buttons according to the user and process status
    /// </summary>
    private void ShowButton()
    {


        switch (statusId)
        {
            //in progress
            case 0:
                if (lockedBy == null)
                {
                    imbSave.Visible = true;
                    imbComplete.Visible = true;
                    imbCalculate.Visible = true;


                    AccordionPane1.Enabled = true;
                    AccordionPane2.Enabled = true;
                    AccordionPane3.Enabled = true;
                    AccordionPane4.Enabled = true;
                    AccordionPane5.Enabled = true;
                    AccordionPane6.Enabled = true;
                    AccordionPane7.Enabled = true;
                }
                else if (lockedBy.Value.Equals(userId))
                {
                    imbSave.Visible = true;
                    imbComplete.Visible = true;
                    imbCalculate.Visible = true;
                    AccordionPane1.Enabled = true;
                    AccordionPane2.Enabled = true;
                    AccordionPane3.Enabled = true;
                    AccordionPane4.Enabled = true;
                    AccordionPane5.Enabled = true;
                    AccordionPane6.Enabled = true;
                    AccordionPane7.Enabled = true;
                }
                else
                {
                    imbSave.Visible = false;
                    imbComplete.Visible = false;
                    imbCalculate.Visible = false;
                    //SGS Changes on 08/11/2011
                    imbUpload1.ImageUrl = "~/Images/attach_documentnew.jpg";
                    imbUpload1.ToolTip = "Attach Document";
                    //
                    AccordionPane1.Enabled = false;
                    AccordionPane2.Enabled = false;
                    AccordionPane3.Enabled = false;
                    AccordionPane4.Enabled = false;
                    AccordionPane5.Enabled = false;
                    AccordionPane6.Enabled = false;
                    AccordionPane7.Enabled = false;
                    txtBidpackageDesc.Enabled = false;
                    txtAppSubContractorValue.Enabled = false;
                    txtVendorContactName.Enabled = false;
                }
                imbAwarded.Visible = false;
                imbNotAwarded.Visible = true; //004
                imbUnlockSRE.Visible = false;
                imbUnlockAdminSRE.Visible = false;  //014
                break;
            case 1:
            case 3:
                if ((editStatus.Equals(0)) || (editStatus.Equals(2) && lockedBy.Value.Equals(userId)))
                {
                    imbComplete.Visible = true;
                    AccordionPane1.Enabled = true;
                    AccordionPane2.Enabled = true;
                    AccordionPane3.Enabled = true;
                    AccordionPane4.Enabled = true;
                    AccordionPane5.Enabled = true;
                    AccordionPane6.Enabled = true;
                    AccordionPane7.Enabled = true;
                }
                else
                {
                    imbComplete.Visible = false;
                    AccordionPane1.Enabled = false;
                    AccordionPane2.Enabled = false;
                    AccordionPane3.Enabled = false;
                    AccordionPane4.Enabled = false;
                    AccordionPane5.Enabled = false;
                    AccordionPane6.Enabled = false;
                    AccordionPane7.Enabled = false;
                    txtBidpackageDesc.Enabled = false;
                    txtAppSubContractorValue.Enabled = false;
                    txtVendorContactName.Enabled = false;

                }
                imbSave.Visible = true;
                imbSubmit.Visible = false;
                imbAwarded.Visible = false;  
                imbNotAwarded.Visible = false;
                imbUnlockSRE.Visible = false;
                imbUnlockAdminSRE.Visible = false;  //014
                imbCalculate.Visible = true;
                break;
            case 2:
                string role = Session["EmployeeRole"].ConvertToString();    //014
                if (Convert.ToString(Session["Access"]).Equals("1") && role != clsAdminLogin.EmployeeRole.Admin_SRE.ToString())
                {
                    imbUnlockSRE.Visible = true;
                    imbAwarded.Visible = false;
                    imbNotAwarded.Visible = false;
                    imbSave.Visible = false;
                    imbSubmit.Visible = false;
                    imbViewEMR.Visible = false;
                    imbViewLegal.Visible = false;
                    imbComplete.Visible = false;
                    imbCalculate.Visible = false;
                    //SGS Changes on 08/11/2011
                    imbUpload1.ImageUrl = "~/Images/attach_documentnew.jpg";
                    imbUpload1.ToolTip = "Attach Document";
                    //
                    //AccordionPane1.Enabled = false;
                    //AccordionPane2.Enabled = false;
                    //AccordionPane3.Enabled = false;
                    //AccordionPane4.Enabled = false;
                    //AccordionPane5.Enabled = false;
                    //AccordionPane6.Enabled = false;
                    //AccordionPane7.Enabled = false;

                    //Sgs Changes on 08/22/2011 - First Section
                    txtBidpackageDesc.Enabled = false;
                    txtAppSubContractorValue.Enabled = false;
                    txtVendorContactName.Enabled = false;
                    //

                    //Sgs Changes on 08/22/2011 - Business / Legal Section 
                    //AccordionPane1.Enabled = false;                    
                    txtBcomments.ReadOnly = true;
                    //


                    //Sgs Changes on 08/22/2011 - Financial Section 
                    //AccordionPane2.Enabled = false;
                    rdoFinancial5No.Enabled = false;
                    rdoFinancial5Yes.Enabled = false;
                    txtSubcontractorCurrentBid.ReadOnly = true;
                    txtNextLowestBid.ReadOnly = true;
                    txtFcomments.ReadOnly = true;
                    //

                    //Sgs Changes on 08/22/2011 - Licensing Section 
                    //AccordionPane3.Enabled = false;
                    rdoLicense1Yes.Enabled = false;
                    rdoLicense1No.Enabled = false;
                    txtVerificationDate.ReadOnly = true;
                    imgVerficationDate.Enabled = false;
                    txtPMNotes.ReadOnly = true;
                    txtLcomments.ReadOnly = true;
                    ///

                    //Sgs Changes on 08/22/2011 - Safety Section 
                    //AccordionPane4.Enabled = false;
                    rdoSafety1Yes.Enabled = false;
                    rdoSafety1No.Enabled = false;
                    txtRate1.ReadOnly = true;
                    txtRate2.ReadOnly = true;
                    txtRate3.ReadOnly = true;
                    rdoSafety2Yes.Enabled = false;
                    rdoSafety2No.Enabled = false;
                    txtScomments.ReadOnly = true;
                    //


                    //Sgs Changes on 08/22/2011 - Insurance & Bonding Section 
                    //AccordionPane5.Enabled = false;
                    rdoInsurance1Yes.Enabled = false;
                    rdoInsurance1No.Enabled = false;
                    rdoInsurance2Yes.Enabled = false;
                    rdoInsurance2No.Enabled = false;
                    rdoInsurance3Yes.Enabled = false;
                    rdoInsurance3No.Enabled = false;
                    rdoInsurance4Yes.Enabled = false;
                    rdoInsurance4No.Enabled = false;
                    txtIcomments.ReadOnly = true;
                    //

                    //Sgs Changes on 08/22/2011 - Reference Section 
                    //AccordionPane6.Enabled = false;
                    txtReferenceDate1.ReadOnly = true;
                    txtReferenceDate2.ReadOnly = true;
                    txtReferenceDate3.ReadOnly = true;


                    txtReferenceDate1.ReadOnly = true;
                    txtReferenceDate2.ReadOnly = true;
                    txtReferenceDate3.ReadOnly = true;

                    imgReferenceDate1.Enabled = false;
                    imgReferenceDate2.Enabled = false;
                    imgReferenceDate3.Enabled = false;


                    txtSupplierReference1.ReadOnly = true;
                    txtSupplierReference2.ReadOnly = true;
                    txtSupplierReference3.ReadOnly = true;

                    txtSupplierDate1.ReadOnly = true;
                    txtSupplierDate2.ReadOnly = true;
                    txtSupplierDate3.ReadOnly = true;

                    imgSupplierDate1.Enabled = false;
                    imgSupplierDate2.Enabled = false;
                    imgSupplierDate3.Enabled = false;

                    rdoReference1Yes.Enabled = false;
                    rdoReference1No.Enabled = false;
                    rdoReference2Yes.Enabled = false;
                    rdoReference2No.Enabled = false;
                    rdoReference3Yes.Enabled = false;
                    rdoReference3No.Enabled = false;
                    txtRcomments.ReadOnly = true;

                    //Sgs Changes on 08/22/2011 - Risk Determination 
                    //AccordionPane7.Enabled = false;
                    txtRiskcomments.ReadOnly = true;
                    txtRiskMgmntComments.ReadOnly = true;   //004
                    //
                }
                else
                {
                    imbAwarded.Visible = true;
                    imbNotAwarded.Visible = true;
                    imbSave.Visible = false;
                    imbSubmit.Visible = false;
                    imbComplete.Visible = false;
                    imbViewEMR.Visible = false;
                    imbViewLegal.Visible = false;
                    imbUnlockSRE.Visible = false;
                    imbUnlockAdminSRE.Visible = (role == clsAdminLogin.EmployeeRole.Admin_SRE.ToString());  //014, GV 02/21/2018
                    imbCalculate.Visible = false;
                    //SGS Changes on 08/11/2011
                    imbUpload1.ImageUrl = "~/Images/attach_documentnew.jpg";
                    imbUpload1.ToolTip = "Attach Document";
                    //

                    //Sgs Changes on 08/22/2011 - First Section
                    txtBidpackageDesc.Enabled = false;
                    txtAppSubContractorValue.Enabled = false;
                    txtVendorContactName.Enabled = false;
                    //

                    //Sgs Changes on 08/22/2011 - Business / Legal Section 
                    //AccordionPane1.Enabled = false;                    
                    txtBcomments.ReadOnly = true;
                    //


                    //Sgs Changes on 08/22/2011 - Financial Section 
                    //AccordionPane2.Enabled = false;
                    rdoFinancial5No.Enabled = false;
                    rdoFinancial5Yes.Enabled = false;
                    txtSubcontractorCurrentBid.ReadOnly = true;
                    txtNextLowestBid.ReadOnly = true;
                    txtFcomments.ReadOnly = true;
                    //

                    //Sgs Changes on 08/22/2011 - Licensing Section 
                    //AccordionPane3.Enabled = false;
                    rdoLicense1Yes.Enabled = false;
                    rdoLicense1No.Enabled = false;
                    txtVerificationDate.ReadOnly = true;
                    imgVerficationDate.Enabled = false;
                    txtPMNotes.ReadOnly = true;
                    txtLcomments.ReadOnly = true;
                    ///

                    //Sgs Changes on 08/22/2011 - Safety Section 
                    //AccordionPane4.Enabled = false;
                    rdoSafety1Yes.Enabled = false;
                    rdoSafety1No.Enabled = false;
                    txtRate1.ReadOnly = true;
                    txtRate2.ReadOnly = true;
                    txtRate3.ReadOnly = true;
                    rdoSafety2Yes.Enabled = false;
                    rdoSafety2No.Enabled = false;
                    txtScomments.ReadOnly = true;
                    //


                    //Sgs Changes on 08/22/2011 - Insurance & Bonding Section 
                    //AccordionPane5.Enabled = false;
                    rdoInsurance1Yes.Enabled = false;
                    rdoInsurance1No.Enabled = false;
                    rdoInsurance2Yes.Enabled = false;
                    rdoInsurance2No.Enabled = false;
                    rdoInsurance3Yes.Enabled = false;
                    rdoInsurance3No.Enabled = false;
                    rdoInsurance4Yes.Enabled = false;
                    rdoInsurance4No.Enabled = false;
                    txtIcomments.ReadOnly = true;
                    //

                    //Sgs Changes on 08/22/2011 - Reference Section 
                    //AccordionPane6.Enabled = false;
                    txtReferenceDate1.ReadOnly = true;
                    txtReferenceDate2.ReadOnly = true;
                    txtReferenceDate3.ReadOnly = true;


                    txtReferenceDate1.ReadOnly = true;
                    txtReferenceDate2.ReadOnly = true;
                    txtReferenceDate3.ReadOnly = true;

                    imgReferenceDate1.Enabled = false;
                    imgReferenceDate2.Enabled = false;
                    imgReferenceDate3.Enabled = false;


                    txtSupplierReference1.ReadOnly = true;
                    txtSupplierReference2.ReadOnly = true;
                    txtSupplierReference3.ReadOnly = true;

                    txtSupplierDate1.ReadOnly = true;
                    txtSupplierDate2.ReadOnly = true;
                    txtSupplierDate3.ReadOnly = true;

                    imgSupplierDate1.Enabled = false;
                    imgSupplierDate2.Enabled = false;
                    imgSupplierDate3.Enabled = false;

                    rdoReference1Yes.Enabled = false;
                    rdoReference1No.Enabled = false;
                    rdoReference2Yes.Enabled = false;
                    rdoReference2No.Enabled = false;
                    rdoReference3Yes.Enabled = false;
                    rdoReference3No.Enabled = false;
                    txtRcomments.ReadOnly = true;

                    //Sgs Changes on 08/22/2011 - Risk Determination 
                    //AccordionPane7.Enabled = false;
                    txtRiskcomments.ReadOnly = true;
                    txtRiskMgmntComments.ReadOnly = true;   //004
                    //


                }

                break;
            case 4:
                //014
                if (Session["Access"].ToString() == "1")
                {
                    imbUnlockSRE.Visible = true;
                    imbUnlockAdminSRE.Visible = true;  //014
                    imbDeleteSRE.Visible = true;
                }
                imbAward.Visible = false;   //014
                imbAwarded.Visible = false;  //014
                break;
            case 5:
                imbAwarded.Visible = false;
                imbNotAwarded.Visible = false;
                imbSave.Visible = false;
                imbSubmit.Visible = false;
                imbComplete.Visible = false;
                imbUnlockSRE.Visible = false;
                imbUnlockAdminSRE.Visible = false;  //014
                imbCalculate.Visible = false;
                //SGS Changes on 08/11/2011
                imbUpload1.ImageUrl = "~/Images/attach_documentnew.jpg";
                imbUpload1.ToolTip = "Attach Document";
                //
                //AccordionPane1.Enabled = false;
                //AccordionPane2.Enabled = false;
                //AccordionPane3.Enabled = false;
                //AccordionPane4.Enabled = false;
                //AccordionPane5.Enabled = false;
                //AccordionPane6.Enabled = false;
                //AccordionPane7.Enabled = false;
                //Sgs Changes on 08/22/2011 - First Section
                txtBidpackageDesc.Enabled = false;
                txtAppSubContractorValue.Enabled = false;
                txtVendorContactName.Enabled = false;
                //

                //Sgs Changes on 08/22/2011 - Business / Legal Section 
                //AccordionPane1.Enabled = false;                    
                txtBcomments.ReadOnly = true;
                //


                //Sgs Changes on 08/22/2011 - Financial Section 
                //AccordionPane2.Enabled = false;
                rdoFinancial5No.Enabled = false;
                rdoFinancial5Yes.Enabled = false;
                txtSubcontractorCurrentBid.ReadOnly = true;
                txtNextLowestBid.ReadOnly = true;
                txtFcomments.ReadOnly = true;
                //

                //Sgs Changes on 08/22/2011 - Licensing Section 
                //AccordionPane3.Enabled = false;
                rdoLicense1Yes.Enabled = false;
                rdoLicense1No.Enabled = false;
                txtVerificationDate.ReadOnly = true;
                imgVerficationDate.Enabled = false;
                txtPMNotes.ReadOnly = true;
                txtLcomments.ReadOnly = true;
                ///

                //Sgs Changes on 08/22/2011 - Safety Section 
                //AccordionPane4.Enabled = false;
                rdoSafety1Yes.Enabled = false;
                rdoSafety1No.Enabled = false;
                txtRate1.ReadOnly = true;
                txtRate2.ReadOnly = true;
                txtRate3.ReadOnly = true;
                rdoSafety2Yes.Enabled = false;
                rdoSafety2No.Enabled = false;
                txtScomments.ReadOnly = true;
                //


                //Sgs Changes on 08/22/2011 - Insurance & Bonding Section 
                //AccordionPane5.Enabled = false;
                rdoInsurance1Yes.Enabled = false;
                rdoInsurance1No.Enabled = false;
                rdoInsurance2Yes.Enabled = false;
                rdoInsurance2No.Enabled = false;
                rdoInsurance3Yes.Enabled = false;
                rdoInsurance3No.Enabled = false;
                rdoInsurance4Yes.Enabled = false;
                rdoInsurance4No.Enabled = false;
                txtIcomments.ReadOnly = true;
                //

                //Sgs Changes on 08/22/2011 - Reference Section 
                //AccordionPane6.Enabled = false;
                txtReferenceDate1.ReadOnly = true;
                txtReferenceDate2.ReadOnly = true;
                txtReferenceDate3.ReadOnly = true;


                txtReferenceDate1.ReadOnly = true;
                txtReferenceDate2.ReadOnly = true;
                txtReferenceDate3.ReadOnly = true;

                imgReferenceDate1.Enabled = false;
                imgReferenceDate2.Enabled = false;
                imgReferenceDate3.Enabled = false;


                txtSupplierReference1.ReadOnly = true;
                txtSupplierReference2.ReadOnly = true;
                txtSupplierReference3.ReadOnly = true;

                txtSupplierDate1.ReadOnly = true;
                txtSupplierDate2.ReadOnly = true;
                txtSupplierDate3.ReadOnly = true;

                imgSupplierDate1.Enabled = false;
                imgSupplierDate2.Enabled = false;
                imgSupplierDate3.Enabled = false;

                rdoReference1Yes.Enabled = false;
                rdoReference1No.Enabled = false;
                rdoReference2Yes.Enabled = false;
                rdoReference2No.Enabled = false;
                rdoReference3Yes.Enabled = false;
                rdoReference3No.Enabled = false;
                txtRcomments.ReadOnly = true;

                //Sgs Changes on 08/22/2011 - Risk Determination 
                //AccordionPane7.Enabled = false;
                txtRiskcomments.ReadOnly = true;
                txtRiskMgmntComments.ReadOnly = true;   //004
                //
                txtBidpackageDesc.Enabled = false;
                txtAppSubContractorValue.Enabled = false;
                txtVendorContactName.Enabled = false;

                break;
            default:
                if ((editStatus.Equals(0)) || ((editStatus.Equals(2)) && (lockedBy != null && lockedBy.Value.Equals(userId))))
                {
                    imbSave.Visible = true;
                    imbComplete.Visible = true;
                    imbCalculate.Visible = true;
                    //AccordionPane1.Enabled = true;
                    //AccordionPane2.Enabled = true;
                    //AccordionPane3.Enabled = true;
                    //AccordionPane4.Enabled = true;
                    //AccordionPane5.Enabled = true;
                    //AccordionPane6.Enabled = true;
                    //AccordionPane7.Enabled = true;
                    //Sgs Changes on 08/22/2011 - First Section
                    txtBidpackageDesc.Enabled = false;
                    txtAppSubContractorValue.Enabled = false;
                    txtVendorContactName.Enabled = false;
                    //

                    //Sgs Changes on 08/22/2011 - Business / Legal Section 
                    //AccordionPane1.Enabled = false;                    
                    txtBcomments.ReadOnly = true;
                    //


                    //Sgs Changes on 08/22/2011 - Financial Section 
                    //AccordionPane2.Enabled = false;
                    rdoFinancial5No.Enabled = false;
                    rdoFinancial5Yes.Enabled = false;
                    txtSubcontractorCurrentBid.ReadOnly = true;
                    txtNextLowestBid.ReadOnly = true;
                    txtFcomments.ReadOnly = true;
                    //

                    //Sgs Changes on 08/22/2011 - Licensing Section 
                    //AccordionPane3.Enabled = false;
                    rdoLicense1Yes.Enabled = false;
                    rdoLicense1No.Enabled = false;
                    txtVerificationDate.ReadOnly = true;
                    imgVerficationDate.Enabled = false;
                    txtPMNotes.ReadOnly = true;
                    txtLcomments.ReadOnly = true;
                    ///

                    //Sgs Changes on 08/22/2011 - Safety Section 
                    //AccordionPane4.Enabled = false;
                    rdoSafety1Yes.Enabled = false;
                    rdoSafety1No.Enabled = false;
                    txtRate1.ReadOnly = true;
                    txtRate2.ReadOnly = true;
                    txtRate3.ReadOnly = true;
                    rdoSafety2Yes.Enabled = false;
                    rdoSafety2No.Enabled = false;
                    txtScomments.ReadOnly = true;
                    //


                    //Sgs Changes on 08/22/2011 - Insurance & Bonding Section 
                    //AccordionPane5.Enabled = false;
                    rdoInsurance1Yes.Enabled = false;
                    rdoInsurance1No.Enabled = false;
                    rdoInsurance2Yes.Enabled = false;
                    rdoInsurance2No.Enabled = false;
                    rdoInsurance3Yes.Enabled = false;
                    rdoInsurance3No.Enabled = false;
                    rdoInsurance4Yes.Enabled = false;
                    rdoInsurance4No.Enabled = false;
                    txtIcomments.ReadOnly = true;
                    //

                    //Sgs Changes on 08/22/2011 - Reference Section 
                    //AccordionPane6.Enabled = false;
                    txtReferenceDate1.ReadOnly = true;
                    txtReferenceDate2.ReadOnly = true;
                    txtReferenceDate3.ReadOnly = true;


                    txtReferenceDate1.ReadOnly = true;
                    txtReferenceDate2.ReadOnly = true;
                    txtReferenceDate3.ReadOnly = true;

                    imgReferenceDate1.Enabled = false;
                    imgReferenceDate2.Enabled = false;
                    imgReferenceDate3.Enabled = false;


                    txtSupplierReference1.ReadOnly = true;
                    txtSupplierReference2.ReadOnly = true;
                    txtSupplierReference3.ReadOnly = true;

                    txtSupplierDate1.ReadOnly = true;
                    txtSupplierDate2.ReadOnly = true;
                    txtSupplierDate3.ReadOnly = true;

                    imgSupplierDate1.Enabled = false;
                    imgSupplierDate2.Enabled = false;
                    imgSupplierDate3.Enabled = false;

                    rdoReference1Yes.Enabled = false;
                    rdoReference1No.Enabled = false;
                    rdoReference2Yes.Enabled = false;
                    rdoReference2No.Enabled = false;
                    rdoReference3Yes.Enabled = false;
                    rdoReference3No.Enabled = false;
                    txtRcomments.ReadOnly = true;

                    //Sgs Changes on 08/22/2011 - Risk Determination 
                    //AccordionPane7.Enabled = false;
                    txtRiskcomments.ReadOnly = true;
                    txtRiskMgmntComments.ReadOnly = true;   //004
                    //
                }
                else
                {
                    imbSave.Visible = false;
                    imbSubmit.Visible = false;
                    imbComplete.Visible = false;
                    imbCalculate.Visible = false;
                    //SGS Changes on 08/11/2011
                    imbUpload1.ImageUrl = "~/Images/attach_documentnew.jpg";
                    imbUpload1.ToolTip = "Attach Document";
                    //
                    //AccordionPane1.Enabled = false;
                    //AccordionPane2.Enabled = false;
                    //AccordionPane3.Enabled = false;
                    //AccordionPane4.Enabled = false;
                    //AccordionPane5.Enabled = false;
                    //AccordionPane6.Enabled = false;
                    //AccordionPane7.Enabled = false;
                    //txtBidpackageDesc.Enabled = false;
                    txtAppSubContractorValue.Enabled = false;
                    txtVendorContactName.Enabled = false;
                    //Sgs Changes on 08/22/2011 - First Section
                    txtBidpackageDesc.Enabled = false;
                    txtAppSubContractorValue.Enabled = false;
                    txtVendorContactName.Enabled = false;
                    //

                    //Sgs Changes on 08/22/2011 - Business / Legal Section 
                    //AccordionPane1.Enabled = false;                    
                    txtBcomments.ReadOnly = true;
                    //


                    //Sgs Changes on 08/22/2011 - Financial Section 
                    //AccordionPane2.Enabled = false;
                    rdoFinancial5No.Enabled = false;
                    rdoFinancial5Yes.Enabled = false;
                    txtSubcontractorCurrentBid.ReadOnly = true;
                    txtNextLowestBid.ReadOnly = true;
                    txtFcomments.ReadOnly = true;
                    //

                    //Sgs Changes on 08/22/2011 - Licensing Section 
                    //AccordionPane3.Enabled = false;
                    rdoLicense1Yes.Enabled = false;
                    rdoLicense1No.Enabled = false;
                    txtVerificationDate.ReadOnly = true;
                    imgVerficationDate.Enabled = false;
                    txtPMNotes.ReadOnly = true;
                    txtLcomments.ReadOnly = true;
                    ///

                    //Sgs Changes on 08/22/2011 - Safety Section 
                    //AccordionPane4.Enabled = false;
                    rdoSafety1Yes.Enabled = false;
                    rdoSafety1No.Enabled = false;
                    txtRate1.ReadOnly = true;
                    txtRate2.ReadOnly = true;
                    txtRate3.ReadOnly = true;
                    rdoSafety2Yes.Enabled = false;
                    rdoSafety2No.Enabled = false;
                    txtScomments.ReadOnly = true;
                    //


                    //Sgs Changes on 08/22/2011 - Insurance & Bonding Section 
                    //AccordionPane5.Enabled = false;
                    rdoInsurance1Yes.Enabled = false;
                    rdoInsurance1No.Enabled = false;
                    rdoInsurance2Yes.Enabled = false;
                    rdoInsurance2No.Enabled = false;
                    rdoInsurance3Yes.Enabled = false;
                    rdoInsurance3No.Enabled = false;
                    rdoInsurance4Yes.Enabled = false;
                    rdoInsurance4No.Enabled = false;
                    txtIcomments.ReadOnly = true;
                    //

                    //Sgs Changes on 08/22/2011 - Reference Section 
                    //AccordionPane6.Enabled = false;
                    txtReferenceDate1.ReadOnly = true;
                    txtReferenceDate2.ReadOnly = true;
                    txtReferenceDate3.ReadOnly = true;


                    txtReferenceDate1.ReadOnly = true;
                    txtReferenceDate2.ReadOnly = true;
                    txtReferenceDate3.ReadOnly = true;

                    imgReferenceDate1.Enabled = false;
                    imgReferenceDate2.Enabled = false;
                    imgReferenceDate3.Enabled = false;


                    txtSupplierReference1.ReadOnly = true;
                    txtSupplierReference2.ReadOnly = true;
                    txtSupplierReference3.ReadOnly = true;

                    txtSupplierDate1.ReadOnly = true;
                    txtSupplierDate2.ReadOnly = true;
                    txtSupplierDate3.ReadOnly = true;

                    imgSupplierDate1.Enabled = false;
                    imgSupplierDate2.Enabled = false;
                    imgSupplierDate3.Enabled = false;

                    rdoReference1Yes.Enabled = false;
                    rdoReference1No.Enabled = false;
                    rdoReference2Yes.Enabled = false;
                    rdoReference2No.Enabled = false;
                    rdoReference3Yes.Enabled = false;
                    rdoReference3No.Enabled = false;
                    txtRcomments.ReadOnly = true;

                    //Sgs Changes on 08/22/2011 - Risk Determination 
                    //AccordionPane7.Enabled = false;
                    txtRiskcomments.ReadOnly = true;
                    txtRiskMgmntComments.ReadOnly = true;   //004
                    //

                }
                imbAwarded.Visible = false;
                imbNotAwarded.Visible = false;
                imbUnlockSRE.Visible = false;
                imbUnlockAdminSRE.Visible = false;  //014
                break;
        }
        
    }
    /// <summary>
    /// bind the SRE Details according to the riskid
    /// </summary>
    private void GetSREDetail()
    {

        var risk = riskEvaluation.GetSREDetail(riskId);
        Session["RISKIDLOGOUT"] = Convert.ToInt64(riskId);
        vendorId = risk.FK_VendorID;
        statusId = Convert.ToInt32(risk.StatusId);
        projectNumber = risk.ProjectNumber;
        editStatus = Convert.ToByte(risk.EditStatus);
        lockedBy = risk.LockedBy;
        txtProjectNumber.Text = risk.ProjectNumber;
        txtProjectName.Text = riskEvaluation.GetProjectName(projectNumber);
        txtProjectName.ReadOnly = true;
        txtProjectNumber.Enabled = false;
        txtBidpackageNumber.ReadOnly = true;

        //004
        if (risk.CreatedUserID == userId) hdnIsOriginator.Value = "true";
        else hdnIsOriginator.Value = "false";
        hdnOriginator.Value = new clsAdminLogin().GetEmployeeName(risk.CreatedBy);
        lblOriginator.Text = "Originated By: " + hdnOriginator.Value;
        //004
        if (Convert.ToString(Session["Access"]).Equals("1") && statusId != 4) imbDeleteSRE.Visible = true;
        else imbDeleteSRE.Visible = false;
    }
    /// <summary>
    /// Save the vendor details for SRE
    /// </summary>
    private void SaveVendorDetailForSRE()
    {
        string yearsInBusiness = string.Empty, vendorName = string.Empty, currentBackLog = string.Empty, availBondingCapacity = string.Empty;
        string averageAnnualRevenue = string.Empty, maxContract3Year = string.Empty;
        bool? legal1 = null, legal2 = null, legal3 = null, legalLitigation = null;
        bool? safetyProgram = null, safetyRequirement = null, companySafety = null, meetings = null, drugPolicy = null, wipInspections = null, isCitation = null;
        DateTime? vendorLastModifiedDT = null;

        DataSet vendorDataSet = riskEvaluation.GetVendorDetailsForSRE(vendorId, Convert.ToInt16(Lookup.Tables[0].Rows[0]["LargestMaxPastYear"]), Convert.ToInt16(Lookup.Tables[0].Rows[0]["EMRPastYear"]));
        if (vendorDataSet.Tables.Count > 0)
        {
            vendorName = vendorDataSet.Tables[0].Rows[0]["Name"].ToString();
            if (!vendorDataSet.Tables[0].Rows[0].IsNull("LastModifiedDT") && !string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[0].Rows[0]["LastModifiedDT"])))
            {
                vendorLastModifiedDT = Convert.ToDateTime(vendorDataSet.Tables[0].Rows[0]["LastModifiedDT"]);
            }

            if (vendorDataSet.Tables.Count > 1)
            {
                if ((vendorDataSet.Tables[1].Rows.Count > 0) && (!vendorDataSet.Tables[1].Rows[0].IsNull("YearsInBusiness")) && (!string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[1].Rows[0]["YearsInBusiness"]))))
                {
                    yearsInBusiness = Convert.ToString(vendorDataSet.Tables[1].Rows[0]["YearsInBusiness"]);
                }
            }
            if (vendorDataSet.Tables.Count > 2)
            {
                legal1 = vendorDataSet.Tables[2].Rows[0].IsNull("Legal1") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[2].Rows[0]["Legal1"]);
                //009 - legal2 = vendorDataSet.Tables[2].Rows[0].IsNull("Legal2") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[2].Rows[0]["Legal2"]);
                //009 - should be getting value from Legal3 column
                //014 - new legal question
                legalLitigation = vendorDataSet.Tables[2].Rows[0].IsNull("Legal2") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[2].Rows[0]["Legal2"]);
                legal2 = vendorDataSet.Tables[2].Rows[0].IsNull("Legal3") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[2].Rows[0]["Legal3"]);
                legal3 = vendorDataSet.Tables[2].Rows[0].IsNull("Legal4") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[2].Rows[0]["Legal4"]);
                currentBackLog = Convert.ToString(vendorDataSet.Tables[2].Rows[0]["CurrentTotalBackLog"]);
            }
            if (vendorDataSet.Tables.Count > 3)
            {
                //double annualRevenue = 0;
                //double maxContract = 0;
                //int countRevenue = 0;
                //for (int count = 0; count < vendorDataSet.Tables[3].Rows.Count; count++)
                //{
                //    if ((!vendorDataSet.Tables[3].Rows[count].IsNull("AnnualRevenue")) && (!string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[3].Rows[count]["AnnualRevenue"]))))
                //    {
                //        annualRevenue = annualRevenue + Convert.ToDouble(vendorDataSet.Tables[3].Rows[count]["AnnualRevenue"]);
                //        countRevenue = countRevenue + 1;
                //    }
                //    if ((!vendorDataSet.Tables[3].Rows[count].IsNull("ContractValue")) && (!string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[3].Rows[count]["ContractValue"]))))
                //    {
                //        maxContract = maxContract + Convert.ToDouble(vendorDataSet.Tables[3].Rows[count]["ContractValue"]);
                //    }
                //}
                //if (countRevenue == 0)
                //    countRevenue = 1;
                //averageAnnualRevenue = (Math.Round(annualRevenue / countRevenue)).ToString();

                //maxContract3Year = maxContract.ToString();

                //REMARKED OUT BY N SCHOENBERGER 11/02/2011 *** The logic here is INCORRECT ***
                //double annualRevenue = 0;
                //double maxContract = 0;
                //int countRevenue = 0;
                //for (int count = 0; count < vendorDataSet.Tables[1].Rows.Count; count++)
                //{
                //    if ((!vendorDataSet.Tables[3].Rows[count].IsNull("AnnualRevenue")) && (!string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[3].Rows[count]["AnnualRevenue"]))))
                //    {
                //        annualRevenue = annualRevenue + Convert.ToDouble(vendorDataSet.Tables[3].Rows[count]["AnnualRevenue"]);
                //        countRevenue = countRevenue + 1;
                //    }
                //    if ((!vendorDataSet.Tables[3].Rows[count].IsNull("ContractValue")) && (!string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[3].Rows[count]["ContractValue"]))))
                //    {
                //        if (maxContract <= Convert.ToDouble(vendorDataSet.Tables[3].Rows[count]["ContractValue"]))
                //        {
                //            maxContract = Convert.ToDouble(vendorDataSet.Tables[3].Rows[count]["ContractValue"]);
                //        }

                //    }
                //}
                //if (countRevenue == 0)
                //    countRevenue = 1;
                //averageAnnualRevenue = (Math.Round(annualRevenue / countRevenue)).ToString();

                //maxContract3Year = maxContract.ToString();
                //************************************************************

                //Added by N Schoenberger 11/02/2011 to replace incorrect logic found above ***************************
                double maxContract = 0;
                double annualRevenue = 0;
                //011 - modified logic to account for year 4
                bool useFinancialYear4 = Convert.ToBoolean(vendorDataSet.Tables[3].Rows[0]["NotAvailable"].ToString().ToLower() == "true" || string.IsNullOrEmpty(vendorDataSet.Tables[3].Rows[0]["AnnualRevenue"] as string));

                //011
                if (useFinancialYear4)
                {
                    if (vendorDataSet.Tables[3].Rows.Count > 3)
                        vendorDataSet.Tables[3].Rows.RemoveAt(0);
                }
                else
                {
                    if (vendorDataSet.Tables[3].Rows.Count > 3)
                        vendorDataSet.Tables[3].Rows.RemoveAt(3);
                }

                maxContract = Convert.ToDouble(vendorDataSet.Tables[3].Rows.Cast<DataRow>().Max(row => Convert.ToDouble(NullOrEmptyToZero(row["ContractValue"].ToString()))));
                maxContract3Year = string.Format("{0:n0}", maxContract);

                //011 - need to divide by the number of rows vendor has values on not 3
                var q = vendorDataSet.Tables[3].AsEnumerable().Where<DataRow>(dr => dr["AnnualRevenue"] != "");
                int countRevenue = (q.Count() > 3) ? 3 : q.Count();

                annualRevenue = Convert.ToDouble(vendorDataSet.Tables[3].Rows.Cast<DataRow>().Sum(row => Convert.ToDouble(NullOrEmptyToZero(row["AnnualRevenue"].ToString()))));
                //011: not right: averageAnnualRevenue = string.Format("{0:n0}", Math.Round(annualRevenue / 3));
                averageAnnualRevenue = (annualRevenue != 0 && countRevenue != 0) ? string.Format("{0:n0}", Math.Round(annualRevenue / countRevenue)) : "0";  //011, 013

                //*******************************************************************************************

            }

            if ((vendorDataSet.Tables.Count > 5) && (vendorDataSet.Tables[5].Rows.Count > 0))
            {
                safetyProgram = vendorDataSet.Tables[5].Rows[0].IsNull("SafetyProgram") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[5].Rows[0]["SafetyProgram"]);
                safetyRequirement = vendorDataSet.Tables[5].Rows[0].IsNull("SafetyRequirements") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[5].Rows[0]["SafetyRequirements"]);
                companySafety = vendorDataSet.Tables[5].Rows[0].IsNull("CompanySafety") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[5].Rows[0]["CompanySafety"]);
                //014 - START
                meetings = vendorDataSet.Tables[5].Rows[0].IsNull("IsPeriodicSafetyMeetings") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[5].Rows[0]["IsPeriodicSafetyMeetings"]);
                wipInspections = vendorDataSet.Tables[5].Rows[0].IsNull("IsSafetyInspectionOfWIP") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[5].Rows[0]["IsSafetyInspectionOfWIP"]);
                drugPolicy = vendorDataSet.Tables[5].Rows[0].IsNull("IsWrittenDrugAlcoholProgram") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[5].Rows[0]["IsWrittenDrugAlcoholProgram"]);
                isCitation = vendorDataSet.Tables[7].Rows[0].IsNull("IsCitation") ? new Nullable<bool>() : Convert.ToBoolean(vendorDataSet.Tables[7].Rows[0]["IsCitation"]);
                //014 - end
            }
            if (vendorDataSet.Tables.Count > 6 && vendorDataSet.Tables[6].Rows.Count > 0)
            {
                if ((!vendorDataSet.Tables[6].Rows[0].IsNull("AvailableBonding")) && (!string.IsNullOrEmpty(Convert.ToString(vendorDataSet.Tables[6].Rows[0]["AvailableBonding"]))))
                {
                    availBondingCapacity = vendorDataSet.Tables[6].Rows[0]["AvailableBonding"].ToString();
                }
            }
        }

        riskEvaluation.DeleteVQFDataForSRE(riskId);
        //014 - add new field
        //if any of these were answered No then flip to true,
        //else if all were answered Yes then set to false, 
        //else something is null leave it unanswered
        bool? safety2 = new Nullable<bool>();
        
        if (safetyProgram == false || safetyRequirement == false || companySafety == false || meetings == false || wipInspections == false || drugPolicy == false)
        {
            safety2 = true;
        }
        else if (safetyProgram == true && safetyRequirement == true && companySafety == true && meetings == true && wipInspections == true && drugPolicy == true)
        {
            safety2 = false;
        }
        else
        {
            //nothing, keep it null
        }

        riskEvaluation.SaveVQFDetailForSRE(riskId, vendorLastModifiedDT, yearsInBusiness, legal1, legal2, legal3, legalLitigation,drugPolicy,meetings,wipInspections,
                                            safetyProgram, safetyRequirement, companySafety, availBondingCapacity, averageAnnualRevenue,
                                            maxContract3Year, currentBackLog, userId, 0, isCitation, safety2);

        riskEvaluation.SaveVQFSafetyEMRRateForSRE(riskId, vendorDataSet.Tables[4]);
        riskEvaluation.SaveVQFLegalFinancialForSRE(riskId, vendorDataSet.Tables[3]);

    }
    /// <summary>
    /// check whether the SRE is in Editable mode
    /// </summary>
    /// <returns></returns>
    private Boolean IsEdittable()
    {
        string role = Session["EmployeeRole"].ConvertToString();    //014
        if (Convert.ToString(Session["Access"]).Equals("1") && role != clsAdminLogin.EmployeeRole.Admin_SRE.ToString())
        {
            return false;
        }
        else
        {
            if (statusId != 2)
            {
                if ((statusId.Equals(0) || statusId.Equals(1) || statusId.Equals(3) || statusId.Equals(2)) && (lockedBy == null || lockedBy.Value.Equals(Convert.ToInt32(Session["EmployeeId"]))))
                {
                    return true;
                }

            }
            else
            {
                riskEvaluation.UpdateCompleteLockStatus(riskId, userId, 2);
                return false;
            }
        }
        return false;
    }
    /// <summary>
    /// Bind the Risk Details
    /// </summary>
    /// <param name="vendorDataSet"></param>
    private void LoadRiskDetails(DataSet vendorDataSet)
    {
        if (vendorDataSet.Tables[0].Rows.Count > 0)
        {

            DataRow dataRow = vendorDataSet.Tables[0].Rows[0];
            txtBidpackageNumber.Text = dataRow["BidPackageNumber"].ToString();
            txtBidpackageDesc.Text = dataRow["BidPackageDescription"].ToString();
            txtAppSubContractorValue.Text = dataRow["AppSubcontractorValue"].ToString();

            CheckYesNoRadio(dataRow["Business1"], rdoBusiness1Yes, rdoBusiness1No);
            CheckYesNoRadio(dataRow["Business2"], rdoBusiness2Yes, rdoBusiness2No);
            CheckYesNoRadio(dataRow["Business3"], rdoBusiness3Yes, rdoBusiness3No);
            CheckYesNoRadio(dataRow["Business4"], rdoBusiness4Yes, rdoBusiness4No);
            CheckYesNoRadio(dataRow["Business5"], rdoBusinessLitigationYes, rdoBusinessLitigationNo);     //014
            CheckRiskIndicator(dataRow["BusinessRiskIndicator"], chkBusinessRiskIndicator);

            txtNextLowestBid.Text = dataRow["NextLowestBid"].ToString();
            txtSubcontractorCurrentBid.Text = dataRow["SubcontractorCurrentBid"].ToString();

            CheckYesNoRadio(dataRow["Financial1"], rdoFinancial1Yes, rdoFinancial1No);
            CheckYesNoRadio(dataRow["Financial2"], rdoFinancial2Yes, rdoFinancial2No);
            CheckYesNoRadio(dataRow["Financial3"], rdoFinancial3Yes, rdoFinancial3No);
            CheckYesNoRadio(dataRow["Financial4"], rdoFinancial4Yes, rdoFinancial4No);
            CheckYesNoRadio(dataRow["Financial5"], rdoFinancial5Yes, rdoFinancial5No);
            CheckRiskIndicator(dataRow["FinancialRiskIndicator"], chkFinancialRiskIndicator);

            if (dataRow["Financial3"] != null && dataRow["Financial3"].ToString() != "" && Convert.ToBoolean(dataRow["Financial3"]) == true)
            {

                hdnFinancial3.Value = "1";
            }
            else
            { hdnFinancial3.Value = "0"; }

            //hdnFinancial1.Value = dataRow["Financial1"].ToString();

            if (dataRow["Financial1"] != null && dataRow["Financial1"].ToString() != "" && Convert.ToBoolean(dataRow["Financial1"]) == true)
            {

                hdnFinancial1.Value = "1";
            }
            else
            { hdnFinancial1.Value = "0"; }


            if (dataRow["Financial4"] != null && dataRow["Financial4"].ToString() != "" && Convert.ToBoolean(dataRow["Financial4"]) == true)
            {

                hdnFinancial4.Value = "1";
            }
            else
            { hdnFinancial4.Value = "0"; }



            txtPMNotes.Text = dataRow["PMVerificationNotes"].ToString();
            txtVerificationDate.Text = dataRow["PMVerificationDate"].ToString();
            CheckYesNoRadio(dataRow["License"], rdoLicense1Yes, rdoLicense1No);

            CheckYesNoRadio(dataRow["Insurance1"], rdoInsurance1Yes, rdoInsurance1No);
            CheckYesNoRadio(dataRow["Insurance2"], rdoInsurance2Yes, rdoInsurance2No);
            CheckYesNoRadio(dataRow["Insurance3"], rdoInsurance3Yes, rdoInsurance3No);
            CheckYesNoRadio(dataRow["Insurance4"], rdoInsurance4Yes, rdoInsurance4No);
            CheckRiskIndicator(dataRow["InsuranceRiskIndicator"], chkInsuranceRiskIndicator);

            CheckYesNoRadio(dataRow["Safety1"], rdoSafety1Yes, rdoSafety1No);
            CheckYesNoRadio(dataRow["Safety2"], rdoSafety2Yes, rdoSafety2No);
            CheckYesNoRadio(dataRow["Safety3"], rdoSafetyCitationYes, rdoSafetyCitationNo);     //014
            CheckRiskIndicator(dataRow["SafetyRiskIndicator"], chkSafetyRiskIndicator);

            txtProjectReference1.Text = dataRow["ProjectReference1"].ToString();
            txtReferenceDate1.Text = dataRow["ProjectReference1DateCalled"].ToString();
            txtProjectReference2.Text = dataRow["ProjectReference2"].ToString();
            txtReferenceDate2.Text = dataRow["ProjectReference2DateCalled"].ToString();

            txtProjectReference3.Text = dataRow["ProjectReference3"].ToString();
            txtReferenceDate3.Text = dataRow["ProjectReference3DateCalled"].ToString();

            txtSupplierReference1.Text = dataRow["SupplierReference1"].ToString();
            txtSupplierDate1.Text = dataRow["SupplierReference1DateCalled"].ToString();
            txtSupplierReference2.Text = dataRow["SupplierReference2"].ToString();
            txtSupplierDate2.Text = dataRow["SupplierReference2DateCalled"].ToString();

            txtSupplierReference3.Text = dataRow["SupplierReference3"].ToString();
            txtSupplierDate3.Text = dataRow["SupplierReference3DateCalled"].ToString();

            CheckYesNoRadio(dataRow["Reference1"], rdoReference1Yes, rdoReference1No);
            CheckYesNoRadio(dataRow["Reference2"], rdoReference2Yes, rdoReference2No);
            CheckYesNoRadio(dataRow["Reference3"], rdoReference3Yes, rdoReference3No);

            //Added by N Schoenberger for "Not Applicable" functionality 10/20/2011
            CheckNotApplicable(dataRow["Reference3NA"], rdoReference3NA);
            CheckRiskIndicator(dataRow["ReferenceRiskIndicator"], chkReferenceRiskIndicator);

            //015: Is it >= 100,000?
            //016: Changed to 250,000
            isContractNeedDOCReview = (txtAppSubContractorValue.Text.Replace(",","").ConvertToInteger() >= DocApprovalTreshold);

            if (!dataRow.IsNull("RiskFlag"))
            {
                txtProjectManagerName.Text = dataRow["ProjectManagerName"].ToString();
                txtRiskDate1.Text = Convert.ToDateTime(dataRow["AvgDate"]).ToString("MM/dd/yyyy");




                switch (Convert.ToInt32(dataRow["RiskFlag"]))
                {
                    case 1:
                        chkAverageRisk.Checked = true;
                        txtProjectManagerName.Enabled = true;
                        txtRiskDate1.Enabled = true;
                        hdnrisk.Value = dataRow["RiskFlag"].ToString();
                        chkHighRisk.Checked = false;
                        chkDisapproved.Checked = false;
                        //015 - START
                        txtGroupName1.Text = (isContractNeedDOCReview) ? dataRow["GroupDOCName"].ToString() : "";
                        txtRiskDate2.Text = (isContractNeedDOCReview) ? dataRow["SREDate"].ToString() : "";
                        txtGroupName1.Enabled = isContractNeedDOCReview;
                        txtRiskDate2.Enabled = isContractNeedDOCReview;
                        //015 - END

                        break;
                    case 2:
                        chkHighRisk.Checked = true;
                        txtGroupName1.Text = dataRow["GroupDOCName"].ToString();
                        txtRiskDate2.Text = dataRow["SREDate"].ToString();
                        txtGroupName1.Enabled = true;
                        txtRiskDate2.Enabled = true;
                        hdnrisk.Value = dataRow["RiskFlag"].ToString();
                        chkAverageRisk.Checked = false;
                        chkDisapproved.Checked = false;
                        break;
                    case 3:
                        chkDisapproved.Checked = true;
                        txtGroupName2.Text = dataRow["GroupDOCName"].ToString();
                        txtRiskDate3.Text = dataRow["SREDate"].ToString();
                        txtGroupName1.Enabled = true;
                        txtRiskDate3.Enabled = true;
                        hdnrisk.Value = dataRow["RiskFlag"].ToString();
                        chkAverageRisk.Checked = false;
                        chkHighRisk.Checked = false;
                        break;
                }
            }
        }
    }
    /// <summary>
    /// Select the radiobutton according to the given value
    /// </summary>
    /// <param name="value"></param>
    /// <param name="rdoYes"></param>
    /// <param name="rdoNo"></param>
    private void CheckYesNoRadio(object value, RadioButton rdoYes, RadioButton rdoNo)
    {
        if (!value.Equals(DBNull.Value))
        {
            if (Convert.ToBoolean(value))
            {
                rdoYes.Checked = true;
                rdoNo.Checked = false;
            }
            else
            {
                rdoNo.Checked = true;
                rdoYes.Checked = false;
            }
        }
        else
        {
            //014 - bug fix
            rdoNo.Checked = false;
            rdoYes.Checked = false;
        }
    }

    private void CheckNotApplicable(object value, RadioButton rdoNotApplicable)
    {
        if (!value.Equals(DBNull.Value))
        {
            if (Convert.ToBoolean(value))
                rdoNotApplicable.Checked = true;
            else
                rdoNotApplicable.Checked = false;
        }
    }
    /// <summary>
    /// check the riskindicator checkbox according to the given value
    /// </summary>
    /// <param name="value"></param>
    /// <param name="chkRiskIndicator"></param>
    private void CheckRiskIndicator(object value, CheckBox chkRiskIndicator)
    {
        if (!value.Equals(DBNull.Value))
        {
            chkRiskIndicator.Checked = Convert.ToBoolean(value);
        }
    }
    /// <summary>
    /// check which radio button is checked
    /// </summary>
    /// <param name="rdoYes"></param>
    /// <param name="rdoNo"></param>
    /// <returns></returns>
    private Boolean? GetBoolean(RadioButton rdoYes, RadioButton rdoNo)
    {
        if (rdoYes.Checked)
            return true;
        else if (rdoNo.Checked)
            return false;
        else
            return new Nullable<Boolean>();
    }

    private Boolean? GetBoolean(RadioButton rdoNotApplicable)
    {
        if (rdoNotApplicable.Checked)
            return true;
        else
            return false;
    }
    /// <summary>
    /// check the textbox value is datetime or not
    /// </summary>
    /// <param name="dateTextBox"></param>
    /// <returns></returns>
    private DateTime? GetDateTime(TextBox dateTextBox)
    {
        if (!string.IsNullOrEmpty(dateTextBox.Text))
            return Convert.ToDateTime(dateTextBox.Text);
        return new Nullable<DateTime>();
    }
    /// <summary>
    /// compare the VQF with the existing SRE's VQF and display the modified VQF
    /// </summary>
    /// <param name="SREDataset"></param>
    /// <param name="VQFDataset"></param>
    /// <returns></returns>
    private string CompareSREVQF(DataSet SREDataset, DataSet VQFDataset)
    {
        string returnString = string.Empty;
        if (SREDataset.Tables[0].Rows[0]["YearsInBusiness"].ToString() != VQFDataset.Tables[1].Rows[0][0].ToString())
        {
            returnString += BusinessYear.InnerText + " is modified. </br>"; //Δ
        }
        if (SREDataset.Tables[0].Rows[0]["VQFLegal1"].ToString() != VQFDataset.Tables[2].Rows[0][0].ToString())
        {
            returnString += "Has the Subcontractor failed to complete awarded work or terminated for cause? is modified. </br>"; //Δ
        }
        if (SREDataset.Tables[0].Rows[0]["VQFLegal2"].ToString() != VQFDataset.Tables[2].Rows[0][2].ToString()) //014 changed Rows[0][1] ro [0][2]
        {
            returnString += "Has the Subcontractor had any judgments, bankruptcies, or reorganizations? is modified. </br>";//Δ
        }
        if (SREDataset.Tables[0].Rows[0]["VQFLegal3"].ToString() != VQFDataset.Tables[2].Rows[0][3].ToString())
        {
            returnString += "Any indictments or convictions of felony or other criminal conduct for key personnel? is modified. </br>";//Δ
        }
        //014
        if (SREDataset.Tables[0].Rows[0]["VQFLegalAnyLitigation"].ToString() != VQFDataset.Tables[2].Rows[0][1].ToString())
        {
            returnString += "Any of its Owners, Officers or Major Stockholders currently involved in any arbitration, litigation, suits or liens? is modified. </br>";//Δ
        }
        if (VQFDataset.Tables[3].Rows.Count == SREDataset.Tables[1].Rows.Count)
        {
            for (int i = 0; i < VQFDataset.Tables[3].Rows.Count; i++)
            {
                if (SREDataset.Tables[1].Rows[i]["ContractValue"].ToString() != VQFDataset.Tables[3].Rows[i]["ContractValue"].ToString())
                {
                    returnString += LargestMaxPastYear.InnerText + " is modified. </br>"; //Δ
                    break;
                }
            }
        }
        else
        {
            returnString += LargestMaxPastYear.InnerText + " </br>"; //Δ
        }
        if (SREDataset.Tables[0].Rows[0]["CurrentBackLog"].ToString() != VQFDataset.Tables[2].Rows[0][4].ToString())
        {
            returnString += "Current Total Backlog is modified. is modified. </br>";//Δ
        }
        if (VQFDataset.Tables[3].Rows.Count == SREDataset.Tables[1].Rows.Count)
        {
            for (int i = 0; i < VQFDataset.Tables[3].Rows.Count; i++)
            {
                if (SREDataset.Tables[1].Rows[i]["AnnualRevenue"].ToString() != VQFDataset.Tables[3].Rows[i]["AnnualRevenue"].ToString())
                {
                    returnString += AannualRevenuePastYear1.InnerText + " is modified. </br>"; //Δ
                    break;
                }
            }
        }
        else
        {
            returnString += AannualRevenuePastYear1.InnerText + " </br>"; //Δ
        }
        if (VQFDataset.Tables[4].Rows.Count == SREDataset.Tables[2].Rows.Count)
        {
            for (int i = 0; i < VQFDataset.Tables[4].Rows.Count; i++)
            {
                if (SREDataset.Tables[2].Rows[i]["EMRRate"].ToString() != VQFDataset.Tables[4].Rows[i]["EMRRate"].ToString())
                {
                    returnString += EMRPastYear.InnerText + " is modified. </br>"; //Δ
                    break;
                }
            }
        }
        else
        {
            returnString += EMRPastYear.InnerText + " is modified. </br>"; //Δ
        }
        if (VQFDataset.Tables[5].Rows.Count>0)
        if (SREDataset.Tables[0].Rows[0]["SafetyProgram"].ToString() != VQFDataset.Tables[5].Rows[0][1].ToString() || SREDataset.Tables[0].Rows[0]["SafetyRequirements"].ToString() != VQFDataset.Tables[5].Rows[0][2].ToString() || SREDataset.Tables[0].Rows[0]["CompanySafety"].ToString() != VQFDataset.Tables[5].Rows[0][3].ToString())
        {
            returnString += "Did Subcontractor answer 'NO' to any safety question on the VQF? is modified. </br>"; //Δ
        }
        if (VQFDataset.Tables[6].Rows.Count > 0 && SREDataset.Tables[0].Rows[0]["AvailableBondingCapacity"].ToString() != VQFDataset.Tables[6].Rows[0][0].ToString())
        {
            returnString += "Available bonding capacity is modified. </br>"; //Δ
        }
        return returnString;
    }


    ///// <summary>
    ///// REcalculate the SRE
    ///// </summary>
    //private void RefreshSRE()
    //{
    //    decimal annualRevenue = Convert.ToDecimal(txtAverageAnnualRevenue.Text.Replace(",", "").Trim() == "" ? "0" : txtAverageAnnualRevenue.Text.Replace(",", "").Trim());
    //    decimal maxContract = Convert.ToDecimal(txtMaxContract3Year.Text.Replace(",", "").Trim() == "" ? "0" : txtMaxContract3Year.Text.Replace(",", "").Trim());
    //    decimal currentBid = Convert.ToDecimal(txtSubcontractorCurrentBid.Text.Replace(",", "").Trim() == "" ? "0" : txtSubcontractorCurrentBid.Text.Replace(",", "").Trim());
    //    decimal nextLowestBid = Convert.ToDecimal(txtNextLowestBid.Text.Replace(",", "").Trim() == "" ? "0" : txtNextLowestBid.Text.Replace(",", "").Trim());
    //   decimal final= ((((nextLowestBid - currentBid)/nextLowestBid))* 100);

    //    if (currentBid > (annualRevenue + (Convert.ToDecimal(Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"]) * annualRevenue / 100)))
    //    {
    //        rdoFinancial3Yes.Checked = true;
    //        rdoFinancial3No.Checked = false;
    //    }
    //    else
    //    {
    //        rdoFinancial3No.Checked = true;
    //        rdoFinancial3Yes.Checked = false;
    //    }
    //    if (currentBid > (maxContract + (Convert.ToDecimal(Lookup.Tables[0].Rows[0]["LargestMaxPercentage"]) * maxContract / 100)))
    //    {
    //        rdoFinancial1Yes.Checked = true;
    //        rdoFinancial1No.Checked = false;
    //    }
    //    else
    //    {
    //        rdoFinancial1No.Checked = true;
    //        rdoFinancial1Yes.Checked = false;
    //    }

    //    //if (currentBid > (nextLowestBid + (Convert.ToDecimal(Lookup.Tables[0].Rows[0]["LowestbidPercentage"]) * nextLowestBid / 100)))
    //    if (currentBid > nextLowestBid)
    //    {
    //        rdoFinancial4Yes.Checked = false;
    //        rdoFinancial4No.Checked = true;
    //    }
    //    else if (final >= Convert.ToDecimal(Lookup.Tables[0].Rows[0]["LowestbidPercentage"]))
    //        {
    //            rdoFinancial4No.Checked = false;
    //            rdoFinancial4Yes.Checked = true;
    //        }
    //        else
    //        {
    //            rdoFinancial4No.Checked = true;
    //            rdoFinancial4Yes.Checked = false;
    //        }
    //    if (Convert.ToDecimal(txtAppSubContractorValue.Text.Replace(",", "").Trim() == "" ? "0" : txtAppSubContractorValue.Text.Replace(",", "").Trim()) > Convert.ToDecimal(txtAvailBondingCapacity.Text.Replace(",", "").Trim() == "" ? "0" : txtAvailBondingCapacity.Text.Replace(",", "").Trim()))
    //    {
    //        rdoInsurance4Yes.Checked = true;
    //        rdoInsurance4No.Checked = false;
    //    }
    //    else
    //    {
    //        rdoInsurance4No.Checked = true;
    //        rdoInsurance4Yes.Checked = false;
    //    }
    //    int riskCount = 0;
    //    if (rdoFinancial1Yes.Checked)
    //        riskCount = riskCount + 1;
    //    if (rdoFinancial2Yes.Checked)
    //        riskCount = riskCount + 1;
    //    if (rdoFinancial3Yes.Checked)
    //        riskCount = riskCount + 1;
    //    if (rdoFinancial4Yes.Checked)
    //        riskCount = riskCount + 1;
    //    if (rdoFinancial5Yes.Checked)
    //        riskCount = riskCount + 1;

    //    if (riskCount >= 2)
    //        chkFinancialRiskIndicator.Checked = true;

    //    riskCount = 0;
    //    if (chkBusinessRiskIndicator.Checked)
    //        ++riskCount;
    //    if (chkFinancialRiskIndicator.Checked)
    //        ++riskCount;
    //    if (chkInsuranceRiskIndicator.Checked)
    //        ++riskCount;
    //    if (chkReferenceRiskIndicator.Checked)
    //        ++riskCount;
    //    if (chkSafetyRiskIndicator.Checked)
    //        ++riskCount;




    //}




    /// <summary>
    /// REcalculate the SRE
    ///  Haskell -Calculation provided by Haskell 
    /// </summary>
    private void RefreshSRE()
    {
        string message = string.Empty;
        if (ValidateSRE(ref message))
        {
            decimal annualRevenue = Convert.ToDecimal(txtAverageAnnualRevenue.Text.Replace(",", "").Trim() == "" ? "0" : txtAverageAnnualRevenue.Text.Replace(",", "").Trim());
            decimal maxContract = Convert.ToDecimal(txtMaxContract3Year.Text.Replace(",", "").Trim() == "" ? "0" : txtMaxContract3Year.Text.Replace(",", "").Trim());
            decimal currentBid = Convert.ToDecimal(txtSubcontractorCurrentBid.Text.Replace(",", "").Trim() == "" ? "0" : txtSubcontractorCurrentBid.Text.Replace(",", "").Trim());
            decimal nextLowestBid = Convert.ToDecimal(txtNextLowestBid.Text.Replace(",", "").Trim() == "" ? "0" : txtNextLowestBid.Text.Replace(",", "").Trim());
            //002 decimal final = ((((nextLowestBid - currentBid) / nextLowestBid)) * 100);
            decimal final = (nextLowestBid == 0) ? (0 * 100) : ((((nextLowestBid - currentBid) / nextLowestBid)) * 100);    //002
            decimal percentOfAvgRev = (Convert.ToDecimal(Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"]) / 100) * annualRevenue;
            if (currentBid > percentOfAvgRev) //G. Vera - Bug Fix 04/29/2013 (annualRevenue + (Convert.ToDecimal(Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"]) * annualRevenue / 100)))
            {
                rdoFinancial3Yes.Checked = true;
                rdoFinancial3No.Checked = false;
            }
            else
            {
                rdoFinancial3No.Checked = true;
                rdoFinancial3Yes.Checked = false;
            }
            if (currentBid > (maxContract + (Convert.ToDecimal(Lookup.Tables[0].Rows[0]["LargestMaxPercentage"]) * maxContract / 100)))
            {
                rdoFinancial1Yes.Checked = true;
                rdoFinancial1No.Checked = false;
            }
            else
            {
                rdoFinancial1No.Checked = true;
                rdoFinancial1Yes.Checked = false;
            }
            //if (currentBid > (nextLowestBid + (Convert.ToDecimal(Lookup.Tables[0].Rows[0]["LowestbidPercentage"]) * nextLowestBid / 100)))
            if (currentBid > nextLowestBid)
            {
                rdoFinancial4Yes.Checked = false;
                rdoFinancial4No.Checked = true;
            }
            else if (final >= Convert.ToDecimal(Lookup.Tables[0].Rows[0]["LowestbidPercentage"]))
            {
                rdoFinancial4No.Checked = false;
                rdoFinancial4Yes.Checked = true;
            }
            else
            {
                rdoFinancial4No.Checked = true;
                rdoFinancial4Yes.Checked = false;
            }
            // G. Vera 06/21/2012 - Modified as the condition test was in reverse order (refer to javascrip function bondingSection())
            decimal subContractValue = Convert.ToDecimal(txtAppSubContractorValue.Text.Replace(",", "").Trim() == "" ? "0" : txtAppSubContractorValue.Text.Replace(",", "").Trim());
            decimal bondingCapacity = Convert.ToDecimal(txtAvailBondingCapacity.Text.Replace(",", "").Trim() == "" ? "0" : txtAvailBondingCapacity.Text.Replace(",", "").Trim());
            //G. Vera 06/21/2012 - Commented: if (Convert.ToDecimal(txtAppSubContractorValue.Text.Replace(",", "").Trim() == "" ? "0" : txtAppSubContractorValue.Text.Replace(",", "").Trim()) > Convert.ToDecimal(txtAvailBondingCapacity.Text.Replace(",", "").Trim() == "" ? "0" : txtAvailBondingCapacity.Text.Replace(",", "").Trim()))
            if (bondingCapacity > subContractValue)
            {
                rdoInsurance4Yes.Checked = true;
                rdoInsurance4No.Checked = false;
            }
            else
            {
                rdoInsurance4No.Checked = true;
                rdoInsurance4Yes.Checked = false;
            }
            int riskCount = 0;
            if (rdoFinancial1Yes.Checked)
                riskCount = riskCount + 1;
            if (rdoFinancial2Yes.Checked)
                riskCount = riskCount + 1;
            if (rdoFinancial3Yes.Checked)
                riskCount = riskCount + 1;
            if (rdoFinancial4Yes.Checked)
                riskCount = riskCount + 1;
            if (rdoFinancial5Yes.Checked)
                riskCount = riskCount + 1;
            if (riskCount >= 2)
                chkFinancialRiskIndicator.Checked = true;
            riskCount = 0;
            if (chkBusinessRiskIndicator.Checked)
                ++riskCount;
            if (chkFinancialRiskIndicator.Checked)
                ++riskCount;
            if (chkInsuranceRiskIndicator.Checked)
                ++riskCount;
            if (chkReferenceRiskIndicator.Checked)
                ++riskCount;
            if (chkSafetyRiskIndicator.Checked)
                ++riskCount;
        }
        else
        {
            lblMessage.Text = message;
            imbOK1.Visible = true;
            imbOK.Visible = false;
        }
        align1.Align = "Center";
        mpopAlert.Show();
    }

    /// <summary>
    /// check whether the radio button is checked or not
    /// </summary>
    /// <param name="rBtn"></param>
    /// <returns></returns>
    private int CheckRadio(RadioButton rBtn)
    {
        if (rBtn.Checked)
            return 1;
        else
            return 0;
    }
    /// <summary>
    /// check the checkbox based on the data selected
    /// </summary>
    /// <param name="Min"></param>
    /// <param name="chkBox"></param>
    /// <param name="rdoBtn"></param>
    private void CheckCeheckBox(int Min, CheckBox chkBox, params RadioButton[] rdoBtn)
    {
        int count = 0;
        for (int i = 0; i < rdoBtn.Length; i++)
        {
            count += CheckRadio(rdoBtn[i]);
        }
        if (count > Min)
            chkBox.Checked = true;
        else
            chkBox.Checked = false;
    }
    /// <summary>
    /// fix the HighRisk indicator
    /// </summary>
    private void HighRisk()
    {
        CheckCeheckBox(1, chkBusinessRiskIndicator, rdoBusiness1Yes, rdoBusiness2Yes, rdoBusiness3Yes, rdoBusiness4Yes, rdoBusinessLitigationYes);    //014
        CheckCeheckBox(1, chkFinancialRiskIndicator, rdoFinancial1Yes, rdoFinancial2Yes, rdoFinancial3Yes, rdoFinancial4Yes, rdoFinancial5Yes);
        CheckCeheckBox(0, chkSafetyRiskIndicator, rdoSafety1Yes, rdoSafety2Yes, rdoSafetyCitationYes);    //014
        CheckCeheckBox(1, chkInsuranceRiskIndicator, rdoInsurance1No, rdoInsurance2No, rdoInsurance3No, rdoInsurance4No);
        CheckCeheckBox(1, chkReferenceRiskIndicator, rdoReference1No, rdoReference2No, rdoReference3No);

    }
    /// <summary>
    /// Code to display the controls for Admin View
    /// </summary>
    /// <param name="Access"></param>
    private void AdminView(string Access)
    {
        string role = Session["EmployeeRole"].ConvertToString(); //014

        if (Access == "1" && role != clsAdminLogin.EmployeeRole.Admin_SRE.ToString())   //014
        {
            txtAppSubContractorValue.Enabled = false;
            txtBidpackageDesc.Enabled = false;
            rdoFinancial1Yes.Enabled = false;
            rdoFinancial1No.Enabled = false;
            txtSubcontractorCurrentBid.Enabled = false;
            rdoFinancial3Yes.Enabled = false;
            rdoFinancial3No.Enabled = false;
            rdoFinancial4Yes.Enabled = false;
            rdoFinancial4No.Enabled = false;
            txtNextLowestBid.Enabled = false;
            rdoFinancial5Yes.Enabled = false;
            rdoFinancial5No.Enabled = false;
            rdoLicense1Yes.Enabled = false;
            rdoLicense1No.Enabled = false;
            txtVerificationDate.Enabled = false;
            imgVerficationDate.Visible = false;
            txtPMNotes.Enabled = false;
            rdoInsurance1Yes.Enabled = false;
            rdoInsurance1No.Enabled = false;
            rdoInsurance2Yes.Enabled = false;
            rdoInsurance2No.Enabled = false;
            rdoInsurance3Yes.Enabled = false;
            rdoInsurance3No.Enabled = false;
            rdoInsurance4Yes.Enabled = false;
            rdoInsurance4No.Enabled = false;
            txtProjectReference1.Enabled = false;
            txtReferenceDate1.Enabled = false;
            imgReferenceDate1.Visible = false;
            txtProjectReference2.Enabled = false;
            txtReferenceDate2.Enabled = false;
            imgReferenceDate2.Visible = false;
            txtProjectReference3.Enabled = false;
            txtReferenceDate3.Enabled = false;
            imgReferenceDate3.Visible = false;
            txtSupplierReference1.Enabled = false;
            txtSupplierDate1.Enabled = false;
            imgSupplierDate1.Visible = false;
            txtSupplierReference2.Enabled = false;
            txtSupplierDate2.Enabled = false;
            imgSupplierDate2.Visible = false;
            txtSupplierReference3.Enabled = false;
            txtSupplierDate3.Enabled = false;
            imgSupplierDate3.Visible = false;
            rdoReference1Yes.Enabled = false;
            rdoReference1No.Enabled = false;
            rdoReference2Yes.Enabled = false;
            rdoReference2No.Enabled = false;
            rdoReference3Yes.Enabled = false;
            rdoReference3No.Enabled = false;
            chkAverageRisk.Enabled = false;
            txtProjectManagerName.Enabled = false;
            txtRiskDate1.Enabled = false;
            imgRiskDate1.Visible = false;
            chkHighRisk.Enabled = false;
            txtGroupName1.Enabled = false;
            txtRiskDate2.Enabled = false;
            imgRiskDate2.Visible = false;
            chkDisapproved.Enabled = false;
            txtGroupName2.Enabled = false;
            txtRiskDate3.Enabled = false;
            imgRiskDate3.Visible = false;
            //txtBcomments.Enabled = false;
            //txtFcomments.Enabled = false;
            //txtScomments.Enabled = false;
            //txtRcomments.Enabled = false;
            //txtLcomments.Enabled = false;
            //txtIcomments.Enabled = false;
        }



    }
    /// <summary>
    /// Method to Save the SRE
    /// </summary>
    private void imbSaveSubmit()
    {
        string riskVal = hdnrisk.Value;

        //bool a = rdoFinancial1Yes.Checked;
        //bool b = rdoFinancial1No.Checked;
        byte? overallRiskFlag = null;
        //SGS Changes on 08/11/2011
        bool InsuranceRiskIndicator, FinancialRiskIndicator, ReferenceRiskIndicator;
        //
        string projectManagerName = string.Empty, groupDOCName = string.Empty;
        DateTime? sreDate = new Nullable<DateTime>();
        projectManagerName = Session["EmployeeName"].ToString();
        //SGS Changes on 08/11/2011

        //G. VEra 04/29/2013 - fixed what SGS on top did of looking for the value on a hidden field that is never set in server code.
        if (hdnInsuranceRiskIndicator.Value == "1" || chkInsuranceRiskIndicator.Checked)
        {
            InsuranceRiskIndicator = true;

        }
        else
        {
            InsuranceRiskIndicator = false;
        }

        if (hdnFinancialRiskIndicator.Value == "1" || chkFinancialRiskIndicator.Checked)
        {
            FinancialRiskIndicator = true;

        }
        else
        {
            FinancialRiskIndicator = false;
        }
        if (hdnReferenceRiskIndicator.Value == "1" || chkReferenceRiskIndicator.Checked)
        {
            ReferenceRiskIndicator = true;

        }
        else
        {
            ReferenceRiskIndicator = false;
        }

        bool isContractNeedDOCReview = (txtAppSubContractorValue.Text.Replace(",", "").ConvertToInteger() >= DocApprovalTreshold);   //016

        if (hdnrisk.Value == "1")
        {
            overallRiskFlag = 1;
            groupDOCName = txtGroupName1.Text;

            if(isContractNeedDOCReview)
            {
                sreDate = !string.IsNullOrEmpty(txtRiskDate2.Text.Trim()) ? Convert.ToDateTime(txtRiskDate2.Text.Trim()) : new Nullable<DateTime>();
            }
            else
            {
                sreDate = !string.IsNullOrEmpty(txtRiskDate1.Text.Trim()) ? Convert.ToDateTime(txtRiskDate1.Text.Trim()) : new Nullable<DateTime>();
            }
            
        }
        else if (hdnrisk.Value == "2")
        {
            overallRiskFlag = 2;
            groupDOCName = txtGroupName1.Text;
            sreDate = !string.IsNullOrEmpty(txtRiskDate2.Text.Trim()) ? Convert.ToDateTime(txtRiskDate2.Text.Trim()) : new Nullable<DateTime>();
        }
        else if (hdnrisk.Value == "3")
        {
            overallRiskFlag = 3;
            groupDOCName = txtGroupName2.Text;
            sreDate = !string.IsNullOrEmpty(txtRiskDate3.Text.Trim()) ? Convert.ToDateTime(txtRiskDate3.Text.Trim()) : new Nullable<DateTime>();
        }

        SetSafetyRiskIndicator();

        riskEvaluation.UpdateBidPackage(riskId, txtBidpackageNumber.Text.Trim(), statusId, 0, userId);// new Nullable<Int64>()); 

        //SGS Changes on 08/11/2011
        //014
        riskEvaluation.UpdateSRE(riskId, txtVendorContactName.Text.Trim(), GetDateTime(txtDate), txtBidpackageDesc.Text,
                                txtAppSubContractorValue.Text, GetBoolean(rdoBusiness1Yes, rdoBusiness1No), GetBoolean(rdoBusiness2Yes, rdoBusiness2No), 
                                GetBoolean(rdoBusinessLitigationYes, rdoBusinessLitigationNo),
                                GetBoolean(rdoBusiness3Yes, rdoBusiness3No), GetBoolean(rdoBusiness4Yes, rdoBusiness4No), chkBusinessRiskIndicator.Checked,
                                txtSubcontractorCurrentBid.Text, txtMaxContract3Year.Text, txtCurrentTotalBacklog.Text, txtAverageAnnualRevenue.Text, txtNextLowestBid.Text, FinancialValue(hdnFinancial1),
                                GetBoolean(rdoFinancial2Yes, rdoFinancial2No), FinancialValue(hdnFinancial3), FinancialValue(hdnFinancial4),
                                GetBoolean(rdoFinancial5Yes, rdoFinancial5No), FinancialRiskIndicator,
                                txtPMNotes.Text, GetDateTime(txtVerificationDate), GetBoolean(rdoLicense1Yes, rdoLicense1No),
                                GetBoolean(rdoSafety1Yes, rdoSafety1No), GetBoolean(rdoSafety2Yes, rdoSafety2No), chkSafetyRiskIndicator.Checked,
                                GetBoolean(rdoInsurance1Yes, rdoInsurance1No), GetBoolean(rdoInsurance2Yes, rdoInsurance2No),
                                GetBoolean(rdoInsurance3Yes, rdoInsurance3No), FinancialValue(hdnInsurance), InsuranceRiskIndicator,
                                txtProjectReference1.Text, GetDateTime(txtReferenceDate1), txtProjectReference2.Text, GetDateTime(txtReferenceDate2),
                                txtProjectReference3.Text, GetDateTime(txtReferenceDate3),
                                txtSupplierReference1.Text, GetDateTime(txtSupplierDate1), txtSupplierReference2.Text, GetDateTime(txtSupplierDate2),
                                txtSupplierReference3.Text, GetDateTime(txtSupplierDate3),
                                GetBoolean(rdoReference1Yes, rdoReference1No), GetBoolean(rdoReference2Yes, rdoReference2No), GetBoolean(rdoReference3Yes, rdoReference3No), ReferenceRiskIndicator,
                                overallRiskFlag, projectManagerName, groupDOCName, sreDate, userId, (byte)statusId, GetDateTime(txtRiskDate1), txtBcomments.Text.Trim(),
                                txtFcomments.Text.Trim(), txtIcomments.Text.Trim(), txtLcomments.Text.Trim(), txtScomments.Text.Trim(), txtRcomments.Text.Trim(), 
                                txtRiskcomments.Text.Trim(), GetBoolean(rdoReference3NA), txtRiskMgmntComments.Text.Trim(), GetBoolean(rdoSafetyCitationYes, rdoSafetyCitationNo));  //004, 014
        ////

        //015 Save Risk Management Checklist
        SaveRiskManagement(riskId);
        SREDataSet = riskEvaluation.GetSelectedRiskDetail(riskId);
        HighRisk();
    }

    private void SaveRiskManagement(long riskId)
    {
        List<VMSDAL.SRERiskManagement> riskMgmtList = new List<SRERiskManagement>();
        VMSDAL.SRERiskManagement riskMgmt;

        //High Risk Determination selections
        foreach (ListItem chkitem in chkListHighRiskDet.Items)
        {
            if(chkitem.Selected)
            {
                riskMgmt = new SRERiskManagement();
                riskMgmt.FK_RiskID = riskId;
                riskMgmt.FK_RiskMgmtListID = Convert.ToInt64(chkitem.Value);
                //TODO riskMgmt.ModifiedBy
                riskMgmt.OtherText = (chkitem.Text == "Other") ? txtHighRiskDetOther.Text : "";
                //riskMgmt.CreatedBy = 
                riskMgmt.DateAdded = DateTime.Today;
                riskMgmt.DateMaint = DateTime.Today;

                riskMgmtList.Add(riskMgmt);
            }
        }

        //Average Risk Determination selections
        //016
        foreach (ListItem chkitem in chkListAvgRiskDet.Items)
        {
            if (chkitem.Selected)
            {
                riskMgmt = new SRERiskManagement();
                riskMgmt.FK_RiskID = riskId;
                riskMgmt.FK_RiskMgmtListID = Convert.ToInt64(chkitem.Value);
                //TODO riskMgmt.ModifiedBy
                riskMgmt.OtherText = (chkitem.Text == "Other") ? txtHighRiskDetOther.Text : "";
                //riskMgmt.CreatedBy = 
                riskMgmt.DateAdded = DateTime.Today;
                riskMgmt.DateMaint = DateTime.Today;

                riskMgmtList.Add(riskMgmt);
            }
        }
        //016

        //Enrollment selections
        foreach (ListItem chkitem in chklstEnrollment.Items)
        {
            if (chkitem.Selected)
            {
                riskMgmt = new SRERiskManagement();
                riskMgmt.FK_RiskID = riskId;
                riskMgmt.FK_RiskMgmtListID = Convert.ToInt64(chkitem.Value);
                //TODO riskMgmt.ModifiedBy
                riskMgmt.OtherText = (chkitem.Text == "Other") ? txtEnrollmentOther.Text : "";
                //riskMgmt.CreatedBy = 
                riskMgmt.DateAdded = DateTime.Today;
                riskMgmt.DateMaint = DateTime.Today;
                riskMgmtList.Add(riskMgmt);
            }
        }

        //Average risk controls selections
        foreach (ListItem chkitem in chkListAverageRisk.Items)
        {
            if (chkitem.Selected)
            {
                riskMgmt = new SRERiskManagement();
                riskMgmt.FK_RiskID = riskId;
                riskMgmt.FK_RiskMgmtListID = Convert.ToInt64(chkitem.Value);
                //TODO riskMgmt.ModifiedBy
                riskMgmt.OtherText = (chkitem.Text == "Other") ? txtAverageRiskControlsOther.Text : "";
                //riskMgmt.CreatedBy = 
                riskMgmt.DateAdded = DateTime.Today;
                riskMgmt.DateMaint = DateTime.Today;

                riskMgmtList.Add(riskMgmt);
            }
        }

        //High risk controls selections
        foreach (ListItem chkitem in chkListHighRisk.Items)
        {
            if (chkitem.Selected)
            {
                riskMgmt = new SRERiskManagement();
                riskMgmt.FK_RiskID = riskId;
                riskMgmt.FK_RiskMgmtListID = Convert.ToInt64(chkitem.Value);
                //TODO riskMgmt.ModifiedBy
                riskMgmt.OtherText = (chkitem.Text == "Other") ? txtHighRiskControlsOther.Text : "";
                //riskMgmt.CreatedBy = 
                riskMgmt.DateAdded = DateTime.Today;
                riskMgmt.DateMaint = DateTime.Today;

                riskMgmtList.Add(riskMgmt);
            }
        }

        riskEvaluation.SaveSelectedRiskManagementOptions(riskId, riskMgmtList);
    }

    private bool FinancialValue(HiddenField hdn)
    {
        if (hdn.Value == "1")
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    private bool DateChecker(TextBox txtDate)
    {
        DateTime attempttdate;
        string DateTemp = txtDate.Text;

        if ((DateTime.TryParse(txtDate.Text.Trim(), out attempttdate)))
        {

            //    SGS Changes on 09/03/2011 - date format            
            try
            {
                txtDate.Text = Convert.ToDateTime(txtDate.Text).ToString("MM/dd/yyyy");
                if ((Convert.ToDateTime(txtDate.Text) > Convert.ToDateTime("01/01/1753")) && (Convert.ToDateTime(txtDate.Text) < Convert.ToDateTime("01/01/2500")))
                    return true;
                else
                {
                    txtDate.Text = DateTemp;
                    return false;
                }
            }
            catch
            {
                txtDate.Text = DateTemp;
                return false;
            }
        }

        return false;
    }
    #endregion

    #region Document Section
    /// <summary>
    /// validate the documents for upload
    /// </summary>
    /// <param name="fluUpload"></param>
    /// <param name="fileName"></param>
    /// <param name="description"></param>
    private void ValidateDocument(FileUpload fluUpload, TextBox fileName, string description)
    {

        if ((string.IsNullOrEmpty(fileName.Text.Trim())) && (fluUpload.HasFile))
        {
            lblattach.Text += TAG_ERROR_MSG.Replace("{}", "Please upload the document " + description);
        }
        else if (!(string.IsNullOrEmpty(fileName.Text.Trim())) && !(fluUpload.HasFile))
        {
            lblattach.Text += TAG_ERROR_MSG.Replace("{}", "Please enter the description " + description);
        }
        else if (!(string.IsNullOrEmpty(fileName.Text.Trim())) && (fluUpload.HasFile))
        {
            if (!Common.IsValidExtension(System.IO.Path.GetExtension(fluUpload.FileName)))
            {
                lblattach.Text += TAG_ERROR_MSG.Replace("{}", "Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file " + description);
            }
            if (fluUpload.PostedFile.ContentLength - MAXFILESIZE > 0)
            {
                //005 - same here
                lblattach.Text += TAG_ERROR_MSG.Replace("{}", "File should be less than " + ConfigurationManager.AppSettings["MaxFileSizeMBs"].ToString() + "MB of size for description " + description);
            }
            else if (riskEvaluation.IsDocumentExists(riskId, fileName.Text.Trim()))
            {
                lblattach.Text += TAG_ERROR_MSG.Replace("{}", "Description " + description + " already exists");
            }
        }
    }
    /// <summary>
    /// check for valid extension of the documents
    /// </summary>
    /// <param name="extension"></param>
    /// <returns></returns>
    /// GV:  Moved to Common class
    //private Boolean IsValidExtension(string extension)
    //{
    //    switch (extension.ToLower())
    //    {
    //        case ".gif":
    //        case ".jpg":
    //        case ".jpeg":
    //        case ".xls":
    //        case ".xlsx":
    //        case ".txt":
    //        case ".pdf":
    //        case ".doc":
    //        case ".docx":
    //            return true;
    //        default:
    //            return false;
    //    }
    //}
    /// <summary>
    /// method to save the SRE documents
    /// </summary>
    /// <param name="txtFileName"></param>
    /// <param name="fluUpload"></param>
    private void SaveSREDoc(TextBox txtFileName, FileUpload fluUpload)
    {

        string fileName = string.Empty;
        if (!string.IsNullOrEmpty(txtFileName.Text.Trim()) && fluUpload.HasFile)
        {
            try
            {
                fileName = SaveDocument(fluUpload);
                riskEvaluation.SaveSREDocument(riskId, 0, txtFileName.Text.Trim(), fileName, userId);
            }
            catch
            {
                DeleteSREDocument(fileName);
            }
        }
    }
    /// <summary>
    /// save the documents
    /// </summary>
    /// <param name="fileUpload"></param>
    /// <returns></returns>
    private string SaveDocument(FileUpload fileUpload)
    {
        string filename = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(fileUpload.FileName);
        if (System.IO.Directory.Exists(DOCUMENT_LOCATION))
        {
            System.IO.Directory.CreateDirectory(DOCUMENT_LOCATION);
        }
        fileUpload.SaveAs(DOCUMENT_LOCATION + filename);
        return filename;
    }
    /// <summary>
    /// Delete the SRE document method
    /// </summary>
    /// <param name="fileName"></param>
    private void DeleteSREDocument(string fileName)
    {
        if (File.Exists(DOCUMENT_LOCATION + fileName))
        {
            File.Delete(DOCUMENT_LOCATION + fileName);
        }
    }
    /// <summary>
    /// clear the SRE document upload controls
    /// </summary>
    private void ClearDocumentValue()
    {
        txtFileName1.Text = "";
        txtFileName2.Text = "";
        txtFileName3.Text = "";
        txtFileName4.Text = "";
        txtFileName5.Text = "";
        lblattach.Text = "";
    }
    /// <summary>
    /// Bind the documents to the grid view
    /// </summary>
    /// <param name="attachmentType"></param>
    private void BindDocument(byte attachmentType)
    {


        var risk = riskEvaluation.GetSREDetail(riskId);
        DataSet document = riskEvaluation.GetSREDocumentList(riskId, attachmentType, "2");
        rptDocument.DataSource = document.Tables[0];
        rptDocument.DataBind();
        // GV - not sure why only getting the attachment type 0
        DataSet document1 = riskEvaluation.GetSREDocumentList(riskId, attachmentType, "1");
        rptDocumentAdmin.DataSource = document1.Tables[0];
        rptDocumentAdmin.DataBind();
        DocumentTd.Style["display"] = "";// document.Count > 0 ? "" : "none";
        if (rptDocumentAdmin.Items.Count == 0)
        {
            rptadmin.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "NoDocument").ToString();
        }
        else
        {
            rptadmin.Text = "";
        }
        if (rptDocument.Items.Count == 0)
        {
            rptPM.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "NoDocument").ToString();
        }
        else
        {
            rptPM.Text = "";
        }
        //if (imbAwarded.Visible == true || (imbAwarded.Visible == false && imbComplete.Visible == false) || Convert.ToString(Session["Access"]) == "1")
        //{

        //    ImageButton imb = (ImageButton)rptDocument.FindControl("imbDelete");
        //    for (int i = 0; i < document.Tables[0].Rows.Count; i++)
        //    {

        //        rptDocument.Items[i].FindControl("imbDelete").Visible = false;
        //    }

        //}

        string access = Convert.ToString(Session["Access"]);    //004
        string role = Session["EmployeeRole"].ConvertToString();    //014
        //GV FIX -STARTS
        //if (imbAwarded.Visible == true || (imbAwarded.Visible == false && imbComplete.Visible == false) || access != "1" || role == clsAdminLogin.EmployeeRole.Admin_SRE.ToString())
        //{
        imbAward.Visible = (!imbComplete.Visible && !imbAwarded.Visible);

        //GV FIX - for (int i = 0; i < document1.Tables[0].Rows.Count; i++)
        for (int i = 0; i < rptDocumentAdmin.Items.Count; i++)
        {
            //004 - admin should be able to delete ADMIN attachments
            rptDocumentAdmin.Items[i].FindControl("imbDelete1").Visible = (access == "1");
        }

        //004 - only if user is a PM or employee
        //GV FIX
        for (int i = 0; i < rptDocument.Items.Count; i++)
        {
            rptDocument.Items[i].FindControl("imbDelete").Visible = true; //GV FIX (risk.CreatedUserID == userId);
        }


        //}
        //GV-FIX - ENDS

        //if (imbAwarded.Visible == false && imbComplete.Visible == false)
        //{
        //    trAttachDocument.Style["display"] = "none";
        //}
        //else if (imbAwarded.Visible)
        //{
        //    trAttachDocument.Style["display"] = "none";
        //}
        //else
        //{
        //    trAttachDocument.Style["display"] = "";
        //}

        //004
        //if (statusId == 4 || statusId == 5)
        //{
        //    trAttachDocument.Style["display"] = "none";
        //}
        //else
        //{ trAttachDocument.Style["display"] = ""; }

        trAttachDocument.Style["display"] = "";

        //004
        //if (trAttachDocument.Style["display"] == "none")
        //{
        //imbAward.Visible = true;
        //004 Td1.Visible = false;
        //}
        //else
        //{
        //imbAward.Visible = false;
        //004 Td1.Visible = true;
        //}

        if (Convert.ToString(Session["Access"]) == "1" && imbUnlockSRE.Visible && statusId == 2)    //004 - added statusID
        {
            imbAward.Visible = true;
            //004 Td1.Visible = false;
        }
        else
        {
            imbAward.Visible = false;    //004 - added
        }
        //if (statusId == 3)
        //{ 
        //    trAttachDocument.Style["display"] = "";
        //}

    }
    /// <summary>
    /// open the selected documents
    /// </summary>
    /// <param name="fileName"></param>
    private void ShowDocument(string fileName)
    {
        string strFileName = DOCUMENT_LOCATION + fileName;
        FileInfo file = new FileInfo(strFileName);

        // Checking if file exists
        if (file.Exists)
        {
            // Clear the content of the response
            Response.ClearContent();
            // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            // Add the file size into the response header
            Response.AddHeader("Content-Length", file.Length.ToString());
            // Set the ContentType
            Response.ContentType = Common.GetDocumentContentType(file.Extension.ToLower());
            Response.TransmitFile(file.FullName);
            // End the response
            Response.Flush();
        }
    }
    private Boolean ValidateDoc()
    {
        if ((string.IsNullOrEmpty(txtFileName1.Text.Trim())) && (string.IsNullOrEmpty(txtFileName2.Text.Trim()))
                    && (string.IsNullOrEmpty(txtFileName3.Text.Trim())) && (string.IsNullOrEmpty(txtFileName4.Text.Trim())
                    && (string.IsNullOrEmpty(txtFileName5.Text.Trim())) && !(fluUpload1.HasFile) && !(fluUpload2.HasFile)
                    && !(fluUpload3.HasFile) && !(fluUpload4.HasFile) && !(fluUpload5.HasFile)))
        {
            lblattach.Text = TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "Upload").ToString());
            mpopAddDocument.Show();
            return false;
        }
        lblattach.Text = "";
        ValidateDocument(fluUpload1, txtFileName1, "one");
        ValidateDocument(fluUpload2, txtFileName2, "two");
        ValidateDocument(fluUpload3, txtFileName3, "three");
        ValidateDocument(fluUpload4, txtFileName4, "four");
        ValidateDocument(fluUpload5, txtFileName5, "five");
        if (!string.IsNullOrEmpty(lblattach.Text.Trim()))
        {
            return false;
        }
        return true;
    }

    #endregion

    #region Validate Methods
    /// <summary>
    /// validate SRE and list the errors
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    private bool ValidateSRE(ref string message)
    {
        if (chkAverageRisk.Checked)
        {
            txtProjectManagerName.Text = Session["EmployeeName"].ToString();
        }
        if (string.IsNullOrEmpty(txtDate.Text.Trim()))
        {
            //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter date");
            message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SREDate").ToString());
            accrodinOpen = "Main";
        }
        if (string.IsNullOrEmpty(txtAppSubContractorValue.Text.Trim()))
        {
            //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter approximate subcontract value");

            message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ApproximateSubcontract").ToString());
            accrodinOpen = "Main";
        }
        if (string.IsNullOrEmpty(txtBidpackageDesc.Text.Trim()))
        {
            //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter bid package description");
            message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "DidPackageDescription").ToString());
            accrodinOpen = "Main";
        }
        if (string.IsNullOrEmpty(txtBidpackageNumber.Text.Trim()))
        {
            //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter bid package #");
            message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "BidPackageNo").ToString());
            accrodinOpen = "Main";
        }
        // Business Section
        if (accrodinOpen == string.Empty)
        {
            if (!rdoBusiness1Yes.Checked && !rdoBusiness1No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 1 under business section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "BusinessAnswer1").ToString());
                accrodinOpen = "Business";
            }
            if (!rdoBusiness2Yes.Checked && !rdoBusiness2No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 2 under business section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "BusinessAnswer2").ToString());
                accrodinOpen = "Business";
            }
            if (!rdoBusinessLitigationYes.Checked && !rdoBusinessLitigationNo.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 3 under business section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "BusinessAnswer5").ToString());
                accrodinOpen = "Business";
            }
            if (!rdoBusiness3Yes.Checked && !rdoBusiness3No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 3 under business section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "BusinessAnswer3").ToString());
                accrodinOpen = "Business";
            }
            if (!rdoBusiness4Yes.Checked && !rdoBusiness4No.Checked)
            {

                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 4 under business section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "BusinessAnswer4").ToString());
                accrodinOpen = "Business";
            }
        }
        // Financial Section
        if (accrodinOpen == string.Empty)
        {
            if (!rdoFinancial1Yes.Checked && !rdoFinancial1No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 1 under financial section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "FinancialAnswer1").ToString());
                accrodinOpen = "Financial";
            }
            if (!rdoFinancial2Yes.Checked && !rdoFinancial2No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 2 under financial section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "FinancialAnswer2").ToString());
                accrodinOpen = "Financial";
            }
            if (!rdoFinancial3Yes.Checked && !rdoFinancial3No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 3 under financial section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "FinancialAnswer3").ToString());
                accrodinOpen = "Financial";
            }
            if (!rdoFinancial4Yes.Checked && !rdoFinancial4No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 4 under financial section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "FinancialAnswer4").ToString());
                accrodinOpen = "Financial";
            }

            if (string.IsNullOrEmpty(txtSubcontractorCurrentBid.Text.Trim()))
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter subcontractor's current bid");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "CurrentBid").ToString());
                accrodinOpen = "Financial";
            }
            if (string.IsNullOrEmpty(txtNextLowestBid.Text.Trim()))
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter next lowest bid value");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "LowestBid").ToString());
                accrodinOpen = "Financial";
            }
            if (!rdoFinancial5Yes.Checked && !rdoFinancial5No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 5 under financial section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "FinancialAnswer5").ToString());
                accrodinOpen = "Financial";
            }
        }
        // License Section
        if (accrodinOpen == string.Empty)
        {
            if (!rdoLicense1Yes.Checked && !rdoLicense1No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 1 under license section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SRELicenseQuestion").ToString());
                accrodinOpen = "Licence";
            }

            if (string.IsNullOrEmpty(txtVerificationDate.Text.Trim()))
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter PM verification date");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "PMDate").ToString());
                accrodinOpen = "Licence";
            }
            if (string.IsNullOrEmpty(txtPMNotes.Text.Trim()))
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter PM verification notes");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "PMNotes").ToString());
                accrodinOpen = "Licence";
            }
        }
        // Safety Section
        if (accrodinOpen == string.Empty)
        {
            if (!rdoSafety1Yes.Checked && !rdoSafety1No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 1 under safety section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SRESafety1").ToString());
                accrodinOpen = "Safety";
            }
            if (!rdoSafety2Yes.Checked && !rdoSafety2No.Checked)
            {

                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 2 under safety section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SRESafety2").ToString());
                if (accrodinOpen == string.Empty)
                    accrodinOpen = "Safety";
            }
            //014 - START
            if (!rdoSafetyCitationNo.Checked && !rdoSafetyCitationYes.Checked)
            {

                message = message + TAG_ERROR_MSG.Replace("{}", "Answer for question 3 under safety section is missing.");
                if (accrodinOpen == string.Empty)
                    accrodinOpen = "Safety";
            }
            //014 - END
        }
        // Insurance & Bonding Section
        if (accrodinOpen == string.Empty)
        {
            if (!rdoInsurance1Yes.Checked && !rdoInsurance1No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 1 under insurance & bonding section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SREInsurance1").ToString());
                accrodinOpen = "Insurance";
            }
            if (rdoInsurance1No.Checked)
            {

                if (!rdoInsurance2Yes.Checked && !rdoInsurance2No.Checked)
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 2 under insurance & bonding section");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SREInsurance2").ToString());
                    accrodinOpen = "Insurance";
                }
            }
            if (!rdoInsurance3Yes.Checked && !rdoInsurance3No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 3 under insurance & bonding section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SREInsurance3").ToString());
                accrodinOpen = "Insurance";
            }
            if (!rdoInsurance4Yes.Checked && !rdoInsurance4No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 4 under insurance & bonding section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SREInsurance4").ToString());
                accrodinOpen = "Insurance";
            }
        }

        if (string.IsNullOrEmpty(message))
            return true;
        return false;
    }
    /// <summary>
    /// validate page and list the errors
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {

            if (!validator.IsValid)
            {
                errmsg += "<li>" + validator.ErrorMessage + "</li>";
            }
        }
        return errmsg;

    }




    /// <summary>
    /// validate SRE and list the errors
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    private bool ValidateSRE1(ref string message)
    {
        // Reference Section
        if (accrodinOpen == string.Empty)
        {
            if (string.IsNullOrEmpty(txtProjectReference1.Text.Trim()))
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter project reference 1 under reference section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ProjectReference1").ToString());
                accrodinOpen = "Reference";
            }
            if (string.IsNullOrEmpty(txtReferenceDate1.Text.Trim()))
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter project reference date called 1 under reference section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ProjectDate1").ToString());
                accrodinOpen = "Reference";
            }
            if (string.IsNullOrEmpty(txtProjectReference2.Text.Trim()))
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter project reference 2 under reference section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ProjectReference2").ToString());
                accrodinOpen = "Reference";
            }
            if (string.IsNullOrEmpty(txtReferenceDate2.Text.Trim()))
            {
                accrodinOpen = "Reference";
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter project reference date called 2 under reference section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ProjectDate2").ToString());
            }
            if (string.IsNullOrEmpty(txtProjectReference3.Text.Trim()))
            {
                accrodinOpen = "Reference";
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter Project Reference 3 under Reference Section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ProjectReference3").ToString());
            }
            if (string.IsNullOrEmpty(txtReferenceDate3.Text.Trim()))
            {
                accrodinOpen = "Reference";
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter Project Reference Date Called 3 under Reference Section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ProjectDate3").ToString());
            }

            if (!rdoReference1Yes.Checked && !rdoReference1No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 1 under reference section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ReferenceQuestion1").ToString());
                accrodinOpen = "Reference";
            }
            if (!rdoReference2Yes.Checked && !rdoReference2No.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 2 under reference section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ReferenceQuestion2").ToString());
                accrodinOpen = "Reference";
            }
            if (!rdoReference3Yes.Checked && !rdoReference3No.Checked && !rdoReference3NA.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please answer any option for question 3 under reference section");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ReferenceQuestion3").ToString());
                accrodinOpen = "Reference";
            }
        }
        // Risk Determination
        if (accrodinOpen == string.Empty)
        {
            if (!chkHighRisk.Checked && !chkAverageRisk.Checked && !chkDisapproved.Checked)
            {
                //message = message + TAG_ERROR_MSG.Replace("{}", "Please select risk level");
                message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "RiskLevel").ToString());
                accrodinOpen = "Risk";
            }
            if (chkAverageRisk.Checked)
            {
                if (string.IsNullOrEmpty(txtProjectManagerName.Text.Trim()))
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter project manager name");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ProjecManagerName").ToString());
                    accrodinOpen = "Risk";
                }
                if (string.IsNullOrEmpty(txtRiskDate1.Text.Trim()))
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter date for average risk");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "AverageRiskDate").ToString());
                    accrodinOpen = "Risk";
                }
                //015 - START
                if (txtAppSubContractorValue.Text.Replace(",", "").ConvertToInteger() >= DocApprovalTreshold)
                {
                    if (string.IsNullOrEmpty(txtGroupName1.Text.Trim()))
                    {
                        message = message + TAG_ERROR_MSG.Replace("{}", "Please enter group DOC Name/ signature (Subcontract value >= " + DocApprovalTresholdDisplay + ")");
                        //message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "DOCName").ToString());
                        accrodinOpen = "Risk";
                    }
                    if (string.IsNullOrEmpty(txtRiskDate2.Text.Trim()))
                    {
                        message = message + TAG_ERROR_MSG.Replace("{}", "Please enter DOC signature date (Subcontract value >= " + DocApprovalTresholdDisplay + ")");
                        //message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "HighRisk").ToString());
                        accrodinOpen = "Risk";
                    } 
                }
                //015 - END


            }
            if (chkHighRisk.Checked)
            {
                if (string.IsNullOrEmpty(txtGroupName1.Text.Trim()))
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter group DOC Name/ signature for high risk");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "DOCName").ToString());
                    accrodinOpen = "Risk";
                }
                if (string.IsNullOrEmpty(txtRiskDate2.Text.Trim()))
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter date for high risk");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "HighRisk").ToString());
                    accrodinOpen = "Risk";
                }
            }
            if (chkDisapproved.Checked)
            {
                if (string.IsNullOrEmpty(txtGroupName2.Text.Trim()))
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter group DOC Name / signature for disapproved risk");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "DisapprovedRisk").ToString());
                    accrodinOpen = "Risk";
                }
                if (string.IsNullOrEmpty(txtRiskDate3.Text.Trim()))
                {
                    // message = message + TAG_ERROR_MSG.Replace("{}", "Please enter date for disapproved risk");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "DisapprovedRiskDate").ToString());
                    accrodinOpen = "Risk";
                }
            }
        }
        if (string.IsNullOrEmpty(message))
            return true;
        return false;
    }

    private bool ValidateSREDate(ref string message)
    {
        //    SGS Changes on 09/03/2011 - date format              

        // License Section
        if (accrodinOpen == string.Empty)
        {
            if (txtVerificationDate.Text.Trim() != string.Empty)
            {

                if (DateChecker(txtVerificationDate) != true)
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter valid PM verification date");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "PMVerificationDate").ToString());
                    accrodinOpen = "Licence";
                }


            }
        }
        //Reference Section
        if (accrodinOpen == string.Empty)
        {

            if (txtReferenceDate1.Text.Trim() != string.Empty)
            {
                if (DateChecker(txtReferenceDate1) != true)
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter project valid reference date called 1 under reference section");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ProjectValidReferenceDate1").ToString());
                    accrodinOpen = "Reference";
                }

            }
            if (txtReferenceDate2.Text.Trim() != string.Empty)
            {

                if (DateChecker(txtReferenceDate2) != true)
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter project valid reference date called 2 under reference section");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ProjectValidReferenceDate2").ToString());
                    accrodinOpen = "Reference";
                }

            }
            if (txtReferenceDate3.Text.Trim() != string.Empty)
            {

                if (DateChecker(txtReferenceDate3) != true)
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter project valid reference date called 3 under reference section");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ProjectValidReferenceDate3").ToString());
                    accrodinOpen = "Reference";
                }

            }
            if (txtSupplierDate1.Text.Trim() != string.Empty)
            {
                if (DateChecker(txtSupplierDate1) != true)
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter valid supplier reference date called 1 under reference section");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SupplierValidReferenceDate1").ToString());
                    accrodinOpen = "Reference";
                }

            }
            if (txtSupplierDate2.Text.Trim() != string.Empty)
            {

                if (DateChecker(txtSupplierDate2) != true)
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter valid supplier reference date called 2 under reference section");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SupplierValidReferenceDate2").ToString());
                    accrodinOpen = "Reference";
                }

            }
            if (txtSupplierDate3.Text.Trim() != string.Empty)
            {
                if (DateChecker(txtSupplierDate3) != true)
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter valid supplier reference date called 3 under reference section");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SupplierValidReferenceDate3").ToString());
                    accrodinOpen = "Reference";
                }

            }
        }
        //Risk Detemination
        if (accrodinOpen == string.Empty)
        {

            if (txtRiskDate1.Text.Trim() != string.Empty)
            {

                if (DateChecker(txtRiskDate1) != true)
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter valid date for high risk");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ValidAverageRiskDate").ToString());

                    accrodinOpen = "Risk";
                }

            }
            if (txtRiskDate2.Text.Trim() != string.Empty)
            {


                if (DateChecker(txtRiskDate2) != true)
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter valid date for average risk");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ValidHighRiskDate").ToString());

                    accrodinOpen = "Risk";
                }

            }
            if (txtRiskDate3.Text.Trim() != string.Empty)
            {

                if (DateChecker(txtRiskDate3) != true)
                {
                    //message = message + TAG_ERROR_MSG.Replace("{}", "Please enter valid date for disapproved risk");
                    message = message + TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ValidDisapprovedRiskDate").ToString());

                    accrodinOpen = "Risk";
                }

            }
        }
        if (string.IsNullOrEmpty(message))
            return true;
        return false;
    }
    /// <summary>
    /// Focus to the appropriate Accrodion Pane based on validation
    /// </summary>
    private void AccrodineValidateFocus()
    {
        switch (accrodinOpen)
        {
            case "Business":
                acdnBusiness.SelectedIndex = 0;
                break;
            case "Financial":
                acdnBusiness.SelectedIndex = 1;
                break;
            case "Licence":
                acdnBusiness.SelectedIndex = 2;
                break;
            case "Safety":
                acdnBusiness.SelectedIndex = 3;
                break;
            case "Insurance":
                acdnBusiness.SelectedIndex = 4;
                break;
            case "Reference":
                acdnBusiness.SelectedIndex = 5;
                break;
            case "Risk":
                acdnBusiness.SelectedIndex = 6;
                break;
            default:
                acdnBusiness.SelectedIndex = 0;
                break;
        }
    }


    protected void cusvYearOne_ServerValidate(object source, ServerValidateEventArgs args)
    {

        if (string.IsNullOrEmpty(txtRateOne.Text.Trim()))
        {
            cusvYearOne.ErrorMessage = HttpContext.GetGlobalResourceObject("ErrorMessages", "YearOne").ToString();

            args.IsValid = false;
        }

        else
        {

            args.IsValid = true;
        }
    }


    protected void cusvYearTwo_ServerValidate(object source, ServerValidateEventArgs args)
    {

        if (string.IsNullOrEmpty(txtRateTwo.Text.Trim()))
        {
            cusvYearTwo.ErrorMessage = HttpContext.GetGlobalResourceObject("ErrorMessages", "YearTwo").ToString();

            args.IsValid = false;
        }

        else
        {
            args.IsValid = true;
        }
    }


    protected void cusvYearThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (string.IsNullOrEmpty(txtRateThree.Text.Trim()))
        {
            cusvYearThree.ErrorMessage = HttpContext.GetGlobalResourceObject("ErrorMessages", "Yearthree").ToString();

            args.IsValid = false;
        }

        else
        {
            args.IsValid = true;
        }
    }

    protected string NullOrEmptyToZero(string value)
    {
        if (string.IsNullOrEmpty(value))
            return "0";
        else
            return value;
    }

    #endregion

    #region Deleted Codes
    //protected void cusvYearOne_ServerValidate(object source, ServerValidateEventArgs args)
    //{

    //    if (string.IsNullOrEmpty(txtMaxContractValueYearOne.Text.Trim()) && string.IsNullOrEmpty(txtAnuualCompRevenueYearOne.Text.Trim()))
    //    {
    //        cusvYearOne.ErrorMessage = "Please enter maximum contract value completed under year one";
    //        cusvYearOne.ErrorMessage += "<li>Please enter annual company revenue under year one</li>";
    //        args.IsValid = false;
    //    }
    //    else if (string.IsNullOrEmpty(txtMaxContractValueYearOne.Text.Trim()) || string.IsNullOrEmpty(txtAnuualCompRevenueYearOne.Text.Trim()))
    //    {
    //        if (string.IsNullOrEmpty(txtMaxContractValueYearOne.Text.Trim()))
    //        {
    //            cusvYearOne.ErrorMessage = "Please enter maximum contract value completed under year one";
    //            args.IsValid = false;
    //        }
    //        if (string.IsNullOrEmpty(txtAnuualCompRevenueYearOne.Text.Trim()))
    //        {
    //            cusvYearOne.ErrorMessage = "Please enter annual company revenue under year one";
    //            args.IsValid = false;
    //        }
    //    }

    //}

    //protected void cusvYearTwo_ServerValidate(object source, ServerValidateEventArgs args)
    //{

    //    if (string.IsNullOrEmpty(txtMaxContractValueYearTwo.Text.Trim()) && string.IsNullOrEmpty(txtAnuualCompRevenueYearTwo.Text.Trim()))
    //    {
    //        cusvYearTwo.ErrorMessage = "Please enter maximum contract value completed under year two";
    //        cusvYearTwo.ErrorMessage += "<li>Please enter annual company revenue under year two</li>";
    //        args.IsValid = false;
    //    }
    //    else if (string.IsNullOrEmpty(txtMaxContractValueYearTwo.Text.Trim()) || string.IsNullOrEmpty(txtAnuualCompRevenueYearTwo.Text.Trim()))
    //    {
    //        if (string.IsNullOrEmpty(txtMaxContractValueYearTwo.Text.Trim()))
    //        {
    //            cusvYearTwo.ErrorMessage = "Please enter maximum contract value completed under year two";
    //            args.IsValid = false;
    //        }
    //        if (string.IsNullOrEmpty(txtAnuualCompRevenueYearTwo.Text.Trim()))
    //        {
    //            cusvYearTwo.ErrorMessage = "Please enter annual company revenue under year two";
    //            args.IsValid = false;
    //        }
    //    }

    //}

    //protected void cusvYearThree_ServerValidate(object source, ServerValidateEventArgs args)
    //{

    //    if (string.IsNullOrEmpty(txtMaxContractValueYearThree.Text.Trim()) && string.IsNullOrEmpty(txtAnuualCompRevenueYearThree.Text.Trim()))
    //    {
    //        cusvYearThree.ErrorMessage = "Please enter maximum contract value completed under year three";
    //        cusvYearThree.ErrorMessage += "<li>Please enter annual company revenue under year three</li>";
    //        args.IsValid = false;
    //    }
    //    else if (string.IsNullOrEmpty(txtMaxContractValueYearThree.Text.Trim()) || string.IsNullOrEmpty(txtAnuualCompRevenueYearThree.Text.Trim()))
    //    {
    //        if (string.IsNullOrEmpty(txtMaxContractValueYearThree.Text.Trim()))
    //        {
    //            cusvYearThree.ErrorMessage = "Please enter maximum contract value completed under year three";
    //            args.IsValid = false;
    //        }
    //        if (string.IsNullOrEmpty(txtAnuualCompRevenueYearThree.Text.Trim()))
    //        {
    //            cusvYearThree.ErrorMessage = "Please enter annual company revenue under year three";
    //            args.IsValid = false;
    //        }
    //    }

    //}

    //protected void imbLegalSave_Click(object sender, ImageClickEventArgs e)
    //{
    //    Page.Validate("Legal");
    //    if (Page.IsValid)
    //    {
    //if (string.IsNullOrEmpty(txtMaxContractValueYearThree.Text.Trim()) && string.IsNullOrEmpty(txtAnuualCompRevenueYearThree.Text.Trim()))
    //{
    //    trLegalMessage.Style["display"] = "";
    //    lblLegalmsg.Style["display"] = "";
    //    lblLegalmsg.Text = "<li>Please enter maximum contract value completed under year three";
    //    lblLegalmsg.Text += "<li>Please enter annual company revenue under year three</li>";
    //    mpopLegalAlert.Show();
    //}
    //    else if (string.IsNullOrEmpty(txtMaxContractValueYearThree.Text.Trim()) || string.IsNullOrEmpty(txtAnuualCompRevenueYearThree.Text.Trim()))
    //    {
    //        if (string.IsNullOrEmpty(txtMaxContractValueYearThree.Text.Trim()))
    //        {
    //            trLegalMessage.Style["display"] = "";
    //            lblLegalmsg.Style["display"] = "";
    //            lblLegalmsg.Text = "<li>Please enter maximum contract value completed under year three";
    //            mpopLegalAlert.Show();

    //        }
    //        if (string.IsNullOrEmpty(txtAnuualCompRevenueYearThree.Text.Trim()))
    //        {
    //            trLegalMessage.Style["display"] = "";
    //            lblemrmsg.Style["display"] = "";
    //            lblemrmsg.Text = "<li>Please enter annual company revenue under year three";
    //            mpopLegalAlert.Show();

    //        }
    //    }

    //    if (string.IsNullOrEmpty(txtMaxContractValueYearTwo.Text.Trim()) && string.IsNullOrEmpty(txtAnuualCompRevenueYearTwo.Text.Trim()))
    //    {
    //        trLegalMessage.Style["display"] = "";
    //        lblLegalmsg.Style["display"] = "";
    //        lblLegalmsg.Text = "<li>Please enter maximum contract value completed under year two";
    //        lblLegalmsg.Text += "<li>Please enter annual company revenue under year two</li>";
    //        mpopLegalAlert.Show();
    //    }
    //    else if (string.IsNullOrEmpty(txtMaxContractValueYearTwo.Text.Trim()) || string.IsNullOrEmpty(txtAnuualCompRevenueYearTwo.Text.Trim()))
    //    {
    //        if (string.IsNullOrEmpty(txtMaxContractValueYearTwo.Text.Trim()))
    //        {
    //            trLegalMessage.Style["display"] = "";
    //            lblLegalmsg.Style["display"] = "";
    //            lblLegalmsg.Text = "<li>Please enter maximum contract value completed under year two";
    //            mpopLegalAlert.Show();

    //        }
    //        if (string.IsNullOrEmpty(txtAnuualCompRevenueYearTwo.Text.Trim()))
    //        {
    //            trLegalMessage.Style["display"] = "";
    //            lblLegalmsg.Style["display"] = "";
    //            lblLegalmsg.Text = "<li>Please enter annual company revenue under year two";
    //            mpopLegalAlert.Show();

    //        }
    //    }
    //}
    protected void imbSubmit_Click(object sender, ImageClickEventArgs e)
    {
        if (chkHighRisk.Checked)
        {
        }
        string message = string.Empty;
        if (ValidateSRE(ref message))
        {

            bool isUpdated = riskEvaluation.UpdateBidPackage(riskId, txtBidpackageNumber.Text.Trim(), 1, 0, userId);// new Nullable<Int64>()); 
            if (isUpdated)
            {
                //lblMessage.Text = "SRE has been updated successfully";
                lblMessage.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "SREsuccessfully").ToString();

                imbOK.Visible = false;
                imbOK1.Visible = true;
            }
            else
            {
                lblMessage.Text = "Failed to update";
                imbOK1.Visible = true;
                imbOK.Visible = false;
            }
        }
        else
        {
            lblMessage.Text = message;
            imbOK1.Visible = true;
            imbOK.Visible = false;
        }
        align1.Align = "Center";
        mpopAlert.Show();
    }
    protected void imbUpdate_Click(object sender, ImageClickEventArgs e)
    {
        riskEvaluation.UpdateBidPackage(riskId, string.Empty, 2, 0, userId);// new Nullable<Int64>()); 
    }

    /// <summary>
    /// 004 - Implemented
    /// 015
    /// </summary>
    protected void LoadRiskManagmentChecklist()
    {
        RiskManagementChecklist mngmtChkList = new RiskManagementChecklist();
        RiskEnrollment objEnrollment = new RiskEnrollment();
        List<VMSDAL.SRERiskManagement> selected = riskEvaluation.GetSRERiskManagement(riskId); 


        int i = 0;
        foreach (var item in mngmtChkList.AverageRiskSection)
        {
            chkListAverageRisk.Items.Add(item.Name);
            if (item.Name == "Other")
            {
                string textboxid = txtAverageRiskControlsOther.ClientID;

                if (!selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID))
                {
                    txtAverageRiskControlsOther.Attributes.Add("hidden", "hidden");
                    
                }
                else
                {
                    txtAverageRiskControlsOther.Text = selected.FirstOrDefault(f => f.FK_RiskMgmtListID == item.PK_RiskMgmtListID).OtherText;
                    txtAverageRiskControlsOther.Attributes.Remove("hidden");
                }
                chkListAverageRisk.Items[i].Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
            }
            chkListAverageRisk.Items[i].Value = Convert.ToString(item.PK_RiskMgmtListID);
            chkListAverageRisk.Items[i].Selected = selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID);
            i++;
        }

        i = 0;
        foreach (var item in mngmtChkList.HighRiskSection)
        {
            chkListHighRisk.Items.Add(item.Name);
            if (item.Name == "Other")
            {
                string textboxid = txtHighRiskControlsOther.ClientID;
                
                if (!selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID))
                {
                    txtHighRiskControlsOther.Attributes.Add("hidden", "hidden");
                }
                else
                {
                    txtHighRiskControlsOther.Text = selected.FirstOrDefault(f => f.FK_RiskMgmtListID == item.PK_RiskMgmtListID).OtherText;
                    txtHighRiskControlsOther.Attributes.Remove("hidden");

                }

                chkListHighRisk.Items[i].Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
            }
            chkListHighRisk.Items[i].Value = Convert.ToString(item.PK_RiskMgmtListID);
            chkListHighRisk.Items[i].Selected = selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID);
            i++;
        }

        i = 0;
        foreach (var item in mngmtChkList.HighRiskDetermination)
        {
            chkListHighRiskDet.Items.Add(item.Name);
            if (item.Name == "Other")
            {
                string textboxid = txtHighRiskDetOther.ClientID;
                
                if (!selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID))
                {
                    txtHighRiskDetOther.Attributes.Add("hidden", "hidden");
                }
                else
                {
                    txtHighRiskDetOther.Text = selected.FirstOrDefault(f => f.FK_RiskMgmtListID == item.PK_RiskMgmtListID).OtherText;
                    txtHighRiskDetOther.Attributes.Remove("hidden");

                }
                chkListHighRiskDet.Items[i].Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
            }
            chkListHighRiskDet.Items[i].Value = Convert.ToString(item.PK_RiskMgmtListID);
            chkListHighRiskDet.Items[i].Selected = selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID);
            i++;
        }

        //016 - Start
        i = 0;
        foreach (var item in mngmtChkList.AverageRiskDetermination)
        {
            chkListAvgRiskDet.Items.Add(item.Name);
            if (item.Name == "Other")
            {
                string textboxid = txtAvgRiskDetOther.ClientID;

                if (!selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID))
                {
                    txtAvgRiskDetOther.Attributes.Add("hidden", "hidden");
                }
                else
                {
                    txtAvgRiskDetOther.Text = selected.FirstOrDefault(f => f.FK_RiskMgmtListID == item.PK_RiskMgmtListID).OtherText;
                    txtAvgRiskDetOther.Attributes.Remove("hidden");

                }
                chkListAvgRiskDet.Items[i].Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
            }
            chkListAvgRiskDet.Items[i].Value = Convert.ToString(item.PK_RiskMgmtListID);
            chkListAvgRiskDet.Items[i].Selected = selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID);
            i++;
        }
        //016 - End

        i = 0;
        foreach (var item in objEnrollment.CheckList)
        {
            chklstEnrollment.Items.Add(item.Name);
            if (item.Name == "Other")
            {
                string textboxid = txtEnrollmentOther.ClientID;
                if (!selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID))
                {
                    txtEnrollmentOther.Attributes.Add("hidden", "hidden");
                }
                else
                {
                    txtEnrollmentOther.Text = selected.FirstOrDefault(f => f.FK_RiskMgmtListID == item.PK_RiskMgmtListID).OtherText;
                    txtEnrollmentOther.Attributes.Remove("hidden");

                }
                chklstEnrollment.Items[i].Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
            }
            chklstEnrollment.Items[i].Value = Convert.ToString(item.PK_RiskMgmtListID);
            chklstEnrollment.Items[i].Selected = selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID);

            i++;
        }

    }

    /// <summary>
    /// 004 - Implemented
    /// 015
    /// </summary>
    private void CheckBoxListItemsScripts()
    {
        RiskManagementChecklist mngmtChkList = new RiskManagementChecklist();   //016
        RiskEnrollment objEnrollment = new RiskEnrollment();    //016

        if (chkListAverageRisk.Items.Count > 0)
        {
            foreach (ListItem item in chkListAverageRisk.Items)
            {
                if (item.Text == "Other")
                {
                    string textboxid = txtAverageRiskControlsOther.ClientID;
                    if(item.Attributes["onclick"] == null ) item.Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
                    if (item.Selected) txtAverageRiskControlsOther.Attributes.Remove("hidden"); else txtAverageRiskControlsOther.Attributes.Add("hidden", "hidden");
                }

                //016
                var cli = mngmtChkList.AverageRiskSection.FirstOrDefault(e => e.PK_RiskMgmtListID == item.Value.ConvertToInteger());
                item.Enabled = (bool)cli.IsEnabled;
            }
        }
        if (chkListHighRisk.Items.Count > 0)
        {
            foreach (ListItem item in chkListHighRisk.Items)
            {
                if (item.Text == "Other")
                {
                    string textboxid = txtHighRiskControlsOther.ClientID;
                    if (item.Attributes["onclick"] == null)  item.Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
                    if (item.Selected) txtHighRiskControlsOther.Attributes.Remove("hidden"); else txtHighRiskControlsOther.Attributes.Add("hidden", "hidden");
                }

                //016
                var cli = mngmtChkList.HighRiskSection.FirstOrDefault(e => e.PK_RiskMgmtListID == item.Value.ConvertToInteger());
                item.Enabled = (bool)cli.IsEnabled;
            }
        }
        if (chkListHighRiskDet.Items.Count > 0)
        {
            foreach (ListItem item in chkListHighRiskDet.Items)
            {
                if (item.Text == "Other")
                {
                    string textboxid = txtHighRiskDetOther.ClientID;
                    if (item.Attributes["onclick"] == null) item.Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
                    if (item.Selected) txtHighRiskDetOther.Attributes.Remove("hidden"); else txtHighRiskDetOther.Attributes.Add("hidden", "hidden");
                }

                //016
                var cli = mngmtChkList.HighRiskDetermination.FirstOrDefault(e => e.PK_RiskMgmtListID == item.Value.ConvertToInteger()); 
                item.Enabled = (bool)cli.IsEnabled;
            }
        }

        //016
        if (chkListAvgRiskDet.Items.Count > 0)
        {
            foreach (ListItem item in chkListAvgRiskDet.Items)
            {
                if (item.Text == "Other")
                {
                    string textboxid = txtAvgRiskDetOther.ClientID;
                    if (item.Attributes["onclick"] == null) item.Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
                    if (item.Selected) txtAvgRiskDetOther.Attributes.Remove("hidden"); else txtAvgRiskDetOther.Attributes.Add("hidden", "hidden");
                }

                var cli = mngmtChkList.AverageRiskDetermination.FirstOrDefault(e => e.PK_RiskMgmtListID == item.Value.ConvertToInteger());
                item.Enabled = (bool)cli.IsEnabled;
            }
        }
        //016

        if (chklstEnrollment.Items.Count > 0)
        {
            foreach (ListItem item in chklstEnrollment.Items)
            {
                if (item.Text == "Other")
                {
                    string textboxid = txtEnrollmentOther.ClientID;
                    if (item.Attributes["onclick"] == null) item.Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
                    if (item.Selected) txtEnrollmentOther.Attributes.Remove("hidden"); else txtEnrollmentOther.Attributes.Add("hidden", "hidden");
                }

                //016
                var cli = objEnrollment.CheckList.FirstOrDefault(e => e.PK_RiskMgmtListID == item.Value.ConvertToInteger());
                item.Enabled = (bool)cli.IsEnabled;
            }
        }
    }

    #endregion

}