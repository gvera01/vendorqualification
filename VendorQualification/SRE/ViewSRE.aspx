﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewSRE.aspx.cs" EnableEventValidation="false" ValidateRequest="false" Inherits="SRE_ViewSRE" MaintainScrollPositionOnPostback="true" %>

<%--001 - G. Vera 06/13/2012 Phase 3 Item 4.9 (JIRA:VPI17)--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <link href="../css/Haskell.css" rel="Stylesheet" />
    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbSearch">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                    </td>
                                    <td class="contenttd">
                                   <%--  <asp:UpdatePanel ID="updPanel" runat="server">
                                   
 
                                            <ContentTemplate>--%>
                                            
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="formhead" colspan="2">
                                                            Subcontractor Risk Evaluation
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                                <tr>
                                                                    <td class="formtdlt" width="41%" valign="top">
                                                                    </td>
                                                                    <td class="formtdlt even_pad" align="right" valign=middle >
                                                                            <asp:ImageButton ID="imbPrintSRE" runat="server" 
                                                                                ImageUrl="~/Images/pringsrf.jpg" 
                                            ToolTip="Print SRE Form" OnClick="imbPrintSRE_Click"  Visible="false"   />  
                                                                        <asp:ImageButton ID="imgback" runat="server" ImageUrl="~/Images/back.jpg" ToolTip="Back"
                                                                            OnClick="imgback_Click" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" id="SRETd1" runat="server" width="100%">
                                                                        <table width="100%" cellpadding="3" cellspacing="0" id="Table2" runat="server">
                                                                            <tr>
                                                                                <td class="formtdlt" width="41%" valign="top">
                                                                                    Vendor Name from Vendor Database
                                                                                </td>
                                                                                <td class="formtdrt">

                                                                                    <asp:TextBox ID="txtVendor" runat="server" CssClass="ajaxtxtbox" MaxLength="60"></asp:TextBox>
                                                                                
                                                                                   <%-- <div id="listPlacement" style="height: 100px; overflow: scroll;">
                                                                                    </div>--%>
                                                                                    <Ajax:AutoCompleteExtender ID="autoComplete1" runat="server" BehaviorID="AutoCompleteEx"
                                                                                        CompletionListElementID="listPlacement" EnableCaching="false" FirstRowSelected="True"
                                                                                        MinimumPrefixLength="1" ServiceMethod="GetCompletionList" ServicePath="~/AutoComplete.asmx"
                                                                                        TargetControlID="txtVendor" CompletionSetCount="10">
                                                                                       <%-- <Animations>
                                                            <OnShow>
                                                              <Sequence>
                                                                <OpacityAction Opacity="0" />
                                                                <HideAction Visible="true" />
                                                            
                                                                    <ScriptAction Script="
                                                                        var behavior = $find('AutoCompleteEx');
                                                                        if (!behavior._height) {
                                                                            var target = behavior.get_completionList();
                                                                            behavior._height = target.offsetHeight - 2;
                                                                            target.style.height = '0px';
                                                                        }" />
                                                                <Parallel Duration=".4">
                                                                <FadeIn />
                                                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                                                                </Parallel>
                                                                </Sequence>
                                                                 </OnShow>
                                                                  <OnHide>
                                                                 <Parallel Duration=".4">
                                                                 <FadeOut />
                                                                 <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                                                                 </Parallel>
                                                                 </OnHide>
                                                                                        </Animations>--%>
                                                                                    </Ajax:AutoCompleteExtender>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtdlt" valign="top">
                                                                                    Project Number
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                <%--001--%>
                                                                                <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="ajaxtxtbox" MaxLength="12" />
                                                                                <%--<div id="divProjectNumber" style="height: 100px; overflow: scroll;">
                                                                                </div>--%>
                                                                                <Ajax:AutoCompleteExtender ID="AutoCompleteExtenderProj" runat="server" BehaviorID="AutoCompleteExt1"
                                                                                        CompletionListElementID="divProjectNumber" EnableCaching="false" FirstRowSelected="True"
                                                                                        MinimumPrefixLength="1" ServiceMethod="GetProjectListCompletion" ServicePath="~/AutoComplete.asmx"
                                                                                        TargetControlID="txtProjectNumber" CompletionSetCount="10">
                                                                                    
                                                                                    </Ajax:AutoCompleteExtender>
                                                                                    
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="formtdlt" valign="top">
                                                                                    Bid Package #
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:TextBox ID="txtBidPackageNo" runat="Server" CssClass="ajaxtxtbox" 
                                                                                        MaxLength="15"></asp:TextBox>
                                                                                   <%-- <div id="divBidPackage" style="height: 100px; overflow: scroll;">
                                                                                    </div>--%>
                                                                                    <!-- Modified by N Schoenberger on 8/17/2011 -->
                                                                                    <Ajax:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" BehaviorID="AutoCompleteEx1"
                                                                                        CompletionListElementID="divBidPackage" EnableCaching="false" FirstRowSelected="True"
                                                                                        MinimumPrefixLength="1" ServiceMethod="GetActiveBidPackageList" ServicePath="~/AutoComplete.asmx"
                                                                                        TargetControlID="txtBidPackageNo" CompletionSetCount="10">
                                                                                        <%--<Animations>
                                                            <OnShow>
                                                              <Sequence>
                                                                <OpacityAction Opacity="0" />
                                                                <HideAction Visible="true" />
                                                            
                                                                    <ScriptAction Script="
                                                                        var behavior = $find('AutoCompleteEx1');
                                                                        if (!behavior._height) {
                                                                            var target = behavior.get_completionList();
                                                                            behavior._height = target.offsetHeight - 2;
                                                                            target.style.height = '0px';
                                                                        }" />
                                                                <Parallel Duration=".4">
                                                                <FadeIn />
                                                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                                                                </Parallel>
                                                                </Sequence>
                                                                 </OnShow>
                                                                  <OnHide>
                                                                 <Parallel Duration=".4">
                                                                 <FadeOut />
                                                                 <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                                                                 </Parallel>
                                                                 </OnHide>
                                                                                        </Animations>--%>
                                                                                    </Ajax:AutoCompleteExtender>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td class="formtdrt">
                                                                                    <asp:ImageButton ID="imbSearch" runat="server" ImageUrl="~/Images/search.jpg" ToolTip="Search"
                                                                                        OnClick="imbSearch_Click" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" class="bodytextcenter">
                                                                        <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" id="ContentTd" runat="server" style="display: none" width="100%">
                                                                        <table width="100%" cellpadding="3" cellspacing="0" id="tblContent" runat="server">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:HiddenField ID="hdnCount" runat="server" />
                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="bodytextleft" valign="middle">
                                                                                                Total Number of Records:&nbsp;
                                                                                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextright" valign="middle">
                                                                                                Number of Records Per Page:&nbsp;&nbsp;
                                                                                            </td>
                                                                                            <td width="5%">
                                                                                                <asp:DropDownList ID="ddlNumber" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlNumber_SelectedIndexChanged"
                                                                                                    CssClass="ddlbox">
                                                                                                    <asp:ListItem>10</asp:ListItem>
                                                                                                    <asp:ListItem>20</asp:ListItem>
                                                                                                    <asp:ListItem>30</asp:ListItem>
                                                                                                    <asp:ListItem>40</asp:ListItem>
                                                                                                    <asp:ListItem>50</asp:ListItem>
                                                                                                    <asp:ListItem>100</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="5%">
                                                                                    <asp:GridView ID="grvRiskEvaluation" runat="server" CellPadding="3" GridLines="none"
                                                                                        Width="100%" AllowSorting="true" CellSpacing="1" DataKeyNames="PK_RiskId" AutoGenerateColumns="False"
                                                                                        OnRowDataBound="grvRiskEvaluation_RowDataBound" OnSorting="grvRiskEvaluation_Sorting">
                                                                                        <RowStyle CssClass="listone" />
                                                                                        <AlternatingRowStyle CssClass="listtwo" />
                                                                                        <HeaderStyle CssClass="listheading" />
                                                                                        <Columns>
                                                                                           <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White" ItemStyle-Width="15px" >
                                                                                                <ItemTemplate >
                                                                                                    <asp:CheckBox ID="chkVendor" runat="server"/>
                                                                                                </ItemTemplate>
                                                                                          <HeaderTemplate>
                                                                                    <asp:CheckBox onclick="javascript:CheckAll(this);" id="chkAll" runat="server"></asp:CheckBox>

                                                                                </HeaderTemplate>
                                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Project Number" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                                                ItemStyle-CssClass="gridpad" SortExpression="ProjectNumber" HeaderStyle-Width="220px">
                                                                                                <ItemTemplate>
                                                                                                    <%#Eval("ProjectNumber")%>
                                                                                                    /
                                                                                                    <asp:Label ID="lblProjectDescription" runat="server" Text='<%#this.GetDescriptionSRE(Convert.ToString(Eval("ProjectDescription"))) %>'
                                                                                                        ToolTip='<%#Eval("ProjectDescription") %>'></asp:Label>
                                                                                                    <%--   <%#this.GetDescription(Convert.ToString(Eval("ProjectDescription")))%>--%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                             <%--Added by G. Vera - Phase 3 Item 4.8 (JIRA:VPI6)--%>                                                                                            
                                                                                            <%--<asp:TemplateField HeaderText="Risk">
                                                                                                <HeaderStyle ForeColor="White"/>
                                                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                <ItemTemplate>
                                                                                                    <asp:Image ID="imgRiskFlag" runat="server" ImageAlign="AbsMiddle"/>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>--%>
                                                                                            <asp:TemplateField HeaderText="Vendor Name" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                                                ItemStyle-CssClass="gridpad" SortExpression="Company">
                                                                                                <ItemTemplate>
                                                                                                    <%#Eval("Company") %>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Bid Package #" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                                                ItemStyle-CssClass="gridpad" SortExpression="BidPackageNumber">
                                                                                                <ItemTemplate>
                                                                                                    <%#Eval("BidPackageNumber")%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                          <asp:TemplateField HeaderText="SRE Status" SortExpression="SREStatus" HeaderStyle-ForeColor="White" HeaderStyle-Width="70px" ItemStyle-CssClass="gridpad" >
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblSREStatus1" runat="server" Text='<%#Eval("SREStatus") %>' ></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td  class="bodytextcenter"  >
                                                                                    Page&nbsp;&nbsp;
                                                                                    <asp:ImageButton ID="imbPrevious" runat="server" ImageUrl="~/Images/pre.jpg" OnClick="imbPrevious_Click"
                                                                                       />&nbsp;&nbsp;
                                                                                    <asp:PlaceHolder ID="plcCrntPage" runat="server"></asp:PlaceHolder>
                                                                                    of
                                                                                    <asp:Label ID="lblTotalPage" runat="server"></asp:Label>
                                                                                    &nbsp;&nbsp;<asp:ImageButton ID="imbNext" runat="server" ImageUrl="~/Images/next1.jpg"
                                                                                        OnClick="imbNext_Click" style="height: 7px; width: 7px" />
                                                                                    <asp:HiddenField ID="hdnPage" runat="server" />
                                                                                    <asp:HiddenField ID="hdnCurrentPage" runat="server" />
                                                                                    <asp:HiddenField ID="hdnSort" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" class="tdheight">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="40%" class="tdheight">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:ImageButton ID="imbSearchSRE" ImageUrl="~/Images/select.jpg" ImageAlign="AbsMiddle"
                                                                            runat="server" ToolTip="Select SRE" Visible="false" 
                                                                            OnClick="imbSearchSRE_Click" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" class="tdheight">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" id="SRETd" runat="server" style="display: none" width="100%">
                                                                        <table width="100%" cellpadding="3" cellspacing="0" id="Table1" runat="server">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:HiddenField ID="hdnCount1" runat="server" />
                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="bodytextleft" valign="middle">
                                                                                                Total Number of Records:&nbsp;
                                                                                                <asp:Label ID="lblTotal1" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextright" valign="middle">
                                                                                                Number of Records Per Page:&nbsp;&nbsp;
                                                                                            </td>
                                                                                            <td width="5%">
                                                                                                <asp:DropDownList ID="SREddlNumber" runat="server" AutoPostBack="True" CssClass="ddlbox"
                                                                                                    OnSelectedIndexChanged="SREddlNumber_SelectedIndexChanged">
                                                                                                    <asp:ListItem>10</asp:ListItem>
                                                                                                    <asp:ListItem>20</asp:ListItem>
                                                                                                    <asp:ListItem>30</asp:ListItem>
                                                                                                    <asp:ListItem>40</asp:ListItem>
                                                                                                    <asp:ListItem>50</asp:ListItem>
                                                                                                    <asp:ListItem>100</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trSRE" runat="server" style="display: none;">
                                                                                <td colspan="2">
                                                                                    <asp:HiddenField ID="hdnUnlockClicked" runat="server" />  <%--G. vera 03/22/2013 - added--%>
                                                                                    <asp:GridView ID="grvSRE" runat="server" CellPadding="4" GridLines="None" Width="100%"
                                                                                        CellSpacing="1" DataKeyNames="PK_RiskId,FK_VendorId,EditStatus,Statusid,LockedBy,FilePath,PdfName"
                                                                                        AutoGenerateColumns="False" AllowSorting="True" OnRowDataBound="grvSRE_RowDataBound"
                                                                                        OnSorting="grvSRE_Sorting" OnRowCommand="grvSRE_RowCommand" EnableViewState="true">
                                                                                        <RowStyle CssClass="listone" />
                                                                                        <AlternatingRowStyle CssClass="listtwo" />
                                                                                        <HeaderStyle CssClass="listheading" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="Project Number" HeaderStyle-ForeColor="White" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"  
                                                                                                SortExpression="ProjectNumber" HeaderStyle-Wrap="true" >
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblProjectNumber" runat="server" Text='<%#Eval("ProjectNumber") %>'
                                                                                                        ToolTip='<%#Eval("ProjectNumber") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle ForeColor="White" Wrap="True" Width="100px"  />
                                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                                            </asp:TemplateField>
                                                                                            <%--    <asp:BoundField HeaderText="Project Number" DataField="ProjectNumber" ItemStyle-HorizontalAlign="Center" SortExpression="ProjectNumber"
                                                                                    HeaderStyle-ForeColor="White" >
                                                                                    <HeaderStyle ForeColor="White" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundField>--%>
                                                                                            <asp:TemplateField HeaderText="Project Name" HeaderStyle-ForeColor="White" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"  
                                                                                                SortExpression="ProjectDescription"  HeaderStyle-Width="130px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblProjectDescription" runat="server" Text='<%#this.GetDescription(Convert.ToString(Eval("ProjectDescription"))) %>'
                                                                                                        ToolTip='<%#Eval("ProjectDescription") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle ForeColor="White" />
                                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                                            </asp:TemplateField>
                                                                                            <%--<asp:BoundField DataField="ProjectDescription" HeaderText="Project Name"  SortExpression="ProjectDescription" HeaderStyle-ForeColor="White"    />--%>
                                                                                            <%--              <asp:BoundField DataField="Company" HeaderText="Vendor Name"  SortExpression="Company" HeaderStyle-ForeColor="White" />--%>
                                                                                            <asp:TemplateField HeaderText="Vendor Name" HeaderStyle-ForeColor="White" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"  
                                                                                                SortExpression="Company">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblCompany" runat="server" Text='<%#this.GetDescriptionSRE(Convert.ToString(Eval("Company"))) %>' ToolTip='<%#Eval("Company") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle ForeColor="White" Width="110px" />
                                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                                            </asp:TemplateField>
                                                                                            <%--    <asp:BoundField HeaderText="Bid Package #" DataField="BidPackageNumber" HeaderStyle-Wrap="true"   />--%>
                                                                                            <asp:TemplateField HeaderText="Bid Package #" HeaderStyle-ForeColor="White" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"  
                                                                                                SortExpression="BidPackageNumber"  >
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblBidPackageNumber" runat="server" Text='<%#Eval("BidPackageNumber") %>'
                                                                                                        ToolTip='<%#Eval("BidPackageNumber") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle ForeColor="White" Width="120px" Wrap="True" />
                                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="VQF" HeaderStyle-ForeColor="White" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left"  >
                                                                                                <ItemTemplate>
                                                                                                    <asp:LinkButton ID="lnkVendor" runat="server" CommandName="VQF" CommandArgument='<%#Eval("FK_VendorId") %>'
                                                                                                        Text="VQF" ToolTip="VQF"  ></asp:LinkButton>
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle ForeColor="White" />
                                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:BoundField HeaderText="VQF Status" DataField="VQFStatus" SortExpression="VQFStatus"
                                                                                                HeaderStyle-ForeColor="White" HeaderStyle-Width="70px"   >
                                                                                                <HeaderStyle ForeColor="White" />
                                                                                            </asp:BoundField>
                                                                                            <asp:TemplateField HeaderText="SRE" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"  >
                                                                                                <ItemTemplate>
                                                                                                    <asp:LinkButton ID="lnkSRE" runat="server" CommandName="SRE" CommandArgument='<%#Eval("PK_RiskId") %>'
                                                                                                        Text="SRE" ToolTip="SRE" ></asp:LinkButton>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                            </asp:TemplateField>
                                                                                            <%--<asp:BoundField HeaderText="SRE Status" DataField="SREStatus" />--%>
                                                                                            <asp:TemplateField HeaderText="SRE Status" SortExpression="SREStatus" HeaderStyle-ForeColor="White" HeaderStyle-Width="70px" >
                                                                                                <HeaderStyle ForeColor="White"/>
                                                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblSREStatus" runat="server" Text='<%#Eval("SREStatus") %>' ToolTip='<%#Eval("ToolTip") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <%--Added by G. Vera - Phase 3 Item 4.8 (JIRA:VPI6)--%>                                                                                            
                                                                                            <%--<asp:TemplateField HeaderText="Risk">
                                                                                                <HeaderStyle ForeColor="White"/>
                                                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                <ItemTemplate>
                                                                                                    <asp:Image ID="imgRiskFlag" runat="server" ImageAlign="AbsMiddle"/>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>--%>
                                                                                            <asp:TemplateField HeaderText="SRE Lock Status">
                                                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                <ItemTemplate>
                                                                                                    <asp:Image ID="imgLock" runat="server" ImageAlign="AbsMiddle" ToolTip='<%#Eval("EmployeeName") %>' Visible="true"/>
                                                                                                    <asp:LinkButton ID="imbLock" runat="server" CommandName="Unlock" CommandArgument='<%#Bind("PK_RiskId") %>' Visible="false">
                                                                                                        <asp:Image ID="imbLockImage" runat="server" />
                                                                                                    </asp:LinkButton>
                                                                                                    
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField>
                                                                                                <HeaderTemplate>
                                                                                                    <table cellpadding="4" cellspacing="1">
                                                                                                        <tr>
                                                                                                            <td colspan="2">
                                                                                                                Snapshot
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                VQF
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                SRE
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <table cellpadding="4" cellspacing="1">
                                                                                                        <tr>
                                                                                                            <td align="left" width="20px" >
                                                                                                          <%-- <asp:ImageButton ID="imbVQFSnapshot" runat="server" CommandName="VQFSnapshot" ImageAlign="AbsMiddle"
                                                                                                                    Visible="true" ImageUrl="~/Images/vqf.png" CommandArgument='<%#Eval("PdfName") %>' ToolTip="VQF Snapshot"  />--%>
                                                                                                    <asp:LinkButton ID="imbVQFSnapshot" runat="server" CommandName="VQFSnapshot" CommandArgument='<%#Eval("PdfName") %>'
                                                                                                         ToolTip="VQF Snapshot" ><img src="../Images/vqf.png" alt="VQF Snapshot"    />  </asp:LinkButton>
                                                                                                       
                                                                                                            </td>
                                                                                                            <td align="right">
                                                                                                               <%-- <asp:ImageButton ID="imbSRESnapshot" runat="server" CommandName="SRESnapshot" ImageAlign="AbsMiddle"
                                                                                                                    Visible="true" ImageUrl="~/Images/sre.png" CommandArgument='<%#Eval("FilePath") %>' ToolTip="SRE Snapshot"  />
                                                                                                                    --%>
                                                                                                                      <asp:LinkButton ID="imbSRESnapshot" runat="server" CommandName="SRESnapshot" CommandArgument='<%#Eval("FilePath") %>'
                                                                                                         ToolTip="SRE Snapshot" ><img src="../Images/sre.png" alt="SRE Snapshot"  />  </asp:LinkButton>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytextcenter">
                                                                                    Page&nbsp;&nbsp;
                                                                                    <asp:ImageButton ID="imbSREPrevious" runat="server" ImageUrl="~/Images/pre.jpg" 
                                                                                        OnClick="imbSREPrevious_Click" />&nbsp;&nbsp;
                                                                                    <asp:PlaceHolder ID="plcCrntPage1" runat="server"></asp:PlaceHolder>
                                                                                    of
                                                                                    <asp:Label ID="lblTotalPage1" runat="server"></asp:Label>
                                                                                    &nbsp;&nbsp;<asp:ImageButton ID="imbSRENext" runat="server" ImageUrl="~/Images/next1.jpg"
                                                                                        OnClick="imbSRENext_Click" />
                                                                                    <asp:HiddenField ID="hdnPage1" runat="server" />
                                                                                    <asp:HiddenField ID="hdnCurrentPage1" runat="server" />
                                                                                    <asp:HiddenField ID="hdnSort1" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trSREStatus" runat="server" style="display: none;">
                                                                                <td colspan="2">
                                                                                    <img src="../Images/unlock.gif" alt="" />
                                                                                    &#45; UnLocked
                                                                                    <br />
                                                                                    <img src="../Images/lock1.gif" alt="" />
                                                                                    &#45; Locked by other PM
                                                                                    <br />
                                                                                    <img src="../Images/lock2.gif" alt="" />
                                                                                    &#45; SRE Awarded, SRE Not Awarded
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:HiddenField ID="hdnVendorIds" runat="server" />
                                                            <asp:HiddenField ID="hdnLockedUser" runat="server" />
                                                            <Ajax:ModalPopupExtender ID="mpopAlert" runat="server" BackgroundCssClass="popupbg"
                                                                TargetControlID="hdnAlert" PopupControlID="divAlert" OkControlID="imbOK" RepositionMode="RepositionOnWindowResize">
                                                            </Ajax:ModalPopupExtender>
                                                            <div id="divAlert" runat="server" class="popupdivbg1" style="display: none">
                                                                <table cellspacing="1" cellpadding="3" width="100%">
                                                                    <tr>
                                                                        <td class="Messageheading">
                                                                            Message
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="popupdivcl" style="padding-right: 0px;">
                                                                            <asp:Label ID="lblMessage" CssClass="errormsg" runat="server" Text="Locked by another user"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr align="center">
                                                                        <td>
                                                                            &nbsp;&nbsp;&nbsp;<asp:ImageButton ID="imbOK" ImageUrl="~/Images/ok.jpg" ImageAlign="AbsMiddle"
                                                                                runat="server" OnClick="imbOK_Clicked" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <asp:HiddenField ID="hdnAlert" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                           <%-- </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

<script type="text/javascript">

    //G. Vera 03/22/2013 - Added
    function AlertWhenUnlock() {
        document.getElementById("<%=hdnUnlockClicked.ClientID%>").value = "1";
        document.getElementById("<%=lblMessage.ClientID%>").value = "Unlocking Test";
        $find('<%=mpopAlert.ClientID%>').show();
        return false;
    }


    function selectVendor(sender, id) {
        var ids = document.getElementById("<%=hdnVendorIds.ClientID%>").value;
        if (sender.checked) {
            ids = ids + id + ",";
        }
        else {
            if (ids.indexOf(id + ",") != -1) {
                ids = ids.replace(id + ",", "");
            }
        }
        document.getElementById("<%=hdnVendorIds.ClientID%>").value = ids;
    }
    function validate() {
        if (document.getElementById("<%=hdnVendorIds.ClientID%>").value == "") {
            alert("Please select at least one SRE");
            return false;
        }
        return true;
    }
    function GetValue(i) {
        var str = document.getElementById("<%=hdnPage.ClientID%>");
        str.value = i;
    }
    function GetValue1(i) {
        var str = document.getElementById("<%=hdnPage1.ClientID%>");
        str.value = i;
    }
</script>
<script language="javascript">

    function CheckAll(spanChk) {
        var oItem = spanChk.children;
        var theBox = (spanChk.type == "checkbox") ? spanChk : spanChk.children.item[0];
        xState = theBox.checked;

        elm = theBox.form.elements;
        for (i = 0; i < elm.length; i++)
            if (elm[i].type == "checkbox" && elm[i].id != theBox.id) {
            //elm[i].click();
            if (elm[i].checked != xState)
                elm[i].click();
            //elm[i].checked=xState;
        }

    }
   
		</script>

