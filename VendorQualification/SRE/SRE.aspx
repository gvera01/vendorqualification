﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SRE.aspx.cs" Inherits="SRE" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Haskell</title>
    <link href="../css/Haskell.css" rel="Stylesheet" />
    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
        
<style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top:0px;
        }
        </style>    
        <script type="text/javascript">
            function ClientItemSelected(sender, args) {
                document.getElementById("<%=hdnVendorId.ClientID%>").value = args.get_value();

            }
            function validateSRE() {
                var msg = "";
                if (document.getElementById("<%=hdnVendorId.ClientID%>").value == "") {
                    msg += "Please select Vendor Name\n";
                }
                if (document.getElementById("<%=ddlProjectNumber.ClientID%>").value == "") {
                    msg += "Please select Project name";
                }
                if (document.getElementById("<%=txtBidPackage.ClientID%>").value == "") {
                    msg += "Please enter Bid Package Number";
                }
                if (msg != "") {
                    //alert(msg);
                    document.getElementById("<%=lblMessage.ClientID%>").value = msg;
                    //document.getElementById("<%=mpopAlert.ClientID%>").show();
                    return false;
                }
                return true;
            }
</script>
</head>

<body>
    <form id="form1" runat="server" defaultbutton="imbStartSRE" defaultfocus="txtVendor">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
   
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign=bottom>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                        
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="formhead">
                                                    Subcontractor Risk Evaluation
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight" align="right">
                                                <br />
                                                    <asp:ImageButton ID="imgback" runat="server" ImageUrl="~/Images/back.jpg" ToolTip="Back"
                                                        OnClick="imgback_Click" TabIndex="6" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="tdButtons" runat="server" style="display: none">
                                                    <asp:ImageButton ID="imbAdd" runat="server" ImageUrl="~/Images/addnew.jpg" OnClick="imbAdd_Click"
                                                        ToolTip="Add New Subcontractor Risk Evaluation" TabIndex="4" />
                                                    &nbsp;
                                                    <asp:ImageButton ID="imbExists" runat="server" ImageUrl="~/Images/viewexist.jpg"
                                                        ToolTip="View Existing Subcontractor Risk Evaluation" 
                                                        OnClick="imbExists_Click" TabIndex="5" />
                                                    <asp:HiddenField ID="hdnClick" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                           <%-- <asp:UpdatePanel ID="updSREPanel" runat="server">
                                            <ContentTemplate>--%>
                                                <td id="Vendortd" runat="server" style="display: none" class=even_pad >
                                                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                        <tr>
                                                            <td class="formtdlt" width="41%" valign="top">&nbsp;<span class="mandatorystar">*</span>Vendor Name from Vendor Database
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtVendor" runat="server" CssClass="ajaxtxtbox" MaxLength="60" 
                                                                    Enabled="False" TabIndex="0"></asp:TextBox>
                                                                 
                                                               <%--<div id="listPlacement" style="height: 100px; overflow : hidden;">
                                                                </div>--%>
                                                                <Ajax:AutoCompleteExtender ID="autoComplete1" runat="server" BehaviorID="AutoCompleteEx"
                                                                    CompletionListElementID="listPlacement" EnableCaching="false" FirstRowSelected="True" 
                                                                    MinimumPrefixLength="1" ServiceMethod="GetVendorList" ServicePath="~/AutoComplete.asmx"
                                                                    TargetControlID="txtVendor" CompletionSetCount="10" OnClientItemSelected="ClientItemSelected" CompletionInterval="500">
                                                                    <%--<Animations>
                                                            <OnShow>
                                                              <Sequence>
                                                                <OpacityAction Opacity="0" />
                                                                <HideAction Visible="true" />
                                                                  <ScriptAction Script="
                                                                    var behavior = $find('AutoCompleteEx');
                                                                    if (!behavior._height) {
                                                                        var target = behavior.get_completionList();
                                                                        behavior._height = target.offsetHeight - 2;
                                                                        target.style.height = '0px';
                                                                    }" />
                                                                <Parallel Duration=".4">
                                                                <FadeIn />
                                                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                                                                </Parallel>
                                                                </Sequence>
                                                                 </OnShow>
                                                                  <OnHide>
                                                                 <Parallel Duration=".4">
                                                                 <FadeOut />
                                                                 <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                                                                 </Parallel>
                                                                 </OnHide>
                                                                    </Animations>--%>
                                                                </Ajax:AutoCompleteExtender>
                                                                <asp:HiddenField ID="hdnMatch" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdlt">&nbsp;<span class="mandatorystar">*</span>Project Number
                                                            </td>
                                                            <td class="formtdrt">

                                                                <%--<asp:DropDownList ID="ddlProjectNumber" runat="Server" CssClass="ddlbox" 
                                                                    Width="367px" TabIndex="1"></asp:DropDownList>--%>
                                                                    <asp:TextBox ID="ddlProjectNumber" runat="server" CssClass="ajaxtxtbox" MaxLength="12" TabIndex="1" />
                                                                    <%--<div id="divProjectNumber" style="height: 100px; overflow: scroll;">
                                                                    </div>--%>
                                                                    <Ajax:AutoCompleteExtender ID="AutoCompleteExtenderProj" runat="server" BehaviorID="AutoCompleteExt1"
                                                                            CompletionListElementID="divProjectNumber" EnableCaching="false" FirstRowSelected="True"
                                                                            MinimumPrefixLength="1" ServiceMethod="GetProjectListCompletion" ServicePath="~/AutoComplete.asmx"
                                                                            TargetControlID="ddlProjectNumber" CompletionSetCount="10">
                                                                                    
                                                                    </Ajax:AutoCompleteExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdlt">&nbsp;<span class="mandatorystar">*</span>Bid Package Number</td>
                                                            <td class="formtdrt">
                                                             <asp:TextBox ID="txtBidPackage" runat="server" CssClass="ajaxtxtbox" 
                                                                    MaxLength="15" TabIndex="2"></asp:TextBox>

                                                                </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td class="formtdrt">
                                                            <br />
                                                                <asp:ImageButton ID="imbStartSRE" ImageUrl="~/Images/startsre.jpg" ImageAlign="AbsMiddle"
                                                                    ToolTip="Start Subcontractor Risk Evaluation" runat="server" OnClick="imbStartSRE_Click"
                                                                    TabIndex="3" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td><%--</ContentTemplate> </asp:UpdatePanel> --%>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                    <tr><td>
                    <Ajax:ModalPopupExtender ID="mpopAlert" runat="server" BackgroundCssClass="popupbg"
        TargetControlID="hdnAlert" PopupControlID="divAlert" OkControlID="imbOK">
    </Ajax:ModalPopupExtender>
    <div id="divAlert"  class="popupdivbg1" style="display: none">
    <table cellspacing="1" cellpadding="3" width="100%">
        <tr>
            <td class="Messageheading">
                Message
            </td>
        </tr>
        <tr>
            <td class="tdheight">
            </td>
        </tr>
        <tr>
            
            <td class="popupdivcl" style="padding-right: 0px;">
                        <asp:Label ID="lblMessage" CssClass="errormsg" runat="server"></asp:Label>
                </td>
            
        </tr>
        <tr>
            <td class="tdheight">
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;&nbsp;&nbsp;<asp:ImageButton ID="imbOK" ImageUrl="~/Images/ok.jpg" ImageAlign="AbsMiddle" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="tdheight">
            </td>
        </tr>
    </table>
    </div>
    <asp:HiddenField ID="hdnVendorId" runat="server" />
     <asp:HiddenField ID="hdnpage" runat="server" />
    <asp:HiddenField ID="hdnAlert" runat="server" />
                    
                    </td></tr>
                </table>
            </td>
        </tr>
    </table>
  
    </form>
</body>
</html>

