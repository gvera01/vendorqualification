﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/*********************************************************************************************************
 * Change Comments
 * ================
 * 001 G. Vera 10/01/2012: Modification (JIRA:VPI-69 & VPI-71)
 * 002 G. Vera 09/21/2012: Modification (JIRA:VPI-69, VPI-71, & VPI-70)
 * 003 Sooraj Sudhakaran.T 23/09/2013: - Change SRE Calculation for Non-US vendors and display Label 
 * 004 G. Vera 03/25/2015: A new financial year was added therefore need to modify the text that is using the max largest year from database
 * 005 G. Vera 01/09/2016: New Questions
 * 006 G. Vera 11/29/2018: Enable risk management to be handled in app and not in printed paper.
 * 007 G. Vera 05/18/2020: Diable SSDI Program checklist item in the CheckBoxListItemsScripts() method, change the 
 *                           DOC signature requirement treshold to $250,000
 *********************************************************************************************************/

public partial class SRE_PrintSRE : System.Web.UI.Page
{

    #region Instantiate the class and declare variables

    DataSet SREDetails;
    private Int64 riskId = 0;
    private bool IsNonUSVendor = false; //003
    RiskEvaluation riskEvaluation = new RiskEvaluation();
    public DataSet Lookup = new DataSet();

    #endregion

    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        long RISKID = 0;
        int statusId = -1;
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["RiskID"])))
        {
           RISKID = Convert.ToInt64(Request.QueryString["RiskID"]);
           statusId = Convert.ToInt32(Request.QueryString["statusidprint"]);
        }
        else if (!string.IsNullOrEmpty(Convert.ToString(Session["riskId"])))
        {
            RISKID = Convert.ToInt64(Session["riskId"]);
            statusId = Convert.ToInt32(Session["statusidprint"]);
        }
        else
        {
            statusId = -1;
            RISKID = -1;
        }
        riskId = RISKID;
        //002
        LoadRiskManagmentChecklist();

        //G. Vera 09/05/2012 - Hot Fix on Lookup percentage value found after release of 1.2 version.
        // has been incorrect since delivery oif this product
        SREDetails = riskEvaluation.GetSelectedRiskDetail(RISKID);
        statusId = (SREDetails.Tables[0].Rows.Count > 0 ) ? Convert.ToInt32(SREDetails.Tables[0].Rows[0]["StatusId"]) : 0;  //001 - bug
        lblOriginator.Text = "Originated By: ";  //001 - added

        if (statusId == 2 || statusId == 4 || statusId == 5)
            Lookup = riskEvaluation.GetSRELookUpData(RISKID);
        else
        {
            Lookup = riskEvaluation.GetSRELookUpData(-1);
        }
        BusinessYear.InnerText = "Has the Subcontractor been in business less than " + Lookup.Tables[0].Rows[0]["BusinessYear"].ToString() + " years?";
        LargestMaxPercentage.InnerText = "Does Subcontractor's bid exceed their largest maximum contract value by " + Lookup.Tables[0].Rows[0]["LargestMaxPercentage"].ToString() + "%?";
        LargestMaxPastYear.InnerText = "Largest Max Contract (over past " + (Convert.ToInt32(Lookup.Tables[0].Rows[0]["LargestMaxPastYear"]) - 1).ToString() + " yrs):";        //004
        AannualRevenuePastYear.InnerText = "Is Subcontractor's current total backlog > the " + Lookup.Tables[0].Rows[0]["AannualRevenuePastYear"].ToString() + " year average annual revenue?";
        AannualRevenuePastYear1.InnerText = Lookup.Tables[0].Rows[0]["AannualRevenuePastYear"].ToString() + " Year Average Annual Revenue:";
        BidAannualRevenuePercentage.InnerText = "Is Subcontractor's bid > " + Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"].ToString() + "% of " + Lookup.Tables[0].Rows[0]["BidAannualRevenuePastYear"].ToString() + " year average annual revenue?";
        LowestbidPercentage.InnerText = "Is the Subcontractor's bid " + Lookup.Tables[0].Rows[0]["LowestbidPercentage"].ToString() + "% or more lower than next bidder?";
        EMRPastYear.InnerText = "Is EMR history in any of past " + (Convert.ToInt32(Lookup.Tables[0].Rows[0]["EMRPastYear"].ToString()) -1 ) + " years greater than " + Lookup.Tables[0].Rows[0]["EMRValue"].ToString() + "?";


        if (SREDetails.Tables[0].Rows.Count > 0)    //001 - bug
        {
            LoadRiskDetails(SREDetails);
            if (SREDetails.Tables[0].Rows.Count > 0)
            {
                var vendor = riskEvaluation.GetVendorDetail(Convert.ToInt64(SREDetails.Tables[0].Rows[0]["FK_VendorID"].ToString()));
                txtVendorName.Text = vendor.Company;
                IsNonUSVendor = !vendor.IsBasedInUS;
                CheckIfNonUSAndApplyLabel(); //003
                txtVendorContactName.Text = vendor.FirstName + " " + vendor.LastName;
                if (SREDetails.Tables[0].Rows[0]["SREModifiedDate"].ToString() == "")
                    txtDate.Text = DateTime.Now.ToString();
                else
                    txtDate.Text = SREDetails.Tables[0].Rows[0]["SREModifiedDate"].ToString();
                txtVendorContactName.Text = SREDetails.Tables[0].Rows[0]["contactName"].ToString();

                if (txtVendorContactName.Text == "")
                {
                    txtVendorContactName.Text = vendor.FirstName + " " + vendor.LastName;
                }

                //txtDate.Text = SREDetails.Tables[0].Rows[0]["SREModifiedDate"].ToString();
                txtProjectNumber.Text = SREDetails.Tables[0].Rows[0]["ProjectNumber"].ToString().TrimStart();
                txtVendorQualificationDate.Text = SREDetails.Tables[0].Rows[0]["VendorQualificationDate"].ToString();
                txtProjectName.Text = riskEvaluation.GetProjectName(SREDetails.Tables[0].Rows[0]["ProjectNumber"].ToString());
                txtAppSubContractorValue.Text = SREDetails.Tables[0].Rows[0]["AppSubcontractorValue"].ToString();

                txtMaxContract3Year.Text = SREDetails.Tables[0].Rows[0]["LargestMaxContract"].ToString();
                txtCurrentTotalBacklog.Text = SREDetails.Tables[0].Rows[0]["CurrentBacklog"].ToString();

                txtAverageAnnualRevenue.Text = SREDetails.Tables[0].Rows[0]["AverageAnnualRevenue"].ToString();
                txtAverageAnnualRevenue.Text = string.Format("{0:#,0}", Convert.ToDouble((txtAverageAnnualRevenue.Text).ToString()));
                txtAvailBondingCapacity.Text = SREDetails.Tables[0].Rows[0]["AvailableBondingCapacity"].ToString();
            }
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //sb.Append(@"<script language='javascript'>");
            //sb.Append(@"window.print();");

            //sb.Append(@"</script>");

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "JSCR", sb.ToString(), false);

        }
    }
    #endregion

    #region Methods

    /// <summary>
    /// 003 - Display label for Non US Vendors that have rate as N/A
    /// </summary>
    private void CheckIfNonUSAndApplyLabel()
    {

        if (IsNonUSVendor)  
        if (txtRate1.Text == "N/A" && txtRate2.Text == "N/A" && txtRate3.Text == "N/A")
        {
            trNonUSVendor.Style["display"] = "";
        }

    }

    /// <summary>
    /// Bind the SRE Details Method
    /// </summary>
    /// <param name="SREDetails"></param>
    private void LoadRiskDetails(DataSet SREDetails)
    {
        if (SREDetails.Tables[0].Rows.Count > 0)
        {
            //005 - START
            long vendorID = Convert.ToInt64(SREDetails.Tables[0].Rows[0]["FK_VendorID"].ToString());
            string conductsHazardAnalysis = string.Empty;
            var safety = new clsSafety().GetSingleVQFSafetyData(vendorID);
            if (safety != null)
            {
                conductsHazardAnalysis = (safety.IsConductHazardAnalysis != null) ?
                (((bool)safety.IsConductHazardAnalysis) ? "Vendor conducts job hazard analysis or task hazard analysis on a " +
                                                          safety.ConductHazardAnalysisPeriods.Replace("|", ", ").Replace("?", "") + " basis.<br/><i>(Not included in SRE calculation)</i>"
                                                          : "Vendor does not conduct job hazard analysis or task hazard analysis.<br/><i>(Not included in SRE calculation)</i>") : "";
            }
            ltrlHazardAnalysis.Text = conductsHazardAnalysis;
            //005 - END
            DataRow dataRow = SREDetails.Tables[0].Rows[0];
            txtBidpackageNumber.Text = dataRow["BidPackageNumber"].ToString();
            txtBidpackageDesc.Text = dataRow["BidPackageDescription"].ToString();
            txtAppSubContractorValue.Text = dataRow["AppSubcontractorValue"].ToString();

            CheckYesNoRadio(dataRow["Business1"], rdoBusiness1Yes, rdoBusiness1No);
            CheckYesNoRadio(dataRow["Business2"], rdoBusiness2Yes, rdoBusiness2No);
            CheckYesNoRadio(dataRow["Business3"], rdoBusiness3Yes, rdoBusiness3No);
            CheckYesNoRadio(dataRow["Business4"], rdoBusiness4Yes, rdoBusiness4No);
            CheckYesNoRadio(dataRow["Business5"], rdoBusinessLitigationYes, rdoBusinessLitigationNo);     //005
            CheckRiskIndicator(dataRow["BusinessRiskIndicator"], chkBusinessRiskIndicator);

            txtNextLowestBid.Text = dataRow["NextLowestBid"].ToString();
            txtSubcontractorCurrentBid.Text = dataRow["SubcontractorCurrentBid"].ToString();

            CheckYesNoRadio(dataRow["Financial1"], rdoFinancial1Yes, rdoFinancial1No);
            CheckYesNoRadio(dataRow["Financial2"], rdoFinancial2Yes, rdoFinancial2No);
            CheckYesNoRadio(dataRow["Financial3"], rdoFinancial3Yes, rdoFinancial3No);
            CheckYesNoRadio(dataRow["Financial4"], rdoFinancial4Yes, rdoFinancial4No);
            CheckYesNoRadio(dataRow["Financial5"], rdoFinancial5Yes, rdoFinancial5No);
            CheckRiskIndicator(dataRow["FinancialRiskIndicator"], chkFinancialRiskIndicator);

            // ***** Modified/ Added by N Schoenberger 11/16/2011 *****
            //txtPMNotes.Text = dataRow["PMVerificationNotes"].ToString();
            string PMVerificationNotes = dataRow["PMVerificationNotes"].ToString();
            PMVerificationNotes = PMVerificationNotes.Replace("\r\n", "<BR/>");
            PMVerificationNotes = PMVerificationNotes.Replace(" ", " &nbsp;");
            lblPMVerificationNotes.InnerHtml = PMVerificationNotes;
            //***** End modification *****

            txtVerificationDate.Text = dataRow["PMVerificationDate"].ToString();
            CheckYesNoRadio(dataRow["License"], rdoLicense1Yes, rdoLicense1No);

            CheckYesNoRadio(dataRow["Insurance1"], rdoInsurance1Yes, rdoInsurance1No);
            CheckYesNoRadio(dataRow["Insurance2"], rdoInsurance2Yes, rdoInsurance2No);
            CheckYesNoRadio(dataRow["Insurance3"], rdoInsurance3Yes, rdoInsurance3No);
            CheckYesNoRadio(dataRow["Insurance4"], rdoInsurance4Yes, rdoInsurance4No);
            CheckRiskIndicator(dataRow["InsuranceRiskIndicator"], chkInsuranceRiskIndicator);

            CheckYesNoRadio(dataRow["Safety1"], rdoSafety1Yes, rdoSafety1No);
            CheckYesNoRadio(dataRow["Safety2"], rdoSafety2Yes, rdoSafety2No);
            //GV 05242018: Added this to handle Design Consultants (Dianne states that this was throwing DCs as high risk in Safety before the
            //new questions.
            if (safety != null) //only Design consultants will have a null Safety object.
            { 
                CheckYesNoRadio(dataRow["SafetyCitation"], rdoSafetyCitationYes, rdoSafetyCitationNo);     //005
            }
            else
            {
                rdoSafetyCitationNo.Checked = true;
                rdoSafety2Yes.Checked = true;
                rdoSafety1No.Checked = true;
            }
            CheckRiskIndicator(dataRow["SafetyRiskIndicator"], chkSafetyRiskIndicator);

            txtProjectReference1.Text = dataRow["ProjectReference1"].ToString();
            txtReferenceDate1.Text = dataRow["ProjectReference1DateCalled"].ToString();
            txtProjectReference2.Text = dataRow["ProjectReference2"].ToString();
            txtReferenceDate2.Text = dataRow["ProjectReference2DateCalled"].ToString();
            txtProjectReference3.Text = dataRow["ProjectReference3"].ToString();
            txtReferenceDate3.Text = dataRow["ProjectReference3DateCalled"].ToString();
            txtSupplierReference1.Text = dataRow["SupplierReference1"].ToString();
            txtSupplierDate1.Text = dataRow["SupplierReference1DateCalled"].ToString();
            txtSupplierReference2.Text = dataRow["SupplierReference2"].ToString();
            txtSupplierReference3.Text = dataRow["SupplierReference3"].ToString();
            txtSupplierDate2.Text = dataRow["SupplierReference2DateCalled"].ToString();
            txtSupplierDate3.Text = dataRow["SupplierReference3DateCalled"].ToString();
            CheckYesNoRadio(dataRow["Reference1"], rdoReference1Yes, rdoReference1No);
            CheckYesNoRadio(dataRow["Reference2"], rdoReference2Yes, rdoReference2No);
            CheckYesNoRadio(dataRow["Reference3"], rdoReference3Yes, rdoReference3No);
            //Added by N Schoenberger for "Not Applicable" functionality 10/20/2011
            CheckNotApplicable(dataRow["Reference3NA"], rdoReference3NA);

            CheckRiskIndicator(dataRow["ReferenceRiskIndicator"], chkReferenceRiskIndicator);
            txtProjectManagerName.Text = dataRow["ProjectManagerName"].ToString();


            //  DataRow EMRRate = SREDetails.Tables[2].Rows[0];
            //003 - start
            if (SREDetails.Tables[2] != null && SREDetails.Tables[2].Rows.Count > 0)    //GV 05252018: Added condition
            {
                bool skipCurrentEMR = (SREDetails.Tables[2].Rows[0]["NotAvailable"].ToString().ToUpper() == "TRUE" ||
                                        string.IsNullOrEmpty(SREDetails.Tables[2].Rows[0]["EMRRate"] as string)) ? true : false;

                if (!skipCurrentEMR)
                {
                    if (SREDetails.Tables[2].Rows[0]["EMRRate"].ToString() == "")
                    {
                        txtRate1.Text = "N/A";
                    }
                    else
                    {
                        txtRate1.Text = SREDetails.Tables[2].Rows[0]["EMRRate"].ToString();
                    }
                    if (SREDetails.Tables[2].Rows[1]["EMRRate"].ToString() == "")
                    {
                        txtRate2.Text = "N/A";
                    }
                    else
                    {
                        txtRate2.Text = SREDetails.Tables[2].Rows[1]["EMRRate"].ToString();
                    }
                    if (SREDetails.Tables[2].Rows[2]["EMRRate"].ToString() == "")
                    {
                        txtRate3.Text = "N/A";
                    }
                    else
                    {
                        txtRate3.Text = SREDetails.Tables[2].Rows[2]["EMRRate"].ToString();
                    }

                    lblYearDisp1.Text = "(" + SREDetails.Tables[2].Rows[0]["EMRYear"].ToString() + ")";
                    lblYearDisp2.Text = "(" + SREDetails.Tables[2].Rows[1]["EMRYear"].ToString() + ")";
                    lblYearDisp3.Text = "(" + SREDetails.Tables[2].Rows[2]["EMRYear"].ToString() + ")";
                }
                else
                {
                    if (SREDetails.Tables[2].Rows[1]["EMRRate"].ToString() == "")
                    {
                        txtRate1.Text = "N/A";
                    }
                    else txtRate1.Text = SREDetails.Tables[2].Rows[1]["EMRRate"].ToString();

                    if (SREDetails.Tables[2].Rows[2]["EMRRate"].ToString() == "")
                    {
                        txtRate2.Text = "N/A";
                    }
                    else txtRate2.Text = SREDetails.Tables[2].Rows[2]["EMRRate"].ToString();

                    if (SREDetails.Tables[2].Rows.Count == 4)
                    {
                        if (SREDetails.Tables[2].Rows[3]["EMRRate"].ToString() == "")
                        {
                            txtRate3.Text = "N/A";
                        }
                        else txtRate3.Text = SREDetails.Tables[2].Rows[3]["EMRRate"].ToString();
                    }
                    else txtRate3.Text = "N/A";

                    lblYearDisp1.Text = "(" + SREDetails.Tables[2].Rows[1]["EMRYear"].ToString() + ")";
                    lblYearDisp2.Text = "(" + SREDetails.Tables[2].Rows[2]["EMRYear"].ToString() + ")";
                    lblYearDisp3.Text = "(" + ((SREDetails.Tables[2].Rows.Count == 4) ? SREDetails.Tables[2].Rows[3]["EMRYear"].ToString() : (Convert.ToInt32(SREDetails.Tables[2].Rows[2]["EMRYear"].ToString()) - 1).ToString()) + ")";
                }
            }
            //003 - end

            // Modified by N Schoenberger 11/7/2011 [PM Risk/ DOC Risk Dates]
            //txtRiskDate1.Text = dataRow["SREDate"].ToString();
            txtRiskDate1.Text = string.Format("{0:M/d/yyyy}", dataRow["SubmittedDate"]);
            // ****************************************************************

            string BComments = dataRow["BusinessComments"].ToString();
            BComments = BComments.Replace("\r\n", "<BR/>");
            BComments = BComments.Replace(" ", " &nbsp;");
            lblBComments.InnerHtml = BComments;
            string FComments = dataRow["FinanceComments"].ToString();
            FComments = FComments.Replace("\r\n", "<BR/>");
            FComments = FComments.Replace(" ", " &nbsp;");
            lblFComments.InnerHtml = FComments;
            //SGS Changes on 08/11/2011
            string LComments = dataRow["LicenseComments"].ToString();
            LComments = LComments.Replace("\r\n", "<BR/>");
            LComments = LComments.Replace(" ", " &nbsp;");
            lblLComments.InnerHtml = LComments;
            ///
            string SComments = dataRow["SaftyComments"].ToString();
            SComments = SComments.Replace("\r\n", "<BR/>");
            SComments = SComments.Replace(" ", " &nbsp;");
            lblSComments.InnerHtml = SComments;
            //SGS Changes on 08/11/2011
            string IComments = dataRow["InsuranceComments"].ToString();
            IComments = IComments.Replace("\r\n", "<BR/>");
            IComments = IComments.Replace(" ", " &nbsp;");
            lblIComments.InnerHtml = IComments;
            //
            string RComments = dataRow["ReferenceComments"].ToString();
            RComments = RComments.Replace("\r\n", "<BR/>");
            RComments = RComments.Replace(" ", " &nbsp;");
            lblRComments.InnerHtml = RComments;
            //SGS Changes on 08/11/2011
            string RiskComments = dataRow["Comments"].ToString();
            RiskComments = RiskComments.Replace("\r\n", "<BR/>");
            RiskComments = RiskComments.Replace(" ", " &nbsp;");
            lblRiskComments.InnerHtml = RiskComments;
            txtRiskMgmntComments.InnerHtml =  dataRow["RiskManagementComments"].ToString();   //002
            //
            //001
            long createdBY = Convert.ToInt64(dataRow["CreatedBy"].ToString());
            lblOriginator.Text = "Originated By: " + new clsAdminLogin().GetEmployeeName(createdBY);



            if (!dataRow.IsNull("RiskFlag"))
            {
                switch (Convert.ToInt32(dataRow["RiskFlag"]))
                {
                    case 1:
                        chkAverageRisk.Checked = true;
                        txtProjectManagerName.Text = dataRow["ProjectManagerName"].ToString();
                        txtRiskDate1.Text = dataRow["SREDate"].ToString();
                        txtGroupName1.Text = dataRow["GroupDOCName"].ToString();
                        txtRiskDate2.Text = dataRow["SREDate"].ToString();
                        //txtProjectManagerName.Enabled = true;
                        //txtRiskDate1.Enabled = true;
                        break;
                    case 2:
                        chkHighRisk.Checked = true;
                        txtGroupName1.Text = dataRow["GroupDOCName"].ToString();
                        txtRiskDate2.Text = dataRow["SREDate"].ToString();
                        //txtGroupName1.Enabled = true;
                        //txtRiskDate2.Enabled = true;
                        break;
                    case 3:
                        chkDisapproved.Checked = true;
                        txtGroupName2.Text = dataRow["GroupDOCName"].ToString();
                        txtRiskDate3.Text = dataRow["SREDate"].ToString();
                        //txtGroupName1.Enabled = true;
                        //txtRiskDate3.Enabled = true;
                        break;
                }
            }
        }
    }

    /// <summary>
    /// Select the Yes/No Radiobuttons Method
    /// </summary>
    /// <param name="value"></param>
    /// <param name="rdoYes"></param>
    /// <param name="rdoNo"></param>
    private void CheckYesNoRadio(object value, RadioButton rdoYes, RadioButton rdoNo)
    {
        if (!value.Equals(DBNull.Value))
        {
            if (Convert.ToBoolean(value))
            {
                rdoYes.Checked = true;
            }
            else
            {
                rdoNo.Checked = true;
            }
        }
    }

    private void CheckNotApplicable(object value, RadioButton rdoNotApplicable)
    {
        if (!value.Equals(DBNull.Value))
        {
            if (Convert.ToBoolean(value))
                rdoNotApplicable.Checked = true;
            else
                rdoNotApplicable.Checked = false;
        }
    }
    /// <summary>
    /// Check the Risk indicator Checkbox Method
    /// </summary>
    /// <param name="value"></param>
    /// <param name="chkRiskIndicator"></param>
    private void CheckRiskIndicator(object value, CheckBox chkRiskIndicator)
    {
        if (!value.Equals(DBNull.Value))
        {
            chkRiskIndicator.Checked = Convert.ToBoolean(value);
        }
    }

    /// <summary>
    /// 002 - Implemented
    /// 006
    /// </summary>
    protected void LoadRiskManagmentChecklist()
    {
        RiskManagementChecklist mngmtChkList = new RiskManagementChecklist();
        RiskEnrollment objEnrollment = new RiskEnrollment();
        List<VMSDAL.SRERiskManagement> selected = riskEvaluation.GetSRERiskManagement(riskId);

        int i = 0;
        foreach (var item in mngmtChkList.AverageRiskSection)
        {
            chkListAverageRisk.Items.Add(item.Name);
            if (item.Name == "Other")
            {
                string textboxid = txtAverageRiskControlsOther.ClientID;

                if (!selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID))
                {
                    txtAverageRiskControlsOther.Visible = false;

                }
                else
                {
                    txtAverageRiskControlsOther.Text = selected.FirstOrDefault(f => f.FK_RiskMgmtListID == item.PK_RiskMgmtListID).OtherText;
                    txtAverageRiskControlsOther.Visible = true;
                }
                chkListAverageRisk.Items[i].Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
            }
            chkListAverageRisk.Items[i].Value = Convert.ToString(item.PK_RiskMgmtListID);
            chkListAverageRisk.Items[i].Selected = selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID);
            chkListAverageRisk.Items[i].Enabled = (bool)item.IsEnabled;     //007

            i++;
        }

        //007 - Start
        i = 0;
        foreach (var item in mngmtChkList.AverageRiskDetermination)
        {
            chkListAvgRiskDet.Items.Add(item.Name);
            if (item.Name == "Other")
            {
                string textboxid = txtAvgRiskDetOther.ClientID;

                if (!selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID))
                {
                    txtAvgRiskDetOther.Attributes.Add("hidden", "hidden");
                }
                else
                {
                    txtAvgRiskDetOther.Text = selected.FirstOrDefault(f => f.FK_RiskMgmtListID == item.PK_RiskMgmtListID).OtherText;
                    txtAvgRiskDetOther.Attributes.Remove("hidden");

                }
                chkListAvgRiskDet.Items[i].Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
            }
            chkListAvgRiskDet.Items[i].Value = Convert.ToString(item.PK_RiskMgmtListID);
            chkListAvgRiskDet.Items[i].Selected = selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID);
            chkListAvgRiskDet.Items[i].Enabled = (bool)item.IsEnabled;     //007
            i++;
        }
        //007 - End

        i = 0;
        foreach (var item in mngmtChkList.HighRiskSection)
        {
            chkListHighRisk.Items.Add(item.Name);
            if (item.Name == "Other")
            {
                string textboxid = txtHighRiskControlsOther.ClientID;

                if (!selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID))
                {
                    txtHighRiskControlsOther.Visible = false;;
                }
                else
                {
                    txtHighRiskControlsOther.Text = selected.FirstOrDefault(f => f.FK_RiskMgmtListID == item.PK_RiskMgmtListID).OtherText;
                    txtHighRiskControlsOther.Visible = true;

                }

                chkListHighRisk.Items[i].Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
            }
            chkListHighRisk.Items[i].Value = Convert.ToString(item.PK_RiskMgmtListID);
            chkListHighRisk.Items[i].Selected = selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID);
            chkListHighRisk.Items[i].Enabled = (bool)item.IsEnabled;     //007
            i++;
        }

        i = 0;
        foreach (var item in mngmtChkList.HighRiskDetermination)
        {
            chkListHighRiskDet.Items.Add(item.Name);
            if (item.Name == "Other")
            {
                string textboxid = txtHighRiskDetOther.ClientID;

                if (!selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID))
                {
                    txtHighRiskDetOther.Visible = false;;
                }
                else
                {
                    txtHighRiskDetOther.Text = selected.FirstOrDefault(f => f.FK_RiskMgmtListID == item.PK_RiskMgmtListID).OtherText;
                    txtHighRiskDetOther.Visible = true;

                }
                chkListHighRiskDet.Items[i].Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
            }
            chkListHighRiskDet.Items[i].Value = Convert.ToString(item.PK_RiskMgmtListID);
            chkListHighRiskDet.Items[i].Selected = selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID);
            chkListHighRiskDet.Items[i].Enabled = (bool)item.IsEnabled;     //007
            i++;
        }

        i = 0;
        foreach (var item in objEnrollment.CheckList)
        {
            chklstEnrollment.Items.Add(item.Name);
            if (item.Name == "Other")
            {
                string textboxid = txtEnrollmentOther.ClientID;
                if (!selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID))
                {
                    txtEnrollmentOther.Visible = false;;
                }
                else
                {
                    txtEnrollmentOther.Text = selected.FirstOrDefault(f => f.FK_RiskMgmtListID == item.PK_RiskMgmtListID).OtherText;
                    txtEnrollmentOther.Visible = true;

                }
                chklstEnrollment.Items[i].Attributes.Add("onclick", "Javascript:ToggleElement('" + textboxid + "', this.checked)");
            }
            chklstEnrollment.Items[i].Value = Convert.ToString(item.PK_RiskMgmtListID);
            chklstEnrollment.Items[i].Selected = selected.Exists(s => s.FK_RiskMgmtListID == item.PK_RiskMgmtListID);
            chklstEnrollment.Items[i].Enabled = (bool)item.IsEnabled;     //007

            i++;
        }

    }

    #endregion
}
