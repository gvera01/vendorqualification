﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 09/07/2012:  VMS Stabilization Release 1.2 Hot Fixes (JIRA:VPI-68)
 *
 ****************************************************************************************************************/
#endregion

public partial class SREStart : System.Web.UI.Page
{

    #region Instantiate the class and declare varaibles
    RiskEvaluation riskEvaluation = new RiskEvaluation();
    clsRegistration objRegistration = new clsRegistration();
    private const string TAG_ERROR_MSG = "<li>{}</li>";
    Common objCommon = new Common();
    private int Flag = 0;
    public string SortExp;

    #endregion

    #region Page Events

    protected override void OnInit(EventArgs e)// Paging
    {
        base.OnInit(e);
        string strPageRequest = Request.Form["__EVENTTARGET"];
        CreatePaging(true, strPageRequest);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]).Equals("0"))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
        }
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (!IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            if (Session["RISKIDLOGOUT"] != null && Session["LOCKEDBYLOGOUT"] != null)
            {
                Int64 Lockedby = riskEvaluation.LockebByUser(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                if (Lockedby == Convert.ToInt64(Session["LOCKEDBYLOGOUT"]))
                {
                    riskEvaluation.UpdateStatus(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                    Session["RISKIDLOGOUT"] = null;
                }
            }
            imbNew.Attributes.Add("onclick", "isbtnclick()");
            tdButtons.Style["display"] = "";
            if (Request.QueryString.Count > 0)
            {
                txtVendor.Text = objCommon.decode(Request.QueryString["VNameStart"].ToString());
                txtVNo.Text = objCommon.decode(Request.QueryString["VNoStart"].ToString());

                Vendortd.Style["display"] = "";
                start.Style["display"] = "";
                hdnbtnclick.Value = string.Empty;
                if (Request.QueryString["Pagenumber"].ToString() != "")
                {
                    hdnPage.Value = Request.QueryString["Pagenumber"].ToString();

                    ddlNumber.SelectedValue = Request.QueryString["pageSize"].ToString();
                }
                else
                {
                    hdnPage.Value = "1";
                }
                tdButtons.Style["display"] = "none";
            }
        }
        if (start.Style["display"] == "" && hdnbtnclick.Value == string.Empty) // Paging
        {

            BindGridView();
            string StrPageRequest;
            //Get id of the control that raises the event
            StrPageRequest = Request.Form["__EVENTTARGET"];
            CreatePaging(true, StrPageRequest);
            DisplayPreviousNext();
            //Apply css for the current page
            StyleforPaging(hdnPage.Value, plcCrntPage);
            lblTotalPage.Text = Convert.ToString(Session["Page"]);
        }
        mpopAlert.Hide();
        AdminMenu.DisplayMenu(Convert.ToString(Session["Access"]).Equals("1") ? "Admin" : "Employee");
    }
    #endregion

    #region Button Events

    /// <summary>
    /// Add New SRE
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAdd_Click(object sender, EventArgs e)
    {
        hdnClick.Value = "Add";
        tdButtons.Style["display"] = "none";
        Vendortd.Style["display"] = "";
        ViewState["VendorAdminId"] = 0;
        BindProjectNumber();
    }

    /// <summary>
    /// View the Existing SRE 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbExists_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/SRE/ViewSRE.aspx");
    }

    protected void imbSearch_Click(object sender, ImageClickEventArgs e)
    {
        lblMsg.Text = String.Empty; //002;
        bool expiredOrIncomplete = false;
        hdnPage.Value = "1"; // Paging
        BindGridView();
        // Added by N Schoenberger 10/03/2011
        if (txtVendor.Text != "")
        {
            clsRegistration objRegistration = new clsRegistration();

            DAL.Vendor objResult = objRegistration.GetVendorByName(txtVendor.Text.ToString());
            if (objResult != null)
            {
                if (objResult.VendorStatus == 5)
                {
                    expiredOrIncomplete = true;
                    lblMsg.Text = txtVendor.Text + @" - VQF IS IN A STATUS OF INCOMPLETE.";
                }
                if (objResult.VendorStatus == 6)
                {
                    expiredOrIncomplete = true;
                    lblMsg.Text = txtVendor.Text + @" - VQF IS IN A STATUS OF EXPIRED.";
                }
            }
        }
        if (grvRiskEvaluation.Rows.Count == 0 && expiredOrIncomplete == false)
        {
            lblMsg.Text = string.Empty;
            mpopAlert.Show();
            lblMessage.Text = TAG_ERROR_MSG.Replace("{}",HttpContext.GetGlobalResourceObject("ErrorMessages", "NoRecords").ToString()); 
        }
    }

    protected void imbNew_Click(object sender, ImageClickEventArgs e)
    {
        if (hdnVendorIds.Value == string.Empty)
        {
            mpopAlert.Show();
           // lblMessage.Text = TAG_ERROR_MSG.Replace("{}", "Please select any one vendor");
            lblMessage.Text = TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SelectVendor").ToString()); 
        }
        else
        {
            GridViewRowCollection gvRowCollection = grvRiskEvaluation.Rows;
            foreach (GridViewRow gvRow in gvRowCollection)
            {
                switch (gvRow.RowType)
                {
                    case DataControlRowType.DataRow:
                        CheckBox chkVendor = (CheckBox)grvRiskEvaluation.Rows[gvRow.RowIndex].FindControl("chkVendor");
                        if (chkVendor.Checked)
                        {
                            hdnbtnclick.Value = string.Empty;
                            hdnVendorIds.Value = string.Empty;
                            if (grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString() != "InProgress" && grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString() != "Expired" && grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString() != "Reject")
                            {
                                Response.Redirect("~/SRE/SRE.aspx?VName=" + objCommon.encode(grvRiskEvaluation.DataKeys[gvRow.RowIndex]["Company"].ToString()) + "&VNameStart=" + objCommon.encode(txtVendor.Text.Trim()) + "&VNoStart=" + objCommon.encode(txtVNo.Text.Trim()) + "&Pagenumber=" + Convert.ToInt32(hdnPage.Value) + "&pageSize=" + Convert.ToInt32(ddlNumber.SelectedValue));
                            }
                            else
                            {
                                mpopAlert.Show();
                                lblMessage.Text = "VQF is in " + grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString() + " status, hence SRE cannot be started";
                                BindGridView();
                            }
                        }
                        if (chkVendor.Checked && grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString() == "Complete")
                        {
                            hdnbtnclick.Value = string.Empty;
                        }
                        break;
                }
            }
        }
    }

    // ***** Modified by N Schoenberger on 01/05/2012 [Phase II Enhancement] ***** //
    protected void imbNotify_Click(object sender, ImageClickEventArgs e)
    {
        if (hdnVendorIds.Value == string.Empty)
        {
            mpopAlert.Show();
            lblMessage.Text = TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "SelectVendor").ToString()); 
        }
        else
        {
            GridViewRowCollection gvRowCollection = grvRiskEvaluation.Rows;
            foreach (GridViewRow gvRow in gvRowCollection)
            {
                switch (gvRow.RowType)
                {
                    case DataControlRowType.DataRow:
                        CheckBox chkVendor = (CheckBox)grvRiskEvaluation.Rows[gvRow.RowIndex].FindControl("chkVendor");
                        if (chkVendor.Checked)
                        {
                            hdnbtnclick.Value = string.Empty;
                            hdnVendorIds.Value = string.Empty;
                            //if (grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString() != "Complete" && grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString() != "Expired" && grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString() != "Reject")
                            if (grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString() != "Complete" && grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString() != "Reject")
                            {
                                riskEvaluation.AddMailNotification(Convert.ToInt64(grvRiskEvaluation.DataKeys[gvRow.RowIndex]["FK_VendorId"]), grvRiskEvaluation.DataKeys[gvRow.RowIndex]["Company"].ToString(), grvRiskEvaluation.DataKeys[gvRow.RowIndex]["Email"].ToString(), grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString(), Session["EmployeeName"].ToString(), Session["EmailUser"].ToString(), 0);
                                mpopAlert.Show();
                                //lblMessage.Text = "Email Notification will be sent when VQF is completed";
                                lblMessage.Text = TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "EmailNotification").ToString()); 

                                
                            }
                            else
                            {
                                mpopAlert.Show();
                                lblMessage.Text = "VQF status is already " + grvRiskEvaluation.DataKeys[gvRow.RowIndex]["VQFStatus"].ToString() + ", hence Start SRE, notification can be sent only for InProgress, Pending, Revision and Expired status.";
                                BindGridView();
                            }
                            BindGridView();
                        }
                        break;
                }
            }
        }
    }
    // ***** End Modification [Phase II Enhancement] ***** //


    protected void imgback_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/SRE/SREStart.aspx");
    }


    #endregion

    #region Methods

    /// <summary>
    ///Bind Project number method
    /// </summary>
    private void BindProjectNumber()
    {
        ddlProjectNumber.DataSource = new clsRating().GetProject();
        ddlProjectNumber.DataTextField = "ProjectNumber";
        ddlProjectNumber.DataBind();
        ListItem lstSelect = new ListItem();
        lstSelect.Text = "--Select--";
        ddlProjectNumber.Items.Insert(0, lstSelect);
    }
    private void BindGridView()// Paging
    {
        SortExp = hdnSort.Value;
        if (string.IsNullOrEmpty(SortExp))
        {
            SortExp = "";
        }

        DataSet riskDataSet = riskEvaluation.GetSRE(txtVendor.Text.Trim(), ddlProjectNumber.SelectedValue == "--Select--" ? "" : ddlProjectNumber.SelectedValue, txtBidPackage.Text.Trim(), Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.SelectedValue), SortExp, txtVNo.Text.Trim());
        grvRiskEvaluation.DataSource = riskDataSet;
        grvRiskEvaluation.DataBind();
        if (grvRiskEvaluation.Rows.Count > 0)
        {
            start.Style["display"] = "";
            start1.Style["display"] = "";
            start2.Style["display"] = "";
            start3.Style["display"] = "";
        }
        else
        {
            start.Style["display"] = "none";
            start1.Style["display"] = "none";
            start2.Style["display"] = "none";
            start3.Style["display"] = "none";
        }
        hdnCount.Value = riskDataSet.Tables[1].Rows[0][0].ToString();

        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));
        lblTotal.Text = hdnCount.Value;
        Session["Page"] = Convert.ToString(page);

        //Call method to create paging controls dynamically
        CreatePaging(false, "");
        //Display previous and next button
        DisplayPreviousNext();
        //Apply css for the current page
        StyleforPaging(hdnPage.Value, plcCrntPage);
        lblTotalPage.Text = Convert.ToString(Session["Page"]);
    }
    #endregion

    #region gridview events
    protected void grvRiskEvaluation_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType.Equals(DataControlRowType.DataRow))
        {

            ((CheckBox)e.Row.FindControl("chkVendor")).Attributes.Add("onclick", "javascript:selectVendor(this,'" + grvRiskEvaluation.DataKeys[e.Row.RowIndex].Value.ToString() + "')");
        }
    }
    protected void grvRiskEvaluation_Sorting(object sender, GridViewSortEventArgs e)//sorting
    {
        string sortExpression = e.SortExpression;
        if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortDirection"])))
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
        else if (Convert.ToString(ViewState["SortDirection"]) == Convert.ToString(SortDirection.Ascending))
        {
            GridViewSortDirection = SortDirection.Descending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Descending);
            SortGridView(sortExpression, "DESC");
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
    }
    #endregion

    #region paging methods
    /// <summary>
    /// Create paging for SRE search 
    /// </summary>
    /// <param name="FromLoad"></param>
    /// <param name="strPageRequest"></param>
    private void CreatePaging(bool FromLoad, string strPageRequest)// Paging
    {
        plcCrntPage.Controls.Clear();
        if (FromLoad)
        {
            if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page"]))) && (string.IsNullOrEmpty(strPageRequest) || strPageRequest.ToUpperInvariant() != "DDLNUMBER") && Flag != 1)
            {
                for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
                {
                    if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                    {
                        LinkButton lnkPage = new LinkButton();
                        lnkPage.Click += new EventHandler(lnkPage_Click);
                        lnkPage.Text = Convert.ToString(i);
                        lnkPage.CssClass = i.Equals(Convert.ToInt32(hdnPage.Value)) ? "Selectlnkcolor" : "lnkcolor";
                        lnkPage.Text = Convert.ToString(i);
                        lnkPage.ID = "lnk" + Convert.ToString(i);
                        lnkPage.Attributes.Add("onclick", "GetValue(" + lnkPage.Text.Trim() + ")");
                        plcCrntPage.Controls.Add(lnkPage);
                    }
                }
            }
        }
        else
        {
            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {
                    LinkButton lnk = new LinkButton();
                    Literal ltr = new Literal();
                    lnk.Click += new EventHandler(lnkPage_Click);
                    ltr.Text = ",";
                    lnk.Text = Convert.ToString(i);
                    lnk.CssClass = i.Equals(Convert.ToInt32(hdnPage.Value)) ? "Selectlnkcolor" : "lnkcolor";
                    lnk.Text = Convert.ToString(i);
                    lnk.ID = "lnk" + Convert.ToString(i);
                    lnk.Attributes.Add("onclick", "GetValue(" + lnk.Text.Trim() + ")");
                    plcCrntPage.Controls.Add(lnk);
                }
            }
        }
    }

    /// <summary>
    /// Display Previous/Next Page SRE Method
    /// </summary>
    private void DisplayPreviousNext()// Paging
    {
        imbPrevious.Visible = !hdnPage.Value.Equals("1");
        imbNext.Visible = !(Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value));
    }

    /// <summary>
    /// paginf style method
    /// </summary>
    /// <param name="CurrentLink"></param>
    private void StyleforPaging(string CurrentLink, PlaceHolder plc)// Paging
    {
        foreach (Control ctrl in plc.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                lnk1.CssClass = lnk1.Text.Equals(CurrentLink) ? "Selectlnkcolor" : "lnkcolor";
            }
        }
    }

    /// <summary>
    /// dynamic event for Page Creation
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void lnkPage_Click(object sender, EventArgs e)// Paging
    {
        LinkButton Currentlnk = new LinkButton();
        Currentlnk = (LinkButton)sender;
        hdnPage.Value = Currentlnk.Text.Trim();
        BindGridView();
        CreatePaging(false, "");
        DisplayPreviousNext();
        StyleforPaging(Currentlnk.Text.Trim(), plcCrntPage);
    }

    /// <summary>
    /// Selected control style Method
    /// </summary>
    /// <param name="CurrentPage"></param>
    private void StyleForSelectedControl(string CurrentPage)// Paging
    {
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text.Trim() == CurrentPage)
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }
    }

    #endregion

    #region paging events
    protected void imbPrevious_Click(object sender, ImageClickEventArgs e)// Paging
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) - 1);
        lblTotalPage.Text = Convert.ToString(Session["Page"]);
        BindGridView();
        CreatePaging(false, "");
        StyleForSelectedControl(hdnPage.Value);
        DisplayPreviousNext();
    }
    protected void imbNext_Click(object sender, ImageClickEventArgs e)// Paging
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) + 1);
        lblTotalPage.Text = Convert.ToString(Session["Page"]);
        BindGridView();
        CreatePaging(false, "");
        DisplayPreviousNext();
        StyleForSelectedControl(hdnPage.Value);
    }
    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)// Paging
    {
        hdnPage.Value = "1";
        BindGridView();
        CreatePaging(false, "");
        DisplayPreviousNext();
        StyleforPaging(hdnPage.Value, plcCrntPage);
    }
    #endregion

    #region Sorting methods
    /// <summary>
    /// Get sorting direction
    /// </summary>
    public SortDirection GridViewSortDirection//sorting
    {
        get
        {
            if (ViewState["sortDirection"] == null)
            {
                ViewState["sortDirection"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["sortDirection"];
        }
        set
        {
            ViewState["sortDirection"] = value;
        }
    }

    /// <summary>
    /// Method to sort gridview
    /// </summary>
    /// <param name="sortExpression"></param>
    /// <param name="direction"></param>
    private void SortGridView(string sortExpression, string direction)//sorting
    {
        SortExp = sortExpression + " " + direction;
        hdnSort.Value = SortExp;

        BindGridView();

        //Call method to create paging controls dynamically
        CreatePaging(false, "");
        //Display previous and next button
        DisplayPreviousNext();
        //Apply css for the current page
        StyleforPaging(hdnPage.Value, plcCrntPage);
        lblTotalPage.Text = Convert.ToString(Session["Page"]);

    }
    #endregion


   
}