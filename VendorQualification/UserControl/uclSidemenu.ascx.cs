﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Web;
using System.Net;
using VMSDAL;
using System.Threading;

#region Change Comments
/* ***************************************************************************************************************
 * 001 - G. Vera - Changed to use the UpdateVendorBit() method from the clsRegistration class instead of the new 
 * UpdateVendorBitStatus() method that excludes the last modified date.
 * 
 * 002 - G. Vera 05/18/2012:  Changing how the Visibility of the Update VQF and Submit VQF is handled and adding
 *       some Session variables to handle the edit availability of the Safety EMR and Financial yearly values
 * 003 - G. Vera 07/06/2012: VMS Stabilization Release 1.1 (JIRA:VPI-15)
 * 004 - G. vera 11/05/2012:  VMS Stabilization Release 1.3 (correcting PDF print issue with accessing server 
 *       resources in production's server WEBEHDQ01P).
 * 005 - G. Vera 01/29/2013 - VMS Hot Fix version 1.3.1 (JIRA:VPI-90)
 * 006 Sooraj Sudhakaran.T 09/28/2013: VMS Modification - Added code to hide section that do not apply to design consultant
 * 007 Sooraj Sudhakaran.T 09/28/2013: VMS Modification - Added code to read URL from web.config
 * 008 G. Vera 06/02/2014: Modification to the submission email message
 * 009 G. Vera 06/19/2014: Correct the handling of vendor statuses
 * 010 G. Vera 07/09/2014: Update the LastModifedDT field upon save or save and next click
 * 011 G. Vera 07/14/2014: Added parameter to the Print VQF routine to make it external print.
 * 012 G. Vera 05/05/2015: Show pop-up message upon Update VQF click
 ******************************************************************************************************************/
#endregion

public partial class UserControl_uclSidemenu : System.Web.UI.UserControl
{

    private int _selectedMenu;
    Common ObjCommon = new Common();
    string strFileName1, strFileName2, strFileName3, strFileName4, strFileName5;
    string sMimeType;
    DataSet Complete = new DataSet();
    string DOCUMENT_LOCATION = string.Empty;

    long VendorId;
    DataSet Attch = new DataSet();
    clsRegistration objRegistration = new clsRegistration();
    clsSafety objSafety = new clsSafety();
    clsLegal objLegal = new clsLegal();

    public int SelectedMenu
    {
        get { return _selectedMenu; }
        set { _selectedMenu = value; }

    }
    private int status;
    public int VendorStatus
    {
        get { return status; }
        set { status = value; }
    }

    //004
    private bool isEditing;

    public bool IsEditing
    {
        get { return isEditing; }
        set { isEditing = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DOCUMENT_LOCATION = ConfigurationManager.AppSettings["VQFDocument"].ToString();
        if (!IsPostBack)
        {
            CallScripts();

            //get the vendor details
            DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(Session["VendorId"]));
            string result = objRegistration.GetVendorCompanyType(Convert.ToInt64(Session["VendorId"])).Trim();

            if ((!string.IsNullOrEmpty(result)) & (result == TypeOfCompany.DesignConsultant))
            {
                liLabor.Style["display"] = "none";
                liCertification.Style["display"] = "";       //006-Sooraj
                liSupplierCredit.Style["display"] = "none";       //006-Sooraj
                liBondingSurety.Style["display"] = "none";
                liSafety.Style["display"] = "none";
                lblSafetyMessage.Visible = false;
            }
            else
            {
                liLabor.Style["display"] = "";
                liCertification.Style["display"] = "";
                //006-Sooraj
                liSupplierCredit.Style["display"] = "";
                liBondingSurety.Style["display"] = "";
                liSafety.Style["display"] = "";
                lblSafetyMessage.Visible = true;
            }

            VendorStatus = 0;
            if (objVendor.Length > 0)
            {
                VendorStatus = objVendor[0].VendorStatus;
                if ((VendorStatus == 1) || (VendorStatus == 2))
                {
                    imbSubmitStatus.ImageUrl = "~/images/updatevqf.gif";
                    imbSubmitStatus.ToolTip = "Update VQF";
                }
                else
                {
                    imbSubmitStatus.ImageUrl = "~/images/subcominfo1.gif";
                }
            }

            //G. Vera 05/12/2014:  This should be inside the Update VQF event: DataSet ds = ObjCommon.CheckNewField(Convert.ToInt64(Session["VendorId"]), VendorStatus.ToString());    //005
            GetCompleteStatus();
            GetIncompleteStatus();

            string ThisPage = Page.AppRelativeVirtualPath;
            // 002 - This button should be visible to Pending and Complete statuses
            // and Invisible to InProgress and Revision statuses if there are incomplete VQF sections
            if (imbSubmitStatus.ImageUrl == "~/images/updatevqf.gif")
                imbSubmitStatus.Visible = true;
            else imbSubmitStatus.Visible = grdFeedbackComplete.Rows.Count.Equals(6);
        }
    }

    void mnuValidation_MenuItemClick(object sender, MenuEventArgs e)
    {
        int index = Int32.Parse(e.Item.Value);
    }

    private void CallScripts()
    {
        imbAddDoc.Attributes.Add("onClick", " return CheckDocument();");
    }

    //006
    /// <summary>
    /// Switch view when company changes 
    /// </summary>
    /// <param name="IsDesignConsultant"></param>
    /// <Author>Sooraj Sudhakaran.T</Author>
    /// <Date>30-SEP-2013</Date>
    public void SwitchViews(bool IsDesignConsultant)
    {

        if (IsDesignConsultant)
        {
            liLabor.Style["display"] = "none";
            liCertification.Style["display"] = "";
            //006-Sooraj
            liSupplierCredit.Style["display"] = "none";
            liBondingSurety.Style["display"] = "none";
            liSafety.Style["display"] = "none";
            lblSafetyMessage.Visible = false;
        }
        else
        {
            liLabor.Style["display"] = "";
            liCertification.Style["display"] = "";

            //006-Sooraj
            liSupplierCredit.Style["display"] = "";
            liBondingSurety.Style["display"] = "";
            liSafety.Style["display"] = "";
            lblSafetyMessage.Visible = true;

        }

        GetIncompleteStatus();
        GetCompleteStatus();
    }

    //Get the pages which are incomplete
    public void GetIncompleteStatus()
    {
        Complete = ObjCommon.GetVQFStatusData(Convert.ToInt64(Session["VendorId"]), 0);
        grdFeedbackInComplete.DataSource = Complete;
        grdFeedbackInComplete.DataBind();
        Complete.Dispose();
    }

    //Get the pages which are completed
    public void GetCompleteStatus()
    {
        Complete = ObjCommon.GetVQFStatusData(Convert.ToInt64(Session["VendorId"]), 1);
        grdFeedbackComplete.DataSource = Complete;
        grdFeedbackComplete.DataBind();
        imbSubmitStatus.Visible = grdFeedbackComplete.Rows.Count.Equals(6);
        Complete.Dispose();
    }

    /// <summary>
    /// 010 - implemented
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="date"></param>
    public void LastUpdatedDate(long ID, DateTime date)
    {
        clsRegistration registration = new clsRegistration();
        registration.UpdateLastModifiedDate(ID, date);
    }

    //Change to complete and revision status
    //GV - This is the event for the "Ok" button on the modal confirmation box
    protected void imbSubmitHome_Click(object sender, ImageClickEventArgs e)
    {
        DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(Session["VendorId"]));
        DAL.VQFSafety SafetyResult = objSafety.GetSingleVQLSafetyData(Convert.ToInt64(Session["VendorId"]));
        DAL.VQFLegal LegalResult = objLegal.GetSingleVQLlegalData(Convert.ToInt64(Session["VendorId"]));
        string result = objRegistration.GetVendorCompanyType(Convert.ToInt64(Session["VendorId"])).Trim();
        var IsDesignConsultant = result == TypeOfCompany.DesignConsultant;
        long safetyId = 0;
        if (!IsDesignConsultant)
            safetyId = SafetyResult.PK_SafetyID;
        long LegalId = LegalResult.PK_LegalID;

        if (imbSubmitStatus.ImageUrl == "~/images/subcominfo1.gif")
        {
            if (objVendor.Length > 0)
            {
                // 002
                Session["Year1EMRChanged"] = "0";
                Session["Year2EMRChanged"] = "0";
                Session["Year3EMRChanged"] = "0";
                Session["Year4EMRChanged"] = "0";   //005
                Session["Year1MaxContractEnabled"] = "0";
                Session["Year1CompRevenueEnabled"] = "0";
                Session["Year2MaxContractEnabled"] = "0";
                Session["Year2CompRevenueEnabled"] = "0";
                Session["Year3MaxContractEnabled"] = "0";
                Session["Year3CompRevenueEnabled"] = "0";



                imbSubmitStatus.ImageUrl = "~/images/updatevqf.gif";
                //009 actually, this should not even happen because this event is called by the Dialog box that only comes up if the vendor was in progress.
                //if ((Convert.ToString(objVendor[0].VendorStatus) == "3") && SafetyResult.EditStatus == 0 && LegalResult.EditStatus == 0)
                //{
                //    objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorId"]), 2);
                //    ManagerMail(Convert.ToInt64(Session["VendorId"]));
                //    objRegistration.ExpiredActivateDate(Convert.ToInt64(Session["VendorId"]));
                //    if (safetyId > 0) //006-Sooraj
                //    {
                //        objSafety.UpdateSafetyBit(safetyId, 1);
                //        objSafety.SafetyPastYear(safetyId);
                //    }
                //    //objLegal.LegalPastYear(LegalId);  
                //    objLegal.UpdateLegalBit(Convert.ToInt64(Session["VendorId"]), 1);
                //    Response.Redirect(Request.UrlReferrer.ToString());
                //}
                //else
                //{
                //    if (IsDesignConsultant || SafetyResult != null)//006-Sooraj
                //    {
                        objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorId"]), 1);
                        //009 again, I do not understand why this check.  Vendor has completed all sections if this event is raised: if (IsDesignConsultant || SafetyResult.EditStatus != 1)//006-Sooraj
                        //{
                            if (safetyId > 0) //006-Sooraj
                            {
                                objSafety.UpdateSafetyBit(safetyId, 1);
                                objSafety.SafetyPastYear(safetyId);
                            }
                            objLegal.UpdateLegalBit(Convert.ToInt64(Session["VendorId"]), 1);
                            string adminemail = ConfigurationManager.AppSettings["adminemail"].ToString();
                            DAL.UspGetVendorDetailsByIdResult[] objresult = objRegistration.GetVendor(Convert.ToInt64(Session["VendorId"]));
                            StringBuilder Body = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
                            Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hi" + " " + Convert.ToString(objresult[0].FirstName) + " " + Convert.ToString(objresult[0].LastName) + ", <br></font></td></tr>");
                            Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Welcome to Haskell!</font></td></tr>");
                            Body.Append("<tr><td>&nbsp;</td></tr>");

                            //008
                            if (!IsDesignConsultant)
                            {
                                Body.Append("<tr><td><font  face='verdana' size='2'>Thank you for registering with Haskell’s Vendor Management System.  " + 
                                            "Your business will now be eligible for future invitations to bid by any of our project teams.</font></td></tr>");
                            }
                            else
                            {
                                Body.Append("<tr><td><font  face='verdana' size='2'>Thank you for registering with Haskell’s Vendor Management System.  " +
                                            "Your business will now be available for use by any of our project teams.</font></td></tr>");
                            }
                            
                            Body.Append("<tr><td>&nbsp;</td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Your company information will be valid for one year from date of submission.  Near the time of expiration, the registered contact person will receive an e-mail reminder to update your company data.  If at any time during the year, the structure of your company changes, please notify our database coordinator. </font></td></tr>");
                            Body.Append("<tr><td></td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Please keep in mind that only one submission per company is necessary.</font></td></tr>");
                            Body.Append("<tr><td>&nbsp;</td></tr>");
                            //Body.Append("<tr><td><font  face='verdana' size='2'>Your company information will be valid for one year from date of submission.  At the time of expiration, the registered contact person will receive an e-mail.</font></td></tr>");
                            //Body.Append("<tr><td>&nbsp;</td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Please use the tracking number <b>" + Convert.ToString(objresult[0].TrackingNumber) + "</b> to refer your record to Haskell. </font></td></tr>");
                            Body.Append("<tr><td>&nbsp;</td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Respectfully,</font><br/><br/></td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Haskell</font><br></td></tr></table>");
                            objSafety.SendMail(adminemail, Convert.ToString(objresult[0].Email), "Successful VQF Submission to Haskell", Body);
                            Response.Redirect("~/VQF/VQFComplete.aspx");
                        //}
                   //}
               // }
            }
        }
        else
        {
            if (objVendor.Length > 0)
            {
                if ((Convert.ToString(objVendor[0].VendorStatus) == "1"))
                {
                    objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorId"]), 0);
                }
                else
                {
                    objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorId"]), 3);
                }
                imbSubmitStatus.ImageUrl = "~/images/subcominfo1.gif";
            }
        }
    }

    //Confirmation to change
    //GV - This is the actual event for the "Submit VQF" and "Update VQF" button.
    public void imbSubmitStatus_Click(object sender, ImageClickEventArgs e)
    {
        if (imbSubmitStatus.ImageUrl == "~/images/subcominfo1.gif")
        {
            if (grdFeedbackComplete.Rows.Count == 6)
            {
                // 002
                Session["Year1EMRChanged"] = "0";
                Session["Year2EMRChanged"] = "0";
                Session["Year3EMRChanged"] = "0";
                Session["Year4EMRChanged"] = "0";   //005
                Session["Year1MaxContractEnabled"] = "0";
                Session["Year1CompRevenueEnabled"] = "0";

                trAttachDocument.Style["display"] = "block";
                DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(Session["VendorId"]));
                DAL.VQFSafety SafetyResult = objSafety.GetSingleVQLSafetyData(Convert.ToInt64(Session["VendorId"]));
                DAL.VQFLegal LegalResult = objLegal.GetSingleVQLlegalData(Convert.ToInt64(Session["VendorId"]));
                if (objVendor.Length > 0)
                {
                    string result = objRegistration.GetVendorCompanyType(Convert.ToInt64(Session["VendorId"])).Trim();
                    //if (result != TypeOfCompany.DesignConsultant)
                    //{

                        //009 - commented: if ((Convert.ToString(objVendor[0].VendorStatus) == "3") && ((SafetyResult.EditStatus == 1) && LegalResult.EditStatus == 1))
                        //009 - was completed before
                        if (Convert.ToString(objVendor[0].VendorStatus) == "3")
                        {
                            VendorStatus = Convert.ToInt32(objVendor[0].VendorStatus);
                            tdFirst.Style["display"] = "none";
                            objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorId"]), 2);
                            ManagerMail(Convert.ToInt64(Session["VendorId"]));
                            objRegistration.ExpiredActivateDate(Convert.ToInt64(Session["VendorId"]));
                            Response.Redirect(Request.UrlReferrer.ToString());
                            //009 - do we need to update the Edit statuses now?  Maybe...
                            if (SafetyResult != null)
                            {
                                long safetyId = SafetyResult.PK_SafetyID;
                                objSafety.UpdateSafetyBit(safetyId, 1);

                            }
                            if (LegalResult != null)
                            {
                                long vendorID = LegalResult.FK_VendorID;
                                objLegal.UpdateLegalBit(vendorID, 1);
                            }
                        }
                        else
                        {
                            // long safetyId = SafetyResult.PK_SafetyID; //006-Sooraj commented as its not used any where 
                            //009 - if (SafetyResult.EditStatus != 1 && LegalResult.EditStatus != 1)
                            // it should only be inprogress, if it has a submitted date back to Pending
                            if (objVendor[0].SubmittedDate == null)
                            {
                                tdFirst.Style["display"] = "";
                                Modalextnd.Show();
                            }
                            else
                            {
                                objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorId"]), 1);
                                imbSubmitStatus.ImageUrl = "~/images/updatevqf.gif";
                                imbSubmitStatus.ToolTip = "Update VQF";
                                Response.Redirect(Request.UrlReferrer.ToString());
                            }
                        }
                    //}
                    //else
                    //{
                    //    if ((Convert.ToString(objVendor[0].VendorStatus) == "3") &&   LegalResult.EditStatus == 1)
                    //    {
                    //        VendorStatus = Convert.ToInt32(objVendor[0].VendorStatus);
                    //        tdFirst.Style["display"] = "none";
                    //        objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorId"]), 2);
                    //        ManagerMail(Convert.ToInt64(Session["VendorId"]));
                    //        objRegistration.ExpiredActivateDate(Convert.ToInt64(Session["VendorId"]));
                    //        Response.Redirect(Request.UrlReferrer.ToString());
                    //    }
                    //    else
                    //    {
                    //        // long safetyId = SafetyResult.PK_SafetyID; //006-Sooraj commented as its not used any where 

                    //        // if (  SafetyResult.EditStatus != 1 && LegalResult.EditStatus != 1)
                    //        if ( LegalResult.EditStatus != 1)
                    //        {
                    //            tdFirst.Style["display"] = "";
                    //            Modalextnd.Show();
                    //        }
                    //        else
                    //        {
                    //            objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorId"]), 1);
                    //            imbSubmitStatus.ImageUrl = "~/images/updatevqf.gif";
                    //            imbSubmitStatus.ToolTip = "Update VQF";
                    //            Response.Redirect(Request.UrlReferrer.ToString());
                    //        }
                    //    }
                    //}
                }
            }
        }
        else
        {
            DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(Session["VendorId"]));
            trAttachDocument.Style["display"] = "none";

            
            if (objVendor.Length > 0)
            {
                VendorStatus = objVendor[0].VendorStatus;
            }

            //SGS Changes On 09/14/2011
            if (VendorStatus == 1 || VendorStatus == 2)
            {
                DataSet ds = ObjCommon.CheckNewField(Convert.ToInt64(Session["VendorId"]), VendorStatus.ToString());    //005
            }


            if ((objVendor.Length > 0) && (VendorStatus == 2))
            {
                objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorId"]), 3);
                imbSubmitStatus.ImageUrl = "~/images/subcominfo1.gif";
                ShowTenDaysMessage();   //012
            }
            else if ((objVendor.Length > 0) && (VendorStatus == 1))
            {
                objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorId"]), 0);
                imbSubmitStatus.ImageUrl = "~/images/subcominfo1.gif";
                ShowNinetyDaysMessage();    //012
            }
        }
    }

    //012
    private void ShowTenDaysMessage()
    {
        lblMsg.Text = "<br/>The system provides 10 days for you to <i>complete</i> and <i>submit</i> your update." +
                      "  Click the \"Update VQF\" button to your left to make changes.  Once all sections are marked" +
                      " complete and you are ready to submit the changes, a \"Submit completed information\" button will" +
                      " appear on left side of screen.  Please click it to finish the process.";

        lblheading.Text = "Information";
        Modalextnd1.Show();
    }

    //012
    private void ShowNinetyDaysMessage()
    {
        lblMsg.Text = "<br/>The system provides 90 days from your registered date for you to <i>complete</i> and submit your update.  " +
                      "Once all sections are marked complete and you are ready to submit the changes, please click  \"Submit completed information\" to finish the process.";
        lblheading.Text = "Information";
        Modalextnd1.Show();
    }

    //Get the vendor status 
    public void GetVendorStatus()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Session["VendorId"])))
        {
            DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(Session["VendorId"]));
            if (objVendor.Length > 0)
            {
                VendorStatus = objVendor[0].VendorStatus;
            }
        }
        else
        {
            VendorStatus = 0;
        }
    }

    private void ManagerMail(long VendorID)
    {
        DataSet ManagerMail = ObjCommon.GetVQFManagerAutoMail(Convert.ToInt64(VendorID));
        if (ManagerMail.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ManagerMail.Tables[0].Rows.Count; i++)
            {
                StringBuilder Body = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
                Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hi, <br></font></td></tr>");
                Body.Append("<tr><td style='Padding-left:25'><font  face='verdana' size='2'>This is to inform you that" + " " + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + " " + " has successfully completed VQF.</font></td></tr>");
                Body.Append("<tr><td>&nbsp;</td></tr>");
                //Body.Append("<tr><td><font  face='verdana' size='2'>Thanks,</font></td></tr>");
                //Body.Append("<tr><td><font  face='verdana' size='2'> " + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + "</font><br></td></tr></table>");
                ObjCommon.SendMail(Convert.ToString(ManagerMail.Tables[0].Rows[i]["vendorEmail"]), Convert.ToString(ManagerMail.Tables[0].Rows[i]["ManagerEmail"]), "" + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + " - VQF Status - Complete ", Body);
                ObjCommon.UpdateVQFManagerAutoMail(Convert.ToInt64(ManagerMail.Tables[0].Rows[i]["NotificationID"]));
            }
        }
    }
    protected void grdFeedbackComplete_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //006-Sooraj - Modified for hiding the 'Safety' section for design consultant 
        if (!lblSafetyMessage.Visible)
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "Name").ConvertToString() == "Safety")
                {
                    e.Row.Style["display"] = "none";
                }
                else
                {
                    e.Row.Style["display"] = "";
                }
            }
    }

    #region Attachdocument Details

    /// <summary>
    /// Attach Document
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgAttch_Click(object sender, ImageClickEventArgs e)
    {
        lblempty.Visible = false;
        if (hdnVendorStatus.Value.Equals("1"))
        {
            if (DataListAttchView.Items.Count > 0)
            {
                trAttachDocument.Style["display"] = "none";
            }
            else
            {
                lblheading.Text = "Information";
                lblMsg.Text = "<br />No documents attached";
                Modalextnd1.Show();
            }
        }
        else
        {
            lblattach.Text = "";
            TxtFileName1.Focus();
            ModalAddDoc.Show();
            LoadAttchDocumentData();
        }

        if (imbSubmitStatus.ImageUrl != "~/images/subcominfo1.gif")
        {
            trAttachDocument.Style["display"] = "none";
            Td1.Style["display"] = "none";
            imbAttachClose1.Visible = true;
        }

    }

    /// <summary>
    /// Load attached document
    /// </summary>
    private void LoadAttchDocumentData()
    {
        VendorId = Convert.ToInt64(Session["VendorId"]);
        TxtFileName1.Text = "";
        TxtFileName2.Text = "";
        TxtFileName3.Text = "";
        TxtFileName4.Text = "";
        TxtFileName5.Text = "";

        Attch = ObjCommon.GetAttchDocumentData(VendorId, false, false);
        if (Attch.Tables[0].Rows.Count > 0)
        {
            DataListAttchView.DataSource = Attch;
            DataListAttchView.DataBind();
        }
        if (Attch.Tables[0].Rows.Count > 0)
        {
            tdattach.Style["Display"] = "";
            lblempty.Visible = false;
            DataListAttchView.Visible = true;
        }
        else
        {
            //tdattach.Style["Display"] = "none";
            //trAttachDocument.Visible = false; 
            //DataListAttchView.Visible = false;
            //lblMsg.Text = "<br />No documents attached";

            lblempty.Visible = true;
            tdattach.Style["Display"] = "";
            //lblheading.Text = "Information";
            lblempty.Text = "<br />No documents attached";
            //Modalextnd1.Show();
            DataListAttchView.Visible = false;

        }

    }

    /// <summary>
    /// Add Documents
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAdddoc_Click(object sender, ImageClickEventArgs e)
    {
        lblattach.Text = "";
        try
        {
            if (Page.IsValid)
            {
                TxtFileName1.Focus();
                lblattach.Text = string.Empty;
                if ((TxtFileName1.Text == "") && (TxtFileName2.Text == "") && (TxtFileName3.Text == "") && (TxtFileName4.Text == "") && (TxtFileName5.Text == "")
                        && !(Flu1.HasFile) && !(Flu2.HasFile) && !(Flu3.HasFile) && !(Flu4.HasFile) && !(Flu5.HasFile))
                {
                    lblattach.Text = "&nbsp;<li>Please upload at least one document </li>";
                    ModalAddDoc.Show();
                }
                else
                {
                    //file1
                    if (TxtFileName1.Text != "")
                    {
                        if (!(Flu1.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document one</li>";
                        }
                    }
                    else if (Flu1.HasFile)
                    {
                        if (TxtFileName1.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description one</li>";
                        }
                    }

                    //file2
                    if (TxtFileName2.Text != "")
                    {
                        if (!(Flu2.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document two</li>";
                        }
                    }
                    else if (Flu2.HasFile)
                    {
                        if (TxtFileName2.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description two</li>";
                        }
                    }

                    //file3
                    if (TxtFileName3.Text != "")
                    {
                        if (!(Flu3.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document three</li>";
                        }
                    }
                    else if (Flu3.HasFile)
                    {
                        if (TxtFileName3.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description three</li>";
                        }
                    }

                    //file4
                    if (TxtFileName4.Text != "")
                    {
                        if (!(Flu4.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document four</li>";
                        }
                    }
                    else if (Flu4.HasFile)
                    {
                        if (TxtFileName4.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description four</li>";
                        }
                    }

                    //file5
                    if (TxtFileName5.Text != "")
                    {
                        if (!(Flu5.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document five</li>";
                        }
                    }
                    else if (Flu5.HasFile)
                    {
                        if (TxtFileName5.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description five</li>";
                        }
                    }

                    int Insert = 0;
                    if (System.IO.Directory.Exists(DOCUMENT_LOCATION))
                    {
                        System.IO.Directory.CreateDirectory(DOCUMENT_LOCATION);
                    }
                    if ((Flu1.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu1.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu1.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                //strFileName1 = Convert.ToString(Session["VendorId"]) + Path.GetFileName(Flu1.PostedFile.FileName);
                                strFileName1 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu1.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName1;
                                Flu1.PostedFile.SaveAs(filename);
                                string filepath = strFileName1;
                                int Count = objRegistration.CheckDocument(Convert.ToInt64(Session["VendorId"]), TxtFileName1.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(TxtFileName1.Text, filepath);
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description1 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "&nbsp;<li>File should be less than 2MB of size for description1</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "&nbsp;<li>Invalid file type for description1</li>";
                        }
                    }

                    if ((Flu2.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu2.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu2.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                //strFileName2 = Convert.ToString(Session["VendorId"]) + Path.GetFileName(Flu2.PostedFile.FileName);
                                //string filename = Server.MapPath("../UploadDocuments/") + strFileName2;
                                //Flu2.PostedFile.SaveAs(filename);
                                //string filepath = "../UploadDocuments/" + strFileName2;
                                strFileName2 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu2.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName2;
                                Flu2.PostedFile.SaveAs(filename);
                                string filepath = strFileName2;
                                int Count = objRegistration.CheckDocument(Convert.ToInt64(Session["VendorId"]), TxtFileName2.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(TxtFileName2.Text, filepath);
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description2 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2MB of size for description2</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description2</li>";
                        }
                    }

                    if ((Flu3.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu3.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu3.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                //strFileName3 = Convert.ToString(Session["VendorId"]) + Path.GetFileName(Flu3.PostedFile.FileName);
                                //string filename = Server.MapPath("../UploadDocuments/") + strFileName3;
                                //Flu3.PostedFile.SaveAs(filename);
                                //string filepath = "../UploadDocuments/" + strFileName3;

                                strFileName3 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu3.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName3;
                                Flu3.PostedFile.SaveAs(filename);
                                string filepath = strFileName3;

                                int Count = objRegistration.CheckDocument(Convert.ToInt64(Session["VendorId"]), TxtFileName3.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(TxtFileName3.Text, filepath);
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description3 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2MB of size for description3</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type  for description3</li>";
                        }
                    }
                    if ((Flu4.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu4.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu4.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {

                                //strFileName4 = Convert.ToString(Session["VendorId"]) + Path.GetFileName(Flu4.PostedFile.FileName);
                                //string filename = Server.MapPath("../UploadDocuments/") + strFileName4;
                                //Flu4.PostedFile.SaveAs(filename);
                                //string filepath = "../UploadDocuments/" + strFileName4;
                                strFileName4 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu4.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName4;
                                Flu4.PostedFile.SaveAs(filename);
                                string filepath = strFileName4;
                                int Count = objRegistration.CheckDocument(Convert.ToInt64(Session["VendorId"]), TxtFileName4.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(TxtFileName4.Text, filepath);
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description4 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2MB of size for description4</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description4</li>";
                        }
                    }

                    if ((Flu5.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu5.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu5.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                //strFileName5 = Convert.ToString(Session["VendorId"]) + Path.GetFileName(Flu5.PostedFile.FileName);
                                //string filename = Server.MapPath("../UploadDocuments/") + strFileName5;
                                //Flu5.PostedFile.SaveAs(filename);
                                //string filepath = "../UploadDocuments/" + strFileName5;
                                strFileName5 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu5.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName5;
                                Flu5.PostedFile.SaveAs(filename);
                                string filepath = strFileName5;
                                int Count = objRegistration.CheckDocument(Convert.ToInt64(Session["VendorId"]), TxtFileName5.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(TxtFileName5.Text, filepath);
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "</li>Description5 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2MB of size for description5</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description5</li>";
                        }
                    }
                    if (lblattach.Text != "")
                    {
                        ModalAddDoc.Show();
                    }
                    else
                    {
                        if (Insert == 1)
                        {
                            lblError.Text = "Result";
                            TxtFileName1.Text = "";
                            TxtFileName2.Text = "";
                            TxtFileName3.Text = "";
                            TxtFileName4.Text = "";
                            TxtFileName5.Text = "";
                            LoadAttchDocumentData();
                            lblattach.Text = "&nbsp;<li>Document uploaded successfully</li>";
                            ModalAddDoc.Show();
                        }
                        else if (Insert == 0)
                        {
                            lblError.Text = "Result";
                            lblattach.Text = "&nbsp;<li>Error Occured in uploading the document </li>";
                            ModalAddDoc.Show();
                        }
                    }
                }
            }
            else
            {
                ModalAddDoc.Show();
            }
        }
        catch (System.UnauthorizedAccessException ex)
        {
            lblattach.Text = "<li>Access denied to the document attached</li>";
            ModalAddDoc.Show();
        }
        finally
        {
            TxtFileName1.Focus();
        }
    }

    /// <summary>
    /// Delete Attached documents
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void DataListAttchView_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        Int64 id = (Int64)DataListAttchView.DataKeys[e.Item.ItemIndex];

        DataSet AttachName = ObjCommon.SelectAttchDocName(id);


        if (File.Exists(AttachName.Tables[0].Rows[0]["FilePath"].ToString()))
        {
            File.Delete(AttachName.Tables[0].Rows[0]["FilePath"].ToString());
        }



        ObjCommon.DeleteAttchDoc(id);
        lblError.Text = "Result";
        LoadAttchDocumentData();
        lblattach.Text = "&nbsp;<li>File deleted successfully</li>";
        ModalAddDoc.Show();
    }

    protected void DataListAttchView_SelectedIndexChanged(object sender, EventArgs e)
    {
        string FileName = ((HiddenField)DataListAttchView.SelectedItem.FindControl("hdnFileName")).Value;
        Response.Redirect("../Common/GenerateFile.aspx?region=" + DOCUMENT_LOCATION + FileName);
        ModalAddDoc.Show();
    }

    private void DeleteVQFDocument(string fileName)
    {
        if (File.Exists(DOCUMENT_LOCATION + fileName))
        {
            File.Delete(DOCUMENT_LOCATION + fileName);
        }
    }
    /// <summary>
    /// Insert Documents
    /// </summary>
    /// <param name="FileName"></param>
    /// <param name="FilePath"></param>
    /// <param name="PageName"></param>
    private void InsertDocument(string FileName, string FilePath)
    {
        objRegistration.InsertInsuranceDocument(Convert.ToInt64(Session["VendorId"]), FileName, FilePath, false, false);
    }

    #endregion

    /// <summary>
    /// 003 - Added
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrint_Click(object sender, ImageClickEventArgs e)
    {
        //004
        // NOTE:  For external SSL site this need to be like this
        Common objCommon = new Common();

        //007-Sooraj : To read URL from web.config 
        var siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
        string url = string.Format("{0}/VQF/PrintVQFForm.aspx?vendorid={1}&print=1", siteUrl, Convert.ToInt64(Session["VendorId"]));
        string baseURI = string.Format("{0}", siteUrl);

        //007-Sooraj :Commented

           //string url = "https://" + HttpContext.Current.Request.Url.Host + "/VQF/PrintVQFForm.aspx?vendorid=" + Convert.ToInt64(Session["VendorId"]) + "&print=1";
           //string baseURI = "https://" + HttpContext.Current.Request.Url.Host; //+ ":" + HttpContext.Current.Request.Url.Port;

        //Common objCommon = new Common();
        //string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/VQF/PrintVQFForm.aspx?vendorid=" + Convert.ToInt64(Session["VendorId"]) + "&print=1";
        //string baseURI = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;

        //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //Stream pageStream = response.GetResponseStream();

        //this.Response.Redirect(url);
       objCommon.Print2PDF(url, baseURI, false);    //011
    }

    ///
    /// 003 - Added but not in use now
    ///
    //protected void imbHasAdobe_Click(object sender, ImageClickEventArgs e)
    //{
    //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
    //    Common objCommon = new Common();

    //    switch (((ImageButton)sender).ToolTip.ToUpper())
    //    {
    //        case "YES":
    //            modalHasAdobe.Hide();
    //            string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/VQF/PrintVQFForm.aspx?vendorid=" + Convert.ToInt64(Session["VendorId"]) + "&print=1";
    //            objCommon.Print2PDF(url);
    //            break;
    //        case "NO":
    //            modalHasAdobe.Hide();
    //            sb.Append(@"<script language='javascript'>");
    //            sb.Append(@"window.open('../VQF/PrintVQFForm.aspx','_blank');");
    //            sb.Append(@"</script>");
    //            ScriptManager.RegisterStartupScript(imbPrint, this.GetType(), "JSCR", sb.ToString(), false);
    //            break;
    //        default:
    //            modalHasAdobe.Hide();
    //            sb.Append(@"<script language='javascript'>");
    //            sb.Append(@"window.open('../VQF/PrintVQFForm.aspx','_blank');");
    //            sb.Append(@"</script>");
    //            ScriptManager.RegisterStartupScript(imbPrint, this.GetType(), "JSCR", sb.ToString(), false);
    //            break;
    //    }
    //}
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //Modalextnd1.Hide();
        Response.Redirect(Request.UrlReferrer.ToString());
    }
}