﻿using System;
using System.Data;
using System.Web.UI;

public partial class UserControl_uclAdminCompanyViewMenu : System.Web.UI.UserControl
{
    clsCompany objCompany = new clsCompany();
    public DataSet CompanyNotes;
    string User = string.Empty;
    string Notes = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //Change the menu based on the role of the person who logged in
        if (Request.QueryString["user"] == "admin")
        {
            User = "Admin";
            tdcomment.Style["display"] = "block";
            tdsave.Style["display"] = "block";
        }
        else if (Request.QueryString["user"] == "employee")
        {
            User = "Employee";
            tdcomment.Style["display"] = "none";
            tdsave.Style["display"] = "none";
            if (txtNotes.Text == "")
            {
                tdcomment.Style["display"] = "none";
            }
        }
        else
        {
            tdcomment.Style["display"] = "none";
            tdsave.Style["display"] = "none";
        }

        Notes = txtNotes.Text.ToString();
        switch (hdnAlert.Value)
        { 
            case "Red":
                radred.Checked = true;
                tdcomment.Style["display"] = "";
                break;
            case "Yellow":
                radyellow.Checked = true;
                tdcomment.Style["display"] = "";
                break;
            case "Warning":
                radwarning.Checked = true;
                tdcomment.Style["display"] = "none";
                break;
        }
        if ((Request.QueryString.Count > 0) && (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["VendorID"]))))
        {
            Session["VendorAdminId"] = Request.QueryString["VendorID"].ToString();
        }
        if (!Page.IsPostBack)
        {
            CallScript();
            long Vendor = 0;
            if (((!string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"]))) || Convert.ToString(Session["VendorAdminId"]) != "0"))
            {
                if (Convert.ToInt64(Session["VendorAdminId"]) > 0)
                {
                    Vendor = Convert.ToInt64(Session["VendorAdminId"]);
                }
                string Tag;
                CompanyNotes = objCompany.Checknotes();
                if (CompanyNotes.Tables[0].Rows.Count > 0)
                {

                    txtNotes.Text = CompanyNotes.Tables[0].Rows[0]["AddNotes"].ToString();
                    Tag = Convert.ToString(CompanyNotes.Tables[0].Rows[0]["Tag"]);
                    switch (Tag)
                    { 
                        case "0":
                            radred.Checked = true;
                            tdcomment.Style["display"] = "block";
                            hdnAlert.Value = "Red";
                            break;
                        case "1":
                            radyellow.Checked = true;
                            tdcomment.Style["display"] = "block";
                            hdnAlert.Value = "Yellow";
                            break;
                        case "2":
                            radwarning.Checked = true;
                            tdcomment.Style["display"] = "none";
                            hdnAlert.Value = "Warning";
                            break;
                        default:
                            radwarning.Checked = true;
                            hdnAlert.Value = "Warning";
                            tdcomment.Style["display"] = "none";
                            tdsave.Style["display"] = "none";
                            break;
                    }
                }
                else
                {
                    if (User.Equals("Admin"))
                    {
                        tdcomment.Style["display"] = "none";
                        tdsave.Style["display"] = "none";
                        hdnAlert.Value = "Warning";
                    }
                    else if (User.Equals("Employee"))
                    {
                        tdcomment.Style["display"] = "block";
                        tdsave.Style["display"] = "none";
                        if (txtNotes.Text == "")
                        {
                            tdcomment.Style["display"] = "none";
                        }
                    }
                }
            }
        }
    }

    //Add notes
    protected void lnkAddNotes_Click(object sender, EventArgs e)
    {
        tdcomment.Style["display"] = "none";
        tdsave.Style["display"] = "block";
        radwarning.Checked = true;
        hdnAlert.Value = "Warning";
    }

    //Javascript to the link
    private void CallScript()
    {
        radred.Attributes.Add("onClick", "return DisplayAlert('Red','" + tdcomment.ClientID + "','" + hdnAlert.ClientID + "' );");
        radyellow.Attributes.Add("onClick", "return DisplayAlert('Yellow','" + tdcomment.ClientID + "' ,'" + hdnAlert.ClientID + "');");
        radwarning.Attributes.Add("onClick", "return DisplayAlert('Warning','" + tdcomment.ClientID + "' ,'" + hdnAlert.ClientID + "');");
    }

}