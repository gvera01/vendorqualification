﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uclSidemenu.ascx.cs" Inherits="UserControl_uclSidemenu" %>
<link rel="stylesheet" type="text/css" href="../css/ddlevelsmenu-base.css" />
<link rel="stylesheet" type="text/css" href="../css/ddlevelsmenu-sidebar.css" />
<link rel="Stylesheet" type="text/css" href="../css/Haskell.css" />
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script type="text/javascript" src="../Script/ddlevelsmenu.js"></script>
<script type="text/javascript">
    //G. Vera - Added
    function ShowBox(modalBox) {
        $find("modalHasAdobe").show();
    }

    ddlevelsmenu.setup("ddsidemenubar", "sidebar") //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
    function ShowMessage() {
        alert("Details submitted successfully");
    }
    function ShowFile(fValue, fName) {
        alert(fValue);
        document.getElementById(fName).value = fValue;
    }
    function getConfirmation() {
        if (confirm("Are you sure you want to delete the selected document?"))
            return true;
        return false;
    }
    function CheckDocument() {
        var msg = "";
        //Document 1
        if (trimAll(document.getElementById("<%=Flu1.ClientID%>").value) != "") {
            var ext = document.getElementById("<%=Flu1.ClientID%>").value;
            var len = document.getElementById("<%=Flu1.ClientID%>").value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();
                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = "";
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file one</li> ";
                }
            }
        }
        //Document 2
        if (trimAll(document.getElementById("<%=Flu2.ClientID%>").value) != "") {
            var ext = document.getElementById("<%=Flu2.ClientID%>").value;
            var len = document.getElementById("<%=Flu2.ClientID%>").value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();
                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file two</li> ";
                }
            }
        }
        //Document 3
        if (trimAll(document.getElementById("<%=Flu3.ClientID%>").value) != "") {
            var ext = document.getElementById("<%=Flu3.ClientID%>").value;
            var len = document.getElementById("<%=Flu3.ClientID%>").value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();
                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file three</li> ";
                }
            }
        }
        //Document 4
        if (trimAll(document.getElementById("<%=Flu4.ClientID%>").value) != "") {
            var ext = document.getElementById("<%=Flu4.ClientID%>").value;
            var len = document.getElementById("<%=Flu4.ClientID%>").value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();
                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file four</li> ";
                }
            }
        }
        //Document 5
        if (trimAll(document.getElementById("<%=Flu5.ClientID%>").value) != "") {
            var ext = document.getElementById("<%=Flu5.ClientID%>").value;
            var len = document.getElementById("<%=Flu5.ClientID%>").value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();
                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file five</li>";
                }
            }
        }
        if (msg == "")
            return true;
        else {
            var txt = document.getElementById('<%=lblattach.ClientID%>');
            txt.innerHTML = msg;
            return false;
        }
    }
</script>
<style type="text/css">
    .style2
    {
        height: 23px;
    }
    .style3
    {
        height: 35px;
    }
</style>
<table width="100%">
    <tr>
        <td>
            <div id="ddsidemenubar" class="markermenu">
                <ul>
                    <li><a href="../VQF/Company.aspx" rel="ddsubmenuside1">Company</a></li>
                    <li><a href="../VQF/LegalFinancial.aspx" rel="ddsubmenuside2">Legal & Financial</a></li>
                    <li><a href="../VQF/TradeInformation.aspx" rel="ddsubmenuside3">Trade Information</a></li>
                    <!-- 006-Sooraj -->
                    <li id="liSafety" runat="server"><a href="../VQF/Safety.aspx" rel="ddsubmenuside4">Safety</a></li>
                    <li><a href="../VQF/References.aspx" rel="ddsubmenuside5">References</a></li>
                    <li><a href="../VQF/InsurancesBonding.aspx" rel="ddsubmenuside6">Insurance & Bonding</a></li>
                </ul>
            </div>
            <ul id="ddsubmenuside1" class="ddsubmenustyle blackwhite">
                <li><a href="../VQF/Company.aspx?submenu=1">Business Section</a></li>
                <li><a href="../VQF/Company.aspx?submenu=2">Business Type</a></li>
                <li id="liLabor" runat="server"><a href="../VQF/Company.aspx?submenu=3">Labor Affiliation</a></li>
                <li id="liCertification" runat="server"><a href="../VQF/Company.aspx?submenu=4">Certifications
                </a></li>
                <li><a href="../VQF/Company.aspx?submenu=5">Contacts</a></li>
                <li><a href="../VQF/Company.aspx?submenu=6">Officers</a></li>
            </ul>
            <!--Side Drop Down Menu 2 HTML-->
            <ul id="ddsubmenuside2" class="ddsubmenustyle blackwhite">
                <li><a href="../VQF/LegalFinancial.aspx?submenu=1">Legal </a></li>
                <li><a href="../VQF/LegalFinancial.aspx?submenu=2">Financial</a></li>
                <li><a href="../VQF/LegalFinancial.aspx?submenu=3">Banking</a></li>
            </ul>
            <!--Side Drop Down Menu 3 HTML-->
            <ul id="ddsubmenuside3" class="ddsubmenustyle blackwhite">
                <li><a href="../VQF/TradeInformation.aspx?submenu=1">Scopes of Work</a></li>
                <li><a href="../VQF/TradeInformation.aspx?submenu=2">Regions</a></li>
                <li><a href="../VQF/TradeInformation.aspx?submenu=3">Project Types</a></li>
                <%--G. Vera - 02/13/2013--%>
                <li><a href="../VQF/TradeInformation.aspx?submenu=4">Building Information<br />
                    Modeling</a></li>
            </ul>
            <ul id="ddsubmenuside4" class="ddsubmenustyle blackwhite">
                <%--G. Vera - 04/23/2013--%>
                <li><a href="../VQF/Safety.aspx?submenu=1">Experience Modification Rate</a></li>
                <li><a href="../VQF/Safety.aspx?submenu=2">Safety History / OSHA 300</a></li>
                <li><a href="../VQF/Safety.aspx?submenu=3">Questionaire</a></li>
            </ul>
            <ul id="ddsubmenuside5" class="ddsubmenustyle blackwhite">
                <li><a href="../VQF/References.aspx?submenu=1">Project References</a></li>
                <%--006-Sooraj --%>
                <li id="liSupplierCredit" runat="server"><a href="../VQF/References.aspx?submenu=2">
                    Supplier/Credit</a></li>
            </ul>
            <ul id="ddsubmenuside6" class="ddsubmenustyle blackwhite">
                <li><a href="../VQF/InsurancesBonding.aspx?submenu=1">Contacts</a></li>
                <li><a href="../VQF/InsurancesBonding.aspx?submenu=2">Aggregate Limits</a></li>
                <%--006-Sooraj --%>
                <li id="liBondingSurety" runat="server"><a href="../VQF/InsurancesBonding.aspx?submenu=3">
                    Bonding/Surety</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="grdFeedbackComplete" runat="server" AutoGenerateColumns="false"
                CellPadding="5" CellSpacing="1" GridLines="None" GridViewStyle="GridViewStyle"
                HeaderStyle-CssClass="completeSection" RowStyle-CssClass="rowstyleone" Width="100%"
                OnRowDataBound="grdFeedbackComplete_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Complete">
                        <HeaderStyle CssClass="completeSection" />
                        <ItemTemplate>
                            &nbsp;
                            <%# Eval("Name")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="grdFeedbackInComplete" runat="server" AutoGenerateColumns="false"
                CellPadding="5" CellSpacing="1" GridLines="None" GridViewStyle="GridViewStyle"
                HeaderStyle-CssClass="incompleteSection" RowStyle-CssClass="rowstyleone" Width="100%"
                OnRowDataBound="grdFeedbackComplete_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Incomplete">
                        <HeaderStyle CssClass="incompleteSection" />
                        <ItemTemplate>
                            &nbsp;
                            <%# Eval("Name")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            <cc1:ModalPopupExtender ID="Modalextnd" BehaviorID="Div1" TargetControlID="hdnValue"
                BackgroundCssClass="popupbg" PopupControlID="Div1" runat="server">
            </cc1:ModalPopupExtender>
            <asp:HiddenField ID="hdnValue" runat="server" />
            <div id="Div1" runat="server" class="popupdivsmall" style="display: none;">
                <div>
                    <table width="100%" cellpadding="4" cellspacing="0" border="0" bgcolor="white">
                        <tr>
                            <td class="listheading" colspan="2">
                                &nbsp; Message
                            </td>
                        </tr>
                        <tr>
                            <td class="formtdlt" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td id="tdFirst" runat="server" class="formtdrt" colspan="2">
                                Once submitted, you cannot edit the information in the following sections
                                <br />
                                <br />
                                &bull; &nbsp; Financial section under Legal & Financial
                                <br />
                                <asp:Label ID="lblSafetyMessage" runat="server">&bull; &nbsp; Experience Modification Rate and Safety History / OSHA 300 under Safety</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="bodytextcenter">
                                <asp:ImageButton ID="imbSubmitHome" runat="server" ImageUrl="~/Images/ok.jpg" ValidationGroup="Second"
                                    ToolTip="Ok" OnClick="imbSubmitHome_Click" />
                                &nbsp;
                                <asp:ImageButton ID="imbCancel1" runat="server" ImageUrl="~/Images/cancel.jpg" ToolTip="Close"
                                    ValidationGroup="Second" TabIndex="0" OnClientClick="javascript:Close();" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <cc1:ModalPopupExtender ID="modalRevision" BehaviorID="Div3" TargetControlID="hdnValue"
                CancelControlID="imgOkButton" BackgroundCssClass="popupbg" PopupControlID="Div3"
                runat="server">
            </cc1:ModalPopupExtender>
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <div id="Div3" runat="server" class="popupdivsmall" style="display: none;">
                <div>
                    <table width="100%" cellpadding="4" cellspacing="0" border="0" width="350" align="center">
                        <tr>
                            <td class="searchhdrbarbold" colspan="2">
                                Message
                            </td>
                        </tr>
                        <tr>
                            <td class="formtdlt" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="formtdrt" colspan="2">
                                The VQF you have submitted is not yet approved. Please try after 24 hours or contact
                                Haskell Administrator at XXX-XXX-XXXX
                            </td>
                        </tr>
                        <tr>
                            <td class="bodytextcenter" colspan="2">
                                <asp:ImageButton ID="imgOkButton" runat="server" ImageUrl="~/Images/ok.jpg" ValidationGroup="Second"
                                    ToolTip="Ok" />
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <cc1:ModalPopupExtender ID="modalHasAdobe" BehaviorID="modalHasAdobe" TargetControlID="HiddenField2"
                BackgroundCssClass="popupbg" PopupControlID="Div4" runat="server">
            </cc1:ModalPopupExtender>
            <asp:HiddenField ID="HiddenField2" runat="server" />
            <div id="Div4" runat="server" class="popupdivsmall" style="display: none;">
                <div>
                    <table width="100%" cellpadding="4" cellspacing="0" border="0" bgcolor="white">
                        <tr>
                            <td class="listheading" colspan="2">
                                &nbsp; Message
                            </td>
                        </tr>
                        <tr>
                            <td class="formtdlt" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td id="td4" runat="server" class="formtdrt" colspan="2">
                                Is Adobe reader installed?
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="bodytextcenter">
                                <asp:ImageButton ID="imbYes" runat="server" ImageUrl="~/Images/yes.jpg" ToolTip="Yes"
                                    TabIndex="1" />
                                &nbsp;
                                <asp:ImageButton ID="imbNo" runat="server" ImageUrl="~/Images/no.jpg" ToolTip="No"
                                    TabIndex="2" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="Div2" runat="server" class="popupdivsmall" style="display: none;">
                <table width="100%" cellpadding="4" cellspacing="0" border="0" bgcolor="white">
                    <tr>
                        <td class="listheading" colspan="2">
                            Message
                        </td>
                    </tr>
                    <tr>
                        <td class="formtdlt" colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td class="formtdrt" colspan="2">
                            Submission is been cancelled
                        </td>
                    </tr>
                    <tr>
                        <td class="formtdlt">
                            &nbsp;
                        </td>
                        <td class="bodytextcenter">
                            <asp:ImageButton ID="imbok" runat="server" ImageUrl="~/Images/ok.jpg" ValidationGroup="Third"
                                ToolTip="Ok" />
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:ImageButton ID="imbSubmitStatus" runat="server" CausesValidation="false" CommandName="Submit"
                ToolTip="Please click this button to submit your VQF record to Haskell" ImageUrl="~/Images/subcominfo1.gif"
                OnClick="imbSubmitStatus_Click" />
            <asp:HiddenField ID="hdnVendorStatus" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:ImageButton runat="server" ID="imbPrint" ImageUrl="~/Images/printfrm.gif" ToolTip="Print Form"
                OnClick="imbPrint_Click" />
            <%--<a href="../VQF/PrintVQFForm.aspx" title="Print Form" style="border-bottom-width: 0;"
                target="_blank">
                <img src="../Images/printfrm.gif" border="0" />
            </a>--%>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; vertical-align: top; margin-top: 20px; height: 33px;">
            &nbsp;
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.adobe.com/products/reader.html"
                ToolTip="Adobe Reader X" Target="_blank" Style="color: #990000; font-family: Calibri;
                font-size: small">Download Adobe Reader!</asp:HyperLink>
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:ImageButton ID="imbAttch" CausesValidation="False" runat="server" ImageUrl="~/Images/attachdoc.gif"
                OnClick="imgAttch_Click" ToolTip="Attach Document" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <cc1:ModalPopupExtender ID="ModalAddDoc" runat="server" TargetControlID="hdnAddDoc"
                PopupControlID="divAddDoc" BackgroundCssClass="popupbg">
            </cc1:ModalPopupExtender>
            <asp:HiddenField ID="hdnAddDoc" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <div id="divAddDoc" class="popupdivbg1" style="display: none;">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                <tr>
                                    <td class="Messageheading" runat="server" id="Td1">
                                        Attach Document
                                    </td>
                                </tr>
                                <tr id="trAttachDocument" runat="server">
                                    <td style="padding: 0px 5px">
                                        <table width="100%">
                                            <tr>
                                                <td class="popupdivcl" align="left" colspan="2">
                                                    <asp:Label ID="lblattach" runat="server" CssClass="errormsg"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt" colspan="2" style="padding: 10px 7px; border: 1px solid #000">
                                                    <b>NOTE:</b><br />
                                                    <br />
                                                    Each file should be less than or equal to 2 MB of size.<br />
                                                    Please upload files only in the format gif, jpg, jpeg, xls, xlsx, txt, pdf, doc,
                                                    docx
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <b>Description</b>
                                                </td>
                                                <td class="formtdrt">
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>Upload Path</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="TxtFileName1" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                </td>
                                                <td class="formtdrt">
                                                    <asp:FileUpload ID="Flu1" runat="server" CssClass="fileUpload" TabIndex="0" />
                                                    <asp:HiddenField ID="hdnFile1" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="TxtFileName2" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                </td>
                                                <td class="formtdrt">
                                                    <asp:FileUpload ID="Flu2" runat="server" CssClass="fileUpload" TabIndex="0" />
                                                    <asp:HiddenField ID="hdnFile2" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="TxtFileName3" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                </td>
                                                <td class="formtdrt">
                                                    <asp:FileUpload ID="Flu3" runat="server" CssClass="fileUpload" TabIndex="0" />
                                                    <asp:HiddenField ID="hdnFile3" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="TxtFileName4" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                </td>
                                                <td class="formtdrt">
                                                    <asp:FileUpload ID="Flu4" runat="server" CssClass="fileUpload" TabIndex="0" />
                                                    <asp:HiddenField ID="hdnFile4" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtdrt">
                                                    <asp:TextBox ID="TxtFileName5" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                </td>
                                                <td class="formtdrt">
                                                    <asp:FileUpload ID="Flu5" runat="server" CssClass="fileUpload" TabIndex="0" />
                                                    <asp:HiddenField ID="hdnFile5" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ModalPopupTd" colspan="2">
                                                    <asp:HiddenField runat="server" ID="hdnpath" />
                                                    <asp:ImageButton ID="imbAddDoc" runat="server" ImageUrl="~/Images/upload.jpg" ToolTip="Upload"
                                                        TabIndex="0" OnClick="imbAdddoc_Click" CausesValidation="true" />
                                                    &nbsp;
                                                    <asp:ImageButton ID="imbAttachClose" runat="server" ImageUrl="~/Images/close.jpg"
                                                        ToolTip="Close" TabIndex="0" OnClientClick="javascript:Close();" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                            <tr>
                                                <td class="Messageheading" runat="server" id="tdattach">
                                                    Attached Document files
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popupdivcl" align="left" colspan="2">
                                                    <asp:Label ID="lblempty" runat="server" CssClass="errormsg" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="even_pad">
                                                    <table cellpadding="5" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:DataList ID="DataListAttchView" runat="server" DataKeyField="Pk_DocumentID"
                                                                    CellPadding="3" OnSelectedIndexChanged="DataListAttchView_SelectedIndexChanged"
                                                                    OnDeleteCommand="DataListAttchView_DeleteCommand">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="bodytextcenter" style="padding-right: 0px; padding-left: 5px; background-color: #efefef;"
                                                                                width="50px">
                                                                                <%# Eval("Row") %>
                                                                            </td>
                                                                            <td class="bodytextleft" width="400px" style="background-color: #efefef;">
                                                                                <%--     <asp:Label ID="FileName" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>--%>
                                                                                <asp:LinkButton CssClass="bodytextleft" ID="FileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                    CommandName="select"></asp:LinkButton>
                                                                                <asp:HiddenField ID="hdnFileName" runat="server" Value='<%# Eval("Filepath") %>' />
                                                                            </td>
                                                                            <td align="right" style="background-color: #efefef; padding-right: 5px">
                                                                                <asp:ImageButton ID="Delete" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                                                    CommandName="delete" OnClientClick="javascript:return getConfirmation()" />
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ModalPopupTd" colspan="2">
                                        <asp:ImageButton ID="imbAttachClose1" runat="server" ImageUrl="~/Images/close.jpg"
                                            ToolTip="Close" TabIndex="0" OnClientClick="javascript:Close();" Visible="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdheight">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <Ajax:ModalPopupExtender ID="Modalextnd1" runat="server" TargetControlID="lblHidden"
                PopupControlID="modalPopupDiv1" BackgroundCssClass="popupbg">
            </Ajax:ModalPopupExtender>
            <asp:HiddenField ID="lblHidden" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <div id="modalPopupDiv1" class="popupdivsmall" style="display: none; width: 400;">
                <table cellpadding="0" cellspacing="0" border="0" width="400">
                    <tr>
                        <td class="searchhdrbarbold" runat="server" id="Td3">
                            <asp:Label ID="lblheading" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="popupdivcl tdheight" align="center">
                        </td>
                    </tr>
                    <tr>
                        <td class="popupdivcl">
                            <asp:Label ID="lblMsg" runat="server" CssClass="errormsg"></asp:Label>
                            <asp:Label ID="lblError" runat="server" CssClass="summaryerrormsg"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="popupdivcl tdheight" align="center">
                        </td>
                    </tr>
                    <tr>
                        <td class="popupdivcl" align="center">
                            <%--G. Vera 05/05/2015 - added server button--%>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/ok.jpg" ValidationGroup="Second"
                                ToolTip="Ok" OnClick="ImageButton1_Click" />

                            <%--<input type="button" value="OK" title="Ok" id="Button1" class="ModalPopupButton"
                                onclick="$find('Modalextnd1').hide();" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="popupdivcl tdheight" align="center">
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
