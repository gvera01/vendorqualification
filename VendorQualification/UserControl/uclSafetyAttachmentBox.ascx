﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uclSafetyAttachmentBox.ascx.cs" Inherits="UserControl_uclSafetyAttachmentBox" %>

<asp:UpdatePanel ID="pnlMain" runat="server">
<Triggers>
    <asp:PostBackTrigger ControlID="imbUploadDoc" />
</Triggers>
<ContentTemplate>
<div id="divSafetyAttachment" class="popupdivbg1" style="display: none">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                    <tr>
                        <td class="Messageheading" runat="server" id="Td1">Attach Document
                        </td>
                    </tr>
                    <tr id="trAttachDocument" runat="server">
                        <td style="padding: 0px 5px">
                            <table width="100%">
                                <tr>
                                    <td class="popupdivcl" align="left" >
                                        <asp:Label ID="lblattach" runat="server" CssClass="errormsg"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formtdrt"  style="padding: 10px 7px; border: 1px solid #000">
                                        <b>NOTE:</b><br />
                                        <br />
                                        File should be less than or equal to <%=ConfigurationManager.AppSettings["MaxFileSizeMBs"].ToString() %> MB of size.<br />
                                        Acceptable file formats are: gif, jpg, jpeg, xls, xlsx, txt, pdf, doc, and
                                        docx
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formtdrt">
                                        <b><%# this.hdnTitle.Value %></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formtdrt" >
                                        <asp:FileUpload ID="fupFile" runat="server" CssClass="fileUpload" TabIndex="0" ClientIDMode="Static" Width="400px"/>
                                        <asp:HiddenField ID="hdnFile" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" align="left" width="100%" border="0">
                                <tr>
                                    <td class="Messageheading" runat="server" id="tdattach">Attached Document
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popupdivcl" align="left">
                                        <asp:Label ID="lblempty" runat="server" CssClass="errormsg" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trAttachedDoc" runat="server" style="display: none">
                                    <td class="even_pad">
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="formtdrt" width="400px" style="background-color: #efefef;">
                                                    <asp:LinkButton CssClass="bodytextleft" ID="lnkFileName" runat="server"
                                                        CommandName="select" OnClick="lnkFileName_Click"></asp:LinkButton>
                                                    <asp:HiddenField ID="hdnFilePath" runat="server" />
                                                </td>
                                                <td align="right" style="background-color: #efefef; padding-right: 5px">
                                                    <asp:ImageButton ID="Delete" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                        OnClientClick="javascript:return getConfirmation()" OnClick="Delete_Click"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ModalPopupTd">
                            <asp:ImageButton ID="imbUploadDoc" runat="server" ImageUrl="~/Images/upload.jpg" ToolTip="Upload"
                                TabIndex="0" OnClick="imbUploadDoc_Click"  CausesValidation="true" />
                            &nbsp;
                            <asp:ImageButton ID="imbAttachClose1" runat="server" ImageUrl="~/Images/close.jpg"
                                ToolTip="Close" TabIndex="0" OnClick="imbAttachClose1_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdheight"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
    </ContentTemplate>
</asp:UpdatePanel>

<Ajax:ModalPopupExtender ID="modalAttachments" runat="server"
    PopupControlID="divSafetyAttachment" BackgroundCssClass="popupbg" >    
</Ajax:ModalPopupExtender>
<asp:HiddenField ID="hdnTarget" runat="server" />
<asp:HiddenField ID="hdnSafetyID" runat="server" />
<asp:HiddenField ID="hdnYear" runat="server" />
<asp:HiddenField ID="hdnTitle" runat="server" />


<script type="text/javascript">
    //G. Vera - Added
    function ShowBox(modalBox) {
        $find("modalHasAdobe").show();
    }

    ddlevelsmenu.setup("ddsidemenubar", "sidebar") //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
    function ShowMessage() {
        alert("Details submitted successfully");
    }
    function ShowFile(fValue, fName) {
        alert(fValue);
        document.getElementById(fName).value = fValue;
    }
    function getConfirmation() {
        if (confirm("Are you sure you want to delete the selected document?"))
            return true;
        return false;
    }
    function CheckDocument() {
        var msg = "";
        //Document 1
        if (trimAll(document.getElementById("<%=fupFile.ClientID%>").value) != "") {
            var ext = document.getElementById("<%=fupFile.ClientID%>").value;
            var len = document.getElementById("<%=fupFile.ClientID%>").value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();
                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = "";
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file one</li> ";
                }
            }
        }
        
        if (msg == "")
            return true;
        else {
            var txt = document.getElementById('<%=lblattach.ClientID%>');
            txt.innerHTML = msg;
            return false;
        }
    }
</script>


