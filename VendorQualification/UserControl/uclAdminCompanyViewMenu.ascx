﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uclAdminCompanyViewMenu.ascx.cs" Inherits="UserControl_uclAdminCompanyViewMenu" %>
<link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
  <style media="print" type="text/css">
        .hide_print {display: none;}
</style>
<script language="javascript" type="text/javascript">
function DisplayAlert(text,commenttd,hdn)
{
    if(text=="Red")
    {
        document.getElementById(commenttd).style.display="block";
        document.getElementById(hdn).value="Red";
    }
    else if(text=="Yellow")
    {
        document.getElementById(commenttd).style.display="block";
        document.getElementById(hdn).value="Yellow";
    } 
    else if(text=="Warning")
    {
        document.getElementById(commenttd).style.display="none";
        document.getElementById(hdn).value="Warning";
       
    }
}

</script>


<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="even_pad">
     
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="40%" rowspan="4" valign="top" style="padding: 5px">
                                <img src="../images/haskelllogo.jpg" />
                            </td>
                           
                            <td width="60%" >
                            
                     <%if (Request.QueryString["user"] == "employee")
                       { %>
                              <span class="hide_print"  > <%} %>
                                <table cellpadding="0" width="100%" cellspacing="0" border="0" align="right">
  
                                    <tr>
                                        
                                        <td id="tdcomment" runat="server" class="bodytextleft" style="display: none; " align="right" >
                                            <asp:Label ID="txtNotes" runat="server"  ></asp:Label>&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table> <%if (Request.QueryString["user"] == "employee")
                       { %></span>  <%} %>
                               
                            </td>
                        </tr>
                        <tr>
                            <td  >
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                          <%if (Request.QueryString["user"] == "employee")
                       { %> <span class="hide_print"> <%}
                                           %>
                        <tr>
                            <td id="tdsave" runat="server" align="right" style="display: none"  >
                                &nbsp;&nbsp;<asp:CheckBox ID="chkVPP" runat="server" />
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/V.gif" />
                                &nbsp;&nbsp;<asp:RadioButton ID="radred" runat="server" GroupName="Alert" />
                                <asp:Image ID="imgred" runat="server" ImageUrl="~/Images/red.png" />&nbsp;&nbsp;
                                <asp:RadioButton ID="radyellow" runat="server" GroupName="Alert" />
                                <asp:Image ID="imgyellow" runat="server" ImageUrl="~/Images/yellow.png" />&nbsp;&nbsp;
                                <asp:RadioButton ID="radwarning" runat="server" GroupName="Alert" />
                                <asp:Image ID="imgwar" runat="server" ImageUrl="~/Images/nowarning.gif" />&nbsp;&nbsp;
                                
                                <asp:HiddenField ID="hdnAlert" runat="server" />
                            </td>
                        </tr>
                        
                       <%if (Request.QueryString["user"] == "employee")
                       { %>  </span> <%}
                                           %>
                       <%-- <tr>
                            <td class="bodytextbold_right"  >
                                <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                    PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" CancelControlID="imbOk">
                                </Ajax:ModalPopupExtender>
                                <asp:HiddenField ID="lblHidden" runat="server" />
                                <%if (Session["EmployeeName"] != null) { Response.Write("Welcome" + " " + Session["EmployeeName"].ToString()); }%>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <div id="modalPopupDiv" class="popupdivsmall" style="display: none">
                                    <table cellpadding="0" cellspacing="0" border="0" width="425">
                                        <tr>
                                            <td class="searchhdrbarbold" runat="server" id="msgTd" colspan="3">
                                                <asp:Label ID="lblheading" runat="server" CssClass="searchhdrbarbold"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="popupdivcl">
                                                &nbsp;
                                                <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="popupdivcl">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="popupdivcl" align="center">
                                                <input type="button" value="OK" id="imbOk" title="Ok" class="ModalPopupButton" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="popupdivcl">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
             
        </td>
    </tr>
    <tr>
        <td class="bottommenutd" colspan="2">
            &nbsp;
        </td>
    </tr>
</table>

