﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
//using Microsoft.Office.Interop.Outlook;
using System.Net.Mail;
using System.Diagnostics;
using System.Web;
using VMSDAL;
#region Change Comments
/* ***************************************************************************************************************************
 * 001 - G. Vera - Changed to use the UpdateVendorBit() method from the clsRegistration class instead of the new 
 * UpdateVendorBitStatus() method that excludes the last modified date.
 * 002 - G. Vera 07/06/2012: VMS Stabilization Release 1.1 (JIRA:VPI-15)
 * 003 Sooraj Sudhakaran.T 09/28/2013: VMS Modification - Added code to hide section that do not apply to design consultant
 **************************************************************************************************************************** */
#endregion
public partial class UserControl_UclVQFEditSidemenu : System.Web.UI.UserControl
{
    private int _selectedMenu;

    Common ObjCommon = new Common();
    string strFileName1, strFileName2, strFileName3, strFileName4, strFileName5;
    string sMimeType;
    DataSet Complete = new DataSet();
    string DOCUMENT_LOCATION = string.Empty;

    public long VendorId;
    DataSet Attch = new DataSet();
    clsRegistration objRegistration = new clsRegistration();
    clsSafety objSafety = new clsSafety();
    clsLegal objLegal = new clsLegal();
    Microsoft.Office.Interop.Outlook.Application oApp1;
    public int SelectedMenu
    {
        get { return _selectedMenu; }
        set { _selectedMenu = value; }

    }
    private int status;
    public int VendorStatus
    {
        get { return status; }
        set { status = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        imbSubmitStatus.Style["display"] = "none";
        DOCUMENT_LOCATION = ConfigurationManager.AppSettings["VQFDocument"].ToString();
        if (!IsPostBack)
        {
            VendorId = Convert.ToInt64(Session["VendorAdminId"]);
            CallScripts();

            //get the vendor details

            DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(Session["VendorAdminId"]));
            string result = objRegistration.GetVendorCompanyType(Convert.ToInt64(Session["VendorAdminId"])).Trim();
            if ((!string.IsNullOrEmpty(result)) & (result == TypeOfCompany.DesignConsultant))
            {
                liLabor.Style["display"] = "none";
                liCertification.Style["display"] = ""; //003-Sooraj
                //003-Sooraj
                liSupplierCredit.Style["display"] = "none";
                liBondingSurety.Style["display"] = "none";
                liSafety.Style["display"] = "none";
                lblSafetyMessage.Visible = false;
            }
            else
            {
                liLabor.Style["display"] = "";
                liCertification.Style["display"] = "";

                //003-Sooraj
                liSupplierCredit.Style["display"] = "";
                liBondingSurety.Style["display"] = "";
                liSafety.Style["display"] = "";
                lblSafetyMessage.Visible = true;

            }
            VendorStatus = 0;   //GV
            if (objVendor.Length > 0)
            {
                VendorStatus = objVendor[0].VendorStatus;
                if ((objVendor[0].VendorStatus == 1) || (objVendor[0].VendorStatus == 2))
                {
                    imbSubmitStatus.ImageUrl = "~/images/updatevqf.gif";
                    imbSubmitStatus.ToolTip = "Update VQF";
                }
                else
                {
                    imbSubmitStatus.ImageUrl = "~/images/subcominfo1.gif";
                }
            }

            //G. Vera 05/12/2014: Backing out this change - DataSet ds = ObjCommon.CheckNewField(Convert.ToInt64(Session["VendorAdminId"]), VendorStatus.ToString());    //GV - added per Dianne..admin needs to see the actual status
            GetCompleteStatus();
            GetIncompleteStatus();
            string ThisPage = Page.AppRelativeVirtualPath;
            imbSubmitStatus.Visible = grdFeedbackComplete.Rows.Count.Equals(6);
        }
    }

    void mnuValidation_MenuItemClick(object sender, MenuEventArgs e)
    {
        int index = Int32.Parse(e.Item.Value);
    }

    private void CallScripts()
    {
        imbAddDoc.Attributes.Add("onClick", " return CheckDocument();");
    }

    /// <summary>
    /// Switch view when company changes 
    /// </summary>
    /// <param name="IsDesignConsultant"></param>
    /// <Author>Sooraj Sudhakaran.T</Author>
    /// <Date>30-SEP-2013</Date>
    public void SwitchViews(bool IsDesignConsultant)
    {
        if (IsDesignConsultant)
        {
            liLabor.Style["display"] = "none";
            liCertification.Style["display"] = "";
            //003-Sooraj
            liSupplierCredit.Style["display"] = "none";
            liBondingSurety.Style["display"] = "none";
            liSafety.Style["display"] = "none";
            lblSafetyMessage.Visible = false;
        }
        else
        {
            liLabor.Style["display"] = "";
            liCertification.Style["display"] = "";

            //003-Sooraj
            liSupplierCredit.Style["display"] = "";
            liBondingSurety.Style["display"] = "";
            liSafety.Style["display"] = "";
            lblSafetyMessage.Visible = true;

        }

        GetCompleteStatus();
        GetIncompleteStatus();
    }

    //Get the pages which are incomplete
    public void GetIncompleteStatus()
    {
        Complete = ObjCommon.GetVQFStatusData(Convert.ToInt64(Session["VendorAdminId"]), 0);
        grdFeedbackInComplete.DataSource = Complete;
        grdFeedbackInComplete.DataBind();
        Complete.Dispose();
    }

    //Get the pages which are completed
    public void GetCompleteStatus()
    {
        Complete = ObjCommon.GetVQFStatusData(Convert.ToInt64(Session["VendorAdminId"]), 1);
        grdFeedbackComplete.DataSource = Complete;
        grdFeedbackComplete.DataBind();
        imbSubmitStatus.Visible = grdFeedbackComplete.Rows.Count.Equals(6);
        Complete.Dispose();
    }

    //Change to complete and revision status
    protected void imbSubmitHome_Click(object sender, ImageClickEventArgs e)
    {
        DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(Session["VendorAdminId"]));
        DAL.VQFSafety SafetyResult = objSafety.GetSingleVQLSafetyData(Convert.ToInt64(Session["VendorAdminId"]));
        DAL.VQFLegal LegalResult = objLegal.GetSingleVQLlegalData(Convert.ToInt64(Session["VendorAdminId"]));
        string result = objRegistration.GetVendorCompanyType(Convert.ToInt64(Session["VendorAdminId"])).Trim();
        var IsDesignConsultant = result == TypeOfCompany.DesignConsultant;  //003-Sooraj
        long safetyId = 0;
        if (!IsDesignConsultant)
            safetyId = SafetyResult.PK_SafetyID;
      
        long LegalId = LegalResult.PK_LegalID;
        if (imbSubmitStatus.ImageUrl == "~/images/subcominfo1.gif")
        {
            if (objVendor.Length > 0)
            {
                imbSubmitStatus.ImageUrl = "~/images/updatevqf.gif";
                if ((Convert.ToString(objVendor[0].VendorStatus) == "3") && SafetyResult.EditStatus == 0 && LegalResult.EditStatus == 0)
                {
                    objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 2);
                    if (safetyId>0) //003-Sooraj
                    objSafety.UpdateSafetyBit(safetyId, 1);
                    objLegal.UpdateLegalBit(Convert.ToInt64(Session["VendorAdminId"]), 1);
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
                else
                {
                    if (IsDesignConsultant ||  SafetyResult != null)  //003-Sooraj
                    {
                        objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 1);
                        if (IsDesignConsultant || SafetyResult.EditStatus != 1)//003-Sooraj
                        {
                            if (safetyId > 0) //003-Sooraj
                            objSafety.UpdateSafetyBit(safetyId, 1);
                       
                            objLegal.UpdateLegalBit(Convert.ToInt64(Session["VendorAdminId"]), 1);
                            string adminemail = ConfigurationManager.AppSettings["adminemail"].ToString();
                            DAL.UspGetVendorDetailsByIdResult[] objresult = objRegistration.GetVendor(Convert.ToInt64(Session["VendorAdminId"]));
                            StringBuilder Body = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
                            Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hi" + " " + Convert.ToString(objresult[0].FirstName) + " " + Convert.ToString(objresult[0].LastName) + ", <br></font></td></tr>");
                            Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Welcome to Haskell!</font></td></tr>");
                            Body.Append("<tr><td>&nbsp;</td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Thank you for registering with Haskell’s Vendor Database.  Your business will now be eligible for future invitations to bid by any of our project teams.</font></td></tr>");
                            Body.Append("<tr><td>&nbsp;</td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Your company information will be valid for one year from date of submission.  At the time of expiration, the registered contact person will receive an e-mail reminder to update your company data.  If at any time during the year, the structure of your company changes, please notify our database coordinator. </font></td></tr>");
                            Body.Append("<tr><td></td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Please keep in mind that only one submission per company per year is necessary.</font></td></tr>");
                            Body.Append("<tr><td>&nbsp;</td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Your company information will be valid for one year from date of submission.  At the time of expiration, the registered contact person will receive an e-mail </font></td></tr>");
                            Body.Append("<tr><td>&nbsp;</td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Please use the tracking number <b>" + Convert.ToString(objresult[0].TrackingNumber) + "</b> to refer your record to Haskell.</font></td></tr>");
                            Body.Append("<tr><td>&nbsp;</td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Respectfully,</font><br/><br/></td></tr>");
                            Body.Append("<tr><td><font  face='verdana' size='2'>Haskell</font><br></td></tr></table>");
                            objSafety.SendMail(adminemail, Convert.ToString(objresult[0].Email), "Successful VQF Submission to Haskell.", Body);
                            Response.Redirect("~/AdminVQF/VQFComplete.aspx");
                        }
                    }
                }
            }
        }
        else
        {
            if (objVendor.Length > 0)
            {
                if ((Convert.ToString(objVendor[0].VendorStatus) == "1"))
                {
                    objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 0);
                }
                else
                {
                    objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 3);
                }
                imbSubmitStatus.ImageUrl = "~/images/subcominfo1.gif";
            }
        }
    }

    /// <summary>
    /// 002 - Added
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrint_Click(object sender, ImageClickEventArgs e)
    {
        Common objCommon = new Common();
        string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/VQF/PrintVQFForm.aspx?vendorid=" + Convert.ToInt64(Session["VendorId"]) + "&print=1";
        string baseURI = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;

        objCommon.Print2PDF(url, baseURI);
    }

    //Confirmation to change
    protected void imbSubmitStatus_Click(object sender, ImageClickEventArgs e)
    {
        if (imbSubmitStatus.ImageUrl == "~/images/updatevqf.gif")
        {
            Session["OUTLOOKDATA"] = string.Empty;
            Session["OUTLOOKTO"] = string.Empty;
        }

        if (imbSubmitStatus.ImageUrl == "~/images/subcominfo1.gif")
        {
            if (grdFeedbackComplete.Rows.Count == 6)
            {
                DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(Session["VendorAdminId"]));

                DAL.VQFSafety SafetyResult = objSafety.GetSingleVQLSafetyData(Convert.ToInt64(Session["VendorAdminId"]));
                DAL.VQFLegal LegalResult = objLegal.GetSingleVQLlegalData(Convert.ToInt64(Session["VendorAdminId"]));
                if (objVendor.Length > 0)
                {
                    string result = objRegistration.GetVendorCompanyType(Convert.ToInt64(Session["VendorId"])).Trim();
                    if (result != TypeOfCompany.DesignConsultant)
                    {

                        if (Convert.ToString(objVendor[0].VendorStatus) == "3" && (SafetyResult.EditStatus == 1 && LegalResult.EditStatus == 1))
                        {
                            VendorStatus = Convert.ToInt32(objVendor[0].VendorStatus);
                            tdFirst.Style["display"] = "none";
                            objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 2);
                            ManagerMail(Convert.ToInt64(Session["VendorAdminId"]));

                            try
                            {
                                Session["OUTLOOKDATA"] = string.Empty;
                                // Outlook();
                            }
                            catch
                            {
                            }
                            Response.Redirect(Session["ReturnQuery"].ToString());
                            //Response.Redirect(Request.UrlReferrer.ToString());

                        }

                        else
                        {

                            // long safetyId = SafetyResult.PK_SafetyID; //003-Sooraj commented as its not used any where 

                            // if (  SafetyResult.EditStatus != 1 && LegalResult.EditStatus != 1)
                            //Added logic to support design consultant 
                            if (SafetyResult.EditStatus != 1 && LegalResult.EditStatus != 1)
                            {
                                tdFirst.Style["display"] = "";
                                Modalextnd.Show();
                            }
                            else
                            {
                                objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 1);
                                imbSubmitStatus.ImageUrl = "~/images/updatevqf.gif";
                                imbSubmitStatus.ToolTip = "Update VQF";
                                try
                                {
                                    Session["OUTLOOKDATA"] = string.Empty;
                                    // Outlook();
                                }
                                catch
                                {
                                }
                                Response.Redirect(Session["ReturnQuery"].ToString());
                                //Response.Redirect(Request.UrlReferrer.ToString());
                            }
                        }
                    }
                    else
                    {
                        if (Convert.ToString(objVendor[0].VendorStatus) == "3" &&  LegalResult.EditStatus == 1)
                        {
                            VendorStatus = Convert.ToInt32(objVendor[0].VendorStatus);
                            tdFirst.Style["display"] = "none";
                            objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 2);
                            ManagerMail(Convert.ToInt64(Session["VendorAdminId"]));

                            try
                            {
                                Session["OUTLOOKDATA"] = string.Empty;
                                // Outlook();
                            }
                            catch
                            {
                            }
                            Response.Redirect(Session["ReturnQuery"].ToString());
                            //Response.Redirect(Request.UrlReferrer.ToString());

                        }

                        else
                        {

                            // long safetyId = SafetyResult.PK_SafetyID; //003-Sooraj commented as its not used any where 

                            // if (  SafetyResult.EditStatus != 1 && LegalResult.EditStatus != 1)
                            //Added logic to support design consultant 
                            if (LegalResult.EditStatus != 1)
                            {
                                tdFirst.Style["display"] = "";
                                Modalextnd.Show();
                            }
                            else
                            {
                                objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 1);
                                imbSubmitStatus.ImageUrl = "~/images/updatevqf.gif";
                                imbSubmitStatus.ToolTip = "Update VQF";
                                try
                                {
                                    Session["OUTLOOKDATA"] = string.Empty;
                                    // Outlook();
                                }
                                catch
                                {
                                }
                                Response.Redirect(Session["ReturnQuery"].ToString());
                                //Response.Redirect(Request.UrlReferrer.ToString());
                            }
                        }
                    }

                }
            }

        }
        else
        {
            DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(Session["VendorAdminId"]));
            if (objVendor.Length > 0)
            {
                VendorStatus = objVendor[0].VendorStatus;
            }
            if ((objVendor.Length > 0) && (VendorStatus == 2))
            {
                objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 3);
                imbSubmitStatus.ImageUrl = "~/images/subcominfo1.gif";
                try
                {
                    //  Outlook();
                }
                catch
                {
                }
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            else if ((objVendor.Length > 0) && (VendorStatus == 1))
            {
                objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 0);
                imbSubmitStatus.ImageUrl = "~/images/subcominfo1.gif";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
        }
    }

    //Get the vendor status 
    public void GetVendorStatus()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Session["VendorId"])))
        {
            DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(Session["VendorId"]));
            if (objVendor.Length > 0)
            {
                VendorStatus = objVendor[0].VendorStatus;
            }
        }
        else
        {
            VendorStatus = 0;
        }
    }

    protected void grdFeedbackComplete_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //003-Sooraj - Modified for hiding the 'Safety' section for design consultant 
        if (!lblSafetyMessage.Visible)
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "Name").ConvertToString() == "Safety")
                {
                    e.Row.Style["display"] = "none";
                }
                else
                {
                    e.Row.Style["display"] = "";
                }
            }
    }
    private void ManagerMail(long VendorID)
    {
        DataSet ManagerMail = ObjCommon.GetVQFManagerAutoMail(Convert.ToInt64(VendorID));
        if (ManagerMail.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ManagerMail.Tables[0].Rows.Count; i++)
            {
                StringBuilder Body = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
                Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hi, <br></font></td></tr>");
                Body.Append("<tr><td style='Padding-left:25'><font  face='verdana' size='2'>This is to inform you that" + " " + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + " " + " has successfully completed VQF.</font></td></tr>");
                Body.Append("<tr><td>&nbsp;</td></tr>");
                //Body.Append("<tr><td><font  face='verdana' size='2'>Thanks,</font></td></tr>");
                //Body.Append("<tr><td><font  face='verdana' size='2'> " + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + "</font><br></td></tr></table>");
                ObjCommon.SendMail(Convert.ToString(ManagerMail.Tables[0].Rows[i]["vendorEmail"]), Convert.ToString(ManagerMail.Tables[0].Rows[i]["ManagerEmail"]), "" + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + " - VQF Status - Complete ", Body);
                ObjCommon.UpdateVQFManagerAutoMail(Convert.ToInt64(ManagerMail.Tables[0].Rows[i]["NotificationID"]));
            }
        }
    }
    private void Outlook()
    {
        bool Running = false;
        Process[] List = Process.GetProcesses();

        foreach (Process proc in List)
        {
            if (proc.ProcessName.ToLower().Equals("outlook"))
            {
                Running = true;
            }
        }

        if (!Running)
        {
            Process.Start("Outlook.exe");
            oApp1 = new Microsoft.Office.Interop.Outlook.Application();
        }

        try
        {
            Microsoft.Office.Interop.Outlook.MailItem email;
            email = (Microsoft.Office.Interop.Outlook.MailItem)oApp1.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);
            email.To = "ponnuraj@themajesticpeople.com";
            email.Subject = "Subject";
            //email.BodyFormat = Microsoft.Office.Interop.Outlook.OlBodyFormat.olFormatHTML;
            email.HTMLBody = Session["OUTLOOKDATA"].ToString();

            // if (Session["OUTLOOKDATA"].ToString() != "")
            ((Microsoft.Office.Interop.Outlook.MailItem)email).Display(false);
        }
        catch
        {
            foreach (Process proc in List)
            {
                if (proc.ProcessName.ToLower().Equals("outlook"))
                {
                    proc.Kill();


                    oApp1 = new Microsoft.Office.Interop.Outlook.Application();

                    Microsoft.Office.Interop.Outlook.MailItem email;
                    email = (Microsoft.Office.Interop.Outlook.MailItem)oApp1.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);
                    email.To = "ponnuraj@themajesticpeople.com";
                    email.Subject = "Subject";
                    //email.BodyFormat = Microsoft.Office.Interop.Outlook.OlBodyFormat.olFormatHTML;
                    email.HTMLBody = Session["OUTLOOKDATA"].ToString();

                    // if (Session["OUTLOOKDATA"].ToString() != "")
                    ((Microsoft.Office.Interop.Outlook.MailItem)email).Display(false);
                    break;
                }

            }

        }
    }
    #region Attachdocument Details

    /// <summary>
    /// Attach Document
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgAttch_Click(object sender, ImageClickEventArgs e)
    {
        lblempty.Visible = false;

        if (hdnVendorStatus.Value.Equals("1"))
        {
            //if (DataListAttchView.Items.Count > 0)
            //{
            //    trAttachDocument.Style["display"] = "none";
            //}
            //else
            //{
            trAttachDocument.Style["display"] = "none";
            Td1.Style["display"] = "none";
            imbAttachClose1.Visible = true;
            lblheading.Text = "Information";
            lblMsg.Text = "<br />No documents attached";
            Modalextnd1.Show();
            // }
        }
        else
        {

            lblattach.Text = "";
            TxtFileName1.Focus();
            ModalAddDoc.Show();
            LoadAttchDocumentData();
        }
        //if (imbSubmitStatus.ImageUrl != "~/images/subcominfo1.gif")
        //{
        //trAttachDocument.Style["display"] = "none";
        //Td1.Style["display"] = "none";
        //imbAttachClose1.Visible = true;
        //}
    }

    /// <summary>
    /// Load attached document
    /// </summary>
    private void LoadAttchDocumentData()
    {
        //VendorId = VendorId;
        TxtFileName1.Text = "";
        TxtFileName2.Text = "";
        TxtFileName3.Text = "";
        TxtFileName4.Text = "";
        TxtFileName5.Text = "";

        Attch = ObjCommon.GetAttchDocumentData(Convert.ToInt64(Session["VendorId"]), false, false);
        if (Attch.Tables[0].Rows.Count > 0)
        {
            DataListAttchView.DataSource = Attch;
            DataListAttchView.DataBind();
        }
        if (Attch.Tables[0].Rows.Count > 0)
        {
            tdattach.Style["Display"] = "";
            lblempty.Visible = false;
            DataListAttchView.Visible = true;
        }
        else
        {
            lblempty.Visible = true;
            tdattach.Style["Display"] = "";
            //lblheading.Text = "Information";
            lblempty.Text = "<br />No documents attached";
            //Modalextnd1.Show();
            DataListAttchView.Visible = false;

        }
    }

    /// <summary>
    /// Add Documents
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAdddoc_Click(object sender, ImageClickEventArgs e)
    {
        lblattach.Text = "";
        try
        {
            if (Page.IsValid)
            {
                TxtFileName1.Focus();
                lblattach.Text = string.Empty;
                if ((TxtFileName1.Text == "") && (TxtFileName2.Text == "") && (TxtFileName3.Text == "") && (TxtFileName4.Text == "") && (TxtFileName5.Text == "")
                        && !(Flu1.HasFile) && !(Flu2.HasFile) && !(Flu3.HasFile) && !(Flu4.HasFile) && !(Flu5.HasFile))
                {
                    lblattach.Text = "&nbsp;<li>Please upload at least one document </li>";
                    ModalAddDoc.Show();
                }
                else
                {
                    //file1
                    if (TxtFileName1.Text != "")
                    {
                        if (!(Flu1.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document one</li>";
                        }
                    }
                    else if (Flu1.HasFile)
                    {
                        if (TxtFileName1.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description one</li>";
                        }
                    }

                    //file2
                    if (TxtFileName2.Text != "")
                    {
                        if (!(Flu2.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document two</li>";
                        }
                    }
                    else if (Flu2.HasFile)
                    {
                        if (TxtFileName2.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description two</li>";
                        }
                    }

                    //file3
                    if (TxtFileName3.Text != "")
                    {
                        if (!(Flu3.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document three</li>";
                        }
                    }
                    else if (Flu3.HasFile)
                    {
                        if (TxtFileName3.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description three</li>";
                        }
                    }

                    //file4
                    if (TxtFileName4.Text != "")
                    {
                        if (!(Flu4.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document four</li>";
                        }
                    }
                    else if (Flu4.HasFile)
                    {
                        if (TxtFileName4.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description four</li>";
                        }
                    }

                    //file5
                    if (TxtFileName5.Text != "")
                    {
                        if (!(Flu5.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document five</li>";
                        }
                    }
                    else if (Flu5.HasFile)
                    {
                        if (TxtFileName5.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description five</li>";
                        }
                    }

                    int Insert = 0;
                    if (System.IO.Directory.Exists(DOCUMENT_LOCATION))
                    {
                        System.IO.Directory.CreateDirectory(DOCUMENT_LOCATION);
                    }
                    if ((Flu1.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu1.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu1.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                // strFileName1 = Convert.ToString(Session["VendorId"]) + Path.GetFileName(Flu1.PostedFile.FileName);
                                //string filename = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu1.FileName);
                                strFileName1 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu1.FileName);

                                //string filename = strFileName1;
                                //Flu1.PostedFile.SaveAs(filename);
                                //strFileName1 = VendorId + Path.GetFileName(Flu1.PostedFile.FileName);
                                string filename = DOCUMENT_LOCATION + strFileName1;
                                Flu1.PostedFile.SaveAs(filename);
                                string filepath = strFileName1;
                                int Count = objRegistration.CheckDocument(VendorId, TxtFileName1.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(TxtFileName1.Text, filepath);
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description1 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "&nbsp;<li>File should be less than 2MB of size for description1</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "&nbsp;<li>Invalid file type for description1</li>";
                        }
                    }

                    if ((Flu2.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu2.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu2.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                //strFileName2 = Convert.ToString(Session["VendorId"]) + Path.GetFileName(Flu2.PostedFile.FileName);
                                strFileName2 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu2.FileName);

                                //string filename = strFileName2;
                                //Flu2.PostedFile.SaveAs(filename);
                                // strFileName2 = VendorId + Path.GetFileName(Flu2.PostedFile.FileName);
                                string filename = DOCUMENT_LOCATION + strFileName2;
                                Flu1.PostedFile.SaveAs(filename);
                                string filepath = strFileName2;
                                int Count = objRegistration.CheckDocument(VendorId, TxtFileName2.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(TxtFileName2.Text, filepath);
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description2 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2MB of size for description2</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description2</li>";
                        }
                    }

                    if ((Flu3.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu3.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu3.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                // strFileName3 = Convert.ToString(Session["VendorId"]) + Path.GetFileName(Flu3.PostedFile.FileName);
                                strFileName3 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu3.FileName);

                                //string filename = strFileName3;
                                //strFileName3 = VendorId + Path.GetFileName(Flu3.PostedFile.FileName);
                                string filename = DOCUMENT_LOCATION + strFileName3;
                                Flu3.PostedFile.SaveAs(filename);
                                Flu3.PostedFile.SaveAs(filename);
                                string filepath = strFileName3;
                                int Count = objRegistration.CheckDocument(VendorId, TxtFileName3.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(TxtFileName3.Text, filepath);
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description3 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2MB of size for description3</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type  for description3</li>";
                        }
                    }
                    if ((Flu4.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu4.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu4.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {

                                //strFileName4 = Convert.ToString(Session["VendorId"]) + Path.GetFileName(Flu4.PostedFile.FileName);
                                strFileName4 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu4.FileName);

                                //string filename = strFileName4;
                                //Flu4.PostedFile.SaveAs(filename);
                                //strFileName4 = VendorId + Path.GetFileName(Flu4.PostedFile.FileName);
                                string filename = DOCUMENT_LOCATION + strFileName4;
                                Flu4.PostedFile.SaveAs(filename);
                                string filepath = strFileName4;
                                int Count = objRegistration.CheckDocument(VendorId, TxtFileName4.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(TxtFileName4.Text, filepath);
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description4 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2MB of size for description4</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description4</li>";
                        }
                    }

                    if ((Flu5.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu5.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu5.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                //strFileName5 = Convert.ToString(Session["VendorId"]) + Path.GetFileName(Flu5.PostedFile.FileName);
                                strFileName5 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu5.FileName);

                                //string filename = strFileName5;
                                //Flu5.PostedFile.SaveAs(filename);
                                //strFileName5 = VendorId + Path.GetFileName(Flu5.PostedFile.FileName);
                                string filename = DOCUMENT_LOCATION + strFileName5;
                                Flu5.PostedFile.SaveAs(filename);
                                string filepath = strFileName5;
                                int Count = objRegistration.CheckDocument(VendorId, TxtFileName5.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(TxtFileName5.Text, filepath);
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "</li>Description5 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2MB of size for description5</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description5</li>";
                        }
                    }
                    if (lblattach.Text != "")
                    {
                        ModalAddDoc.Show();
                    }
                    else
                    {
                        if (Insert == 1)
                        {
                            lblError.Text = "Result";
                            TxtFileName1.Text = "";
                            TxtFileName2.Text = "";
                            TxtFileName3.Text = "";
                            TxtFileName4.Text = "";
                            TxtFileName5.Text = "";
                            LoadAttchDocumentData();
                            lblattach.Text = "&nbsp;<li>Document uploaded successfully</li>";
                            ModalAddDoc.Show();
                        }
                        else if (Insert == 0)
                        {
                            lblError.Text = "Result";
                            lblattach.Text = "&nbsp;<li>Error Occured in uploading the document </li>";
                            ModalAddDoc.Show();
                        }
                    }
                }
            }
            else
            {
                ModalAddDoc.Show();
            }
        }
        catch (System.UnauthorizedAccessException ex)
        {
            lblattach.Text = "<li>Access denied to the document attached</li>";
            ModalAddDoc.Show();
        }
        finally
        {
            TxtFileName1.Focus();
        }
    }

    /// <summary>
    /// Delete Attached documents
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void DataListAttchView_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        Int64 id = (Int64)DataListAttchView.DataKeys[e.Item.ItemIndex];

        //File.Delete(DOCUMENT_LOCATION + e.CommandArgument.ToString());
        ObjCommon.DeleteAttchDoc(id);
        lblError.Text = "Result";
        LoadAttchDocumentData();
        lblattach.Text = "&nbsp;<li>File deleted successfully</li>";
        ModalAddDoc.Show();
    }

    protected void DataListAttchView_SelectedIndexChanged(object sender, EventArgs e)
    {
        string FileName = ((HiddenField)DataListAttchView.SelectedItem.FindControl("hdnFileName")).Value;
        Response.Redirect("../Common/GenerateFile.aspx?region=" + DOCUMENT_LOCATION + FileName);
        ModalAddDoc.Show();
    }

    /// <summary>
    /// Insert Documents
    /// </summary>
    /// <param name="FileName"></param>
    /// <param name="FilePath"></param>
    /// <param name="PageName"></param>
    private void InsertDocument(string FileName, string FilePath)
    {
        objRegistration.InsertInsuranceDocument(Convert.ToInt64(Session["VendorId"]), FileName, FilePath, false, false);
    }

    #endregion

}
