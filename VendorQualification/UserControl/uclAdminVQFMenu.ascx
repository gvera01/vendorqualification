﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uclAdminVQFMenu.ascx.cs"
    Inherits="UserControl_uclAdminVQFMenu" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="right" style="background: url(../Images/tabbg.gif) repeat-x">
            <asp:ImageButton ID="imbCompanyCertification" runat="server" ImageUrl="~/Images/compcert.gif"
                PostBackUrl="~/Admin/VendorMatch.aspx" />
            <asp:ImageButton ID="imbCSICodes" runat="server" ImageUrl="~/Images/csi.gif" PostBackUrl="~/Admin/SavedSearches.aspx" />
        </td>
    </tr>
</table>
