﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UclTopMenu.ascx.cs" Inherits="UserControl_UclTopMenu" %>
<link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
<table cellpadding="0" cellspacing="0" width="100%">
 <tr> 
 <td>
 <table  width="100%" border="0" cellpadding="0" cellspacing="0">
 <tr>
            <td class="even_pad"  >
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" style="padding: 5px">
                          <img src="../images/haskelllogo.jpg" alt="Haskell" /> 
                        </td>
                        <td width="50%" class="bodytextbold_right" >
                           <% 
                               if ((Session["EmployeeName"] != null) ) { Response.Write("Welcome" + " " + Session["EmployeeName"].ToString()); }%>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="headerbg">
                <tr><td align="right">
                <table width="35%" border="0" cellspacing="0" cellpadding="0" class="main_link">
                <tr>           
            <td>
                <a href="../VQF/Company.aspx">VQF</a>
            </td>
            <td>
                |
            </td>
            <td>
                <a href="../Common/ContactUs.aspx">Contact Us</a>
            </td>
            <td>
                |
            </td>
            <td>
                <a href="../Common/ChangePassword.aspx?menu=1">Change My Password</a>
            </td>
            <td>
                |
            </td>
            <%--<td>
                <a href="../Common/Contact.aspx?menu=1">Contact Users</a>
            </td>
            <td>
                |
            </td>
            <td>
                <a href="../Common/Inquiries.aspx?menu=1">Inquiries</a>
            </td>
            <td>
                |
            </td>--%>
            <td>
                <a href="../Common/Home.aspx">Logout</a>
            </td></tr>
                </table>
                </td></tr>
          
                </table>
            </td>
            
        </tr>
      
 
 </table>
 </td>
 </tr>
 </table>