﻿using System;
using System.Data;
using System.Web.UI;

public partial class UserControl_UclAdminCompanyMenu : System.Web.UI.UserControl
{
    clsCompany objCompany = new clsCompany();
    public DataSet CompanyNotes;
    Common objCommon = new Common();
    string User;
    string Notes;

    protected void Page_Load(object sender, EventArgs e)
    {
        //Change the menu based on the role of the person who logged in
        if (Request.QueryString["user"] == "admin")
        {
            User = "Admin";
            tdadmin.Style["display"] = "block";
            tdcomment.Style["display"] = "block";
            tdsave.Style["display"] = "block";
            tdFinComment.Style["display"] = (chkFinancials.Checked) ? " " : "none";
            txtFinComment.ReadOnly = false;
        }
        else if (Request.QueryString["user"] == "employee")
        {
            User = "Employee";
            tdadmin.Style["display"] = "none";
            tdcomment.Style["display"] = "none";
            tdsave.Style["display"] = "none";
            if (txtNotes.Text == "")
            {
                tdcomment.Style["display"] = "none";
            }
            txtNotes.ReadOnly = true;
            tdFinComment.Style["display"] = (chkFinancials.Checked) ? " " : "none";
            txtFinComment.ReadOnly = true;
        }
        else
        {
            tdadmin.Style["display"] = "none";
            tdcomment.Style["display"] = "none";
            tdsave.Style["display"] = "none";
        }
        Notes = txtNotes.Text.ToString();
        switch (hdnAlert.Value)
        {
            case "Red":
                radred.Checked = true;
                tdcomment.Style["display"] = "block";
                break;
            case "Yellow":
                radyellow.Checked = true;
                tdcomment.Style["display"] = "block";
                break;
            case "Warning":
                radwarning.Checked = true;
                tdcomment.Style["display"] = "none";
                break;
        }
        if ((Request.QueryString.Count > 0) && (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["VendorID"]))))
        {
            Session["VendorAdminId"] = Request.QueryString["VendorID"].ToString();
            Session["VendorAdminId"] = Convert.ToInt64(objCommon.decode(Request.QueryString["vendorview"]));
        }
        if (!Page.IsPostBack)
        {
            CallScript();
            long Vendor = 0;
            if (((!string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"]))) || Convert.ToString(Session["VendorAdminId"]) != "0"))
            {
                if (Convert.ToInt64(Session["VendorAdminId"]) > 0)
                {
                    Vendor = Convert.ToInt64(Session["VendorAdminId"]);
                }
                string Tag;
                CompanyNotes = objCompany.Checknotes();
                if (CompanyNotes.Tables[0].Rows.Count > 0)
                {
                    chkVPP.Checked = Convert.ToBoolean(CompanyNotes.Tables[0].Rows[0]["VPPFLAG"].ToString());
                    txtNotes.Text = CompanyNotes.Tables[0].Rows[0]["AddNotes"].ToString();
                    Tag = Convert.ToString(CompanyNotes.Tables[0].Rows[0]["Tag"]);

                    //007 - START
                    chkFinancials.Checked = (CompanyNotes.Tables[0].Rows.Count > 0) ? ((CompanyNotes.Tables[0].Rows[0]["IsFinancialsReceieved"] is DBNull) ? false : (bool)(CompanyNotes.Tables[0].Rows[0]["IsFinancialsReceieved"])) : false;
                    txtFinComment.Text = (CompanyNotes.Tables[0].Rows.Count > 0) ? (CompanyNotes.Tables[0].Rows[0]["FinancialsComment"] as string) : string.Empty;
                    tdFinComment.Style["display"] = (chkFinancials.Checked) ? " " : "none";
                    //007 - END

                    switch (Tag)
                    {
                        case "0":
                            radred.Checked = true;
                            tdadmin.Style["display"] = "none";
                            tdcomment.Style["display"] = "";
                            if (Request.QueryString["user"] == "admin")
                            { txtNotes.ReadOnly = false; }
                            else
                            {
                                txtNotes.ReadOnly = true;
                            }
                            hdnAlert.Value = "Red";
                            break;
                        case "1":
                            tdadmin.Style["display"] = "none";
                            radyellow.Checked = true;
                            tdcomment.Style["display"] = "";
                            if (Request.QueryString["user"] == "admin")
                            { txtNotes.ReadOnly = false; }
                            else
                            {
                                txtNotes.ReadOnly = true;
                            }
                            hdnAlert.Value = "Yellow";
                            break;
                        case "2":
                            radwarning.Checked = true;
                            tdadmin.Style["display"] = "none";
                            tdcomment.Style["display"] = "none";
                            hdnAlert.Value = "Warning";
                            break;
                        default:
                            radwarning.Checked = true;
                            hdnAlert.Value = "Warning";
                            if (User.Equals("Admin"))
                            {
                                tdadmin.Style["display"] = "";
                            }
                            tdcomment.Style["display"] = "none";
                            tdsave.Style["display"] = "none";
                            break;
                    }
                }
                else
                {
                    if (User == "Admin")
                    {
                        tdadmin.Style["display"] = "block";
                        tdcomment.Style["display"] = "none";
                        tdsave.Style["display"] = "none";
                        hdnAlert.Value = "Warning";
                        //007
                        tdFinComment.Style["display"] = "none";
                    }
                    else if (User == "Employee")
                    {
                        tdadmin.Style["display"] = "none";
                        tdcomment.Style["display"] = "block";
                        tdsave.Style["display"] = "none";
                        if (txtNotes.Text == "")
                        {
                            tdcomment.Style["display"] = "none";
                        }
                        txtNotes.ReadOnly = true;
                        //007
                        tdFinComment.Style["display"] = (chkFinancials.Checked) ? " " : "none";
                        txtFinComment.ReadOnly = true;
                    }
                }
            }
        }

        //007
        tdFinComment.Style["display"] = (chkFinancials.Checked) ? " " : "none";
    }

    //Add notes
    protected void lnkAddNotes_Click(object sender, EventArgs e)
    {
        tdcomment.Style["display"] = "none";
        tdsave.Style["display"] = "block";
        lnkAddNotes.Visible = false;
        radwarning.Checked = true;
        hdnAlert.Value = "Warning";
    }

    //Javascript to the link
    private void CallScript()
    {
        radred.Attributes.Add("onClick", "return DisplayAlert('Red','" + tdcomment.ClientID + "','" + hdnAlert.ClientID + "' );");
        radyellow.Attributes.Add("onClick", "return DisplayAlert('Yellow','" + tdcomment.ClientID + "' ,'" + hdnAlert.ClientID + "');");
        radwarning.Attributes.Add("onClick", "return DisplayAlert('Warning','" + tdcomment.ClientID + "' ,'" + hdnAlert.ClientID + "');");
    }

    //Save the comments and tags
    protected void imbSave_Click(object sender, ImageClickEventArgs e)
    {
        bool Result = false;
        if (Notes != "")
        {
            txtNotes.Text = Notes;
        }
        if ((!string.IsNullOrEmpty(txtNotes.Text)) && (!string.IsNullOrEmpty(hdnAlert.Value)))
        {
            if (hdnAlert.Value.Equals("Red"))
            {
                Result = objCompany.SaveNotes(txtNotes.Text, 0, chkVPP.Checked, chkFinancials.Checked, txtFinComment.Text); //007
                lnkAddNotes.Style["Display"] = "None";
            }
            else if (hdnAlert.Value.Equals("Yellow"))
            {
                Result = objCompany.SaveNotes(txtNotes.Text, 1, chkVPP.Checked, chkFinancials.Checked, txtFinComment.Text); //007
                lnkAddNotes.Style["Display"] = "None";
            }
            else if (hdnAlert.Value.Equals("Warning"))
            {
                Result = objCompany.SaveNotes(null, 2, chkVPP.Checked, chkFinancials.Checked, txtFinComment.Text);  //007
                txtNotes.Text = string.Empty;
                lnkAddNotes.Style["Display"] = "None";
            }
        }
        else if (string.IsNullOrEmpty(txtNotes.Text) && string.IsNullOrEmpty(hdnAlert.Value))
        {
            lblMsg.Text = "&nbsp;<li>Please enter notes and select a tag</li>";
            lblheading.Text = "Validation";
            modalExtnd.Show();
        }
        else
        {
            if (!string.IsNullOrEmpty(txtNotes.Text))
            {
                if (string.IsNullOrEmpty(hdnAlert.Value))
                {
                    lblMsg.Text = "&nbsp;<li>Please select a tag for notes</li>";
                    lblheading.Text = "Validation";
                    modalExtnd.Show();
                }
            }
            else if (((hdnAlert.Value.Equals("Red")) || (hdnAlert.Value.Equals("Yellow"))))
            {
                if (string.IsNullOrEmpty(txtNotes.Text))
                {
                    lblMsg.Text = "&nbsp;<li>Please enter notes</li>";
                    lblheading.Text = "Validation";
                    modalExtnd.Show();
                }
            }
            else if (hdnAlert.Value.Equals("Warning"))
            {
                Result = objCompany.SaveNotes(null, 2, chkVPP.Checked, chkFinancials.Checked, txtFinComment.Text);  //007
                txtNotes.Text = string.Empty;
                lnkAddNotes.Style["Display"] = "None";
            }
           
        }

        if (Result == true)
        {
            lblheading.Text = "Result";
            lblMsg.Text = "&nbsp;<li>Company notes submitted successfully</li>";
            modalExtnd.Show();
        }




    }
}