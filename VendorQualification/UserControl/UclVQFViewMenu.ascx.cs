﻿using System;
using System.Web.UI;
using System.Web;
using VMSDAL;
#region Change Comments
/* ***************************************************************************************************************************
 * 001 Sooraj Sudhakaran.T 09/28/2013: VMS Modification - Added code to hide section that do not apply to design consultant
 **************************************************************************************************************************** */
#endregion
public partial class UserControl_UclVQFViewMenu : System.Web.UI.UserControl
{
    string usertype, match;
    string vendorview, riskid, pagerec, curpage, vname, pno, bpno, Matchno, pagerec0, curpage0, Maintain;
    Common objCommon = new Common();
    string[] QueryStrSplit;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["ReturnQuery"] = null;
            Session["QueryStr"] = Request.QueryString["vendorview"] + ":" + Request.QueryString["riskid"] + ":" + Request.QueryString["match"] + ":" + Request.QueryString["PageRecords1"] + ":" + Request.QueryString["CurrentPage1"] + ":" + Request.QueryString["VendorName"] + ":" + Request.QueryString["ProjectN0"] + ":" + Request.QueryString["BidPackNO"] + ":" + Request.QueryString["PageRecords"] + ":" + Request.QueryString["CurrentPage"] + ":" + Request.QueryString["user"] + ":" + Request.QueryString["Maintain"];
        }
        QueryStrSplit = Session["QueryStr"].ToString().Split(':');
        vendorview = QueryStrSplit[0];
        riskid = QueryStrSplit[1];
        Matchno = QueryStrSplit[2];
        pagerec = QueryStrSplit[3];
        curpage = QueryStrSplit[4];
        vname = QueryStrSplit[5];
        pno = QueryStrSplit[6];
        bpno = QueryStrSplit[7];
        pagerec0 = QueryStrSplit[8];
        curpage0 = QueryStrSplit[9];
        usertype = QueryStrSplit[10];
        Maintain = QueryStrSplit[11];
        if (usertype == "Admin" || usertype == "admin")
            lnkEdit.Visible = true;
        else
            lnkEdit.Visible = false;


        //Show or hide view based on Type of company - //001-Sooraj
        SwitchDesignConsultantView();
    }
    //001
    /// <summary>
    /// Switch view based on logged in user
    /// </summary>
    /// <param name="IsShow"></param>
    /// <Author>Sooraj Sudhakaran.T</Author>
    ///  <Date>20-SEP-2013</Date>
    private void SwitchDesignConsultantView()
    {
        //Design Consultant 
        bool IsShow = (Session["VendorCompanyType"].ConvertToString() == TypeOfCompany.DesignConsultant) ? true : false;
        imbSafety.Style["display"] = IsShow ? "none" : "";
    }


    protected void Imghome_Click(object sender, ImageClickEventArgs e)
    {

        if (Session["match"] != null)
        {
            match = Session["match"].ToString();
        }

        //Change based on the role of the user who logged in
        if (Maintain == "Maintain")
        {
            Response.Redirect("../Admin/VendorMaintenance.aspx?user=" + usertype + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&Maintain=Maintain");
        }
        else if (match == "match" || usertype == "Admin")
        {
            Response.Redirect("../Admin/VendorMatch.aspx?user=" + usertype);
        }
        else if (match == "SRE" || usertype == "Admin")
        {
            //Response.Redirect("~/SRE/ViewSRE.aspx?VendorID=" + Request.QueryString["VendorId"].ToString() + "&PageRecords=" + objCommon.encode(Request.QueryString["PageRecords"].ToString()) + "&CurrentPage=" + objCommon.encode(Request.QueryString["CurrentPage"].ToString()) + "&VendorName=" + Request.QueryString["VendorName"].ToString() + "&ProjectN0=" + Request.QueryString["ProjectN0"].ToString() + "&BidPackNO=" + Request.QueryString["BidPackNO"].ToString());
            //Response.Redirect("~/SRE/ViewSRE.aspx?VendorId=" + Request.QueryString["riskid"].ToString() + "&PageRecords=" + objCommon.encode(Request.QueryString["PageRecords"].ToString()) + "&CurrentPage=" + objCommon.encode(Request.QueryString["CurrentPage"].ToString()) + "&PageRecords1=" + Request.QueryString["PageRecords1"].ToString() + "&CurrentPage1=" + Request.QueryString["CurrentPage1"].ToString() + "&VendorName=" + Request.QueryString["VendorName"].ToString() + "&ProjectN0=" + Request.QueryString["ProjectN0"].ToString() + "&BidPackNO=" + Request.QueryString["BidPackNO"].ToString());
            Response.Redirect("~/SRE/ViewSRE.aspx?VendorId=" + riskid + "&PageRecords=" + pagerec + "&CurrentPage=" + curpage + "&PageRecords1=" + pagerec0 + "&CurrentPage1=" + curpage0 + "&VendorName=" + vname + "&ProjectN0=" + pno + "&BidPackNO=" + bpno);

        }
        else
        {
            Response.Redirect("../Admin/SearchVendor.aspx?user=" + usertype);
        }
    }
    protected void imbCompany_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["match"] != null)
        {
            match = Session["match"].ToString();
        }
        //Change based on the role of the user who logged in
        if (Maintain == "Maintain")
        {
            Response.Redirect("~/VQFView/CompanyView.aspx?user=" + usertype + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&Maintain=Maintain");
        }
        else if (match == "match" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/CompanyView.aspx?user=" + usertype + "&match=" + match);
        }
        else if (match == "match" || usertype == "Admin")
        {
            Response.Redirect("../Admin/VendorMatch.aspx?user=" + usertype);
        }
        else if (match == "SRE" || usertype == "Admin")
        {
            Response.Redirect("~/VQFView/CompanyView.aspx?user=" + usertype + "&vendorview=" + vendorview + "&riskid=" + riskid + "&match=" + Matchno + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&PageRecords1=" + pagerec + "&CurrentPage1=" + curpage + "&VendorName=" + vname + "&ProjectN0=" + pno + "&BidPackNO=" + bpno);
        }
        else if (match == "" && usertype == "employee")
        {
            Response.Redirect("~/VQFView/CompanyView.aspx?user=" + usertype);
        }
        else if (match == "" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/CompanyView.aspx?user=" + usertype);
        }
    }
    protected void imblegal_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["match"] != null)
        {
            match = Session["match"].ToString();
        }
        //Change based on the role of the user who logged in
        if (Maintain == "Maintain")
        {
            Response.Redirect("~/VQFView/LegalFinancialView.aspx?user=" + usertype + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&Maintain=Maintain");
        }
        else if (match == "match" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/LegalFinancialView.aspx?user=" + usertype + "&match=" + match);
        }
        else if (match == "match" || usertype == "Admin")
        {
            Response.Redirect("../Admin/VendorMatch.aspx?user=" + usertype);
        }
        else if (match == "SRE" || usertype == "Admin")
        {
            Response.Redirect("~/VQFView/LegalFinancialView.aspx?user=" + usertype + "&vendorview=" + vendorview + "&riskid=" + riskid + "&match=" + Matchno + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&PageRecords1=" + pagerec + "&CurrentPage1=" + curpage + "&VendorName=" + vname + "&ProjectN0=" + pno + "&BidPackNO=" + bpno);
        }
        else if (match == "" && usertype == "employee")
        {
            Response.Redirect("~/VQFView/LegalFinancialView.aspx?user=" + usertype);
        }
        else if (match == "" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/LegalFinancialView.aspx?user=" + usertype);
        }
    }
    protected void imbTrade_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["match"] != null)
        {
            match = Session["match"].ToString();
        }
        //Change based on the role of the user who logged in
        if (Maintain == "Maintain")
        {
            Response.Redirect("~/VQFView/TradeView.aspx?user=" + usertype + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&Maintain=Maintain");
        }
        else if (match == "match" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/TradeView.aspx?user=" + usertype + "&match=" + match);
        }
        else if (match == "match" || usertype == "Admin")
        {
            Response.Redirect("../Admin/VendorMatch.aspx?user=" + usertype);
        }
        else if (match == "SRE" || usertype == "Admin")
        {
            // Response.Redirect("~/SRE/ViewSRE.aspx?VendorId=" + Request.QueryString["riskid"].ToString() + "&PageRecords=" + objCommon.encode(Request.QueryString["PageRecords"].ToString()) + "&CurrentPage=" + objCommon.encode(Request.QueryString["CurrentPage"].ToString()) + "&VendorName=" + Request.QueryString["VendorName"].ToString() + "&ProjectN0=" + Request.QueryString["ProjectN0"].ToString() + "&BidPackNO=" + Request.QueryString["BidPackNO"].ToString());
            Response.Redirect("~/VQFView/TradeView.aspx?user=" + usertype + "&vendorview=" + vendorview + "&riskid=" + riskid + "&match=" + Matchno + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&PageRecords1=" + pagerec + "&CurrentPage1=" + curpage + "&VendorName=" + vname + "&ProjectN0=" + pno + "&BidPackNO=" + bpno);
        }
        else if (match == "" && usertype == "employee")
        {
            Response.Redirect("~/VQFView/TradeView.aspx?user=" + usertype);
        }
        else if (match == "" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/TradeView.aspx?user=" + usertype);
        }
    }
    protected void imbSafety_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["match"] != null)
        {
            match = Session["match"].ToString();
        }
        //Change based on the role of the user who logged in
        if (Maintain == "Maintain")
        {
            Response.Redirect("~/VQFView/SafetyView.aspx?user=" + usertype + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&Maintain=Maintain");
        }
        else if (match == "match" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/SafetyView.aspx?user=" + usertype + "&match=" + match);
        }
        else if (match == "match" || usertype == "Admin")
        {
            Response.Redirect("../Admin/VendorMatch.aspx?user=" + usertype);
        }
        else if (match == "SRE" || usertype == "Admin")
        {
            // Response.Redirect("~/SRE/ViewSRE.aspx?VendorId=" + Request.QueryString["riskid"].ToString() + "&PageRecords=" + objCommon.encode(Request.QueryString["PageRecords"].ToString()) + "&CurrentPage=" + objCommon.encode(Request.QueryString["CurrentPage"].ToString()) + "&VendorName=" + Request.QueryString["VendorName"].ToString() + "&ProjectN0=" + Request.QueryString["ProjectN0"].ToString() + "&BidPackNO=" + Request.QueryString["BidPackNO"].ToString());
            Response.Redirect("~/VQFView/SafetyView.aspx?user=" + usertype + "&vendorview=" + vendorview + "&riskid=" + riskid + "&match=" + Matchno + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&PageRecords1=" + pagerec + "&CurrentPage1=" + curpage + "&VendorName=" + vname + "&ProjectN0=" + pno + "&BidPackNO=" + bpno);

        }
        else if (match == "" && usertype == "employee")
        {
            Response.Redirect("~/VQFView/SafetyView.aspx?user=" + usertype);
        }
        else if (match == "" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/SafetyView.aspx?user=" + usertype);
        }
    }
    protected void imbRef_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["match"] != null)
        {
            match = Session["match"].ToString();
        }
        //Change based on the role of the user who logged in
        if (Maintain == "Maintain")
        {
            Response.Redirect("~/VQFView/ReferencesView.aspx?user=" + usertype + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&Maintain=Maintain");
        }
        else if (match == "match" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/ReferencesView.aspx?user=" + usertype + "&match=" + match);
        }
        else if (match == "match" || usertype == "Admin")
        {
            Response.Redirect("../Admin/VendorMatch.aspx?user=" + usertype);
        }
        else if (match == "SRE" || usertype == "Admin")
        {
            // Response.Redirect("~/SRE/ViewSRE.aspx?VendorId=" + Request.QueryString["riskid"].ToString() + "&PageRecords=" + objCommon.encode(Request.QueryString["PageRecords"].ToString()) + "&CurrentPage=" + objCommon.encode(Request.QueryString["CurrentPage"].ToString()) + "&VendorName=" + Request.QueryString["VendorName"].ToString() + "&ProjectN0=" + Request.QueryString["ProjectN0"].ToString() + "&BidPackNO=" + Request.QueryString["BidPackNO"].ToString());
            Response.Redirect("~/VQFView/ReferencesView.aspx?user=" + usertype + "&vendorview=" + vendorview + "&riskid=" + riskid + "&match=" + Matchno + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&PageRecords1=" + pagerec + "&CurrentPage1=" + curpage + "&VendorName=" + vname + "&ProjectN0=" + pno + "&BidPackNO=" + bpno);

        }
        else if (match == "" && usertype == "employee")
        {
            Response.Redirect("~/VQFView/ReferencesView.aspx?user=" + usertype);
        }
        else if (match == "" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/ReferencesView.aspx?user=" + usertype);
        }
    }
    protected void imbIns_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["match"] != null)
        {
            match = Session["match"].ToString();
        }
        //Change based on the role of the user who logged in
        if (Maintain == "Maintain")
        {
            Response.Redirect("~/VQFView/InsuranceBondingView.aspx?user=" + usertype + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&Maintain=Maintain");
        }
        else if (match == "match" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/InsuranceBondingView.aspx?user=" + usertype + "&match=" + match);
        }
        else if (match == "match" || usertype == "Admin")
        {
            Response.Redirect("../Admin/VendorMatch.aspx?user=" + usertype);
        }
        else if (match == "SRE" || usertype == "Admin")
        {
            // Response.Redirect("~/SRE/ViewSRE.aspx?VendorId=" + Request.QueryString["riskid"].ToString() + "&PageRecords=" + objCommon.encode(Request.QueryString["PageRecords"].ToString()) + "&CurrentPage=" + objCommon.encode(Request.QueryString["CurrentPage"].ToString()) + "&VendorName=" + Request.QueryString["VendorName"].ToString() + "&ProjectN0=" + Request.QueryString["ProjectN0"].ToString() + "&BidPackNO=" + Request.QueryString["BidPackNO"].ToString());
            Response.Redirect("~/VQFView/InsuranceBondingView.aspx?user=" + usertype + "&vendorview=" + vendorview + "&riskid=" + riskid + "&match=" + Matchno + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&PageRecords1=" + pagerec + "&CurrentPage1=" + curpage + "&VendorName=" + vname + "&ProjectN0=" + pno + "&BidPackNO=" + bpno);
        }
        else if (match == "" && usertype == "employee")
        {
            Response.Redirect("~/VQFView/InsuranceBondingView.aspx?user=" + usertype);
        }
        else if (match == "" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/InsuranceBondingView.aspx?user=" + usertype);
        }
    }
    protected void imbFile_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["match"] != null)
        {
            match = Session["match"].ToString();
        }
        //Change based on the role of the user who logged in
        if (Maintain == "Maintain")
        {
            Response.Redirect("~/VQFView/Files.aspx?user=" + usertype + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&Maintain=Maintain");
        }
        else if (match == "match" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/Files.aspx?user=" + usertype + "&match=" + match);
        }
        else if (match == "match" || usertype == "Admin")
        {
            Response.Redirect("../Admin/VendorMatch.aspx?user=" + usertype);
        }
        else if (match == "SRE" || usertype == "Admin")
        {
            // Response.Redirect("~/SRE/ViewSRE.aspx?VendorId=" + Request.QueryString["riskid"].ToString() + "&PageRecords=" + objCommon.encode(Request.QueryString["PageRecords"].ToString()) + "&CurrentPage=" + objCommon.encode(Request.QueryString["CurrentPage"].ToString()) + "&VendorName=" + Request.QueryString["VendorName"].ToString() + "&ProjectN0=" + Request.QueryString["ProjectN0"].ToString() + "&BidPackNO=" + Request.QueryString["BidPackNO"].ToString());
            Response.Redirect("~/VQFView/Files.aspx?user=" + usertype + "&vendorview=" + vendorview + "&riskid=" + riskid + "&match=" + Matchno + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0 + "&PageRecords1=" + pagerec + "&CurrentPage1=" + curpage + "&VendorName=" + vname + "&ProjectN0=" + pno + "&BidPackNO=" + bpno);

        }
        else if (match == "" && usertype == "employee")
        {
            Response.Redirect("~/VQFView/Files.aspx?user=" + usertype);
        }
        else if (match == "" && usertype == "admin")
        {
            Response.Redirect("~/VQFView/Files.aspx?user=" + usertype);
        }
    }
    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        if (Session["match"] != null)
        {
            match = Session["match"].ToString();
        }
        //Change based on the role of the user who logged in
        if (Maintain == "Maintain")
        {
            Session["ReturnQuery"] = "../Admin/VendorMaintenance.aspx?user=" + usertype + "&PageRecords=" + pagerec0 + "&CurrentPage=" + curpage0;
            Response.Redirect("../AdminVQF/Company.aspx");
        }
        else if (match == "match" || usertype == "Admin")
        {
            Session["ReturnQuery"] = "../Admin/VendorMatch.aspx?user=" + usertype;
            Response.Redirect("../AdminVQF/Company.aspx");
            //Response.Redirect("../Admin/VendorMatch.aspx?user=" + usertype);
        }
        else if (match == "SRE" || usertype == "Admin")
        {
            Session["ReturnQuery"] = "~/SRE/ViewSRE.aspx?VendorId=" + riskid + "&PageRecords=" + pagerec + "&CurrentPage=" + curpage + "&PageRecords1=" + pagerec0 + "&CurrentPage1=" + curpage0 + "&VendorName=" + vname + "&ProjectN0=" + pno + "&BidPackNO=" + bpno;
            Response.Redirect("../AdminVQF/Company.aspx");
            // Response.Redirect("~/SRE/ViewSRE.aspx?VendorId=" + riskid + "&PageRecords=" + objCommon.encode(pagerec) + "&CurrentPage=" + objCommon.encode(curpage) + "&PageRecords1=" + pagerec0 + "&CurrentPage1=" + curpage0 + "&VendorName=" + vname + "&ProjectN0=" + pno + "&BidPackNO=" + bpno);
        }
        else
        {
            Session["ReturnQuery"] = "../Admin/SearchVendor.aspx?user=" + usertype;
            Response.Redirect("../AdminVQF/Company.aspx");
            //Response.Redirect("../Admin/SearchVendor.aspx?user=" + usertype);
        }
    }
}