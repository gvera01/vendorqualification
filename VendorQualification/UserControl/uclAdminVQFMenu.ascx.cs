﻿using System;

public partial class UserControl_uclAdminVQFMenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void DisplayButton(int Page)
    {
        switch (Page)
        {
            case 1:
                imbCompanyCertification.ImageUrl = "~/Images/compcert.gif";
                imbCSICodes.ImageUrl = "~/Images/compcerta.gif";

                break;
            case 2:
                imbCompanyCertification.ImageUrl = "~/Images/csi.gif";
                imbCSICodes.ImageUrl = "~/Images/csia.gif";
                break;
        }
    }
}