﻿using System;
using VMSDAL;
//*******************************************************************************************************************
// Change Comments:
// 001 G. Vera 12/29/2016:  Add new employee role
//*******************************************************************************************************************
public partial class UserControl_UclAdminMenu : System.Web.UI.UserControl
{
    RiskEvaluation riskEvaluation = new RiskEvaluation();
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["RISKIDLOGOUT"] != null)
        //{
        //    riskEvaluation.UpdateStatus(Convert.ToInt64(Session["RISKIDLOGOUT"]));
        //    Session["RISKIDLOGOUT"] = null;
        //}
    }

   public void DisplayMenu(string Access)
    {
        string role = Session["EmployeeRole"].ConvertToString();    //001
        if (Access.Equals("Admin"))
        {
            tdMaintenance.Style["display"] = "";
            //001
            if (role != clsAdminLogin.EmployeeRole.Admin_SRE.ToString())
            {
                trAdminSRE.Style["display"] = "";
                trEmployeeSRE.Style["display"] = "none";
            }
            else
            {
                trEmployeeSRE.Style["display"] = "";
                trAdminSRE.Style["display"] = "none";
            }
        }
        else
        {
            tdMaintenance.Style["display"] = "none";
            trEmployeeSRE.Style["display"] = "";
            trAdminSRE.Style["display"] = "none";
        }
    }
}