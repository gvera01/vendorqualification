﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class UserControl_uclAttachmentDialog : System.Web.UI.UserControl
{
    clsTradeInfomation objTradeInformation = new clsTradeInfomation();
    clsRegistration objRegistration = new clsRegistration();
    clsCompany objCompany = new clsCompany();
    Common objCommon = new Common();
    clsSearchVendor objSearchVendor = new clsSearchVendor();
    public DataSet Vendordetails = new DataSet();
    public DataSet SRAttch = new DataSet();
    DataSet Attch = new DataSet();
    DataSet Attch1 = new DataSet();
    public string SortExp;
    public string SearchSortExp;
    string DOCUMENT_LOCATION = string.Empty;
    string sMimeType;
    string strFileName1, strFileName2, strFileName3, strFileName4, strFileName5;

    private static int _vendorID;
    public static int VendorID
    {
        get { return _vendorID; }
        set { _vendorID = value; }
    }
    //public int VendorID
    //{
    //    get { return Convert.ToInt32(ViewState["VendorID"]); }
    //}


    protected void Page_Load(object sender, EventArgs e)
    {
        DOCUMENT_LOCATION = ConfigurationManager.AppSettings["VQFDocument"].ToString();
    }

    /// <summary>
    /// 001 - Added
    /// </summary>
    /// <param name="VendorId"></param>
    public void SRLoadAttchDocumentData(int VendorId)
    {

        PaSREAttach.Visible = true;
        PanAttach.Visible = false;
        TxtFileName1.Text = "";
        TxtFileName2.Text = "";
        TxtFileName3.Text = "";
        TxtFileName4.Text = "";
        TxtFileName5.Text = "";
        trAttachDocument.Style["Display"] = "";
        trAttachHeader.Style["Display"] = "";
        SRAttch = objCommon.GetAttchDocumentData(VendorId, false, true);
        if (SRAttch.Tables[0].Rows.Count > 0)
        {
            lblSRmessage.Text = "";
            lblattach.Text = "";
            tdattach.Style["Display"] = "";
            tdSRattach.Style["Display"] = "";
            imgClose.Visible = false;
            DataListSRAttchView.Visible = true;
            DataListSRAttchView.DataSource = SRAttch;
            DataListSRAttchView.DataBind();
            if (SRAttch.Tables[0].Rows.Count > 3)
            {
                PaSREAttach.Style["overflow"] = "scroll";
                PaSREAttach.Style["Height"] = "225px";
                PaSREAttach.Style["width"] = "425px";
            }
            else
            {
                PaSREAttach.Style["overflow"] = "";
                PaSREAttach.Style["Height"] = "";
                PaSREAttach.Style["width"] = "";

            }

        }
        else
        {
            lblSRmessage.Text = "No files are uploaded by admin";
            tdattach.Style["Display"] = "None";
            tdSRattach.Style["Display"] = "None";
            trAttachHeader.Style["Display"] = "none";
            imgClose.Visible = true;
            DataListSRAttchView.Visible = false;

        }
    }

    /// <summary>
    /// 001 - Added
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>


    /// <summary>
    /// Insert Documents
    /// </summary>
    /// <param name="FileName"></param>
    /// <param name="FilePath"></param>
    /// <param name="PageName"></param>
    public void InsertDocument(int VendorID, string FileName, string FilePath, bool IsAdmin, bool IsSRAdmin)
    {
        objRegistration.InsertInsuranceDocument(VendorID, FileName, FilePath, IsAdmin, IsSRAdmin);
    }

    /// <summary>
    /// Load attached document
    /// </summary>
    public void LoadAttchDocumentData(int VendorId)
    {


        //VendorId = 1;
        PanAttach.Visible = true;
        PaSREAttach.Visible = false;
        TxtFileName1.Text = "";
        TxtFileName2.Text = "";
        TxtFileName3.Text = "";
        TxtFileName4.Text = "";
        TxtFileName5.Text = "";
        trAttachDocument.Style["Display"] = "";
        trAttachHeader.Style["Display"] = "";
        DataListAttchView.Visible = false;
        Attch = objCommon.GetAttchDocumentData(VendorId, false, false);
        if (Attch.Tables[0].Rows.Count > 0)
        {
            DataListAttchView.Visible = true;
            PanAttach.Visible = true;
            DataListAttchView.DataSource = Attch;
            DataListAttchView.DataBind();

            tblVendors.Style["Display"] = "";
            tdattach.Style["Display"] = "";

            if (Attch.Tables[0].Rows.Count > 3)
            {
                PanAttach.Style["overflow"] = "scroll";
                PanAttach.Style["Height"] = "200px";
                PanAttach.Style["width"] = "425px";

            }
        }
        else
        {
            tdattach.Style["Display"] = "none";
            tblVendors.Style["Display"] = "None";
            DataListAttchView.Visible = false;
            divAddDoc.Style["Height"] = "";
            //PanAttach.Style["overflow"] = "scroll";
            PanAttach.Style["Height"] = "";
            PanAttach.Style["width"] = "";
            PanAttach.Visible = false;
        }
        dlAdminUpload.Visible = false;
        Attch1 = objCommon.GetAttchDocumentData(VendorId, true, false);
        if (Attch1.Tables[0].Rows.Count > 0)
        {
            dlAdminUpload.Visible = true;
            PanAttach.Visible = true;
            tdattach.Style["Display"] = "";

            PanAttach.Visible = true;
            tblAdmin.Style["Display"] = "";
            dlAdminUpload.DataSource = Attch1;
            dlAdminUpload.DataBind();

            if (Attch1.Tables[0].Rows.Count > 3)
            {
                PanAttach.Style["overflow"] = "scroll";
                PanAttach.Style["Height"] = "200px";
                PanAttach.Style["width"] = "425px";
            }
        }
        else
        {
            tblAdmin.Style["Display"] = "None";
            tdattach.Style["Display"] = "none";
            dlAdminUpload.Visible = false;
            PanAttach.Style["overflow"] = "";
            PanAttach.Style["Height"] = "";
            PanAttach.Style["width"] = "";
            //PanAttach.Visible = false;

        }




        if (Attch.Tables[0].Rows.Count > 0)
        {
            tdattach.Style["Display"] = "";
        }





    }

    #region Server Side Events
    protected void imbAttachClose_Click(object sender, ImageClickEventArgs e)
    {
        this.ModalAddDoc.Hide();        
    }


    protected void imbAdddoc_Click(object sender, ImageClickEventArgs e)
    {
        lblattach.Visible = false;
        lblattach.Text = "";
        PaSREAttach.Visible = false;
        VendorID = Convert.ToInt32(Session["VendorID"]);
        //AssignPath();
        //RetrievePath();
        try
        {
            if (Page.IsValid)
            {
                lblattach.Visible = true;
                TxtFileName1.Focus();
                lblattach.Text = string.Empty;
                if ((TxtFileName1.Text == "") && (TxtFileName2.Text == "") && (TxtFileName3.Text == "") && (TxtFileName4.Text == "") && (TxtFileName5.Text == "") &&
                    !(Flu1.HasFile) && !(Flu2.HasFile) && !(Flu3.HasFile) && !(Flu4.HasFile) && !(Flu5.HasFile))
                {
                    lblattach.Text = "&nbsp;<li>Please upload at least one document </li>";
                    
                    ModalAddDoc.Show();

                }
                else
                {
                    //file1
                    if (TxtFileName1.Text != "")
                    {
                        if (!(Flu1.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document one</li>";
                        }
                    }
                    else if (Flu1.HasFile)
                    {
                        if (TxtFileName1.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description one</li>";
                        }
                    }

                    //file2
                    if (TxtFileName2.Text != "")
                    {
                        if (!(Flu2.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document two</li>";
                        }
                    }
                    else if (Flu2.HasFile)
                    {
                        if (TxtFileName2.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description two</li>";
                        }
                    }

                    //file3
                    if (TxtFileName3.Text != "")
                    {
                        if (!(Flu3.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document three</li>";
                        }
                    }
                    else if (Flu3.HasFile)
                    {
                        if (TxtFileName3.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description three</li>";
                        }
                    }

                    //file4
                    if (TxtFileName4.Text != "")
                    {
                        if (!(Flu4.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document four</li>";
                        }
                    }
                    else if (Flu4.HasFile)
                    {
                        if (TxtFileName4.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description four</li>";
                        }
                    }

                    //file5
                    if (TxtFileName5.Text != "")
                    {
                        if (!(Flu5.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document five</li>";
                        }
                    }
                    else if (Flu5.HasFile)
                    {
                        if (TxtFileName5.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description five</li>";
                        }
                    }
                    int Insert = 0;
                    if ((Flu1.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu1.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu1.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                //strFileName1 = Convert.ToString(_vendorID) + Path.GetFileName(Flu1.PostedFile.FileName);
                                strFileName1 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu1.FileName);


                                string filename = DOCUMENT_LOCATION + strFileName1;
                                Flu1.PostedFile.SaveAs(filename);
                                //string filepath = DOCUMENT_LOCATION + strFileName1;
                                string filepath = strFileName1;
                                int Count = objRegistration.CheckDocument(VendorID, TxtFileName1.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(VendorID, TxtFileName1.Text, filepath, Convert.ToBoolean(hdnInAddDoc.Value), Convert.ToBoolean(hdnSRAddDoc.Value));
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description1 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "&nbsp;<li>File should be less than 2 MB of size for description1</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "&nbsp;<li>Invalid file type for description1</li>";
                        }
                    }
                    if ((Flu2.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu2.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu2.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                //strFileName2 = _vendorID + Path.GetFileName(Flu2.PostedFile.FileName);
                                strFileName2 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu2.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName2;
                                Flu2.PostedFile.SaveAs(filename);
                                // string filepath = DOCUMENT_LOCATION + strFileName2;
                                string filepath = strFileName2;
                                int Count = objRegistration.CheckDocument(VendorID, TxtFileName2.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(VendorID, TxtFileName2.Text, filepath, Convert.ToBoolean(hdnInAddDoc.Value), Convert.ToBoolean(hdnSRAddDoc.Value));
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description2 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2 MB of size for description2</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description2</li>";
                        }
                    }
                    if ((Flu3.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu3.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu3.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                //strFileName3 = _vendorID + Path.GetFileName(Flu3.PostedFile.FileName);
                                strFileName3 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu3.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName3;
                                Flu3.PostedFile.SaveAs(filename);
                                //string filepath = DOCUMENT_LOCATION + strFileName3;
                                string filepath = strFileName3;
                                int Count = objRegistration.CheckDocument(Convert.ToInt64(Session["VendorId"]), TxtFileName3.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(VendorID, TxtFileName3.Text, filepath, Convert.ToBoolean(hdnInAddDoc.Value), Convert.ToBoolean(hdnSRAddDoc.Value));
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description3 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2 MB of size for description3</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type  for description3</li>";
                        }
                    }
                    if ((Flu4.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu4.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu4.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                //strFileName4 = _vendorID + Path.GetFileName(Flu4.PostedFile.FileName);
                                strFileName4 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu4.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName4;
                                Flu4.PostedFile.SaveAs(filename);
                                //string filepath = DOCUMENT_LOCATION + strFileName4;
                                string filepath = strFileName4;

                                int Count = objRegistration.CheckDocument(VendorID, TxtFileName4.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(VendorID, TxtFileName4.Text, filepath, Convert.ToBoolean(hdnInAddDoc.Value), Convert.ToBoolean(hdnSRAddDoc.Value));
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description4 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2 MB of size for description4</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description4</li>";
                        }
                    }
                    if ((Flu5.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu5.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu5.PostedFile.ContentLength - (2 * 1024 * 1024) <= 0)
                            {
                                //strFileName5 = _vendorID+ Path.GetFileName(Flu5.PostedFile.FileName);
                                strFileName5 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu5.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName5;
                                Flu5.PostedFile.SaveAs(filename);
                                // string filepath = DOCUMENT_LOCATION + strFileName5;
                                string filepath = strFileName5;
                                int Count = objRegistration.CheckDocument(VendorID, TxtFileName5.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(VendorID, TxtFileName5.Text, filepath, Convert.ToBoolean(hdnInAddDoc.Value), Convert.ToBoolean(hdnSRAddDoc.Value));
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "</li>Description5 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than 2 MB of size for description5</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description5</li>";
                        }
                    }
                    if (lblattach.Text != "")
                    {
                       
                        ModalAddDoc.Show();
                    }
                    else
                    {
                        if (Insert == 1)
                        {
                            TxtFileName1.Text = "";
                            TxtFileName2.Text = "";
                            TxtFileName3.Text = "";
                            TxtFileName4.Text = "";
                            TxtFileName5.Text = "";
                            LoadAttchDocumentData(VendorID);
                            if (hdnSRAddDoc.Value == "true")
                            {
                                SRLoadAttchDocumentData(VendorID);
                            }
                            if (Attch1.Tables[0].Rows.Count > 0)
                            {
                                PanAttach.Visible = true;
                                tdattach.Style["Display"] = "";
                                Attch1.Dispose();

                            }

                            if (hdnSRAddDoc.Value == "true")
                            {
                                tdSRattach.Style["Display"] = "";

                                tblVendors.Style["Display"] = "none";
                                tblAdmin.Style["Display"] = "none";
                                tdattach.Style["Display"] = "none";
                                PaSREAttach.Visible = true;

                            }

                            lblattach.Text = "&nbsp;<li>Document uploaded successfully</li>";
                            
                            ModalAddDoc.Show();
                        }
                        else if (Insert == 0)
                        {

                            lblattach.Text = "&nbsp;<li>Error Occured in uploading the document </li>";
                            
                            ModalAddDoc.Show();
                        }
                    }
                }
            }
            else
            {
                
                ModalAddDoc.Show();
            }
        }
        catch (System.UnauthorizedAccessException ex)
        {
            lblattach.Text = "<li>Access denied to the document attached</li>";
            
            ModalAddDoc.Show();
        }
        finally
        {
            TxtFileName1.Focus();
        }
    }
    protected void DataListAttchView_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        Int64 id = (Int64)DataListAttchView.DataKeys[e.Item.ItemIndex];
        objCommon.DeleteAttchDoc(id);

        LoadAttchDocumentData(VendorID);
        lblattach.Text = "&nbsp;<li>File deleted successfully</li>";
                            ;
        ModalAddDoc.Show();

    }
    protected void DataListSRAttchView_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        Int64 id = (Int64)DataListSRAttchView.DataKeys[e.Item.ItemIndex];
        objCommon.DeleteAttchDoc(id);

        SRLoadAttchDocumentData(VendorID);
        lblattach.Text = "&nbsp;<li>File deleted successfully</li>";
        trAttachHeader.Style["Display"] = "";
        
        ModalAddDoc.Show();

    }
    protected void DataListSRAttchView_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        ImageButton imbdel = (ImageButton)e.Item.FindControl("Delete");

        if (imbdel != null)
        {

            if (Session["Access"].ToString() != "1")
            {
                imbdel.Visible = false;
            }
            else
            {
                imbdel.Visible = true;
            }
        }

    }
    protected void dlAdminUpload_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        ImageButton imbdel = (ImageButton)e.Item.FindControl("Delete");

        if (imbdel != null)
        {

            if (Session["Access"].ToString() != "1")
            {
                imbdel.Visible = false;
            }
            else
            {
                imbdel.Visible = true;
            }
        }
    }
    protected void DataListAttchView_ItemDataBound1(object sender, DataListItemEventArgs e)
    {
        ImageButton imbdel = (ImageButton)e.Item.FindControl("Delete");

        if (imbdel != null)
        {

            if (Session["Access"].ToString() != "1")
            {
                imbdel.Visible = false;
            }
            else
            {
                imbdel.Visible = true;
            }
        }
    }
    protected void dlAdminUpload_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        Int64 id = (Int64)dlAdminUpload.DataKeys[e.Item.ItemIndex];
        objCommon.DeleteAttchDoc(id);

        LoadAttchDocumentData(VendorID);
        lblattach.Text = "&nbsp;<li>File deleted successfully</li>";
        
        ModalAddDoc.Show();

    }
    protected void DataListSRAttchView_SelectedIndexChanged(object sender, EventArgs e)
    {
        string FileName = ((HiddenField)DataListSRAttchView.SelectedItem.FindControl("hdnFileName")).Value;
        Response.Redirect("../Common/GenerateFile.aspx?region=" + DOCUMENT_LOCATION + FileName);
        
        ModalAddDoc.Show();
    }
    protected void DataListAttchView_SelectedIndexChanged(object sender, EventArgs e)
    {
        string FileName = ((HiddenField)DataListAttchView.SelectedItem.FindControl("hdnFileName")).Value;
        Response.Redirect("../Common/GenerateFile.aspx?region=" + DOCUMENT_LOCATION + FileName);
        
        ModalAddDoc.Show();
    }
    protected void dlAdminUpload_SelectedIndexChanged(object sender, EventArgs e)
    {
        string FileName = ((HiddenField)dlAdminUpload.SelectedItem.FindControl("hdnFileName")).Value;
        Response.Redirect("../Common/GenerateFile.aspx?region=" + DOCUMENT_LOCATION + FileName);
        
        ModalAddDoc.Show();
    }
    protected void DataListAttchView_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (((HiddenField)e.Item.FindControl("hdnField")).Value == "True")
        {

            ((HtmlTableCell)e.Item.FindControl("tdRow")).Attributes.Add("Class", "greentextleft");
            ((LinkButton)e.Item.FindControl("FileName")).CssClass = "greentextleft";
        }
        else
        {
            ((LinkButton)e.Item.FindControl("FileName")).CssClass = "bodytextleft";
            ((HtmlTableCell)e.Item.FindControl("tdRow")).Attributes.Add("Class", "bodytextleft");
        }
    }

    #endregion
}