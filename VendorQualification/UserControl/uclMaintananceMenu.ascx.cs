﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class UserControl_uclMaintananceMenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void imbVendorMain_Click(object sender, ImageClickEventArgs e)
    {
       
    }
    protected void imbVendorMatch_Click(object sender, ImageClickEventArgs e)
    {
     
    }
    protected void imbSavedSearch_Click(object sender, ImageClickEventArgs e)
    {
     
    }
    protected void imbUserMaintanance_Click(object sender, ImageClickEventArgs e)
    {
      
    }
    protected void imbPasswordReset_Click(object sender, ImageClickEventArgs e)
    {
       
    }

    //Change the style of the button which is clicked
    public void DisplayButton(int Page)
    {
        switch (Page)
        { 
            case 1:
                {
                imbVendorMain.ImageUrl = "~/Images/vendor_maina.gif";
                imbVendorMatch.ImageUrl = "~/Images/venmatch.gif";
                imbSavedSearch.ImageUrl = "~/Images/saved.jpg";
                imbUserMaintanance.ImageUrl = "~/Images/usermaint.gif";
                imbVendorProfile.ImageUrl = "~/Images/vpro.gif";
                imgVQF.ImageUrl = "~/Images/vqf.gif";
                break;
                }
            case 2:
                {
                imbVendorMain.ImageUrl = "~/Images/vendor_main.gif";
                imbVendorMatch.ImageUrl = "~/Images/venmatcha.gif";
                imbSavedSearch.ImageUrl = "~/Images/saved.jpg";
                imbUserMaintanance.ImageUrl = "~/Images/usermaint.gif";
                imbVendorProfile.ImageUrl = "~/Images/vpro.gif";
                imgVQF.ImageUrl = "~/Images/vqf.gif";
                break;
                }
            case 3:
                {
                    imbVendorMain.ImageUrl = "~/Images/vendor_main.gif";
                    imbVendorMatch.ImageUrl = "~/Images/venmatch.gif";
                    imbSavedSearch.ImageUrl = "~/Images/saveda.jpg";
                    imbUserMaintanance.ImageUrl = "~/Images/usermaint.gif";
                    imbVendorProfile.ImageUrl = "~/Images/vpro.gif";
                    imgVQF.ImageUrl = "~/Images/vqf.gif";
                    break;
                }
            case 4:
                {
                    imbVendorMain.ImageUrl = "~/Images/vendor_main.gif";
                    imbVendorMatch.ImageUrl = "~/Images/venmatch.gif";
                    imbSavedSearch.ImageUrl = "~/Images/saved.jpg";
                    imbUserMaintanance.ImageUrl = "~/Images/usermainta.gif";
                    imbVendorProfile.ImageUrl = "~/Images/vpro.gif";
                    imgVQF.ImageUrl = "~/Images/vqf.gif";
                    break;
                }
            case 5:
                {
                    imbVendorMain.ImageUrl = "~/Images/vendor_main.gif";
                    imbVendorMatch.ImageUrl = "~/Images/venmatch.gif";
                    imbSavedSearch.ImageUrl = "~/Images/saved.jpg";
                    imbUserMaintanance.ImageUrl = "~/Images/usermaint.gif";
                    imbVendorProfile.ImageUrl = "~/Images/vproa.gif";
                    imgVQF.ImageUrl = "~/Images/vqf.gif";
                    break;
                }
            case 6:
                {
                    imbVendorMain.ImageUrl = "~/Images/vendor_main.gif";
                    imbVendorMatch.ImageUrl = "~/Images/venmatch.gif";
                    imbSavedSearch.ImageUrl = "~/Images/saved.jpg";
                    imbUserMaintanance.ImageUrl = "~/Images/usermaint.gif";
                    imbVendorProfile.ImageUrl = "~/Images/vpro.gif";
                    imgVQF.ImageUrl = "~/Images/vqfa.gif";
                    break;
                }

        }

            
    }
}
