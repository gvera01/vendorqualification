﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UclAdminCompanyMenu.ascx.cs"
    Inherits="UserControl_UclAdminCompanyMenu" %>
<link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
<style media="print" type="text/css">
    .hide_print
    {
        display: none;
    }
</style>

<script language="javascript" type="text/javascript">
    function DisplayAlert(text, commenttd, hdn) {
        if (text == "Red") {
            document.getElementById(commenttd).style.display = "block";
            document.getElementById(hdn).value = "Red";
        }
        else if (text == "Yellow") {
            document.getElementById(commenttd).style.display = "block";
            document.getElementById(hdn).value = "Yellow";
        }
        else if (text == "Warning") {
            document.getElementById(commenttd).style.display = "none";
            document.getElementById(hdn).value = "Warning";
        }
        
    }

    //G.Vera 03/28/2017
    function ToggleFinComments(checkbox) {
        if(!checkbox.checked) {
            document.getElementById("<%= txtFinComment.ClientID %>").value = "";
        }

        var id = "#" + "<%= tdFinComment.ClientID %>";
        $(id).toggle();

    }

</script>

<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="even_pad">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="65%" rowspan="4" style="padding: 5px">
                        <img src="../images/haskelllogo.jpg" />
                    </td>
                    <td width="35%">
                        <%if (Request.QueryString["user"] == "employee")
                          { %>
                        <span class="hide_print">
                            <%} %>
                            <table cellpadding="0" cellspacing="0" border="0" align="right">
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tdadmin" runat="server" align="right" style="display: none">
                                        &nbsp;
                                        <asp:LinkButton ID="lnkAddNotes" runat="server" Text="Click to add notes" OnClick="lnkAddNotes_Click"></asp:LinkButton>&nbsp;&nbsp;
                                    </td>
                                    <%--G. Vera 03/28/2017 - START--%>
                                    <td id="tdFinComment" runat="server" style="display: none" align="right">
                                        <asp:TextBox ID="txtFinComment" runat="server" TextMode="MultiLine" Height="40px" Width="300px"
                                            onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                            onKeyUp="javascript:textCounter(this,1000,'yes');"></asp:TextBox>&nbsp;&nbsp;
                                    </td>
                                    <%--G. Vera 03/28/2017 - END--%>
                                    <td id="tdcomment" runat="server" style="display: none" align="right">
                                        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Height="40px" Width="300px"
                                            onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                            onKeyUp="javascript:textCounter(this,1000,'yes');"></asp:TextBox>&nbsp;&nbsp;
                                    </td>
                                    
                                </tr>
                            </table>
                            <%if (Request.QueryString["user"] == "employee")
                              { %></span>
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;
                    </td>
                </tr>
                <%if (Request.QueryString["user"] == "employee")
                  { %>
                <span class="hide_print">
                    <%}
                    %>
                    <tr>
                        <td id="tdsave" runat="server" align="right" style="display: none; vertical-align: top;
                            height: 45px;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <%--G. Vera 03/28/2017 - START--%>
                                    <td>
                                        <asp:CheckBox ID="chkFinancials" runat="server" onclick="Javascript:ToggleFinComments(this);"/>
                                    </td>
                                    <td>
                                        <span class="dollars">$</span>
                                    </td>
                                    <%--G. Vera 03/28/2017 - END--%>
                                    <td>
                                        <asp:CheckBox ID="chkVPP" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/V.gif" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="radred" runat="server" GroupName="Alert" />
                                    </td>
                                    <td>
                                        <asp:Image ID="imgred" runat="server" ImageUrl="~/Images/red.png" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="radyellow" runat="server" GroupName="Alert" />
                                    </td>
                                    <td>
                                        <asp:Image ID="imgyellow" runat="server" ImageUrl="~/Images/yellow.png" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="radwarning" runat="server" GroupName="Alert" />
                                    </td>
                                    <td>
                                        <asp:Image ID="imgwar" runat="server" ImageUrl="~/Images/nowarning.gif" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imbSave" CssClass="hide_print" runat="server" ImageUrl="~/Images/submit.jpg"
                                            OnClick="imbSave_Click" />
                                        <asp:HiddenField ID="hdnAlert" runat="server" />                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%if (Request.QueryString["user"] == "employee")
                      { %>
                </span>
                <%}
                %>
                <tr>
                    <td class="bodytextbold_right">
                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                            PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" CancelControlID="imbOk">
                        </Ajax:ModalPopupExtender>
                        <asp:HiddenField ID="lblHidden" runat="server" />
                        <%if (Session["EmployeeName"] != null) { Response.Write("Welcome" + " " + Session["EmployeeName"].ToString()); }%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="modalPopupDiv" class="popupdivsmall" style="display: none">
                            <table cellpadding="0" cellspacing="0" border="0" width="425">
                                <tr>
                                    <td class="searchhdrbarbold" runat="server" id="msgTd" colspan="3">
                                        <asp:Label ID="lblheading" runat="server" CssClass="searchhdrbarbold"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popupdivcl">
                                        &nbsp;
                                        <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popupdivcl">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="popupdivcl" align="center">
                                        <input type="button" value="OK" id="imbOk" title="Ok" class="ModalPopupButton" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="popupdivcl">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="bottommenutd" colspan="2">
            &nbsp;
        </td>
    </tr>
</table>
