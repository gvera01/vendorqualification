﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uclAttachmentDialog.ascx.cs" Inherits="UserControl_uclAttachmentDialog" %>

<%--G. Vera 06/18/2012 - Created usercontrol for re-usability--%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>
        <div id="divAddDoc" class="popupdivbg1"  runat="server" style="display:none;">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                        <tr>
                            <td class="Messageheading" runat="server" id="trAttachHeader">
                                Attach Document
                            </td>
                        </tr>
                        <tr id="trAttachDocument" runat="server">
                            <td style="padding: 0px 5px">
                                <table width="100%">
                                    <tr>
                                        <td class="tdheight">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl" align="left" colspan="2">
                                            <asp:Label ID="lblattach" runat="server" CssClass="errormsg"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt" colspan="2" style="padding: 10px 7px; border: 1px solid #000">
                                            <b>NOTE:</b><br />
                                            <br />
                                            Each file should be less than or equal to 2 MB of size.<br />
                                            Please upload files only in the format gif, jpg, jpeg, xls, xlsx, txt, pdf, doc,
                                            docx
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bodytextcenter">
                                            <b>Description</b>
                                        </td>
                                        <td class="formtdrt">
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>Upload Path</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt">
                                            <asp:TextBox ID="TxtFileName1" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                        </td>
                                        <td class="formtdrt">
                                            <asp:FileUpload ID="Flu1" runat="server" CssClass="fileUpload" TabIndex="0" />
                                            <asp:HiddenField ID="hdnFile1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt">
                                            <asp:TextBox ID="TxtFileName2" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                        </td>
                                        <td class="formtdrt">
                                            <asp:FileUpload ID="Flu2" runat="server" CssClass="fileUpload" TabIndex="0" />
                                            <asp:HiddenField ID="hdnFile2" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt">
                                            <asp:TextBox ID="TxtFileName3" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                        </td>
                                        <td class="formtdrt">
                                            <asp:FileUpload ID="Flu3" runat="server" CssClass="fileUpload" TabIndex="0" />
                                            <asp:HiddenField ID="hdnFile3" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt">
                                            <asp:TextBox ID="TxtFileName4" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                        </td>
                                        <td class="formtdrt">
                                            <asp:FileUpload ID="Flu4" runat="server" CssClass="fileUpload" TabIndex="0" />
                                            <asp:HiddenField ID="hdnFile4" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formtdrt">
                                            <asp:TextBox ID="TxtFileName5" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                        </td>
                                        <td class="formtdrt">
                                            <asp:FileUpload ID="Flu5" runat="server" CssClass="fileUpload" TabIndex="0" />
                                            <asp:HiddenField ID="hdnFile5" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ModalPopupTd" colspan="2">
                                            <asp:HiddenField runat="server" ID="hdnpath" />
                                            <asp:ImageButton ID="imbAddDoc" runat="server" ImageUrl="~/Images/upload.jpg" ToolTip="Upload"
                                                TabIndex="0" OnClick="imbAdddoc_Click" CausesValidation="true" />
                                            &nbsp;
                                            <asp:ImageButton ID="imbAttachClose" runat="server" ImageUrl="~/Images/close.jpg"
                                                ToolTip="Close" TabIndex="0" onclick="imbAttachClose_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                                                            
                        <tr>
                            <td>
                                <asp:Panel ID="PanAttach" runat="server" CssClass="popupdivbg1" >
                                <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                    <tr>
                                        <td class="Messageheading" runat="server" id="tdattach">
                                            Attached Document files
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px">
                                            <table cellpadding="5" cellspacing="0" width="100%" runat="server" id="tblVendors">
                                                <tr>
                                                    <td class="formhead2">
                                                        Posted By Vendor
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:DataList ID="DataListAttchView" runat="server" DataKeyField="Pk_DocumentID"
                                                            OnSelectedIndexChanged="DataListAttchView_SelectedIndexChanged" 
                                                            OnDeleteCommand="DataListAttchView_DeleteCommand" 
                                                            onitemdatabound="DataListAttchView_ItemDataBound1" >
                                                            <ItemTemplate>
                                                                <tr height="25px">
                                                                    <td class="bodytextleft" style="padding-right: 0px; padding-left: 5px; background-color: #efefef;"
                                                                        id="tdRow" runat="server">
                                                                        <%# Eval("Row") %>.
                                                                    </td>
                                                                    <td class="bodytextleft" width="360px" style="background-color: #efefef;">
                                                                        <asp:LinkButton CssClass="bodytextleft" ID="FileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                            CommandName="select"></asp:LinkButton>
                                                                        <asp:HiddenField ID="hdnFileName" runat="server" Value='<%# Eval("Filepath") %>' />
                                                                    </td>
                                                                    <td align="right" style="background-color: #efefef; padding-right: 5px">
                                                                        <%--<asp:Button ID="Delete" Runat="server" Text="Delete" ToolTip="Delete" CommandName="delete" />--%>
                                                                        <asp:ImageButton ID="Delete" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                                            CommandName="delete" OnClientClick="javascript:return getConfirmation()"/>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px">
                                            <table cellpadding="5" cellspacing="0" width="100%" runat="server" id="tblAdmin">
                                                <tr>
                                                    <td class="formhead2">
                                                        Posted By Admin
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:DataList ID="dlAdminUpload" OnSelectedIndexChanged="dlAdminUpload_SelectedIndexChanged"
                                                            runat="server" DataKeyField="Pk_DocumentID" 
                                                            OnDeleteCommand="dlAdminUpload_DeleteCommand" 
                                                            onitemdatabound="dlAdminUpload_ItemDataBound">
                                                            <ItemTemplate>
                                                                <tr height="25px">
                                                                    <td class="bodytextleft" style="padding-right: 0px; padding-left: 5px; background-color: #efefef;"
                                                                        id="tdRow" runat="server">
                                                                        <%# Eval("Row") %>.
                                                                    </td>
                                                                    <td class="bodytextleft" style="background-color: #efefef;" width="360px">
                                                                        <asp:LinkButton ID="FileName" CssClass="bodytextleft" runat="server" Text='<%# Eval("FileName") %>'
                                                                            CommandName="select"></asp:LinkButton>
                                                                        <asp:HiddenField ID="hdnFileName" runat="server" Value='<%# Eval("Filepath") %>' />
                                                                    </td>
                                                                    <td align="right" style="background-color: #efefef; padding-right: 5px">
                                                                        <%--<asp:Button ID="Delete" Runat="server" Text="Delete" ToolTip="Delete" CommandName="delete" />--%>
                                                                        <asp:ImageButton ID="Delete" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                                            CommandName="delete" OnClientClick="javascript:return getConfirmation()"/>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                </asp:Panel> 
                            </td>
                        </tr>

                        <tr>
                            <td id="tdSRattach" runat="server">
                                <asp:Panel ID="PaSREAttach" runat="server" CssClass="popupdivbg1" >
                                <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                                                       
                                    <tr>
                                        <td style="padding: 0px" >
                                            <table cellpadding="5" cellspacing="0" width="100%" runat="server" id="Table1">
                                                <tr>
                                        <td class="Messageheading" runat="server" id="td3">
                                            Attached Document files
                                        </td>
                                    </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                                                
                                                    </td>
                                                </tr>
                                                                                
                                                <tr>
                                                    <td class="formhead2">
                                                        Summary Report
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:DataList ID="DataListSRAttchView" runat="server" DataKeyField="Pk_DocumentID"
                                                            OnSelectedIndexChanged="DataListSRAttchView_SelectedIndexChanged" 
                                                            OnDeleteCommand="DataListSRAttchView_DeleteCommand" 
                                                            onitemdatabound="DataListSRAttchView_ItemDataBound">
                                                            <%--  <HeaderTemplate>    <table>    <tr><td class="bodytextbold" width="50px" nowrap=nowrap>Serial No</td> <td class="bodytextbold" colspan="2" nowrap=nowrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File Name</td>    
                                                 
                                                  
                </tr></table></HeaderTemplate>
                --%>
                                                            <ItemTemplate>
                                                                <tr height="25px">
                                                                    <td class="bodytextleft" style="padding-right: 0px; padding-left: 5px; background-color: #efefef;"
                                                                        id="tdRow" runat="server">
                                                                        <%# Eval("Row") %>.
                                                                    </td>
                                                                    <td class="bodytextleft" width="360px" style="background-color: #efefef;">
                                                                        <asp:LinkButton CssClass="bodytextleft" ID="FileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                            CommandName="select"></asp:LinkButton>
                                                                        <asp:HiddenField ID="hdnFileName" runat="server" Value='<%# Eval("Filepath") %>' />
                                                                    </td>
                                                                    <td align="right" style="background-color: #efefef; padding-right: 5px">
                                                                        <%--<asp:Button ID="Delete" Runat="server" Text="Delete" ToolTip="Delete" CommandName="delete" />--%>
                                                                        <asp:ImageButton ID="Delete" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                                            CommandName="delete" OnClientClick="javascript:return getConfirmation()"/>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </td>
                                                </tr>
                                                <tr>
                                        <td class="tdheight">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl" align="left">
                                            <asp:Label ID="lblSRmessage" runat="server" CssClass="errormsg"></asp:Label>
                                        </td>
                                    </tr>
                                                <tr><td align=center >       
                                                <asp:ImageButton ID="imgClose" runat="server" ImageUrl="~/Images/close.jpg"
                                                ToolTip="Close" TabIndex="0" /></td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdheight">
                                        </td>
                                    </tr>
                                                               
                                </table></asp:Panel> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </td>
</tr>
<tr>
    <td>
        <Ajax:ModalPopupExtender ID="ModalAddDoc" runat="server" TargetControlID="hdnAddDoc"
                                            PopupControlID="divAddDoc" BackgroundCssClass="popupbg">
        </Ajax:ModalPopupExtender>
        <asp:HiddenField ID="hdnAddDoc" runat="server" />
        <asp:HiddenField ID="hdnSRAddDoc" runat="server" />
        <asp:HiddenField ID="hdnInAddDoc" runat="server" />
    </td>
</tr>
</table>

<script language="javascript" type="text/javascript">
    function getConfirmation() {
        if (confirm("Are you sure you want to delete the selected document?"))
            return true;
        return false;
    }
    
    function CheckDocument() {
        var msg = "";
        //Document 1
        if (trimAll(document.form1.Flu1.value) != "") {
            var ext = document.form1.Flu1.value;
            var len = document.form1.Flu1.value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();

                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = "";
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file one</li> ";
                }
            }
        }
        //Document 2
        if (trimAll(document.form1.Flu2.value) != "") {
            var ext = document.form1.Flu2.value;
            var len = document.form1.Flu2.value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();

                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file two </li> ";
                }
            }
        }
        //Document 3
        if (trimAll(document.form1.Flu3.value) != "") {
            var ext = document.form1.Flu3.value;
            var len = document.form1.Flu3.value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();

                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file three</li> ";
                }
            }
        }
        //Document 4
        if (trimAll(document.form1.Flu4.value) != "") {
            var ext = document.form1.Flu4.value;
            var len = document.form1.Flu4.value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();

                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file four</li> ";
                }
            }
        }
        //Document 5
        if (trimAll(document.form1.Flu5.value) != "") {
            var ext = document.form1.Flu5.value;
            var len = document.form1.Flu5.value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();

                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file five</li>";
                }
            }
        }
        if (msg == "") return true;
        else {
            var txt = document.getElementById('<%=lblattach.ClientID%>');
            txt.innerHTML = msg;
            return false;
        }
    }       
</script>


