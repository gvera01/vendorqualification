﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UclAdminMenu.ascx.cs"
    Inherits="UserControl_UclAdminMenu" %>
<link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="adminltmenutd">
            <a href="../Admin/SearchVendor.aspx" class="adminltmenutxt">Search Vendors</a>
        </td>
    </tr>
    <tr>
        <%--G. Vera 10/02/2015: Added link to new search vendor application--%>
        <td class="adminltmenutd">
            <a href='<%=ConfigurationManager.AppSettings["NewVendorSearchURL"] %>' class="adminltmenutxt" target="_blank">Advanced Search</a>
        </td>
    </tr>
    <tr id="trEmployeeSRE" runat="server" style="display: none;"  >
        <td class="adminltmenutd" nowrap=nowrap>
            <a href="../SRE/SREStart.aspx" class="adminltmenutxt">Subcontractor Risk Evaluation</a>
        </td>
    </tr>
    <tr id="trAdminSRE" runat="server" style="display: none;">
        <td class="adminltmenutd" nowrap=nowrap>
            <a href="../SRE/ViewSRE.aspx" class="adminltmenutxt">Subcontractor Risk Evaluation</a>
        </td>
    </tr>
    <tr>
        <td class="adminltmenutd">
            <a href="../Admin/SubContractorRating.aspx" class="adminltmenutxt">Vendor Rating</a>   <%-- G. Vera 08/04/2014: Changed to "Vendor"--%>
        </td>
    </tr>
    <tr>
        <td class="adminltmenutd" id="tdMaintenance" runat="server">
            <a href="../Admin/VendorMatch.aspx" class="adminltmenutxt">Maintenance</a>
        </td>
    </tr>
    <tr>
        <td class="adminltmenutd">
            <a href="../Admin/Login.aspx" class="adminltmenutxt">Logout </a>
        </td>
    </tr>
    <%--<tr><td class="adminltmenutd"><a href="http://intranet.haskell.com/" class="adminltmenutxt"> Logout </a></td></tr>--%>
</table>
