﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uclMaintananceMenu.ascx.cs" Inherits="UserControl_uclMaintananceMenu" %>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
   
    <td align="right" style="background:url(../Images/tabbg.gif) repeat-x">
         <asp:ImageButton ID="imbVendorMatch" runat="server" 
            ImageUrl="~/Images/venmatch.gif" 
            PostBackUrl="~/Admin/VendorMatch.aspx" onclick="imbVendorMatch_Click"/>
             <asp:ImageButton ID="imbSavedSearch" runat="server" 
            ImageUrl="~/Images/saved.jpg" 
            PostBackUrl="~/Admin/SavedSearches.aspx" onclick="imbSavedSearch_Click"/>
            <asp:ImageButton ID="imbVendorMain" runat="server" 
            ImageUrl="~/Images/vendor_main.gif" 
            PostBackUrl="~/Admin/VendorMaintenance.aspx" 
            onclick="imbVendorMain_Click"/>
            <asp:ImageButton ID="imgVQF" runat="server" 
            ImageUrl="~/Images/vqf.gif" 
            PostBackUrl="~/Admin/AddScopes.aspx"/>
        <asp:ImageButton ID="imbUserMaintanance" runat="server" 
            ImageUrl="~/Images/usermaint.gif" 
            PostBackUrl="~/Admin/ListEmployee.aspx" 
            onclick="imbUserMaintanance_Click"/>
             
        <asp:ImageButton ID="imbVendorProfile" runat="server" 
            ImageUrl="~/Images/vpro.gif" 
            PostBackUrl="~/Admin/ResetPassword.aspx" onclick="imbPasswordReset_Click"/></td>
  </tr>
  
</table>

