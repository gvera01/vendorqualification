﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UclFooter.ascx.cs" Inherits="User_Control_UclFooter" %>
<link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="copyrights">
            &copy;
            <%=System.DateTime.Now.Year%>
            Haskell. All rights reserved.
        <!-- ***** Added by N Schoenberger on 1/4/2012 ***** -->
        <!-- ***** This should only been implemented on the EXTERNAL SITE - WEBEHDQ01P ***** -->
        <!--&nbsp;(<a href="../Common/TermsConditions.aspx">Terms and Conditions</a>)&nbsp;-->
        </td>
        <!-- ***** End added -- 1/4/2012 ***** -->
    </tr>
</table>
