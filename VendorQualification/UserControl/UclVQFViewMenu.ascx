﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UclVQFViewMenu.ascx.cs"
    Inherits="UserControl_UclVQFViewMenu" %>
<table border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td style="color: black; padding-left: 40px; font-family: Tahoma; font-size: 12pt;
            font-style: italic;" valign="top" width="75%" colspan="8" align="center">
            *** Confidential: For Internal Use Only ***
        </td>
        <td style="padding-right: 50px;" valign="middle" width="25%" colspan="2" align="right">
            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click"
                Font-Names="Verdana" Font-Size="xx-Small">Edit VQF</asp:LinkButton>
            <br />
            <br />
        </td>
    </tr>
   
    <tr>
        <td width="37" valign="top">
            <img src="../Images/tab_lt.gif" width="37" height="26" />
        </td>
        <td>
        <asp:ImageButton ID="imbCompany" runat="server" ImageUrl="../Images/company.gif" 
                onclick="imbCompany_Click"  />
           <%-- <a href="../VQFView/CompanyView.aspx?user=<%=Request.QueryString["user"]%>">
                <img src="../Images/company.gif" height="26" border="0" /></a>--%>
        </td>
        <td>
         <asp:ImageButton ID="imblegal" runat="server" ImageUrl="../Images/legnfin.gif" 
                onclick="imblegal_Click" Height="26px" 
              />
         <%--   <a href="../VQFView/LegalFinancialView.aspx?user=<%=Request.QueryString["user"] %>">
                <img src="../Images/legnfin.gif" height="26" border="0" /></a>--%>
        </td>
        <td>
          <asp:ImageButton ID="imbTrade" runat="server" ImageUrl="../Images/tradeinfo.gif" onclick="imbTrade_Click" 
              />
      <%--      <a href="../VQFView/TradeView.aspx?user=<%=Request.QueryString["user"] %>">
                <img src="../Images/tradeinfo.gif" width="137" height="26" border="0" /></a>--%>
        </td>
        <td>
         <asp:ImageButton ID="imbSafety" runat="server" ImageUrl="../Images/safety.gif" onclick="imbSafety_Click" 
              />
          <%--  <a href="../VQFView/SafetyView.aspx?user=<%=Request.QueryString["user"] %>">
                <img src="../Images/safety.gif" height="26" border="0" /></a>--%>
        </td>
        <td>
         <asp:ImageButton ID="imbRef" runat="server" ImageUrl="../Images/ref.gif" onclick="imbRef_Click" 
              />
           <%-- <a href="../VQFView/ReferencesView.aspx?user=<%=Request.QueryString["user"] %>">
                <img src="../Images/ref.gif" width="86" height="26" border="0" /></a>--%>
        </td>
        <td>
         <asp:ImageButton ID="imbIns" runat="server" ImageUrl="../Images/insnbond.gif" onclick="imbIns_Click" 
              />
        <%--    <a href="../VQFView/InsuranceBondingView.aspx?user=<%=Request.QueryString["user"] %>">
                <img src="../Images/insnbond.gif" height="26" border="0" /></a>--%>
        </td>
        <td>
         <asp:ImageButton ID="imbFile" runat="server" ImageUrl="../Images/doc.gif" onclick="imbFile_Click" 
              />
            <%--<a href="../VQFView/Files.aspx?user=<%=Request.QueryString["user"] %>">
                <img src="../Images/doc.gif" height="26" border="0" /></a>--%>
        </td>
        <td valign="top">
            <asp:ImageButton ID="Imghome" runat="server" ImageUrl="../Images/return.gif" OnClick="Imghome_Click" />
        </td>
        <td width="37" valign="top">
            <img src="../Images/tab_rt.gif" width="37" height="26" />
        </td>
    </tr>
</table>
