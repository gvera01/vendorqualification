﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net.Mail" %>


<script RunAt="server">

    private static string ErrMessageStartsWith = "The file '";
    private static string ErrMessageEndsWith = "' does not exist.";

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup

    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown
    }

    void Application_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError().GetBaseException();
        string errLogPath = ConfigurationManager.AppSettings["ErrorLogPath"];

        StreamWriter sw = new StreamWriter(Server.MapPath(errLogPath) + DateTime.Now.ToString("yyyy-MM-dd") + ".log", true);

        sw.WriteLine("Date: " + Convert.ToString(System.DateTime.Now));
        sw.WriteLine();

        sw.WriteLine("Cause: ");
        sw.WriteLine(ex.Source);
        sw.WriteLine();

        sw.WriteLine("Message: ");
        sw.WriteLine(ex.Message);
        sw.WriteLine();

        sw.WriteLine("Stack Trace :");
        sw.WriteLine(ex.StackTrace);
        sw.WriteLine();

        sw.WriteLine("Method :");
        sw.WriteLine(ex.TargetSite);
        sw.WriteLine();

        sw.WriteLine("Client Details ");
        sw.WriteLine();

        sw.WriteLine("URL : " + HttpContext.Current.Request.Url.AbsoluteUri.ToString());
        sw.WriteLine("IP address : " + HttpContext.Current.Request.UserHostAddress);
        sw.WriteLine("Browser is : " + HttpContext.Current.Request.Browser.Browser);
        sw.WriteLine("========================================================================================================================================");

        sw.Flush();
        sw.Close();

        // Code that runs when an unhandled error occurs
        if (HttpContext.Current != null)
        {
            //GV - All below
            string exceptionMsg = Server.GetLastError().Message;

            if(exceptionMsg.StartsWith(ErrMessageStartsWith) && exceptionMsg.EndsWith(ErrMessageEndsWith))
            {
                //restrict IP address
                string res;
                try
                {
                    res = IPHandler.Deny(Request.UserHostAddress);
                }
                catch (Exception exc)
                {

                    exceptionMsg += "<br/>While adding Deny rule to IP address";
                }
            }
            else
            {
                MailMessage objmessage = new MailMessage();
                objmessage.Priority = MailPriority.Normal;
                objmessage.Subject = Server.GetLastError().Source;
                objmessage.IsBodyHtml = true;
                StringBuilder sb = new StringBuilder();
                sb.Append(exceptionMsg); //GV
                sb.Append("<br/>");
                sb.Append(Server.GetLastError().InnerException);
                sb.Append("<br/>");
                sb.Append("User's Browser : " + Request.Browser.Browser);
                sb.Append("<br/>");
                sb.Append("User's IP : " + Request.UserHostAddress);
                sb.Append("<br/>");
                sb.Append("The url where the error occured is " + Request.Url.AbsoluteUri);
                objmessage.Body = sb.ToString();
                objmessage.From = new MailAddress(ConfigurationManager.AppSettings["adminemail"]);//replace this in web.config
                objmessage.To.Add(ConfigurationManager.AppSettings["Erroremail"]);

                SmtpClient objsmtpClient = new SmtpClient();
                objsmtpClient.Host = ConfigurationManager.AppSettings["MailServer"];

                //GV:  Crowe Horwath LLP's IP address will be skipped since they perform pentration attacks
                //and this will generate thousands of emails
                if(!HttpContext.Current.Request.UserHostAddress.Trim().StartsWith("159.246.29"))
                {
                    objsmtpClient.Send(objmessage);
                }
            }

        }
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started
        Session["RISKIDLOGOUT"] = null;
        Session["EMPLOYEEIDLOGOUT"] = null;
        Session["LOCKEDBYLOGOUT"] = null;
    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

</script>

