﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InsurancesBonding.aspx.cs"
    Inherits="VQF_InsurancesBonding" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <style type="text/css">
        body
        {
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbSaveNext" defaultfocus="txtBrokerorCompanyName">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:VQFEditSideMenu ID="VQFEditSideMenu" runat="server" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="bodytextbold_right even_pad">
                                                    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click">Return to 
          Search Result</asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextbold_right">
                                                    Welcome
                                                    <asp:Label ID="lblwelcome" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    Vendor <span style="letter-spacing: 1px">Qualification</span> Form - Insurance &
                                                    Bonding
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <Ajax:Accordion ID="accBonding" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
                                                        HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                                                        FadeTransitions="true" FramesPerSecond="40" TransitionDuration="250" AutoSize="none"
                                                        RequireOpenedPane="true" SuppressHeaderPostbacks="true">
                                                        <Panes>
                                                            <Ajax:AccordionPane ID="apnContact" runat="server">
                                                                <Header>
                                                                    Contact Information
                                                                </Header>
                                                                <Content>
                                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass ">
                                                                        <tr>
                                                                            <td class="formtd" width="45%">
                                                                                <span class="mandatorystar">*</span>Broker / Company Name
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtBrokerorCompanyName" runat="server" CssClass="txtbox" TabIndex="0"
                                                                                    MaxLength="100"></asp:TextBox>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvBrokerorCompanyName" ValidationGroup="ContactInformation"
                                                                                    runat="server" SetFocusOnError="true" EnableClientScript="true" Display="None"
                                                                                    ControlToValidate="txtBrokerorCompanyName" ErrorMessage="Please enter broker / company name under contact information "></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Insurance Agent
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtInsuranceAgent" runat="server" CssClass="txtbox" TabIndex="0"
                                                                                    MaxLength="100"></asp:TextBox>
                                                                                <%-- <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtInsuranceAgent" TargetControlID="txtInsuranceAgent"
                                                                                                                            FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                            InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>--%>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvInsuranceAgent" runat="server" ValidationGroup="ContactInformation"
                                                                                    SetFocusOnError="true" EnableClientScript="true" Display="None" ControlToValidate="txtInsuranceAgent"
                                                                                    ErrorMessage="Please enter insurance agent under contact information"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Agent Phone Number&nbsp;<asp:CheckBox ID="chkAgentUSA" runat="server" Text="Non-US" />
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtAgentTelephoneNo1" runat="server" TabIndex="0" ValidationGroup="ContactInformation"
                                                                                    CssClass="txtboxsmall" MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtAgentTelephoneNo1" runat="server" TargetControlID="txtAgentTelephoneNo1"
                                                                                    FilterType="Numbers">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <asp:TextBox ID="txtAgentTelephoneNo2" runat="server" TabIndex="0" ValidationGroup="ContactInformation"
                                                                                    CssClass="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                  <span id="spnPPhone2" runat="server">-</span>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtAgentTelephoneNo2" runat="server" TargetControlID="txtAgentTelephoneNo2"
                                                                                    FilterType="Numbers">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <asp:TextBox ID="txtAgentTelephoneNo3" runat="server" TabIndex="0" ValidationGroup="ContactInformation"
                                                                                    CssClass="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtAgentTelephoneNo3" runat="server" TargetControlID="txtAgentTelephoneNo3"
                                                                                    FilterType="Numbers">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <asp:CustomValidator ID="cusphone" runat="server" Display="None" ValidationGroup="ContactInformation"
                                                                                    EnableClientScript="false" OnServerValidate="cusphone_ServerValidate"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Insurance Carrier
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtInsuranceCarrier" runat="server" CssClass="txtbox" TabIndex="0"
                                                                                    MaxLength="100"></asp:TextBox>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvInsuranceCarrier" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="ContactInformation" EnableClientScript="true" Display="None"
                                                                                    ControlToValidate="txtInsuranceCarrier" ErrorMessage="Please enter insurance carrier under contact information"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </Content>
                                                            </Ajax:AccordionPane>
                                                            <Ajax:AccordionPane ID="apnAggregate" runat="server">
                                                                <Header>
                                                                    Aggregate Limits / Coverage</Header>
                                                                <Content>
                                                                    <table align="center" border="0" cellpadding="0" cellspacing="5" width="100%" class="searchtableclass ">
                                                                        <tr>
                                                                            <td colspan="2" class="hdrbold">
                                                                                General Liability
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtd" width="45%">
                                                                                <span class="mandatorystar">*</span>Each Occurrence $
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtEachOccuranceAmt1" runat="server" CssClass="txtbox1" TabIndex="0"
                                                                                    MaxLength="15"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtEachOccuranceAmt1" runat="server" TargetControlID="txtEachOccuranceAmt1"
                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvEachOccuranceAmt1" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="txtEachOccuranceAmt1"
                                                                                    ErrorMessage="Please enter each occurrence under general liability"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Product / Completed Operations Aggregate $
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtOperationsAggregeteAmt" runat="server" CssClass="txtbox1" TabIndex="0"
                                                                                    MaxLength="15"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtOperationsAggregeteAmt" runat="server" TargetControlID="txtOperationsAggregeteAmt"
                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvOperationsAggregeteAmt" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="txtOperationsAggregeteAmt"
                                                                                    ErrorMessage="Please enter product / completed operations aggregate under general liability"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>General Aggregate $
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtGeneralAggregateAmt" runat="server" CssClass="txtbox1" TabIndex="0"
                                                                                    MaxLength="15"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtGeneralAggregateAmt" runat="server" TargetControlID="txtGeneralAggregateAmt"
                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvGeneralAggregateAmt" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="txtGeneralAggregateAmt"
                                                                                    ErrorMessage="Please enter general aggregate under general liability"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Is policy "Occurrence Based"?
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:RadioButtonList ID="rblOccurrenceBased" runat="server" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Value="0">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="1">No</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvOccurrenceBased" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="rblOccurrenceBased"
                                                                                    ErrorMessage="Please select the type of policy"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Is General Aggregate Limit "Per Project"?
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:RadioButtonList ID="rblGeneralAggregateLimit" runat="server" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Value="0">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="1">No</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvGeneralAggregateLimit" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="rblGeneralAggregateLimit"
                                                                                    ErrorMessage="Please select the general aggregate limit"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="hdrbold" colspan="2">
                                                                                Additional Insured Endorsements
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Will you provide additional insured endorsements
                                                                                for ongoing and completed operations
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:RadioButtonList ID="rblProvideamt" runat="server" RepeatDirection="Horizontal"
                                                                                    onClick="ShowContents()" TabIndex="0">
                                                                                    <asp:ListItem Value="0">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="1">No</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvProvideamt" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="rblProvideamt"
                                                                                    ErrorMessage="Please select a option under additional insured endorsements"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trnotes" runat="server" style="display: none">
                                                                            <td class="formtdrt" colspan="2">
                                                                                Contractor shall be included as an additional insured under the CGL policy for both
                                                                                ongoing and completed operations. ISO additional insured endorsements CG 20 10 (for
                                                                                ongoing operations) and CG 20 37 (for completed operations) or substitute endorsements
                                                                                providing equivalent coverage, will be attached to Subcontractor's CGL. The umbrella
                                                                                insurance, if any, shall provide following form of additional insured coverage.
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="hdrbold" colspan="2">
                                                                                Umbrella/Excess Liability
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Do You Have Umbrella/Excess Liability
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:RadioButtonList ID="rblUmbrellaExcessliability" runat="server" RepeatDirection="Horizontal"
                                                                                    TabIndex="0" onClick="ShowUmbrella()">
                                                                                    <asp:ListItem Value="0">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="1">No</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvUmbrellaExcessliability" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="rblUmbrellaExcessliability"
                                                                                    ErrorMessage="Please select an option under Umbrella/Exess Liability"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trUmbrella" runat="server">
                                                                            <td class="formtdlt">
                                                                                Umbrella Each Occurence/Aggregate
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtUmbrella" runat="server" CssClass="txtbox1" MaxLength="15"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtUmbrella" runat="server" TargetControlID="txtUmbrella"
                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <asp:CustomValidator ID="custTxtUmbrella" runat="server" OnServerValidate="custTxtUmbrella_ServerValidate"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="false" Display="None"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trExcess" runat="server" style="display: none">
                                                                            <td class="formtdlt">
                                                                                Excess Each Occurence/Aggregate
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtExcess" runat="server" CssClass="txtbox1" MaxLength="15"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtExcess" runat="server" TargetControlID="txtExcess"
                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <asp:CustomValidator ID="cusvTxtExcess" runat="server" Display="None" ValidationGroup="AggregateLimits"
                                                                                    EnableClientScript="false" OnServerValidate="cusvTxtExcess_ServerValidate"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="hdrbold" colspan="2">
                                                                                Auto
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Each Accident $
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtAccidentamt" runat="server" CssClass="txtbox1" TabIndex="0" MaxLength="15"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtAccidentamt" runat="server" FilterType="Custom"
                                                                                    ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.," TargetControlID="txtAccidentamt">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvEachAccidentAmt" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="txtAccidentamt"
                                                                                    ErrorMessage="Please enter each accident under auto"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="hdrbold" colspan="2">
                                                                                Workers Compensation And Employers' Liability
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Does WC Meet Statutory Limits?
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:RadioButtonList ID="rblStatutoryLimits" runat="server" RepeatDirection="Horizontal"
                                                                                    TabIndex="0">
                                                                                    <asp:ListItem Value="0">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="1">No</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvStatutoryLimits" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="rblStatutoryLimits"
                                                                                    ErrorMessage="Please select WC meet statutory limits"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Employers Liability-Each Accident$
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtEmpLiabilityAmt" runat="server" CssClass="txtbox1" TabIndex="0"
                                                                                    MaxLength="15"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtEmpLiabilityAmt" runat="server" TargetControlID="txtEmpLiabilityAmt"
                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvEmpLiabilityAmt" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="txtEmpLiabilityAmt"
                                                                                    ErrorMessage="Please enter employees liability under workers compensation"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Employers Liability Disease-Each Employee $
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtDiseaseEmpAmt" runat="server" CssClass="txtbox1" TabIndex="0"
                                                                                    MaxLength="15"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtDiseaseEmpAmtone" runat="server" TargetControlID="txtDiseaseEmpAmt"
                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvDiseaseEmpAmtone" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="txtDiseaseEmpAmt"
                                                                                    ErrorMessage="Please enter disease each employee under workers compensation"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Employers Liability Disease-Policy Limit $
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtDiseasePolicyAmtone" runat="server" CssClass="txtbox1" TabIndex="0"
                                                                                    MaxLength="15"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtDiseasePolicyAmtone" runat="server" TargetControlID="txtDiseasePolicyAmtone"
                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvDiseasePolicyAmt" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="txtDiseasePolicyAmtone"
                                                                                    ErrorMessage="Please enter disease policy limit under workers compensation"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="hdrbold" colspan="2">
                                                                                Professional Liability
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table cellpadding="4" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td width="190px" align="right" style="padding-bottom: 4px;">
                                                                                            <span id="spnProfessional" runat="server" style="color: Red; font-size: 11px">*</span>
                                                                                        </td>
                                                                                        <td class="bodytextright" style="padding-bottom: 4px;">
                                                                                            Each Occurrence $
                                                                                        </td>
                                                                                        <td class="formtdrt">
                                                                                            <asp:TextBox ID="txtEachOccuranceAmt2" runat="server" CssClass="txtbox1" TabIndex="0"
                                                                                                MaxLength="15"></asp:TextBox>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtEachOccuranceAmt2" runat="server" TargetControlID="txtEachOccuranceAmt2"
                                                                                                FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                            <asp:CustomValidator ID="cusvEachOccurances" runat="server" ValidationGroup="AggregateLimits"
                                                                                                Display="None" OnServerValidate="cusvEachOccurances_ServerValidate"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <%--G. Vera 06/11/2021 - Added--%>
                                                                        <tr>
                                                                            <td class="hdrbold" colspan="2">
                                                                                Pollution Liability
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table cellpadding="4" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td width="190px" align="right" style="padding-bottom: 4px;">
                                                                                            <span id="Span2" runat="server" style="color: Red; font-size: 11px">*</span>
                                                                                        </td>
                                                                                        <td class="bodytextright" style="padding-bottom: 4px;">
                                                                                            Each Occurrence/Aggregate $
                                                                                        </td>
                                                                                        <td class="formtdrt">
                                                                                            <asp:TextBox ID="txtPollutionLiability" runat="server" CssClass="txtbox1" TabIndex="0"
                                                                                                MaxLength="18"></asp:TextBox>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftbxePollutionLiability" runat="server" TargetControlID="txtPollutionLiability"
                                                                                                FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                            <asp:RequiredFieldValidator ID="rqfvPollutionLiability" runat="server" SetFocusOnError="true"
                                                                                                ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="txtPollutionLiability"
                                                                                                ErrorMessage="Please enter pollution liability coverage amount"></asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <%--G. Vera 07/19/2021 - Added--%>
                                                                        <tr>
                                                                            <td class="hdrbold" colspan="2">
                                                                                Are you willing to obtain Pollution Liability?
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table cellpadding="4" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td width="190px" align="right" style="padding-bottom: 4px;">
                                                                                            <span id="spanPollutionLiabExplainStar" runat="server" style="color: Red; font-size: 11px" > *</span>                                                                                            
                                                                                        </td>
                                                                                        <td class="bodytextright" style="padding-bottom: 4px;">
                                                                                            Please explain:
                                                                                        </td>
                                                                                        <td class="formtdrt">
                                                                                            <asp:TextBox ID="txtPollutionLiabExplain" runat="server" CssClass="txtboxlarge" TabIndex="0"
                                                                                                MaxLength="10000" onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TextMode="MultiLine"></asp:TextBox>
                                                                                            
                                                                                            <asp:RequiredFieldValidator ID="rqfvPollutionLiabExplain" runat="server" SetFocusOnError="true"
                                                                                                        ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="txtPollutionLiabExplain"
                                                                                                        ErrorMessage="Please explain if you are willing to obtain Pollution Liability coverage"></asp:RequiredFieldValidator>
                                                                                            
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>

                                                                        <%--G. Vera 10/25/2012 - Added--%>
                                                                        <tr>
                                                                            <td class="hdrbold" colspan="2" id="tdMarineCargoHead" runat="server">
                                                                                Marine Cargo/Inland Transit Insurance
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" id="tdMarineCargoQuestions" runat="server">
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td class="bodytextright" style="padding-bottom: 4px">
                                                                                            <div style="float: left; padding-left: 25px; padding-top: 8px; width: 41%;">
                                                                                                <span id="spnMarineCargoStar1" runat="server" style="color: Red; font-size: 11px">*</span>
                                                                                                Do you have Marine Cargo Insurance?
                                                                                            </div>
                                                                                            <div style="float: left; padding-left: 10px;">
                                                                                                <asp:RadioButtonList runat="server" ID="rdoTransitIns" RepeatDirection="Horizontal">
                                                                                                    <asp:ListItem Text="Yes" Value="1" />
                                                                                                    <asp:ListItem Text="No" Value="0" />
                                                                                                </asp:RadioButtonList>
                                                                                                <asp:RequiredFieldValidator ID="reqvTransitIns" runat="server" SetFocusOnError="true"
                                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="rdoTransitIns"
                                                                                                    ErrorMessage="Please answer Inland Transit Insurance question under Marine & Cargo section"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr runat="server" id="trPerConveyanceLimitMarineIns" style="display: none">
                                                                                        <td class="bodytextright" style="padding-bottom: 4px;">
                                                                                            <div style="float: left; padding-left: 25PX; padding-top: 8px; width: 41%">
                                                                                                <span id="spnMarineCargoStar2" runat="server" style="color: Red; font-size: 11px">*</span>
                                                                                                Per Conveyance Limit USD $</div>
                                                                                            <div style="float: left; padding-left: 10px">
                                                                                                <asp:TextBox ID="txtMarineCargoConvLimit" runat="server" CssClass="txtbox1" MaxLength="15"></asp:TextBox>
                                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtextPerConveyance" runat="server" TargetControlID="txtMarineCargoConvLimit"
                                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                <asp:RequiredFieldValidator ID="reqvMarineCargoConvLimit" runat="server" SetFocusOnError="true"
                                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="txtMarineCargoConvLimit"
                                                                                                    ErrorMessage="Please answer Marine Insurance conveyance limit question under Marine & Cargo section"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextright" style="padding-bottom: 4px;">
                                                                                            <div style="float: left; padding-left: 25PX; padding-top: 8px; width: 41%">
                                                                                                <span id="spnMarineCargoStar3" runat="server" style="color: Red; font-size: 11px">*
                                                                                                </span>Do you have Inland Transit?
                                                                                            </div>
                                                                                            <div style="float: left; padding-left: 10px">
                                                                                                <asp:RadioButtonList runat="server" ID="rdoMarineCargoInlandTransit" RepeatDirection="Horizontal">
                                                                                                    <asp:ListItem Text="Yes" Value="1" />
                                                                                                    <asp:ListItem Text="No" Value="0" />
                                                                                                </asp:RadioButtonList>
                                                                                                <asp:RequiredFieldValidator ID="reqvMarineCargoInlandTransit" runat="server" SetFocusOnError="true"
                                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="rdoMarineCargoInlandTransit"
                                                                                                    ErrorMessage="Please answer Inland Transit question under Marine & Cargo section"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr runat="server" id="trPerConveyanceLimitInlandTrans" style="display: none">
                                                                                        <td class="bodytextright" style="padding-bottom: 4px;">
                                                                                            <div style="float: left; padding-left: 25PX; padding-top: 8px; width: 41%">
                                                                                                <span id="spnConveyanceLimitInlandTrans" runat="server" style="color: Red; font-size: 11px">
                                                                                                    *</span> Per Conveyance Limit USD $</div>
                                                                                            <div style="float: left; padding-left: 10px">
                                                                                                <asp:TextBox ID="txtPerConveyanceLimitInlandTrans" runat="server" CssClass="txtbox1"
                                                                                                    MaxLength="15"></asp:TextBox>
                                                                                                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtPerConveyanceLimitInlandTrans"
                                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                                <asp:RequiredFieldValidator ID="reqvConvLimitInlandTrans" runat="server" SetFocusOnError="true"
                                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="txtPerConveyanceLimitInlandTrans"
                                                                                                    ErrorMessage="Please answer Inland Transit conveyance limit question under Marine & Cargo section"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextright" style="padding-bottom: 4px;">
                                                                                            <div style="float: left; padding-left: 25px; padding-top: 8px; width: 41%">
                                                                                                <span id="spnMarineCargoStar4" runat="server" style="color: Red; font-size: 11px">*
                                                                                                </span>Storage?
                                                                                            </div>
                                                                                            <div style="float: left; padding-left: 10px">
                                                                                                <asp:RadioButtonList runat="server" ID="rdoMarineCargoStorage" RepeatDirection="Horizontal">
                                                                                                    <asp:ListItem Text="Yes" Value="1" />
                                                                                                    <asp:ListItem Text="No" Value="0" />
                                                                                                </asp:RadioButtonList>
                                                                                                <asp:RequiredFieldValidator ID="reqvMarineCargoStorage" runat="server" SetFocusOnError="true"
                                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="rdoMarineCargoStorage"
                                                                                                    ErrorMessage="Please answer Storage question under Marine & Cargo section"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextright" style="padding-bottom: 4px;">
                                                                                            <div style="float: left; padding-left: 25px; padding-top: 8px; width: 41%;">
                                                                                                <span id="spnMarineCargoStar5" runat="server" style="color: Red; font-size: 11px">*
                                                                                                </span>Installation of Equipment?
                                                                                            </div>
                                                                                            <div style="float: left; padding-left: 10px">
                                                                                                <asp:RadioButtonList runat="server" ID="rdoMarineCargoInstEquip" RepeatDirection="Horizontal">
                                                                                                    <asp:ListItem Text="Yes" Value="1" />
                                                                                                    <asp:ListItem Text="No" Value="0" />
                                                                                                </asp:RadioButtonList>
                                                                                                <asp:RequiredFieldValidator ID="reqvMarineCargoInsEquip" runat="server" SetFocusOnError="true"
                                                                                                    ValidationGroup="AggregateLimits" EnableClientScript="true" Display="None" ControlToValidate="rdoMarineCargoInstEquip"
                                                                                                    ErrorMessage="Please answer Installation of Equipment question under Marine & Cargo section"></asp:RequiredFieldValidator>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </Content>
                                                            </Ajax:AccordionPane>
                                                            <Ajax:AccordionPane ID="apnBonding" runat="server">
                                                                <Header>
                                                                    Bonding and Surety</Header>
                                                                <Content>
                                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                        <tr id="trMessage" style="display: none;">
                                                                            <td colspan="2">
                                                                                &nbsp;&nbsp;<asp:Label ID="lblMessage" runat="server" CssClass="summaryerrormsg"
                                                                                    Text="Available bonding capacity should be less than or equal to total bonding capacity"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtd" width="45%">
                                                                                <span class="mandatorystar">*</span>Bonding / Surety Company Name
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtBondingSuretyCompanyNameone" runat="server" TabIndex="0" CssClass="txtbox"
                                                                                    MaxLength="100"></asp:TextBox>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvBondingSuretyCompanyNameone" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="Bonding" EnableClientScript="true" Display="None" ControlToValidate="txtBondingSuretyCompanyNameone"
                                                                                    ErrorMessage="Please enter bonding / surety company name under bonding and surety"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Company Contact Name
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtCompanyContactName" runat="server" CssClass="txtbox" TabIndex="0"
                                                                                    MaxLength="100"></asp:TextBox>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvCompanyContactName" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="Bonding" EnableClientScript="true" Display="None" ControlToValidate="txtCompanyContactName"
                                                                                    ErrorMessage="Please enter company contact name under bonding and surety"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Contact Telephone Number &nbsp; <asp:CheckBox ID="chkContactUSA" runat="server" Text="Non-US" />
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtContactTelephoneNo1" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                    MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtContactTelephoneNo1" runat="server" TargetControlID="txtContactTelephoneNo1"
                                                                                    FilterType="Numbers">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <asp:TextBox ID="txtContactTelephoneNo2" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                    MaxLength="3"></asp:TextBox>
                                                                                 <span id="spnCPhone2" runat="server">-</span><Ajax:FilteredTextBoxExtender ID="ftxtContactTelephoneNo2" runat="server" TargetControlID="txtContactTelephoneNo2"
                                                                                    FilterType="Numbers">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <asp:TextBox ID="txtContactTelephoneNo3" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                    MaxLength="4"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtContactTelephoneNo3" runat="server" TargetControlID="txtContactTelephoneNo3"
                                                                                    FilterType="Numbers">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <asp:CustomValidator ID="cusvContactPhone" runat="server" Display="None" ValidationGroup="Bonding"
                                                                                    EnableClientScript="false" OnServerValidate="cusContactPhone_ServerValidate"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Total Bonding Capacity $
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtTotalBondingCapacityAmt" runat="server" TabIndex="0" CssClass="txtbox1"
                                                                                    MaxLength="15"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtTotalBondingCapacityAmt" runat="server" TargetControlID="txtTotalBondingCapacityAmt"
                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvTotalBondingCapacityAmt" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="Bonding" EnableClientScript="true" Display="None" ControlToValidate="txtTotalBondingCapacityAmt"
                                                                                    ErrorMessage="Please enter total bonding capacity under bonding and surety"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdlt">
                                                                                <span class="mandatorystar">*</span>Available Bonding Capacity $
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtCurrentBondingCapacityAmt" TabIndex="0" runat="server" CssClass="txtbox1"
                                                                                    MaxLength="15"></asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtCurrentBondingCapacityAmt" runat="server" TargetControlID="txtCurrentBondingCapacityAmt"
                                                                                    FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0" InvalidChars="A-Z,a-z,-,.,">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <%--<asp:RequiredFieldValidator ID="reqvCurrentBondingCapacityAmt" runat="server" SetFocusOnError="true"
                                                                                    ValidationGroup="Bonding" EnableClientScript="true" Display="None" ControlToValidate="txtCurrentBondingCapacityAmt"
                                                                                    ErrorMessage="Please enter available bonding capacity under bonding and surety"></asp:RequiredFieldValidator>--%>
                                                                                <asp:CompareValidator ID="compvAvailableBonding" runat="server" Display="None" ControlToValidate="txtCurrentBondingCapacityAmt"
                                                                                    ControlToCompare="txtTotalBondingCapacityAmt" Operator="LessThanEqual" Type="Currency"
                                                                                    ErrorMessage="Available bonding capacity should be less than or equal to total bonding capacity"
                                                                                    ValidationGroup="Bonding"></asp:CompareValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </Content>
                                                            </Ajax:AccordionPane>
                                                        </Panes>
                                                    </Ajax:Accordion>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <asp:HiddenField ID="hdnInsuranceId" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td class="bodytextcenter">
                                                                <asp:ImageButton ID="imbSave" runat="server" CausesValidation="false" ImageUrl="~/Images/save.jpg"
                                                                    ToolTip="Save" TabIndex="0" OnClick="imbSave_Click" />
                                                                <asp:ImageButton ID="imbSaveNext" runat="server" ToolTip="Save to Complete" ImageUrl="~/Images/stc.jpg"
                                                                    OnClick="imbSaveNext_Click" CausesValidation="False" ValidationGroup="ContactInformation"
                                                                    Visible="false" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                            PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" CancelControlID="imbOk">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalPopupDiv" class="popupdivsmall" style="display: none; width: 485px;">
                                            <table cellpadding="0" cellspacing="0" border="0" width="485px">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="Td3">
                                                        <asp:Label ID="lblheading" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl tdheight" align="center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        <asp:Label ID="lblMsg" runat="server" CssClass="errormsg"></asp:Label>
                                                        <asp:Label ID="lblError" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                        <asp:ValidationSummary CssClass="summaryerrormsg" ValidationGroup="ContactInformation"
                                                            DisplayMode="BulletList" runat="server" ID="valSummary" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl tdheight" align="center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl" align="center">
                                                        <input type="button" value="OK" title="Ok" id="imbOk" class="ModalPopupButton" onclick="$find('modalExtnd').hide();"
                                                            runat="server" />
                                                        <asp:ImageButton ID="imbOkoutlook" ImageAlign="AbsMiddle" ToolTip="OK" CausesValidation="false"
                                                            OnClientClick="TriggerOutlook();" ImageUrl="~/Images/ok.jpg" runat="server" OnClick="imbOkoutlook_Click"
                                                            Visible="False" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl tdheight" align="center">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hdnVendorStatus" runat="server" />
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

    <script language="javascript" type="text/javascript">
        //G. Vera 12/12/12 - Added function
        function OpenHideControl(controlId, isDisplay) {
            if (isDisplay == 1) {
                document.getElementById(controlId).style.display = "";
            }
            else document.getElementById(controlId).style.display = "none";
        }

        function OpenPrint() {
            window.open("../VQFView/InsuranceBondingView.aspx?Print=1");
        }
        function Close() {
            var x = $find("ModalAddDoc");
            if (x) {
                x.hide();
            }
        }

        function numberFormat(nStr, prefix, ctrl) {
            var txt = document.getElementById(ctrl);
            if (txt.value != "0") {

                var prefix = prefix || '';
                nStr += '';
                nStr = nStr.replace(/^[0]+/g, "");

                y = nStr.split(',');
                var y1 = '';
                for (var i = 0; i < y.length; i++) {
                    y1 += y[i];

                }
                if (y.length > 0) {
                    nStr = y1;
                }
                x = nStr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1))
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                var txt = document.getElementById(ctrl);

                txt.value = prefix + x1 + x2;
            }
        }
        function ShowContents() {

            if (document.getElementById("<%=rblProvideamt.ClientID%>_0").checked) {
                document.getElementById("<%=trnotes.ClientID%>").style.display = "";
            }
            else {
                document.getElementById("<%=trnotes.ClientID%>").style.display = "none";
            }
        }

        function validateNumber(txtBox1) {
            if (document.getElementById(txtBox1).value != "") {
                var str = document.getElementById(txtBox1).value;
                var str1 = "0123456789.";
                var i = 0;
                for (i = 0; i < str.length; i++) {
                    if (str1.indexOf(str.substring(i, i + 1)) < 0 || str1.indexOf(str.substring(i, i + 1)) > str1.length || str.indexOf(".") != str.lastIndexOf(".")) {
                        document.getElementById(txtBox1).value = str.substring(0, str.length - 1);
                        document.getElementById(txtBox1).focus();
                        break;
                    }
                }
            }
        }

        function checkDecimal(txtBox1) {

            if (document.getElementById(txtBox1).value != "") {
                var str2 = document.getElementById(txtBox1).value;
                var str3;
                if (str2.indexOf('.') < 0) {
                    str3 = str2 + ".00";
                    document.getElementById(txtBox1).value = roundNumber(str3, 2);
                }
                else {
                    document.getElementById(txtBox1).value = roundNumber(str2, 2);
                }
            }

        }

        function ShowUmbrella() {

            if (document.getElementById("<%= rblUmbrellaExcessliability.ClientID %>_0").checked == true) {
                document.getElementById("<%= trUmbrella.ClientID %>").style.display = "";
                document.getElementById("<%= trExcess.ClientID %>").style.display = "";
            }
            else {
                document.getElementById("<%= trUmbrella.ClientID %>").style.display = "None";
                document.getElementById("<%= trExcess.ClientID %>").style.display = "None";
                document.getElementById("<%= txtUmbrella.ClientID %>").value = "";
                document.getElementById("<%= txtExcess.ClientID %>").value = "";
            }
        }
        function removeCommas(val) {
            var str = val.split(",");
            return str.join("");
        }
        function validateBondingCapacity() {
            var totalBonding = removeCommas(document.getElementById("<%=txtTotalBondingCapacityAmt.ClientID%>").value.replace(",", ""));
            var availBonding = removeCommas(document.getElementById("<%=txtCurrentBondingCapacityAmt.ClientID%>").value.replace(",", ""));
            if (totalBonding != "" && availBonding != "") {
                if (parseFloat(totalBonding) < parseFloat(availBonding)) {
                    document.getElementById("trMessage").style.display = "";  //alert("Available bonding capacity should be less than total bonding capacity");
                }
                else {
                    document.getElementById("trMessage").style.display = "none";
                }
            }
        }
        function SwitchPhone(val, phone1, phone2, phone3, spnPhone) {

            var p1 = document.getElementById(phone1);
            var p2 = document.getElementById(phone2);
            var p3 = document.getElementById(phone3);

            var s1 = document.getElementById(spnPhone);


            if (val.checked) {
                //val.nextSibling.innerHTML = "Non-US";
                p3.style.display = "none";
                p1.maxLength = 5;
                p2.maxLength = 15;
                p2.className = "txtbox";
                p3.value = "";
                s1.style.display = "none";
                p1.attributes.removeNamedItem("onkeyup");
                p2.attributes.removeNamedItem("onkeyup");
            }
            else {
                //val.nextSibling.innerHTML = "Non-US";
                p3.style.display = "";
                p2.className = "txtboxsmall";
                p1.maxLength = 3;
                p2.maxLength = 3;

                s1.style.display = "";

                p1.setAttribute("onkeyup", "SetFocusOnPhone(" + phone1 + "," + phone2 + ")");
                p2.setAttribute("onkeyup", "SetFocusOnPhone(" + phone2 + "," + phone3 + ")");
            }
            p3.value = "";
            p2.value = "";
            p1.value = "";
        }
    </script>
    <script src="../Script/jquery.js" type="text/javascript"></script>
</body>
</html>
