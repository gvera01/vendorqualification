﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Safety.aspx.cs" Inherits="VQF_Safety" %>
<%@ Register Src="~/UserControl/uclSafetyAttachmentBox.ascx" TagPrefix="Haskell"
    TagName="uclSafetyAttachmentBox" %>


<%@ Register TagPrefix="cc1" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Haskell</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache">
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultfocus="txtYearOne">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="960px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:VQFEditSideMenu ID="VQFEditSideMenu" runat="server" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="bodytextbold_right even_pad">
                                                    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click">Return to 
          Search Result</asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextbold_right">
                                                    Welcome
                                                    <asp:Label ID="lblwelcome" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    Vendor <span style="letter-spacing: 1px">Qualification</span> Form - Safety
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <asp:HiddenField ID="hdnVendorStatus" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="upnlSafety" runat="server">
                                                        <ContentTemplate>
                                                            <%--G. Vera 06/19/2014: Added--%>
                                                            <Haskell:uclSafetyAttachmentBox runat="server" ID="uclAttachEMR1" />

                                                            <Ajax:Accordion ID="accSafety" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
                                                                HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                                                                FadeTransitions="true" FramesPerSecond="40" TransitionDuration="250" AutoSize="none"
                                                                RequireOpenedPane="true" SuppressHeaderPostbacks="true">
                                                                <Panes>
                                                                    <Ajax:AccordionPane ID="apnRates" runat="server">
                                                                        <%--G. Vera 01/30/2013 - Changed header--%>
                                                                        <Header>
                                                                            Experience Modification Rate (EMR)
                                                                        </Header>
                                                                        <Content>
                                                                            <asp:Panel ID="pnlEmr" runat="server" Width="100%">
                                                                                <table class="searchtableclass" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="sectionpad">
                                                                                            <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <%--G. Vera 01/30/2013 - Changed wording --%>
                                                                                                    <td colspan="4" class="bodytextbold">
                                                                                                        Please provide your most current three years EMR
                                                                                                        <br />
                                                                                                        <span style="font-family: Tahoma; font-size: 9px; font-style: italic; font-weight: normal">
                                                                                                            If first year shown is not your current year, please mark "N/A" </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trMessage" style="display: none;">
                                                                                                    <td colspan="5">
                                                                                                        <asp:Label ID="lblMessage" runat="server" CssClass="summaryerrormsg" Style="display: none;"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--EMR Year 1--%>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft" valign="top" width="7%">
                                                                                                        <span class="mandatorystar">*</span>Year
                                                                                                    </td>
                                                                                                    <td class="formtdrt" valign="top" width="11%">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtYearOne" Text="2010" ReadOnly="true" runat="server"
                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtYearOne" runat="server" TargetControlID="txtYearOne"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top" width="8%">
                                                                                                        <span class="mandatorystar">*</span>Rate
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top" width="26%">
                                                                                                        <asp:TextBox CssClass="txtboxmedium1" ID="txtRateOne" TabIndex="0" runat="server"
                                                                                                            MaxLength="4"></asp:TextBox>&nbsp;&nbsp;<asp:CheckBox ID="chkNA1" runat="server"
                                                                                                                Text="N/A" />
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRateOne" runat="server" TargetControlID="txtRateOne"
                                                                                                            FilterMode="ValidChars" FilterType="Custom" ValidChars="0123456789.">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusvYearOne" runat="server" Display="None" ValidationGroup="Current"
                                                                                                            OnServerValidate="cusvYearOne_ServerValidate"></asp:CustomValidator>
                                                                                                        <asp:RangeValidator ID="rangRateOne" runat="server" ControlToValidate="txtRateOne"
                                                                                                            Type="Double" ErrorMessage="Please enter rate under year one range between 0 to 3 "
                                                                                                            Display="None" ValidationGroup="Current" MaximumValue="3" MinimumValue="0"></asp:RangeValidator>
                                                                                                    </td>
                                                                                                    <td id="trExplain1" runat="server" style="display: none">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr >
                                                                                                                <td>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" valign="top" width="30%">
                                                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplanation1" TabIndex="0" runat="server"
                                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TextMode="MultiLine"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>

                                                                                                    <%--G. Vera 06/12/2014 Added below attachment link--%>
                                                                                                    <td id="trAttachEMR1" runat="server" style="display: none">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>

                                                                                                                <td class="bodytextleft" valign="top" style="vertical-align: top;">
                                                                                                                    <%--<span class="mandatorystar" id="starAttachEMR1" runat="server">*</span>--%>
                                                                                                                    <asp:LinkButton ID="lnkAttachEMR1" OnClick="lnkAttach_Click" runat="server" Text="Attach EMR Letter"
                                                                                                                        CssClass="lnkAttach" />
                                                                                                                    <asp:ImageButton ID="imbAttachEMR1" OnClick="imbAttach_Click" runat="server"
                                                                                                                        ImageUrl="~/Images/paperclip.png" Visible="false" />
                                                                                                                </td>
                                                                                                                <td>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--EMR YEar 2--%>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <span class="mandatorystar">*</span>Year
                                                                                                    </td>
                                                                                                    <td class="formtdrt" valign="top">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtYearTwo" Text="2008" ReadOnly="true" TabIndex="0"
                                                                                                            runat="server" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtYearTwo" runat="server" TargetControlID="txtYearTwo"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <span class="mandatorystar">*</span>Rate
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top" width="26%">
                                                                                                        <asp:TextBox CssClass="txtboxmedium1" ID="txtRateTwo" runat="server" TabIndex="0"
                                                                                                            MaxLength="4"></asp:TextBox>&nbsp;&nbsp;<asp:CheckBox ID="chkNA2" runat="server"
                                                                                                                Text="N/A" />
                                                                                                        <asp:CustomValidator ID="cusvYearTwo" runat="server" Display="None" ValidationGroup="Current"
                                                                                                            OnServerValidate="cusvYearTwo_ServerValidate"></asp:CustomValidator>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRateTwo" runat="server" TargetControlID="txtRateTwo"
                                                                                                            FilterMode="ValidChars" FilterType="Custom" ValidChars="0123456789.">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:RangeValidator ID="rangRateTwo" runat="server" ControlToValidate="txtRateTwo"
                                                                                                            Type="Double" ErrorMessage="Please enter rate under year two range between 0 to 3"
                                                                                                            Display="None" ValidationGroup="Current" MaximumValue="3" MinimumValue="0"></asp:RangeValidator>
                                                                                                    </td>
                                                                                                    <td id="trExplain2" runat="server" style="display: none">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr >
                                                                                                                <td>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" valign="top" width="30%">
                                                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplanation2" TabIndex="0" runat="server"
                                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TextMode="MultiLine"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>

                                                                                                    <%--G. Vera 06/12/2014 Added below attachment link--%>
                                                                                                    <td id="trAttachEMR2" runat="server" style="display: none">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>

                                                                                                                <td class="bodytextleft" valign="top" style="vertical-align: top;">
                                                                                                                    <%--<span class="mandatorystar" id="starAttachEMR2" runat="server">*</span>--%>
                                                                                                                    <asp:LinkButton ID="lnkAttachEMR2" OnClick="lnkAttach_Click" runat="server" Text="Attach EMR Letter"
                                                                                                                        CssClass="lnkAttach" />
                                                                                                                    <asp:ImageButton ID="imbAttachEMR2" OnClick="imbAttach_Click" runat="server"
                                                                                                                        ImageUrl="~/Images/paperclip.png" Visible="false" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--EMR Year 3--%>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft" valign="top" valign="top">
                                                                                                        <span class="mandatorystar">*</span>Year
                                                                                                    </td>
                                                                                                    <td class="formtdrt" valign="top">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtYearThree" Text="2007" ReadOnly="true"
                                                                                                            TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtYearThree" runat="server" TargetControlID="txtYearThree"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <span class="mandatorystar">*</span>Rate
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <asp:TextBox CssClass="txtboxmedium1" ID="txtRateThree" runat="server" TabIndex="0"
                                                                                                            MaxLength="4"></asp:TextBox>&nbsp;&nbsp;<asp:CheckBox ID="chkNA3" runat="server"
                                                                                                                Text="N/A" />
                                                                                                        <asp:CustomValidator ID="cusvYearThree" runat="server" Display="None" ValidationGroup="Current"
                                                                                                            OnServerValidate="cusvYearThree_ServerValidate"></asp:CustomValidator>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRateThree" runat="server" TargetControlID="txtRateThree"
                                                                                                            FilterMode="ValidChars" FilterType="Custom" ValidChars="0123456789.">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:RangeValidator ID="rangRateThree" runat="server" ControlToValidate="txtRateThree"
                                                                                                            Type="Double" ErrorMessage="Please enter rate under year three range between 0 to 3"
                                                                                                            Display="None" ValidationGroup="Current" MaximumValue="3" MinimumValue="0"></asp:RangeValidator>
                                                                                                    </td>
                                                                                                    <td id="trExplain3" runat="server" style="display: none">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" valign="top" width="30%">
                                                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplanation3" TabIndex="0" runat="server"
                                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TextMode="MultiLine"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>

                                                                                                    <%--G. Vera 06/12/2014 Added below attachment link--%>
                                                                                                    <td id="trAttachEMR3" runat="server" style="display: none">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>

                                                                                                                <td class="bodytextleft" valign="top" style="vertical-align: top;">
                                                                                                                    <%--<span class="mandatorystar" id="starAttachEMR3" runat="server">*</span>--%>
                                                                                                                    <asp:LinkButton ID="lnkAttachEMR3" OnClick="lnkAttach_Click" runat="server" Text="Attach EMR Letter"
                                                                                                                        CssClass="lnkAttach" />
                                                                                                                    <asp:ImageButton ID="imbAttachEMR3" OnClick="imbAttach_Click" runat="server"
                                                                                                                        ImageUrl="~/Images/paperclip.png" Visible="false" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--G. Vera 01/29/2013 - Implemented--%>
                                                                                                <%--EMR Year 4--%>
                                                                                                <tr id="trEMRYEar4Field" runat="server" style="display: none">
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <span class="mandatorystar">*</span>Year
                                                                                                    </td>
                                                                                                    <td class="formtdrt" valign="top">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtYearFour" Text="2006" ReadOnly="true"
                                                                                                            TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtYearFour" runat="server" TargetControlID="txtYearFour"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <span class="mandatorystar">*</span>Rate
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <asp:TextBox CssClass="txtboxmedium1" ID="txtRateFour" runat="server" TabIndex="0"
                                                                                                            MaxLength="4"></asp:TextBox>&nbsp;&nbsp;<asp:CheckBox ID="chkEMRNA4" runat="server"
                                                                                                                Text="N/A" />
                                                                                                        <asp:CustomValidator ID="cusvYearFour" runat="server" Display="None" ValidationGroup="Current"
                                                                                                            OnServerValidate="cusvYearFour_ServerValidate"></asp:CustomValidator>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRateFour" runat="server" TargetControlID="txtRateFour"
                                                                                                            FilterMode="ValidChars" FilterType="Custom" ValidChars="0123456789.">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:RangeValidator ID="rangRateFour" runat="server" ControlToValidate="txtRateFour"
                                                                                                            Type="Double" ErrorMessage="Please enter rate under year four range between 0 to 3"
                                                                                                            Display="None" ValidationGroup="Current" MaximumValue="3" MinimumValue="0"></asp:RangeValidator>
                                                                                                    </td>
                                                                                                    <td id="trExplainEMR4" runat="server" style="display: none">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" valign="top" width="30%">
                                                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplanation4" TabIndex="0" runat="server"
                                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TextMode="MultiLine"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>

                                                                                                    <%--G. Vera 06/12/2014 Added below attachment link--%>
                                                                                                    <td id="trAttachEMR4" runat="server" style="display: none">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>

                                                                                                                <td class="bodytextleft" valign="top" style="vertical-align: top;">
                                                                                                                    <%--<span class="mandatorystar" id="starAttachEMR4" runat="server">*</span>--%>
                                                                                                                    <asp:LinkButton ID="lnkAttachEMR4" OnClick="lnkAttach_Click" runat="server" Text="Attach EMR Letter"
                                                                                                                        CssClass="lnkAttach" />
                                                                                                                    <asp:ImageButton ID="imbAttachEMR4" OnClick="imbAttach_Click" runat="server"
                                                                                                                        ImageUrl="~/Images/paperclip.png" Visible="false" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--G.Vera - End--%>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                        </Content>
                                                                    </Ajax:AccordionPane>
                                                                    <Ajax:AccordionPane ID="apnOSHA" runat="server">
                                                                        <Header>
                                                                            Safety History/OSHA 300 & 300 A
                                                                        </Header>
                                                                        <Content>
                                                                            <asp:Panel ID="pnlOsha" runat="server" Width="100%">
                                                                                <table align="center" border="0" cellpadding="4" cellspacing="0" width="100%" class="searchtableclass">
                                                                                    <tr>
                                                                                        <td class="Safetytextbold" colspan="5">
                                                                                            We understand that OSHA may not apply to your company. However, Haskell would like
                                                                                            to obtain the information below.
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdheight">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                        <td class="bodytextbold" width="45%">
                                                                                            <span class="mandatorystar">*</span>Year
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxsmall" ID="txtOSHAYearOne" TabIndex="0" Text="2009"
                                                                                                ReadOnly="true" runat="server" MaxLength="4"></asp:TextBox>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtOSHAYearOne" runat="server" TargetControlID="txtOSHAYearOne"
                                                                                                FilterType="Numbers">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkNA4" runat="server" Text="N/A" />
                                                                                            <asp:CustomValidator ID="cusvOSHAYearOne" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHAYearOne_ServerValidate"></asp:CustomValidator>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trExplain4" runat="server" style="display: none">
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="45%">
                                                                                            <span class="mandatorystar">*</span>Please Explain
                                                                                        </td>
                                                                                        <td class="bodytextleft" colspan="3" width="54%">
                                                                                            <asp:TextBox CssClass="txtboxlarge" ID="txtExplain4" TextMode="MultiLine" runat="server"
                                                                                                onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="45%">
                                                                                            <span class="mandatorystar">*</span>Total Number of Fatalities
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofDeathsYearOne" TabIndex="0" runat="server"
                                                                                                MaxLength="13"></asp:TextBox>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtNoofDeathsYearOne" runat="server" TargetControlID="txtNoofDeathsYearOne"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="41%" nowrap="nowrap">
                                                                                            <span class="mandatorystar">*</span>Total Number of Cases with Days Away from Work
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofCasesFrmWorkOne" TabIndex="0" runat="server"
                                                                                                MaxLength="13"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvOSHAWork1" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHAWork1_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtNoofCasesFrmWorkOne" runat="server" TargetControlID="txtNoofCasesFrmWorkOne"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="45%" nowrap="nowrap">
                                                                                            <span class="mandatorystar">*</span>Total Number of Cases with Job Transfer or Restriction
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofCasesTransferOne" runat="server"
                                                                                                TabIndex="0" MaxLength="13"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvOSHATransfer1" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHATransfer1_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtNoofCasesTransferOne" runat="server" TargetControlID="txtNoofCasesTransferOne"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="40%">
                                                                                            <span class="mandatorystar">*</span>Total Number of Other Recordable Cases
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtRecordableCasesOne" TabIndex="0" runat="server"
                                                                                                MaxLength="13"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvOSHARecord1" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHARecord1_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtRecordableCasesOne" runat="server" TargetControlID="txtRecordableCasesOne"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="44%">
                                                                                            <span class="mandatorystar">*</span>Total Hours Worked By All Employees
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtTotalHours1" runat="server" TabIndex="0"
                                                                                                MaxLength="13"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvTotalHours1" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvTotalHours1_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtTotalHours" runat="server" TargetControlID="txtTotalHours1"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td>&nbsp;</td>
                                                                                        
                                                                                        <td class="bodytextleft">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--G. vera 10/20/2014: Added below--%>
                                                                                    <tr>
                                                                                        <%--G. vera 06/17/2014: Added below--%>
                                                                                        <td>&nbsp;</td>
                                                                                        <td class="bodytextleft" width="40%" id="trAttachOSHA1" runat="server" colspan="5">
                                                                                            <span class="mandatorystar" id="starAttachOSHA1" runat="server">*</span>&nbsp;Please attach your supporting OSHA 300 & 300 A log&nbsp;
                                                                                            <asp:LinkButton ID="lnkAttachOSHA1" runat="server" Text="(attach document)" CssClass="lnkAttach"
                                                                                                OnClick="lnkAttach_Click" />
                                                                                            <asp:ImageButton ID="imbAttachOSHA1" runat="server" ImageUrl="~/Images/paperclip.png"
                                                                                                Visible="false" OnClick="imbAttach_Click" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                        <td class="bodytextleft" width="100%" colspan="5" id="tdNoAttachment1" runat="server">
                                                                                            <asp:CheckBox Text="&nbsp;&nbsp;I do not have supporting OSHA 300 & 300 A documentation"
                                                                                                runat="server"
                                                                                                ID="chkNoAttachment1"
                                                                                                OnCheckedChanged="chkNoAttachment1_CheckedChanged" AutoPostBack="true" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="5" style="border-bottom: 1px solid #cccccc;">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="5" class="tdheight">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                        <td class="bodytextbold">
                                                                                            <span class="mandatorystar">*</span>Year
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxsmall" ID="txtOSHAYearTwo" Text="2008" ReadOnly="true"
                                                                                                TabIndex="0" runat="server" MaxLength="4"></asp:TextBox>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtOSHAYearTwo" runat="server" TargetControlID="txtOSHAYearTwo"
                                                                                                FilterType="Numbers">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkNA5" runat="server" Text="N/A" />
                                                                                            <asp:CustomValidator ID="cusvOSHAYearTwo" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHAYearTwo_ServerValidate"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trExplain5" runat="server" style="display: none">
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="45%">
                                                                                            <span class="mandatorystar">*</span>Please Explain
                                                                                        </td>
                                                                                        <td class="bodytextleft" colspan="3" width="54%">
                                                                                            <asp:TextBox CssClass="txtboxlarge" ID="txtExplain5" runat="server" TextMode="MultiLine"
                                                                                                onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="44%">
                                                                                            <span class="mandatorystar">*</span>Total Number of Fatalities
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofDeathsYearTwo" runat="server" TabIndex="0"
                                                                                                MaxLength="13"></asp:TextBox>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtNoofDeathsYearTwo" runat="server" TargetControlID="txtNoofDeathsYearTwo"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="40%" nowrap="nowrap">
                                                                                            <span class="mandatorystar">*</span>Total Number of Cases with Days Away from Work
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofCasesFrmWorkTwo" runat="server"
                                                                                                TabIndex="0" MaxLength="13"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvOSHAWork2" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHAWork2_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtNoofCasesFrmWorkTwo" runat="server" TargetControlID="txtNoofCasesFrmWorkTwo"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="44%" nowrap="nowrap">
                                                                                            <span class="mandatorystar">*</span>Total Number of Cases with Job Transfer or Restriction
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofCasesTransferTwo" runat="server"
                                                                                                MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvOSHATransfer2" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHATransfer2_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtNoofCasesTransferTwo" runat="server" TargetControlID="txtNoofCasesTransferTwo"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="40%">
                                                                                            <span class="mandatorystar">*</span>Total Number of Other Recordable Cases
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtRecordableCasesTwo" runat="server" MaxLength="13"
                                                                                                TabIndex="0"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvOSHARecord2" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHARecord2_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtRecordableCasesTwo" runat="server" TargetControlID="txtRecordableCasesTwo"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="44%">
                                                                                            <span class="mandatorystar">*</span>Total Hours Worked By All Employees
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtTotalHours2" runat="server" TabIndex="0"
                                                                                                MaxLength="13"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvTotalHours2" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvTotalHours2_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtTotalHours2" runat="server" TargetControlID="txtTotalHours2"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td>&nbsp;</td>                                                                                        
                                                                                        <td class="bodytextleft">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                        <%--G. vera 06/17/2014: Added below--%>
                                                                                        <td class="bodytextleft" width="40%" id="trAttachOSHA2" runat="server" colspan="4">
                                                                                            <span class="mandatorystar" id="starAttachOSHA2" runat="server">*</span>&nbsp;Please attach your supporting OSHA 300 & 300 A log&nbsp;
                                                                                            <asp:LinkButton ID="lnkAttachOSHA2" runat="server" Text="(attach document)" CssClass="lnkAttach"
                                                                                                OnClick="lnkAttach_Click" />
                                                                                            <asp:ImageButton ID="imbAttachOSHA2" runat="server" ImageUrl="~/Images/paperclip.png"
                                                                                                OnClick="imbAttach_Click"
                                                                                                Visible="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                        <td class="bodytextleft" width="100%" colspan="5" id="tdNoAttachment2" runat="server">
                                                                                            <asp:CheckBox Text="&nbsp;&nbsp;I do not have supporting OSHA 300 & 300 A documentation"
                                                                                                runat="server"
                                                                                                ID="chkNoAttachment2"
                                                                                                OnCheckedChanged="chkNoAttachment1_CheckedChanged" AutoPostBack="true" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="5" style="border-bottom: 1px solid #cccccc;">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="5" class="tdheight">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                        <td class="bodytextbold">
                                                                                            <span class="mandatorystar">*</span>Year
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxsmall" ID="txtOSHAYearThree" Text="2007" runat="server"
                                                                                                ReadOnly="true" TabIndex="0" MaxLength="4"></asp:TextBox>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtOSHAYearThree" runat="server" TargetControlID="txtOSHAYearThree"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkNA6" runat="server" Text="N/A" />
                                                                                            <asp:CustomValidator ID="cusvOSHAYearThree" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHAYearThree_ServerValidate"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trExplain6" runat="server" style="display: none">
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="45%">
                                                                                            <span class="mandatorystar">*</span>Please Explain
                                                                                        </td>
                                                                                        <td class="bodytextleft" colspan="3" width="54%">
                                                                                            <asp:TextBox CssClass="txtboxlarge" TextMode="MultiLine" ID="txtExplain6" runat="server"
                                                                                                onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="44%">
                                                                                            <span class="mandatorystar">*</span>Total Number of Fatalities
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofDeathsYearThree" runat="server"
                                                                                                MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtNoofDeathsYearThree" runat="server" TargetControlID="txtNoofDeathsYearThree"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="40%" nowrap="nowrap">
                                                                                            <span class="mandatorystar">*</span>Total Number of Cases with Days Away from Work
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofCasesFrmWorkThree" runat="server"
                                                                                                TabIndex="0" MaxLength="13"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvOSHAWork3" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHAWork3_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtNoofCasesFrmWorkThree" runat="server" TargetControlID="txtNoofCasesFrmWorkThree"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="44%" nowrap="nowrap">
                                                                                            <span class="mandatorystar">*</span>Total Number of Cases with Job Transfer or Restriction
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofCasesTransferThree" runat="server"
                                                                                                TabIndex="0" MaxLength="13"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvOSHATransfer3" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHATransfer3_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtNoofCasesTransferThree" runat="server" TargetControlID="txtNoofCasesTransferThree"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="40%">
                                                                                            <span class="mandatorystar">*</span>Total Number of Other Recordable Cases
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtRecordableCasesThree" runat="server"
                                                                                                MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvOSHARecord3" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvOSHARecord3_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtRecordableCasesThree" runat="server" TargetControlID="txtRecordableCasesThree"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="44%">
                                                                                            <span class="mandatorystar">*</span>Total Hours Worked By All Employees
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtTotalHours3" runat="server" TabIndex="0"
                                                                                                MaxLength="13"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvTotalHours3" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvTotalHours3_ServerValidate"></asp:CustomValidator>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtTotalHours3" runat="server" TargetControlID="txtTotalHours3"
                                                                                                FilterType="Numbers, Custom" ValidChars=",">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td>&nbsp;</td>
                                                                                        <td class="bodytextleft">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <%--G. vera 06/17/2014: Added below--%>
                                                                                        <td>&nbsp;</td>
                                                                                        <td class="bodytextleft" width="40%" id="trAttachOSHA3" runat="server" colspan="4">
                                                                                            <span class="mandatorystar" id="starAttachOSHA3" runat="server">*</span>&nbsp;Please attach your supporting OSHA 300 & 300 A log&nbsp;
                                                                                            <asp:LinkButton ID="lnkAttachOSHA3" runat="server" Text="(attach document)" CssClass="lnkAttach"
                                                                                                OnClick="lnkAttach_Click" />
                                                                                            <asp:ImageButton ID="imbAttachOSHA3" runat="server" ImageUrl="~/Images/paperclip.png"
                                                                                                OnClick="imbAttach_Click"
                                                                                                Visible="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                        <td class="bodytextleft" width="100%" colspan="5" id="tdNoAttachment3" runat="server">
                                                                                            <asp:CheckBox Text="&nbsp;&nbsp;I do not have supporting OSHA 300 & 300 A documentation"
                                                                                                runat="server"
                                                                                                ID="chkNoAttachment3"
                                                                                                OnCheckedChanged="chkNoAttachment1_CheckedChanged" AutoPostBack="true" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="5" style="border-bottom: 1px solid #cccccc;">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>

                                                                                    <%--G. Vera 06/09/2014:  New OSHA year 4--%>
                                                                                    <tr runat="server" id="pnlOSHAYear4" style="display: none">
                                                                                        <td colspan="5">
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td colspan="5" class="tdheight">&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="1%"></td>
                                                                                                    <td class="bodytextbold">
                                                                                                        <span class="mandatorystar">*</span>Year
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtOSHAYearFour" runat="server"
                                                                                                            ReadOnly="true" TabIndex="0" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtOSHAYearFour" runat="server" TargetControlID="txtOSHAYearFour"
                                                                                                            FilterType="Numbers, Custom" ValidChars=",">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="chkNA7" runat="server" Text="N/A" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trExplain7" runat="server" style="display: none">
                                                                                                    <td width="1%"></td>
                                                                                                    <td class="bodytextleft" width="45%">
                                                                                                        <span class="mandatorystar">*</span>Please Explain
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" colspan="3" width="54%">
                                                                                                        <asp:TextBox CssClass="txtboxlarge" TextMode="MultiLine" ID="txtExplain7" runat="server"
                                                                                                            onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                            onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td></td>
                                                                                                    <td class="bodytextleft" width="44%">
                                                                                                        <span class="mandatorystar">*</span>Total Number of Fatalities
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofDeathsYearFour" runat="server"
                                                                                                            MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtNoofDeathsYearFour"
                                                                                                            FilterType="Numbers, Custom" ValidChars=",">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="40%" nowrap="nowrap">
                                                                                                        <span class="mandatorystar">*</span>Total Number of Cases with Days Away from Work
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofCasesFrmWorkFour" runat="server"
                                                                                                            TabIndex="0" MaxLength="13"></asp:TextBox>
                                                                                                        
                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtNoofCasesFrmWorkFour"
                                                                                                            FilterType="Numbers, Custom" ValidChars=",">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td></td>
                                                                                                    <td class="bodytextleft" width="44%" nowrap="nowrap">
                                                                                                        <span class="mandatorystar">*</span>Total Number of Cases with Job Transfer or Restriction
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxmedium2" ID="txtNoofCasesTransferFour" runat="server"
                                                                                                            TabIndex="0" MaxLength="13"></asp:TextBox>
                                                                                                        
                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtNoofCasesTransferFour"
                                                                                                            FilterType="Numbers, Custom" ValidChars=",">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                        <span class="mandatorystar">*</span>Total Number of Other Recordable Cases
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxmedium2" ID="txtRecordableCasesFour" runat="server"
                                                                                                            MaxLength="13" TabIndex="0"></asp:TextBox>
                                                                                                        
                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtRecordableCasesFour"
                                                                                                            FilterType="Numbers, Custom" ValidChars=",">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td></td>
                                                                                                    <td class="bodytextleft" width="44%">
                                                                                                        <span class="mandatorystar">*</span>Total Hours Worked By All Employees
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxmedium2" ID="txtTotalHours4" runat="server" TabIndex="0"
                                                                                                            MaxLength="13"></asp:TextBox>
                                                                                                        
                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtTotalHours4"
                                                                                                            FilterType="Numbers, Custom" ValidChars=",">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                    <td>&nbsp;</td>
                                                                                                    <td class="bodytextleft"></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>&nbsp;</td>
                                                                                                    <%--G. vera 06/17/2014: Added below--%>
                                                                                                    <td class="bodytextleft" width="40%" id="trAttachOSHA4" runat="server" colspan="4">
                                                                                                        <span class="mandatorystar" id="starAttachOSHA4" runat="server">*</span>&nbsp;Please attach your supporting OSHA 300 & 300 A log&nbsp;
                                                                                                    <asp:LinkButton ID="lnkAttachOSHA4" runat="server" Text="(attach document)" CssClass="lnkAttach"
                                                                                                        OnClick="lnkAttach_Click" />
                                                                                                        <asp:ImageButton ID="imbAttachOSHA4" runat="server" ImageUrl="~/Images/paperclip.png"
                                                                                                            OnClick="imbAttach_Click"
                                                                                                            Visible="false" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>&nbsp;</td>
                                                                                                    <td class="bodytextleft" width="100%" colspan="5" id="tdNoAttachment4" runat="server">
                                                                                                        <asp:CheckBox Text="&nbsp;&nbsp;I do not have supporting OSHA 300 & 300 A documentation"
                                                                                                            runat="server"
                                                                                                            ID="chkNoAttachment4"
                                                                                                            OnCheckedChanged="chkNoAttachment1_CheckedChanged" AutoPostBack="true" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="5" style="border-bottom: 1px solid #cccccc;">&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--End of Change--%>


                                                                                    <tr>
                                                                                        <td class="tdheight">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <span class="mandatorystar">*</span>SIC or NAICS Code (From OSHA 300 Form)
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:TextBox CssClass="txtboxmedium2" ID="txtSIC" TabIndex="0" runat="server" MaxLength="6"></asp:TextBox>
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtSIC" runat="server" TargetControlID="txtSIC"
                                                                                                FilterType="UppercaseLetters,LowercaseLetters,Numbers">
                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:CheckBox ID="chkSICNA" runat="server" Text="N/A" />
                                                                                            <asp:CustomValidator ID="cusvSICNA" runat="server" Display="None" ValidationGroup="Osha"
                                                                                                OnServerValidate="cusvSICNA_ServerValidate"></asp:CustomValidator>
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSICExplain" runat="server" style="display: none">
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="45%">
                                                                                            <span class="mandatorystar">*</span>Please Explain
                                                                                        </td>
                                                                                        <td class="bodytextleft" colspan="3" width="54%">
                                                                                            <asp:TextBox CssClass="txtboxlarge" TextMode="MultiLine" ID="txtSICExplain" runat="server"
                                                                                                onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdheight">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                        </Content>
                                                                    </Ajax:AccordionPane>
                                                                    <Ajax:AccordionPane ID="apnQuestionnaire" runat="server">
                                                                        <Header>
                                                                            Questionnaire
                                                                        </Header>
                                                                        <Content>
                                                                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr>
                                                                                    <td width="1%">
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <span class="mandatorystar">*</span>Does your company have a written Safety Program?
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <asp:RadioButtonList ID="rdoSafetyProgram" runat="server" TabIndex="0" RepeatDirection="Horizontal">
                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                        <%--<asp:RequiredFieldValidator ID="reqvSafetyProgram" ControlToValidate="rdoSafetyProgram"
                                                                                            Display="None" runat="server" SetFocusOnError="true" ValidationGroup="Questionair"
                                                                                            ErrorMessage="Please select a option in safety program" EnableClientScript="true">
                                                                                        </asp:RequiredFieldValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="1%">
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <span class="mandatorystar">*</span> Are all employees trained in safety requirements?
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <asp:RadioButtonList ID="rdoSafetytraining" runat="server" TabIndex="0" RepeatDirection="Horizontal" onclick="javascript:ShowHideRequiredContent();">
                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                        <%--<asp:RequiredFieldValidator ID="reqvSafetytraining" ControlToValidate="rdoSafetytraining"
                                                                                            Display="None" runat="server" SetFocusOnError="true" ValidationGroup="Questionair"
                                                                                            ErrorMessage="Please select a option in safety requirements" EnableClientScript="true">
                                                                                        </asp:RequiredFieldValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                                <%--G. Vera : 01/13/2016: Goes here--%>
                                                                                <tr id="trEmployeeProgram" runat="server" style="display: none">
                                                                                    <td colspan="2">
                                                                                        <span>
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td width="2%"></td>
                                                                                                    <td class="bodytextleft keyleft_border" width="50%"><span class="mandatorystar">* </span>If yes, can you provide documentation?
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:RadioButtonList ID="rdoListEmployeeProgramDocumentation" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="1%">
                                                                                    </td>
                                                                                    <td class="bodytextleft" width="70%">
                                                                                        <span class="mandatorystar">*</span> Do you have a Company Safety Director or other
                                                                                        Safety Professionals on Staff?
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <asp:RadioButtonList ID="rdoSafetyDirector" TabIndex="0" runat="server" RepeatDirection="Horizontal"
                                                                                            onclick="javascript:ShowContents();">
                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                        <asp:CustomValidator ID="cusSafetyDirector" runat="server" Display="None" OnServerValidate="cusSafetyDirector_ServerValidate"
                                                                                            ValidationGroup="Questionair" EnableClientScript="false"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="spnContact" runat="server" style="display: none">
                                                                                    <td colspan="2">
                                                                                        <span>
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                
                                                                                                <tr>
                                                                                                    <td width="2%">
                                                                                                    </td>
                                                                                                    <td class="bodytextleft keyleft_border" width="50%">
                                                                                                        Please Specify Contact Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" ID="txtContactName" runat="server" MaxLength="100"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="2%">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        Contact Phone Number &nbsp;
                                                                                                        <asp:CheckBox ID="chkContactUSA" runat="server" Text="Non-US" />
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox ID="txtMainPhno1" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                            MaxLength="3"></asp:TextBox>&nbsp; -
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftbxMainPhno1" runat="server" TargetControlID="txtMainPhno1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox ID="txtMainPhno2" runat="server" CssClass="txtboxsmall" MaxLength="3"></asp:TextBox>
                                                                                                        <span id="spnCPhone2" runat="server">-</span><Ajax:FilteredTextBoxExtender ID="ftbxMainPhno2"
                                                                                                            runat="server" TargetControlID="txtMainPhno2" FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox ID="txtMainPhno3" runat="server" CssClass="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftbxMainPhno3" runat="server" TargetControlID="txtMainPhno3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusPhone" runat="server" Display="None" OnServerValidate="cusPhone_ServerValidate"
                                                                                                            EnableClientScript="false" ValidationGroup="Questionair"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--G. Vera 12/14/2016:  Add new required question if yes is selected above--%>
                                                                                                <tr>
                                                                                                    <td width="2%"></td>
                                                                                                    <td class="bodytextleft keyleft_border" width="50%">Please provide the level of certification and training:
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxlarge" TabIndex="0" ID="txtProLevelOfTraining" runat="server"
                                                                                                            onFocus="javascript:textCounter(this,500,'yes');" onKeyDown="javascript:textCounter(this,500,'yes');"
                                                                                                            onKeyUp="javascript:textCounter(this,500,'yes');"
                                                                                                            TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                                <%--Sooraj 10/24/2013 - New Question added--%>
                                                                                <tr id="trNonUSSafetyQuestion" runat="server" style="display: ">
                                                                                    <td width="1%">
                                                                                    </td>
                                                                                    <td class="bodytextleft" width="70%">
                                                                                        <span class="mandatorystar">*</span> Are you able to provide onsite qualified Safety
                                                                                        Representation?
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <asp:RadioButtonList ID="rdoRepresentation" TabIndex="0" runat="server" RepeatDirection="Horizontal"
                                                                                            onclick="javascript:ShowContentsRepresentation();">
                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                        <asp:CustomValidator ID="cusSafetyRepresentation" runat="server" Display="None" OnServerValidate="cusSafetyRepresentation_ServerValidate"
                                                                                            ValidationGroup="Questionair" EnableClientScript="false"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="spnContactNonUS" runat="server" style="display: none">
                                                                                    <td colspan="2">
                                                                                        <span>
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td width="2%">
                                                                                                    </td>
                                                                                                    <td class="bodytextleft keyleft_border" width="50%">
                                                                                                        If Yes, Please Specify Contact Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" ID="txtRepresentationContactName" runat="server"
                                                                                                            MaxLength="100"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="2%">
                                                                                                    </td>
                                                                                                    <td class="bodytextleft keyleft_border">
                                                                                                        Contact Phone Number &nbsp;
                                                                                                        <asp:CheckBox ID="chkReperesentUSA" runat="server" Text="Non-US" />
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox ID="txtRepresentationContactNumberOne" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                            MaxLength="3"></asp:TextBox>&nbsp; -
                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtRepresentationContactNumberOne"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox ID="txtRepresentationContactNumberTwo" runat="server" CssClass="txtboxsmall"
                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                        &nbsp; <span id="spnRepresentPhone" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtRepresentationContactNumberTwo"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox ID="txtRepresentationContactNumberThree" runat="server" CssClass="txtboxsmall"
                                                                                                            MaxLength="4"> </asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtRepresentationContactNumberThree"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusPhoneNonUS" runat="server" Display="None" OnServerValidate="cusPhoneNonUS_ServerValidate"
                                                                                                            EnableClientScript="false" ValidationGroup="Questionair"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>

                                                                                <%--G. Vera 12/14/2016:  Add new required list of questions--%>
                                                                                <%--START--%>
                                                                                <tr>
                                                                                    <td width="1%"></td>
                                                                                    <td class="bodytextleft" width="70%">
                                                                                        <span class="mandatorystar">*</span> Has your firm been cited by an Occupational Safety & Health or Environmental Enforcement Agency for the following years?
                                                                                    </td>
                                                                                    <td class="bodytextleft">&nbsp;
                                                                                        <asp:CustomValidator ID="cusQuestionaire" runat="server" Display="None" OnServerValidate="cusQuestionaire_ServerValidate"
                                                                                            EnableClientScript="false" ValidationGroup="Questionair"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trCitationAttachment" runat="server">
                                                                                    <td colspan="2">
                                                                                        <span>
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%" style="padding-left: 30px">
                                                                                                <tr>
                                                                                                    <td class="bodytextleft" width="25%">
                                                                                                        <asp:Label Text="Year" runat="server" ID="lblCitationYear1" />
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="25%">

                                                                                                        <asp:RadioButtonList ID="rdoCitationYear1" TabIndex="0" runat="server" RepeatDirection="Horizontal"
                                                                                                            onclick="javascript:ShowHideRequiredContent();">
                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <%--TODO: Upload box--%>
                                                                                                        <%--G. Vera 06/19/2014: Added--%>
                                                                                                        <span id="tdAttachClick1" runat="server" style="display: none">
                                                                                                            <asp:LinkButton ID="lnkAttachCITATION1" runat="server" Text="(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)" CssClass="lnkAttach"
                                                                                                                OnClick="lnkAttach_Click" Visible="true" />

                                                                                                        </span>
                                                                                                        <asp:HiddenField ID="hdnCitationFile1" runat="server" />

                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft" width="25%">
                                                                                                        <asp:Label Text="Year" runat="server" ID="lblCitationYear2" />
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="25%">

                                                                                                        <asp:RadioButtonList ID="rdoCitationYear2" TabIndex="0" runat="server" RepeatDirection="Horizontal"
                                                                                                            onclick="javascript:ShowHideRequiredContent();">
                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <%--TODO: Upload box--%>
                                                                                                        <%--G. Vera 06/19/2014: Added--%>
                                                                                                        <span id="tdAttachClick2" runat="server" style="display: none">
                                                                                                            <asp:LinkButton ID="lnkAttachCITATION2" runat="server" Text="(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)" CssClass="lnkAttach"
                                                                                                                OnClick="lnkAttach_Click" Visible="true" /></span>
                                                                                                        <asp:HiddenField ID="hdnCitationFile2" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft" width="25%">
                                                                                                        <asp:Label Text="Year" runat="server" ID="lblCitationYear3" />
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="25%">

                                                                                                        <asp:RadioButtonList ID="rdoCitationYear3" TabIndex="0" runat="server" RepeatDirection="Horizontal"
                                                                                                            onclick="javascript:ShowHideRequiredContent();">
                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <%--TODO: Upload box--%>
                                                                                                        <%--G. Vera 06/19/2014: Added--%>
                                                                                                        <span id="tdAttachClick3" runat="server" style="display: none">
                                                                                                            <asp:LinkButton ID="lnkAttachCITATION3" runat="server" Text="(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)" CssClass="lnkAttach"
                                                                                                                OnClick="lnkAttach_Click" Visible="true" /></span>
                                                                                                        <asp:HiddenField ID="hdnCitationFile3" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                                <%--<tr>
                                                                                    <td width="1%"></td>
                                                                                    <td class="bodytextleft" width="70%">
                                                                                        <span class="mandatorystar">*</span> Do you have a new employee training program?
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <asp:RadioButtonList ID="rdoListEmployeeProgram" TabIndex="0" runat="server" RepeatDirection="Horizontal"
                                                                                            onclick="javascript:ShowHideRequiredContent();">
                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                    </td>
                                                                                </tr>--%>
                                                                                
                                                                                <tr>
                                                                                    <td width="1%"></td>
                                                                                    <td class="bodytextleft" width="70%">
                                                                                        <span class="mandatorystar">*</span> Do you have a written drug and alcohol program?
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <asp:RadioButtonList ID="rdoListDrugAlcohol" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="1%"></td>
                                                                                    <td class="bodytextleft" width="70%">
                                                                                        <span class="mandatorystar">*</span> Do you hold periodic safety meetings for your employees?
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <asp:RadioButtonList ID="rdoListSafetyMeetings" TabIndex="0" runat="server" RepeatDirection="Horizontal"
                                                                                            onclick="javascript:ShowHideRequiredContent();">
                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trSafetyMeetings" runat="server" style="display: none">
                                                                                    <td colspan="2">
                                                                                        <span>
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td width="2%"></td>
                                                                                                    <td class="bodytextleft keyleft_border" width="50%"><span class="mandatorystar">* </span>If yes, how often?
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:CheckBoxList runat="server" ID="chkSafetyMeetings" RepeatDirection="Horizontal" RepeatColumns="3">
                                                                                                            <asp:ListItem Text="Daily" />
                                                                                                            <asp:ListItem Text="Weekly" />
                                                                                                            <asp:ListItem Text="Bi Weekly" />
                                                                                                            <asp:ListItem Text="Monthly" />
                                                                                                            <asp:ListItem Text="Less often?" />
                                                                                                        </asp:CheckBoxList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="1%"></td>
                                                                                    <td class="bodytextleft" width="70%">
                                                                                        <span class="mandatorystar">*</span> Do you conduct field safety inspection of work in progress?
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <asp:RadioButtonList ID="rdoListInspectionWIP" TabIndex="0" runat="server" RepeatDirection="Horizontal"
                                                                                            onclick="javascript:ShowHideRequiredContent();">
                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trInspectionWIP" runat="server" style="display: none">
                                                                                    <td colspan="2">
                                                                                        <span>
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td width="2%"></td>
                                                                                                    <td class="bodytextleft keyleft_border" width="50%"><span class="mandatorystar">* </span>If Yes, who conducts the inspection?
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox runat="server" ID="txtWhoInspectionWIP" MaxLength="200" CssClass="txtbox" TabIndex="0" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td width="2%"></td>
                                                                                                    <td class="bodytextleft keyleft_border" width="50%"><span class="mandatorystar">* </span>If yes, how often?
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:CheckBoxList runat="server" ID="chkInspectionWIP" RepeatDirection="Horizontal" RepeatColumns="3">
                                                                                                            <asp:ListItem Text="Daily" />
                                                                                                            <asp:ListItem Text="Weekly" />
                                                                                                            <asp:ListItem Text="Bi Weekly" />
                                                                                                            <asp:ListItem Text="Monthly" />
                                                                                                            <asp:ListItem Text="Less often?" />
                                                                                                        </asp:CheckBoxList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="1%"></td>
                                                                                    <td class="bodytextleft" width="70%">
                                                                                        <span class="mandatorystar">*</span> Do you conduct Job Hazard Analysis or Task Hazard Analysis?
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <asp:RadioButtonList ID="rdoHazardAnalysis" TabIndex="0" runat="server" RepeatDirection="Horizontal"
                                                                                            onclick="javascript:ShowHideRequiredContent();">
                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trHazardAnalysis" runat="server" style="display: none">
                                                                                    <td colspan="2">
                                                                                        <span>
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td width="2%"></td>
                                                                                                    <td class="bodytextleft keyleft_border" width="50%"><span class="mandatorystar">* </span>If yes, how often?
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:CheckBoxList runat="server" ID="chkHazardAnalysisPeriods" RepeatDirection="Horizontal" RepeatColumns="3">
                                                                                                            <asp:ListItem Text="Daily" />
                                                                                                            <asp:ListItem Text="Weekly" />
                                                                                                            <asp:ListItem Text="Bi Weekly" />
                                                                                                            <asp:ListItem Text="Monthly" />
                                                                                                            <asp:ListItem Text="Less often?" />
                                                                                                        </asp:CheckBoxList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>

                                                                                <%--END--%>
                                                                                <%--G. Vera 12/14/2016: Add note at bottom of the questionnaire section--%>
                                                                                <tr>
                                                                                    <td colspan="8" class="bodytextboldBlue">Note:  Subcontractors are responsible for the safety management of tiered subcontractors under their control.
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </Content>
                                                                    </Ajax:AccordionPane>
                                                                </Panes>
                                                            </Ajax:Accordion>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <asp:HiddenField ID="hdnSafetyId" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td class="bodytextcenter">
                                                                <asp:ImageButton ID="imbSave" runat="server" CausesValidation="false" ImageUrl="~/Images/save.jpg"
                                                                    ToolTip="Save" TabIndex="0" OnClick="imbSave_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                            PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalPopupDiv" class="popupdivsmall" style="display: none; width: 500px;">
                                            <table cellpadding="0" cellspacing="0" border="0" width="500px" align="center">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="msgTd" colspan="3">
                                                        <asp:Label ID="lblheading" runat="server" class="searchhdrbarbold"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        <asp:Label ID="lblError" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                        <asp:Label ID="lblMsg" runat="server" CssClass="errormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl" align="center">
                                                        <input type="button" id="imbOk" value="OK" title="Ok" class="ModalPopupButton" onclick="$find('modalExtnd').hide();"
                                                            runat="server" />
                                                        <asp:ImageButton ID="imbOkoutlook" ImageAlign="AbsMiddle" ToolTip="OK" CausesValidation="false"
                                                            OnClientClick="TriggerOutlook();" ImageUrl="~/Images/ok.jpg" runat="server" OnClick="imbOkoutlook_Click"
                                                            Visible="False" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
<script src="../Script/jquery.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">

    //G. Vera 12/14/2016: Add document ready function to handle visibility on ready
    $(document).ready(function () {

        ShowContents();
        ShowHideRequiredContent();
    });

    //G. Vera 12/14/2016 - Add client script to handle the YesNo click
    function ShowHideRequiredContent() {

        // Citations question
        if (document.getElementById("<%=rdoCitationYear1.ClientID%>_0").checked) {
            document.getElementById("<%=tdAttachClick1.ClientID%>").style.display = "";
            if ($("#" + '<%=hdnCitationFile1.ClientID%>').val() != "") {
                $("#" + '<%=lnkAttachCITATION1.ClientID%>').html("Edit (" + $("#" + '<%=hdnCitationFile1.ClientID%>').val() + ")");
            }
            else { $("#" + '<%=lnkAttachCITATION1.ClientID%>').html("(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)") };
        }
        else if (document.getElementById("<%=rdoCitationYear1.ClientID%>_1").checked) {
            document.getElementById("<%=tdAttachClick1.ClientID%>").style.display = "none";
        }
    if (document.getElementById("<%=rdoCitationYear2.ClientID%>_0").checked) {
            document.getElementById("<%=tdAttachClick2.ClientID%>").style.display = "";
            if ($("#" + '<%=hdnCitationFile2.ClientID%>').val() != "") {
                $("#" + '<%=lnkAttachCITATION2.ClientID%>').html("Edit (" + $("#" + '<%=hdnCitationFile2.ClientID%>').val() + ")");
            }
            else { $("#" + '<%=lnkAttachCITATION2.ClientID%>').html("(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)") };
        }
        else if (document.getElementById("<%=rdoCitationYear2.ClientID%>_1").checked) {
            document.getElementById("<%=tdAttachClick2.ClientID%>").style.display = "none";
        }
    if (document.getElementById("<%=rdoCitationYear3.ClientID%>_0").checked) {
            document.getElementById("<%=tdAttachClick3.ClientID%>").style.display = "";
            if ($("#" + '<%=hdnCitationFile3.ClientID%>').val() != "") {
                $("#" + '<%=lnkAttachCITATION3.ClientID%>').html("Edit (" + $("#" + '<%=hdnCitationFile3.ClientID%>').val() + ")");
            }
            else { $("#" + '<%=lnkAttachCITATION3.ClientID%>').html("(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)") };
        }
        else if (document.getElementById("<%=rdoCitationYear3.ClientID%>_1").checked) {
            document.getElementById("<%=tdAttachClick3.ClientID%>").style.display = "none";
        }

        //Employee training
        if (document.getElementById("<%=rdoSafetytraining.ClientID%>_0").checked) {
            document.getElementById("<%=trEmployeeProgram.ClientID%>").style.display = "";
        }
        else if (document.getElementById("<%=rdoSafetytraining.ClientID%>_1").checked) {
            document.getElementById("<%=trEmployeeProgram.ClientID%>").style.display = "none";
            document.getElementById("<%=rdoListEmployeeProgramDocumentation.ClientID%>_0").checked = false;
            document.getElementById("<%=rdoListEmployeeProgramDocumentation.ClientID%>_1").checked = false;
        }

        //Employee Meetings
    if (document.getElementById("<%=rdoListSafetyMeetings.ClientID%>_0").checked) {
            document.getElementById("<%=trSafetyMeetings.ClientID%>").style.display = "";
        }
        else if (document.getElementById("<%=rdoListSafetyMeetings.ClientID%>_1").checked) {
            document.getElementById("<%=trSafetyMeetings.ClientID%>").style.display = "none";
            document.getElementById("<%=chkSafetyMeetings.ClientID%>_0").checked = false;
            document.getElementById("<%=chkSafetyMeetings.ClientID%>_1").checked = false;
            document.getElementById("<%=chkSafetyMeetings.ClientID%>_2").checked = false;
            document.getElementById("<%=chkSafetyMeetings.ClientID%>_3").checked = false;
            document.getElementById("<%=chkSafetyMeetings.ClientID%>_4").checked = false;

        }

        //Inspection WIP
        //trInspectionWIP
        //Employee Meetings
    if (document.getElementById("<%=rdoListInspectionWIP.ClientID%>_0").checked) {
            document.getElementById("<%=trInspectionWIP.ClientID%>").style.display = "";
        }
        else if (document.getElementById("<%=rdoListInspectionWIP.ClientID%>_1").checked) {
            document.getElementById("<%=trInspectionWIP.ClientID%>").style.display = "none";
        document.getElementById("<%=chkInspectionWIP.ClientID%>_0").checked = false;
        document.getElementById("<%=chkInspectionWIP.ClientID%>_1").checked = false;
        document.getElementById("<%=chkInspectionWIP.ClientID%>_2").checked = false;
        document.getElementById("<%=chkInspectionWIP.ClientID%>_3").checked = false;
        document.getElementById("<%=chkInspectionWIP.ClientID%>_4").checked = false;

        }

    //Hazard analaysis
    if (document.getElementById("<%=rdoHazardAnalysis.ClientID%>_0").checked) {
        document.getElementById("<%=trHazardAnalysis.ClientID%>").style.display = "";
    }
    else if (document.getElementById("<%=rdoHazardAnalysis.ClientID%>_1").checked) {
        document.getElementById("<%=trHazardAnalysis.ClientID%>").style.display = "none";
        document.getElementById("<%=chkHazardAnalysisPeriods.ClientID%>_0").checked = false;
        document.getElementById("<%=chkHazardAnalysisPeriods.ClientID%>_1").checked = false;
        document.getElementById("<%=chkHazardAnalysisPeriods.ClientID%>_2").checked = false;
        document.getElementById("<%=chkHazardAnalysisPeriods.ClientID%>_3").checked = false;
        document.getElementById("<%=chkHazardAnalysisPeriods.ClientID%>_4").checked = false;

    }
}

    function DisplayOSHAExplain(chkStatus, txtDeaths, txtCases, txtTransfer, txtRecords, trExplain, txtExplain, txtTotalHours) {
        if (chkStatus == true) {
            document.getElementById(trExplain).style.display = "";
            document.getElementById(txtDeaths).value = "";
            document.getElementById(txtDeaths).disabled = true;
            document.getElementById(txtCases).value = "";
            document.getElementById(txtCases).disabled = true;
            document.getElementById(txtTransfer).value = "";
            document.getElementById(txtTransfer).disabled = true;
            document.getElementById(txtRecords).value = "";
            document.getElementById(txtRecords).disabled = true;
            document.getElementById(txtTotalHours).value = "";
            document.getElementById(txtTotalHours).disabled = true;
        }
        else {

            document.getElementById(trExplain).style.display = "None";
            document.getElementById(txtDeaths).disabled = false;
            document.getElementById(txtCases).disabled = false;
            document.getElementById(txtTransfer).disabled = false;
            document.getElementById(txtRecords).disabled = false;
            document.getElementById(txtExplain).value = "";
            document.getElementById(txtTotalHours).value = "";
            document.getElementById(txtTotalHours).disabled = false;
        }
    }
    function DisplayExplain(chkStatus, txtRate, trExplain, txtExplain) {
        if (chkStatus == true) {
            document.getElementById(trExplain).style.display = "";
            document.getElementById(txtRate).value = "";
            ValidatorEnable(document.getElementById("<%=rangRateOne.ClientID%>"), false);
            ValidatorEnable(document.getElementById("<%=rangRateTwo.ClientID%>"), false);
            ValidatorEnable(document.getElementById("<%=rangRateThree.ClientID%>"), false);
            document.getElementById(txtRate).disabled = true;
        }
        else {
            document.getElementById(trExplain).style.display = "None";
            ValidatorEnable(document.getElementById("<%=rangRateOne.ClientID%>"), true);
            ValidatorEnable(document.getElementById("<%=rangRateTwo.ClientID%>"), true);
            ValidatorEnable(document.getElementById("<%=rangRateThree.ClientID%>"), true);
            document.getElementById(txtRate).disabled = false;
            document.getElementById(txtExplain).value = "";
        }
    }

    //G. Vera 06/13/2014 - added
    function DisplayAttachment(trAttachment, checked) {
        if (checked) {
            document.getElementById(trAttachment).style.display = "none";
            
        }
        else {
            document.getElementById(trAttachment).style.display = "";

        }
    }

    //G. Vera - Added 01/30/2013
    function DisplayYear4(chkStatus, cleintID) {
        if (chkStatus == true) {
            document.getElementById(cleintID).style.display = "";
        }
        else {
            document.getElementById(cleintID).style.display = "none";
        }
    }

    function OpenPrint() {
        window.open("../VQFView/SafetyView.aspx?Print=1");
    }
    function checkDecimal(txtBox1) {
        var str2 = document.getElementById(txtBox1).value;
        if (str2.length != 0) {
            document.getElementById(txtBox1).value = roundNumber(str2, 2);
        }
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
            return false;
        return true;
    }
    function ShowContents() {

        //spnContact
        if (document.getElementById("<%=rdoSafetyDirector.ClientID%>_0").checked) {
            document.getElementById("<%=spnContact.ClientID%>").style.display = "";
        }
        else if (document.getElementById("<%=rdoSafetyDirector.ClientID%>_1").checked) {
            document.getElementById("<%=spnContact.ClientID%>").style.display = "none";
            document.getElementById("<%=txtContactName.ClientID%>").value = "";
            document.getElementById("<%=txtMainPhno1.ClientID%>").value = "";
            document.getElementById("<%=txtMainPhno2.ClientID%>").value = "";
            document.getElementById("<%=txtMainPhno3.ClientID%>").value = "";
        }
    }
    function ShowContentsRepresentation() {
        var txt1 = document.getElementById("<%=txtRepresentationContactNumberOne.ClientID%>");
        var txt2 = document.getElementById("<%=txtRepresentationContactNumberTwo.ClientID%>");
        var txt3 = document.getElementById("<%=txtRepresentationContactNumberThree.ClientID%>");
        var txt4 = document.getElementById("<%=txtRepresentationContactName.ClientID%>");

        if (txt1 != null)
            txt1.value = "";
        if (txt2 != null)
            txt2.value = "";
        if (txt3 != null)
            txt3.value = "";
        if (txt4 != null)
            txt4.value = "";

        if (document.getElementById("<%=rdoRepresentation.ClientID%>_0").checked) {
            document.getElementById("<%=spnContactNonUS.ClientID%>").style.display = "";
        }
        else if (document.getElementById("<%=rdoRepresentation.ClientID%>_1").checked) {
            document.getElementById("<%=spnContactNonUS.ClientID%>").style.display = "none";
        }
    }
    function Close() {
        var x = $find("ModalAddDoc");
        if (x) { x.hide(); }
    }
    function numberRateFormat(nStr, prefix, ctrl) {
        var txt = document.getElementById(ctrl);
        if (txt.value != "0") {
            var prefix = prefix || '';
            nStr += '';
            nStr = nStr.replace(/^[0]+/g, "");

            y = nStr.split(',');
            var y1 = '';
            for (var i = 0; i < y.length; i++) {
                y1 += y[i];
            }
            if (y.length > 0) {
                nStr = y1;
            }
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1))
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            var txt = document.getElementById(ctrl);
            txt.value = x1 + x2;
        }
        if (txt.value.length != 0) {
            txt.value = roundNumber(txt.value, 2);
        }
    }
    function numberEMRFormat(nStr, prefix, ctrl) {
        var txt = document.getElementById(ctrl);
        if (txt.value != "0") {
            var prefix = prefix || '';
            nStr += '';
            nStr = nStr.replace(/^[0]+/g, "");
            y = nStr.split(',');
            var y1 = '';
            for (var i = 0; i < y.length; i++) {
                y1 += y[i];
            }
            if (y.length > 0) {
                nStr = y1;
            }
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1))
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            var txt = document.getElementById(ctrl);
            txt.value = x1 + x2;
        }
    }
    function numberFormat(nStr, prefix, ctrl) {
        var txt = document.getElementById(ctrl);
        if (txt.value != "0") {
            var prefix = prefix || '';
            nStr += '';
            nStr = nStr.replace(/^[0]+/g, "");
            y = nStr.split(',');
            var y1 = '';
            for (var i = 0; i < y.length; i++) {
                y1 += y[i];
            }
            if (y.length > 0) {
                nStr = y1;
            }
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1))
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            var txt = document.getElementById(ctrl);
            txt.value = x1 + x2;
        }
    }
    function validateRateRange(srcElem, year) {
        var msg = "";
        if (document.getElementById("<%=txtRateOne.ClientID%>").value != "") {
            if (!(parseFloat(document.getElementById("<%=txtRateOne.ClientID%>").value) >= 0 && parseFloat(document.getElementById("<%=txtRateOne.ClientID%>").value) <= 3)) {
                msg = msg + "<li>Please enter rate under year one range between 0 to 3</li>";
            }
        }
        if (document.getElementById("<%=txtRateTwo.ClientID%>").value != "") {
            if (!(parseFloat(document.getElementById("<%=txtRateTwo.ClientID%>").value) >= 0 && parseFloat(document.getElementById("<%=txtRateTwo.ClientID%>").value) <= 3)) {
                msg = msg + "<li>Please enter rate under year two range between 0 to 3</li>";
            }
        }
        if (document.getElementById("<%=txtRateThree.ClientID%>").value != "") {
            if (!(parseFloat(document.getElementById("<%=txtRateThree.ClientID%>").value) >= 0 && parseFloat(document.getElementById("<%=txtRateThree.ClientID%>").value) <= 3)) {
                msg = msg + "<li>Please enter rate under year three range between 0 to 3</li>";
            }
        }
        if (msg != "") {
            document.getElementById("<%=lblMessage.ClientID%>").style.display = "";
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = msg;
            document.getElementById("trMessage").style.display = "";
        }
        else {
            document.getElementById("<%=lblMessage.ClientID%>").style.display = "none";
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = "";
            document.getElementById("trMessage").style.display = "none";
        }
    }
    function SwitchPhone(val, phone1, phone2, phone3, spnPhone) {

        var p1 = document.getElementById(phone1);
        var p2 = document.getElementById(phone2);
        var p3 = document.getElementById(phone3);

        var s1 = document.getElementById(spnPhone);


        if (val.checked) {
            //val.nextSibling.innerHTML = "Non-US";
            p3.style.display = "none";
            p1.maxLength = 5;
            p2.maxLength = 15;
            p2.className = "txtbox";
            p3.value = "";
            s1.style.display = "none";
            // G. Vera 05/13/2014: Changed
            p1.onkeyup = "";
            p2.onkeyup = "";
            //p1.attributes.removeNamedItem("onkeyup");
            //p2.attributes.removeNamedItem("onkeyup");
        }
        else {
            //val.nextSibling.innerHTML = "Non-US";
            p3.style.display = "";
            p2.className = "txtboxsmall";
            p1.maxLength = 3;
            p2.maxLength = 3;

            s1.style.display = "";

            p1.setAttribute("onkeyup", "SetFocusOnPhone(" + phone1 + "," + phone2 + ")");
            p2.setAttribute("onkeyup", "SetFocusOnPhone(" + phone2 + "," + phone3 + ")");
        }
        p3.value = "";
        p2.value = "";
        p1.value = "";
    }
</script>
