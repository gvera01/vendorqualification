﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Text;
using VMSDAL;
using System.Web.UI.HtmlControls;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 10/19/2012: VMS Stabilization Release 1.3 (see JIRA Issues for more details)
 * 003 Sooraj Sudhakaran.T 09/24/2013: VMS Modification - Added code to hide section that do not apply to design consultant
 * 004 G. Vera 07/08/2014: Remove all validation from Admin VQF screens
 ****************************************************************************************************************/
#endregion

public partial class VQF_InsurancesBonding : CommonPage
{

    #region Declaration

    bool? AdditionalInsured;
    bool EditData;
    clsCompany objCompany = new clsCompany();
    Common ObjCommon = new Common();

    long VendorId, InsuranceId;
    DataSet Attch = new DataSet();
    clsInsurance objInsurance = new clsInsurance();
    public DataSet Vendordetails = new DataSet();
    public int CountInsurance;

    Byte InsuranceStatus;


    public VMSDAL.VQFInsurance EditDate;            //002
    public VMSDAL.VQFInsurance UpdateInsuranceData; //002

    public const string Bond = "INSURANCE AND BONDING";
    public const string Break = "</BR></BR>";
    StringBuilder CompareBody = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
    StringBuilder CompareMainBody = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
    string ContactPart = "";
    string SurityPart = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])) || Convert.ToString(Session["VendorAdminId"]) == "0")
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
        }
        else
        {
            //001
            if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");
            }
            VendorId = Convert.ToInt64(Session["VendorAdminId"]);
            if (!Page.IsPostBack)
            {
                //001
                Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

                HiddenField outlookContent = VQFEditSideMenu.FindControl("hdnOutllook") as HiddenField;
                if (Session["OUTLOOKDATA"] != null)
                {
                    outlookContent.Value = Session["OUTLOOKDATA"].ToString();

                }
                CountInsurance = objInsurance.GetInsuranceStatusCount(VendorId);
                //  VendorId = VendorId;
                Vendordetails = ObjCommon.GetVendorDetails(VendorId);
                //Show or hide design consultant view based on Type of company - 003-sooraj
                SwitchDesignConsultantView(Vendordetails.Tables[0].Rows[0]["TypeOfCompany"].ConvertToString() == TypeOfCompany.DesignConsultant);
               

                lblwelcome.Text = Vendordetails.Tables[0].Rows[0]["Company"].ToString();

                HiddenField outlookContentVendorName = VQFEditSideMenu.FindControl("hdnOutllookVendorName") as HiddenField;
                outlookContentVendorName.Value = "Dear " + lblwelcome.Text.Replace("&", "AND") + ",%0A";
                HiddenField outlookContentCompanyName = VQFEditSideMenu.FindControl("hdnOutllookCompanyName") as HiddenField;
                outlookContentCompanyName.Value = lblwelcome.Text.Replace("&", "AND") + ",%0A";
                HiddenField outlookVendorEmail = VQFEditSideMenu.FindControl("hdnOutllookVendorEmail") as HiddenField;
                outlookVendorEmail.Value = Session["OUTLOOKTO"].ToString();

                LoadEditDatas(true);
                //CallScripts();
                ShowHideControls();
                if (objCompany.GetSingleVQFCompanyData(VendorId) != null)
                {
                    ViewState["BusinessType"] = objCompany.GetSingleVQFCompanyData(VendorId).TypeOfCompany;
                }
                else
                {
                    ViewState["BusinessType"] = null;
                }

                //002 - added below
                string businessType = (ViewState["BusinessType"] != null) ? ViewState["BusinessType"].ToString() : string.Empty;
                RequiredFieldDisplay(businessType);
            }
        }

        //002 - added below:
        if (rdoMarineCargoInlandTransit.SelectedValue == "1") trPerConveyanceLimitInlandTrans.Attributes["style"] = "display: ";
        else trPerConveyanceLimitInlandTrans.Attributes["style"] = "display: none";
        if (rdoTransitIns.SelectedValue == "1") trPerConveyanceLimitMarineIns.Attributes["style"] = "display: ";
        else trPerConveyanceLimitMarineIns.Attributes["style"] = "display: none";
        CallScripts();

        VQFEditSideMenu.GetVendorStatus();
        hdnVendorStatus.Value = Convert.ToString(VQFEditSideMenu.VendorStatus);
        if (VQFEditSideMenu.VendorStatus == 1 || VQFEditSideMenu.VendorStatus == 2)
        {

            imbSaveNext.Visible = false;
            // imbSave.Visible = false;  
            ImageButton imgAttach = (ImageButton)VQFEditSideMenu.FindControl("imbAttch");
            imgAttach.Visible = true;


        }
        if (rblProvideamt.SelectedIndex >= 0)
        {
            trnotes.Style["display"] = rblProvideamt.SelectedIndex == 0 ? "" : "none";
        }

        if (rblUmbrellaExcessliability.Items[0].Selected)
        {
            trExcess.Style["display"] = "";
            trUmbrella.Style["display"] = "";
        }
        else
        {
            trExcess.Style["display"] = "None";
            trUmbrella.Style["display"] = "None";
        }
        SwitchPhoneForKeyContact(null);//005
    }

    #region Button Events

    /// <summary>
    /// Save and Next
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSaveNext_Click(object sender, ImageClickEventArgs e)
    {
        // VendorId = VendorId;
        //VendorId = 10;

        valSummary.ValidationGroup = "ContactInformation";
        string AgentTelephoneNumber = ObjCommon.Phoneformat(txtAgentTelephoneNo1.Text.Trim(), txtAgentTelephoneNo2.Text.Trim(), txtAgentTelephoneNo3.Text.Trim(),txtAgentTelephoneNo3);//005
        string ContactTelephoneNumber = ObjCommon.Phoneformat(txtContactTelephoneNo1.Text.Trim(), txtContactTelephoneNo2.Text.Trim(), txtContactTelephoneNo3.Text.Trim(), txtContactTelephoneNo3);//005
        //002 - added below
        bool? isMarineCargoIns = (!string.IsNullOrEmpty(rdoTransitIns.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoTransitIns.SelectedValue)) : (bool?)null;
        bool? isInlandTransit = (!string.IsNullOrEmpty(rdoMarineCargoInlandTransit.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoInlandTransit.SelectedValue)) : (bool?)null;
        bool? isInstallEquip = (!string.IsNullOrEmpty(rdoMarineCargoInstEquip.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoInstEquip.SelectedValue)) : (bool?)null;
        bool? isMarinceCargoStorage = (!string.IsNullOrEmpty(rdoMarineCargoStorage.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoStorage.SelectedValue)) : (bool?)null;
        txtMarineCargoConvLimit.Text = (isMarineCargoIns == false || isMarineCargoIns == null) ? "" : txtMarineCargoConvLimit.Text;
        txtPerConveyanceLimitInlandTrans.Text = (isInlandTransit == false || isInlandTransit == null) ? "" : txtPerConveyanceLimitInlandTrans.Text;

        string convLimit = txtMarineCargoConvLimit.Text;
        string convLimitInlandTrans = txtPerConveyanceLimitInlandTrans.Text;

        reqvMarineCargoConvLimit.Enabled = (isMarineCargoIns != null) ? (bool)isMarineCargoIns : false;
        reqvConvLimitInlandTrans.Enabled = (isInlandTransit != null) ? (bool)isInlandTransit : false;
        spanPollutionLiabExplainStar.Visible = txtPollutionLiability.Text == "0";
        rqfvPollutionLiabExplain.Enabled = txtPollutionLiability.Text == "0";

        //002 - End

        bool Result = false;
        Page.Validate("ContactInformation");
        if (Page.IsValid)
        {
            valSummary.ValidationGroup = "AggregateLimits";
            Page.Validate("AggregateLimits");

            if (Page.IsValid)
            {
                valSummary.ValidationGroup = "Bonding";
                Page.Validate("Bonding");
                if (Page.IsValid)
                {
                    if (!(rblProvideamt.SelectedIndex < 0))
                    {
                        if (rblProvideamt.SelectedIndex == 0)
                        {
                            AdditionalInsured = true;
                        }
                        else if (rblProvideamt.SelectedIndex == 1)
                        {
                            AdditionalInsured = false;
                        }
                    }
                    else
                    {
                        AdditionalInsured = null;
                    }

                    int CountInsurance = objInsurance.GetInsuranceCount(VendorId);
                    if (CountInsurance == 0)
                    {
                        InsuranceStatus = 1;
                        Result = objInsurance.InsertInsurance(txtBrokerorCompanyName.Text.Trim(), txtInsuranceAgent.Text.Trim(), AgentTelephoneNumber, txtInsuranceCarrier.Text.Trim(), 
                            txtEachOccuranceAmt1.Text.Trim(), txtOperationsAggregeteAmt.Text.Trim(), txtGeneralAggregateAmt.Text.Trim(), AdditionalInsured, txtAccidentamt.Text.Trim(), 
                            txtEmpLiabilityAmt.Text.Trim(), txtDiseaseEmpAmt.Text.Trim(), txtDiseasePolicyAmtone.Text.Trim(), txtEachOccuranceAmt2.Text.Trim(), 
                            txtBondingSuretyCompanyNameone.Text.Trim(), txtCompanyContactName.Text.Trim(), ContactTelephoneNumber, txtTotalBondingCapacityAmt.Text.Trim(), 
                            txtCurrentBondingCapacityAmt.Text.Trim(), VendorId, InsuranceStatus, rblOccurrenceBased.Items[0].Selected, rblGeneralAggregateLimit.Items[0].Selected,
                            rblUmbrellaExcessliability.Items[0].Selected, txtUmbrella.Text, txtExcess.Text, rblStatutoryLimits.Items[0].Selected, isMarineCargoIns, isInlandTransit, 
                            isInstallEquip, isMarinceCargoStorage, convLimit, convLimitInlandTrans, !chkAgentUSA.Checked, !chkContactUSA.Checked, txtPollutionLiability.Text, txtPollutionLiabExplain.Text);//005
                        Session["Flag"] = 1;
                    }
                    else
                    {
                        string OutlookString = string.Empty;
                        InsuranceStatus = 2;
                        if (!string.IsNullOrEmpty(hdnInsuranceId.Value))
                        {
                            InsuranceId = Convert.ToInt64(hdnInsuranceId.Value);

                            //COMPARE
                            EditDate = objInsurance.GetSingleVQLInsuranceDate(VendorId);

                            Result = objInsurance.UpdateInsurance(txtBrokerorCompanyName.Text.Trim(), txtInsuranceAgent.Text.Trim(), AgentTelephoneNumber, txtInsuranceCarrier.Text.Trim(), txtEachOccuranceAmt1.Text.Trim(), 
                                txtOperationsAggregeteAmt.Text.Trim(), txtGeneralAggregateAmt.Text.Trim(), AdditionalInsured, txtAccidentamt.Text.Trim(), "", 
                                txtEmpLiabilityAmt.Text.Trim(), txtDiseaseEmpAmt.Text.Trim(), txtDiseasePolicyAmtone.Text.Trim(), txtEachOccuranceAmt2.Text.Trim(), 
                                txtBondingSuretyCompanyNameone.Text.Trim(), txtCompanyContactName.Text.Trim(), ContactTelephoneNumber, txtTotalBondingCapacityAmt.Text.Trim(), 
                                txtCurrentBondingCapacityAmt.Text.Trim(), InsuranceId, InsuranceStatus, rblOccurrenceBased.Items[0].Selected, rblGeneralAggregateLimit.Items[0].Selected, 
                                rblUmbrellaExcessliability.Items[0].Selected, txtUmbrella.Text, txtExcess.Text, rblStatutoryLimits.Items[0].Selected
                                , isMarineCargoIns, isInlandTransit, isInstallEquip, isMarinceCargoStorage, convLimit, convLimitInlandTrans, !chkAgentUSA.Checked, !chkContactUSA.Checked, txtPollutionLiability.Text, txtPollutionLiabExplain.Text);//005

                            UpdateInsuranceData = objInsurance.GetSingleVQLInsuranceDate(VendorId);
                            ObjCommon.CompareChangeConsolidated = string.Empty;
                            ObjCommon.CompareChanges = Compare.CompareInsurance(EditDate, UpdateInsuranceData);
                            bool UmbrellaExcessliability = false;
                            if (rblUmbrellaExcessliability.SelectedIndex == 0)
                                UmbrellaExcessliability = true;
                            else if (rblUmbrellaExcessliability.SelectedIndex == 1)
                                UmbrellaExcessliability = false;
                            if (ObjCommon.CompareChanges != string.Empty)
                            {
                                ObjCommon.CompareChangeConsolidated += Bond + Break + ObjCommon.CompareChanges + Break;
                                string[] a = ObjCommon.CompareChanges.Split(';');
                                int k = 0;
                                ObjCommon.CompareChanges = string.Empty;
                                while (k < a.Length - 1)
                                {
                                    //    if (a[k] == "Broker_CompanyName")
                                    //        ContactPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                    //    else if (a[k] == "InsuranceAgent")
                                    //        ContactPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                    //    else if (a[k] == "AgentTelephoneNumber")
                                    //        ContactPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                    //    else if (a[k] == "InsuranceCarrier")
                                    //        ContactPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                    //    else if (a[k] == "Bonding_SuretyCompanyName")
                                    //        SurityPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                    //    else if (a[k] == "CompanyContactName")
                                    //        SurityPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                    //    else if (a[k] == "ContactTelephoneNumber")
                                    //        SurityPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                    //    else if (a[k] == "TotalBondingCapacity")
                                    //        SurityPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                    //    else if (a[k] == "CurrentBondingCapacity")
                                    //        SurityPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                    //    else 
                                    //        ObjCommon.CompareChanges += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";

                                    if (a[k].Length > 23 && a[k].Substring(6, "Broker_CompanyName".Length) == "Broker_CompanyName")
                                        ContactPart += "%09" + a[k] + "%0A";
                                    else if (a[k].Length > 19 && a[k].Substring(6, "InsuranceAgent".Length) == "InsuranceAgent")
                                        ContactPart += "%09" + a[k] + "%0A";
                                    else if (a[k].Length > 24 && a[k].Substring(6, "AgentTelephoneNumber".Length) == "AgentTelephoneNumber")
                                        ContactPart += "%09" + a[k] + "%0A";
                                    else if (a[k].Length > 21 && a[k].Substring(6, "InsuranceCarrier".Length) == "InsuranceCarrier")
                                        ContactPart += "%09" + a[k] + "%0A";
                                    else if (a[k].Length > 30 && a[k].Substring(6, "Bonding_SuretyCompanyName".Length) == "Bonding Surety Company Name") //Bonding_SuretyCompanyName
                                        SurityPart += "%09" + a[k] + "%0A";
                                    else if (a[k].Length > 23 && a[k].Substring(6, "CompanyContactName".Length) == "CompanyContactName")
                                        SurityPart += "%09" + a[k] + "%0A";
                                    else if (a[k].Length > 27 && a[k].Substring(6, "ContactTelephoneNumber".Length) == "ContactTelephoneNumber")
                                        SurityPart += "%09" + a[k] + "%0A";
                                    else if (a[k].Length > 25 && a[k].Substring(6, "TotalBondingCapacity".Length) == "TotalBondingCapacity")
                                        SurityPart += "%09" + a[k] + "%0A";
                                    else if (a[k].Length > 29 && a[k].Substring(6, "AvailableBondingCapacity".Length) == "AvailableBondingCapacity")
                                        SurityPart += "%09" + a[k] + "%0A";
                                    else if ((a[k].Length > 20 && a[k].Substring(6, a[k].IndexOf(' ') - 6) == "Umbrella_Excess") || (a[k].Length > 13 && a[k].Substring(6, 8) == "Umbrella") || (a[k].Length > 11 && a[k].Substring(6, "Excess".Length) == "Excess"))
                                    {
                                        if (EditDate.Umbrella_Excess != UmbrellaExcessliability)
                                        {
                                            if (a[k].Length > 20 && a[k].Substring(6, "Umbrella_Excess".Length) == "Umbrella_Excess")
                                            {
                                                ObjCommon.CompareChanges += "%09" + a[k] + "%0A";
                                            }
                                            else if (a[k].Length > 13 && a[k].Substring(6, "Umbrella".Length) == "Umbrella")
                                            {
                                                if (rblUmbrellaExcessliability.SelectedIndex == 0)
                                                {
                                                    ObjCommon.CompareChanges += "%09" + "Field:Umbrella Data=" + txtUmbrella.Text.Trim() + "%0A";
                                                }
                                                else if (rblUmbrellaExcessliability.SelectedIndex == 1)
                                                {
                                                    ObjCommon.CompareChanges += "Deleted Field:Umbrella Data=" + EditDate.Umbrella.ToString() + ",";
                                                }
                                            }
                                            else if (a[k].Length > 11 && a[k].Substring(6, "Excess".Length) == "Excess")
                                            {
                                                if (rblUmbrellaExcessliability.SelectedIndex == 0)
                                                {
                                                    ObjCommon.CompareChanges += "%09" + "Field:Excess Data=" + txtExcess.Text.Trim() + "%0A";
                                                }
                                                else if (rblUmbrellaExcessliability.SelectedIndex == 1)
                                                {
                                                    ObjCommon.CompareChanges += "Deleted Field:Excess Data=" + EditDate.Excess.ToString() + "%0A";
                                                }
                                            }

                                        }
                                        else
                                        {
                                            ObjCommon.CompareChanges += "%09" + a[k] + "%0A";
                                        }
                                    }
                                    else if (a[k].Length > 20 && a[k].Substring(6, "InsuranceStatus".Length) == "InsuranceStatus")
                                    {
                                        ObjCommon.CompareChanges += "";
                                    }
                                    else
                                        ObjCommon.CompareChanges += "%09" + a[k] + "%0A";
                                    k++;
                                }

                            }
                            if (ContactPart != string.Empty)
                            {
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>Contact Information</u></b><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append(ContactPart);
                                OutlookString += "%0AContact Information" + "%0A" + ContactPart;

                            }
                            if (ObjCommon.CompareChanges != string.Empty)
                            {
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>Aggregate Limits / Coverage </u></b><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append(ObjCommon.CompareChanges);
                                OutlookString += "%0AAggregate Limits / Coverage " + "%0A" + ObjCommon.CompareChanges;
                            }
                            if (SurityPart != string.Empty)
                            {
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>Bonding and Surety</u></b><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append(SurityPart);
                                OutlookString += "%0ABonding and Surety" + "%0A" + SurityPart;
                            }
                            if (ObjCommon.CompareChangeConsolidated != string.Empty)
                            {
                                //ObjCommon.CompareChangeOutlook += ObjCommon.CompareChangeConsolidated;
                                //CompareBody.Append("</table>");
                                //CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='4'><b><u>" + Bond + "</u></b><br></font></td></tr>");
                                //CompareMainBody.Append("</table>");
                                //CompareMainBody.Append(CompareBody);
                                OutlookString = Bond + "%0A" + OutlookString;
                            }
                            //Session["OUTLOOKDATA"] = Session["OUTLOOKDATA"].ToString() + CompareMainBody;
                            Session["OUTLOOKDATA"] = Session["OUTLOOKDATA"].ToString() + "%0A" + OutlookString;
                            HiddenField outlookContent = VQFEditSideMenu.FindControl("hdnOutllook") as HiddenField;
                            outlookContent.Value = Session["OUTLOOKDATA"].ToString();
                            HiddenField outlookVendorEmail = VQFEditSideMenu.FindControl("hdnOutllookVendorEmail") as HiddenField;
                            outlookVendorEmail.Value = Session["OUTLOOKTO"].ToString();
                        }
                    }
                    //Get InsuranceID
                    if (Result == true)
                    {
                        VMSDAL.VQFInsurance EditInsuranceData = objInsurance.GetSingleVQLInsuranceDate(VendorId);
                        hdnInsuranceId.Value = Convert.ToString(EditInsuranceData.PK_InsuranceID);
                        if (!string.IsNullOrEmpty(hdnInsuranceId.Value))
                        {
                            InsuranceId = Convert.ToInt64(hdnInsuranceId.Value);

                            VQFEditSideMenu.GetCompleteStatus();
                            VQFEditSideMenu.GetIncompleteStatus();
                        }
                    }
                }
                else
                {
                    lblheading.Text = "Bonding and Surety - Validation";
                    lblMsg.Text = "";
                    lblError.Text = "";
                    modalExtnd.Show();
                    accBonding.SelectedIndex = 2;
                }
            }
            else
            {
                lblheading.Text = "Aggregate Limits / Coverage - Validation";
                lblMsg.Text = "";
                lblError.Text = "";
                modalExtnd.Show();
                accBonding.SelectedIndex = 1;
            }
        }
        else
        {
            lblheading.Text = "Contact Information - Validation";
            lblMsg.Text = "";
            lblError.Text = "";
            modalExtnd.Show();
            accBonding.SelectedIndex = 0;
        }
    }

    protected void imbSave_Click(object sender, ImageClickEventArgs e)
    {
        //004 - valSummary.ValidationGroup = "ContactInformation";
        string AgentTelephoneNumber = ObjCommon.Phoneformat(txtAgentTelephoneNo1.Text.Trim(), txtAgentTelephoneNo2.Text.Trim(), txtAgentTelephoneNo3.Text.Trim(), txtAgentTelephoneNo3);//005
        string ContactTelephoneNumber = ObjCommon.Phoneformat(txtContactTelephoneNo1.Text.Trim(), txtContactTelephoneNo2.Text.Trim(), txtContactTelephoneNo3.Text.Trim(), txtContactTelephoneNo3);//005
        //002 - added below
        bool? isMarineCargoIns = (!string.IsNullOrEmpty(rdoTransitIns.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoTransitIns.SelectedValue)) : (bool?)null;
        bool? isInlandTransit = (!string.IsNullOrEmpty(rdoMarineCargoInlandTransit.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoInlandTransit.SelectedValue)) : (bool?)null;
        bool? isInstallEquip = (!string.IsNullOrEmpty(rdoMarineCargoInstEquip.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoInstEquip.SelectedValue)) : (bool?)null;
        bool? isMarinceCargoStorage = (!string.IsNullOrEmpty(rdoMarineCargoStorage.SelectedValue)) ? Convert.ToBoolean(Convert.ToInt32(rdoMarineCargoStorage.SelectedValue)) : (bool?)null;
        txtMarineCargoConvLimit.Text = (isMarineCargoIns == false || isMarineCargoIns == null) ? "" : txtMarineCargoConvLimit.Text;
        txtPerConveyanceLimitInlandTrans.Text = (isInlandTransit == false || isInlandTransit == null) ? "" : txtPerConveyanceLimitInlandTrans.Text;

        string convLimit = txtMarineCargoConvLimit.Text;
        string convLimitInlandTrans = txtPerConveyanceLimitInlandTrans.Text;

        reqvMarineCargoConvLimit.Enabled = (isMarineCargoIns != null) ? (bool)isMarineCargoIns : false;
        reqvConvLimitInlandTrans.Enabled = (isInlandTransit != null) ? (bool)isInlandTransit : false;

        //002 - End
        spanPollutionLiabExplainStar.Visible = txtPollutionLiability.Text == "0";
        rqfvPollutionLiabExplain.Enabled = txtPollutionLiability.Text == "0";

        bool Result = false;
        //004 - Page.Validate("ContactInformation");
        //004 - if (Page.IsValid)
        //004 - {
            //004 - valSummary.ValidationGroup = "AggregateLimits";
            //004 - Page.Validate("AggregateLimits");

            //004 - if (Page.IsValid)
            //004 - {
                //004 - valSummary.ValidationGroup = "Bonding";
                //004 - Page.Validate("Bonding");
                //004 - if (Page.IsValid)
                //004 - {
                    if (!(rblProvideamt.SelectedIndex < 0))
                    {
                        if (rblProvideamt.SelectedIndex == 0)
                        {
                            AdditionalInsured = true;
                        }
                        else if (rblProvideamt.SelectedIndex == 1)
                        {
                            AdditionalInsured = false;
                        }
                    }
                    else
                    {
                        AdditionalInsured = null;
                    }

                    int CountInsurance = objInsurance.GetInsuranceCount(VendorId);
                    if (CountInsurance == 0)
                    {
                        InsuranceStatus = 0;

                        Result = objInsurance.InsertInsurance(txtBrokerorCompanyName.Text.Trim(), txtInsuranceAgent.Text.Trim(), AgentTelephoneNumber, txtInsuranceCarrier.Text.Trim(), 
                                                              txtEachOccuranceAmt1.Text.Trim(), txtOperationsAggregeteAmt.Text.Trim(), txtGeneralAggregateAmt.Text.Trim(), 
                                                              AdditionalInsured, txtAccidentamt.Text.Trim(), txtEmpLiabilityAmt.Text.Trim(), txtDiseaseEmpAmt.Text.Trim(), 
                                                              txtDiseasePolicyAmtone.Text.Trim(), txtEachOccuranceAmt2.Text.Trim(), txtBondingSuretyCompanyNameone.Text.Trim(), 
                                                              txtCompanyContactName.Text.Trim(), ContactTelephoneNumber, txtTotalBondingCapacityAmt.Text.Trim(), 
                                                              txtCurrentBondingCapacityAmt.Text.Trim(), VendorId, InsuranceStatus, rblOccurrenceBased.Items[0].Selected, 
                                                              rblGeneralAggregateLimit.Items[0].Selected, rblUmbrellaExcessliability.Items[0].Selected, txtUmbrella.Text,
                                                              txtExcess.Text, rblStatutoryLimits.Items[0].Selected, isMarineCargoIns, isInlandTransit, isInstallEquip, isMarinceCargoStorage, convLimit, convLimitInlandTrans, 
                                                              !chkAgentUSA.Checked, !chkContactUSA.Checked, txtPollutionLiability.Text, txtPollutionLiabExplain.Text);//005
                        Session["Flag"] = 1;
                    }
                    else
                    {
                        string OutlookString = string.Empty;
                     
                        if (!string.IsNullOrEmpty(hdnInsuranceId.Value))
                        {
                            InsuranceId = Convert.ToInt64(hdnInsuranceId.Value);

                            //COMPARE
                            EditDate = objInsurance.GetSingleVQLInsuranceDate(VendorId);
                            InsuranceStatus = Convert.ToByte(EditDate.InsuranceStatus); 
                            Result = objInsurance.UpdateInsurance(txtBrokerorCompanyName.Text.Trim(), txtInsuranceAgent.Text.Trim(), AgentTelephoneNumber, txtInsuranceCarrier.Text.Trim(), 
                                                                  txtEachOccuranceAmt1.Text.Trim(), txtOperationsAggregeteAmt.Text.Trim(), txtGeneralAggregateAmt.Text.Trim(), AdditionalInsured, 
                                                                  txtAccidentamt.Text.Trim(), "", txtEmpLiabilityAmt.Text.Trim(), txtDiseaseEmpAmt.Text.Trim(), 
                                                                  txtDiseasePolicyAmtone.Text.Trim(), txtEachOccuranceAmt2.Text.Trim(), txtBondingSuretyCompanyNameone.Text.Trim(), 
                                                                  txtCompanyContactName.Text.Trim(), ContactTelephoneNumber, txtTotalBondingCapacityAmt.Text.Trim(), 
                                                                  txtCurrentBondingCapacityAmt.Text.Trim(), InsuranceId, InsuranceStatus, rblOccurrenceBased.Items[0].Selected, 
                                                                  rblGeneralAggregateLimit.Items[0].Selected, rblUmbrellaExcessliability.Items[0].Selected, txtUmbrella.Text,
                                                                  txtExcess.Text, rblStatutoryLimits.Items[0].Selected, isMarineCargoIns, isInlandTransit, isInstallEquip, isMarinceCargoStorage, convLimit, convLimitInlandTrans,
                                                                  !chkAgentUSA.Checked, !chkContactUSA.Checked, txtPollutionLiability.Text, txtPollutionLiabExplain.Text);//005

                            UpdateInsuranceData = objInsurance.GetSingleVQLInsuranceDate(VendorId);
                            ObjCommon.CompareChangeConsolidated = string.Empty;
                            ObjCommon.CompareChanges = Compare.CompareInsurance(EditDate, UpdateInsuranceData);
                            //bool UmbrellaExcessliability = false;
                            //if (rblUmbrellaExcessliability.SelectedIndex == 0)
                            //    UmbrellaExcessliability = true;
                            //else if (rblUmbrellaExcessliability.SelectedIndex == 1)
                            //    UmbrellaExcessliability = false;
                            if (ObjCommon.CompareChanges != string.Empty)
                            {
                                ObjCommon.CompareChangeConsolidated += Bond + Break + ObjCommon.CompareChanges + Break;
                                //string[] a = ObjCommon.CompareChanges.Split(';');
                                //int k = 0;
                                //ObjCommon.CompareChanges = string.Empty;
                                //while (k < a.Length - 1)
                                //{
                                //    //    if (a[k] == "Broker_CompanyName")
                                //    //        ContactPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                //    //    else if (a[k] == "InsuranceAgent")
                                //    //        ContactPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                //    //    else if (a[k] == "AgentTelephoneNumber")
                                //    //        ContactPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                //    //    else if (a[k] == "InsuranceCarrier")
                                //    //        ContactPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                //    //    else if (a[k] == "Bonding_SuretyCompanyName")
                                //    //        SurityPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                //    //    else if (a[k] == "CompanyContactName")
                                //    //        SurityPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                //    //    else if (a[k] == "ContactTelephoneNumber")
                                //    //        SurityPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                //    //    else if (a[k] == "TotalBondingCapacity")
                                //    //        SurityPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                //    //    else if (a[k] == "CurrentBondingCapacity")
                                //    //        SurityPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                //    //    else 
                                //    //        ObjCommon.CompareChanges += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";

                                //    //if (a[k].Length > 23 && a[k].Substring(6, "Broker_CompanyName".Length) == "Broker/CompanyName")
                                //    //    ContactPart += "%09" + a[k] + "%0A";
                                //    //else if (a[k].Length > 19 && a[k].Substring(6, "InsuranceAgent".Length) == "InsuranceAgent")
                                //    //    ContactPart += "%09" + a[k] + "%0A";
                                //    //else if (a[k].Length > 24 && a[k].Substring(6, "AgentTelephoneNumber".Length) == "AgentTelephoneNumber")
                                //    //    ContactPart += "%09" + a[k] + "%0A";
                                //    //else if (a[k].Length > 21 && a[k].Substring(6, "InsuranceCarrier".Length) == "InsuranceCarrier")
                                //    //    ContactPart += "%09" + a[k] + "%0A";
                                //    //else if (a[k].Length > 30 && a[k].Substring(6, "Bonding_SuretyCompanyName".Length) == "Bonding_SuretyCompanyName")
                                //    //    SurityPart += "%09" + a[k] + "%0A";
                                //    //else if (a[k].Length > 23 && a[k].Substring(6, "CompanyContactName".Length) == "CompanyContactName")
                                //    //    SurityPart += "%09" + a[k] + "%0A";
                                //    //else if (a[k].Length > 27 && a[k].Substring(6, "ContactTelephoneNumber".Length) == "ContactTelephoneNumber")
                                //    //    SurityPart += "%09" + a[k] + "%0A";
                                //    //else if (a[k].Length > 25 && a[k].Substring(6, "TotalBondingCapacity".Length) == "TotalBondingCapacity")
                                //    //    SurityPart += "%09" + a[k] + "%0A";
                                //    //else if (a[k].Length > 29 && a[k].Substring(6, "AvailableBondingCapacity".Length) == "AvailableBondingCapacity")
                                //    //    SurityPart += "%09" + a[k] + "%0A";
                                //    //else if ((a[k].Length > 20 && a[k].Substring(6, a[k].IndexOf(' ') - 6) == "Umbrella_Excess") || (a[k].Length > 13 && a[k].Substring(6, 8) == "Umbrella") || (a[k].Length > 11 && a[k].Substring(6, "Excess".Length) == "Excess"))
                                //    //{
                                //    //if (EditDate.Umbrella_Excess != UmbrellaExcessliability)
                                //    //{
                                //    //    if (a[k].Length > 20 && a[k].Substring(6, "Umbrella_Excess".Length) == "Umbrella_Excess")
                                //    //    {
                                //    //        ObjCommon.CompareChanges += "%09" + a[k] + "%0A";
                                //    //    }
                                //    //    else if (a[k].Length > 13 && a[k].Substring(6, "Umbrella".Length) == "Umbrella")
                                //    //    {
                                //    //        if (rblUmbrellaExcessliability.SelectedIndex == 0)
                                //    //        {
                                //    //            ObjCommon.CompareChanges += "%09" + "Umbrella =" + txtUmbrella.Text.Trim() + "%0A";
                                //    //        }
                                //    //        //else if (rblUmbrellaExcessliability.SelectedIndex == 1)
                                //    //        //{
                                //    //        //    ObjCommon.CompareChanges += "Deleted Field:Umbrella Data=" + EditDate.Umbrella.ToString() + ",";
                                //    //        //}
                                //    //    }
                                //    //    else if (a[k].Length > 11 && a[k].Substring(6, "Excess".Length) == "Excess")
                                //    //    {
                                //    //        if (rblUmbrellaExcessliability.SelectedIndex == 0)
                                //    //        {
                                //    //            ObjCommon.CompareChanges += "%09" + "Excess =" + txtExcess.Text.Trim() + "%0A";
                                //    //        }
                                //    //        //else if (rblUmbrellaExcessliability.SelectedIndex == 1)
                                //    //        //{
                                //    //        //    ObjCommon.CompareChanges += "Deleted Field:Excess Data=" + EditDate.Excess.ToString() + "%0A";
                                //    //        //}
                                //    //    }

                                //    //}
                                //    //    else
                                //    //    {
                                //    //        ObjCommon.CompareChanges += "%09" + a[k] + "%0A";
                                //    //    }
                                //    //}
                                //    //else if (a[k].Length > 20 && a[k].Substring(6, "InsuranceStatus".Length) == "InsuranceStatus")
                                //    //{
                                //    //    ObjCommon.CompareChanges += "";
                                //    //}
                                //    else
                                //        ObjCommon.CompareChanges += "%09" + a[k] + "%0A";
                                //    k++;
                                //}

                            }
                            if (ContactPart != string.Empty)
                            {
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>Contact Information</u></b><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append(ContactPart);
                                //OutlookString += "%0AContact Information" + "%0A" + ContactPart;
                                OutlookString += "%0A" + ContactPart;

                            }
                            if (ObjCommon.CompareChanges != string.Empty)
                            {
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>Aggregate Limits / Coverage </u></b><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append(ObjCommon.CompareChanges);
                                //OutlookString += "%0AAggregate Limits / Coverage " + "%0A" + ObjCommon.CompareChanges;
                                OutlookString += "%0A" + ObjCommon.CompareChanges;
                            }
                            if (SurityPart != string.Empty)
                            {
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>Bonding and Surety</u></b><br></font></td></tr>");
                                //CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                //CompareBody.Append(SurityPart);
                                OutlookString += "%0ABonding and Surety" + "%0A" + SurityPart;
                            }
                            if (ObjCommon.CompareChangeConsolidated != string.Empty)
                            {
                                //ObjCommon.CompareChangeOutlook += ObjCommon.CompareChangeConsolidated;
                                //CompareBody.Append("</table>");
                                //CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='4'><b><u>" + Bond + "</u></b><br></font></td></tr>");
                                //CompareMainBody.Append("</table>");
                                //CompareMainBody.Append(CompareBody);
                                OutlookString = Bond + "%0A" + OutlookString;
                            }

                            imbOk.Style["display"] = "none";
                            imbOkoutlook.Visible = true;

                            //Session["OUTLOOKDATA"] = Session["OUTLOOKDATA"].ToString() + CompareMainBody;
                            // Session["OUTLOOKDATA"] = Session["OUTLOOKDATA"].ToString() + "%0A" + OutlookString;
                            HiddenField outlookContent = VQFEditSideMenu.FindControl("hdnOutllook") as HiddenField;
                            outlookContent.Value = OutlookString;
                            HiddenField outlookVendorEmail = VQFEditSideMenu.FindControl("hdnOutllookVendorEmail") as HiddenField;
                            outlookVendorEmail.Value = Session["OUTLOOKTO"].ToString();
                            lblMsg.Text = "&nbsp;<li>Insurance & bonding details has been saved successfully</li>";
                            lblheading.Text = "Result";
                            modalExtnd.Show();
                        }

                    }
                    //Get InsuranceID
                    if (Result == true)
                    {
                        VMSDAL.VQFInsurance EditInsuranceData = objInsurance.GetSingleVQLInsuranceDate(VendorId);
                        hdnInsuranceId.Value = Convert.ToString(EditInsuranceData.PK_InsuranceID);
                        if (!string.IsNullOrEmpty(hdnInsuranceId.Value))
                        {
                            InsuranceId = Convert.ToInt64(hdnInsuranceId.Value);

                            VQFEditSideMenu.GetCompleteStatus();
                            VQFEditSideMenu.GetIncompleteStatus();
                        }
                    }
                //004 - }
                //004 - else
                //004 - {
                    //004 - lblheading.Text = "Bonding and Surety - Validation";
                   //004 -  lblMsg.Text = "";
                    //004 - lblError.Text = "";
                    //004 - modalExtnd.Show();
                    //004 - accBonding.SelectedIndex = 2;
                //004 - }
            //004 - }
            //004 - else
        //    {
        //        lblheading.Text = "Aggregate Limits / Coverage - Validation";
        //        lblMsg.Text = "";
        //        lblError.Text = "";
        //        modalExtnd.Show();
        //        accBonding.SelectedIndex = 1;
        //    }
        //}
        //else
        //{
        //    lblheading.Text = "Contact Information - Validation";
        //    lblMsg.Text = "";
        //    lblError.Text = "";
        //    modalExtnd.Show();
        //    accBonding.SelectedIndex = 0;
        //}
    }

    #endregion

    #region Validation Controls

    protected void cusvEachOccurances_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (Convert.ToString(ViewState["BusinessType"]).Equals("Design Consultant"))
        {
            if (string.IsNullOrEmpty(txtEachOccuranceAmt2.Text.Trim()))
            {
               // cusvEachOccurances.ErrorMessage = "Please enter each occurrence under professional liability";
                args.IsValid = true;
            }
        }
    }

    protected void cusphone_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if (((txtAgentTelephoneNo1.Text == "") && ((txtAgentTelephoneNo2.Text == "") && (txtAgentTelephoneNo3.Text == ""))))
        {
            //cusphone.ErrorMessage = "Please enter agent phone number under contact information";
            args.IsValid = true;
        }
        else
        {
            bool isValid = ObjCommon.ValidatePhoneformat(txtAgentTelephoneNo1.Text.Trim(), txtAgentTelephoneNo2.Text.Trim(), txtAgentTelephoneNo3.Text.Trim(), txtAgentTelephoneNo3);//005
            if (!isValid)
            {
                cusphone.ErrorMessage = "Please enter valid agent phone number under contact information";
                args.IsValid = false;
            }
        }
    }

    protected void cusContactPhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (((txtContactTelephoneNo1.Text == "") && ((txtContactTelephoneNo2.Text == "") && (txtContactTelephoneNo3.Text == ""))))
        {
           // cusvContactPhone.ErrorMessage = "Please enter contact telephone number under bonding and surety";
             args.IsValid = true;
        }
        else
        {
            bool isValid = ObjCommon.ValidatePhoneformat(txtContactTelephoneNo1.Text.Trim(), txtContactTelephoneNo2.Text.Trim(), txtContactTelephoneNo3.Text.Trim(), txtContactTelephoneNo3);//005
            if (!isValid)
            {
                cusvContactPhone.ErrorMessage = "Please enter valid contact telephone number under bonding and surety";
                args.IsValid = false;
            }
        }
    }

    protected void custTxtUmbrella_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((rblUmbrellaExcessliability.Items[0].Selected) && (string.IsNullOrEmpty(txtUmbrella.Text)))
        {
            //custTxtUmbrella.ErrorMessage = "Please enter umbrella each occurence/aggregate";
            args.IsValid = true;
        }
    }

    protected void cusvTxtExcess_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((rblUmbrellaExcessliability.Items[0].Selected) && (string.IsNullOrEmpty(txtExcess.Text)))
        {
           //cusvTxtExcess.ErrorMessage = "Please enter excess each occurence/aggregate";
            args.IsValid = true;
        }
    }

    #endregion

    #region Methods


    /// <summary>
    /// 003-Switch view based on logged in user
    /// </summary>
    /// <param name="IsShow"></param>
    /// <Author>Sooraj Sudhakaran.T</Author>
    ///  <Date>20-SEP-2013</Date>
    private void SwitchDesignConsultantView(bool IsShow)
    {
        //Design Consultant 
        apnBonding.Visible = !IsShow;
    }

    /// <summary>
    /// Will hide the required star on UI
    /// 003 - implemented
    /// </summary>
    /// <param name="businessType"></param>
    private void RequiredFieldDisplay(string businessType)
    {
        if (!String.IsNullOrEmpty(businessType))
        {
            if (!businessType.Equals(VMSDAL.TypeOfCompany.Subcontractor) && !businessType.Equals(VMSDAL.TypeOfCompany.EquipAndOnsiteSupport))
            {
                spnMarineCargoStar1.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar2.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar3.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar4.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar5.Attributes["class"] = "hidecontrol";


                // marine cargo
                reqvMarineCargoConvLimit.Enabled = false;
                reqvMarineCargoInlandTransit.Enabled = false;
                reqvMarineCargoInsEquip.Enabled = false;
                reqvMarineCargoStorage.Enabled = false;
                reqvTransitIns.Enabled = false;


            }

            if (businessType.Trim().Equals(VMSDAL.TypeOfCompany.DesignConsultant))
            {
                // hide the marine cargo section
                tdMarineCargoHead.Attributes.Add("style", "display: none");
                tdMarineCargoQuestions.Attributes.Add("style", "display: none");

                // hide required stars
                spnMarineCargoStar1.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar2.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar3.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar4.Attributes["class"] = "hidecontrol";
                spnMarineCargoStar5.Attributes["class"] = "hidecontrol";

                // disable validators (marine & cargo)
                reqvMarineCargoConvLimit.Enabled = false;
                reqvMarineCargoInlandTransit.Enabled = false;
                reqvMarineCargoInsEquip.Enabled = false;
                reqvMarineCargoStorage.Enabled = false;
                reqvTransitIns.Enabled = false;
            }

            spnProfessional.Style["display"] = Convert.ToString(ViewState["BusinessType"]).Equals("Design Consultant") ? "" : "none";
            cusvEachOccurances.Enabled = Convert.ToString(ViewState["BusinessType"]).Equals("Design Consultant") ? true : false;


        }
    }

    /// <summary>
    /// Display panel based on sub-menu selection.
    /// </summary>
    private void ShowHideControls()
    {
        switch (Convert.ToString(Request.QueryString["submenu"]))
        {
            case "1":
                accBonding.SelectedIndex = 0;
                break;
            case "2":
                accBonding.SelectedIndex = 1;
                break;
            case "3":
                accBonding.SelectedIndex = 2;
                break;
        }
    }

    /// <summary>
    /// Append Client-side events to controls.
    /// </summary>
    private void CallScripts()
    {
        txtContactTelephoneNo1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtContactTelephoneNo1.ClientID + "," + txtContactTelephoneNo2.ClientID + ")");
        txtContactTelephoneNo2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtContactTelephoneNo2.ClientID + "," + txtContactTelephoneNo3.ClientID + ")");

        txtAgentTelephoneNo1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtAgentTelephoneNo1.ClientID + "," + txtAgentTelephoneNo2.ClientID + ")");
        txtAgentTelephoneNo2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtAgentTelephoneNo2.ClientID + "," + txtAgentTelephoneNo3.ClientID + ")");

        txtEachOccuranceAmt1.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtEachOccuranceAmt1.ClientID + "')");
        txtEachOccuranceAmt1.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtEachOccuranceAmt1.ClientID + "')");
        txtEachOccuranceAmt1.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtEachOccuranceAmt1.ClientID + "')");
        txtOperationsAggregeteAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtOperationsAggregeteAmt.ClientID + "')");

        txtGeneralAggregateAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtGeneralAggregateAmt.ClientID + "')");
        txtGeneralAggregateAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtGeneralAggregateAmt.ClientID + "')");
        txtAccidentamt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAccidentamt.ClientID + "')");
        txtAccidentamt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtAccidentamt.ClientID + "')");

        txtEmpLiabilityAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtEmpLiabilityAmt.ClientID + "')");
        txtEmpLiabilityAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtEmpLiabilityAmt.ClientID + "')");

        txtDiseaseEmpAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtDiseaseEmpAmt.ClientID + "')");
        txtDiseaseEmpAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtDiseaseEmpAmt.ClientID + "')");

        txtDiseasePolicyAmtone.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtDiseasePolicyAmtone.ClientID + "')");
        txtDiseasePolicyAmtone.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtDiseasePolicyAmtone.ClientID + "')");

        txtEachOccuranceAmt2.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtEachOccuranceAmt2.ClientID + "')");
        txtEachOccuranceAmt2.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtEachOccuranceAmt2.ClientID + "')");
        txtPollutionLiability.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtPollutionLiability.ClientID + "')");
        txtPollutionLiability.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtPollutionLiability.ClientID + "')");

        txtTotalBondingCapacityAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtTotalBondingCapacityAmt.ClientID + "')");
        txtTotalBondingCapacityAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalBondingCapacityAmt.ClientID + "')");


        txtCurrentBondingCapacityAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtCurrentBondingCapacityAmt.ClientID + "');validateBondingCapacity();");
        txtCurrentBondingCapacityAmt.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtCurrentBondingCapacityAmt.ClientID + "');validateBondingCapacity();");



        txtOperationsAggregeteAmt.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtOperationsAggregeteAmt.ClientID + "')");
        txtOperationsAggregeteAmt.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtOperationsAggregeteAmt.ClientID + "')");
        txtOperationsAggregeteAmt.Attributes.Add("onblur", "javascript:numberFormat(this.value,'','" + txtOperationsAggregeteAmt.ClientID + "')");




        txtGeneralAggregateAmt.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtGeneralAggregateAmt.ClientID + "')");
        txtAccidentamt.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAccidentamt.ClientID + "')");
        txtEmpLiabilityAmt.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtEmpLiabilityAmt.ClientID + "')");
        txtDiseaseEmpAmt.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtDiseaseEmpAmt.ClientID + "')");
        txtDiseasePolicyAmtone.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtDiseasePolicyAmtone.ClientID + "')");
        txtEachOccuranceAmt2.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtEachOccuranceAmt2.ClientID + "')");
        txtPollutionLiability.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtPollutionLiability.ClientID + "')");
        txtTotalBondingCapacityAmt.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtTotalBondingCapacityAmt.ClientID + "')");
        txtCurrentBondingCapacityAmt.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtCurrentBondingCapacityAmt.ClientID + "')");
        txtUmbrella.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtUmbrella.ClientID + "')");
        txtUmbrella.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtUmbrella.ClientID + "')");
        txtExcess.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtExcess.ClientID + "')");
        txtExcess.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtExcess.ClientID + "')");

        //002 - added below:
        txtMarineCargoConvLimit.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMarineCargoConvLimit.ClientID + "')");
        txtMarineCargoConvLimit.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtMarineCargoConvLimit.ClientID + "')");
        txtPerConveyanceLimitInlandTrans.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtPerConveyanceLimitInlandTrans.ClientID + "')");
        txtPerConveyanceLimitInlandTrans.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtPerConveyanceLimitInlandTrans.ClientID + "')");
        rdoMarineCargoInlandTransit.Items[0].Attributes.Add("onclick", @"javascript:OpenHideControl('" + trPerConveyanceLimitInlandTrans.ClientID + "'" + ",this.value)");
        rdoMarineCargoInlandTransit.Items[1].Attributes.Add("onclick", @"javascript:OpenHideControl('" + trPerConveyanceLimitInlandTrans.ClientID + "'" + ",this.value)");
        rdoTransitIns.Items[0].Attributes.Add("onclick", @"javascript:OpenHideControl('" + trPerConveyanceLimitMarineIns.ClientID + "'" + ",this.value)");
        rdoTransitIns.Items[1].Attributes.Add("onclick", @"javascript:OpenHideControl('" + trPerConveyanceLimitMarineIns.ClientID + "'" + ",this.value)");

        //005 - Sooraj --Internationalization for Phone 
        chkAgentUSA.Attributes.Add("OnClick", "SwitchPhone(this,'" + txtAgentTelephoneNo1.ClientID + "','" + txtAgentTelephoneNo2.ClientID + "','" + txtAgentTelephoneNo3.ClientID + "','" + spnPPhone2.ClientID + "')");
        chkContactUSA.Attributes.Add("OnClick", "SwitchPhone(this,'" + txtContactTelephoneNo1.ClientID + "','" + txtContactTelephoneNo2.ClientID + "','" + txtContactTelephoneNo3.ClientID + "','" + spnCPhone2.ClientID + "')");

    }

    /// <summary>
    /// 005
    /// </summary>
    /// <param name="val"></param>
    /// <param name="phone1"></param>
    /// <param name="phone2"></param>
    /// <param name="phone3"></param>
    /// <param name="spnPhone"></param>
    private void SwitchPhone(CheckBox val, TextBox phone1, TextBox phone2, TextBox phone3, HtmlControl spnPhone)
    {

        if (val.Checked)//Non-US
        {
            val.Text = "Non-US";
            phone3.Style["display"] = "none";

            phone1.MaxLength = 5;
            phone2.MaxLength = 15;
            phone2.CssClass = "txtbox";
            phone3.Text = "";
            spnPhone.Style["display"] = "none";
            phone1.Attributes.Remove("onkeyup");
            phone2.Attributes.Remove("onkeyup");
        }
        else  //US
        {
            phone1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + phone1.ClientID + "," + phone2.ClientID + ")");
            phone2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + phone2.ClientID + "," + phone3.ClientID + ")");
            // val.Text = "US";
            phone3.Style["display"] = "";
            phone2.CssClass = "txtboxsmall";
            phone1.MaxLength = 3;
            phone2.MaxLength = 3;

            spnPhone.Style["display"] = "";

        }

    }


    /// <summary>
    /// 005-Switch phone for Key Contacts 
    /// </summary>
    /// <param name="chk"></param>
    private void SwitchPhoneForKeyContact(CheckBox chk)
    {
        if (chk != null)
        {
            switch (chk.ID)
            {
                case "chkContactUSA":
                    SwitchPhone(chkContactUSA, txtContactTelephoneNo1, txtContactTelephoneNo2, txtContactTelephoneNo3, spnCPhone2);
                    break;
                case "chkAgentUSA":
                    SwitchPhone(chkAgentUSA, txtAgentTelephoneNo1, txtAgentTelephoneNo2, txtAgentTelephoneNo3, spnPPhone2);

                    break;

            }
        }
        else
        {
            SwitchPhone(chkContactUSA, txtContactTelephoneNo1, txtContactTelephoneNo2, txtContactTelephoneNo3, spnCPhone2);
            SwitchPhone(chkAgentUSA, txtAgentTelephoneNo1, txtAgentTelephoneNo2, txtAgentTelephoneNo3, spnPPhone2);
        }

    }

    /// <summary>
    /// Populate data from data store.
    /// </summary>
    /// <param name="IsMyProfile"></param>
    private void LoadEditDatas(bool IsMyProfile)
    {
        VendorId = VendorId;
        EditData = true;
        //VendorId =10;
        bool? isPolicy, isGeneral, isUmbrellaExcess, isWCPolicy;
        int CountInsurance = objInsurance.GetInsuranceCount(VendorId);

        if (CountInsurance > 0)
        {
            EditDate = objInsurance.GetSingleVQLInsuranceDate(VendorId);
            if (EditDate != null)
            {
                hdnInsuranceId.Value = Convert.ToString(EditDate.PK_InsuranceID);
                LoadMarineCargoInsurance(EditDate); //002
                long InsuranceId = EditDate.PK_InsuranceID;
                txtBrokerorCompanyName.Text = EditDate.Broker_CompanyName;
                txtInsuranceAgent.Text = EditDate.InsuranceAgent;
                chkAgentUSA.Checked = !EditDate.InsuranceAgentIsBasedInUS.Value;
                SwitchPhoneForKeyContact(chkAgentUSA);
                string AgentTeleNo = EditDate.AgentTelephoneNumber;
                ObjCommon.loadphone(txtAgentTelephoneNo1, txtAgentTelephoneNo2, txtAgentTelephoneNo3, AgentTeleNo);
                txtInsuranceCarrier.Text = EditDate.InsuranceCarrier;
                txtEachOccuranceAmt1.Text = EditDate.GeneralEachOccurrence;
                txtOperationsAggregeteAmt.Text = EditDate.Product_CompletedOperations;
                txtGeneralAggregateAmt.Text = EditDate.GeneralAggregate;
                bool? AdditionalInsured = EditDate.AdditionalInsured;
                if (AdditionalInsured != null)
                {
                    if (AdditionalInsured == true)
                    {
                        rblProvideamt.SelectedIndex = 0;
                        trnotes.Style["display"] = "";
                    }
                    else
                    {
                        rblProvideamt.SelectedIndex = 1;
                        trnotes.Style["display"] = "none";
                    }
                }
                isPolicy = EditDate.IsPolicy;
                if (isPolicy != null)
                {
                    rblOccurrenceBased.SelectedIndex = Convert.ToBoolean(isPolicy) ? 0 : 1;
                }

                isGeneral = EditDate.IsGeneral;
                if (isGeneral != null)
                {
                    rblGeneralAggregateLimit.SelectedIndex = Convert.ToBoolean(isGeneral) ? 0 : 1;
                }
                isUmbrellaExcess = EditDate.Umbrella_Excess;
                if (isUmbrellaExcess != null)
                {
                    if (isUmbrellaExcess == true)
                    {
                        rblUmbrellaExcessliability.SelectedIndex = 0;
                        trUmbrella.Style["Display"] = "";
                        trExcess.Style["Display"] = "";
                    }
                    else
                    {
                        rblUmbrellaExcessliability.SelectedIndex = 1;
                        trUmbrella.Style["Display"] = "None";
                        trExcess.Style["Display"] = "None";
                    }
                }
                else
                {
                    trUmbrella.Style["Display"] = "None";
                    trExcess.Style["Display"] = "None";
                }

                txtUmbrella.Text = EditDate.Umbrella;
                txtExcess.Text = EditDate.Excess;
                isWCPolicy = EditDate.WCPolicy;
                if (isWCPolicy != null)
                {
                    rblStatutoryLimits.SelectedIndex = Convert.ToBoolean(isWCPolicy) ? 0 : 1;
                }
                txtAccidentamt.Text = EditDate.EachAccident;
                //txtStateRequirementsAmt.Text = EditDate.StateRequirements;
                txtEmpLiabilityAmt.Text = EditDate.EmployersLiability;
                txtDiseaseEmpAmt.Text = EditDate.DiseaseEachEmployee;
                txtDiseasePolicyAmtone.Text = EditDate.DiseasePolicyLimit;
                txtEachOccuranceAmt2.Text = EditDate.ProfessionalEachOccurrence;
                txtPollutionLiability.Text = EditDate.PollutionPolicyAmount != default(decimal?) ? EditDate.PollutionPolicyAmount.ToString().Substring(0, EditDate.PollutionPolicyAmount.ToString().Length - 3) : string.Empty;
                txtPollutionLiabExplain.Text = EditDate.PollutionPolicyExplain ?? "";
                txtBondingSuretyCompanyNameone.Text = EditDate.Bonding_SuretyCompanyName;
                txtCompanyContactName.Text = EditDate.CompanyContactName;
                chkContactUSA.Checked = !EditDate.ContactIsBasedInUS.Value; //005
                SwitchPhoneForKeyContact(chkContactUSA);
                string ContactTeleNo = EditDate.ContactTelephoneNumber;
                ObjCommon.loadphone(txtContactTelephoneNo1, txtContactTelephoneNo2, txtContactTelephoneNo3, ContactTeleNo);
                txtTotalBondingCapacityAmt.Text = EditDate.TotalBondingCapacity;
                txtCurrentBondingCapacityAmt.Text = EditDate.CurrentBondingCapacity;
            }
        }
    }

    /// <summary>
    /// 002 - implemented
    /// </summary>
    /// <param name="EditDate"></param>
    private void LoadMarineCargoInsurance(VMSDAL.VQFInsurance EditDate)
    {
        rdoMarineCargoInlandTransit.Items[1].Selected = (EditDate.IsInlandTransit != null) ? !(bool)EditDate.IsInlandTransit : false;
        rdoMarineCargoInlandTransit.Items[0].Selected = (EditDate.IsInlandTransit != null) ? (bool)EditDate.IsInlandTransit : false;
        rdoTransitIns.Items[1].Selected = (EditDate.IsMarineCargoInsurance != null) ? !(bool)EditDate.IsMarineCargoInsurance : false;
        rdoTransitIns.Items[0].Selected = (EditDate.IsMarineCargoInsurance != null) ? (bool)EditDate.IsMarineCargoInsurance : false;
        rdoMarineCargoStorage.Items[1].Selected = (EditDate.IsStorage != null) ? !(bool)EditDate.IsStorage : false;
        rdoMarineCargoStorage.Items[0].Selected = (EditDate.IsStorage != null) ? (bool)EditDate.IsStorage : false;
        rdoMarineCargoInstEquip.Items[1].Selected = (EditDate.IsInstallationOfEquip != null) ? !(bool)EditDate.IsInstallationOfEquip : false;
        rdoMarineCargoInstEquip.Items[0].Selected = (EditDate.IsInstallationOfEquip != null) ? (bool)EditDate.IsInstallationOfEquip : false;
        txtMarineCargoConvLimit.Text = (EditDate.ConveyanceLimitMarineInsurance != null) ? EditDate.ConveyanceLimitMarineInsurance : string.Empty;
        txtPerConveyanceLimitInlandTrans.Text = (EditDate.ConveyanceLimitInlandTransit != null) ? EditDate.ConveyanceLimitInlandTransit : string.Empty;
    }

    #endregion

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(Session["ReturnQuery"].ToString());
    }
    protected void imbOkoutlook_Click(object sender, ImageClickEventArgs e)
    {
        modalExtnd.Hide();
        imbOkoutlook.Visible = false;
        imbOk.Style["display"] = "";
        //Response.Redirect(Session["ReturnQuery"].ToString());
        LoadEditDatas(true);
        CallScripts();
        ShowHideControls();
    }

}
