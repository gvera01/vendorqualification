﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LegalFinancial.aspx.cs" Inherits="VQF_LegalFinancial" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <style type="text/css">
        body
        {
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbSaveNext">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:VQFEditSideMenu ID="VQFEditSideMenu" runat="server" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="bodytextbold_right even_pad">
                                                    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click">Return to 
          Search Result</asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextbold_right">
                                                    Welcome
                                                    <asp:Label ID="lblwelcome" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    Vendor <span style="letter-spacing: 1px">Qualification</span> Form - Legal & Financial
                                                </td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdheight">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdheight">
                                    </td>
                                </tr>
                                <tr>
                                    <td><asp:UpdatePanel ID="updPanel" runat="server">
                                                        <ContentTemplate>
                                        <Ajax:Accordion ID="accLegal" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
                                            HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                                            FadeTransitions="true" FramesPerSecond="40" TransitionDuration="250" AutoSize="none"
                                            RequireOpenedPane="true" SuppressHeaderPostbacks="true">
                                            <Panes>
                                                <Ajax:AccordionPane ID="apnLegal" runat="server">
                                                    <Header>
                                                        Legal
                                                    </Header>
                                                    <Content>
                                                        <table align="center" border="0" cellpadding="5" cellspacing="5" width="100%" class="searchtableclass">
                                                            <tr>
                                                                <td>
                                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td class="bodytextleft">
                                                                                <span class="mandatorystar">*</span>Have you failed to complete awarded work or
                                                                                been terminated for cause?
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextleft" style="padding: 0 0 0 5px;">
                                                                                <asp:RadioButtonList ID="rdlListOne" RepeatDirection="Horizontal" onclick="javascript:ShowrdlListOne();"
                                                                                    runat="server" TabIndex="0">
                                                                                    <asp:ListItem Value="0">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="1">No</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                                <asp:CustomValidator ID="cusvListOne" runat="server" ValidationGroup="Legal" Display="None"
                                                                                    EnableClientScript="false" SetFocusOnError="true" OnServerValidate="cusvListOne_ServerValidate"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <span id="trWorkAwdOne" runat="server" style="display: none">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td class="bodytextleft" width="20%" valign="top">
                                                                                                &nbsp; If Yes, Please Explain
                                                                                            </td>
                                                                                            <td class="bodytextleft">
                                                                                                <asp:TextBox CssClass="txtboxmultilarge" TabIndex="0" ID="txtWorkAwd" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                    onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                                                    TextMode="MultiLine" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextleft" nowrap="nowrap">
                                                                                <span class="mandatorystar">*</span>Is your company or any of its Owners, Officers
                                                                                or Major Stockholders currently involved in any arbitration, litigation, suits or
                                                                                liens?
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextleft" style="padding: 0 0 0 5px;">
                                                                                <asp:RadioButtonList ID="rdlListTwo" RepeatDirection="Horizontal" onclick="javascript:ShowrdlListTwo()"
                                                                                    runat="server" TabIndex="0">
                                                                                    <asp:ListItem Value="0">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="1">No</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                                <asp:CustomValidator ID="cusvListTwo" runat="server" ValidationGroup="Legal" Display="None"
                                                                                    EnableClientScript="false" SetFocusOnError="true" OnServerValidate="cusvListTwo_ServerValidate"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <span id="trStock" runat="server" style="display: none">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td class="bodytextleft" width="20%" valign="top">
                                                                                                &nbsp; If Yes, Please Explain
                                                                                            </td>
                                                                                            <td class="bodytextleft">
                                                                                                <asp:TextBox CssClass="txtboxmultilarge" TabIndex="0" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                    onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                                                    ID="txtStake" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextleft">
                                                                                <span class="mandatorystar">*</span>Has the company had any judgments, bankruptcies
                                                                                or reorganizations?
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextleft" style="padding: 0 0 0 5px;">
                                                                                <asp:RadioButtonList ID="rdlListThree" TabIndex="0" RepeatDirection="Horizontal"
                                                                                    onclick="javascript:ShowrdlListThree()" runat="server">
                                                                                    <asp:ListItem Value="0">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="1">No</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                                <asp:CustomValidator ID="cusvListThree" runat="server" ValidationGroup="Legal" Display="None"
                                                                                    EnableClientScript="false" SetFocusOnError="true" OnServerValidate="cusvListThree_ServerValidate"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <span id="trBankruptcy" runat="server" style="display: none">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td class="bodytextleft" width="20%" valign="top">
                                                                                                &nbsp; If Yes, Please Explain
                                                                                            </td>
                                                                                            <td class="bodytextleft">
                                                                                                <asp:TextBox CssClass="txtboxmultilarge" TabIndex="0" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                    onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                                                    ID="txtBankruptcy" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextleft">
                                                                                <span class="mandatorystar">*</span>Have any of the Owner, Officers or Major Stockholders
                                                                                of your company ever been indicted or convicted of felony or other &nbsp;&nbsp;&nbsp;criminal
                                                                                conduct?
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextleft" style="padding: 0 0 0 5px;">
                                                                                <asp:RadioButtonList ID="rdlListFour" TabIndex="0" RepeatDirection="Horizontal" onclick="javascript:ShowrdlListFour()"
                                                                                    runat="server">
                                                                                    <asp:ListItem Value="0">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="1">No</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                                <asp:CustomValidator ID="cusvListFour" runat="server" ValidationGroup="Legal" Display="None"
                                                                                    EnableClientScript="false" SetFocusOnError="true" OnServerValidate="cusvListFour_ServerValidate"></asp:CustomValidator>
                                                                                <asp:CustomValidator ID="cusvLegalQuestion" runat="server" ValidationGroup="Legal"
                                                                                    Display="None" EnableClientScript="false" SetFocusOnError="true" OnServerValidate="cusvLegalQuestion_ServerValidate"></asp:CustomValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <span id="trConviction" runat="server" style="display: none">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td class="bodytextleft" width="20%" valign="top">
                                                                                                &nbsp; If Yes, Please Explain
                                                                                            </td>
                                                                                            <td class="bodytextleft">
                                                                                                <asp:TextBox CssClass="txtboxmultilarge" TabIndex="0" ID="txtRisk" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                    onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                                                    TextMode="MultiLine" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Content>
                                                </Ajax:AccordionPane>
                                                <Ajax:AccordionPane ID="apnFinancial" runat="server">
                                                    <Header>
                                                        Financial</Header>
                                                    <Content>
                                                        <asp:Panel ID="pnlContent" runat="server" Width="100%">
                                                            <table align="center" border="0" cellpadding="5" cellspacing="5" width="100%" class="searchtableclass">
                                                                <tr>
                                                                    <td>
                                                                        <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                            <%--G. Vera 04/11/2016: Added row below HDT # 31519--%>
                                                                            <tr>
                                                                                <td colspan="4" class="left_border formtdbold_rt">
                                                                                    Please be advised that Haskell and its affiliates will require your most current Financial Statements (preferably audited) 
                                                                                    for consideration of award on all contracts estimated at $500,000.00 and greater. 
                                                                                    Financial Statements can be e-mailed directly to 
                                                                                    <a href='<%="mailto://Prequal@haskell.com?subject=Financial Statements for " + this.lblwelcome.Text %>'>Prequal@haskell.com</a>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" class="left_border formtdbold_rt">
                                                                                    List data for the three most recent completed fiscal years
                                                                              <span id="lblCompletedinDollar" class="bodytextleft" style="display:none" runat="server"><br /><br />(This section needs
                                                                                                            to be completed in US Dollars) </span>  </td>
                                                                            </tr>
                                                                            <tr id="trMessage" style="display: none;">
                                                                                <td colspan="4">
                                                                                    <asp:Label ID="lblMessage" CssClass="summaryerrormsg" runat="server" Style="display: none;"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytextbold sectionpad">
                                                                                    Year One
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtYearOne" TabIndex="0" runat="server" ReadOnly="true"
                                                                                        Text="2009"></asp:TextBox>
                                                                                    <asp:CheckBox ID="chkNAYear1" runat="server" Text="N/A" />
                                                                                    <asp:CustomValidator ID="cusvYearOne" runat="server" Display="None" ValidationGroup="Financial"
                                                                                        OnServerValidate="cusvYearOne_ServerValidate"></asp:CustomValidator>
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trExplainYear1" runat="server" style="display: none">
                                                                                <td class="bodytextleft sectionpad">
                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                </td>
                                                                                <td class="bodytextleft" colspan="3">
                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplainYear1" TextMode="MultiLine" runat="server"
                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytextleft sectionpad" width="34%">
                                                                                    <span class="mandatorystar">*</span>Maximum Contract Value Completed $
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtMaxContractValueYearOne"
                                                                                        runat="server"></asp:TextBox>
                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtMaxContract" runat="server" TargetControlID="txtMaxContractValueYearOne"
                                                                                        FilterType="Numbers,Custom" ValidChars=",">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                </td>
                                                                                <td class="bodytextleft" width="26%">
                                                                                    &nbsp; <span class="mandatorystar">*</span>Annual Company Revenue $
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtAnuualCompRevenueYearOne"
                                                                                        runat="server"></asp:TextBox>
                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAnnualRevenue" runat="server" FilterType="Numbers,Custom"
                                                                                        ValidChars="," TargetControlID="txtAnuualCompRevenueYearOne">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                    <%--<asp:CustomValidator ID="cusvAnnualRevenueYear1" runat="server" ValidationGroup="Financial" Display="None"
                                                                                                    ErrorMessage="Annual company revenue should be greater than maximum contract value completed under year one"
                                                                                                    OnServerValidate="cusvAnnualRevenueYear1_ServerValidate"></asp:CustomValidator>--%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytextbold sectionpad">
                                                                                    Year Two
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    <asp:TextBox CssClass="txtboxsmall" TabIndex="0" MaxLength="4" ID="txtYearTwo" runat="server"
                                                                                        ReadOnly="true" Text="2008"></asp:TextBox>&nbsp;&nbsp;<asp:CheckBox ID="chkNAYear2"
                                                                                            runat="server" Text="N/A" />
                                                                                    <asp:CustomValidator ID="cusvYearTwo" runat="server" Display="None" ValidationGroup="Financial"
                                                                                        OnServerValidate="cusvYearTwo_ServerValidate"></asp:CustomValidator>
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trExplainYear2" runat="server" style="display: none">
                                                                                <td class="bodytextleft sectionpad">
                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                </td>
                                                                                <td class="bodytextleft" colspan="3">
                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplainYesr2" runat="server" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                        onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                                        TabIndex="0" TextMode="MultiLine"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytextleft sectionpad">
                                                                                    <span class="mandatorystar">*</span>Maximum Contract Value Completed $
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtMaxContractValueYearTwo"
                                                                                        runat="server"></asp:TextBox>
                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtMaxContractvalue" runat="server" FilterType="Numbers,Custom"
                                                                                        ValidChars="," TargetControlID="txtMaxContractValueYearTwo">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    &nbsp; <span class="mandatorystar">*</span>Annual Company Revenue $
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtAnuualCompRevenueYearTwo"
                                                                                        runat="server"></asp:TextBox>
                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAnnualCompRevenue" runat="server" FilterType="Numbers,Custom"
                                                                                        ValidChars="," TargetControlID="txtAnuualCompRevenueYearTwo">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                    <%--<asp:CustomValidator ID="cusvAnnualRevenueYear2" runat="server" ValidationGroup="Financial"
                                                                                                    Display="None" ErrorMessage="Annual company revenue should be greater than maximum contract value completed under year two"
                                                                                                    OnServerValidate="cusvAnnualRevenueYear2_ServerValidate"></asp:CustomValidator>--%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytextbold sectionpad">
                                                                                    Year Three
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    <asp:TextBox CssClass="txtboxsmall" TabIndex="0" MaxLength="4" ID="txtYearThree"
                                                                                        runat="server" ReadOnly="true" Text="2007"></asp:TextBox>&nbsp;&nbsp;<asp:CheckBox
                                                                                            ID="chkNAYear3" runat="server" Text="N/A" />
                                                                                    <asp:CustomValidator ID="cusvYearThree" runat="server" Display="None" ValidationGroup="Financial"
                                                                                        OnServerValidate="cusvYearThree_ServerValidate"></asp:CustomValidator>
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trExplainYear3" runat="server" style="display: none">
                                                                                <td class="bodytextleft sectionpad">
                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                </td>
                                                                                <td class="bodytextleft" colspan="3">
                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplainYear3" runat="server" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                        onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                                        TabIndex="0" TextMode="MultiLine"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytextleft sectionpad">
                                                                                    <span class="mandatorystar">*</span>Maximum Contract Value Completed $
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    <asp:TextBox CssClass="txtboxmedium1" MaxLength="15" TabIndex="0" ID="txtMaxContractValueYearThree"
                                                                                        runat="server"></asp:TextBox>
                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtMaxContractValue2" runat="server" FilterType="Numbers,Custom"
                                                                                        ValidChars="," TargetControlID="txtMaxContractValueYearThree">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    &nbsp; <span class="mandatorystar">*</span>Annual Company Revenue $
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    <asp:TextBox CssClass="txtboxmedium1" MaxLength="15" TabIndex="0" ID="txtAnuualCompRevenueYearThree"
                                                                                        runat="server"></asp:TextBox>
                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAnnualRevenue2" runat="server" FilterType="Numbers,Custom"
                                                                                        ValidChars="," TargetControlID="txtAnuualCompRevenueYearThree">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                    <%--<asp:CustomValidator ID="cusvAnnualRevenueYear3" runat="server" ValidationGroup="Financial"
                                                                                                    Display="None" ErrorMessage="Annual company revenue should be greater than maximum contract value completed under year three"
                                                                                                    OnServerValidate="cusvAnnualRevenueYear3_ServerValidate"></asp:CustomValidator>--%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" height="9px">
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Start Here 1 -->
                                                                            <tr id="spnYearMore1" runat="server" style="display: none;">
                                                                                <td colspan="4" style="padding-left: 1px">
                                                                                    <span>
                                                                                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold sectionpad" width="35%">
                                                                                                    Year Four
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="18%">
                                                                                                    <asp:TextBox CssClass="txtboxsmall" TabIndex="0" MaxLength="4" ID="txtYear1" runat="server"></asp:TextBox>
                                                                                                    &nbsp;&nbsp;<asp:CheckBox ID="chkNAYear4" runat="server" Text="N/A" />
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtYear1" runat="server" FilterType="Numbers"
                                                                                                        TargetControlID="txtYear1">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <asp:RegularExpressionValidator Display="None" ValidationGroup="Financial" ID="regvYear1"
                                                                                                        ControlToValidate="txtYear1" runat="server" ErrorMessage="Please enter valid year 1"
                                                                                                        ValidationExpression="\d{4}" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    <asp:CustomValidator ID="cusvYearFour" runat="server" Display="None" ValidationGroup="Financial"
                                                                                                        OnServerValidate="cusvYearFour_ServerValidate"></asp:CustomValidator>
                                                                                                </td>
                                                                                                <td width="24%">
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trExplainYear4" runat="server" style="display: none">
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                                </td>
                                                                                                <td class="bodytextleft" colspan="3">
                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplainYear4" TextMode="MultiLine" runat="server"
                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    &nbsp; Maximum Contract Value Completed $
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="23%">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" MaxLength="15" TabIndex="0" ID="txtMaxContractValueYear1"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtContractValue3" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtMaxContractValueYear1">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    Annual Company Revenue $
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" MaxLength="15" TabIndex="0" ID="txtAnuualCompRevenueYear1"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAnnualRevenue3" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars=".,$" TargetControlID="txtAnuualCompRevenueYear1">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <%--<asp:CustomValidator ID="cusvAnnualRevenueYear4" runat="server" ValidationGroup="Financial"
                                                                                                                    Display="None" ErrorMessage="Annual company revenue should be greater than maximum contract value completed under year four"
                                                                                                                    OnServerValidate="cusvAnnualRevenueYear4_ServerValidate"></asp:CustomValidator>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="spnYearMore2" runat="server" style="display: none">
                                                                                <td colspan="4" style="padding-left: 1px">
                                                                                    <span>
                                                                                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold sectionpad" width="35%">
                                                                                                    Year Five
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="18%">
                                                                                                    <asp:TextBox CssClass="txtboxsmall" TabIndex="0" MaxLength="4" ID="txtYear2" runat="server"></asp:TextBox>
                                                                                                    &nbsp;&nbsp;<asp:CheckBox ID="chkNAYear5" runat="server" Text="N/A" />
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtYear2" runat="server" FilterType="Numbers"
                                                                                                        TargetControlID="txtYear2">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <asp:RegularExpressionValidator Display="None" ValidationGroup="Financial" ID="RegularExpressionValidator1"
                                                                                                        ControlToValidate="txtYear2" runat="server" ErrorMessage="Please enter valid year 2"
                                                                                                        ValidationExpression="\d{4}" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    <asp:CustomValidator ID="cusvYearFive" runat="server" Display="None" ValidationGroup="Financial"
                                                                                                        OnServerValidate="cusvYearFive_ServerValidate"></asp:CustomValidator>
                                                                                                </td>
                                                                                                <td width="24%">
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trExplainYear5" runat="server" style="display: none">
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                                </td>
                                                                                                <td class="bodytextleft" colspan="3">
                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplainYear5" TextMode="MultiLine" runat="server"
                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    &nbsp; Maximum Contract Value Completed $
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="23%">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtMaxContractValueYear2"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtContractValue4" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtMaxContractValueYear2">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    Annual Company Revenue $
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtAnuualCompRevenueYear2"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAnnualRevenue4" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtAnuualCompRevenueYear2">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <%--<asp:CustomValidator ID="cusvAnnualRevenueYear5" runat="server" ValidationGroup="Financial"
                                                                                                                    Display="None" ErrorMessage="Annual company revenue should be greater than maximum contract value completed under year five"
                                                                                                                    OnServerValidate="cusvAnnualRevenueYear5_ServerValidate"></asp:CustomValidator>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="spnYearMore3" runat="server" style="display: none">
                                                                                <td colspan="4" style="padding-left: 1px">
                                                                                    <span>
                                                                                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold sectionpad" width="35%">
                                                                                                    Year Six
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="18%">
                                                                                                    <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtYear3" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                    &nbsp;&nbsp;<asp:CheckBox ID="chkNAYear6" runat="server" Text="N/A" />
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtYear3" runat="server" FilterType="Numbers"
                                                                                                        TargetControlID="txtYear3">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <asp:RegularExpressionValidator Display="None" ValidationGroup="Financial" ID="regvYear3"
                                                                                                        ControlToValidate="txtYear3" runat="server" ErrorMessage="Please enter valid year 3"
                                                                                                        ValidationExpression="\d{4}" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    <asp:CustomValidator ID="cusvYearSix" runat="server" Display="None" ValidationGroup="Financial"
                                                                                                        OnServerValidate="cusvYearSix_ServerValidate"></asp:CustomValidator>
                                                                                                </td>
                                                                                                <td width="24%">
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trExplainYear6" runat="server" style="display: none">
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                                </td>
                                                                                                <td class="bodytextleft" colspan="3">
                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplainYear6" TextMode="MultiLine" runat="server"
                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    &nbsp; Maximum Contract Value Completed $
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="23%">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtMaxContractValueYear3"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtContractValue5" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtMaxContractValueYear3">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    Annual Company Revenue $
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtAnuualCompRevenueYear3"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAnnualRevenue5" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtAnuualCompRevenueYear3">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <%--<asp:CustomValidator ID="cusvAnnualRevenueYear6" runat="server" ValidationGroup="Financial"
                                                                                                                    Display="None" ErrorMessage="Annual company revenue should be greater than maximum contract value completed under year six"
                                                                                                                    OnServerValidate="cusvAnnualRevenueYear6_ServerValidate"></asp:CustomValidator>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" height="8px">
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="spnYearMore4" runat="server" style="display: none">
                                                                                <td colspan="4" style="padding-left: 1px">
                                                                                    <span>
                                                                                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold sectionpad" width="35%">
                                                                                                    Year Seven
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="18%">
                                                                                                    <asp:TextBox CssClass="txtboxsmall" ID="txtYear4" TabIndex="0" runat="server" MaxLength="4"> </asp:TextBox>
                                                                                                    &nbsp;&nbsp;<asp:CheckBox ID="chkNAYear7" runat="server" Text="N/A" />
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtYear4" runat="server" FilterType="Numbers"
                                                                                                        TargetControlID="txtYear4">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <asp:RegularExpressionValidator Display="None" ValidationGroup="Financial" ID="regvYear4"
                                                                                                        ControlToValidate="txtYear4" runat="server" ErrorMessage="Please enter valid year 4"
                                                                                                        ValidationExpression="\d{4}" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    <asp:CustomValidator ID="cusvYearSeven" runat="server" Display="None" ValidationGroup="Financial"
                                                                                                        OnServerValidate="cusvYearSeven_ServerValidate"></asp:CustomValidator>
                                                                                                </td>
                                                                                                <td width="24%">
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trExplainYear7" runat="server" style="display: none">
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                                </td>
                                                                                                <td class="bodytextleft" colspan="3">
                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplainYear7" TextMode="MultiLine" runat="server"
                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    &nbsp; Maximum Contract Value Completed $
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="23%">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtMaxContractValueYear4"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtContractValue6" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtMaxContractValueYear4">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    Annual Company Revenue $
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtAnuualCompRevenueYear4"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAnnualRevenue6" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtAnuualCompRevenueYear4">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <%--<asp:CustomValidator ID="cusvAnnualRevenue7" runat="server" ValidationGroup="Financial"
                                                                                                                    Display="None" ErrorMessage="Annual company revenue should be greater than maximum contract value completed under year seven"
                                                                                                                    OnServerValidate="cusvAnnualRevenueYear7_ServerValidate"></asp:CustomValidator>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" class="tdheight">
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="spnYearMore5" runat="server" style="display: none">
                                                                                <td colspan="4" style="padding-left: 1px">
                                                                                    <span>
                                                                                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold sectionpad" width="35%">
                                                                                                    Year Eight
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="18%">
                                                                                                    <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtYear5" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                    &nbsp;&nbsp;<asp:CheckBox ID="chkNAYear8" runat="server" Text="N/A" />
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtYear5" runat="server" FilterType="Numbers"
                                                                                                        TargetControlID="txtYear5">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <asp:RegularExpressionValidator Display="None" ValidationGroup="Financial" ID="regvYear5"
                                                                                                        ControlToValidate="txtYear5" runat="server" ErrorMessage="Please enter valid year 5"
                                                                                                        ValidationExpression="\d{4}" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    <asp:CustomValidator ID="cusvYearEight" runat="server" Display="None" ValidationGroup="Financial"
                                                                                                        OnServerValidate="cusvYearEight_ServerValidate"></asp:CustomValidator>
                                                                                                </td>
                                                                                                <td width="24%">
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trExplainYear8" runat="server" style="display: none">
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                                </td>
                                                                                                <td class="bodytextleft" colspan="3">
                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplainYear8" TextMode="MultiLine" runat="server"
                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    &nbsp; Maximum Contract Value Completed $
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="23%">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtMaxContractValueYear5"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtContractValue7" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtMaxContractValueYear5">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    Annual Company Revenue $
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtAnuualCompRevenueYear5"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAnnualRevenue7" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtAnuualCompRevenueYear5">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <%--<asp:CustomValidator ID="cusvAnnualRevenueYear8" runat="server" ValidationGroup="Financial"
                                                                                                                    Display="None" ErrorMessage="Annual company revenue should be greater than maximum contract value completed under year eight"
                                                                                                                    OnServerValidate="cusvAnnualRevenueYear8_ServerValidate"></asp:CustomValidator>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" class="tdheight">
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="spnYearMore6" runat="server" style="display: none">
                                                                                <td colspan="4" style="padding-left: 1px">
                                                                                    <span>
                                                                                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold sectionpad" width="35%">
                                                                                                    Year Nine
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="18%">
                                                                                                    <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtYear6" runat="server" MaxLength="4"> </asp:TextBox>
                                                                                                    &nbsp;&nbsp;<asp:CheckBox ID="chkNAYear9" runat="server" Text="N/A" />
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtYear6" runat="server" FilterType="Numbers"
                                                                                                        TargetControlID="txtYear6">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <asp:RegularExpressionValidator Display="None" ValidationGroup="Financial" ID="regvYear6"
                                                                                                        ControlToValidate="txtYear6" runat="server" ErrorMessage="Please enter valid year 6"
                                                                                                        ValidationExpression="\d{4}" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    <asp:CustomValidator ID="cusvYearnine" runat="server" Display="None" ValidationGroup="Financial"
                                                                                                        OnServerValidate="cusvYearnine_ServerValidate"></asp:CustomValidator>
                                                                                                </td>
                                                                                                <td width="24%">
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trExplainYear9" runat="server" style="display: none">
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                                </td>
                                                                                                <td class="bodytextleft" colspan="3">
                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplainYear9" TextMode="MultiLine" runat="server"
                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    &nbsp; Maximum Contract Value Completed $
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="23%">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtMaxContractValueYear6"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtContractValue8" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtMaxContractValueYear6">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    Annual Company Revenue $
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtAnuualCompRevenueYear6"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAnnualRevernue8" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtAnuualCompRevenueYear6">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <%--<asp:CustomValidator ID="cusvAnnualRevenueYear9" runat="server" ValidationGroup="Financial"
                                                                                                                    Display="None" ErrorMessage="Annual company revenue should be greater than maximum contract value completed under year nine"
                                                                                                                    OnServerValidate="cusvAnnualRevenueYear9_ServerValidate"></asp:CustomValidator>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" class="tdheight">
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="spnYearMore7" runat="server" style="display: none">
                                                                                <td colspan="4" style="padding-left: 1px">
                                                                                    <span>
                                                                                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold sectionpad" width="35%">
                                                                                                    Year Ten
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="18%">
                                                                                                    <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtYear7" runat="server" MaxLength="4"></asp:TextBox>
                                                                                                    &nbsp;&nbsp;<asp:CheckBox ID="chkNAYear10" runat="server" Text="N/A" />
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtYear7" runat="server" FilterType="Numbers"
                                                                                                        TargetControlID="txtYear7">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <asp:RegularExpressionValidator Display="None" ValidationGroup="Financial" ID="regvYear7"
                                                                                                        ControlToValidate="txtYear7" runat="server" ErrorMessage="Please enter valid year 7"
                                                                                                        ValidationExpression="\d{4}" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    <asp:CustomValidator ID="cusvYearTen" runat="server" Display="None" ValidationGroup="Financial"
                                                                                                        OnServerValidate="cusvYearTen_ServerValidate"></asp:CustomValidator>
                                                                                                </td>
                                                                                                <td width="24%">
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trExplainYear10" runat="server" style="display: none">
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    <span class="mandatorystar">*</span>Please Explain
                                                                                                </td>
                                                                                                <td class="bodytextleft" colspan="3">
                                                                                                    <asp:TextBox CssClass="txtboxlarge" ID="txtExplainYear10" TextMode="MultiLine" runat="server"
                                                                                                        onFocus="javascript:textCounter(this,1000,'yes');" onKeyDown="javascript:textCounter(this,1000,'yes');"
                                                                                                        onKeyUp="javascript:textCounter(this,1000,'yes');" TabIndex="0"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextleft sectionpad">
                                                                                                    &nbsp; Maximum Contract Value Completed $
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="23%">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtMaxContractValueYear7"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtContractValue9" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtMaxContractValueYear7">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    Annual Company Revenue $
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtAnuualCompRevenueYear7"
                                                                                                        runat="server"></asp:TextBox>
                                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtAnnualRevenue9" runat="server" FilterType="Numbers,Custom"
                                                                                                        ValidChars="," TargetControlID="txtAnuualCompRevenueYear7">
                                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                                    <%--<asp:CustomValidator ID="cusvAnnualRevenue10" runat="server" ValidationGroup="Financial"
                                                                                                                    Display="None" ErrorMessage="Annual company revenue should be greater than maximum contract value completed under year ten"
                                                                                                                    OnServerValidate="cusvAnnualRevenueYear10_ServerValidate"></asp:CustomValidator>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" class="tdheight">
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- Ends Here -->
                                                                            <tr>
                                                                                <td class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="5" align="center">
                                                                                    <asp:LinkButton ID="lnkYearAddMore" runat="server" CssClass="hyplinks">Click to Add More Information</asp:LinkButton>
                                                                                    <asp:HiddenField ID="hdnCount" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytextbold left_border">
                                                                                    <span class="mandatorystar">*</span>Current Year Projected Revenue $
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtCurrentProjectedRevenue"
                                                                                        runat="server"></asp:TextBox>
                                                                                    <%--<asp:RequiredFieldValidator ID="reqvCurrentProjectedRevenue" ValidationGroup="Financial"
                                                                                                    runat="server" SetFocusOnError="true" Display="None" ControlToValidate="txtCurrentProjectedRevenue"
                                                                                                    ErrorMessage="Please enter current year projected revenue"></asp:RequiredFieldValidator>--%>
                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtCurrentRevenue" runat="server" FilterType="Numbers,Custom"
                                                                                        ValidChars="," TargetControlID="txtCurrentProjectedRevenue">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                </td>
                                                                                <td class="bodytextbold">
                                                                                    <span class="mandatorystar">*</span>Current Total Backlog $
                                                                                </td>
                                                                                <td class="bodytextleft">
                                                                                    <asp:TextBox CssClass="txtboxmedium1" TabIndex="0" MaxLength="15" ID="txtCurrentTotalBacking"
                                                                                        runat="server"></asp:TextBox>
                                                                                    <%--<asp:RequiredFieldValidator ID="reqvCurrentTotalBacking" ValidationGroup="Financial"
                                                                                                    runat="server" SetFocusOnError="true" Display="None" ControlToValidate="txtCurrentTotalBacking"
                                                                                                    ErrorMessage="Please enter current total backlog"></asp:RequiredFieldValidator>--%>
                                                                                    <Ajax:FilteredTextBoxExtender ID="ftxtCurrentBacklog" runat="server" FilterType="Numbers,Custom"
                                                                                        ValidChars="," TargetControlID="txtCurrentTotalBacking">
                                                                                    </Ajax:FilteredTextBoxExtender>
                                                                                    <%--<asp:CompareValidator ID="compvCurrentBacklog" ValidationGroup="Financial" runat="server"
                                                                                                    SetFocusOnError="true" Display="None" ControlToValidate="txtCurrentTotalBacking"
                                                                                                    ControlToCompare="txtCurrentProjectedRevenue" Type="Currency" Operator="GreaterThan"
                                                                                                    ErrorMessage="Current Total Backlog should be greater than Current Year Projected Revenue "></asp:CompareValidator>--%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                                <td colspan="2" class="bodytextleft">
                                                                                    (All work under contract not yet completed)
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </Content>
                                                </Ajax:AccordionPane>
                                                <Ajax:AccordionPane ID="apnBanking" runat="server">
                                                    <Header>
                                                        Banking</Header>
                                                    <Content>
                                                        <table border="0" cellpadding="5" cellspacing="5" width="100%" class="searchtableclass">
                                                            <tr>
                                                                <td colspan="4" class="left_border formtdbold_rt">
                                                                    Banking References: Provide at least one
                                                                </td>
                                                            </tr>
                                                              <%--003-Sooraj - International Currency--%>
                                                             <tr id="trDenominateinDollar" runat="server" style="display:none">
                                                                <td colspan="4" ><span>
                                                               
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="bodytextleft" width="15%">
                                                                                Base Currency:
                                                                            </td>
                                                                            <td class="bodytextleft" width="28%">
                                                                                <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="ddlboxCountry" >

                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td class="bodytextleft" width="27%">
                                                                                Will you denominate in dollars?
                                                                            </td>
                                                                            <td class="bodytextleft" width="30%">
                                                                                <asp:RadioButtonList ID="rdbDenominateInDollars" runat="server" CssClass="radiobtn" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                                                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                    </table></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 0px 0px 0px 10px">
                                                                    <table align="center" border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td class="tdheight">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="keyheader">
                                                                                <span class="mandatorystar">*</span>Financial Institution One
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="keyleft_border">
                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                        Financial Institution Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                        Established Line of Credit?
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold">
                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="100" ID="txtFinancialInstitutionYearOne"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                        <%--<asp:RequiredFieldValidator ID="reqvFinancial" ValidationGroup="Banking" runat="server"
                                                                                                                        SetFocusOnError="true" Display="None" ControlToValidate="txtFinancialInstitutionYearOne"
                                                                                                                        ErrorMessage="Please enter name for financial institution 1"></asp:RequiredFieldValidator>--%>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:RadioButtonList ID="rdlLineofCreditOne" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                        <%--<asp:RequiredFieldValidator ID="reqvLineOfcredit" ValidationGroup="Banking" runat="server"
                                                                                                                        SetFocusOnError="true" Display="None" ControlToValidate="rdlLineofCreditOne"
                                                                                                                        ErrorMessage="Please select line of credit for financial institution 1"></asp:RequiredFieldValidator>--%>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                <tr>
                                                                                                    <td class="bodytextleft" width="30%">
                                                                                                        Address
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="17%">
                                                                                                        City
                                                                                                    </td>
                                                                                                      <td class="bodytextleft" width="20%">
                                                                                                        Country
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="16%">
                                                                                                        State
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        ZIP/Postal Code
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxlarge" MaxLength="100" TabIndex="0" ID="txtAddressYearOne"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                        <%--<asp:RequiredFieldValidator ID="reqvAddress" ValidationGroup="Banking" runat="server"
                                                                                                                        SetFocusOnError="true" Display="None" ControlToValidate="txtAddressYearOne" ErrorMessage="Please enter address for financial institution 1"></asp:RequiredFieldValidator>--%>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxmedium" TabIndex="0" MaxLength="50" ID="txtCityOne"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                        <%--<asp:RequiredFieldValidator ID="reqvCity" ValidationGroup="Banking" runat="server"
                                                                                                                        SetFocusOnError="true" Display="None" ControlToValidate="txtCityOne" ErrorMessage="Please enter city for financial institution 1"></asp:RequiredFieldValidator>--%>
                                                                                                    </td>
                                                                                                      <td class="bodytextleft">
                                                                                                                            <asp:DropDownList ID="ddlCountryYearOne" runat="server" CssClass="ddlboxCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                                                                                                                TabIndex="0" AutoPostBack="true" >
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>

                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:DropDownList ID="ddlStateYearOne" runat="server" TabIndex="0" CssClass="ddlboxState"
                                                                                                            AutoPostBack="false">
                                                                                                        </asp:DropDownList>
                                                                                                        <asp:CustomValidator ID="cusState1" runat="server" ValidationGroup="Banking" Display="None"
                                                                                                            OnServerValidate="custState1_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtZIPOne" TabIndex="0" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtZip1" runat="server" TargetControlID="txtZIPOne"
                                                                                                            FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <%--<asp:RequiredFieldValidator ID="reqvZIPOne" ValidationGroup="Banking" runat="server"
                                                                                                                        SetFocusOnError="true" Display="None" ControlToValidate="txtZIPOne" ErrorMessage="Please enter ZIP for financial institution 1"></asp:RequiredFieldValidator>--%>
                                                                                                        <asp:RegularExpressionValidator Display="None" ID="regvMAZip2" ControlToValidate="txtZIPOne"
                                                                                                            ValidationGroup="Banking" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code for financial institution 1"
                                                                                                            ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trOtherStateOne" runat="server" style="display: none">
                                                                                        <td>
                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                <tr>
                                                                                                    <td class="bodytextright">
                                                                                                        Please Specify
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="43%">
                                                                                                        <asp:TextBox ID="txtOtherState" TabIndex="0" MaxLength="50" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherState" TargetControlID="txtOtherState"
                                                                                                            FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                            InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>

                                                                                 <tr>
                                                                                 <td>
                                                                                <table cellpadding="3" cellspacing="0" border="0" width="100%">

                                                                                 
                                                                                     <tr>
                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                        Contact Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                        Contact Phone Number
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold">
                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="50" ID="txtContactNameOne"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                        <%--<asp:RequiredFieldValidator ID="reqvContactNameOne" ValidationGroup="Banking" runat="server"
                                                                                                                        SetFocusOnError="true" Display="None" ControlToValidate="txtContactNameOne" ErrorMessage="Please enter contact name for financial institution 1"></asp:RequiredFieldValidator>--%>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox ID="txtMainPhno1" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                            MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtMainPhno1" runat="server" TargetControlID="txtMainPhno1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox ID="txtMainPhno2" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                         <span id="spnMainPhno2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtMainPhno2" runat="server" TargetControlID="txtMainPhno2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox ID="txtMainPhno3" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtMainPhno3" runat="server" TargetControlID="txtMainPhno3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusphone6" runat="server" Display="None" ValidationGroup="Banking"
                                                                                                            EnableClientScript="false" OnServerValidate="cusphone6_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                 
                                                                                 </table>
                                                                                 </td>
                                                                                 </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tdheight">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 0px 0px 0px 10px">
                                                                    <table align="center" border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td class="keyheader">
                                                                                Financial Institution Two
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tdheight">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="keyleft_border">
                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                        Financial Institution Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                        Established Line of Credit?
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold">
                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="100" ID="txtFinancialInstitutionYearOne1"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:RadioButtonList ID="rdlLineofCreditOne1" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                              
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                <tr>
                                                                                                    <td class="bodytextleft" width="30%">
                                                                                                        Address
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="17%">
                                                                                                        City
                                                                                                    </td>
                                                                                                     <td class="bodytextleft" width="17%">
                                                                                                                    Country
                                                                                                   </td>
                                                                                                    <td class="bodytextleft" width="16%">
                                                                                                        State
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        ZIP/Postal Code
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxlarge" MaxLength="100" TabIndex="0" ID="txtAddressYearOne1"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxmedium" TabIndex="0" MaxLength="50" ID="txtCityOne1"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                    </td>
                                                                                                      <td class="bodytextleft">
                                                                                                                            <asp:DropDownList ID="ddlCountryYearOne1" runat="server" CssClass="ddlboxCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                                                                                                                TabIndex="0" AutoPostBack="true" >
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:DropDownList ID="ddlStateYearOne1" TabIndex="0" runat="server" CssClass="ddlboxState"
                                                                                                            AutoPostBack="false">
                                                                                                        </asp:DropDownList>
                                                                                                        <asp:CustomValidator ID="cusStateOne1" runat="server" ValidationGroup="Banking" Display="None"
                                                                                                            OnServerValidate="custStateOne1_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtZIPOne1" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtZIPOne1" runat="server" TargetControlID="txtZIPOne1"
                                                                                                            FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:RegularExpressionValidator Display="None" ID="regvZipOne1" ControlToValidate="txtZIPOne1"
                                                                                                            ValidationGroup="Banking" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code for financial institution 2"
                                                                                                            ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trOtherStateOne1" runat="server" style="display: none">
                                                                                        <td>
                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                <tr>
                                                                                                    <td class="bodytextright">
                                                                                                        Please Specify
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="43%">
                                                                                                        <asp:TextBox ID="txtOtherState1" MaxLength="50" runat="server" TabIndex="0" CssClass="txtbox"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherState1" TargetControlID="txtOtherState1"
                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                            InvalidChars="_-#.$">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                    <td>
                                                                                    <table cellpadding="3" cellspacing="0" border="0" width="100%">

                                                                                      <tr>
                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                        Contact Name
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                        Contact Phone Number
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold">
                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="50" ID="txtContactNameOne1"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox ID="txtRefOne1Phno1" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                            MaxLength="3"></asp:TextBox>&nbsp;-
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefOne1Phno1" runat="server" TargetControlID="txtRefOne1Phno1"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox ID="txtRefOne1Phno2" runat="server" CssClass="txtboxsmall" TabIndex="0"
                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                          <span id="spnRefOne1Phno2" runat="server">-</span>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefOne1Phno2" runat="server" TargetControlID="txtRefOne1Phno2"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:TextBox ID="txtRefOne1Phno3" runat="server" CssClass="txtboxsmall" MaxLength="4"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefOne1Phno3" runat="server" TargetControlID="txtRefOne1Phno3"
                                                                                                            FilterType="Numbers">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                        <asp:CustomValidator ID="cusphone7" runat="server" Display="None" ValidationGroup="Banking"
                                                                                                            EnableClientScript="false" OnServerValidate="cusphone7_ServerValidate"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                    </table>
                                                                                    </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tdheight">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td id="spnmore1" runat="server" style="display: none; padding: 0px 0px 0px 10px">
                                                                                <span>
                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="keyheader">
                                                                                                Financial Institution Three
                                                                                            </td>
                                                                                            <tr>
                                                                                                <td class="tdheight">
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="keyleft_border">
                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Financial Institution Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Established Line of Credit?
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="100" ID="txtFinancialInstitutionYearTwo"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:RadioButtonList ID="rdlLineofCreditTwo" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                                        </asp:RadioButtonList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                             
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="30%">
                                                                                                                        Address
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="17%">
                                                                                                                        City
                                                                                                                    </td>
                                                                                                                        <td class="bodytextleft" width="17%">
Country
        </td>
                                                                                                                    <td class="bodytextleft" width="16%">
                                                                                                                        State
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        ZIP/Postal Code
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge" MaxLength="100" TabIndex="0" ID="txtAddressYearTwo"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxmedium" ID="txtCityTwo" MaxLength="50" TabIndex="0"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                            <td class="bodytextleft">
                                                                                                                            <asp:DropDownList Width="130px" ID="ddlCountryYearTwo" runat="server" CssClass="ddlbox" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                                                                                                                TabIndex="0" AutoPostBack="true" >
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:DropDownList ID="ddlStateYearTwo" TabIndex="0" runat="server" CssClass="ddlboxState"
                                                                                                                            AutoPostBack="false">
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:CustomValidator ID="cusState2" runat="server" ValidationGroup="Banking" Display="None"
                                                                                                                            OnServerValidate="custState2_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="100px">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtZIPTwo" TabIndex="0" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtZipTwo" runat="server" FilterType="Custom"
                                                                                                                            ValidChars="1,2,3,4,5,6,7,8,9,0" TargetControlID="txtZIPTwo">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:RegularExpressionValidator Display="None" ID="regvZipTwo" ControlToValidate="txtZIPTwo"
                                                                                                                            ValidationGroup="Banking" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code for financial institution 3"
                                                                                                                            ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="trOtherStateTwo" runat="server" style="display: none">
                                                                                                        <td>
                                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextright">
                                                                                                                        Please Specify
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="43%">
                                                                                                                        <asp:TextBox ID="txtOtherStateTwo" MaxLength="50" TabIndex="0" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateTwo" TargetControlID="txtOtherStateTwo"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                    <td>
                                                                                                    <table cellpadding="3" cellspacing="0" border="0" width="100%">

                                                                                                       <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Contact Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Contact Phone Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" MaxLength="50" ID="txtContactNameTwo" TabIndex="0"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox ID="txtReftwoPhno1" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        -
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtReftwoPhno1" runat="server" TargetControlID="txtReftwoPhno1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtReftwoPhno2" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                       <span id="spnReftwoPhno2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtReftwoPhno2" runat="server" TargetControlID="txtReftwoPhno2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtReftwoPhno3" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtReftwoPhno3" runat="server" TargetControlID="txtReftwoPhno3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone8" runat="server" Display="None" ValidationGroup="Banking"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone8_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                    </table>
                                                                                                    </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td id="spnmore2" runat="server" style="display: none; padding: 0px 0px 0px 10px">
                                                                                <span>
                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="keyheader">
                                                                                                Financial Institution Four
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="keyleft_border">
                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Financial Institution Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Established Line of Credit?
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="100" ID="txtFinancialInstitutionYearThree"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:RadioButtonList ID="rdlLineofCreditThree" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                                        </asp:RadioButtonList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                              
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="30%">
                                                                                                                        Address
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="17%">
                                                                                                                        City
                                                                                                                    </td>
                                                                                                                          <td class="bodytextleft" width="17%">
                                                                                                                    Country
                                                                                                                </td>
                                                                                                                    <td class="bodytextleft" width="16%">
                                                                                                                        State
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        ZIP/Postal Code
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge" TabIndex="0" MaxLength="100" ID="txtAddressYearThree"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxmedium" TabIndex="0" MaxLength="50" ID="txtCityThree"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                         <td class="bodytextleft">
                                                                                                                            <asp:DropDownList ID="ddlCountryYearThree" runat="server" CssClass="ddlboxCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                                                                                                                TabIndex="0" AutoPostBack="true" >
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:DropDownList ID="ddlStateYearThree" TabIndex="0" runat="server" CssClass="ddlboxState"
                                                                                                                            AutoPostBack="false">
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:CustomValidator ID="cusState3" runat="server" ValidationGroup="Banking" Display="None"
                                                                                                                            OnServerValidate="custState3_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtZIPThree" runat="server"
                                                                                                                            MaxLength="5"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtZipThree" runat="server" FilterType="Custom"
                                                                                                                            ValidChars="1,2,3,4,5,6,7,8,9,0" TargetControlID="txtZIPThree">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:RegularExpressionValidator Display="None" ID="regvZipThree" ValidationGroup="Banking"
                                                                                                                            ControlToValidate="txtZIPThree" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code for financial institution 4"
                                                                                                                            ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="trOtherStateThree" runat="server" style="display: none">
                                                                                                        <td>
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextright">
                                                                                                                        Please Specify
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="43%">
                                                                                                                        <asp:TextBox MaxLength="50" ID="txtOtherStateThree" TabIndex="0" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateThree" TargetControlID="txtOtherStateThree"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                    <td>
                                                                                                    <table cellpadding="3" cellspacing="0" border="0" width="100%">

                                                                                                      <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Contact Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Contact Phone Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="50" ID="txtContactNameThree"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox ID="txtRefthreePhno1" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        -
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefthreePhno1" runat="server" TargetControlID="txtRefthreePhno1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefthreePhno2" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                       <span id="spnRefthreePhno2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefthreePhno2" runat="server" TargetControlID="txtRefthreePhno2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefthreePhno3" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefthreePhno3" runat="server" TargetControlID="txtRefthreePhno3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone17" runat="server" Display="None" ValidationGroup="Banking"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone17_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                    </table>
                                                                                                    </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td id="spnmore3" runat="server" style="display: none; padding: 0px 0px 0px 10px">
                                                                                <span>
                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="keyheader">
                                                                                                Financial Institution Five
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="keyleft_border">
                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Financial Institution Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Established Line of Credit?
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="100" ID="txtFinancialInstitutionYearFour"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:RadioButtonList ID="rdlLineofCreditFour" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                                        </asp:RadioButtonList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Address
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="17%">
                                                                                                                        City
                                                                                                                    </td>    <td class="bodytextleft" width="17%">
Country
        </td>
                                                                                                                    <td class="bodytextleft" width="16%">
                                                                                                                        State
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        ZIP/Postal Code
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge" TabIndex="0" MaxLength="100" ID="txtAddressYearFour"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxmedium" TabIndex="0" MaxLength="50" ID="txtCityFour"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                            <asp:DropDownList ID="ddlCountryYearFour" runat="server" CssClass="ddlboxCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                                                                                                                TabIndex="0" AutoPostBack="true" >
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:DropDownList ID="ddlStateYearFour" TabIndex="0" runat="server" CssClass="ddlboxState"
                                                                                                                            AutoPostBack="false">
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:CustomValidator ID="cusState4" runat="server" ValidationGroup="Banking" Display="None"
                                                                                                                            OnServerValidate="custState4_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtZIPFour" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtZipFour" runat="server" FilterType="Custom"
                                                                                                                            ValidChars="1,2,3,4,5,6,7,8,9,0" TargetControlID="txtZIPFour">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:RegularExpressionValidator Display="None" ValidationGroup="Banking" ID="regvZipFour"
                                                                                                                            ControlToValidate="txtZIPFour" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code for financial institution 5"
                                                                                                                            ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="trOtherStateFour" runat="server" style="display: none">
                                                                                                        <td>
                                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextright">
                                                                                                                        Please Specify
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="43%">
                                                                                                                        <asp:TextBox MaxLength="50" ID="txtOtherStateFour" TabIndex="0" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateFour" TargetControlID="txtOtherStateFour"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                    <td>
                                                                                               <table cellpadding="3" cellspacing="0" border="0" width="100%">

                                                                                                        <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Contact Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Contact Phone Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="50" ID="txtContactNameFour"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox ID="txtRefFourPhno1" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        -
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefFourPhno1" runat="server" TargetControlID="txtRefFourPhno1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefFourPhno2" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                          <span id="spnRefFourPhno2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefFourPhno2" runat="server" TargetControlID="txtRefFourPhno2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefFourPhno3" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefFourPhno3" runat="server" TargetControlID="txtRefFourPhno3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone9" runat="server" Display="None" ValidationGroup="Banking"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone9_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                    </table>
                                                                                                    </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td id="spnmore4" runat="server" style="display: none; padding: 0px 0px 0px 10px">
                                                                                <span>
                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="keyheader">
                                                                                                Financial Institution Six
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="padding-left: 15px">
                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Financial Institution Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Established Line of Credit?
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="100" ID="txtFinancialInstitutionYearFive"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:RadioButtonList ID="rdlLineofCreditFive" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                                        </asp:RadioButtonList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                              
                                                                                                            </table>
                                                                                                        </td>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextleft" width="40%">
                                                                                                                            Address
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft" width="17%">
                                                                                                                            City
                                                                                                                        </td>
                                                                                                                            <td class="bodytextleft" width="17%">
Country
        </td>
                                                                                                                        <td class="bodytextleft" width="16%">
                                                                                                                            State
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            ZIP/Postal Code
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <asp:TextBox CssClass="txtboxlarge" TabIndex="0" MaxLength="100" ID="txtAddressYearFive"
                                                                                                                                runat="server"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <asp:TextBox CssClass="txtboxmedium" TabIndex="0" MaxLength="50" ID="txtCityFive"
                                                                                                                                runat="server"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <asp:DropDownList ID="ddlCountryYearFive" runat="server" CssClass="ddlboxCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                                                                                                                TabIndex="0" AutoPostBack="true" >
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <asp:DropDownList ID="ddlStateYearFive" TabIndex="0" runat="server" CssClass="ddlboxState"
                                                                                                                                AutoPostBack="false">
                                                                                                                            </asp:DropDownList>
                                                                                                                            <asp:CustomValidator ID="cusState5" runat="server" ValidationGroup="Banking" Display="None"
                                                                                                                                OnServerValidate="custState5_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtZIPFive" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtZipFive" runat="server" FilterType="Custom"
                                                                                                                                ValidChars="1,2,3,4,5,6,7,8,9,0" TargetControlID="txtZIPFive">
                                                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                                                            <asp:RegularExpressionValidator Display="None" ValidationGroup="Banking" ID="regvZipFive"
                                                                                                                                ControlToValidate="txtZIPFive" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code for financial institution 6"
                                                                                                                                ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trOtherStateFive" runat="server" style="display: none">
                                                                                                            <td>
                                                                                                                <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextright">
                                                                                                                            Please Specify
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft" width="43%">
                                                                                                                            <asp:TextBox MaxLength="50" ID="txtOtherStateFive" TabIndex="0" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                                            <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateFive" TargetControlID="txtOtherStateFive"
                                                                                                                                FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                                InvalidChars="_-#.$">
                                                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                        <td>
                                                                                                      <table cellpadding="3" cellspacing="0" border="0" width="100%">

                                                                                                          <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Contact Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Contact Phone Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="50" ID="txtContactNameFive"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox ID="txtRefFivePhno1" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        -
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefFivePhno1" runat="server" TargetControlID="txtRefFivePhno1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefFivePhno2" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                       <span id="spnRefFivePhno2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefFivePhno2" runat="server" TargetControlID="txtRefFivePhno2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefFivePhno3" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefFivePhno3" runat="server" TargetControlID="txtRefFivePhno3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone10" runat="server" Display="None" ValidationGroup="Banking"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone10_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                        </table>
                                                                                                        </td>
                                                                                                        </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td id="spnmore5" runat="server" style="display: none; padding: 0px 0px 0px 10px">
                                                                                <span>
                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="keyheader">
                                                                                                Financial Institution Seven
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="keyleft_border">
                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Financial Institution Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Established Line of Credit?
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" MaxLength="100" ID="txtFinancialInstitutionYearSix"
                                                                                                                            TabIndex="0" runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:RadioButtonList ID="rdlLineofCreditSix" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                                        </asp:RadioButtonList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                             
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Address
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="17%">
                                                                                                                        City
                                                                                                                    </td>    <td class="bodytextleft" width="17%">
Country
        </td>
                                                                                                                    <td class="bodytextleft" width="16%">
                                                                                                                        State
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        ZIP/Postal Code
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge" MaxLength="100" ID="txtAddressYearSix" runat="server"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxmedium" MaxLength="50" ID="txtCitySix" runat="server"
                                                                                                                            TabIndex="0"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                            <asp:DropDownList ID="ddlCountryYearSix" runat="server" CssClass="ddlboxCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                                                                                                                TabIndex="0" AutoPostBack="true" >
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:DropDownList ID="ddlStateYearSix" runat="server" CssClass="ddlboxState" TabIndex="0"
                                                                                                                            AutoPostBack="false">
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:CustomValidator ID="cusState6" runat="server" ValidationGroup="Banking" Display="None"
                                                                                                                            OnServerValidate="custState6_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" ID="txtZIPSix" TabIndex="0" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtZipSix" runat="server" FilterType="Custom"
                                                                                                                            ValidChars="1,2,3,4,5,6,7,8,9,0" TargetControlID="txtZIPSix">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:RegularExpressionValidator Display="None" ValidationGroup="Banking" ID="regvZipSix"
                                                                                                                            ControlToValidate="txtZIPSix" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code for financial institution 7"
                                                                                                                            ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="trOtherStateSix" runat="server" style="display: none">
                                                                                                        <td>
                                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextright">
                                                                                                                        Please Specify
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="43%">
                                                                                                                        <asp:TextBox MaxLength="50" ID="txtOtherStateSix" TabIndex="0" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateSix" TargetControlID="txtOtherStateSix"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                    <td>
                                                                                                  <table cellpadding="3" cellspacing="0" border="0" width="100%">

                                                                                                    
                                                                                                       <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Contact Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Contact Phone Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="50" ID="txtContactNameSix"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox ID="txtRefSixPhno1" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        -
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefSixPhno1" runat="server" TargetControlID="txtRefSixPhno1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefSixPhno2" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                       <span id="spnRefSixPhno2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefSixPhno2" runat="server" TargetControlID="txtRefSixPhno2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefSixPhno3" runat="server" CssClass="txtboxsmall" TabIndex="0"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefSixPhno3" runat="server" TargetControlID="txtRefSixPhno3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone11" runat="server" Display="None" ValidationGroup="Banking"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone11_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>

                                                                                                    </table>
                                                                                                    </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td id="spnmore6" runat="server" style="display: none; padding: 0px 0px 0px 10px">
                                                                                <span>
                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="keyheader">
                                                                                                Financial Institution Eight
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="keyleft_border">
                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Financial Institution Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Established Line of Credit?
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" MaxLength="100" ID="txtFinancialInstitutionYearSeven"
                                                                                                                            TabIndex="0" runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:RadioButtonList ID="rdlLineofCreditSeven" runat="server" TabIndex="0" RepeatDirection="Horizontal">
                                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                                        </asp:RadioButtonList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                           
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Address
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="17%">
                                                                                                                        City
                                                                                                                    </td>
                                                                                                                        <td class="bodytextleft" width="17%">
                                                                                                                    Country
                                                                                                                </td>
                                                                                                                    <td class="bodytextleft" width="16%">
                                                                                                                        State
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        ZIP/Postal Code
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox TabIndex="0" CssClass="txtboxlarge" MaxLength="100" ID="txtAddressYearSeven"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox TabIndex="0" CssClass="txtboxmedium" MaxLength="50" ID="txtCitySeven"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                      <td class="bodytextleft">
                                                                                                                            <asp:DropDownList ID="ddlCountryYearSeven" runat="server" CssClass="ddlboxCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                                                                                                                TabIndex="0" AutoPostBack="true" >
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:DropDownList TabIndex="0" ID="ddlStateYearSeven" runat="server" CssClass="ddlboxState"
                                                                                                                            AutoPostBack="false">
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:CustomValidator ID="cusState7" runat="server" ValidationGroup="Banking" Display="None"
                                                                                                                            OnServerValidate="custState7_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtZIPSeven" runat="server"
                                                                                                                            MaxLength="5"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtZipSeven" runat="server" FilterType="Custom"
                                                                                                                            ValidChars="1,2,3,4,5,6,7,8,9,0" TargetControlID="txtZIPSeven">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:RegularExpressionValidator Display="None" ValidationGroup="Banking" ID="regvZipSeven"
                                                                                                                            ControlToValidate="txtZIPSeven" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code for financial institution 8"
                                                                                                                            ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="trOtherStateSeven" runat="server" style="display: none">
                                                                                                        <td>
                                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextright">
                                                                                                                        Please Specify
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="43%">
                                                                                                                        <asp:TextBox MaxLength="50" ID="txtOtherStateSeven" TabIndex="0" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateSeven" TargetControlID="txtOtherStateSeven"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                    <td>
                                                                                                     <table cellpadding="3" cellspacing="0" border="0" width="100%">

                                                                                                         <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Contact Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Contact Phone Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="50" ID="txtContactNameSeven"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox ID="txtRefSevenPhno1" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        -
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefSevenPhno1" runat="server" TargetControlID="txtRefSevenPhno1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefSevenPhno2" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnRefSevenPhno2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefSevenPhno2" runat="server" TargetControlID="txtRefSevenPhno2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefSevenPhno3" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefSevenPhno3" runat="server" TargetControlID="txtRefSevenPhno3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone12" runat="server" Display="None" ValidationGroup="Banking"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone12_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr></table>
                                                                                                    </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td id="spnmore7" runat="server" style="display: none; padding: 0px 0px 0px 10px">
                                                                                <span>
                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="keyheader">
                                                                                                Financial Institution Nine
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="keyleft_border">
                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Financial Institution Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Established Line of Credit?
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="100" ID="txtFinancialInstitutionYearEight"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:RadioButtonList ID="rdlLineofCreditEight" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                                        </asp:RadioButtonList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                           
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Address
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="17%">
                                                                                                                        City
                                                                                                                    </td>
                                                                                                                      <td class="bodytextleft" width="17%">
                                                                                                                    Country
                                                                                                                </td>
                                                                                                                    <td class="bodytextleft" width="16%">
                                                                                                                        State
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        ZIP/Postal Code
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge" TabIndex="0" MaxLength="100" ID="txtAddressYearEight"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxmedium" TabIndex="0" MaxLength="50" ID="txtCityEight"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                      <td class="bodytextleft">
                                                                                                                            <asp:DropDownList ID="ddlCountryYearEight" runat="server" CssClass="ddlboxCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                                                                                                                TabIndex="0" AutoPostBack="true" >
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:DropDownList ID="ddlStateYearEight" TabIndex="0" runat="server" CssClass="ddlboxState"
                                                                                                                            AutoPostBack="false">
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:CustomValidator ID="cusState8" runat="server" ValidationGroup="Banking" Display="None"
                                                                                                                            OnServerValidate="custState8_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtZIPEight" runat="server"
                                                                                                                            MaxLength="5"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtzipEight" runat="server" FilterType="Custom"
                                                                                                                            ValidChars="1,2,3,4,5,6,7,8,9,0" TargetControlID="txtZIPEight">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:RegularExpressionValidator Display="None" ValidationGroup="Banking" ID="regvZipEight"
                                                                                                                            ControlToValidate="txtZIPEight" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code for financial institution 9"
                                                                                                                            ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="trOtherStateEight" runat="server" style="display: none">
                                                                                                        <td>
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextright">
                                                                                                                        Please Specify
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="43%">
                                                                                                                        <asp:TextBox MaxLength="50" ID="txtOtherStateEight" TabIndex="0" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateEight" TargetControlID="txtOtherStateEight"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                    <td>
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">

                                                                                                    
                                                                                                         <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Contact Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Contact Phone Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="50" ID="txtContactNameEight"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox ID="txtRefEightPhno1" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        -
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefEightPhno1" runat="server" TargetControlID="txtRefEightPhno1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefEightPhno2" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                         <span id="spnRefEightPhno2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefEightPhno2" runat="server" TargetControlID="txtRefEightPhno2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefEightPhno3" TabIndex="0" runat="server" CssClass="txtboxsmall"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefEightPhno3" runat="server" TargetControlID="txtRefEightPhno3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone13" runat="server" Display="None" ValidationGroup="Banking"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone13_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>

                                                                                                    </table>
                                                                                                    </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td id="spnmore8" runat="server" style="display: none; padding: 0px 0px 0px 10px">
                                                                                <span>
                                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="keyheader">
                                                                                                Financial Institution Ten
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="keyleft_border">
                                                                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Financial Institution Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="60%">
                                                                                                                        Established Line of Credit?
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="100" ID="txtFinancialInstitutionYearNine"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:RadioButtonList ID="rdlLineofCreditNine" TabIndex="0" runat="server" RepeatDirection="Horizontal">
                                                                                                                            <asp:ListItem>Yes</asp:ListItem>
                                                                                                                            <asp:ListItem>No</asp:ListItem>
                                                                                                                        </asp:RadioButtonList>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                             
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft" width="40%">
                                                                                                                        Address
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="17%">
                                                                                                                        City
                                                                                                                    </td>
                                                                                                                        <td class="bodytextleft" width="17%">
Country
        </td>
                                                                                                                    <td class="bodytextleft" width="16%">
                                                                                                                        State
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        ZIP/Postal Code
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxlarge" TabIndex="0" MaxLength="100" ID="txtAddressYearNine"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox TabIndex="0" CssClass="txtboxmedium" MaxLength="50" ID="txtCityNine"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                      <td class="bodytextleft">
                                                                                                                            <asp:DropDownList ID="ddlCountryYearNine" runat="server" CssClass="ddlboxCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                                                                                                                TabIndex="0" AutoPostBack="true" >
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:DropDownList ID="ddlStateYearNine" TabIndex="0" runat="server" CssClass="ddlboxState"
                                                                                                                            AutoPostBack="false">
                                                                                                                        </asp:DropDownList>
                                                                                                                        <asp:CustomValidator ID="cusState9" runat="server" ValidationGroup="Banking" Display="None"
                                                                                                                            OnServerValidate="custState9_ServerValidate" EnableClientScript="false"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox CssClass="txtboxsmall" TabIndex="0" ID="txtZIPNine" runat="server" MaxLength="5"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtZipNine" runat="server" FilterType="Custom"
                                                                                                                            ValidChars="1,2,3,4,5,6,7,8,9,0" TargetControlID="txtZIPNine">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:RegularExpressionValidator Display="None" ValidationGroup="Banking" ID="regvZipNine"
                                                                                                                            ControlToValidate="txtZIPNine" runat="server" ErrorMessage="Please enter valid ZIP/Postal Code for financial institution 10"
                                                                                                                            ValidationExpression="^[0-9a-zA-Z -]+$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="trOtherStateNine" runat="server" style="display: none">
                                                                                                        <td>
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextright">
                                                                                                                        Please Specify
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft" width="43%">
                                                                                                                        <asp:TextBox MaxLength="50" ID="txtOtherStateNine" TabIndex="0" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherStateNine" TargetControlID="txtOtherStateNine"
                                                                                                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                            InvalidChars="_-#.$">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                    <td>
                                                                                                  <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                    
                                                                                                       <tr>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Contact Name
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        Contact Phone Number
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold">
                                                                                                                        <asp:TextBox CssClass="txtbox" TabIndex="0" MaxLength="50" ID="txtContactNameNine"
                                                                                                                            runat="server"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleft">
                                                                                                                        <asp:TextBox ID="txtRefNinePhno1" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        -
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefNinePhno1" runat="server" TargetControlID="txtRefNinePhno1"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefNinePhno2" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                                            MaxLength="3"></asp:TextBox>
                                                                                                                        <span id="spnRefNinePhno2" runat="server">-</span>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefNinePhno2" runat="server" TargetControlID="txtRefNinePhno2"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:TextBox ID="txtRefNinePhno3" runat="server" TabIndex="0" CssClass="txtboxsmall"
                                                                                                                            MaxLength="4"></asp:TextBox>
                                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtRefNinePhno3" runat="server" TargetControlID="txtRefNinePhno3"
                                                                                                                            FilterType="Numbers">
                                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                                        <asp:CustomValidator ID="cusphone14" runat="server" Display="None" ValidationGroup="Banking"
                                                                                                                            EnableClientScript="false" OnServerValidate="cusphone14_ServerValidate"></asp:CustomValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                    
                                                                                                    </table>
                                                                                                    </td>
                                                                                                    </tr>

                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:LinkButton ID="imgAddMore" runat="server" CssClass="hyplinks">Click to Add More Information</asp:LinkButton>
                                                                                <asp:HiddenField ID="hdnCountValue" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </Content>
                                                </Ajax:AccordionPane>
                                            </Panes>
                                        </Ajax:Accordion></ContentTemplate></asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdheight">
                                        <asp:HiddenField ID="hdnLegalId" runat="server" />
                                        <asp:HiddenField ID="hdnLegalBankingId" runat="server" />
                                        <asp:HiddenField ID="hdnLegalFinancialId" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bodytextcenter">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <asp:ImageButton ID="imbSave" runat="server" CausesValidation="false" ImageUrl="~/Images/save.jpg"
                                                        ToolTip="Save" TabIndex="0" OnClick="imbSave_Click" />
                                                    <asp:ImageButton ID="imbSaveNext" runat="server" CausesValidation="false" ImageUrl="~/Images/save_nextnew.jpg"
                                                        ToolTip="Save and Next" OnClick="imbSaveNext_Click" TabIndex="0" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" CancelControlID="imbOk">
                            </Ajax:ModalPopupExtender>
                            <asp:HiddenField ID="lblHidden" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="modalPopupDiv" class="popupdivsmall" style="display: none;">
                                <table cellpadding="0" cellspacing="0" border="0" width="450px">
                                    <tr>
                                        <td class="searchhdrbarbold" runat="server" id="msgTd" colspan="4">
                                            <asp:Label ID="lblheading" runat="server" class="searchhdrbarbold"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl">
                                            <asp:Label ID="lblError" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                            <asp:Label ID="lblMsg" runat="server" CssClass="errormsg"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl" align="center">
                                            <input type="button" value="OK" id="imbOk" title="Ok" class="ModalPopupButton" onclick="$find('modalExtnd').hide();"
                                                runat="server" />
                                            <asp:ImageButton ID="imbOkoutlook" ImageAlign="AbsMiddle" ToolTip="OK" CausesValidation="false"
                                                OnClientClick="TriggerOutlook();" ImageUrl="~/Images/ok.jpg" runat="server" OnClick="imbOkoutlook_Click"
                                                Visible="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hdnVendorStatus" runat="server" />
                <Haskell:Footer ID="BottomMenu" runat="server" />
            </td>
        </tr>
    </table>
   
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

    <script language="javascript" type="text/javascript">

        //G. Vera 06/10/2014: Added
        function DisplayElement(cbxId, elementId) {
            var chbox = document.getElementById(cbxId);
            if (chbox.checked) {
                document.getElementById(elementId).style.display = "";
            }
            else {
                document.getElementById(elementId).style.display = "none";
            }
        }

        function DisplayExplain(chkna, trExplain, txtMaximum, txtAnnual, txtExplain) {

            var na = document.getElementById(chkna);
            if (na.checked == true) {
                document.getElementById(trExplain).style.display = "";
                document.getElementById(txtMaximum).value = "";
                document.getElementById(txtMaximum).disabled = true;
                document.getElementById(txtAnnual).value = "";
                document.getElementById(txtAnnual).disabled = true;
            }
            else {
                document.getElementById(trExplain).style.display = "none";

                document.getElementById(txtMaximum).disabled = false;
                document.getElementById(txtExplain).value = "";
                document.getElementById(txtAnnual).disabled = false;
            }
        }
        function OpenPrint() {
            window.open("../VQFView/LegalFinancialView.aspx?Print=1");
        }
        function numberFormat(nStr, prefix, ctrl) {

            var txt = document.getElementById(ctrl);
            if (txt.value != "0") {
                var prefix = prefix || '';

                nStr += '';
                nStr = nStr.replace(/^[0]+/g, "");
                y = nStr.split(',');
                var y1 = '';
                for (var i = 0; i < y.length; i++) {
                    y1 += y[i];
                }
                if (y.length > 0) {
                    nStr = y1;
                }
                x = nStr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1))
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');

                // txt.value="";
                // txt.value= prefix + x1 + x2;
                // alert(prefix + x1 + x2);
                //   return prefix + x1 + x2;
                txt.value = x1 + x2;
            }
            //validateRevenue();
        }


        function Visibletr() {
            var count = document.getElementById("<%=hdnCountValue.ClientID %>");
            var count1 = parseInt(count.value) + 1;
            count.value = count1;
            if (count.value == 1)
                document.getElementById("<%=spnmore1.ClientID %>").style.display = "";
            else if (count.value == 2)
                document.getElementById("<%=spnmore2.ClientID %>").style.display = "";
            else if (count.value == 3)
                document.getElementById("<%=spnmore3.ClientID %>").style.display = "";
            else if (count.value == 4)
                document.getElementById("<%=spnmore4.ClientID %>").style.display = "";
            else if (count.value == 5)
                document.getElementById("<%=spnmore5.ClientID %>").style.display = "";
            else if (count.value == 6)
                document.getElementById("<%=spnmore6.ClientID %>").style.display = "";
            else if (count.value == 7)
                document.getElementById("<%=spnmore7.ClientID %>").style.display = "";
            else if (count.value == 8) {
                document.getElementById("<%=spnmore8.ClientID %>").style.display = "";
                document.getElementById("<%=imgAddMore.ClientID %>").style.display = "none";
            }
            count.value = count1;
            return false;


        }
        function VisibleYear() {
            var count = document.getElementById("<%=hdnCount.ClientID %>");
            var count1 = parseInt(count.value) + 1;
            count.value = count1;
            if (count.value == 1)
                document.getElementById("<%=spnYearMore1.ClientID %>").style.display = "";
            else if (count.value == 2)
                document.getElementById("<%=spnYearMore2.ClientID %>").style.display = "";
            else if (count.value == 3)
                document.getElementById("<%=spnYearMore3.ClientID %>").style.display = "";
            else if (count.value == 4)
                document.getElementById("<%=spnYearMore4.ClientID %>").style.display = "";
            else if (count.value == 5)
                document.getElementById("<%=spnYearMore5.ClientID %>").style.display = "";
            else if (count.value == 6)
                document.getElementById("<%=spnYearMore6.ClientID %>").style.display = "";
            else if (count.value == 7) {
                document.getElementById("<%=spnYearMore7.ClientID %>").style.display = "";
                document.getElementById("<%=lnkYearAddMore.ClientID %>").style.display = "none";
            }
            count.value = count1;
            return false;
        }

        function ShowrdlListOne() {
            if (document.getElementById("<%=rdlListOne.ClientID%>_0").checked) {
                document.getElementById("<%=trWorkAwdOne.ClientID%>").style.display = "";
            }
            else if (document.getElementById("<%=rdlListOne.ClientID%>_1").checked) {
                document.getElementById("<%=trWorkAwdOne.ClientID%>").style.display = "none";
            }
        }

        function ShowrdlListTwo() {
            if (document.getElementById("<%=rdlListTwo.ClientID%>_0").checked) {
                document.getElementById("<%=trStock.ClientID%>").style.display = "";
            }
            else if (document.getElementById("<%=rdlListTwo.ClientID%>_1").checked) {
                document.getElementById("<%=trStock.ClientID%>").style.display = "none";
            }
        }

        function ShowrdlListThree() {
            if (document.getElementById("<%=rdlListThree.ClientID%>_0").checked) {
                document.getElementById("<%=trBankruptcy.ClientID%>").style.display = "";
            }
            else if (document.getElementById("<%=rdlListThree.ClientID%>_1").checked) {
                document.getElementById("<%=trBankruptcy.ClientID%>").style.display = "none";
            }
        }
        function ShowrdlListFour() {
            if (document.getElementById("<%=rdlListFour.ClientID%>_0").checked) {
                document.getElementById("<%=trConviction.ClientID%>").style.display = "";
            }
            else if (document.getElementById("<%=rdlListFour.ClientID%>_1").checked) {
                document.getElementById("<%=trConviction.ClientID%>").style.display = "none";
            }
        }

        function validateNumber(txtBox1) {
            if (document.getElementById(txtBox1).value != "") {
                var str = document.getElementById(txtBox1).value;
                var str1 = "0123456789.";
                var i = 0;
                for (i = 0; i < str.length; i++) {
                    if (str1.indexOf(str.substring(i, i + 1)) < 0 || str1.indexOf(str.substring(i, i + 1)) > str1.length || str.indexOf(".") != str.lastIndexOf(".")) {
                        document.getElementById(txtBox1).value = str.substring(0, str.length - 1);
                        alert("Invalid");
                        document.getElementById(txtBox1).focus();
                        break;
                    }
                }
            }
        }

        function checkDecimal(txtBox1) {
            if (document.getElementById(txtBox1).value != "") {
                var str2 = document.getElementById(txtBox1).value;
                var str3;
                if (str2.indexOf('.') < 0) {
                    str3 = str2 + ".00";
                    document.getElementById(txtBox1).value = roundNumber(str3, 2);
                }
                else {
                    document.getElementById(txtBox1).value = roundNumber(str2, 2);
                }
            }
        }
        function Close() {
            var x = $find("ModalAddDoc");
            if (x) { x.hide(); }
        }

        function validateRevenue() {
            // debugger; 
            var msg = "";
            var maxContractValue;
            var annualRevenue;
            if (!document.getElementById("<%=chkNAYear1.ClientID%>").checked) {
                maxContractValue = document.getElementById("<%=txtMaxContractValueYearOne.ClientID%>").value.replace(",", "");
                annualRevenue = document.getElementById("<%=txtAnuualCompRevenueYearOne.ClientID%>").value.replace(",", "");
                if (maxContractValue != "" && annualRevenue != "") {
                    if (parseFloat(maxContractValue) >= parseFloat(annualRevenue)) {
                        msg = msg + "<li>Annual company revenue should be greater than maximum contract value completed under year one</li>";
                    }
                }
            }
            if (!document.getElementById("<%=chkNAYear2.ClientID%>").checked) {
                maxContractValue = document.getElementById("<%=txtMaxContractValueYearTwo.ClientID%>").value.replace(",", "");
                annualRevenue = document.getElementById("<%=txtAnuualCompRevenueYearTwo.ClientID%>").value.replace(",", "");
                if (maxContractValue != "" && annualRevenue != "") {
                    if (parseFloat(maxContractValue) >= parseFloat(annualRevenue)) {
                        msg = msg + "<li>Annual company revenue should be greater than maximum contract value completed under year two</li>";
                    }
                }
            }
            if (!document.getElementById("<%=chkNAYear3.ClientID%>").checked) {
                maxContractValue = document.getElementById("<%=txtMaxContractValueYearThree.ClientID%>").value.replace(",", "");
                annualRevenue = document.getElementById("<%=txtAnuualCompRevenueYearThree.ClientID%>").value.replace(",", "");
                if (maxContractValue != "" && annualRevenue != "") {
                    if (parseFloat(maxContractValue) >= parseFloat(annualRevenue)) {
                        msg = msg + "<li>Annual company revenue should be greater than maximum contract value completed under year three</li>";
                    }
                }
            }
            if ((document.getElementById("<%=spnYearMore1.ClientID%>").style.display == "") && (!document.getElementById("<%=chkNAYear4.ClientID%>").checked)) {
                maxContractValue = document.getElementById("<%=txtMaxContractValueYear1.ClientID%>").value.replace(",", "");
                annualRevenue = document.getElementById("<%=txtAnuualCompRevenueYear1.ClientID%>").value.replace(",", "");
                if (maxContractValue != "" && annualRevenue != "") {
                    if (parseFloat(maxContractValue) >= parseFloat(annualRevenue)) {
                        msg = msg + "<li>Annual company revenue should be greater than maximum contract value completed under year four</li>";
                    }
                }
            }
            if ((document.getElementById("<%=spnYearMore2.ClientID%>").style.display == "") && (!document.getElementById("<%=chkNAYear5.ClientID%>").checked)) {
                maxContractValue = document.getElementById("<%=txtMaxContractValueYear2.ClientID%>").value.replace(",", "");
                annualRevenue = document.getElementById("<%=txtAnuualCompRevenueYear2.ClientID%>").value.replace(",", "");
                if (maxContractValue != "" && annualRevenue != "") {
                    if (parseFloat(maxContractValue) >= parseFloat(annualRevenue)) {
                        msg = msg + "<li>Annual company revenue should be greater than maximum contract value completed under year five</li>";
                    }
                }
            }
            if ((document.getElementById("<%=spnYearMore3.ClientID%>").style.display == "") && (!document.getElementById("<%=chkNAYear6.ClientID%>").checked)) {
                maxContractValue = document.getElementById("<%=txtMaxContractValueYear3.ClientID%>").value.replace(",", "");
                annualRevenue = document.getElementById("<%=txtAnuualCompRevenueYear3.ClientID%>").value.replace(",", "");
                if (maxContractValue != "" && annualRevenue != "") {
                    if (parseFloat(maxContractValue) >= parseFloat(annualRevenue)) {
                        msg = msg + "<li>Annual company revenue should be greater than maximum contract value completed under year six</li>";
                    }
                }
            }
            if ((document.getElementById("<%=spnYearMore4.ClientID%>").style.display == "") && (!document.getElementById("<%=chkNAYear7.ClientID%>").checked)) {
                maxContractValue = document.getElementById("<%=txtMaxContractValueYear4.ClientID%>").value.replace(",", "");
                annualRevenue = document.getElementById("<%=txtAnuualCompRevenueYear4.ClientID%>").value.replace(",", "");
                if (maxContractValue != "" && annualRevenue != "") {
                    if (parseFloat(maxContractValue) >= parseFloat(annualRevenue)) {
                        msg = msg + "<li>Annual company revenue should be greater than maximum contract value completed under year seven</li>";
                    }
                }
            }
            if ((document.getElementById("<%=spnYearMore5.ClientID%>").style.display == "") && (!document.getElementById("<%=chkNAYear8.ClientID%>").checked)) {
                maxContractValue = document.getElementById("<%=txtMaxContractValueYear5.ClientID%>").value.replace(",", "");
                annualRevenue = document.getElementById("<%=txtAnuualCompRevenueYear5.ClientID%>").value.replace(",", "");
                if (maxContractValue != "" && annualRevenue != "") {
                    if (parseFloat(maxContractValue) >= parseFloat(annualRevenue)) {
                        msg = msg + "<li>Annual company revenue should be greater than maximum contract value completed under year eight</li>";
                    }
                }
            }
            if ((document.getElementById("<%=spnYearMore6.ClientID%>").style.display == "") && (!document.getElementById("<%=chkNAYear9.ClientID%>").checked)) {
                maxContractValue = document.getElementById("<%=txtMaxContractValueYear6.ClientID%>").value.replace(",", "");
                annualRevenue = document.getElementById("<%=txtAnuualCompRevenueYear6.ClientID%>").value.replace(",", "");
                if (maxContractValue != "" && annualRevenue != "") {
                    if (parseFloat(maxContractValue) >= parseFloat(annualRevenue)) {
                        msg = msg + "<li>Annual company revenue should be greater than maximum contract value completed under nine</li>";
                    }
                }
            }
            if ((document.getElementById("<%=spnYearMore7.ClientID%>").style.display == "") && (!document.getElementById("<%=chkNAYear10.ClientID%>").checked)) {
                maxContractValue = document.getElementById("<%=txtMaxContractValueYear7.ClientID%>").value.replace(",", "");
                annualRevenue = document.getElementById("<%=txtAnuualCompRevenueYear7.ClientID%>").value.replace(",", "");
                if (maxContractValue != "" && annualRevenue != "") {
                    if (parseFloat(maxContractValue) >= parseFloat(annualRevenue)) {
                        msg = msg + "<li>Annual company revenue should be greater than maximum contract value completed under year ten</li>";
                    }
                }
            }
            if (msg != "") {
                document.getElementById("<%=lblMessage.ClientID%>").style.display = "";
                document.getElementById("<%=lblMessage.ClientID%>").innerHTML = msg;
                document.getElementById("trMessage").style.display = "";
            }
            else {
                document.getElementById("<%=lblMessage.ClientID%>").style.display = "none";
                document.getElementById("<%=lblMessage.ClientID%>").text = "";
                document.getElementById("trMessage").style.display = "none";
            }
        }
        function visibleControl(dropDownList, textBox, tr, filterExtender, regularExprValidator, otherTextBox) {
            var TextBoxFilter = $find(filterExtender);
            var ddl = document.getElementById(dropDownList);
            var ddltext = ddl.options[ddl.selectedIndex].text;
            var zipText = document.getElementById(textBox);
            var TXT = document.getElementById(otherTextBox);
            zipText.value = "";
            TXT.value = "";
            if (ddltext == "Other") {
                document.getElementById(tr).style.display = "";
                var TXT = document.getElementById(otherTextBox);
                TXT.focus();
                document.getElementById(regularExprValidator).ValidationGroup = "State";
                zipText.maxLength = 10;
                zipText.style.width = "80px";
                if (TextBoxFilter != null) {
                    //  TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.UppercaseLetters + AjaxControlToolkit.FilterTypes.LowercaseLetters + AjaxControlToolkit.FilterTypes.Numbers + AjaxControlToolkit.FilterTypes.Custom);
                    //  TextBoxFilter.set_ValidChars(" ,@"); //002-Sooraj commented
                }
            }
            else {
                document.getElementById(tr).style.display = "none";
                document.getElementById(regularExprValidator).ValidationGroup = "Banking";
                if (TextBoxFilter != null) {
                    //TextBoxFilter.set_FilterType(AjaxControlToolkit.FilterTypes.Numbers); //002-Sooraj commented
                }
                zipText.maxLength = 5;
                zipText.style.width = "40px";

                if (ddl != null && ddl.value > 51) {
                    zipText.maxLength = 10;
                    zipText.style.width = "80px";
                }
            }
        }
    </script>
    <script src="../Script/jquery.js" type="text/javascript"></script>
</body>
</html>
