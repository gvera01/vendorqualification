﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using VMSDAL;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 01/29/2013: Hot Fix 1.3.1 (JIRA:VPI-90)
 * 003 Sooraj Sudhakaran.T 09/28/2013: VMS Modification - Added code to hide section that do not apply to design consultant
 * 004 Sooraj Sudhakaran.T 10/24/2013: VMS Modification - Added new question for Non US vendors 
 * 005 Sooraj Sudhakaran.T 10/24/2013: VMS Modification - Added new question for Non US vendors 
 * 006 Sooraj Sudhakaran.T 12/12/2013: VMS Modification - Added code to handle International contact phone number
 * 007 G. Vera 06/09/2014: Adding Year 4 to OSHA if user clicks on "N/A" for the first OSHA year
 * 008 G. Vera 06/12/2014: Add OSHA and EMR attachments (required)
 * 009 G. Vera 07/08/2014: Remove all validations from Admin VQF screens
 * 010 G. Vera 12/14/2016: Add new questions to Questionnaire section
 * 011 G. Vera 03/01/2017:  Change safety emr and osha new year from 05/01 to 03/01
 * 012 G. Vera 03/01/2018:  Change safety emr and osha new year from 03/01 to 01/01
 ****************************************************************************************************************/
#endregion

public partial class VQF_Safety : CommonPage
{
    bool? CompanySafety = null;
    bool? SafetyRequirements = null;
    bool? SafetyRepresentation = null;//004
    bool? SafetyProgram = null;
    byte SafetyStatus;
    Common objCommon = new Common();
    clsSafety objSafety = new clsSafety();
    clsCompany objCompany = new clsCompany();
    long VendorId, SafetyId;
    VMSDAL.VQFSafetyEMRRate objRates;
    VMSDAL.VQFSafetyEMRRate objRates1;
    VMSDAL.VQFSafetyEMRRate objRates2;
    VMSDAL.VQFSafetyOSHA objOsha1;
    VMSDAL.VQFSafetyOSHA objOsha2;
    VMSDAL.VQFSafetyOSHA objOsha3;
    VMSDAL.VQFSafetyOSHA objOsha4;     //007
    VMSDAL.VQFSafety EditData;  //010 - Change to use new DAL
    public DataSet Vendordetails = new DataSet();
    public int CountSafety;
    Type type;
    bool Result;
    public const string Safety = "SAFETY";
    public const string EMRRate = "Current EMR Rates";
    public const string OSHA = "Safety History / OSHA 300";
    public const string Questionnaire = "QUESTIONNAIRE";
    public const string Break = "%0A%0A";   //010 Modify
    public const string Tab = "%09";   //010 Add
    string OshaPart = "";
    bool IsAvail = false;
    StringBuilder CompareBody = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
    StringBuilder CompareMainBody = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
    private static bool attachmentChanged = false;
    private static string attachmentChangedText = string.Empty;
    private VQFSafetyCitation citation1
    {
        get { return ViewState["citation1"] != null ? (ViewState["citation1"] as VQFSafetyCitation) : null; }
        set { ViewState["citation1"] = value; }
    } //010
    private VQFSafetyCitation citation2
    {
        get { return ViewState["citation2"] != null ? (ViewState["citation2"] as VQFSafetyCitation) : null; }
        set { ViewState["citation2"] = value; }
    } //010
    private VQFSafetyCitation citation3
    {
        get { return ViewState["citation3"] != null ? (ViewState["citation3"] as VQFSafetyCitation) : null; }
        set { ViewState["citation3"] = value; }
    } //010
    private string CitationYear1
    {
        get { return ViewState["CitationYear1"] != null ? ViewState["CitationYear1"].ToString() : string.Empty; }
        set { ViewState["CitationYear1"] = value; }
    } //010
    private string CitationYear2
    {
        get { return ViewState["CitationYear2"] != null ? ViewState["CitationYear2"].ToString() : string.Empty; }
        set { ViewState["CitationYear2"] = value; }
    } //010
    private string CitationYear3
    {
        get { return ViewState["CitationYear3"] != null ? ViewState["CitationYear3"].ToString() : string.Empty; }
        set { ViewState["CitationYear3"] = value; }
    } //010

    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }

        ViewState["EditStatus"] = 0;
        //Check for vendor id if null, redirect to home page.
        if (string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])) || Convert.ToString(Session["VendorAdminId"]) == "0")
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
        }
        else
        {
            VendorId = Convert.ToInt64(Session["VendorAdminId"]);
            if (!Page.IsPostBack)
            {
                //001
                Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

                HiddenField outlookContent = VQFEditSideMenu.FindControl("hdnOutllook") as HiddenField;
                if (Session["OUTLOOKDATA"] != null)
                    outlookContent.Value = Session["OUTLOOKDATA"].ToString();
                /* Changed since client asked to change from current Year 
                 txtYearOne.Text= Convert.ToString(DateTime.Now.Year - 1);
                 txtYearTwo.Text= Convert.ToString(DateTime.Now.Year - 2);
                 txtYearThree.Text= Convert.ToString(DateTime.Now.Year - 3);
                 txtOSHAYearOne.Text= Convert.ToString(DateTime.Now.Year - 1);
                 txtOSHAYearTwo.Text= Convert.ToString(DateTime.Now.Year - 2);
                 txtOSHAYearThree.Text= Convert.ToString(DateTime.Now.Year - 3);*/

                // G. Vera Phase 3 Modifications Item 1.3
                //DateTime today = DateTime.Now.AddMonths(1);
                DateTime today = DateTime.Now;

                //Included the above line for task no : 20
                //011, 012
                if (today >= Convert.ToDateTime("01/01/" + Convert.ToString(DateTime.Now.Year)))
                {
                    txtYearOne.Text = Convert.ToString(DateTime.Now.Year - 0);
                    txtYearTwo.Text = Convert.ToString(DateTime.Now.Year - 1);
                    txtYearThree.Text = Convert.ToString(DateTime.Now.Year - 2);
                    txtYearFour.Text = Convert.ToString(DateTime.Now.Year - 3);         //002

                    // G. Vera Phase 3 Modifications Item 1.3
                    txtOSHAYearOne.Text = Convert.ToString(DateTime.Now.Year - 1);
                    txtOSHAYearTwo.Text = Convert.ToString(DateTime.Now.Year - 2);
                    txtOSHAYearThree.Text = Convert.ToString(DateTime.Now.Year - 3);
                    txtOSHAYearFour.Text = Convert.ToString(DateTime.Now.Year - 4);     //007

                }
                else
                {
                    txtYearOne.Text = Convert.ToString(DateTime.Now.Year - 1);
                    txtYearTwo.Text = Convert.ToString(DateTime.Now.Year - 2);
                    txtYearThree.Text = Convert.ToString(DateTime.Now.Year - 3);
                    txtYearFour.Text = Convert.ToString(DateTime.Now.Year - 4);         //002

                    // G. Vera Phase 3 Modifications Item 1.3
                    txtOSHAYearOne.Text = Convert.ToString(DateTime.Now.Year - 2);
                    txtOSHAYearTwo.Text = Convert.ToString(DateTime.Now.Year - 3);
                    txtOSHAYearThree.Text = Convert.ToString(DateTime.Now.Year - 4);
                    txtOSHAYearFour.Text = Convert.ToString(DateTime.Now.Year - 5);     //007
                }

                //VendorId = 2; 
                Vendordetails = objCommon.GetVendorDetails(Convert.ToInt64(VendorId));
                //G. Vera - According to Dianne, this question was supposed to apply only to all Onsite Support vendors
                SwitchQuestion((Vendordetails.Tables[0].Rows[0]["TypeOfCompany"].ConvertToString() == TypeOfCompany.EquipAndOnsiteSupport), txtRepresentationContactNumberOne, txtRepresentationContactNumberTwo, txtRepresentationContactNumberThree);//005

                //Show or hide design consultant view based on Type of company - 003-sooraj
                SwitchDesignConsultantView(Vendordetails.Tables[0].Rows[0]["TypeOfCompany"].ConvertToString() == TypeOfCompany.DesignConsultant);


                lblwelcome.Text = Vendordetails.Tables[0].Rows[0]["Company"].ToString();
                CountSafety = objSafety.GetSafetyStatusCount(VendorId);

                HiddenField outlookContentVendorName = VQFEditSideMenu.FindControl("hdnOutllookVendorName") as HiddenField;
                outlookContentVendorName.Value = "Dear " + lblwelcome.Text.Replace("&", "AND") + ",%0A";
                HiddenField outlookContentCompanyName = VQFEditSideMenu.FindControl("hdnOutllookCompanyName") as HiddenField;
                outlookContentCompanyName.Value = lblwelcome.Text.Replace("&", "AND") + ",%0A";
                HiddenField outlookVendorEmail = VQFEditSideMenu.FindControl("hdnOutllookVendorEmail") as HiddenField;
                outlookVendorEmail.Value = Session["OUTLOOKTO"].ToString();

                CallScripts();
                // BindRequiredFieldValidators();
                ShowHideControls();
                //LoadDropdowns();

                //010 - START
                CitationYear1 = DateTime.Now.Year.ConvertToString();
                CitationYear2 = (DateTime.Now.Year - 1).ConvertToString();
                CitationYear3 = (DateTime.Now.Year - 2).ConvertToString();
                //010 - END


                LoadEditDatas(true);
                HandleStatus(); //008
                
            }
            if (rdoSafetyDirector.SelectedIndex == 0)
            {
                spnContact.Style["display"] = "";

            }

            if (rdoRepresentation.SelectedIndex == 0)
            {
                spnContactNonUS.Style["display"] = "";

            }

        }
        SwitchPhoneForKeyContact(chkContactUSA);//006
        SwitchPhoneForKeyContact(chkReperesentUSA);//006

        uclAttachEMR1.CloseEvent += uclAttachEMR1_CloseEvent;   //008
        uclAttachEMR1.IsUseSafetySection = true;           //011

        ControlVisiblity();
    }

    /// <summary>
    /// 008
    /// </summary>
    private void HandleStatus()
    {
        VQFEditSideMenu.GetVendorStatus();
        hdnVendorStatus.Value = Convert.ToString(VQFEditSideMenu.VendorStatus);

        if (VQFEditSideMenu.VendorStatus == 1 || VQFEditSideMenu.VendorStatus == 2)
        {

            ImageButton imgAttach = (ImageButton)VQFEditSideMenu.FindControl("imbAttch");
            imgAttach.Visible = true;
        }
    }

    /// <summary>
    /// 006-Switch phone for Key Contacts 
    /// </summary>
    /// <param name="chk"></param>
    private void SwitchPhoneForKeyContact(CheckBox chk)
    {
        if (chk != null)
        {
            switch (chk.ID)
            {
                case "chkContactUSA":
                    SwitchPhone(chkContactUSA, txtMainPhno1, txtMainPhno2, txtMainPhno3, spnCPhone2);
                    break;
                case "chkReperesentUSA":
                    SwitchPhone(chkReperesentUSA, txtRepresentationContactNumberOne, txtRepresentationContactNumberTwo, txtRepresentationContactNumberThree, spnRepresentPhone);
                    break;
            }
        }



    }

    /// <summary>
    /// 006
    /// </summary>
    /// <param name="val"></param>
    /// <param name="phone1"></param>
    /// <param name="phone2"></param>
    /// <param name="phone3"></param>
    /// <param name="spnPhone"></param>
    private void SwitchPhone(CheckBox val, TextBox phone1, TextBox phone2, TextBox phone3, HtmlControl spnPhone)
    {

        if (val.Checked)//Non-US
        {
            val.Text = "Non-US";
            phone3.Style["display"] = "none";

            phone1.MaxLength = 5;
            phone2.MaxLength = 15;
            phone2.CssClass = "txtbox";
            phone3.Text = "";
            spnPhone.Style["display"] = "none";
            phone1.Attributes.Remove("onkeyup");
            phone2.Attributes.Remove("onkeyup");
        }
        else  //US
        {
            phone1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + phone1.ClientID + "," + phone2.ClientID + ")");
            phone2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + phone2.ClientID + "," + phone3.ClientID + ")");
            // val.Text = "US";
            phone3.Style["display"] = "";
            phone2.CssClass = "txtboxsmall";
            phone1.MaxLength = 3;
            phone2.MaxLength = 3;

            spnPhone.Style["display"] = "";

        }

    }


    /// <summary>
    /// visible controls in postback
    /// </summary>
    public void ControlVisiblity()
    {
        if (rdoSafetyDirector.SelectedIndex == 0)
        {
            spnContact.Style["display"] = "";

        }
        spnContactNonUS.Style["display"] = "none";
        if (rdoRepresentation.SelectedIndex == 0)//005
        {
            spnContactNonUS.Style["display"] = "";
        }
        if (chkSICNA.Checked)
        {
            trSICExplain.Style["display"] = "";
            txtSIC.Enabled = false;
        }
        else
        {
            trSICExplain.Style["display"] = "None";
            txtSIC.Enabled = true;
        }
        //008 if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 0))
        //008 {

            if (chkNA1.Checked)
            {
                trExplain1.Style["Display"] = "";
                txtRateOne.Enabled = false;
                trEMRYEar4Field.Attributes["Style"] = @"display: """;   //002
                trAttachEMR1.Attributes["Style"] = @"display: none";   //008
                

            }
            else
            {
                trExplain1.Style["Display"] = "none";
                txtRateOne.Enabled = true;
                trEMRYEar4Field.Attributes["Style"] = @"display: none";     //002
                trAttachEMR1.Attributes["Style"] = @"display: ";   //008
                

            }

            if (chkNA2.Checked)
            {
                trExplain2.Style["Display"] = "";
                txtRateTwo.Enabled = false;
                trAttachEMR2.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain2.Style["Display"] = "none";
                txtRateTwo.Enabled = true;
                trAttachEMR2.Attributes["Style"] = @"display: ";   //008
            }
            if (chkNA3.Checked)
            {
                trExplain3.Style["Display"] = "";
                txtRateThree.Enabled = false;
                trAttachEMR3.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain3.Style["Display"] = "none";
                txtRateThree.Enabled = true;
                trAttachEMR3.Attributes["Style"] = @"display: ";   //008
            }

            //003
            if (chkEMRNA4.Checked)
            {
                trExplainEMR4.Style["Display"] = "";
                txtRateFour.Enabled = false;
                trAttachEMR4.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplainEMR4.Style["Display"] = "none";
                txtRateFour.Enabled = true;
                trAttachEMR4.Attributes["Style"] = @"display: ";   //008
            }

            if (chkNA4.Checked)
            {
                trExplain4.Style["Display"] = "";
                txtNoofCasesFrmWorkOne.Enabled = false;
                txtNoofCasesTransferOne.Enabled = false;
                txtNoofDeathsYearOne.Enabled = false;
                txtRecordableCasesOne.Enabled = false;
                txtTotalHours1.Enabled = false;
                pnlOSHAYear4.Attributes["Style"] = @"display: """;   //007
                trAttachOSHA1.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment1.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain4.Style["Display"] = "None";
                txtNoofCasesFrmWorkOne.Enabled = true;
                txtNoofCasesTransferOne.Enabled = true;
                txtNoofDeathsYearOne.Enabled = true;
                txtRecordableCasesOne.Enabled = true;
                txtTotalHours1.Enabled = true;
                pnlOSHAYear4.Attributes["Style"] = @"display: none";   //007
                trAttachOSHA1.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment1.Attributes["Style"] = @"display: ";   //008
            }

            if (chkNA5.Checked)
            {
                trExplain5.Style["Display"] = "";
                txtNoofCasesFrmWorkTwo.Enabled = false;
                txtNoofCasesTransferTwo.Enabled = false;
                txtNoofDeathsYearTwo.Enabled = false;
                txtRecordableCasesTwo.Enabled = false;
                txtTotalHours2.Enabled = false;
                trAttachOSHA2.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment2.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain5.Style["Display"] = "None";
                txtNoofCasesFrmWorkTwo.Enabled = true;
                txtNoofCasesTransferTwo.Enabled = true;
                txtNoofDeathsYearTwo.Enabled = true;
                txtRecordableCasesTwo.Enabled = true;
                txtTotalHours2.Enabled = true;
                trAttachOSHA2.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment2.Attributes["Style"] = @"display: ";   //008
            }

            if (chkNA6.Checked)
            {
                trExplain6.Style["Display"] = "";
                txtNoofCasesFrmWorkThree.Enabled = false;
                txtNoofCasesTransferThree.Enabled = false;
                txtNoofDeathsYearThree.Enabled = false;
                txtRecordableCasesThree.Enabled = false;
                txtTotalHours3.Enabled = false;
                trAttachOSHA3.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment3.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain6.Style["Display"] = "None";
                txtNoofCasesFrmWorkThree.Enabled = true;
                txtNoofCasesTransferThree.Enabled = true;
                txtNoofDeathsYearThree.Enabled = true;
                txtRecordableCasesThree.Enabled = true;
                txtTotalHours3.Enabled = true;
                trAttachOSHA3.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment3.Attributes["Style"] = @"display: ";   //008
            }

            //007
            if (chkNA7.Checked)
            {
                trExplain7.Style["Display"] = "";
                txtNoofCasesFrmWorkFour.Enabled = false;
                txtNoofCasesTransferFour.Enabled = false;
                txtNoofDeathsYearFour.Enabled = false;
                txtRecordableCasesFour.Enabled = false;
                txtTotalHours4.Enabled = false;
                trAttachOSHA4.Attributes["Style"] = @"display: none";   //008
                tdNoAttachment4.Attributes["Style"] = @"display: none";   //008
            }
            else
            {
                trExplain7.Style["Display"] = "None";
                txtNoofCasesFrmWorkFour.Enabled = true;
                txtNoofCasesTransferFour.Enabled = true;
                txtNoofDeathsYearFour.Enabled = true;
                txtRecordableCasesFour.Enabled = true;
                txtTotalHours4.Enabled = true;
                trAttachOSHA4.Attributes["Style"] = @"display: ";   //008
                tdNoAttachment4.Attributes["Style"] = @"display: ";   //008
            }

            //}

            #region Not Needed
            //008 else
        // 008 {

            //DAL.VQFSafetyEMRRate objRates = objSafety.SelectSafetyEMRRates(SafetyId, txtYearOne.Text.Trim());
            //if (objRates != null)
            //{
            //    if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
            //    {
            //        chkNA1.Enabled = false;
            //        txtRateOne.Enabled = false;
            //        txtExplanation1.Enabled = false;
            //        txtSIC.Enabled = false;

            //    }
            //    else
            //    {
            //        chkNA1.Enabled = true;
            //        txtRateOne.Enabled = true;
            //        txtExplanation1.Enabled = true;
            //        txtSIC.Enabled = true;

            //    }
            //}
            //int CountSafety = objSafety.GetSafetyCount(VendorId);
            //if (CountSafety > 0)
            //{
            //    if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1)) || (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
            //    {
            //        txtSICExplain.Enabled = false;
            //        chkSICNA.Enabled = false;
            //        txtSIC.Enabled = false;
            //    }
            //    else
            //    {
            //        txtSICExplain.Enabled = true;
            //        chkSICNA.Enabled = true;
            //        txtSIC.Enabled = true;
            //    }
            //}
            //objRates = objSafety.SelectSafetyEMRRates(SafetyId, txtYearTwo.Text.Trim());
            //if (objRates != null)
            //{
            //    if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1)) || (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
            //    //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
            //    {
            //        chkNA2.Enabled = false;
            //        txtRateTwo.Enabled = false;
            //        txtExplanation2.Enabled = false;
            //    }
            //    else
            //    {
            //        chkNA2.Enabled = true;
            //        txtRateTwo.Enabled = true;
            //        txtExplanation2.Enabled = true;
            //    }
            //}
            //objRates = objSafety.SelectSafetyEMRRates(SafetyId, txtYearThree.Text.Trim());
            //if (objRates != null)
            //{

            //    //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
            //    if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1)) || (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
            //    {
            //        chkNA3.Enabled = false;
            //        txtRateThree.Enabled = false;
            //        txtExplanation3.Enabled = false;
            //    }
            //    else
            //    {
            //        chkNA3.Enabled = true;
            //        txtRateThree.Enabled = true;
            //        txtExplanation3.Enabled = true;
            //    }
            //}
            //DAL.VQFSafetyOSHA objOsha1 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearOne.Text.Trim());
            //if (objOsha1 != null)
            //{
            //    if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
            //    //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1)) || (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
            //    {
            //        txtNoofCasesFrmWorkOne.Enabled = false;
            //        txtNoofCasesTransferOne.Enabled = false;
            //        txtNoofDeathsYearOne.Enabled = false;
            //        txtRecordableCasesOne.Enabled = false;
            //        chkNA4.Enabled = false;
            //        txtExplain4.Enabled = false;
            //        txtTotalHours1.Enabled = false;
            //    }
            //    else
            //    {
            //        txtNoofCasesFrmWorkOne.Enabled = true;
            //        txtNoofCasesTransferOne.Enabled = true;
            //        txtNoofDeathsYearOne.Enabled = true;
            //        txtRecordableCasesOne.Enabled = true;
            //        chkNA4.Enabled = true;
            //        txtExplain4.Enabled = true;
            //        txtTotalHours1.Enabled = true;
            //    }


            //}
            //else
            //{

            //    txtNoofCasesFrmWorkOne.Enabled = true;
            //    txtNoofCasesTransferOne.Enabled = true;
            //    txtNoofDeathsYearOne.Enabled = true;
            //    txtRecordableCasesOne.Enabled = true;
            //    chkNA4.Enabled = true;
            //    txtExplain4.Enabled = true;
            //    txtTotalHours1.Enabled = true;

            //}
            ////if ((Convert.ToInt32(objSafety.GetSafetyStatusCount(VendorId)) == 0) && ((hdnVendorStatus.Value != "3") && (hdnVendorStatus.Value != "2")))
            ////{
            ////    txtNoofCasesFrmWorkOne.Enabled = true;
            ////    txtNoofCasesTransferOne.Enabled = true;
            ////    txtNoofDeathsYearOne.Enabled = true;
            ////    txtRecordableCasesOne.Enabled = true;
            ////    txtTotalHours1.Enabled = true;
            ////    chkNA4.Enabled = true;
            ////    txtExplain4.Enabled = true;
            ////}
            //objOsha1 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearTwo.Text.Trim());
            //if (objOsha1 != null)
            //{
            //    //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
            //    if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1)) || (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
            //    {

            //        txtNoofCasesFrmWorkTwo.Enabled = false;
            //        txtNoofCasesTransferTwo.Enabled = false;
            //        txtNoofDeathsYearTwo.Enabled = false;
            //        txtRecordableCasesTwo.Enabled = false;
            //        chkNA5.Enabled = false;
            //        txtExplain5.Enabled = false;
            //        txtTotalHours2.Enabled = false;

            //    }
            //    else
            //    {

            //        txtNoofCasesFrmWorkTwo.Enabled = true;
            //        txtNoofCasesTransferTwo.Enabled = true;
            //        txtNoofDeathsYearTwo.Enabled = true;
            //        txtRecordableCasesTwo.Enabled = true;
            //        chkNA5.Enabled = true;
            //        txtExplain5.Enabled = true;
            //        txtTotalHours2.Enabled = true;

            //    }
            //}
            //objOsha1 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearThree.Text.Trim());
            //if (objOsha1 != null)
            //{
            //    //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
            //    if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1)) || (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
            //    {
            //        txtNoofCasesFrmWorkThree.Enabled = false;
            //        txtNoofCasesTransferThree.Enabled = false;
            //        txtNoofDeathsYearThree.Enabled = false;
            //        txtRecordableCasesThree.Enabled = false;
            //        chkNA6.Enabled = false;
            //        txtExplain6.Enabled = false;
            //        txtTotalHours3.Enabled = false;

            //    }
            //    else
            //    {

            //        txtNoofCasesFrmWorkThree.Enabled = true;
            //        txtNoofCasesTransferThree.Enabled = true;
            //        txtNoofDeathsYearThree.Enabled = true;
            //        txtRecordableCasesThree.Enabled = true;
            //        chkNA6.Enabled = true;
            //        txtExplain6.Enabled = true;
            //        txtTotalHours3.Enabled = true;

            //    }
            //}

            #endregion

        if (chkNA1.Checked)
        {
            trExplain1.Style["Display"] = "";
            //     txtRateOne.Enabled = false;
            trEMRYEar4Field.Attributes["Style"] = @"display: """;   //002

        }
        else
        {
            trExplain1.Style["Display"] = "none";
            //  txtRateOne.Enabled = true;
            trEMRYEar4Field.Attributes["Style"] = @"display: none";     //002
        }
        if (chkNA2.Checked)
        {
            trExplain2.Style["Display"] = "";
            //  txtRateTwo.Enabled = false;
        }
        else
        {
            trExplain2.Style["Display"] = "none";
            //   txtRateTwo.Enabled = true;
        }
        if (chkNA3.Checked)
        {
            trExplain3.Style["Display"] = "";
            //  txtRateThree.Enabled = false;
        }
        else
        {
            trExplain3.Style["Display"] = "none";
            //   txtRateThree.Enabled = true;
        }

        //002
        if (chkEMRNA4.Checked)
        {
            trExplainEMR4.Style["Display"] = "";
        }
        else
        {
            trExplainEMR4.Style["Display"] = "none";
        }

        if (chkNA4.Checked)
        {
            trExplain4.Style["Display"] = "";
        }
        else
        {
            trExplain4.Style["Display"] = "None";

        }

        if (chkNA5.Checked)
        {
            trExplain5.Style["Display"] = "";

        }
        else
        {
            trExplain5.Style["Display"] = "None";

        }

        if (chkNA6.Checked)
        {
            trExplain6.Style["Display"] = "";

        }
        else
        {
            trExplain6.Style["Display"] = "None";

        }

        //txtNoofCasesFrmWorkTwo.Enabled = false;
        //txtNoofCasesTransferTwo.Enabled = false;
        //txtNoofDeathsYearTwo.Enabled = false;
        //txtRecordableCasesTwo.Enabled = false;
        //txtNoofCasesFrmWorkThree.Enabled = false;
        //txtNoofCasesTransferThree.Enabled = false;
        //txtNoofDeathsYearThree.Enabled = false;
        //txtRecordableCasesThree.Enabled = false;
        //txtRateOne.Enabled = false;
        //txtRateTwo.Enabled = false;
        //txtRateThree.Enabled = false;

    }

    /// <summary>
    /// Display the panel based on the sub menu selected
    /// </summary>
    private void ShowHideControls()
    {
        if (Request.QueryString.Count > 0)
        {
            if (Request.QueryString["submenu"] == "1")
            {
                accSafety.SelectedIndex = 0;
            }
            else if (Request.QueryString["submenu"] == "2")
            {
                accSafety.SelectedIndex = 1;
            }
            else if (Request.QueryString["submenu"] == "3")
            {
                accSafety.SelectedIndex = 2;
            }
        }
    }

    /// <summary>
    /// 007-Non US Question changes
    /// </summary>
    ///  <param name="IsOnsiteSupport"></param>
    /// <param name="phone1"></param>
    /// <param name="phone2"></param>
    /// <param name="phone3"></param>
    private void SwitchQuestion(bool IsOnsiteSupport, TextBox phone1, TextBox phone2, TextBox phone3)
    {
        //G. Vera - According to Dianne, this question was supposed to apply only to all Onsite Support vendors
        if (IsOnsiteSupport)
        {
            trNonUSSafetyQuestion.Style["display"] = "";
            phone3.Style["display"] = "none";
            phone1.MaxLength = 5;
            phone2.MaxLength = 15;
            phone2.CssClass = "txtbox";
        }
        else
        {
            trNonUSSafetyQuestion.Style["display"] = "none";
        }

    }

    /// <summary>
    /// 003-Switch view based on logged in user
    /// </summary>
    /// <param name="IsShow"></param>
    /// <Author>Sooraj Sudhakaran.T</Author>
    ///  <Date>20-SEP-2013</Date>
    private void SwitchDesignConsultantView(bool IsShow)
    {
        //Design Consultant 
        accSafety.Visible = !IsShow;
        imbSave.Visible = !IsShow;
    }

    /// <summary>
    /// Call scripts for menus
    /// </summary>

    private void CallScripts()
    {
        txtRateOne.Attributes.Add("onBlur", "javascript:numberRateFormat(this.value,'','" + txtRateOne.ClientID + "');validateRateRange('" + txtRateOne.ClientID + "','one')");
        txtRateTwo.Attributes.Add("onBlur", "javascript:numberRateFormat(this.value,'','" + txtRateTwo.ClientID + "');validateRateRange('" + txtRateTwo.ClientID + "','two')");
        txtRateThree.Attributes.Add("onBlur", "javascript:numberRateFormat(this.value,'','" + txtRateThree.ClientID + "');validateRateRange('" + txtRateThree.ClientID + "','three')");
        //002
        txtRateFour.Attributes.Add("onBlur", "javascript:numberRateFormat(this.value,'','" + txtRateFour.ClientID + "');validateRateRange('" + txtRateFour.ClientID + "','four')");

        chkNA1.Attributes.Add("onClick", "javascript:DisplayExplain(this.checked,'" + txtRateOne.ClientID + "','" + trExplain1.ClientID + "', '" + txtExplanation1.ClientID + "');DisplayYear4(this.checked,'" + trEMRYEar4Field.ClientID + "');DisplayAttachment('" + trAttachEMR1.ClientID + "',this.checked)");    //003, 007, 008
        chkNA2.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + txtRateTwo.ClientID + "','" + trExplain2.ClientID + "', '" + txtExplanation2.ClientID + "');DisplayAttachment('" + trAttachEMR2.ClientID + "',this.checked)");   //008
        chkNA3.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + txtRateThree.ClientID + "','" + trExplain3.ClientID + "', '" + txtExplanation3.ClientID + "');DisplayAttachment('" + trAttachEMR3.ClientID + "',this.checked)"); //008
        chkSICNA.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + txtSIC.ClientID + "','" + trSICExplain.ClientID + "', '" + txtSICExplain.ClientID + "');");
        //002
        chkEMRNA4.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + txtRateFour.ClientID + "','" + trExplainEMR4.ClientID + "', '" + txtExplanation4.ClientID + "');DisplayAttachment('" + trAttachEMR1.ClientID + "',this.checked)");    //008

        chkSICNA.Attributes.Add("onClick", "DisplayExplain(this.checked,'" + txtSIC.ClientID + "','" + trSICExplain.ClientID + "', '" + txtSICExplain.ClientID + "');");

        chkNA4.Attributes.Add("onClick", "DisplayOSHAExplain(this.checked,'" + txtNoofDeathsYearOne.ClientID + "','" + txtNoofCasesFrmWorkOne.ClientID + "','" + txtNoofCasesTransferOne.ClientID + "','" + txtRecordableCasesOne.ClientID + "','" + trExplain4.ClientID + "', '" + txtExplain4.ClientID + "','" + txtTotalHours1.ClientID + "');DisplayYear4(this.checked,'" + pnlOSHAYear4.ClientID + "');DisplayAttachment('" + trAttachOSHA1.ClientID + "',this.checked);DisplayAttachment('" + tdNoAttachment1.ClientID + "',this.checked)");  //007, 008
        chkNA5.Attributes.Add("onClick", "DisplayOSHAExplain(this.checked,'" + txtNoofDeathsYearTwo.ClientID + "','" + txtNoofCasesFrmWorkTwo.ClientID + "','" + txtNoofCasesTransferTwo.ClientID + "','" + txtRecordableCasesTwo.ClientID + "','" + trExplain5.ClientID + "', '" + txtExplain5.ClientID + "','" + txtTotalHours2.ClientID + "');DisplayAttachment('" + trAttachOSHA2.ClientID + "',this.checked);DisplayAttachment('" + tdNoAttachment2.ClientID + "',this.checked)");    //008
        chkNA6.Attributes.Add("onClick", "DisplayOSHAExplain(this.checked,'" + txtNoofDeathsYearThree.ClientID + "','" + txtNoofCasesFrmWorkThree.ClientID + "','" + txtNoofCasesTransferThree.ClientID + "','" + txtRecordableCasesThree.ClientID + "','" + trExplain6.ClientID + "', '" + txtExplain6.ClientID + "','" + txtTotalHours3.ClientID + "');DisplayAttachment('" + trAttachOSHA3.ClientID + "',this.checked);DisplayAttachment('" + tdNoAttachment3.ClientID + "',this.checked)");    //008
        //007
        chkNA7.Attributes.Add("onClick", "DisplayOSHAExplain(this.checked,'" + txtNoofDeathsYearFour.ClientID + "','" + txtNoofCasesFrmWorkFour.ClientID + "','" + txtNoofCasesTransferFour.ClientID + "','" + txtRecordableCasesFour.ClientID + "','" + trExplain7.ClientID + "', '" + txtExplain7.ClientID + "','" + txtTotalHours4.ClientID + "');DisplayAttachment('" + trAttachOSHA4.ClientID + "',this.checked);DisplayAttachment('" + tdNoAttachment4.ClientID + "',this.checked)");    //007, 008

        txtRateOne.Attributes.Add("onkeypress", "javascript:numberEMRFormat(this.value,'','" + txtRateOne.ClientID + "')");
        txtRateTwo.Attributes.Add("onkeypress", "javascript:numberEMRFormat(this.value,'','" + txtRateTwo.ClientID + "')");
        txtRateThree.Attributes.Add("onkeypress", "javascript:numberEMRFormat(this.value,'','" + txtRateThree.ClientID + "')");
        //002
        txtRateFour.Attributes.Add("onkeypress", "javascript:numberEMRFormat(this.value,'','" + txtRateFour.ClientID + "')");

        txtTotalHours1.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtTotalHours1.ClientID + "')");
        txtTotalHours2.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtTotalHours2.ClientID + "')");
        txtTotalHours3.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtTotalHours3.ClientID + "')");


        txtTotalHours1.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours1.ClientID + "')");
        txtTotalHours2.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours2.ClientID + "')");
        txtTotalHours3.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours3.ClientID + "')");

        txtNoofCasesFrmWorkOne.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkOne.ClientID + "')");
        txtNoofCasesFrmWorkTwo.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkTwo.ClientID + "')");
        txtNoofCasesFrmWorkThree.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkThree.ClientID + "')");

        txtNoofCasesTransferOne.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferOne.ClientID + "')");
        txtNoofCasesTransferTwo.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferTwo.ClientID + "')");
        txtNoofCasesTransferThree.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferThree.ClientID + "')");

        txtNoofDeathsYearOne.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearOne.ClientID + "')");
        txtNoofDeathsYearTwo.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearTwo.ClientID + "')");
        txtNoofDeathsYearThree.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearThree.ClientID + "')");
        // txtTotalHours1.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkOne.ClientID + "')");




        txtRecordableCasesOne.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtRecordableCasesOne.ClientID + "')");
        txtRecordableCasesTwo.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtRecordableCasesTwo.ClientID + "')");
        txtRecordableCasesThree.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtRecordableCasesThree.ClientID + "')");

        txtNoofCasesFrmWorkOne.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkOne.ClientID + "')");
        txtNoofCasesFrmWorkTwo.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkTwo.ClientID + "')");
        txtNoofCasesFrmWorkThree.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkThree.ClientID + "')");

        txtNoofCasesTransferOne.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferOne.ClientID + "')");
        txtNoofCasesTransferTwo.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferTwo.ClientID + "')");
        txtNoofCasesTransferThree.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferThree.ClientID + "')");

        txtNoofDeathsYearOne.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearOne.ClientID + "')");
        txtNoofDeathsYearTwo.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearTwo.ClientID + "')");
        txtNoofDeathsYearThree.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearThree.ClientID + "')");

        txtRecordableCasesOne.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtRecordableCasesOne.ClientID + "')");
        txtRecordableCasesTwo.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtRecordableCasesTwo.ClientID + "')");
        txtRecordableCasesThree.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtRecordableCasesThree.ClientID + "')");

        //txtTotalHours1.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtTotalHours1.ClientID + "')");
        //txtTotalHours2.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtTotalHours2.ClientID + "')");
        //txtTotalHours3.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtTotalHours3.ClientID + "')");


        //txtTotalHours1.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours1.ClientID + "')");
        //txtTotalHours2.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours2.ClientID + "')");
        //txtTotalHours3.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours3.ClientID + "')");


        txtMainPhno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtMainPhno1.ClientID + "," + txtMainPhno2.ClientID + ")");
        txtMainPhno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtMainPhno2.ClientID + "," + txtMainPhno3.ClientID + ")");

        //
        //txtRateOne.Attributes.Add("onblur", "javascript:validateRateRange('" + txtRateOne.ClientID + "')");
        //txtRateTwo.Attributes.Add("onblur", "javascript:validateRateRange('" + txtRateTwo.ClientID + "')");
        //txtRateThree.Attributes.Add("onblur", "javascript:validateRateRange('" + txtRateThree.ClientID + "')");

        //007
        txtTotalHours4.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtTotalHours4.ClientID + "')");

        //007
        txtTotalHours4.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtTotalHours4.ClientID + "')");

        //007
        txtNoofCasesFrmWorkFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkFour.ClientID + "')");

        //007
        txtNoofCasesTransferFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferFour.ClientID + "')");

        //007
        txtNoofDeathsYearFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearFour.ClientID + "')");

        //007
        txtNoofDeathsYearFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearFour.ClientID + "')");

        //007
        txtRecordableCasesFour.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtRecordableCasesFour.ClientID + "')");

        //007
        txtNoofCasesFrmWorkFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesFrmWorkFour.ClientID + "')");

        //007
        txtNoofCasesTransferFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofCasesTransferFour.ClientID + "')");

        //007
        txtNoofDeathsYearFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtNoofDeathsYearFour.ClientID + "')");

        //007
        txtRecordableCasesFour.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtRecordableCasesFour.ClientID + "')");

        //006 - Sooraj --Internationalization for Phone 
        chkContactUSA.Attributes.Add("OnClick", "SwitchPhone(this,'" + txtMainPhno1.ClientID + "','" + txtMainPhno2.ClientID + "','" + txtMainPhno3.ClientID + "','" + spnCPhone2.ClientID + "')");
        chkReperesentUSA.Attributes.Add("OnClick", "SwitchPhone(this,'" + txtRepresentationContactNumberOne.ClientID + "','" + txtRepresentationContactNumberTwo.ClientID + "','" + txtRepresentationContactNumberThree.ClientID + "','" + spnRepresentPhone.ClientID + "')");

    }

    protected void imbSave_Click(object sender, ImageClickEventArgs e)
    {
        VMSDAL.VQFSafetyEMRRate objRates4 = new VMSDAL.VQFSafetyEMRRate();  //008

        //010 - START
        string levelOfTraining = txtProLevelOfTraining.Text;
        bool? isProvideDocumentation = (rdoListEmployeeProgramDocumentation.SelectedItem != null) ? rdoListEmployeeProgramDocumentation.SelectedItem.Text.ConvertToBool() : new bool?();
        bool? isDrugAlcohol = (rdoListDrugAlcohol.SelectedItem != null) ? rdoListDrugAlcohol.SelectedItem.Text.ConvertToBool() : new bool?();
        bool? isSafetyMeetingsHeld = (rdoListSafetyMeetings.SelectedItem != null) ? rdoListSafetyMeetings.SelectedItem.Text.ConvertToBool() : new bool?();
        string safetyMeetingsPeriods = (chkSafetyMeetings.SelectedIndex >= 0) ?
                                        string.Join("|", chkSafetyMeetings.Items.Cast<ListItem>().Where(c => c.Selected).Select(cc => cc.Text).ToArray<string>()) :
                                        string.Empty;
        bool? isInspectionWIP = (rdoListInspectionWIP.SelectedItem != null) ? rdoListInspectionWIP.SelectedItem.Text.ConvertToBool() : new bool?();
        string whoConductsInspectionWIP = txtWhoInspectionWIP.Text;
        string inspectionPeriods = (chkInspectionWIP.SelectedIndex >= 0) ?
                                    string.Join("|", chkInspectionWIP.Items.Cast<ListItem>().Where(c => c.Selected).Select(cc => cc.Text).ToArray<string>()) :
                                    string.Empty;
        bool? isHazardAnalysis = (rdoHazardAnalysis.SelectedItem != null) ? rdoHazardAnalysis.SelectedItem.Text.ConvertToBool() : new bool?();
        string hazardPeriods = (chkHazardAnalysisPeriods.SelectedIndex >= 0) ?
                                    string.Join("|", chkHazardAnalysisPeriods.Items.Cast<ListItem>().Where(c => c.Selected).Select(cc => cc.Text).ToArray<string>()) :
                                    string.Empty;

        #region Citations
        citation1.IsCitation = (rdoCitationYear1.SelectedIndex >= 0) ? (rdoCitationYear1.SelectedIndex == 0) : new bool?();
        citation2.IsCitation = (rdoCitationYear2.SelectedIndex >= 0) ? (rdoCitationYear2.SelectedIndex == 0) : new bool?();
        citation3.IsCitation = (rdoCitationYear3.SelectedIndex >= 0) ? (rdoCitationYear3.SelectedIndex == 0) : new bool?();
        #endregion

        //010 - END
        
        //009 - commented out all page validations
        //Page.Validate("Current");
        //if (Page.IsValid)
        //{

        //    Page.Validate("Osha");
        //    if (Page.IsValid)
        //    {
        //        Page.Validate("Questionair");
        //        if (Page.IsValid)
        //        {


                    //Sooraj Commented 

                    //if (rdoSafetyProgram.SelectedIndex == -1)
                    //{
                    //    SafetyProgram = null;
                    //}
                    //else if (rdoSafetyProgram.SelectedIndex == 0)
                    //{
                    //    SafetyProgram = true;
                    //}
                    //else if (rdoSafetyProgram.SelectedIndex == 1)
                    //{
                    //    SafetyProgram = false;
                    //}

                    //if (rdoSafetyDirector.SelectedIndex == -1)
                    //{
                    //    CompanySafety = null;
                    //}
                    //else if (rdoSafetyDirector.SelectedIndex == 0)
                    //{
                    //    CompanySafety = true;
                    //}
                    //else if (rdoSafetyDirector.SelectedIndex == 1)
                    //{
                    //    CompanySafety = false;
                    //}

                    //if (rdoSafetytraining.SelectedIndex == -1)
                    //{
                    //    SafetyRequirements = null;
                    //}
                    //else if (rdoSafetytraining.SelectedIndex == 0)
                    //{
                    //    SafetyRequirements = true;
                    //}
                    //else if (rdoSafetytraining.SelectedIndex == 1)
                    //{
                    //    SafetyRequirements = false;
                    //}


                    SafetyRepresentation = objCommon.ReturnCheckedStatus(rdoRepresentation); //005 

                    //Sooraj Modified for radio button checked status to avoid redundant code

                    SafetyProgram = objCommon.ReturnCheckedStatus(rdoSafetyProgram);

                    CompanySafety = objCommon.ReturnCheckedStatus(rdoSafetyDirector);

                    SafetyRequirements = objCommon.ReturnCheckedStatus(rdoSafetytraining);

                    string MainPhone = objCommon.CheckPhone(txtMainPhno1.Text.Trim(), txtMainPhno2.Text.Trim(), txtMainPhno3.Text.Trim(), txtMainPhno3);//005


                    string RepPhone = objCommon.CheckPhone(txtRepresentationContactNumberOne.Text.Trim(), txtRepresentationContactNumberTwo.Text.Trim(), txtRepresentationContactNumberThree.Text.Trim(), txtRepresentationContactNumberThree); //005



                    int CountSafety = objSafety.GetSafetyCount(VendorId);
                    if (CountSafety == 0)
                    {
                        SafetyStatus = 0;
                        Result = objSafety.InsertSafety(CompanySafety, txtContactName.Text.Trim(), MainPhone, SafetyProgram, SafetyRequirements, VendorId, SafetyStatus, txtSIC.Text.Trim(), chkSICNA.Checked,
                            txtSICExplain.Text, SafetyRepresentation, txtRepresentationContactName.Text, RepPhone, !chkContactUSA.Checked, !chkReperesentUSA.Checked,
                                            levelOfTraining, isProvideDocumentation, isDrugAlcohol, isSafetyMeetingsHeld, safetyMeetingsPeriods, isInspectionWIP, whoConductsInspectionWIP, inspectionPeriods, isHazardAnalysis, hazardPeriods);   //005, 010

                        objSafety.insertSafetyEMR(txtRateOne.Text.Trim(), txtYearOne.Text.Trim(), chkNA1.Checked, txtExplanation1.Text.Trim());


                        objSafety.insertSafetyEMR(txtRateTwo.Text.Trim(), txtYearTwo.Text.Trim(), chkNA2.Checked, txtExplanation2.Text.Trim());
                        objSafety.insertSafetyEMR(txtRateThree.Text.Trim(), txtYearThree.Text.Trim(), chkNA3.Checked, txtExplanation3.Text.Trim());
                        //002
                        objSafety.insertSafetyEMR(txtRateFour.Text.Trim(), txtYearFour.Text.Trim(), chkEMRNA4.Checked, txtExplanation4.Text.Trim());

                        objSafety.InsertSafetyOsha(txtOSHAYearOne.Text.Trim(), txtNoofDeathsYearOne.Text.Trim(), txtNoofCasesFrmWorkOne.Text.Trim(), txtNoofCasesTransferOne.Text.Trim(), txtRecordableCasesOne.Text.Trim(), chkNA4.Checked, txtExplain4.Text.Trim(), txtTotalHours1.Text.Trim(), !chkNoAttachment1.Checked);
                        objSafety.InsertSafetyOsha(txtOSHAYearTwo.Text.Trim(), txtNoofDeathsYearTwo.Text.Trim(), txtNoofCasesFrmWorkTwo.Text.Trim(), txtNoofCasesTransferTwo.Text.Trim(), txtRecordableCasesTwo.Text.Trim(), chkNA5.Checked, txtExplain5.Text.Trim(), txtTotalHours2.Text.Trim(), !chkNoAttachment2.Checked);
                        objSafety.InsertSafetyOsha(txtOSHAYearThree.Text.Trim(), txtNoofDeathsYearThree.Text.Trim(), txtNoofCasesFrmWorkThree.Text.Trim(), txtNoofCasesTransferThree.Text.Trim(), txtRecordableCasesThree.Text.Trim(), chkNA6.Checked, txtExplain6.Text.Trim(), txtTotalHours3.Text.Trim(), !chkNoAttachment3.Checked);
                        //007
                        objSafety.InsertSafetyOsha(txtOSHAYearFour.Text.Trim(), txtNoofDeathsYearFour.Text.Trim(), txtNoofCasesFrmWorkFour.Text.Trim(), txtNoofCasesTransferFour.Text.Trim(), txtRecordableCasesFour.Text.Trim(), chkNA7.Checked, txtExplain7.Text.Trim(), txtTotalHours4.Text.Trim(), !chkNoAttachment4.Checked);
                        DAL.VQFSafety EditSafetyData = objSafety.GetSingleVQLSafetyData(VendorId);
                        hdnSafetyId.Value = Convert.ToString(EditSafetyData.PK_SafetyID);
                        ViewState["SafetyID"] = Convert.ToString(EditSafetyData.PK_SafetyID);
                        SafetyId = Convert.ToInt64(hdnSafetyId.Value);
                        citation1 = objSafety.NewCitationRecord(SafetyId, CitationYear1);
                        citation2 = objSafety.NewCitationRecord(SafetyId, CitationYear2);
                        citation3 = objSafety.NewCitationRecord(SafetyId, CitationYear3);
                        citation1.IsCitation = (rdoCitationYear1.SelectedIndex >= 0) ? (rdoCitationYear1.SelectedIndex == 0) : new bool?();
                        citation2.IsCitation = (rdoCitationYear2.SelectedIndex >= 0) ? (rdoCitationYear2.SelectedIndex == 0) : new bool?();
                        citation3.IsCitation = (rdoCitationYear3.SelectedIndex >= 0) ? (rdoCitationYear3.SelectedIndex == 0) : new bool?();
                        if (e != null) modalExtnd.Show();    //008

                    }
                    else
                    {

                        if (!string.IsNullOrEmpty(hdnSafetyId.Value))
                        {
                            SafetyId = Convert.ToInt64(hdnSafetyId.Value);
                        }
                        else
                        {
                            SafetyId = Convert.ToInt64(ViewState["SafetyID"]);   //GV - 11/17/2014

                        }
                            //COMPARE
                            EditData = objSafety.GetSingleVQFSafetyData(VendorId);  //010 change to use new DAL
                            SafetyStatus = Convert.ToByte(EditData.SafetyStatus);
                            objRates = objSafety.SelectSafetyEMRRates(SafetyId, txtYearOne.Text.Trim());
                            objRates1 = objSafety.SelectSafetyEMRRates(SafetyId, txtYearTwo.Text.Trim());
                            objRates2 = objSafety.SelectSafetyEMRRates(SafetyId, txtYearThree.Text.Trim());
                            objRates4 = objSafety.SelectSafetyEMRRates(SafetyId, txtYearFour.Text.Trim());  //008 was not added
                            objOsha1 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearOne.Text.Trim());
                            objOsha2 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearTwo.Text.Trim());
                            objOsha3 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearThree.Text.Trim());
                            objOsha4 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearFour.Text.Trim());  //007

                            Result = objSafety.UpdateSafety(CompanySafety, txtContactName.Text.Trim(), MainPhone, SafetyProgram, SafetyRequirements, SafetyId, SafetyStatus, txtSIC.Text.Trim(), chkSICNA.Checked, txtSICExplain.Text,
                                SafetyRepresentation, txtRepresentationContactName.Text, RepPhone, !chkContactUSA.Checked, !chkReperesentUSA.Checked, levelOfTraining,
                                            isProvideDocumentation, isDrugAlcohol, isSafetyMeetingsHeld, safetyMeetingsPeriods, isInspectionWIP, whoConductsInspectionWIP, inspectionPeriods, isHazardAnalysis, hazardPeriods);   //005, 010

                            objSafety.UpdateSafetyEMR(txtRateOne.Text.Trim(), txtYearOne.Text.Trim(), chkNA1.Checked, txtExplanation1.Text.Trim());
                            objSafety.UpdateSafetyEMR(txtRateTwo.Text.Trim(), txtYearTwo.Text.Trim(), chkNA2.Checked, txtExplanation2.Text.Trim());
                            objSafety.UpdateSafetyEMR(txtRateThree.Text.Trim(), txtYearThree.Text.Trim(), chkNA3.Checked, txtExplanation3.Text.Trim());
                            //002
                            objSafety.UpdateSafetyEMR(txtRateFour.Text.Trim(), txtYearFour.Text.Trim(), chkEMRNA4.Checked, txtExplanation4.Text.Trim());

                            objSafety.UpdateSafetyOsha(txtOSHAYearOne.Text.Trim(), txtNoofDeathsYearOne.Text.Trim(), txtNoofCasesFrmWorkOne.Text.Trim(), txtNoofCasesTransferOne.Text.Trim(), txtRecordableCasesOne.Text.Trim(), chkNA4.Checked, txtExplain4.Text.Trim(), txtTotalHours1.Text.Trim(), !chkNoAttachment1.Checked);
                            objSafety.UpdateSafetyOsha(txtOSHAYearTwo.Text.Trim(), txtNoofDeathsYearTwo.Text.Trim(), txtNoofCasesFrmWorkTwo.Text.Trim(), txtNoofCasesTransferTwo.Text.Trim(), txtRecordableCasesTwo.Text.Trim(), chkNA5.Checked, txtExplain5.Text.Trim(), txtTotalHours2.Text.Trim(), !chkNoAttachment2.Checked);
                            objSafety.UpdateSafetyOsha(txtOSHAYearThree.Text.Trim(), txtNoofDeathsYearThree.Text.Trim(), txtNoofCasesFrmWorkThree.Text.Trim(), txtNoofCasesTransferThree.Text.Trim(), txtRecordableCasesThree.Text.Trim(), chkNA6.Checked, txtExplain6.Text.Trim(), txtTotalHours3.Text.Trim(), !chkNoAttachment3.Checked);
                            //007
                            objSafety.UpdateSafetyOsha(txtOSHAYearFour.Text.Trim(), txtNoofDeathsYearFour.Text.Trim(), txtNoofCasesFrmWorkFour.Text.Trim(), txtNoofCasesTransferFour.Text.Trim(), txtRecordableCasesFour.Text.Trim(), chkNA7.Checked, txtExplain7.Text.Trim(), txtTotalHours4.Text.Trim(), !chkNoAttachment4.Checked);
                            lblError.Text = "";
                    }

                    //010 - START
                    //compare citations
                    string citationsChanged = Compare.CompareSafetyCitations(citation1);
                    citationsChanged += Compare.CompareSafetyCitations(citation2);
                    citationsChanged += Compare.CompareSafetyCitations(citation3);
                    objCommon.CompareChanges += citationsChanged;

                    //update database and objects
                    citation1 = objSafety.UpdateCitations(citation1);
                    citation2 = objSafety.UpdateCitations(citation2);
                    citation3 = objSafety.UpdateCitations(citation3);
                    
                    //010 - END

                    //Get Safety ID
                    if (Result == true)
                    {
                        string OutlookString = string.Empty;
                        string OutlookStringSub = string.Empty;
                        DAL.VQFSafety EditSafetyData = objSafety.GetSingleVQLSafetyData(VendorId);
                        hdnSafetyId.Value = Convert.ToString(EditSafetyData.PK_SafetyID);
                        ViewState["SafetyID"] = Convert.ToString(EditSafetyData.PK_SafetyID);   //GV - 11/17/2014
                        SafetyId = Convert.ToInt64(hdnSafetyId.Value);
                        objCommon.CompareChangeConsolidated = string.Empty;
                        string CompareData = string.Empty;
                        VMSDAL.VQFSafetyEMRRate objRatesmodified = objSafety.SelectSafetyEMRRates(SafetyId, txtYearOne.Text.Trim());
                        if (objRates != null)
                        {
                            if ((objRates.NotAvailable.ToString() != objRatesmodified.NotAvailable.ToString()))
                            {
                                objCommon.CompareChanges += "Not Available Was= " + Radio(objRates.NotAvailable.ToString()) + " / Now= " + Radio(objRatesmodified.NotAvailable.ToString()) + ", ";
                                if (Convert.ToBoolean(objRatesmodified.NotAvailable))
                                {
                                    //objCommon.CompareChanges += "Explanation Data=" + objRatesmodified.Explanation.ToString() + " Deleted Fields:EMRRate Data=" + objRates.EMRRate.ToString();
                                    objCommon.CompareChanges += "Explanation =" + objRatesmodified.Explanation.ToString();
                                }
                                else if (!Convert.ToBoolean(objRatesmodified.NotAvailable))
                                {
                                    //objCommon.CompareChanges += "EMR Rate Data=" + objRatesmodified.EMRRate.ToString() + " Deleted Fields:Explanation Data=" + objRates.Explanation.ToString();
                                    objCommon.CompareChanges += "EMR Rate =" + objRatesmodified.EMRRate.ToString();
                                }
                            }
                            else
                            {
                                if (Convert.ToBoolean(objRatesmodified.NotAvailable) && objRates.Explanation.ToString() != objRatesmodified.Explanation.ToString())
                                {
                                    objCommon.CompareChanges += "Explanation Was= " + objRates.Explanation.ToString() + " / Now= " + objRatesmodified.Explanation.ToString();
                                }
                                else if (objRates.EMRRate.ToString() != objRatesmodified.EMRRate.ToString())
                                {
                                    objCommon.CompareChanges += "EMR Rate Was= " + EmptyValue(objRates.EMRRate.ToString()) + " / Now= " + EmptyValue(objRatesmodified.EMRRate.ToString());
                                }
                            }
                        }
                        else
                        {

                            if (!string.IsNullOrEmpty(objRatesmodified.EMRRate))
                            {
                                if (Convert.ToBoolean(objRatesmodified.NotAvailable))
                                {
                                    objCommon.CompareChanges += "Explanation Was=  " + "No Value" + " / Now= " + EmptyValue(objRatesmodified.Explanation.ToString());
                                }
                                else
                                {
                                    objCommon.CompareChanges += "EMR Rate Was= " + "No Value " + " / Now= " + EmptyValue(objRatesmodified.EMRRate.ToString());
                                }
                            }
                        }

            //010 - START
            // compare new safety questions
            if (EditData != null)
            {
                if (trNonUSSafetyQuestion.Style["display"] != "none")
                {
                    if (EditData.SafetyRepresentation != null)
                    {
                        if (rdoSafetyDirector.SelectedItem != null)
                        {
                            if ((bool)EditData.SafetyRepresentation != rdoSafetyDirector.SelectedItem.Text.ConvertToBool())
                            {
                                objCommon.CompareChanges += Tab + "Safety Director/Representation Was= " + ((bool)EditData.SafetyRepresentation).ConvertToYesNo() + " / Now= " + rdoSafetyDirector.SelectedItem.Text + "%0A";
                            }
                        }
                    }
                    else
                    {
                        if (rdoSafetyDirector.SelectedItem != null) objCommon.CompareChanges += Tab + "Safety Director/Representation Was=No Value" + " / Now= " + rdoSafetyDirector.SelectedItem.Text + "%0A";
                    }
                }

                if (EditData.ProfessionalLevelOfTraining != null)
                {
                    if (EditData.ProfessionalLevelOfTraining.ToLower().Trim() != txtProLevelOfTraining.Text.ToLower().Trim())
                    {
                        objCommon.CompareChanges += Tab + "Level of Training Was= " + EditData.ProfessionalLevelOfTraining + " / Now= " + txtProLevelOfTraining.Text.ToLower().Trim() + "%0A";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(txtProLevelOfTraining.Text)) objCommon.CompareChanges += Tab + "Level of Training Was=No Value" + " / Now= " + txtProLevelOfTraining.Text + "%0A";
                }

                if (EditData.IsPeriodicSafetyMeetings != null)
                {
                    if (rdoListSafetyMeetings.SelectedItem != null)
                    {
                        if ((bool)EditData.IsPeriodicSafetyMeetings != rdoListSafetyMeetings.SelectedItem.Text.ConvertToBool())
                        {
                            objCommon.CompareChanges += Tab + "Hold Periodic Safety Meetings= " + ((bool)EditData.IsPeriodicSafetyMeetings).ConvertToYesNo() + " / Now= " + rdoListSafetyMeetings.SelectedItem.Text + "%0A";
                        }
                    }
                }
                else
                {
                    if (rdoListSafetyMeetings.SelectedItem != null) objCommon.CompareChanges += Tab + "Hold Periodic Safety Meetings=No Value" + " / Now= " + rdoListSafetyMeetings.SelectedItem.Text + "%0A";
                }

                if (EditData.SafetyMeetingsPeriods != null)
                {
                    if (EditData.SafetyMeetingsPeriods != safetyMeetingsPeriods)
                    {
                        objCommon.CompareChanges += Tab + "Safety meetings held how often= " + EditData.SafetyMeetingsPeriods + " / Now= " + safetyMeetingsPeriods + "%0A";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(safetyMeetingsPeriods)) objCommon.CompareChanges += Tab + "Safety meetings held how often=No Value" + " / Now= " + safetyMeetingsPeriods + "%0A";
                }

                if (EditData.IsProvideEmployeeTrainingDocumentation != null)
                {
                    if (isProvideDocumentation != null)
                    {
                        if (EditData.IsProvideEmployeeTrainingDocumentation != isProvideDocumentation)
                        {
                            objCommon.CompareChanges += Tab + "Can provide training documentation= " + ((bool)EditData.IsProvideEmployeeTrainingDocumentation).ConvertToYesNo() + " / Now= "
                                + ((bool)isProvideDocumentation).ConvertToYesNo() + "%0A";
                        }
                    }
                }
                else
                {
                    if (isProvideDocumentation != null) objCommon.CompareChanges += Tab + "Can provide training documentation=No Value" + " / Now= " + ((bool)isProvideDocumentation).ConvertToYesNo() + "%0A";
                }

                if (EditData.IsSafetyInspectionOfWIP != null)
                {
                    if (isInspectionWIP != null)
                    {
                        if (EditData.IsSafetyInspectionOfWIP != isInspectionWIP)
                        {
                            objCommon.CompareChanges += Tab + "Safety inspections of WIP= " + ((bool)EditData.IsSafetyInspectionOfWIP).ConvertToYesNo() + " / Now= " + ((bool)isInspectionWIP).ConvertToYesNo() + "%0A";
                        }
                    }
                }
                else
                {
                    if (isInspectionWIP != null) objCommon.CompareChanges += Tab + "Safety inspections of WIP=No Value" + " / Now= " + ((bool)isInspectionWIP).ConvertToYesNo() + "%0A";
                }

                if (EditData.IsWrittenDrugAlcoholProgram != null)
                {
                    if (isDrugAlcohol != null)
                    {
                        if (EditData.IsWrittenDrugAlcoholProgram != isDrugAlcohol)
                        {
                            objCommon.CompareChanges += Tab + "Drug and alcohol program= " + ((bool)EditData.IsWrittenDrugAlcoholProgram).ConvertToYesNo() + " / Now= " + ((bool)isDrugAlcohol).ConvertToYesNo() + "%0A";
                        }
                    }
                }
                else
                {
                    if (isDrugAlcohol != null) objCommon.CompareChanges += Tab + "Drug and alcohol program=No Value" + " / Now= " + ((bool)isDrugAlcohol).ConvertToYesNo() + "%0A";
                }

                if (!string.IsNullOrEmpty(EditData.WhoConductsInspectionOfWIP))
                {
                    if (EditData.WhoConductsInspectionOfWIP != whoConductsInspectionWIP)
                    {
                        objCommon.CompareChanges += Tab + "Drug and alcohol program= " + EditData.WhoConductsInspectionOfWIP + " / Now= " + whoConductsInspectionWIP + "%0A";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(whoConductsInspectionWIP)) objCommon.CompareChanges += Tab + "Drug and alcohol program=No Value" + " / Now= " + whoConductsInspectionWIP + "%0A";
                }

                if (EditData.InspectionOfWIPPeriods != null)
                {
                    if (EditData.InspectionOfWIPPeriods != inspectionPeriods)
                    {
                        objCommon.CompareChanges += Tab + "Inspection of WIP, how often= " + EditData.InspectionOfWIPPeriods + " / Now= " + inspectionPeriods + "%0A";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(inspectionPeriods)) objCommon.CompareChanges += Tab + "Inspections of WIP, how often=No Value" + " / Now= " + inspectionPeriods + "%0A";
                }

                //Conduct job hazard analysis
                if (EditData.IsConductHazardAnalysis != null)
                {
                    if (isHazardAnalysis != null)
                    {
                        if (EditData.IsConductHazardAnalysis != isHazardAnalysis)
                        {
                            objCommon.CompareChanges += Tab + "Conducts job hazard analysis was " + ((bool)EditData.IsConductHazardAnalysis).ConvertToYesNo() + " / Now= " + ((bool)isHazardAnalysis).ConvertToYesNo() + "%0A";
                        }
                    }
                }
                else
                {
                    if (isHazardAnalysis != null) objCommon.CompareChanges += Tab + "Conducts job hazard analysis had No Value" + " / Now= " + ((bool)isHazardAnalysis).ConvertToYesNo() + "%0A";
                }

                if (EditData.ConductHazardAnalysisPeriods != null)
                {
                    if (EditData.ConductHazardAnalysisPeriods != hazardPeriods)
                    {
                        objCommon.CompareChanges += Tab + "Conducts job hazard analysis, how often was " + EditData.ConductHazardAnalysisPeriods + " / Now= " + hazardPeriods.Replace("|", ", ") + "%0A";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(hazardPeriods)) objCommon.CompareChanges += Tab + "Conducts job hazard analysis, how often had No Value" + " / Now= " + hazardPeriods.Replace("|", ", ") + "%0A";
                }
            }
                        
                        //010 - END


                        //type = objRates.GetType();
                        //objCommon.CompareChanges = Compare.CompareObjects(objRates, objSafety.SelectSafetyEMRRates(SafetyId, txtYearOne.Text.Trim()), type);
                        if (objCommon.CompareChanges != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += txtYearOne.Text.Trim() + ": " + objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + txtYearOne.Text.Trim() + ": " + objCommon.CompareChanges + "<br></font></td></tr>");
                            OutlookString += "%09" + txtYearOne.Text.Trim() + ": " + objCommon.CompareChanges + "%0A";
                            objCommon.CompareChanges = string.Empty;
                        }


                        objRatesmodified = objSafety.SelectSafetyEMRRates(SafetyId, txtYearTwo.Text.Trim());
                        if (objRates1 != null)
                        {
                            if (objRates1.NotAvailable.ToString() != objRatesmodified.NotAvailable.ToString())
                            {
                                objCommon.CompareChanges += "Not Available Was= " + Radio(objRates1.NotAvailable.ToString()) + " / Now= " + Radio(objRatesmodified.NotAvailable.ToString()) + ", ";
                                if (Convert.ToBoolean(objRatesmodified.NotAvailable))
                                {
                                    //objCommon.CompareChanges += "Explanation Data=" + objRatesmodified.Explanation.ToString() + " Deleted Fields:EMRRate Data=" + objRates1.EMRRate.ToString();

                                    objCommon.CompareChanges += "Explanation = " + objRatesmodified.Explanation.ToString();
                                }
                                else if (!Convert.ToBoolean(objRatesmodified.NotAvailable))
                                {
                                    //objCommon.CompareChanges += "EMR Rate Data=" + objRatesmodified.EMRRate.ToString() + " Deleted Fields:Explanation Data=" + objRates1.Explanation.ToString();
                                    objCommon.CompareChanges += "EMR Rate = " + objRatesmodified.EMRRate.ToString();
                                }
                            }
                            else
                            {
                                if (Convert.ToBoolean(objRatesmodified.NotAvailable) && objRates1.Explanation.ToString() != objRatesmodified.Explanation.ToString())
                                {
                                    objCommon.CompareChanges += "Explanation Was= " + objRates1.Explanation.ToString() + " / Now= " + objRatesmodified.Explanation.ToString();
                                }
                                else if (objRates1.EMRRate.ToString() != objRatesmodified.EMRRate.ToString())
                                {
                                    objCommon.CompareChanges += "EMR Rate Was= " + EmptyValue(objRates1.EMRRate.ToString()) + " / Now= " + EmptyValue(objRatesmodified.EMRRate.ToString());
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objRatesmodified.EMRRate))
                            {

                                if (Convert.ToBoolean(objRatesmodified.NotAvailable))
                                {
                                    objCommon.CompareChanges += "Explanation Was= " + "No Value " + " / Now= " + EmptyValue(objRatesmodified.Explanation.ToString());
                                }
                                else
                                {
                                    objCommon.CompareChanges += "EMR Rate Was= " + "No Value " + " / Now= " + EmptyValue(objRatesmodified.EMRRate.ToString());
                                }
                            }
                        }

                        //type = objRates1.GetType();
                        //objCommon.CompareChanges = Compare.CompareObjects(objRates1, objSafety.SelectSafetyEMRRates(SafetyId, txtYearTwo.Text.Trim()), type);
                        if (objCommon.CompareChanges != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += txtYearTwo.Text.Trim() + ": " + objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + txtYearTwo.Text.Trim() + ": " + objCommon.CompareChanges + "<br></font></td></tr>");
                            OutlookString += "%09" + txtYearTwo.Text.Trim() + ": " + objCommon.CompareChanges + "%0A";
                            objCommon.CompareChanges = string.Empty;

                        }

                        objRatesmodified = objSafety.SelectSafetyEMRRates(SafetyId, txtYearThree.Text.Trim());
                        if (objRates2 != null)
                        {
                            if (objRates2.NotAvailable.ToString() != objRatesmodified.NotAvailable.ToString())
                            {
                                objCommon.CompareChanges += "Not Available Was= " + Radio(objRates2.NotAvailable.ToString()) + " / Now= " + Radio(objRatesmodified.NotAvailable.ToString()) + ", ";
                                if (Convert.ToBoolean(objRatesmodified.NotAvailable))
                                {
                                    //objCommon.CompareChanges += "Explanation Data=" + objRatesmodified.Explanation.ToString() + " Deleted Fields:EMRRate Data=" + objRates2.EMRRate.ToString();
                                    objCommon.CompareChanges += "Explanation = " + objRatesmodified.Explanation.ToString();
                                }
                                else if (!Convert.ToBoolean(objRatesmodified.NotAvailable))
                                {
                                    //objCommon.CompareChanges += "EMR Rate Data=" + objRatesmodified.EMRRate.ToString() + " Deleted Fields:Explanation Data=" + objRates2.Explanation.ToString();
                                    objCommon.CompareChanges += "EMR Rate = " + objRatesmodified.EMRRate.ToString();
                                }
                            }
                            else
                            {
                                if (Convert.ToBoolean(objRatesmodified.NotAvailable) && objRates2.Explanation.ToString() != objRatesmodified.Explanation.ToString())
                                {
                                    objCommon.CompareChanges += "Explanation Was= " + objRates2.Explanation.ToString() + " / Now= " + EmptyValue(objRatesmodified.Explanation.ToString());
                                }
                                else if (objRates2.EMRRate.ToString() != objRatesmodified.EMRRate.ToString())
                                {
                                    objCommon.CompareChanges += "EMR Rate Was= " + objRates2.EMRRate.ToString() + " / Now= " + EmptyValue(objRatesmodified.EMRRate.ToString());
                                }

                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objRatesmodified.EMRRate))
                            {
                                if (Convert.ToBoolean(objRatesmodified.NotAvailable))
                                {
                                    objCommon.CompareChanges += "Explanation Was= " + "No Value" + " / Now= " + EmptyValue(objRatesmodified.Explanation.ToString());
                                }
                                else
                                {
                                    objCommon.CompareChanges += "EMR Rate Was= " + "No Value" + " / Now= " + EmptyValue(objRatesmodified.EMRRate.ToString());
                                }
                            }
                        }
                        //type = objRates2.GetType();
                        //objCommon.CompareChanges = Compare.CompareObjects(objRates2, objSafety.SelectSafetyEMRRates(SafetyId, txtYearThree.Text.Trim()), type);
                        if (objCommon.CompareChanges != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += txtYearThree.Text.Trim() + ": " + objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + txtYearThree.Text.Trim() + ": " + objCommon.CompareChanges + "<br></font></td></tr>");
                            OutlookString += "%09" + txtYearThree.Text.Trim() + ": " + objCommon.CompareChanges + "%0A";
                            objCommon.CompareChanges = string.Empty;

                        }

                        //008 EMR rate 4 if applicable was...left out
                        if (chkNA1.Checked)
                        {
                            objRatesmodified = objSafety.SelectSafetyEMRRates(SafetyId, txtYearFour.Text.Trim());

                            if (objRates4.NotAvailable.ToString() != objRatesmodified.NotAvailable.ToString())
                            {
                                objCommon.CompareChanges += "NotAvailable Was=" + objRates4.NotAvailable.ToString() + " / Now=" + objRatesmodified.NotAvailable.ToString() + ",";
                                if (Convert.ToBoolean(objRatesmodified.NotAvailable))
                                {
                                    objCommon.CompareChanges += "Explanation Data=" + objRatesmodified.Explanation.ToString() + " Deleted Fields:EMRRate Data=" + objRates4.EMRRate.ToString();
                                }
                                else if (!Convert.ToBoolean(objRatesmodified.NotAvailable))
                                {
                                    objCommon.CompareChanges += "EMRRate Data=" + objRatesmodified.EMRRate.ToString() + " Deleted Fields:Explanation Data=" + objRates4.Explanation.ToString();
                                }
                            }
                            else
                            {
                                if (Convert.ToBoolean(objRatesmodified.NotAvailable) && objRates4.Explanation.ToString() != objRatesmodified.Explanation.ToString())
                                {
                                    objCommon.CompareChanges += "Explanation Was=" + objRates4.Explanation.ToString() + " / Now=" + objRatesmodified.Explanation.ToString();
                                }
                                else if (objRates4.EMRRate.ToString() != objRatesmodified.EMRRate.ToString())
                                {
                                    objCommon.CompareChanges += "EMRRate Was=" + objRates4.EMRRate.ToString() + " / Now=" + objRatesmodified.EMRRate.ToString();
                                }
                            }
                            if (objCommon.CompareChanges != string.Empty)
                            {
                                objCommon.CompareChangeConsolidated += txtYearFour.Text.Trim() + ": " + objCommon.CompareChanges + Break;
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + txtYearFour.Text.Trim() + ": " + objCommon.CompareChanges + "<br></font></td></tr>");
                                OutlookString += "%09" + txtYearFour.Text.Trim() + ": " + objCommon.CompareChanges + "%0A";
                                objCommon.CompareChanges = string.Empty;
                            }
                        }

                        if (objCommon.CompareChangeConsolidated != string.Empty)
                        {
                            CompareData += ((objCommon.CompareChangeConsolidated.Contains("EMR"))  ? EMRRate : "") + Break + objCommon.CompareChangeConsolidated;
                            CompareBody.Append("</table>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>" + ((objCommon.CompareChangeConsolidated.Contains("EMR")) ? EMRRate : "") + "</u></b><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("</table>");
                            CompareMainBody.Append(CompareBody);
                            OutlookString = "%0A" + ((objCommon.CompareChangeConsolidated.Contains("EMR")) ? EMRRate : "") + "%0A" + OutlookString;
                        }
                        CompareBody = new StringBuilder();



                        objCommon.CompareChangeConsolidated = string.Empty;
                        VMSDAL.VQFSafetyOSHA objOshamodified = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearOne.Text.Trim());


                        if (objOsha1 != null)
                        {
                            type = objOsha1.GetType();
                            if (objOsha1.NotAvailable.ToString() != objOshamodified.NotAvailable.ToString())
                            {
                                if (Convert.ToBoolean(objOsha1.NotAvailable) && objOsha1.Explanation.ToString() != objOshamodified.Explanation.ToString())
                                {
                                    objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha1, objOshamodified, type, "Explanation", "First");
                                }
                                else
                                {
                                    objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha1, objOshamodified, type, "Explanation", "Last");
                                }
                            }
                            else
                            {
                                objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha1, objOshamodified, type, "", "");
                            }
                        }
                        else
                        {
                            if (Convert.ToBoolean(objOshamodified.NotAvailable))
                            {
                                objCommon.CompareChanges += "Explanation Was= " + " " + " / Now= " + objOshamodified.Explanation.ToString();
                            }
                            else
                            {
                                objCommon.CompareChanges += "Total No of Days Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoDays.ToString() + ",  ";
                                objCommon.CompareChanges += "Total No of Job Transfer Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoJobTransfer.ToString() + ",  ";
                                objCommon.CompareChanges += "Total No of Other Record Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoOtherRecord.ToString() + ",  ";
                                objCommon.CompareChanges += "Total Fatalities Was= " + " No Value" + " / Now= " + objOshamodified.TotalFatalities.ToString() + ",  ";
                                objCommon.CompareChanges += "Total Hours Worked Was= " + " No Value" + " / Now= " + objOshamodified.TotalHoursWorked.ToString() + ",  ";
                            }
                        }
                        if (objCommon.CompareChanges != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += txtOSHAYearOne.Text.Trim() + ": " + objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + txtOSHAYearOne.Text.Trim() + ": " + objCommon.CompareChanges + "<br></font></td></tr>");
                            OutlookStringSub += "%09" + txtOSHAYearOne.Text.Trim() + ": " + objCommon.CompareChanges + "%0A";
                            objCommon.CompareChanges = string.Empty;
                        }


                        objOshamodified = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearTwo.Text.Trim());
                        if (objOsha2 != null)
                        {
                            type = objOsha2.GetType();
                            if (objOsha2.NotAvailable.ToString() != objOshamodified.NotAvailable.ToString())
                            {
                                if (Convert.ToBoolean(objOsha2.NotAvailable) && objOsha2.Explanation.ToString() != objOshamodified.Explanation.ToString())
                                {
                                    objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha2, objOshamodified, type, "Explanation", "First");
                                }
                                else
                                {
                                    objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha2, objOshamodified, type, "Explanation", "Last");
                                }
                            }
                            else
                            {
                                objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha2, objOshamodified, type, "", "");
                            }
                        }
                        else
                        {
                            if (Convert.ToBoolean(objOshamodified.NotAvailable))
                            {
                                objCommon.CompareChanges += "Explanation Was= " + "No Value " + " / Now= " + objOshamodified.Explanation.ToString();
                            }
                            else
                            {
                                objCommon.CompareChanges += "Total No of Days Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoDays.ToString() + ",  ";
                                objCommon.CompareChanges += "Total No of Job Transfer Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoJobTransfer.ToString() + ",  ";
                                objCommon.CompareChanges += "Total No of Other Record Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoOtherRecord.ToString() + ",  ";
                                objCommon.CompareChanges += "Total Fatalities Was= " + "No Value " + " / Now= " + objOshamodified.TotalFatalities.ToString() + ",  ";
                                objCommon.CompareChanges += "Total Hours Worked Was= " + "No Value " + " / Now= " + objOshamodified.TotalHoursWorked.ToString() + ",  ";
                            }
                        }
                        if (objCommon.CompareChanges != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += txtOSHAYearTwo.Text.Trim() + ": " + objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + txtOSHAYearTwo.Text.Trim() + ": " + objCommon.CompareChanges + "<br></font></td></tr>");
                            OutlookStringSub += "%09" + txtOSHAYearTwo.Text.Trim() + ": " + objCommon.CompareChanges + "%0A";
                            objCommon.CompareChanges = string.Empty;
                        }



                        objOshamodified = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearThree.Text.Trim());
                        if (objOsha3 != null)
                        {
                            type = objOsha3.GetType();
                            if (objOsha3.NotAvailable.ToString() != objOshamodified.NotAvailable.ToString())
                            {
                                if (Convert.ToBoolean(objOsha3.NotAvailable) && objOsha3.Explanation.ToString() != objOshamodified.Explanation.ToString())
                                {
                                    objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha3, objOshamodified, type, "Explanation", "First");
                                }
                                else
                                {
                                    objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha3, objOshamodified, type, "Explanation", "Last");
                                }
                            }
                            else
                            {
                                objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha3, objOshamodified, type, "", "");
                            }
                        }
                        else
                        {
                            if (Convert.ToBoolean(objOshamodified.NotAvailable))
                            {
                                objCommon.CompareChanges += "Explanation Was= " + "No Value " + " / Now= " + objOshamodified.Explanation.ToString();
                            }
                            else
                            {
                                objCommon.CompareChanges += "Total No of Days Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoDays.ToString() + ",  ";
                                objCommon.CompareChanges += "Total No of Job Transfer Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoJobTransfer.ToString() + ",  ";
                                objCommon.CompareChanges += "Total No of Other Record Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoOtherRecord.ToString() + ",  ";
                                objCommon.CompareChanges += "Total Fatalities Was= " + "No Value " + " / Now= " + objOshamodified.TotalFatalities.ToString() + ",  ";
                                objCommon.CompareChanges += "Total Hours Worked Was= " + "No Value " + " / Now= " + objOshamodified.TotalHoursWorked.ToString() + ",  ";
                            }
                        }
                        if (objCommon.CompareChanges != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += txtOSHAYearThree.Text.Trim() + ": " + objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + txtOSHAYearThree.Text.Trim() + ": " + objCommon.CompareChanges + "<br></font></td></tr>");
                            OutlookStringSub += "%09" + txtOSHAYearThree.Text.Trim() + ": " + objCommon.CompareChanges + "%0A";
                            objCommon.CompareChanges = string.Empty;
                        }
                        if (objCommon.CompareChangeConsolidated != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated = OSHA + Break + objCommon.CompareChangeConsolidated;
                            CompareBody.Append("</table>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>" + OSHA + "</u></b><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("</table>");
                            CompareMainBody.Append(CompareBody);
                            IsAvail = true;
                            OutlookStringSub = "%0A" + OSHA + "%0A" + OutlookStringSub;
                            OutlookString += OutlookStringSub;
                        }

                        //007
                        objOshamodified = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearFour.Text.Trim());
                        if (objOsha4 != null)
                        {
                            type = objOsha4.GetType();
                            if (objOsha4.NotAvailable.ToString() != objOshamodified.NotAvailable.ToString())
                            {
                                if (Convert.ToBoolean(objOsha4.NotAvailable) && objOsha4.Explanation.ToString() != objOshamodified.Explanation.ToString())
                                {
                                    objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha4, objOshamodified, type, "Explanation", "First");
                                }
                                else
                                {
                                    objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha4, objOshamodified, type, "Explanation", "Last");
                                }
                            }
                            else
                            {
                                objCommon.CompareChanges = Compare.CompareObjectsNotAvailableSafety(objOsha4, objOshamodified, type, "", "");
                            }
                        }
                        else
                        {
                            if (Convert.ToBoolean(objOshamodified.NotAvailable))
                            {
                                objCommon.CompareChanges += "Explanation Was= " + "No Value " + " / Now= " + objOshamodified.Explanation.ToString();
                            }
                            else
                            {
                                objCommon.CompareChanges += "Total No of Days Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoDays.ToString() + ",  ";
                                objCommon.CompareChanges += "Total No of Job Transfer Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoJobTransfer.ToString() + ",  ";
                                objCommon.CompareChanges += "Total No of Other Record Was= " + "No Value " + " / Now= " + objOshamodified.TotalNoOtherRecord.ToString() + ",  ";
                                objCommon.CompareChanges += "Total Fatalities Was= " + "No Value " + " / Now= " + objOshamodified.TotalFatalities.ToString() + ",  ";
                                objCommon.CompareChanges += "Total Hours Worked Was= " + "No Value " + " / Now= " + objOshamodified.TotalHoursWorked.ToString() + ",  ";
                            }
                        }
                        

                        if (objCommon.CompareChanges != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += txtOSHAYearFour.Text.Trim() + ": " + objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + txtOSHAYearFour.Text.Trim() + ": " + objCommon.CompareChanges + "<br></font></td></tr>");
                            OutlookStringSub += "%09" + txtOSHAYearFour.Text.Trim() + ": " + objCommon.CompareChanges + "%0A";
                            objCommon.CompareChanges = string.Empty;
                        }

                        

                        if (objCommon.CompareChangeConsolidated != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated = OSHA + Break + objCommon.CompareChangeConsolidated;
                            CompareBody.Append("</table>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>" + OSHA + "</u></b><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("</table>");
                            CompareMainBody.Append(CompareBody);
                            IsAvail = true;
                            OutlookStringSub = "%0A" + OSHA + "%0A" + OutlookStringSub;
                            OutlookString += OutlookStringSub;
                        }

                        //008 - attachment changed
                        if (attachmentChanged)
                        {
                            OutlookString += attachmentChangedText + "%0A";
                        }
                        OutlookStringSub = string.Empty;
                        CompareBody = new StringBuilder();


                        CompareData += objCommon.CompareChangeConsolidated;

                        objCommon.CompareChangeConsolidated = string.Empty;

                        //type = EditData.GetType();
                        //objCommon.CompareChanges = Compare.CompareObjects(EditData, objSafety.GetSingleVQLSafetyData(VendorId), type);
                        objCommon.CompareChanges = string.Empty;

                        if (Convert.ToBoolean(EditSafetyData.SICNA) != false)
                        {
                            if (chkSICNA.Checked)
                            {
                                if (EditData.Explanation.Trim() != txtSICExplain.Text.Trim())
                                {
                                    OshaPart += "SIC/NAICS Code: Explanation Was= " + EditData.Explanation.ToString() + " / Now= " + txtSICExplain.Text.Trim();
                                }
                            }
                            else
                            {
                                if (EditData.SICNAICSCode.Trim() != txtSIC.Text.Trim())
                                {
                                    OshaPart += "SIC/NAICS Code: Was= " + EditData.SICNAICSCode.ToString() + " / Now= " + txtSIC.Text.Trim();
                                }

                            }
                        }
                        //else
                        //{
                        //    if (chkSICNA.Checked)
                        //    {
                        //        //OshaPart += "SIC/NAICS Code: Not Available Was=" + EditData.SICNA.ToString() + " / Now= " + chkSICNA.Checked.ToString() + ", Explanation Data=" + txtSICExplain.Text.Trim() + " Deleted SIC/NAICSCode Data=" + EditData.SICNAICSCode.ToString();
                        //        OshaPart += "SIC/NAICS Code: Not Available Was= " + EditData.SICNA.ToString() + " / Now= " + chkSICNA.Checked.ToString() + ", Explanation =" + txtSICExplain.Text.Trim();
                        //    }
                        //    else if (!chkSICNA.Checked)
                        //    {
                        //        //OshaPart += " SIC/NAICS Code: Not Available Was=" + EditData.SICNA.ToString() + " / Now= " + chkSICNA.Checked.ToString() + ", SIC/NAICSCode Data=" + txtSIC.Text.Trim() + " Deleted Explanation Data=" + EditData.Explanation.ToString();
                        //        OshaPart += " SIC/NAICS Code: Not Available Was= " + EditData.SICNA.ToString() + " / Now= " + chkSICNA.Checked.ToString() + ", SIC/NAICSCode =" + txtSIC.Text.Trim();
                        //    }
                        //}
                        bool currnetsafety = false;
                        string phoneno = objCommon.Phoneformat(txtMainPhno1.Text.Trim(), txtMainPhno2.Text.Trim(), txtMainPhno3.Text.Trim(), txtMainPhno3);     //008 - bug fix
                        if (rdoSafetyProgram.SelectedIndex == 0)
                            currnetsafety = true;
                        else if (rdoSafetyProgram.SelectedIndex == 1)
                            currnetsafety = false;
                        if (!string.IsNullOrEmpty(Convert.ToString(EditSafetyData.SafetyProgram)))
                        {
                            if (currnetsafety != EditSafetyData.SafetyProgram)
                            {
                                objCommon.CompareChanges += "Safety Program Was= " + Compare.Radio(EditSafetyData.SafetyProgram.ToString()) + " / Now= " + Compare.Radio(currnetsafety.ToString()) + "%0A";
                            }
                        }




                        if (rdoSafetytraining.SelectedIndex == 0)
                            currnetsafety = true;
                        else if (rdoSafetytraining.SelectedIndex == 1)
                            currnetsafety = false;
                        if (!string.IsNullOrEmpty(Convert.ToString(EditSafetyData.SafetyRequirements)))
                        {
                            if (currnetsafety != EditSafetyData.SafetyRequirements)
                            {
                                objCommon.CompareChanges += "Safety Requirements Was= " + Compare.Radio(EditSafetyData.SafetyRequirements.ToString()) + " / Now= " + Compare.Radio(currnetsafety.ToString()) + "%0A";
                            }
                        }


                        if (rdoSafetyDirector.SelectedIndex == 0)
                            currnetsafety = true;
                        else if (rdoSafetyDirector.SelectedIndex == 1)
                            currnetsafety = false;
                        if (currnetsafety == EditSafetyData.CompanySafety && currnetsafety == true)
                        {
                            if (txtContactName.Text.Trim() != EditSafetyData.CompanySafetyContactName)
                                objCommon.CompareChanges += "Company Safety Contact Name Was= " + Compare.Radio(EditSafetyData.CompanySafetyContactName.ToString()) + " / Now= " + txtContactName.Text.Trim();
                            if (phoneno != EditSafetyData.CompanySafetyContactNumber)
                                objCommon.CompareChanges += "Company Safety Contact Number Was= " + Compare.Radio(EditSafetyData.CompanySafetyContactNumber.ToString()) + " / Now= " + phoneno;

                            //objCommon.CompareChanges += "%0A";
                        }
                        else if (currnetsafety != EditSafetyData.CompanySafety)
                        {

                            if (!string.IsNullOrEmpty(Convert.ToString(EditSafetyData.CompanySafety)))
                            {
                                if (currnetsafety)
                                {
                                    objCommon.CompareChanges += "Company Safety Was= " + Compare.Radio(EditSafetyData.CompanySafety.ToString()) + " / Now= " + Compare.Radio(currnetsafety.ToString()) + ", Company Safety Contact Name =" + txtContactName.Text.Trim() + ",Company Safety Contact Number =" + phoneno + "%0A";
                                }
                                else if (!currnetsafety)
                                {
                                    // objCommon.CompareChanges += "Company Safety Was= " + EditData.CompanySafety.ToString() + " / Now= " + currnetsafety.ToString() + " Deleted CompanySafetyContactName =" + EditData.CompanySafetyContactName.ToString() + ",Company Safety Contact Number =" + EditData.CompanySafetyContactNumber.ToString() + "%0A";
                                    objCommon.CompareChanges += "Company Safety Was= " + Compare.Radio(EditSafetyData.CompanySafety.ToString()) + " / Now= " + Compare.Radio(currnetsafety.ToString()) + "%0A";
                                }
                            }
                        }



                        //005 start
                        bool currentRepreset = false;
                        string phonenoRepreset = objCommon.Phoneformat(txtRepresentationContactNumberOne.Text.Trim(), txtRepresentationContactNumberTwo.Text.Trim(), txtRepresentationContactNumberThree.Text.Trim(), txtRepresentationContactNumberThree);
                        currentRepreset = objCommon.ReturnCheckedStatus(rdoRepresentation).HasValue ? objCommon.ReturnCheckedStatus(rdoRepresentation).Value : false;
                        if (EditData != null)
                        {
                            if (currentRepreset == EditData.SafetyRepresentation && currentRepreset == true)
                            {
                                if (txtRepresentationContactName.Text.Trim() != EditData.SafetyRepresentationContactName)
                                    objCommon.CompareChanges += "Safety Representation Contact Name Was= " + EditData.SafetyRepresentationContactName.ToString() + " / Now= " + txtRepresentationContactName.Text.Trim();
                                if (phonenoRepreset != EditData.SafetyRepresentationContactNumber)
                                    objCommon.CompareChanges += "Safety Representation Contact Number Was= " + EditData.SafetyRepresentationContactNumber.ToString() + " / Now= " + phonenoRepreset;
                            }

                            else if (currentRepreset != EditData.SafetyRepresentation)
                            {

                                if (!string.IsNullOrEmpty(Convert.ToString(EditData.SafetyRepresentation)))
                                {
                                    if (currentRepreset)
                                    {
                                        objCommon.CompareChanges += "Safety Representation Was= " + Compare.Radio(EditData.SafetyRepresentation.ToString()) + " / Now= " + Compare.Radio(currentRepreset.ToString()) + ", Safety Representation Contact Name =" + txtRepresentationContactName.Text.Trim() + ",Safety Representation Contact Number =" + phonenoRepreset + "%0A";
                                    }
                                    else if (!currentRepreset)
                                    {
                                        // objCommon.CompareChanges += "Company Safety Was= " + EditData.CompanySafety.ToString() + " / Now= " + currnetsafety.ToString() + " Deleted CompanySafetyContactName =" + EditData.CompanySafetyContactName.ToString() + ",Company Safety Contact Number =" + EditData.CompanySafetyContactNumber.ToString() + "%0A";
                                        objCommon.CompareChanges += "Safety Representation Was= " + Compare.Radio(EditData.SafetyRepresentation.ToString()) + " / Now= " + Compare.Radio(currentRepreset.ToString()) + "%0A";
                                    }
                                }
                            }
                        }

                        //005 end

                        //string[] a = objCommon.CompareChanges.Split(';');
                        //int k = 0;
                        //objCommon.CompareChanges = string.Empty;
                        //while (k < a.Length - 1)
                        //{
                        //    //if (a[k] == "SICNAICSCode")
                        //    //    OshaPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                        //    //else if (a[k] == "SICNA")
                        //    //    OshaPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                        //    //else if (a[k] == "Explanation")
                        //    //    OshaPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                        //    //else if (k == a.Length - 1)
                        //    //    objCommon.CompareChanges += a[k];
                        //    //else
                        //    //    objCommon.CompareChanges += a[k] + ",";
                        //    if (!chkSICNA.Checked && a[k].Substring(6, a[k].IndexOf(' ')) == "SICNAICSCode")
                        //        OshaPart += "%09" + a[k] + "%0A";
                        //    else if (!chkSICNA.Checked && a[k].Substring(6, a[k].IndexOf(' ')) == "SICNA")
                        //        OshaPart += "%09" + a[k] + "%0A";
                        //    else if (chkSICNA.Checked && a[k].Substring(6, a[k].IndexOf(' ')) == "Explanation")
                        //        OshaPart += "%09" + a[k] + "%0A";
                        //    else if (k == a.Length - 1)
                        //        objCommon.CompareChanges += "%09" + a[k] + "%0A";
                        //    else if (a[k].Substring(6, a[k].IndexOf(' ')) != "Explanation" && a[k].Substring(6, a[k].IndexOf(' ')) != "SICNAICSCode" && a[k].Substring(6, a[k].IndexOf(' ')) != "SICNA")
                        //        objCommon.CompareChanges += "%09" + a[k] + "%0A";
                        //    k++;
                        //}
                        if (IsAvail)
                        {
                            CompareMainBody.Append(OshaPart);
                            OutlookString += OshaPart;
                        }
                        else
                        {
                            CompareBody.Append(OshaPart);
                            CompareBody.Append("</table>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>" + OSHA + "</u></b><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("</table>");
                            CompareMainBody.Append(CompareBody);
                            if (OshaPart != "")
                                OutlookStringSub += "%0A" + OSHA + "%0A" + OshaPart;

                            OutlookString += OutlookStringSub;
                        }
                        OutlookStringSub = string.Empty;
                        CompareBody = new StringBuilder();
                        if (objCommon.CompareChanges != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + objCommon.CompareChanges + "<br></font></td></tr>");
                            OutlookStringSub += "%0A" + objCommon.CompareChanges + "%0A";
                        }
                        if (objCommon.CompareChangeConsolidated != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated = Questionnaire + Break + objCommon.CompareChangeConsolidated;
                            CompareBody.Append("</table>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>" + Questionnaire + "</u></b><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("</table>");
                            CompareMainBody.Append(CompareBody);
                            OutlookStringSub = "%0A" + Questionnaire + OutlookStringSub;
                            OutlookString += OutlookStringSub;
                        }
                        CompareBody = new StringBuilder();

                        CompareData += objCommon.CompareChangeConsolidated;

                        if (OutlookString != string.Empty)
                        {
                            objCommon.CompareChangeOutlook += Safety + Break + CompareData;
                            CompareBody = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='4'><b><u>" + Safety + "</u></b><br></font></td></tr>");
                            CompareBody.Append("</table>");
                            CompareBody.Append(CompareMainBody);
                            CompareBody.Append("</table>");
                            OutlookString = Safety + "%0A" + OutlookString;
                        }

                        imbOk.Style["display"] = "none";
                        imbOkoutlook.Visible = true;


                        if (OutlookString != "SAFETY%0A%0AQuestionnaire%0A%0A")
                        {
                            // Session["OUTLOOKDATA"] = Session["OUTLOOKDATA"].ToString() + "%0A" + OutlookString;//Session["OUTLOOKDATA"].ToString() + CompareBody;
                            HiddenField outlookContent = VQFEditSideMenu.FindControl("hdnOutllook") as HiddenField;
                            outlookContent.Value = OutlookString;
                            HiddenField outlookVendorEmail = VQFEditSideMenu.FindControl("hdnOutllookVendorEmail") as HiddenField;
                            outlookVendorEmail.Value = Session["OUTLOOKTO"].ToString();
                        }
                        lblMsg.Text = "&nbsp;<li>Safety details has been saved successfully</li>";
                        lblheading.Text = "Result";
                        if(e != null) modalExtnd.Show();        //008 - if null sent by another method
                    }
            //    }
            //    else
            //    {
            //        lblheading.Text = "Questionnaire - Validation";
            //        lblMsg.Text = "";
            //        lblError.Text = Errormsg();
            //        ControlVisiblity();
            //        modalExtnd.Show(); accSafety.SelectedIndex = 2;

            //    }
            //}
            //else
            //{

            //    lblheading.Text = "OSHA 200/300 - Validation";
            //    lblMsg.Text = "";
            //    lblError.Text = Errormsg();

            //    modalExtnd.Show();
            //    accSafety.SelectedIndex = 1;
            //}
        //}


        //else
        //{
        //    lblheading.Text = "Current EMR Rates - Validation";
        //    lblMsg.Text = "";
        //    lblError.Text = Errormsg();

        //    modalExtnd.Show();
        //    accSafety.SelectedIndex = 0;
        //}
        //  LoadEditDatas(true);
    }

    /// <summary>
    /// validate page and list the errors
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {

            if (!validator.IsValid)
            {
                //010 - Corrected format
                if (!validator.ErrorMessage.Contains("<li>")) errmsg += "<li>" + validator.ErrorMessage + "</li>";
                else errmsg += validator.ErrorMessage;
            }
        }
        return errmsg;

    }

    private string OSHAErrormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {

            if (!validator.IsValid)
            {
                errmsg += validator.ErrorMessage;
            }
        }
        return errmsg;

    }

    protected void cusSafetyRepresentation_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (trNonUSSafetyQuestion.Style["display"] != "none") //Non US
        {
            if (rdoRepresentation.SelectedIndex == 0)
            {
                if (string.IsNullOrEmpty(txtRepresentationContactName.Text.Trim()))
                {
                    cusSafetyRepresentation.ErrorMessage = "Please specify contact name for safety representation";
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = true;
                }

            }
            else if (rdoRepresentation.SelectedIndex == 1)
            {
                args.IsValid = true;
            }
            else
            {
                cusSafetyRepresentation.ErrorMessage = "Please select a option in safety representation";
                args.IsValid = true;
            }
        }
        else
        {
            args.IsValid = true; //Ignore validation in US vendors 
        }

    }

    /// <summary>
    /// 005 validate phone
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void cusPhoneNonUS_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rdoRepresentation.SelectedIndex == 0)
        {
            if ((txtRepresentationContactNumberOne.Text.Trim() == "")
                          && ((txtRepresentationContactNumberTwo.Text.Trim() == "")
                          && (txtRepresentationContactNumberThree.Text.Trim() == "")))
            {
                cusPhoneNonUS.ErrorMessage = "Please enter contact phone number for Safety Representation ";
                args.IsValid = false;

            }
            else
            {
                bool b = objCommon.ValidatePhoneformat(txtRepresentationContactNumberOne.Text.Trim(), txtRepresentationContactNumberTwo.Text.Trim(), txtRepresentationContactNumberThree.Text.Trim(), txtRepresentationContactNumberThree);
                if ((b == false))
                {
                    cusPhoneNonUS.ErrorMessage = "Please enter contact phone number in required format for safety representation";
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }

        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cusSafetyDirector_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rdoSafetyDirector.SelectedIndex == 0)
        {
            if (string.IsNullOrEmpty(txtContactName.Text.Trim()))
            {
                cusSafetyDirector.ErrorMessage = "Please specify contact name";
                args.IsValid = true;
            }
            //010 - ADD
            else if (string.IsNullOrEmpty(txtProLevelOfTraining.Text.Trim()))
            {
                cusSafetyDirector.ErrorMessage = "Please specify certification or level of training";
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }

        }
        else if (rdoSafetyDirector.SelectedIndex == 1)
        {
            args.IsValid = true;
        }
        else
        {
            cusSafetyDirector.ErrorMessage = "Please select a option in company safety director";
            args.IsValid = true;
        }

    }

    //010 - ADD
    protected void cusQuestionaire_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        cusQuestionaire.ErrorMessage = string.Empty;

        //New employee program
        if (rdoSafetytraining.SelectedIndex == 0)
        {
            if (rdoListEmployeeProgramDocumentation.SelectedIndex < 0)
            {
                args.IsValid = false;
                cusQuestionaire.ErrorMessage += "<li>Please specify whether you can provide documentation on new employee training or not</li>";
            }
        }

        //Drug and Alcohol
        if (rdoListDrugAlcohol.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you have a written drug &amp; alcohol program or not</li>";
        }

        // Safety meetings
        if (rdoListSafetyMeetings.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you hold periodic safety meetings or not</li>";
        }
        else if (rdoListSafetyMeetings.SelectedIndex == 0)
        {
            if (chkSafetyMeetings.SelectedIndex < 0)
            {
                args.IsValid = false;
                cusQuestionaire.ErrorMessage += "<li>Please specify how often you hold Safety meetings</li>";
            }
        }

        //Field safety inspection
        if (rdoListInspectionWIP.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you conduct field safety inspections</li>";
        }
        else if (rdoListInspectionWIP.SelectedIndex == 0)
        {
            if (chkInspectionWIP.SelectedIndex < 0)
            {
                args.IsValid = false;
                cusQuestionaire.ErrorMessage += "<li>Please specify how often you conduct safety field inspections</li>";
            }

            if (string.IsNullOrEmpty(txtWhoInspectionWIP.Text.Trim()))
            {
                args.IsValid = false;
                cusQuestionaire.ErrorMessage += "<li>Please specify who conducts safety inspections</li>";
            }
        }

        //Any safety citations?
        if (rdoCitationYear1.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you had any citations for year</li>" + CitationYear1;
        }
        if (rdoCitationYear2.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you had any citations for year</li>" + CitationYear2;
        }
        if (rdoCitationYear3.SelectedIndex < 0)
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please specify whether you had any citations for year</li>" + CitationYear3;
        }
        if (rdoCitationYear1.SelectedIndex == 0 && string.IsNullOrEmpty(hdnCitationFile1.Value))
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please attach description of circumstances for year " + CitationYear1 + "</li>";
        }
        if (rdoCitationYear2.SelectedIndex == 0 && string.IsNullOrEmpty(hdnCitationFile2.Value))
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please attach description of circumstances for year " + CitationYear2 + "</li>";
        }
        if (rdoCitationYear3.SelectedIndex == 0 && string.IsNullOrEmpty(hdnCitationFile3.Value))
        {
            args.IsValid = false;
            cusQuestionaire.ErrorMessage += "<li>Please attach description of circumstances for year " + CitationYear3 + "</li>";
        }

    }


    /// <summary>
    /// validate phone
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void cusPhone_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rdoSafetyDirector.SelectedIndex == 0)
        {
            if ((txtMainPhno1.Text.Trim() == "")
                          && ((txtMainPhno2.Text.Trim() == "")
                          && (txtMainPhno3.Text.Trim() == "")))
            {
                cusPhone.ErrorMessage = "Please enter contact phone number";
                args.IsValid = true;

            }
            else
            {
                bool b = objCommon.ValidatePhoneformat(txtMainPhno1.Text.Trim(), txtMainPhno2.Text.Trim(), txtMainPhno3.Text.Trim(), txtMainPhno3);
                if ((b == false))
                {
                    cusPhone.ErrorMessage = "Please enter contact phone number in required format";
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }

        }
        else
        {
            args.IsValid = true;
        }
    }
    /// <summary>
    /// Load data to edit
    /// </summary>
    /// <param name="IsMyProfile"></param>
    private void LoadEditDatas(bool IsMyProfile)
    {

        //VendorId = 2;
        int CountSafety = objSafety.GetSafetyCount(VendorId);
        if (CountSafety > 0)
        {
            VMSDAL.VQFSafety EditData = objSafety.GetSingleVQFSafetyData(VendorId); //010 - changed to use the VMSDAL instead

            hdnSafetyId.Value = Convert.ToString(EditData.PK_SafetyID);
            ViewState["EditStatus"] = EditData.EditStatus;
            txtSIC.Text = EditData.SICNAICSCode;
            SafetyId = EditData.PK_SafetyID;

            //010 - START
            txtProLevelOfTraining.Text = EditData.ProfessionalLevelOfTraining;
            if ((EditData.SafetyRequirements != null)) rdoSafetytraining.SelectedIndex = (((bool)EditData.SafetyRequirements) ? 0 : 1);
            if ((EditData.IsProvideEmployeeTrainingDocumentation != null)) rdoListEmployeeProgramDocumentation.SelectedIndex = (((bool)EditData.IsProvideEmployeeTrainingDocumentation) ? 0 : 1);
            if ((EditData.IsWrittenDrugAlcoholProgram != null)) rdoListDrugAlcohol.SelectedIndex = (((bool)EditData.IsWrittenDrugAlcoholProgram) ? 0 : 1);
            if ((EditData.IsPeriodicSafetyMeetings != null)) rdoListSafetyMeetings.SelectedIndex = (((bool)EditData.IsPeriodicSafetyMeetings) ? 0 : 1);
            if (EditData.SafetyMeetingsPeriods != null) chkSafetyMeetings.Items.Cast<ListItem>().ToList().ForEach(si => si.Selected = (EditData.SafetyMeetingsPeriods.Split('|').Contains(si.Text)));
            if (EditData.IsSafetyInspectionOfWIP != null) rdoListInspectionWIP.SelectedIndex = (((bool)EditData.IsSafetyInspectionOfWIP) ? 0 : 1);
            if (EditData.InspectionOfWIPPeriods != null) chkInspectionWIP.Items.Cast<ListItem>().ToList().ForEach(si => si.Selected = (EditData.InspectionOfWIPPeriods.Split('|').Contains(si.Text)));
            txtWhoInspectionWIP.Text = EditData.WhoConductsInspectionOfWIP;
            if (EditData.IsConductHazardAnalysis != null) rdoHazardAnalysis.SelectedIndex = (((bool)EditData.IsConductHazardAnalysis) ? 0 : 1);
            if (EditData.ConductHazardAnalysisPeriods != null) chkHazardAnalysisPeriods.Items.Cast<ListItem>().ToList().ForEach(si => si.Selected = (EditData.ConductHazardAnalysisPeriods.Split('|').Contains(si.Text)));

            #region Citations

            lblCitationYear1.Text = "Any citations in " + CitationYear1 + "?";
            citation1 = new clsSafety().SelectSafetyCitation(EditData.PK_SafetyID, CitationYear1);
            if (citation1.IsCitation != null) rdoCitationYear1.SelectedIndex = ((bool)citation1.IsCitation) ? 0 : 1;
            hdnCitationFile1.Value = citation1.CitationFilePath;
            lnkAttachCITATION1.Text = (!string.IsNullOrEmpty(citation1.CitationFilePath)) ?
                                     "(Edit " + citation1.CitationFilePath + " )" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick1.Attributes["style"] = (rdoCitationYear1.SelectedIndex == 0) ? "display: " : "display: none";

            lblCitationYear2.Text = "Any citations in " + CitationYear2 + "?";
            citation2 = new clsSafety().SelectSafetyCitation(EditData.PK_SafetyID, CitationYear2);
            if (citation2.IsCitation != null) rdoCitationYear2.SelectedIndex = ((bool)citation2.IsCitation) ? 0 : 1;
            hdnCitationFile2.Value = citation2.CitationFilePath;
            lnkAttachCITATION2.Text = (!string.IsNullOrEmpty(citation2.CitationFilePath)) ?
                                     lnkAttachCITATION2.Text = "(Edit " + citation2.CitationFilePath + " )" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick2.Attributes["style"] = (rdoCitationYear2.SelectedIndex == 0) ? "display: " : "display: none";

            lblCitationYear3.Text = "Any citations in " + CitationYear3 + "?";
            citation3 = new clsSafety().SelectSafetyCitation(EditData.PK_SafetyID, CitationYear3);
            if (citation3.IsCitation != null) rdoCitationYear3.SelectedIndex = ((bool)citation3.IsCitation) ? 0 : 1;
            hdnCitationFile3.Value = citation3.CitationFilePath;
            lnkAttachCITATION3.Text = (!string.IsNullOrEmpty(citation3.CitationFilePath)) ?
                                     lnkAttachCITATION3.Text = "(Edit " + citation3.CitationFilePath + " )" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick3.Attributes["style"] = (rdoCitationYear3.SelectedIndex == 0) ? "display: " : "display: none";

            #endregion

            //010 - END

            objRates = objSafety.SelectSafetyEMRRates(SafetyId, txtYearOne.Text.Trim());
            if (objRates != null)
            {
                txtRateOne.Text = objRates.EMRRate;
                chkNA1.Checked = Convert.ToBoolean(objRates.NotAvailable);
                txtExplanation1.Text = objRates.Explanation;
                //008
                if (!string.IsNullOrEmpty(objRates.FilePath))
                {
                    imbAttachEMR1.Visible = true;
                    lnkAttachEMR1.Visible = false;
                }
                else
                {
                    imbAttachEMR1.Visible = false;
                    lnkAttachEMR1.Visible = true;
                } 


            }
            else
            {
                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) )
                //{
                chkNA1.Enabled = true;
                txtRateOne.Enabled = true;
                txtExplanation1.Enabled = true;
                trAttachEMR1.Attributes["Style"] = @"display: ";   //008
                //}
                //else
                //{
                //    txtRateOne.Text = string.Empty;
                //}
            }
            // }
            objRates1 = objSafety.SelectSafetyEMRRates(SafetyId, txtYearTwo.Text.Trim());
            if (objRates1 != null)
            {
                txtRateTwo.Text = objRates1.EMRRate;
                chkNA2.Checked = Convert.ToBoolean(objRates1.NotAvailable);
                txtExplanation2.Text = objRates1.Explanation;
                //008
                if (!string.IsNullOrEmpty(objRates1.FilePath))
                {
                    imbAttachEMR2.Visible = true;
                    lnkAttachEMR2.Visible = false;
                }
                else
                {
                    imbAttachEMR2.Visible = false;
                    lnkAttachEMR2.Visible = true;
                } 

            }
            else
            {
                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0))
                //{
                txtRateTwo.Enabled = true;
                chkNA2.Enabled = true;
                txtExplanation2.Enabled = true;

                //}
                //else
                //{
                //    txtRateTwo.Text = string.Empty;
                //}
            }
            objRates2 = objSafety.SelectSafetyEMRRates(SafetyId, txtYearThree.Text.Trim());
            if (objRates2 != null)
            {
                txtRateThree.Text = objRates2.EMRRate;
                chkNA3.Checked = Convert.ToBoolean(objRates2.NotAvailable);
                txtExplanation3.Text = objRates2.Explanation;
                //008
                if (!string.IsNullOrEmpty(objRates2.FilePath))
                {
                    imbAttachEMR3.Visible = true;
                    lnkAttachEMR3.Visible = false;
                }
                else
                {
                    imbAttachEMR3.Visible = false;
                    lnkAttachEMR3.Visible = true;
                } 

            }
            else
            {
                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0))
                //{
                chkNA3.Enabled = true;
                txtRateThree.Enabled = true;
                txtExplanation3.Enabled = true;
                //}
                //else
                //{
                //    txtRateThree.Text = string.Empty;
                //}
            }

            //002
            #region EMR Rate 4
            VMSDAL.VQFSafetyEMRRate objRates4 = objSafety.SelectSafetyEMRRates(SafetyId, txtYearFour.Text.Trim());
            if (objRates4 != null)
            {
                txtRateFour.Text = objRates4.EMRRate;
                chkEMRNA4.Checked = Convert.ToBoolean(objRates4.NotAvailable);
                txtExplanation4.Text = objRates4.Explanation;

                //008
                if (!string.IsNullOrEmpty(objRates4.FilePath))
                {
                    imbAttachEMR4.Visible = true;
                    lnkAttachEMR4.Visible = false;
                }
                else
                {
                    imbAttachEMR4.Visible = false;
                    lnkAttachEMR4.Visible = true;
                }

            }
            #endregion


            if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) == 0))
            {
                if (chkNA1.Checked)
                {
                    trExplain1.Style["Display"] = "";
                    txtRateOne.Enabled = false;
                }
                else
                {
                    trExplain1.Style["Display"] = "None";
                    txtRateOne.Enabled = true;
                }

                if (chkNA2.Checked)
                {
                    trExplain2.Style["Display"] = "";
                    txtRateTwo.Enabled = false;
                }
                else
                {
                    trExplain2.Style["Display"] = "None";
                    txtRateTwo.Enabled = true;
                }

                if (chkNA3.Checked)
                {
                    trExplain3.Style["Display"] = "";
                    txtRateThree.Enabled = false;
                }
                else
                {
                    trExplain3.Style["Display"] = "None";
                    txtRateThree.Enabled = true;
                }

                //002
                if (chkEMRNA4.Checked)
                {
                    trExplainEMR4.Style["Display"] = "";
                    txtRateFour.Enabled = false;
                }
                else
                {
                    trExplainEMR4.Style["Display"] = "None";
                    txtRateFour.Enabled = true;
                }
            }
            objOsha1 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearOne.Text.Trim());
            if (objOsha1 != null)
            {
                txtNoofCasesFrmWorkOne.Text = objOsha1.TotalNoDays;
                txtNoofCasesTransferOne.Text = objOsha1.TotalNoJobTransfer;
                txtNoofDeathsYearOne.Text = objOsha1.TotalFatalities;
                txtRecordableCasesOne.Text = objOsha1.TotalNoOtherRecord;
                txtTotalHours1.Text = objOsha1.TotalHoursWorked;
                chkNA4.Checked = Convert.ToBoolean(objOsha1.NotAvailable);
                txtExplain4.Text = objOsha1.Explanation;
                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (EditData.EditStatus == 1))
                //{
                //    txtNoofCasesFrmWorkOne.Enabled = false;
                //    txtNoofCasesTransferOne.Enabled = false;
                //    txtNoofDeathsYearOne.Enabled = false;
                //    txtRecordableCasesOne.Enabled = false;
                //    txtTotalHours1.Enabled = false;
                //    chkNA4.Enabled = true;
                //    txtExplain4.Enabled = true;
                //}
                //if ((Convert.ToInt32(objSafety.GetSafetyStatusCount(VendorId)) == 0) && ((hdnVendorStatus.Value != "3") && (hdnVendorStatus.Value != "2")))
                //{
                //    txtNoofCasesFrmWorkOne.Enabled = true;
                //    txtNoofCasesTransferOne.Enabled = true;
                //    txtNoofDeathsYearOne.Enabled = true;
                //    txtRecordableCasesOne.Enabled = true;
                //    txtTotalHours1.Enabled = true;
                //    chkNA4.Enabled = true;
                //    txtExplain4.Enabled = true;
                //}

                //008
                if (!string.IsNullOrEmpty(objOsha1.FilePath))
                {
                    imbAttachOSHA1.Visible = true;
                    lnkAttachOSHA1.Visible = false;
                    starAttachOSHA1.Visible = false;
                }
                else
                {
                    imbAttachOSHA1.Visible = false;
                    lnkAttachOSHA1.Visible = true;
                    starAttachOSHA1.Visible = true;
                }

                //008
                chkNoAttachment1.Checked = !(bool)objOsha1.HasOSHALog;
            }
            else
            {
                txtNoofCasesFrmWorkOne.Text = string.Empty;
                txtNoofCasesTransferOne.Text = string.Empty;
                txtNoofDeathsYearOne.Text = string.Empty;
                txtRecordableCasesOne.Text = string.Empty;
                txtTotalHours1.Text = string.Empty;
                chkNA4.Checked = false;
                txtExplain4.Text = string.Empty;
                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                //    txtNoofCasesFrmWorkOne.Enabled = true;
                //    txtNoofCasesTransferOne.Enabled = true;
                //    txtNoofDeathsYearOne.Enabled = true;
                //    txtRecordableCasesOne.Enabled = true;
                //    txtTotalHours1.Enabled = true;
                //    chkNA4.Enabled = true;
                //    txtExplain4.Enabled = true;
                //}
            }
            objOsha2 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearTwo.Text.Trim());
            if (objOsha2 != null)
            {
                txtNoofCasesFrmWorkTwo.Text = objOsha2.TotalNoDays;
                txtNoofCasesTransferTwo.Text = objOsha2.TotalNoJobTransfer;
                txtNoofDeathsYearTwo.Text = objOsha2.TotalFatalities;
                txtRecordableCasesTwo.Text = objOsha2.TotalNoOtherRecord;
                txtTotalHours2.Text = objOsha2.TotalHoursWorked;
                chkNA5.Checked = Convert.ToBoolean(objOsha2.NotAvailable);
                txtExplain5.Text = objOsha2.Explanation;

                //008
                if (!string.IsNullOrEmpty(objOsha2.FilePath))
                {
                    imbAttachOSHA2.Visible = true;
                    lnkAttachOSHA2.Visible = false;
                    starAttachOSHA2.Visible = false;
                }
                else
                {
                    imbAttachOSHA2.Visible = false;
                    lnkAttachOSHA2.Visible = true;
                    starAttachOSHA2.Visible = true;
                }

                //008
                chkNoAttachment2.Checked = !(bool)objOsha2.HasOSHALog;

                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                //    txtNoofCasesFrmWorkTwo.Enabled = false;
                //    txtNoofCasesTransferTwo.Enabled = false;
                //    txtNoofDeathsYearTwo.Enabled = false;
                //    txtRecordableCasesTwo.Enabled = false;
                //    txtTotalHours2.Enabled = false;
                //    chkNA5.Enabled = false;
                //    txtExplain5.Enabled = false;
                //}
            }
            else
            {
                txtNoofCasesFrmWorkTwo.Text = string.Empty;
                txtNoofCasesTransferTwo.Text = string.Empty;
                txtNoofDeathsYearTwo.Text = string.Empty;
                txtRecordableCasesTwo.Text = string.Empty;
                txtTotalHours2.Text = string.Empty;
                chkNA5.Checked = false;
                txtExplain5.Text = string.Empty;
                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                //    txtNoofCasesFrmWorkTwo.Enabled = true;
                //    txtNoofCasesTransferTwo.Enabled = true;
                //    txtNoofDeathsYearTwo.Enabled = true;
                //    txtRecordableCasesTwo.Enabled = true;
                //    txtTotalHours2.Enabled = true;
                //    chkNA5.Enabled = true;
                //    txtExplain5.Enabled = true;
                //}

            }
            objOsha3 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearThree.Text.Trim());
            if (objOsha3 != null)
            {
                txtNoofCasesFrmWorkThree.Text = objOsha3.TotalNoDays;
                txtNoofCasesTransferThree.Text = objOsha3.TotalNoJobTransfer;
                txtNoofDeathsYearThree.Text = objOsha3.TotalFatalities;
                txtRecordableCasesThree.Text = objOsha3.TotalNoOtherRecord;
                txtTotalHours3.Text = objOsha3.TotalHoursWorked;
                chkNA6.Checked = Convert.ToBoolean(objOsha3.NotAvailable);
                txtExplain6.Text = objOsha3.Explanation;
                //008
                if (!string.IsNullOrEmpty(objOsha3.FilePath))
                {
                    imbAttachOSHA3.Visible = true;
                    lnkAttachOSHA3.Visible = false;
                    starAttachOSHA3.Visible = false;
                }
                else
                {
                    imbAttachOSHA3.Visible = false;
                    lnkAttachOSHA3.Visible = true;
                    starAttachOSHA3.Visible = true;
                }

                //008
                chkNoAttachment3.Checked = !(bool)objOsha3.HasOSHALog;
                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                //    txtNoofCasesFrmWorkThree.Enabled = false;
                //    txtNoofCasesTransferThree.Enabled = false;
                //    txtNoofDeathsYearThree.Enabled = false;
                //    txtRecordableCasesThree.Enabled = false;
                //    txtTotalHours3.Enabled = false;
                //    chkNA6.Enabled = false;
                //    txtExplain6.Enabled = false;
                //}
            }
            else
            {
                txtNoofCasesFrmWorkThree.Text = string.Empty;
                txtNoofCasesTransferThree.Text = string.Empty;
                txtNoofDeathsYearThree.Text = string.Empty;
                txtRecordableCasesThree.Text = string.Empty;
                txtTotalHours3.Text = string.Empty;
                chkNA6.Checked = false;
                txtExplain6.Text = string.Empty;
                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                //    txtNoofCasesFrmWorkThree.Enabled = true;
                //    txtNoofCasesTransferThree.Enabled = true;
                //    txtNoofDeathsYearThree.Enabled = true;
                //    txtRecordableCasesThree.Enabled = true;
                //    txtTotalHours3.Enabled = true;
                //    chkNA6.Enabled = true;
                //    txtExplain6.Enabled = true;
                //}
            }

            //007
            #region OSHA Year 4
            VMSDAL.VQFSafetyOSHA objOsha4 = objSafety.SelectSafetyOsha(SafetyId, txtOSHAYearFour.Text.Trim());
            if (objOsha4 != null)
            {
                txtNoofCasesFrmWorkFour.Text = objOsha4.TotalNoDays;
                txtNoofCasesTransferFour.Text = objOsha4.TotalNoJobTransfer;
                txtNoofDeathsYearFour.Text = objOsha4.TotalFatalities;
                txtRecordableCasesFour.Text = objOsha4.TotalNoOtherRecord;
                txtTotalHours4.Text = objOsha4.TotalHoursWorked;
                chkNA7.Checked = Convert.ToBoolean(objOsha4.NotAvailable);
                txtExplain7.Text = objOsha4.Explanation;

                txtNoofCasesFrmWorkFour.Enabled = true;
                txtNoofCasesTransferFour.Enabled = true;
                txtNoofDeathsYearFour.Enabled = true;
                txtRecordableCasesFour.Enabled = true;
                txtTotalHours4.Enabled = true;
                chkNA7.Enabled = true;
                txtExplain7.Enabled = true;

                //008
                if (!string.IsNullOrEmpty(objOsha4.FilePath))
                {
                    imbAttachOSHA4.Visible = true;
                    lnkAttachOSHA4.Visible = false;
                    starAttachOSHA4.Visible = false;
                }
                else
                {
                    imbAttachOSHA4.Visible = false;
                    lnkAttachOSHA4.Visible = true;
                    starAttachOSHA4.Visible = true;
                }

                //008
                chkNoAttachment4.Checked = !(bool)objOsha4.HasOSHALog;

            }
            else
            {
                txtNoofCasesFrmWorkFour.Text = string.Empty;
                txtNoofCasesTransferFour.Text = string.Empty;
                txtNoofDeathsYearFour.Text = string.Empty;
                txtRecordableCasesFour.Text = string.Empty;
                txtTotalHours4.Text = string.Empty;
                chkNA7.Checked = false;
                txtExplain7.Text = string.Empty;

                txtNoofCasesFrmWorkFour.Enabled = true;
                txtNoofCasesTransferFour.Enabled = true;
                txtNoofDeathsYearFour.Enabled = true;
                txtRecordableCasesFour.Enabled = true;
                txtTotalHours4.Enabled = true;
                chkNA7.Enabled = true;
                txtExplain7.Enabled = true;

                //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
                //{
                //    txtNoofCasesFrmWorkFour.Enabled = true;
                //    txtNoofCasesTransferThree.Enabled = true;
                //    txtNoofDeathsYearThree.Enabled = true;
                //    txtRecordableCasesThree.Enabled = true;
                //    txtTotalHours3.Enabled = true;
                //    chkNA6.Enabled = true;
                //    txtExplain6.Enabled = true;
                //}
            }
            #endregion

            //008 - has osha log checkboxes...aaarrrggghh
            starAttachOSHA1.Visible = !chkNoAttachment1.Checked;
            lnkAttachOSHA1.CssClass = (!chkNoAttachment1.Checked) ? "lnkAttach" : "lnk-disabled";
            lnkAttachOSHA1.Enabled = !chkNoAttachment1.Checked;

            starAttachOSHA2.Visible = !chkNoAttachment2.Checked;
            lnkAttachOSHA2.Enabled = !chkNoAttachment2.Checked;
            lnkAttachOSHA2.CssClass = (!chkNoAttachment2.Checked) ? "lnkAttach" : "lnk-disabled";

            starAttachOSHA3.Visible = !chkNoAttachment3.Checked;
            lnkAttachOSHA3.Enabled = !chkNoAttachment3.Checked;
            lnkAttachOSHA3.CssClass = (!chkNoAttachment3.Checked) ? "lnkAttach" : "lnk-disabled";

            starAttachOSHA4.Visible = !chkNoAttachment4.Checked;
            lnkAttachOSHA4.Enabled = !chkNoAttachment4.Checked;
            lnkAttachOSHA4.CssClass = (!chkNoAttachment4.Checked) ? "lnkAttach" : "lnk-disabled";

            //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) == 0))
            //{
            if (chkNA4.Checked)
            {
                trExplain4.Style["Display"] = "";
                txtNoofCasesFrmWorkOne.Enabled = false;
                txtNoofCasesTransferOne.Enabled = false;
                txtNoofDeathsYearOne.Enabled = false;
                txtRecordableCasesOne.Enabled = false;
                txtTotalHours1.Enabled = false;
                pnlOSHAYear4.Attributes["style"] = "display: ";     //007
            }
            else
            {
                trExplain4.Style["Display"] = "None";
                txtNoofCasesFrmWorkOne.Enabled = true;
                txtNoofCasesTransferOne.Enabled = true;
                txtNoofDeathsYearOne.Enabled = true;
                txtRecordableCasesOne.Enabled = true;
                txtTotalHours1.Enabled = true;
                pnlOSHAYear4.Attributes["style"] = "display: none";     //007
            }

            if (chkNA5.Checked)
            {
                trExplain5.Style["Display"] = "";
                txtNoofCasesFrmWorkTwo.Enabled = false;
                txtNoofCasesTransferTwo.Enabled = false;
                txtNoofDeathsYearTwo.Enabled = false;
                txtRecordableCasesTwo.Enabled = false;
                txtTotalHours2.Enabled = false;
            }
            else
            {
                trExplain5.Style["Display"] = "None";
                txtNoofCasesFrmWorkTwo.Enabled = true;
                txtNoofCasesTransferTwo.Enabled = true;
                txtNoofDeathsYearTwo.Enabled = true;
                txtRecordableCasesTwo.Enabled = true;
                txtTotalHours2.Enabled = true;
            }

            if (chkNA6.Checked)
            {
                trExplain6.Style["Display"] = "";
                txtNoofCasesFrmWorkThree.Enabled = false;
                txtNoofCasesTransferThree.Enabled = false;
                txtNoofDeathsYearThree.Enabled = false;
                txtRecordableCasesThree.Enabled = false;
                txtTotalHours3.Enabled = false;
            }
            else
            {
                trExplain6.Style["Display"] = "None";
                txtNoofCasesFrmWorkThree.Enabled = true;
                txtNoofCasesTransferThree.Enabled = true;
                txtNoofDeathsYearThree.Enabled = true;
                txtRecordableCasesThree.Enabled = true;
                txtTotalHours3.Enabled = true;
            }
            //007
            if (chkNA7.Checked)
            {
                trExplain7.Style["Display"] = "";
                txtNoofCasesFrmWorkFour.Enabled = false;
                txtNoofCasesTransferFour.Enabled = false;
                txtNoofDeathsYearFour.Enabled = false;
                txtRecordableCasesFour.Enabled = false;
                txtTotalHours4.Enabled = false;
            }
            else
            {
                trExplain7.Style["Display"] = "None";
                txtNoofCasesFrmWorkFour.Enabled = true;
                txtNoofCasesTransferFour.Enabled = true;
                txtNoofDeathsYearFour.Enabled = true;
                txtRecordableCasesFour.Enabled = true;
                txtTotalHours4.Enabled = true;
            }

            // }
            string SafetyProgram = Convert.ToString(EditData.SafetyProgram);

            chkSICNA.Checked = EditData.SICNA == true ? true : false;

            if (chkSICNA.Checked)
            {
                trSICExplain.Style["display"] = "";
                txtSICExplain.Text = EditData.Explanation;
            }
            else
            {
                trSICExplain.Style["display"] = "None";
                txtSIC.Text = EditData.SICNAICSCode;
            }
            if (SafetyProgram == "False")
            {
                rdoSafetyProgram.SelectedIndex = 1;
            }
            else if (SafetyProgram == "True")
            {
                rdoSafetyProgram.SelectedIndex = 0;
            }

            string SafetyRequirement = Convert.ToString(EditData.SafetyRequirements);
            if (SafetyRequirement == "False")
            {
                rdoSafetytraining.SelectedIndex = 1;
            }
            else if (SafetyRequirement == "True")
            {
                rdoSafetytraining.SelectedIndex = 0;
            }

            string CompanySafety = Convert.ToString(EditData.CompanySafety);
            if (CompanySafety == "False")
            {
                rdoSafetyDirector.SelectedIndex = 1;
            }
            else if (CompanySafety == "True")
            {
                rdoSafetyDirector.SelectedIndex = 0;
            }
            else
            {
                rdoSafetyDirector.SelectedIndex = -1;
            }
            txtContactName.Text = EditData.CompanySafetyContactName;
            objSafety.VentID = VendorId;

            chkContactUSA.Checked = !(bool)EditData.CompanyContactIsBasedInUS;//006
            SwitchPhoneForKeyContact(chkContactUSA);

            chkReperesentUSA.Checked = !(bool)EditData.RepresentContactIsBasedInUS;//006
            SwitchPhoneForKeyContact(chkReperesentUSA);

            string ContactPhoneNo = EditData.CompanySafetyContactNumber;

            objCommon.loadphone(txtMainPhno1, txtMainPhno2, txtMainPhno3, ContactPhoneNo);


            txtRepresentationContactName.Text = EditData.SafetyRepresentationContactName; //005
            objCommon.loadphone(txtRepresentationContactNumberOne, txtRepresentationContactNumberTwo, txtRepresentationContactNumberThree, EditData.SafetyRepresentationContactNumber); //005

            //005
            string SafetyRepresentation = Convert.ToString(EditData.SafetyRepresentation);
            if (SafetyRepresentation == "False")
            {
                rdoRepresentation.SelectedIndex = 1;
            }
            else if (SafetyRepresentation == "True")
            {
                rdoRepresentation.SelectedIndex = 0;
            }
            else
            {
                rdoRepresentation.SelectedIndex = -1;
            }

            txtRepresentationContactName.Text = EditData.SafetyRepresentationContactName;      //005
            objCommon.loadphone(txtRepresentationContactNumberOne, txtRepresentationContactNumberTwo, txtRepresentationContactNumberThree, EditData.SafetyRepresentationContactNumber);      //005


            if (EditData.EditStatus == 1)
            {
                //if (!((string.IsNullOrEmpty(txtRateOne.Text.Trim())) && (string.IsNullOrEmpty(txtExplanation1.Text.Trim()))))
                //{
                //    chkNA1.Enabled = false;
                //    txtExplanation1.ReadOnly = true;
                //    chkNA2.Enabled = false;
                //    txtExplanation2.ReadOnly = true;
                //    chkNA3.Enabled = false;
                //    txtExplanation3.ReadOnly = true;
                //}
                //else
                //{
                //    chkNA1.Enabled = true;
                //    txtExplanation1.ReadOnly = false;
                //    chkNA2.Enabled = false;
                //    txtExplanation2.ReadOnly = true;
                //    chkNA3.Enabled = false;
                //    txtExplanation3.ReadOnly = true;
                //}

                foreach (Control ctrl in pnlEmr.Controls)
                {
                    if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.Text.Trim()Box"))
                    {

                        TextBox txt = (TextBox)ctrl;
                        if (!string.IsNullOrEmpty(txt.Text.Trim()))
                        {
                            txt.ReadOnly = true;

                        }
                        else
                        {
                            txt.ReadOnly = false;
                        }
                    }
                    if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
                    {
                        LinkButton lnk = (LinkButton)ctrl;
                        lnk.Visible = false;
                    }
                }
                foreach (Control ctrl in pnlOsha.Controls)
                {

                    //if (!((string.IsNullOrEmpty(txtExplain4.Text.Trim())) && (string.IsNullOrEmpty(txtNoofDeathsYearOne.Text.Trim())) && (string.IsNullOrEmpty(txtNoofCasesFrmWorkOne.Text.Trim())) && (string.IsNullOrEmpty(txtNoofCasesTransferOne.Text.Trim())) && (string.IsNullOrEmpty(txtRecordableCasesOne.Text.Trim()))))
                    //{
                    //    chkNA4.Enabled = false;
                    //    txtExplain4.ReadOnly = true;
                    //    chkNA5.Enabled = false;
                    //    txtExplain5.ReadOnly = true;
                    //    chkNA6.Enabled = false;
                    //    txtExplain6.ReadOnly = true;
                    //}
                    //else
                    //{
                    //    chkNA4.Enabled = true;
                    //    txtExplain4.ReadOnly = false;
                    //    chkNA5.Enabled = false;
                    //    txtExplain5.ReadOnly = true;
                    //    chkNA6.Enabled = false;
                    //    txtExplain6.ReadOnly = true;
                    //}
                    if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.Text.Trim()Box"))
                    {
                        TextBox txt = (TextBox)ctrl;
                        if (!string.IsNullOrEmpty(txt.Text.Trim()))
                        {
                            txt.ReadOnly = true;

                        }
                        else
                        {
                            txt.ReadOnly = false;
                        }
                    }
                    if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
                    {
                        LinkButton lnk = (LinkButton)ctrl;
                        lnk.Visible = false;
                    }
                }
            }
        }
        else
        {
            #region Citations
            //010
            citation1 = new clsSafety().SelectSafetyCitation(00, CitationYear1);
            lblCitationYear1.Text = "Any citations in " + CitationYear1 + "?";
            lnkAttachCITATION1.Text = (rdoCitationYear1.SelectedIndex != 0) ?
                                     "" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick1.Attributes["style"] = (rdoCitationYear1.SelectedIndex == 0) ? "display: " : "display: none";

            citation2 = new clsSafety().SelectSafetyCitation(00, CitationYear2);
            lblCitationYear2.Text = "Any citations in " + CitationYear2 + "?";
            lnkAttachCITATION2.Text = (rdoCitationYear2.SelectedIndex != 0) ?
                                     "" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick2.Attributes["style"] = (rdoCitationYear2.SelectedIndex == 0) ? "display: " : "display: none";

            citation3 = new clsSafety().SelectSafetyCitation(00, CitationYear3);
            lblCitationYear3.Text = "Any citations in " + CitationYear3 + "?";
            lnkAttachCITATION3.Text = (rdoCitationYear3.SelectedIndex != 0) ?
                                     "" :
                                     "(Attach description of circumstances and any corrective/remedial actions taken to prevent recurrence)";
            tdAttachClick3.Attributes["style"] = (rdoCitationYear3.SelectedIndex == 0) ? "display: " : "display: none";

            #endregion
        }
    }

    //008 - added
    private void LoadSafetyAttachmentControl(string buttonID = "")
    {
        if (string.IsNullOrEmpty(hdnSafetyId.Value))
        {
            this.imbSave_Click(imbSave, null);
        }

        switch (buttonID.ToUpper())
        {
            case "EMR1":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.EMR = new clsSafety().SelectSafetyEMRRates(Convert.ToInt64(hdnSafetyId.Value), txtYearOne.Text);
                uclAttachEMR1.SafetySection = SafetySection.EMR;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtYearOne.Text + " EMR Letter";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "EMR2":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.EMR = new clsSafety().SelectSafetyEMRRates(Convert.ToInt64(hdnSafetyId.Value), txtYearTwo.Text);
                uclAttachEMR1.SafetySection = SafetySection.EMR;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtYearTwo.Text + " EMR Letter";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "EMR3":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.EMR = new clsSafety().SelectSafetyEMRRates(Convert.ToInt64(hdnSafetyId.Value), txtYearThree.Text);
                uclAttachEMR1.SafetySection = SafetySection.EMR;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtYearThree.Text + " EMR Letter";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "EMR4":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.EMR = new clsSafety().SelectSafetyEMRRates(Convert.ToInt64(hdnSafetyId.Value), txtYearFour.Text);
                uclAttachEMR1.SafetySection = SafetySection.EMR;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtYearFour.Text + " EMR Letter";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "OSHA1":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.OSHA = new clsSafety().SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearOne.Text);
                uclAttachEMR1.SafetySection = SafetySection.OSHA;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtOSHAYearOne.Text + " OSHA Attachment";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "OSHA2":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.OSHA = new clsSafety().SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearTwo.Text);
                uclAttachEMR1.SafetySection = SafetySection.OSHA;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtOSHAYearTwo.Text + " OSHA Attachment";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "OSHA3":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.OSHA = new clsSafety().SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearThree.Text);
                uclAttachEMR1.SafetySection = SafetySection.OSHA;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtOSHAYearThree.Text + " OSHA Attachment";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "OSHA4":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.OSHA = new clsSafety().SelectSafetyOsha(Convert.ToInt64(hdnSafetyId.Value), txtOSHAYearFour.Text);
                uclAttachEMR1.SafetySection = SafetySection.OSHA;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = txtOSHAYearFour.Text + " OSHA Attachment";
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            //010 - START
            case "CITATION1":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.CITATION = new clsSafety().SelectSafetyCitation(Convert.ToInt64(hdnSafetyId.Value), CitationYear1);
                uclAttachEMR1.SafetySection = SafetySection.CITATIONS;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = "Citation Document";
                hdnCitationFile1.Value = uclAttachEMR1.CITATION.CitationFilePath;
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "CITATION2":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.CITATION = new clsSafety().SelectSafetyCitation(Convert.ToInt64(hdnSafetyId.Value), CitationYear2);
                uclAttachEMR1.SafetySection = SafetySection.CITATIONS;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = "Citation Document";
                hdnCitationFile2.Value = uclAttachEMR1.CITATION.CitationFilePath;
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            case "CITATION3":
                uclAttachEMR1.Dispose();
                uclAttachEMR1.CITATION = new clsSafety().SelectSafetyCitation(Convert.ToInt64(hdnSafetyId.Value), CitationYear3);
                uclAttachEMR1.SafetySection = SafetySection.CITATIONS;
                (uclAttachEMR1.FindControl("hdnTitle") as HiddenField).Value = "Citation Document";
                hdnCitationFile3.Value = uclAttachEMR1.CITATION.CitationFilePath;
                uclAttachEMR1.DataBind();
                uclAttachEMR1.InitiateData();
                break;
            //010 - END
            default:
                break;
        }

    }


    protected void cusvYearOne_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA1.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplanation1.Text.Trim()))
            {
                cusvYearOne.ErrorMessage = "Please enter the explanation under year one";
                args.IsValid = true;
            }

            else
            {
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtRateOne.Text.Trim()))
            {
                cusvYearOne.ErrorMessage = "Please enter rate under year one";

                args.IsValid = true;
            }

            else
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvYearTwo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA2.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplanation2.Text.Trim()))
            {
                cusvYearTwo.ErrorMessage = "Please enter the explanation under year two";
                args.IsValid = true;
            }

            else
            {
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtRateTwo.Text.Trim()))
            {
                cusvYearTwo.ErrorMessage = "Please enter rate under year two";

                args.IsValid = true;
            }

            else
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvYearThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA3.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplanation3.Text.Trim()))
            {
                cusvYearThree.ErrorMessage = "Please enter the explanation under year three";
                args.IsValid = true;
            }

            else
            {
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtRateThree.Text.Trim()))
            {
                cusvYearThree.ErrorMessage = "Please enter rate under year three";

                args.IsValid = true;
            }

            else
            {
                args.IsValid = true;
            }
        }
    }

    //002
    protected void cusvYearFour_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkEMRNA4.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplanation4.Text.Trim()))
            {
                cusvYearFour.ErrorMessage = "Please enter the explanation under year Four";
                args.IsValid = false;
            }

            else
            {
                args.IsValid = true;
            }
        }
        else
        {
            // only required if year 1 is not available
            if (string.IsNullOrEmpty(txtRateFour.Text.Trim()) && chkNA1.Checked)
            {
                cusvYearFour.ErrorMessage = "Please enter rate under year four";

                args.IsValid = false;
            }
            else if (string.IsNullOrEmpty(txtRateFour.Text.Trim()))
            {
                txtRateFour.Text = "";
                args.IsValid = true;
            }
            else
            {
                args.IsValid = true;
            }
        }
    }



    protected void cusvOSHAYearOne_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA4.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplain4.Text.Trim()))
            {
                cusvOSHAYearOne.ErrorMessage = "Please enter the explanation under OSHA year one";
                args.IsValid = true;
            }

            else
            {
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtNoofDeathsYearOne.Text.Trim()))
            {
                cusvOSHAYearOne.ErrorMessage = "Please enter total number of fatalities under OSHA year one";
                args.IsValid = true;
            }
            else { args.IsValid = true; }



        }
    }

    protected void cusvOSHAYearTwo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA5.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplain5.Text.Trim()))
            {
                cusvOSHAYearTwo.ErrorMessage = "Please enter the explanation under OSHA year two";
                args.IsValid = true;
            }

            else
            {
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtNoofDeathsYearTwo.Text.Trim()))
            {
                cusvOSHAYearTwo.ErrorMessage = "Please enter total number of fatalities under OSHA year two";
                args.IsValid = true;
            }
            else { args.IsValid = true; }



        }
    }

    protected void cusvOSHAYearThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA6.Checked == true)
        {
            if (string.IsNullOrEmpty(txtExplain6.Text.Trim()))
            {
                cusvOSHAYearThree.ErrorMessage = "Please enter the explanation under OSHA year three";
                args.IsValid = true;
            }

            else
            {
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtNoofDeathsYearThree.Text.Trim()))
            {
                cusvOSHAYearThree.ErrorMessage = "Please enter total number of fatalities under OSHA year three";
                args.IsValid = true;
            }
            else { args.IsValid = true; }



        }
    }

    protected void cusvOSHARecord1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA4.Checked == false)
        {
            if (string.IsNullOrEmpty(txtRecordableCasesOne.Text.Trim()))
            {
                cusvOSHARecord1.ErrorMessage = "Please enter total number of other recordable cases under OSHA year one";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHATransfer1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA4.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesTransferOne.Text.Trim()))
            {
                cusvOSHATransfer1.ErrorMessage = "Please enter total number of cases with job transfer or restriction under OSHA year one";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHAWork1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA4.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesFrmWorkOne.Text.Trim()))
            {
                cusvOSHAWork1.ErrorMessage = "Please enter total number of cases with days away from work under OSHA year one";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }
    protected void cusvOSHARecord2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA5.Checked == false)
        {
            if (string.IsNullOrEmpty(txtRecordableCasesTwo.Text.Trim()))
            {
                cusvOSHARecord2.ErrorMessage = "Please enter total number of other recordable cases under OSHA year two";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHATransfer2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA5.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesTransferTwo.Text.Trim()))
            {
                cusvOSHATransfer2.ErrorMessage = "Please enter total number of cases with job transfer or restriction under OSHA year two";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHAWork2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA5.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesFrmWorkTwo.Text.Trim()))
            {
                cusvOSHAWork2.ErrorMessage = "Please enter total number of cases with days away from work under OSHA year two";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }


    protected void cusvOSHARecord3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA6.Checked == false)
        {
            if (string.IsNullOrEmpty(txtRecordableCasesThree.Text.Trim()))
            {
                cusvOSHARecord3.ErrorMessage = "Please enter total number of other recordable cases under OSHA year three";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHATransfer3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA6.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesTransferThree.Text.Trim()))
            {
                cusvOSHATransfer3.ErrorMessage = "Please enter total number of cases with job transfer or restriction under OSHA year three";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }

    protected void cusvOSHAWork3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA6.Checked == false)
        {
            if (string.IsNullOrEmpty(txtNoofCasesFrmWorkThree.Text.Trim()))
            {
                cusvOSHAWork3.ErrorMessage = "Please enter total number of cases with days away from work under OSHA year three";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }
    protected void cusvTotalHours1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA4.Checked == false)
        {
            if (string.IsNullOrEmpty(txtTotalHours1.Text.Trim()))
            {
                cusvTotalHours1.ErrorMessage = "Please enter total hours worked by all employees under OSHA year one";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }
    protected void cusvTotalHours2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA5.Checked == false)
        {
            if (string.IsNullOrEmpty(txtTotalHours2.Text.Trim()))
            {
                cusvTotalHours2.ErrorMessage = "Please enter total hours worked by all employees under OSHA year two";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }
    protected void cusvTotalHours3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA6.Checked == false)
        {
            if (string.IsNullOrEmpty(txtTotalHours3.Text.Trim()))
            {
                cusvTotalHours3.ErrorMessage = "Please enter total hours worked by all employees under OSHA year three";
                args.IsValid = true;

            }
            else { args.IsValid = true; }

        }
    }
    protected void cusvSICNA_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNA4.Checked == false || chkNA5.Checked == false || chkNA6.Checked == false)
        {
            if (chkSICNA.Checked == false && string.IsNullOrEmpty(txtSIC.Text))
            {
                cusvSICNA.ErrorMessage = "Please enter SIC or NAICS Code";
                args.IsValid = true;
            }
            else if (chkSICNA.Checked && string.IsNullOrEmpty(txtSICExplain.Text))
            {
                cusvSICNA.ErrorMessage = "Please enter explanation for SIC or NAICS code";
                args.IsValid = true;
            }
            else { args.IsValid = true; }

        }
        else
        {
            if (chkSICNA.Checked && string.IsNullOrEmpty(txtSICExplain.Text))
            {
                cusvSICNA.ErrorMessage = "Please enter explanation for SIC or NAICS code";
                args.IsValid = true;
            }
            else { args.IsValid = true; }
        }
    }
    private static string Radio(string radio)
    {
        int radiocount;
        if (radio == "False")
        {
            radiocount = 1;
        }
        else if (radio == "True")
        {
            radiocount = 2;
        }
        else
        {
            radiocount = 3;
        }
        switch (radiocount)
        {
            case 1:
                return "No";
            case 2:
                return "Yes";
            case 3:
                return "No Value";
            default:
                return radio;

        }
    }

    private static string EmptyValue(string Empty)
    {
        int Emptycount = 0;
        if (Empty == "")
        {
            Emptycount = 1;
        }
        else if (Empty == "1/1/1753 12:00:00 AM")
        {
            Emptycount = 1;
        }
        switch (Emptycount)
        {
            case 1:
                return "No Value";
            default:
                return Empty;

        }
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(Session["ReturnQuery"].ToString());
    }
    protected void imbOkoutlook_Click(object sender, ImageClickEventArgs e)
    {
        modalExtnd.Hide();
        imbOkoutlook.Visible = false;
        imbOk.Style["display"] = "";
        //Response.Redirect(Session["ReturnQuery"].ToString());
        LoadEditDatas(true);
    }

    /// <summary>
    /// 008
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkAttach_Click(object sender, EventArgs e)
    {
        string id = (sender as LinkButton).ID.Replace("lnkAttach", "");
        LoadSafetyAttachmentControl(id);
        uclAttachEMR1.ChangeVQFSectionStatus = false;
        uclAttachEMR1.Open();


    }

    void uclAttachEMR1_CloseEvent(object sender, EventArgs e)
    {
        attachmentChanged = (!string.IsNullOrEmpty(uclAttachEMR1.AttachmentChanged));
        attachmentChangedText = uclAttachEMR1.AttachmentChanged;
        LoadEditDatas(true);
        //010 lets find out where was the box triggered and determine submenu to open
        var usercontrol = sender as UserControl_uclSafetyAttachmentBox;
        var linkfileName = usercontrol.FindControl("lnkFileName") as LinkButton;
        string subMenu = string.Empty;
        if (linkfileName.Text.Contains("EMR")) subMenu = "1";
        if (linkfileName.Text.Contains("OSHA")) subMenu = "2";
        if (linkfileName.Text.Contains("CITATION")) subMenu = "3";

        if (!Request.RawUrl.Contains("?submenu") && !string.IsNullOrEmpty(subMenu))
        {
            Response.Redirect(Request.RawUrl + "?submenu=" + subMenu);
        }
    }

    /// <summary>
    /// 008
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAttach_Click(object sender, EventArgs e)
    {
        string id = (sender as ImageButton).ID.Replace("imbAttach", "");
        LoadSafetyAttachmentControl(id);
        uclAttachEMR1.ChangeVQFSectionStatus = false;
        uclAttachEMR1.Open();

    }
    protected void chkNoAttachment1_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (sender as CheckBox);
        switch (chk.ID)
        {
            case "chkNoAttachment1":
                lnkAttachOSHA1.Enabled = !chk.Checked;
                lnkAttachOSHA1.CssClass = (!chk.Checked) ? "lnkAttach" : "lnk-disabled";
                starAttachOSHA1.Visible = !chk.Checked;
                break;
            case "chkNoAttachment2":
                lnkAttachOSHA2.Enabled = !chk.Checked;
                lnkAttachOSHA2.CssClass = (!chk.Checked) ? "lnkAttach" : "lnk-disabled";
                starAttachOSHA2.Visible = !chk.Checked;
                break;
            case "chkNoAttachment3":
                lnkAttachOSHA3.Enabled = !chk.Checked;
                lnkAttachOSHA3.CssClass = (!chk.Checked) ? "lnkAttach" : "lnk-disabled";
                starAttachOSHA3.Visible = !chk.Checked;
                break;
            case "chkNoAttachment4":
                lnkAttachOSHA3.Enabled = !chk.Checked;
                lnkAttachOSHA3.CssClass = (!chk.Checked) ? "lnkAttach" : "lnk-disabled";
                starAttachOSHA3.Visible = !chk.Checked;
                break;
            default:
                break;
        }
    }
}
