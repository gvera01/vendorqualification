﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Text;
using VMSDAL;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 Sooraj Sudhakaran.T 09/28/2013: VMS Modification - Added code to handle International Zip/Postal Code
 * 003 Sooraj Sudhakaran.T 10/07/2013: VMS Modification - Add question for International Currency
 * 004 Sooraj Sudhakaran.T 10/09/2013: VMS Modification - Added code to handle International Country ,State or province
 * 005 G. Vera 06/10/2014: Year 4 if Year one is checked N/A
 * 006 G. Vera 07/08/2014: Remove all page validations from Admin VQF screens
 ****************************************************************************************************************/
#endregion

public partial class VQF_LegalFinancial : CommonPage
{

    #region Declaration

    Common objCommon = new Common();
    clsLegal objLegal = new clsLegal();
    clsCompany objCompany = new clsCompany();
    long VendorId;

    DataSet Attch = new DataSet();
    long LegalID;

    public DataSet Vendordetails = new DataSet();
    public DataSet LegalFinancialData;
    public DataSet LegalBankingData;
    public DataSet UpdateBankingData;
    public DataSet UpdateFinancialData;
    public DataSet objFinancial;
    public DataSet objFinancialUpdate;
    public int CountLegal;

    Byte LegalStatus;
    public DAL.VQFLegal UpdateLegalData;
    static bool IsBasedInUs;  //G. Vera 05/13/2014 need static to keep value

    public DAL.VQFLegal EditData;
    public DAL.VQFLegal EditLegalData;
    public DAL.VQFLegalFinancial objFinancial1;
    public DAL.VQFLegalFinancial objFinancial2;
    public DAL.VQFLegalFinancial objFinancial3;
    public DAL.VQFLegalFinancial objFinancial4;
    public DAL.VQFLegalFinancial objFinancial5;
    public DAL.VQFLegalFinancial objFinancial6;
    public DAL.VQFLegalFinancial objFinancial7;
    public DAL.VQFLegalFinancial objFinancial8;
    public DAL.VQFLegalFinancial objFinancial9;
    public DAL.VQFLegalFinancial objFinancial10;


    public const string LandF = "LEGAL AND FINANCIAL ";
    public const string Legal = "Legal";
    public const string Financial = "Financial";
    public const string Banking = "Banking";
    public const string Break = "</BR></BR>";
    StringBuilder CompareBody = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
    StringBuilder CompareMainBody = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
    string CompareData = string.Empty;
    string FinancialPart = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        CallScripts();
        ViewState["EditStatus"] = 0;
        //Check for the session of vendor Id if it is null redirects to another page
        if (string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])) || Convert.ToString(Session["VendorAdminId"]) == "0")
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
        }
        else
        {
            //001
            if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");
            }
            VendorId = Convert.ToInt64(Session["VendorAdminId"]);
            //VendorId = 1718;
            if (!IsPostBack)
            {
                //001
                Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");
                
                //VendorId = Convert.ToInt64(Session["VendorId"]);
                CountLegal = objLegal.GetLegalStatusCount(VendorId);


                Vendordetails = objCommon.GetVendorDetails(VendorId);
                lblwelcome.Text = Vendordetails.Tables[0].Rows[0]["Company"].ToString();
                IsBasedInUs = Vendordetails.Tables[0].Rows[0]["IsBasedInUS"].ConvertToString().ConvertToBool();
                HiddenField outlookContentVendorName = VQFEditSideMenu.FindControl("hdnOutllookVendorName") as HiddenField;
                outlookContentVendorName.Value = "Dear " + lblwelcome.Text.Replace("&", "AND") + ",%0A";
                HiddenField outlookContentCompanyName = VQFEditSideMenu.FindControl("hdnOutllookCompanyName") as HiddenField;
                outlookContentCompanyName.Value = lblwelcome.Text.Replace("&", "AND") + ",%0A";
                HiddenField outlookVendorEmail = VQFEditSideMenu.FindControl("hdnOutllookVendorEmail") as HiddenField;
                outlookVendorEmail.Value = Session["OUTLOOKTO"].ToString();

                hdnCount.Value = "0";
                hdnCountValue.Value = "0";
                ShowHideControls();
    
                FillCurrency();//003-Sooraj
                FillCountry();//004 -Sooraj
                FillState();//004 -Sooraj
                HiddenField outlookContent = VQFEditSideMenu.FindControl("hdnOutllook") as HiddenField;
                if (Session["OUTLOOKDATA"] != null)
                    outlookContent.Value = Session["OUTLOOKDATA"].ToString();
            }
        }
        if (!IsPostBack)
        {
            //included by SGS for task no: 20
            //005 - changed the cutoff date to beginning of year
            if (DateTime.Now >= Convert.ToDateTime("01/01/" + Convert.ToString(DateTime.Now.Year)))
            {
                txtYearOne.Text = Convert.ToString(DateTime.Now.Year - 1);
                txtYearTwo.Text = Convert.ToString(DateTime.Now.Year - 2);
                txtYearThree.Text = Convert.ToString(DateTime.Now.Year - 3);
                txtYear1.Text = Convert.ToString(DateTime.Now.Year - 4);
                txtYear2.Text = Convert.ToString(DateTime.Now.Year - 5);
                txtYear3.Text = Convert.ToString(DateTime.Now.Year - 6);
                txtYear4.Text = Convert.ToString(DateTime.Now.Year - 7);
                txtYear5.Text = Convert.ToString(DateTime.Now.Year - 8);
                txtYear6.Text = Convert.ToString(DateTime.Now.Year - 9);
                txtYear7.Text = Convert.ToString(DateTime.Now.Year - 10);
            }
            else
            {
                txtYearOne.Text = Convert.ToString(DateTime.Now.Year - 2);
                txtYearTwo.Text = Convert.ToString(DateTime.Now.Year - 3);
                txtYearThree.Text = Convert.ToString(DateTime.Now.Year - 4);
                txtYear1.Text = Convert.ToString(DateTime.Now.Year - 5);
                txtYear2.Text = Convert.ToString(DateTime.Now.Year - 6);
                txtYear3.Text = Convert.ToString(DateTime.Now.Year - 7);
                txtYear4.Text = Convert.ToString(DateTime.Now.Year - 8);
                txtYear5.Text = Convert.ToString(DateTime.Now.Year - 9);
                txtYear6.Text = Convert.ToString(DateTime.Now.Year - 10);
                txtYear7.Text = Convert.ToString(DateTime.Now.Year - 11);

            }
        }

        VQFEditSideMenu.GetVendorStatus();
        hdnVendorStatus.Value = Convert.ToString(VQFEditSideMenu.VendorStatus);
        //if (VQFMenu.VendorStatus == 1 || VQFMenu.VendorStatus == 2)
        //{
        //    imbSaveandClose.Visible = false;
        //    imbSaveNext.Visible = false;
        //    ImageButton imgAttach = (ImageButton)VQFMenu.FindControl("imbAttch");
        //    imgAttach.Visible = false;
        //}
        if (VQFEditSideMenu.VendorStatus == 1 || VQFEditSideMenu.VendorStatus == 2)
        {

            imbSaveNext.Visible = false;
            // imbSave.Visible = false;  


        }
        if (!IsPostBack)
        {
            LoadEditDatas(true);
            trExplainYear1.Style["Display"] = chkNAYear1.Checked ? "" : "none";
            trExplainYear2.Style["Display"] = chkNAYear2.Checked ? "" : "none";
            trExplainYear3.Style["Display"] = chkNAYear3.Checked ? "" : "none";
            trExplainYear4.Style["Display"] = chkNAYear4.Checked ? "" : "none";
            trExplainYear5.Style["Display"] = chkNAYear5.Checked ? "" : "none";
            trExplainYear6.Style["Display"] = chkNAYear6.Checked ? "" : "none";
            trExplainYear7.Style["Display"] = chkNAYear7.Checked ? "" : "none";
            trExplainYear8.Style["Display"] = chkNAYear8.Checked ? "" : "none";
            trExplainYear9.Style["Display"] = chkNAYear9.Checked ? "" : "none";
            trExplainYear10.Style["display"] = chkNAYear10.Checked ? "" : "none";
        }
        ControlVisiblity();

        //G. Vera 05/21/2014 - Added
        LoadToolTipOnPostBack();
    }

   

    #region Button Events

    //004-Sooraj
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        var ddl = sender as DropDownList;
        BindAssociateState(ddl, true, false);
        ddl.Focus();
    }


    protected void imbSaveNext_Click(object sender, ImageClickEventArgs e)
    {
        //VendorId = Convert.ToInt64(Session["VendorId"]);
        //VendorId = 1;
        bool? Legal1;
        bool? Legal2;
        bool? Legal3;
        bool? Legal4;

        if (rdlListOne.SelectedIndex < 0)
        {
            Legal1 = null;
        }
        else
        {
            Legal1 = rdlListOne.SelectedIndex == 1 ? false : true;
        }
        if (rdlListTwo.SelectedIndex < 0)
        {
            Legal2 = null;
        }
        else
        {
            Legal2 = rdlListTwo.SelectedIndex == 1 ? false : true;
        }

        if (rdlListThree.SelectedIndex < 0)
        {
            Legal3 = null;
        }
        else
        {
            Legal3 = rdlListThree.SelectedIndex == 1 ? false : true;
        }
        if (rdlListFour.SelectedIndex < 0)
        {
            Legal4 = null;
        }
        else
        {
            Legal4 = rdlListFour.SelectedIndex == 1 ? false : true;
        }

        bool Result = false;
        Page.Validate("Legal");
        if (Page.IsValid)
        {
            Page.Validate("Financial");
            if (Page.IsValid)
            {
                Page.Validate("Banking");
                string OutlookString = string.Empty;
                if (Page.IsValid)
                {
                    int CountLegal = objLegal.GetLegalCount(VendorId);
                    if (CountLegal == 0)
                    {
                        LegalStatus = 1;
                        Result = objLegal.InsertLegal(txtCurrentTotalBacking.Text.Trim(), txtCurrentProjectedRevenue.Text.Trim(), txtWorkAwd.Text.Trim(), txtStake.Text.Trim(), txtBankruptcy.Text.Trim(), txtRisk.Text.Trim(), Legal1, Legal2, Legal3, Legal4, Convert.ToInt64(Session["VendorId"]), LegalStatus,ddlCurrency.SelectedValue.ConvertToShortInt(), rdbDenominateInDollars.SelectedValue.ConvertToBool()); //003-Sooraj
                    }
                    else
                    {
                        LegalStatus = 2;
                        if (!string.IsNullOrEmpty(hdnLegalId.Value))
                        {
                            LegalID = Convert.ToInt64(hdnLegalId.Value);
                            //COMPARE
                            EditData = objLegal.GetSingleVQLData(VendorId);
                            LegalBankingData = objLegal.GetLegalBankingData(LegalID);
                            objFinancial = objLegal.SelectLegalFinancial(LegalID);
                            Result = objLegal.UpdateLegal(txtCurrentTotalBacking.Text.Trim(), txtCurrentProjectedRevenue.Text.Trim(), txtWorkAwd.Text.Trim(), txtStake.Text.Trim(), txtBankruptcy.Text.Trim(), txtRisk.Text.Trim(), Legal1, Legal2, Legal3, Legal4, LegalID, LegalStatus, ddlCurrency.SelectedValue.ConvertToShortInt(), rdbDenominateInDollars.SelectedValue.ConvertToBool()); //003-Sooraj
                            objLegal.DeleteAllRelationsofLegal(LegalID);

                            //COMPARE
                            objCommon.CompareChangeConsolidated = string.Empty;
                            UpdateLegalData = objLegal.GetSingleVQLData(VendorId);
                            objCommon.CompareChanges = Compare.CompareLegal(EditData, UpdateLegalData);

                            string[] a = objCommon.CompareChanges.Split(';');
                            int k = 0;
                            objCommon.CompareChanges = string.Empty;
                            while (k < a.Length - 1)
                            {

                                //if (a[k] == "CurrentYearProjectedRevenue")
                                //    FinancialPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                //else if (a[k] == "CurrentTotalBacklog")
                                //    FinancialPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                                //else if (k == a.Length - 1)
                                //    objCommon.CompareChanges += a[k];
                                //else
                                //    objCommon.CompareChanges += a[k] + ",";

                                if (a[k].Substring(6, a[k].IndexOf(' ') - 6) == "CurrentYearProjectedRevenue")
                                    FinancialPart += "%09" + a[k] + "%0A";
                                else if (a[k].Substring(6, a[k].IndexOf(' ') - 6) == "CurrentTotalBacklog")
                                    FinancialPart += "%09" + a[k] + "%0A";
                                else if (k == a.Length - 1)
                                    objCommon.CompareChanges += a[k];
                                else
                                    objCommon.CompareChanges += "%09" + a[k] + "%0A";

                                k++;

                            }


                            if (objCommon.CompareChanges != string.Empty)
                            {
                                objCommon.CompareChangeConsolidated += Legal + Break + objCommon.CompareChanges + Break;
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>" + Legal + "</u></b><br></font></td></tr>");
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + objCommon.CompareChanges + "<br></font></td></tr>");
                                OutlookString += "%0A" + Legal + "%0A" + objCommon.CompareChanges;
                            }
                            CompareData += objCommon.CompareChangeConsolidated;
                        }
                    }

                    //Get LegalID
                    if (Result)
                    {
                        EditLegalData = objLegal.GetSingleVQLData(VendorId);
                        hdnLegalId.Value = Convert.ToString(EditLegalData.PK_LegalID);
                        LegalID = Convert.ToInt64(hdnLegalId.Value);
                        InsertFinancial();
                        objFinancialUpdate = objLegal.SelectLegalFinancial(LegalID);
                        objCommon.CompareChangeConsolidated = string.Empty;
                        objCommon.CompareChanges = Compare.CompareDataSetNotAvailable(objFinancialUpdate, objFinancial, 0, "Financial", "Explanation");
                        if ((objCommon.CompareChanges + FinancialPart) != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += Financial + Break + objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>" + Financial + "</u></b><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            OutlookString += "%0A" + Financial;
                            if (objCommon.CompareChanges != string.Empty)
                            {
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + objCommon.CompareChanges + "<br></font></td></tr>");
                                OutlookString += "%0A" + objCommon.CompareChanges;
                            }
                            if (FinancialPart != string.Empty)
                            {
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + FinancialPart + "<br></font></td></tr>");
                                OutlookString += FinancialPart;
                            }

                        }
                        CompareData += objCommon.CompareChangeConsolidated;
                        InsertBanking();
                        //COMPARE
                        UpdateBankingData = objLegal.GetLegalBankingData(LegalID);
                        objCommon.CompareChangeConsolidated = string.Empty;
                        objCommon.CompareChanges = Compare.CompareDataSet(UpdateBankingData, LegalBankingData, 0, "Financial Institution");
                        if (objCommon.CompareChanges != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += Banking + Break + objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>" + Banking + "</u></b><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + objCommon.CompareChanges + "<br></font></td></tr>");
                            OutlookString += "%0A" + Banking;
                            OutlookString += "%0A" + objCommon.CompareChanges;
                        }
                        CompareData += objCommon.CompareChangeConsolidated;
                        if (CompareData != string.Empty)
                        {
                            objCommon.CompareChangeOutlook += LandF + Break + CompareData;
                            CompareBody.Append("</table>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='4'><b><u>" + LandF + "</u></b><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append(CompareBody);
                            CompareMainBody.Append("</table>");
                            OutlookString = LandF + OutlookString;
                            //Session["OUTLOOKDATA"] = Session["OUTLOOKDATA"].ToString() + CompareMainBody;
                            Session["OUTLOOKDATA"] = Session["OUTLOOKDATA"].ToString() + "%0A" + OutlookString;
                            HiddenField outlookContent = VQFEditSideMenu.FindControl("hdnOutllook") as HiddenField;
                            outlookContent.Value = Session["OUTLOOKDATA"].ToString();
                            HiddenField outlookVendorEmail = VQFEditSideMenu.FindControl("hdnOutllookVendorEmail") as HiddenField;
                            outlookVendorEmail.Value = Session["OUTLOOKTO"].ToString();

                        }

                        Response.Redirect("TradeInformation.aspx");
                    }
                }
                else
                {
                    lblMsg.Text = "";
                    lblheading.Text = "Banking - Validation";
                    lblError.Text = Errormsg();
                    modalExtnd.Show();
                    accLegal.SelectedIndex = 2;
                }
            }
            else
            {
                lblMsg.Text = "";
                lblheading.Text = "Financial - Validation";
                lblError.Text = Errormsg();
                modalExtnd.Show();
                accLegal.SelectedIndex = 1;
            }
        }
        else
        {
            lblMsg.Text = "";
            lblheading.Text = "Legal - Validation";
            lblError.Text = Errormsg();
            modalExtnd.Show();
            accLegal.SelectedIndex = 0;
        }
    }

    protected void imbSave_Click(object sender, ImageClickEventArgs e)
    {
        //VendorId = Convert.ToInt64(Session["VendorId"]);
        //VendorId = 1;
        bool? Legal1;
        bool? Legal2;
        bool? Legal3;
        bool? Legal4;

        if (rdlListOne.SelectedIndex < 0)
        {
            Legal1 = null;
        }
        else
        {
            Legal1 = rdlListOne.SelectedIndex == 1 ? false : true;
        }
        if (rdlListTwo.SelectedIndex < 0)
        {
            Legal2 = null;
        }
        else
        {
            Legal2 = rdlListTwo.SelectedIndex == 1 ? false : true;
        }

        if (rdlListThree.SelectedIndex < 0)
        {
            Legal3 = null;
        }
        else
        {
            Legal3 = rdlListThree.SelectedIndex == 1 ? false : true;
        }
        if (rdlListFour.SelectedIndex < 0)
        {
            Legal4 = null;
        }
        else
        {
            Legal4 = rdlListFour.SelectedIndex == 1 ? false : true;
        }

        bool Result = false;
        //006 - commented out all page validation...you never know
        //Page.Validate("Legal");
        //if (Page.IsValid)
        //{
            //Page.Validate("Financial");
            //if (Page.IsValid)
            //{
                //Page.Validate("Banking");
                string OutlookString = string.Empty;
                //if (Page.IsValid)
                //{
                    int CountLegal = objLegal.GetLegalCount(VendorId);
                    if (CountLegal == 0)
                    {
                        LegalStatus = 0;
                        Result = objLegal.InsertLegal(txtCurrentTotalBacking.Text.Trim(), txtCurrentProjectedRevenue.Text.Trim(), txtWorkAwd.Text.Trim(), txtStake.Text.Trim(), txtBankruptcy.Text.Trim(), txtRisk.Text.Trim(), Legal1, Legal2, Legal3, Legal4, Convert.ToInt64(Session["VendorId"]), LegalStatus, ddlCurrency.SelectedValue.ConvertToShortInt(), rdbDenominateInDollars.SelectedValue.ConvertToBool()); //003-Sooraj
                    }
                    else
                    {
                        
                        if (!string.IsNullOrEmpty(hdnLegalId.Value))
                        {
                            LegalID = Convert.ToInt64(hdnLegalId.Value);
                            //COMPARE
                            EditData = objLegal.GetSingleVQLData(VendorId);
                            LegalBankingData = objLegal.GetLegalBankingData(LegalID);
                            objFinancial = objLegal.SelectLegalFinancial(LegalID);
                            LegalStatus = Convert.ToByte(EditData.LegalStatus);
                            Result = objLegal.UpdateLegal(txtCurrentTotalBacking.Text.Trim(), txtCurrentProjectedRevenue.Text.Trim(), txtWorkAwd.Text.Trim(), txtStake.Text.Trim(), txtBankruptcy.Text.Trim(), txtRisk.Text.Trim(), Legal1, Legal2, Legal3, Legal4, LegalID, LegalStatus, ddlCurrency.SelectedValue.ConvertToShortInt(), rdbDenominateInDollars.SelectedValue.ConvertToBool()); //003-Sooraj
                            objLegal.DeleteAllRelationsofLegal(LegalID);

                            //COMPARE
                            objCommon.CompareChangeConsolidated = string.Empty;
                            UpdateLegalData = objLegal.GetSingleVQLData(VendorId);
                            objCommon.CompareChanges = Compare.CompareLegal(EditData, UpdateLegalData);

                            //string[] a = objCommon.CompareChanges.Split(';');
                            //int k = 0;
                            //objCommon.CompareChanges = string.Empty;
                            //while (k < a.Length - 1)
                            //{

                            //    //if (a[k] == "CurrentYearProjectedRevenue")
                            //    //    FinancialPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                            //    //else if (a[k] == "CurrentTotalBacklog")
                            //    //    FinancialPart += "<tr><td  colspan='2'><font  face='verdana' size='2'>" + a[k] + "<br></font></td></tr>";
                            //    //else if (k == a.Length - 1)
                            //    //    objCommon.CompareChanges += a[k];
                            //    //else
                            //    //    objCommon.CompareChanges += a[k] + ",";

                            //    if (a[k].Substring(6, a[k].IndexOf(' ') - 6) == "Current Year Projected Revenue")
                            //        FinancialPart += "%09" + a[k] + "%0A";
                            //    else if (a[k].Substring(6, a[k].IndexOf(' ') - 6) == "Current Total Backlog")
                            //        FinancialPart += "%09" + a[k] + "%0A";
                            //    else if (k == a.Length - 1)
                            //        objCommon.CompareChanges += a[k];
                            //    else
                            //        objCommon.CompareChanges += "%09" + a[k] + "%0A";

                            //    k++;

                            //}


                            if (objCommon.CompareChanges != string.Empty)
                            {
                                objCommon.CompareChangeConsolidated += Legal + Break + objCommon.CompareChanges + Break;
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>" + Legal + "</u></b><br></font></td></tr>");
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + objCommon.CompareChanges + "<br></font></td></tr>");
                                OutlookString += "%0A" + Legal + "%0A" + objCommon.CompareChanges;
                            }
                            CompareData += objCommon.CompareChangeConsolidated;
                        }
                    }

                    //Get LegalID
                    if (Result)
                    {
                        EditLegalData = objLegal.GetSingleVQLData(VendorId);
                        hdnLegalId.Value = Convert.ToString(EditLegalData.PK_LegalID);
                        LegalID = Convert.ToInt64(hdnLegalId.Value);
                        InsertFinancial();
                        objFinancialUpdate = objLegal.SelectLegalFinancial(LegalID);
                        objCommon.CompareChangeConsolidated = string.Empty;
                        objCommon.CompareChanges = Compare.CompareDataSetNotAvailable(objFinancialUpdate, objFinancial, 0, "Financial", "Explanation");
                        if ((objCommon.CompareChanges + FinancialPart) != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += Financial + Break + objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>" + Financial + "</u></b><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            OutlookString += "%0A" + Financial;
                            if (objCommon.CompareChanges != string.Empty)
                            {
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + objCommon.CompareChanges + "<br></font></td></tr>");
                                OutlookString += "%0A" + objCommon.CompareChanges;
                            }
                            if (FinancialPart != string.Empty)
                            {
                                CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + FinancialPart + "<br></font></td></tr>");
                                OutlookString += FinancialPart;
                            }

                        }
                        CompareData += objCommon.CompareChangeConsolidated;
                        InsertBanking();
                        //COMPARE
                        UpdateBankingData = objLegal.GetLegalBankingData(LegalID);
                        objCommon.CompareChangeConsolidated = string.Empty;
                        objCommon.CompareChanges = Compare.CompareDataSet(UpdateBankingData, LegalBankingData, 0, "Financial Institution");
                        if (objCommon.CompareChanges != string.Empty)
                        {
                            objCommon.CompareChangeConsolidated += Banking + Break + objCommon.CompareChanges + Break;
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><b><u>" + Banking + "</u></b><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>" + objCommon.CompareChanges + "<br></font></td></tr>");
                            OutlookString += "%0A" + Banking;
                            OutlookString += "%0A" + objCommon.CompareChanges;
                        }
                        CompareData += objCommon.CompareChangeConsolidated;
                        if (CompareData != string.Empty)
                        {
                            objCommon.CompareChangeOutlook += LandF + Break + CompareData;
                            CompareBody.Append("</table>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append("<tr><td  colspan='2'><font  face='verdana' size='4'><b><u>" + LandF + "</u></b><br></font></td></tr>");
                            CompareBody.Append("<tr><td  colspan='2'><font  face='verdana' size='2'><br></font></td></tr>");
                            CompareMainBody.Append(CompareBody);
                            CompareMainBody.Append("</table>");
                            OutlookString = LandF + OutlookString;
                        }

                        imbOk.Style["display"] = "none";
                        imbOkoutlook.Visible = true;

                        //Session["OUTLOOKDATA"] = Session["OUTLOOKDATA"].ToString() + CompareMainBody;
                        // Session["OUTLOOKDATA"] = Session["OUTLOOKDATA"].ToString() + "%0A" + OutlookString;
                        HiddenField outlookContent = VQFEditSideMenu.FindControl("hdnOutllook") as HiddenField;
                        outlookContent.Value = OutlookString;
                        HiddenField outlookVendorEmail = VQFEditSideMenu.FindControl("hdnOutllookVendorEmail") as HiddenField;
                        outlookVendorEmail.Value = Session["OUTLOOKTO"].ToString();
                        lblheading.Text = "Result";
                        lblError.Text = "";
                        lblMsg.Text = "&nbsp;<li>Legal & financial details has been saved successfully</li>";
                        modalExtnd.Show();




                    }
        //        }
        //        else
        //        {
        //            lblMsg.Text = "";
        //            lblheading.Text = "Banking - Validation";
        //            lblError.Text = Errormsg();
        //            modalExtnd.Show();
        //            accLegal.SelectedIndex = 2;
        //        }
        //    }
        //    else
        //    {
        //        lblMsg.Text = "";
        //        lblheading.Text = "Financial - Validation";
        //        lblError.Text = Errormsg();
        //        modalExtnd.Show();
        //        accLegal.SelectedIndex = 1;
        //    }
        //}
        //else
        //{
        //    lblMsg.Text = "";
        //    lblheading.Text = "Legal - Validation";
        //    lblError.Text = Errormsg();
        //    modalExtnd.Show();
        //    accLegal.SelectedIndex = 0;
        //}
    }

    #endregion

    #region Methods



    /// <summary>
    /// 006 -Sooraj
    /// </summary>
    /// <param name="countryDDL"></param>
    private void BindAssociateState(DropDownList countryDDL, bool IsBind = true, bool IsClear = false)
    {
        switch (countryDDL.ID)
        {
            case "ddlCountryYearOne":
                if (IsBind)
                    BindState(ddlCountryYearOne, ddlStateYearOne);
                SwitchPhoneNumber(countryDDL, txtMainPhno1, txtMainPhno2, txtMainPhno3, spnMainPhno2, IsClear);
                SetValidCharsAndLength(ddlStateYearOne, txtZIPOne, ftxtZip1, regvMAZip2, trOtherStateOne);
                break;
            case "ddlCountryYearOne1":
                if (IsBind)
                    BindState(ddlCountryYearOne1, ddlStateYearOne1);
                SwitchPhoneNumber(countryDDL, txtRefOne1Phno1, txtRefOne1Phno2, txtRefOne1Phno3, spnRefOne1Phno2, IsClear);
                SetValidCharsAndLength(ddlStateYearOne1, txtZIPOne1, ftxtZIPOne1, regvZipOne1, trOtherStateOne1);
                break;
            case "ddlCountryYearTwo":
                if (IsBind)
                    BindState(ddlCountryYearTwo, ddlStateYearTwo);
                SwitchPhoneNumber(countryDDL, txtReftwoPhno1, txtReftwoPhno2, txtReftwoPhno3, spnReftwoPhno2, IsClear);
                SetValidCharsAndLength(ddlStateYearTwo, txtZIPTwo, ftxtZipTwo, regvZipTwo, trOtherStateTwo);
                break;
            case "ddlCountryYearThree":
                if (IsBind)
                    BindState(ddlCountryYearThree, ddlStateYearThree);
                SwitchPhoneNumber(countryDDL, txtRefthreePhno1, txtRefthreePhno2, txtRefthreePhno3, spnRefthreePhno2, IsClear);
                SetValidCharsAndLength(ddlStateYearThree, txtZIPThree, ftxtZipThree, regvZipThree, trOtherStateThree);
                break;
            case "ddlCountryYearFour":
                if (IsBind)
                    BindState(ddlCountryYearFour, ddlStateYearFour);
                SwitchPhoneNumber(countryDDL, txtRefFourPhno1, txtRefFourPhno2, txtRefFourPhno3, spnRefFourPhno2, IsClear);
                SetValidCharsAndLength(ddlStateYearFour, txtZIPFour, ftxtZipFour, regvZipFour, trOtherStateFour);
                break;
            case "ddlCountryYearFive":
                if (IsBind)
                    BindState(ddlCountryYearFive, ddlStateYearFive);
                SwitchPhoneNumber(countryDDL, txtRefFivePhno1, txtRefFivePhno2, txtRefFivePhno3, spnRefFivePhno2, IsClear);
                SetValidCharsAndLength(ddlStateYearFive, txtZIPFive, ftxtZipFive, regvZipFive, trOtherStateFive);
                break;
            case "ddlCountryYearSix":
                if (IsBind)
                    BindState(ddlCountryYearSix, ddlStateYearSix);
                SwitchPhoneNumber(countryDDL, txtRefSixPhno1, txtRefSixPhno2, txtRefSixPhno3, spnRefSixPhno2, IsClear);
                SetValidCharsAndLength(ddlStateYearSix, txtZIPSix, ftxtZipSix, regvZipSix, trOtherStateSix);
                break;
            case "ddlCountryYearSeven":
                if (IsBind)
                    BindState(ddlCountryYearSeven, ddlStateYearSeven);
                SwitchPhoneNumber(countryDDL, txtRefSevenPhno1, txtRefSevenPhno2, txtRefSevenPhno3, spnRefSevenPhno2, IsClear);
                SetValidCharsAndLength(ddlStateYearSeven, txtZIPSeven, ftxtZipSeven, regvZipSeven, trOtherStateSeven);
                break;
            case "ddlCountryYearEight":
                if (IsBind)
                    BindState(ddlCountryYearEight, ddlStateYearEight);
                SwitchPhoneNumber(countryDDL, txtRefEightPhno1, txtRefEightPhno2, txtRefEightPhno3, spnRefEightPhno2, IsClear);
                SetValidCharsAndLength(ddlStateYearEight, txtZIPEight, ftxtzipEight, regvZipEight, trOtherStateEight);
                break;
            case "ddlCountryYearNine":
                if (IsBind)
                    BindState(ddlCountryYearNine, ddlStateYearNine);
                SwitchPhoneNumber(countryDDL, txtRefNinePhno1, txtRefNinePhno2, txtRefNinePhno3, spnRefNinePhno2, IsClear);
                SetValidCharsAndLength(ddlStateYearNine, txtZIPNine, ftxtZipNine, regvZipNine, trOtherStateNine);
                break;
        }
    }

    /// <summary>
    /// 006-Phone number changes
    /// </summary>
    /// <param name="phone1"></param>
    /// <param name="phone2"></param>
    /// <param name="phone3"></param>
    ///   <param name="spnHyphen"></param>
    ///   <param name="IsClear"></param>
    private void SwitchPhoneNumber(DropDownList countryDDL, TextBox phone1, TextBox phone2, TextBox phone3, HtmlControl spnHyphen, bool IsClear)
    {
        objCommon.SwitchPhoneNumberAdmin(countryDDL, phone1, phone2, phone3, spnHyphen, IsClear);
    }

    //004-Sooraj
    private void BindState(DropDownList countryDDL, DropDownList stateDDL)
    {
        var stateList = objCommon.GetStateByCountry(countryDDL.SelectedValue.ConvertToShortInt());

        objCommon.BindStateDropDownByCountry(stateList, stateDDL);
    }

    /// <summary>
    /// Need to handle the tool tip onm post back as it is being removed
    /// </summary>
    private void LoadToolTipOnPostBack()
    {
        objCommon.AddToolTipToItems(ddlCountryYearOne);
        objCommon.AddToolTipToItems(ddlCountryYearTwo);
        objCommon.AddToolTipToItems(ddlCountryYearThree);
        objCommon.AddToolTipToItems(ddlCountryYearFour);
        objCommon.AddToolTipToItems(ddlCountryYearFive);
        objCommon.AddToolTipToItems(ddlCountryYearSix);
        objCommon.AddToolTipToItems(ddlCountryYearSeven);
        objCommon.AddToolTipToItems(ddlCountryYearEight);
        objCommon.AddToolTipToItems(ddlCountryYearNine);
        objCommon.AddToolTipToItems(ddlCountryYearOne1);

        objCommon.AddToolTipToItems(ddlStateYearOne);
        objCommon.AddToolTipToItems(ddlStateYearOne1);
        objCommon.AddToolTipToItems(ddlStateYearTwo);
        objCommon.AddToolTipToItems(ddlStateYearThree);
        objCommon.AddToolTipToItems(ddlStateYearFour);
        objCommon.AddToolTipToItems(ddlStateYearFive);
        objCommon.AddToolTipToItems(ddlStateYearSix);
        objCommon.AddToolTipToItems(ddlStateYearSeven);
        objCommon.AddToolTipToItems(ddlStateYearEight);
        objCommon.AddToolTipToItems(ddlStateYearNine);


    }


    ///004
    /// <summary>
    /// Fill country drop down. 
    /// </summary>
    private void FillCountry()
    {
        var countryList = objCommon.GetCountryList();

        objCommon.BindCountryDropDown(countryList, ddlCountryYearOne);
        objCommon.BindCountryDropDown(countryList, ddlCountryYearTwo);
        objCommon.BindCountryDropDown(countryList, ddlCountryYearThree);
        objCommon.BindCountryDropDown(countryList, ddlCountryYearFour);
        objCommon.BindCountryDropDown(countryList, ddlCountryYearFive);
        objCommon.BindCountryDropDown(countryList, ddlCountryYearSix);
        objCommon.BindCountryDropDown(countryList, ddlCountryYearSeven);
        objCommon.BindCountryDropDown(countryList, ddlCountryYearEight);
        objCommon.BindCountryDropDown(countryList, ddlCountryYearNine);
        objCommon.BindCountryDropDown(countryList, ddlCountryYearOne1);

    }

    //003
    /// <summary>
    /// Fill Currency DropDown list and select 'USD' as default
    /// </summary>
    /// <Author>Sooraj Sudhakaran.T</Author>
    ///  <Date>7-OCT-2013</Date>
    private void FillCurrency()
    {
        var currencyList = objCommon.GetCurrencies();
        objCommon.BindCurrencyDropDown(currencyList,ddlCurrency);
    }

    /// <summary>
    /// Enable/Disable control based on Status.
    /// </summary>
    private void ControlVisiblity()
    {

        trDenominateinDollar.Style["Display"] = IsBasedInUs ? "none" : "";
        lblCompletedinDollar.Style["Display"] = IsBasedInUs ? "none" : "";

        if (chkNAYear1.Checked)
        {
            txtMaxContractValueYearOne.Enabled = false;
            txtAnuualCompRevenueYearOne.Enabled = false;
            txtMaxContractValueYearOne.Text = "";
            txtAnuualCompRevenueYearOne.Text = "";
        }
        else
        {
            txtMaxContractValueYearOne.Enabled = true;
            txtAnuualCompRevenueYearOne.Enabled = true;
        }
        if (chkNAYear2.Checked)
        {
            txtMaxContractValueYearTwo.Enabled = false;
            txtAnuualCompRevenueYearTwo.Enabled = false;
            txtMaxContractValueYearTwo.Text = "";
            txtAnuualCompRevenueYearTwo.Text = "";
        }
        else
        {
            txtMaxContractValueYearTwo.Enabled = true;
            txtAnuualCompRevenueYearTwo.Enabled = true;
        }
        if (chkNAYear3.Checked)
        {
            txtMaxContractValueYearThree.Enabled = false;
            txtAnuualCompRevenueYearThree.Enabled = false;
            txtMaxContractValueYearThree.Text = "";
            txtAnuualCompRevenueYearThree.Text = "";

        }
        else
        {
            txtMaxContractValueYearThree.Enabled = true;
            txtAnuualCompRevenueYearThree.Enabled = true;
        }

        if (rdlListOne.SelectedIndex >= 0)
        {
            trWorkAwdOne.Style["display"] = rdlListOne.SelectedItem.Text.Trim().Equals("Yes") ? "" : "none";
        }
        if (rdlListTwo.SelectedIndex >= 0)
        {
            trStock.Style["display"] = rdlListTwo.SelectedItem.Text.Trim().Equals("Yes") ? "" : "none";
        }
        if (rdlListThree.SelectedIndex >= 0)
        {
            trBankruptcy.Style["display"] = rdlListThree.SelectedItem.Text.Trim().Equals("Yes") ? "" : "none";
        }

        if (rdlListFour.SelectedIndex >= 0)
        {
            trConviction.Style["display"] = rdlListFour.SelectedItem.Text.Trim().Equals("Yes") ? "" : "none";
        }
        trExplainYear1.Style["Display"] = chkNAYear1.Checked ? "" : "none";
        trExplainYear2.Style["Display"] = chkNAYear2.Checked ? "" : "none";
        trExplainYear3.Style["Display"] = chkNAYear3.Checked ? "" : "none";
        trExplainYear4.Style["Display"] = chkNAYear4.Checked ? "" : "none";
        trExplainYear5.Style["Display"] = chkNAYear5.Checked ? "" : "none";
        trExplainYear6.Style["Display"] = chkNAYear6.Checked ? "" : "none";
        trExplainYear7.Style["Display"] = chkNAYear7.Checked ? "" : "none";
        trExplainYear8.Style["Display"] = chkNAYear8.Checked ? "" : "none";
        trExplainYear9.Style["Display"] = chkNAYear9.Checked ? "" : "none";
        trExplainYear10.Style["display"] = chkNAYear10.Checked ? "" : "none";

        for (int i = 1; i <= Convert.ToInt32(hdnCount.Value); i++)
        {
            switch (i)
            {
                case 1:
                    spnYearMore1.Style["display"] = "";
                    break;
                case 2:
                    spnYearMore2.Style["display"] = "";
                    break;
                case 3:
                    spnYearMore3.Style["display"] = "";
                    break;
                case 4:
                    spnYearMore4.Style["display"] = "";
                    break;
                case 5:
                    spnYearMore5.Style["display"] = "";
                    break;
                case 6:
                    spnYearMore6.Style["display"] = "";
                    break;
                case 7:
                    spnYearMore7.Style["display"] = "";
                    lnkYearAddMore.Style["display"] = "none";
                    break;
            }
        }
        if (!string.IsNullOrEmpty(hdnCountValue.Value))
        {
            for (int i = 1; i <= Convert.ToInt32(hdnCountValue.Value); i++)
            {
                switch (i)
                {
                    case 1:
                        spnmore1.Style["display"] = "";
                        break;
                    case 2:
                        spnmore2.Style["display"] = "";
                        break;
                    case 3:
                        spnmore3.Style["display"] = "";
                        break;
                    case 4:
                        spnmore4.Style["display"] = "";
                        break;
                    case 5:
                        spnmore5.Style["display"] = "";
                        break;
                    case 6:
                        spnmore6.Style["display"] = "";
                        break;
                    case 7:
                        spnmore7.Style["display"] = "";
                        break;
                    case 8:
                        spnmore8.Style["display"] = "";
                        imgAddMore.Style["display"] = "none";
                        break;
                }
            }
        }
        //txtCurrentProjectedRevenue.ReadOnly = true;
        //txtCurrentTotalBacking.ReadOnly = true;

        if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && ((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
        {
            lnkYearAddMore.Style["display"] = "none";
        }

        //005
        if (chkNAYear1.Checked)
        {
            spnYearMore1.Attributes["style"] = "display: ";
        }
        else
        {
            spnYearMore1.Attributes["style"] = "display: none";
        }

        // DAL.VQFLegalFinancial objFinancial1 = objLegal.SelectLegalFinancial(LegalID, txtYearOne.Text.Trim());

        // if (objFinancial1 != null)
        // {
        //     if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) )
        //     {
        //         //txtYearOne.ReadOnly = true;
        //         //txtMaxContractValueYearOne.ReadOnly = true;

        //         //txtAnuualCompRevenueYearOne.ReadOnly = true;
        //         //txtExplainYear1.ReadOnly = true;
        //         //txtCurrentProjectedRevenue.ReadOnly = true;
        //         //txtCurrentTotalBacking.ReadOnly = true;
        //         //chkNAYear1.Enabled = false;
        //     }
        //     else
        //     {
        //         txtYearOne.ReadOnly = false;
        //         txtMaxContractValueYearOne.ReadOnly = false;
        //         txtAnuualCompRevenueYearOne.ReadOnly = false;
        //         txtExplainYear1.ReadOnly = false;
        //         txtCurrentProjectedRevenue.ReadOnly = false;
        //         txtCurrentTotalBacking.ReadOnly = false;
        //         chkNAYear1.Enabled = true;
        //     }
        // }

        // objFinancial2 = objLegal.SelectLegalFinancial(LegalID, txtYearTwo.Text.Trim());
        // if (objFinancial2 != null)
        // {
        //     //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
        //     if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && ((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
        //     {
        //         chkNAYear2.Enabled = false;
        //         txtYearTwo.ReadOnly = true;
        //         txtMaxContractValueYearTwo.ReadOnly = true;
        //         txtAnuualCompRevenueYearTwo.ReadOnly = true;
        //        // txtExplainYesr2.ReadOnly = true;
        //     }
        //     else
        //     {
        //         chkNAYear2.Enabled = true;
        //         txtYearTwo.ReadOnly = false;
        //         txtMaxContractValueYearTwo.ReadOnly = false;
        //         txtAnuualCompRevenueYear2.ReadOnly = false;
        //         txtExplainYesr2.ReadOnly = false;
        //     }
        // }
        // objFinancial3 = objLegal.SelectLegalFinancial(LegalID, txtYearThree.Text.Trim());
        // if (objFinancial3 != null)
        // {
        //     //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
        //     if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && ((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
        //     {
        //         chkNAYear3.Enabled = false;
        //         txtYearThree.ReadOnly = true;
        //         txtMaxContractValueYearThree.ReadOnly = true;
        //         txtAnuualCompRevenueYearThree.ReadOnly = true;
        //        // txtExplainYear3.ReadOnly = true;
        //     }
        //     else
        //     {
        //         chkNAYear3.Enabled = true;
        //         txtYearThree.ReadOnly = false;
        //         txtMaxContractValueYearThree.ReadOnly = false;
        //         txtAnuualCompRevenueYearThree.ReadOnly = false;
        //         txtExplainYear3.ReadOnly = false;
        //     }
        // }

        // objFinancial4 = objLegal.SelectLegalFinancial(LegalID, txtYear1.Text.Trim());
        // if (objFinancial4 != null)
        // {
        //     //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
        //     if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && ((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
        //     {
        //         chkNAYear4.Enabled = false;
        //         txtYear1.ReadOnly = true;
        //         txtMaxContractValueYear1.ReadOnly = true;
        //         txtAnuualCompRevenueYear1.ReadOnly = true;
        //         txtExplainYear4.ReadOnly = true;
        //     }
        //     else
        //     {
        //         chkNAYear4.Enabled = true;
        //         txtYear1.ReadOnly = false;
        //         txtMaxContractValueYear1.ReadOnly = false;
        //         txtAnuualCompRevenueYear1.ReadOnly = false;
        //         txtExplainYear4.ReadOnly = false;
        //     }
        // }
        // objFinancial5 = objLegal.SelectLegalFinancial(LegalID, txtYear2.Text.Trim());
        // if (objFinancial5 != null)
        // {
        //     //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
        //     if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && ((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
        //     {
        //         chkNAYear5.Enabled = false;
        //         txtYear2.ReadOnly = true;
        //         txtMaxContractValueYear2.ReadOnly = true;
        //         txtAnuualCompRevenueYear2.ReadOnly = true;
        //         txtExplainYear5.ReadOnly = true;
        //     }
        //     else
        //     {
        //         chkNAYear5.Enabled = true;
        //         txtYear2.ReadOnly = false;
        //         txtMaxContractValueYear2.ReadOnly = false;
        //         txtAnuualCompRevenueYear2.ReadOnly = false;
        //         txtExplainYear5.ReadOnly = false;
        //     }
        // }
        // objFinancial6 = objLegal.SelectLegalFinancial(LegalID, txtYear3.Text.Trim());
        // if (objFinancial6 != null)
        // {
        //     //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
        //     if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && ((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
        //     {
        //         chkNAYear6.Enabled = false;
        //         txtYear3.ReadOnly = true;
        //         txtMaxContractValueYear3.ReadOnly = true;
        //         txtAnuualCompRevenueYear3.ReadOnly = true;
        //         txtExplainYear6.ReadOnly = true;
        //     }
        //     else
        //     {
        //         chkNAYear6.Enabled = false;
        //         txtYear3.ReadOnly = false;
        //         txtMaxContractValueYear3.ReadOnly = false;
        //         txtAnuualCompRevenueYear3.ReadOnly = false;
        //         txtExplainYear6.ReadOnly = false;
        //     }
        // }
        // objFinancial7 = objLegal.SelectLegalFinancial(LegalID, txtYear4.Text.Trim());
        // if (objFinancial7 != null)
        // {

        //     //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
        //     if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && ((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
        //     {
        //         chkNAYear7.Enabled = false;
        //         txtYear4.ReadOnly = true;
        //         txtMaxContractValueYear4.ReadOnly = true;
        //         txtAnuualCompRevenueYear4.ReadOnly = true;
        //         txtExplainYear7.ReadOnly = true;

        //     }
        //     else
        //     {
        //         chkNAYear7.Enabled = true;
        //         txtYear4.ReadOnly = false;
        //         txtMaxContractValueYear4.ReadOnly = false;
        //         txtAnuualCompRevenueYear4.ReadOnly = false;
        //         txtExplainYear7.ReadOnly = false;
        //     }
        // }
        //objFinancial8 = objLegal.SelectLegalFinancial(LegalID, txtYear5.Text.Trim());
        // if (objFinancial8 != null)
        // {
        //     //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
        //     if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && ((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
        //     {
        //         chkNAYear8.Enabled = false;
        //         txtYear5.ReadOnly = true;
        //         txtMaxContractValueYear5.ReadOnly = true;
        //         txtAnuualCompRevenueYear5.ReadOnly = true;
        //         txtExplainYear8.ReadOnly = true;
        //     }
        //     else
        //     {
        //         chkNAYear8.Enabled = true;
        //         txtYear5.ReadOnly = false;
        //         txtMaxContractValueYear5.ReadOnly = false;
        //         txtAnuualCompRevenueYear5.ReadOnly = false;
        //         txtExplainYear8.ReadOnly = false;
        //     }
        // }
        // objFinancial9 = objLegal.SelectLegalFinancial(LegalID, txtYear6.Text.Trim());
        // if (objFinancial9 != null)
        // {
        //     //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
        //     if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && ((Convert.ToInt32(hdnVendorStatus.Value) == 0) && Convert.ToInt32(hdnVendorStatus.Value) > 0))
        //     {
        //         chkNAYear9.Enabled = false;
        //         txtYear6.ReadOnly = true;
        //         txtMaxContractValueYear6.ReadOnly = true;
        //         txtAnuualCompRevenueYear6.ReadOnly = true;
        //         txtExplainYear9.ReadOnly = true;
        //     }
        //     else
        //     {
        //         chkNAYear9.Enabled = true;
        //         txtYear6.ReadOnly = false;
        //         txtMaxContractValueYear6.ReadOnly = false;
        //         txtAnuualCompRevenueYear6.ReadOnly = false;
        //         txtExplainYear9.ReadOnly = false;
        //     }
        // }

        // objFinancial10 = objLegal.SelectLegalFinancial(LegalID, txtYear7.Text.Trim());
        // if (objFinancial10 != null)
        // {
        //     //if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) >= 0) && (Convert.ToInt32(ViewState["EditStatus"]) == 1))
        //     if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && ((Convert.ToInt32(hdnVendorStatus.Value) == 0) && (Convert.ToInt32(hdnVendorStatus.Value) > 0)))
        //     {
        //         chkNAYear10.Enabled = false;
        //         txtYear7.ReadOnly = true;
        //         txtMaxContractValueYear7.ReadOnly = true;
        //         txtAnuualCompRevenueYear7.ReadOnly = true;
        //         txtExplainYear10.ReadOnly = true;
        //     }
        //     else
        //     {
        //         chkNAYear10.Enabled = true;
        //         txtYear7.ReadOnly = false;
        //         txtMaxContractValueYear7.ReadOnly = false;
        //         txtAnuualCompRevenueYear7.ReadOnly = false;
        //         txtExplainYear10.ReadOnly = false;
        //     }
        // }

        // SOC

        SetValidCharsAndLength(ddlStateYearOne, txtZIPOne, ftxtZip1, regvMAZip2, trOtherStateOne);
        SetValidCharsAndLength(ddlStateYearOne1, txtZIPOne1, ftxtZIPOne1, regvZipOne1, trOtherStateOne1);
        SetValidCharsAndLength(ddlStateYearTwo, txtZIPTwo, ftxtZipTwo, regvZipTwo, trOtherStateTwo);
        SetValidCharsAndLength(ddlStateYearThree, txtZIPThree, ftxtZipThree, regvZipThree, trOtherStateThree);
        SetValidCharsAndLength(ddlStateYearFour, txtZIPFour, ftxtZipFour, regvZipFour, trOtherStateFour);
        SetValidCharsAndLength(ddlStateYearFive, txtZIPFive, ftxtZipFive, regvZipFive, trOtherStateFive);
        SetValidCharsAndLength(ddlStateYearSix, txtZIPSix, ftxtZipSix, regvZipSix, trOtherStateSix);
        SetValidCharsAndLength(ddlStateYearSeven, txtZIPSeven, ftxtZipSeven, regvZipSeven, trOtherStateSeven);
        SetValidCharsAndLength(ddlStateYearEight, txtZIPEight, ftxtzipEight, regvZipEight, trOtherStateEight);
        SetValidCharsAndLength(ddlStateYearNine, txtZIPNine, ftxtZipNine, regvZipNine, trOtherStateNine);

        // EOC

    }

    /// <summary>
    /// Display Panel based on the selected sub menu
    /// </summary>
    private void ShowHideControls()
    {
        switch (Convert.ToString(Request.QueryString["submenu"]))
        {
            case "1":
                accLegal.SelectedIndex = 0;
                break;
            case "2":
                accLegal.SelectedIndex = 1;
                break;
            case "3":
                accLegal.SelectedIndex = 2;
                break;
        }
    }

    /// <summary>
    /// Fill state for dropdown. 
    /// </summary>
    private void FillState()
    {
        //objCommon.DisplayddlData(ddlStateYearEight);
        //objCommon.DisplayddlData(ddlStateYearFive);
        //objCommon.DisplayddlData(ddlStateYearFour);
        //objCommon.DisplayddlData(ddlStateYearNine);
        //objCommon.DisplayddlData(ddlStateYearOne);
        //objCommon.DisplayddlData(ddlStateYearOne1);
        //objCommon.DisplayddlData(ddlStateYearSeven);
        //objCommon.DisplayddlData(ddlStateYearSix);

        //objCommon.DisplayddlData(ddlStateYearThree);
        //objCommon.DisplayddlData(ddlStateYearTwo);


        //004-Bind State
        BindState(ddlCountryYearOne, ddlStateYearOne);
        BindState(ddlCountryYearOne1, ddlStateYearOne1);
        BindState(ddlCountryYearTwo, ddlStateYearTwo);

        BindState(ddlCountryYearThree, ddlStateYearThree);
        BindState(ddlCountryYearFour, ddlStateYearFour);
        BindState(ddlCountryYearFive, ddlStateYearFive);
        BindState(ddlCountryYearSix, ddlStateYearSix);
        BindState(ddlCountryYearSeven, ddlStateYearSeven);
        BindState(ddlCountryYearEight, ddlStateYearEight);
        BindState(ddlCountryYearNine, ddlStateYearNine);
    }

    /// <summary>
    /// Append client-side event scripts.
    /// </summary>
    private void CallScripts()
    {
        //005
        chkNAYear1.Attributes.Add("onClick", "DisplayExplain('" + chkNAYear1.ClientID + "','" + trExplainYear1.ClientID + "','" + txtMaxContractValueYearOne.ClientID + "','" + txtAnuualCompRevenueYearOne.ClientID + "','" + txtExplainYear1.ClientID + "');Javascript:DisplayElement('" + chkNAYear1.ClientID + "','" + spnYearMore1.ClientID + "')");
        chkNAYear2.Attributes.Add("onClick", "DisplayExplain('" + chkNAYear2.ClientID + "','" + trExplainYear2.ClientID + "','" + txtMaxContractValueYearTwo.ClientID + "','" + txtAnuualCompRevenueYearTwo.ClientID + "','" + txtExplainYesr2.ClientID + "');");
        chkNAYear3.Attributes.Add("onClick", "DisplayExplain('" + chkNAYear3.ClientID + "','" + trExplainYear3.ClientID + "','" + txtMaxContractValueYearThree.ClientID + "','" + txtAnuualCompRevenueYearThree.ClientID + "','" + txtExplainYear3.ClientID + "');");
        chkNAYear4.Attributes.Add("onClick", "DisplayExplain('" + chkNAYear4.ClientID + "','" + trExplainYear4.ClientID + "','" + txtMaxContractValueYear1.ClientID + "','" + txtAnuualCompRevenueYear1.ClientID + "','" + txtExplainYear4.ClientID + "');");
        chkNAYear5.Attributes.Add("onClick", "DisplayExplain('" + chkNAYear5.ClientID + "','" + trExplainYear5.ClientID + "','" + txtMaxContractValueYear2.ClientID + "','" + txtAnuualCompRevenueYear2.ClientID + "','" + txtExplainYear5.ClientID + "');");

        chkNAYear6.Attributes.Add("onClick", "DisplayExplain('" + chkNAYear6.ClientID + "','" + trExplainYear6.ClientID + "','" + txtMaxContractValueYear3.ClientID + "','" + txtAnuualCompRevenueYear3.ClientID + "','" + txtExplainYear6.ClientID + "');");
        chkNAYear7.Attributes.Add("onClick", "DisplayExplain('" + chkNAYear7.ClientID + "','" + trExplainYear7.ClientID + "','" + txtMaxContractValueYear4.ClientID + "','" + txtAnuualCompRevenueYear4.ClientID + "','" + txtExplainYear7.ClientID + "');");
        chkNAYear8.Attributes.Add("onClick", "DisplayExplain('" + chkNAYear8.ClientID + "','" + trExplainYear8.ClientID + "','" + txtMaxContractValueYear5.ClientID + "','" + txtAnuualCompRevenueYear5.ClientID + "','" + txtExplainYear8.ClientID + "');");
        chkNAYear9.Attributes.Add("onClick", "DisplayExplain('" + chkNAYear9.ClientID + "','" + trExplainYear9.ClientID + "','" + txtMaxContractValueYear6.ClientID + "','" + txtAnuualCompRevenueYear6.ClientID + "','" + txtExplainYear9.ClientID + "');");
        chkNAYear10.Attributes.Add("onClick", "DisplayExplain('" + chkNAYear10.ClientID + "','" + trExplainYear10.ClientID + "','" + txtMaxContractValueYear7.ClientID + "','" + txtAnuualCompRevenueYear7.ClientID + "','" + txtExplainYear10.ClientID + "');");

        txtMainPhno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtMainPhno1.ClientID + "," + txtMainPhno2.ClientID + ")");
        txtMainPhno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtMainPhno2.ClientID + "," + txtMainPhno3.ClientID + ")");

        txtRefOne1Phno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefOne1Phno1.ClientID + "," + txtRefOne1Phno2.ClientID + ")");
        txtRefOne1Phno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefOne1Phno2.ClientID + "," + txtRefOne1Phno3.ClientID + ")");

        txtReftwoPhno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtReftwoPhno1.ClientID + "," + txtReftwoPhno2.ClientID + ")");
        txtReftwoPhno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtReftwoPhno2.ClientID + "," + txtReftwoPhno3.ClientID + ")");

        txtRefthreePhno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefthreePhno1.ClientID + "," + txtRefthreePhno2.ClientID + ")");
        txtRefthreePhno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefthreePhno2.ClientID + "," + txtRefthreePhno3.ClientID + ")");

        txtRefFourPhno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefFourPhno1.ClientID + "," + txtRefFourPhno2.ClientID + ")");
        txtRefFourPhno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefFourPhno2.ClientID + "," + txtRefFourPhno3.ClientID + ")");

        txtRefFivePhno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefFivePhno1.ClientID + "," + txtRefFivePhno2.ClientID + ")");
        txtRefFivePhno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefFivePhno2.ClientID + "," + txtRefFivePhno3.ClientID + ")");

        txtRefSixPhno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefSixPhno1.ClientID + "," + txtRefSixPhno2.ClientID + ")");
        txtRefSixPhno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefSixPhno2.ClientID + "," + txtRefSixPhno3.ClientID + ")");

        txtRefSevenPhno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefSevenPhno1.ClientID + "," + txtRefSevenPhno2.ClientID + ")");
        txtRefSevenPhno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefSevenPhno2.ClientID + "," + txtRefSevenPhno3.ClientID + ")");

        txtRefEightPhno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefEightPhno1.ClientID + "," + txtRefEightPhno2.ClientID + ")");
        txtRefEightPhno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefEightPhno2.ClientID + "," + txtRefEightPhno3.ClientID + ")");

        txtRefNinePhno1.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefNinePhno1.ClientID + "," + txtRefNinePhno2.ClientID + ")");
        txtRefNinePhno2.Attributes.Add("onkeyup", "javascript:SetFocusOnPhone(" + txtRefNinePhno2.ClientID + "," + txtRefNinePhno3.ClientID + ")");

        imgAddMore.Attributes.Add("onClick", "return Visibletr();");
        lnkYearAddMore.Attributes.Add("onClick", "return VisibleYear();");

        txtMaxContractValueYearOne.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearOne.ClientID + "')");
        txtMaxContractValueYearOne.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearOne.ClientID + "')");
        txtMaxContractValueYearOne.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearOne.ClientID + "')");

        //txtAnuualCompRevenueYearOne.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearOne.ClientID + "');validateRevenue();");
        txtAnuualCompRevenueYearOne.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearOne.ClientID + "')");
        txtAnuualCompRevenueYearOne.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearOne.ClientID + "')");
        txtAnuualCompRevenueYearOne.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearOne.ClientID + "')");

        txtMaxContractValueYearTwo.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearTwo.ClientID + "')");
        txtMaxContractValueYearTwo.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearTwo.ClientID + "')");
        txtMaxContractValueYearTwo.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearTwo.ClientID + "')");

        txtMaxContractValueYear1.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear1.ClientID + "')");
        txtMaxContractValueYear1.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear1.ClientID + "')");
        txtMaxContractValueYear1.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear1.ClientID + "')");

        //txtAnuualCompRevenueYearTwo.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearTwo.ClientID + "');validateRevenue();");
        txtAnuualCompRevenueYearTwo.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearTwo.ClientID + "')");
        txtAnuualCompRevenueYearTwo.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearTwo.ClientID + "')");
        txtAnuualCompRevenueYearTwo.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearTwo.ClientID + "')");

        txtMaxContractValueYearThree.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearThree.ClientID + "')");
        txtMaxContractValueYearThree.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearThree.ClientID + "')");
        txtMaxContractValueYearThree.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYearThree.ClientID + "')");

        //txtAnuualCompRevenueYearThree.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearThree.ClientID + "');validateRevenue();");
        txtAnuualCompRevenueYearThree.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearThree.ClientID + "')");
        txtAnuualCompRevenueYearThree.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearThree.ClientID + "')");
        txtAnuualCompRevenueYearThree.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYearThree.ClientID + "')");

        txtCurrentProjectedRevenue.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtCurrentProjectedRevenue.ClientID + "')");

        txtCurrentProjectedRevenue.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtCurrentProjectedRevenue.ClientID + "')");
        txtCurrentProjectedRevenue.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtCurrentProjectedRevenue.ClientID + "')");

        txtCurrentTotalBacking.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtCurrentTotalBacking.ClientID + "')");
        txtCurrentTotalBacking.Attributes.Add("onkeyup", "javascript:numberFormat(this.value,'','" + txtCurrentTotalBacking.ClientID + "')");
        txtCurrentTotalBacking.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtCurrentTotalBacking.ClientID + "')");

        //txtAnuualCompRevenueYear1.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear1.ClientID + "');validateRevenue();");
        txtAnuualCompRevenueYear1.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear1.ClientID + "')");
        txtAnuualCompRevenueYear1.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear1.ClientID + "')");

        txtMaxContractValueYear2.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear2.ClientID + "')");
        txtMaxContractValueYear2.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear2.ClientID + "')");

        //txtAnuualCompRevenueYear2.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear2.ClientID + "');validateRevenue();");
        txtAnuualCompRevenueYear2.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear2.ClientID + "')");
        txtAnuualCompRevenueYear2.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear2.ClientID + "')");

        txtMaxContractValueYear3.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear3.ClientID + "')");
        txtMaxContractValueYear3.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear3.ClientID + "')");

        //txtAnuualCompRevenueYear3.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear3.ClientID + "');validateRevenue();");
        txtAnuualCompRevenueYear3.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear3.ClientID + "')");
        txtAnuualCompRevenueYear3.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear3.ClientID + "')");

        txtMaxContractValueYear4.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear4.ClientID + "')");
        txtMaxContractValueYear4.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear4.ClientID + "')");

        //txtAnuualCompRevenueYear4.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear4.ClientID + "');validateRevenue();");
        txtAnuualCompRevenueYear4.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear4.ClientID + "')");
        txtAnuualCompRevenueYear4.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear4.ClientID + "')");

        txtMaxContractValueYear5.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear5.ClientID + "')");
        txtMaxContractValueYear5.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear5.ClientID + "')");

        //txtAnuualCompRevenueYear5.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear5.ClientID + "');validateRevenue();");
        txtAnuualCompRevenueYear5.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear5.ClientID + "')");
        txtAnuualCompRevenueYear5.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear5.ClientID + "')");

        txtMaxContractValueYear6.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear6.ClientID + "')");
        txtMaxContractValueYear6.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear6.ClientID + "')");

        //txtAnuualCompRevenueYear6.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear6.ClientID + "');validateRevenue();");
        txtAnuualCompRevenueYear6.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear6.ClientID + "')");
        txtAnuualCompRevenueYear6.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear6.ClientID + "')");

        txtMaxContractValueYear7.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear7.ClientID + "')");
        txtMaxContractValueYear7.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtMaxContractValueYear7.ClientID + "')");

        //txtAnuualCompRevenueYear7.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear7.ClientID + "');validateRevenue();");
        txtAnuualCompRevenueYear7.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear7.ClientID + "')");
        txtAnuualCompRevenueYear7.Attributes.Add("onkeypress", "javascript:numberFormat(this.value,'','" + txtAnuualCompRevenueYear7.ClientID + "')");


        BindAssociateState(ddlCountryYearOne, false,true); //006-Sooraj
        BindAssociateState(ddlCountryYearOne1, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryYearTwo, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryYearThree, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryYearFour, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryYearFive, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryYearSix, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryYearSeven, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryYearEight, false, true); //006-Sooraj 
        BindAssociateState(ddlCountryYearNine, false, true); //006-Sooraj 
       

        ScriptForState();
    }

    /// <summary>
    /// scipts for state controls
    /// </summary>
    private void ScriptForState()
    {
        //ddlStateYearOne.Attributes.Add("onChange", "return VisibleOtherStateOne();");
        //ddlStateYearOne1.Attributes.Add("onChange", "return VisibleOtherStateOne1();");
        //ddlStateYearTwo.Attributes.Add("onChange", "return VisibleOtherStateTwo();");
        //ddlStateYearThree.Attributes.Add("onChange", "return VisibleOtherStateThree();");
        //ddlStateYearFour.Attributes.Add("onChange", "return VisibleOtherStateFour();");
        //ddlStateYearFive.Attributes.Add("onChange", "return VisibleOtherStateFive();");
        //ddlStateYearSix.Attributes.Add("onChange", "return VisibleOtherStateSix();");
        //ddlStateYearSeven.Attributes.Add("onChange", "return VisibleOtherStateSeven();");
        //ddlStateYearEight.Attributes.Add("onChange", "return VisibleOtherStateEight();");
        //ddlStateYearNine.Attributes.Add("onChange", "return VisibleOtherStateNine();");

        // SOC

        //ddlStateYearOne.Attributes.Add("onchange", "visibleControl('" + ddlStateYearOne.ClientID + "','" + txtZIPOne.ClientID + "','" + trOtherStateOne.ClientID + "','" + ftxtZip1.ClientID + "','" "','" + txtOtherState.ClientID + "')");
        //ddlStateYearOne1.Attributes.Add("onchange", "visibleControl('" + ddlStateYearOne1.ClientID + "','" + txtZIPOne1.ClientID + "','" + trOtherStateOne1.ClientID + "','" + ftxtZIPOne1.ClientID + "','" + regvZipOne1.ClientID + "','" + txtOtherState1.ClientID + "')");

        //ddlStateYearTwo.Attributes.Add("onchange", "visibleControl('" + ddlStateYearTwo.ClientID + "','" + txtZIPTwo.ClientID + "','" + trOtherStateTwo.ClientID + "','" + ftxtZipTwo.ClientID + "','" + regvZipTwo.ClientID + "','" + txtOtherStateTwo.ClientID + "')");
        //ddlStateYearThree.Attributes.Add("onchange", "visibleControl('" + ddlStateYearThree.ClientID + "','" + txtZIPThree.ClientID + "','" + trOtherStateThree.ClientID + "','" + ftxtZipThree.ClientID + "','" + regvZipThree.ClientID + "','" + txtOtherStateThree.ClientID + "')");
        //ddlStateYearFour.Attributes.Add("onchange", "visibleControl('" + ddlStateYearFour.ClientID + "','" + txtZIPFour.ClientID + "','" + trOtherStateFour.ClientID + "','" + ftxtZipFour.ClientID + "','" + regvZipFour.ClientID + "','" + txtOtherStateFour.ClientID + "')");
        //ddlStateYearFive.Attributes.Add("onchange", "visibleControl('" + ddlStateYearFive.ClientID + "','" + txtZIPFive.ClientID + "','" + trOtherStateFive.ClientID + "','" + ftxtZipFive.ClientID + "','" + regvZipFive.ClientID + "','" + txtOtherStateFive.ClientID + "')");
        //ddlStateYearSix.Attributes.Add("onchange", "visibleControl('" + ddlStateYearSix.ClientID + "','" + txtZIPSix.ClientID + "','" + trOtherStateSix.ClientID + "','" + ftxtZipSix.ClientID + "','" + regvZipSix.ClientID + "','" + txtOtherStateSix.ClientID + "')");
        //ddlStateYearSeven.Attributes.Add("onchange", "visibleControl('" + ddlStateYearSeven.ClientID + "','" + txtZIPSeven.ClientID + "','" + trOtherStateSeven.ClientID + "','" + ftxtZipSeven.ClientID + "','" + regvZipSeven.ClientID + "','" + txtOtherStateSeven.ClientID + "')");
        //ddlStateYearEight.Attributes.Add("onchange", "visibleControl('" + ddlStateYearEight.ClientID + "','" + txtZIPEight.ClientID + "','" + trOtherStateEight.ClientID + "','" + ftxtzipEight.ClientID + "','" + regvZipEight.ClientID + "','" + txtOtherStateEight.ClientID + "')");
        //ddlStateYearNine.Attributes.Add("onchange", "visibleControl('" + ddlStateYearNine.ClientID + "','" + txtZIPNine.ClientID + "','" + trOtherStateNine.ClientID + "','" + ftxtZipNine.ClientID + "','" + regvZipNine.ClientID + "','" + txtOtherStateNine.ClientID + "')");

        // EOC
    }

    //To check the ZIP length
    public string CheckZIP(string ZIP)
    {
        return ZIP;
    }

    /// <summary>
    /// Insert legal banking for save and close
    /// </summary>
    private void InsertLegalBanking()
    {
        LegalID = Convert.ToInt64(hdnLegalId.Value);
        string Phone1 = objCommon.CheckPhone(txtMainPhno1.Text.Trim(), txtMainPhno2.Text.Trim(), txtMainPhno3.Text.Trim(), txtMainPhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearOne.Text.Trim()))) || (Phone1 != "") || (rdlLineofCreditOne.SelectedIndex >= 0) || (ddlStateYearOne.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameOne.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearOne.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityOne.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPOne.Text.Trim()))))
        {
            bool? Credit1 = GetBooleanValue(rdlLineofCreditOne);
            bool result = objLegal.InsertLegalBanking(txtAddressYearOne.Text.Trim(), txtCityOne.Text.Trim(), txtContactNameOne.Text.Trim(), Credit1, txtFinancialInstitutionYearOne.Text.Trim(), Convert.ToInt16(ddlStateYearOne.SelectedValue), txtOtherState.Text.Trim(), Phone1, CheckZIP(txtZIPOne.Text.Trim()), LegalID, ddlCountryYearOne.SelectedValue.ConvertToShortInt()); //004-sooraj
            ResetBanking(null, txtAddressYearOne, txtCityOne, txtContactNameOne, txtFinancialInstitutionYearOne, txtOtherState, txtMainPhno1, txtMainPhno2, txtMainPhno3, txtZIPOne, ddlStateYearOne, rdlLineofCreditOne, trOtherStateOne, ftxtZip1, regvMAZip2);
        }

        bool? Credit2 = GetBooleanValue(rdlLineofCreditOne1);
        string Phone2 = objCommon.CheckPhone(txtRefOne1Phno1.Text.Trim(), txtRefOne1Phno2.Text.Trim(), txtRefOne1Phno3.Text.Trim(), txtRefOne1Phno3);//006

        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearOne1.Text.Trim()))) || (Phone2 != "") || (rdlLineofCreditOne1.SelectedIndex >= 0) || (ddlStateYearOne1.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameOne1.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearOne1.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityOne1.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPOne1.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearOne1.Text.Trim(), txtCityOne1.Text.Trim(), txtContactNameOne1.Text.Trim(), Credit2, txtFinancialInstitutionYearOne1.Text.Trim(), Convert.ToInt16(ddlStateYearOne1.SelectedValue), txtOtherState1.Text.Trim(), Phone2, CheckZIP(txtZIPOne1.Text.Trim()), LegalID, ddlCountryYearOne1.SelectedValue.ConvertToShortInt()); //004-sooraj
            ResetBanking(null, txtAddressYearOne1, txtCityOne1, txtContactNameOne1, txtFinancialInstitutionYearOne1, txtOtherState1, txtRefOne1Phno1, txtRefOne1Phno2, txtRefOne1Phno3, txtZIPOne1, ddlStateYearOne1, rdlLineofCreditOne1, trOtherStateOne1, ftxtZIPOne1, regvZipOne1);
        }

        bool? Credit3 = GetBooleanValue(rdlLineofCreditTwo);
        string Phone3 = objCommon.CheckPhone(txtReftwoPhno1.Text.Trim(), txtReftwoPhno2.Text.Trim(), txtReftwoPhno3.Text.Trim(), txtReftwoPhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearTwo.Text.Trim()))) || (Phone3 != "") || (rdlLineofCreditTwo.SelectedIndex >= 0) || (ddlStateYearTwo.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameTwo.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearTwo.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityTwo.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPTwo.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearTwo.Text.Trim(), txtCityTwo.Text.Trim(), txtContactNameTwo.Text.Trim(), Credit3, txtFinancialInstitutionYearTwo.Text.Trim(), Convert.ToInt16(ddlStateYearTwo.SelectedValue), txtOtherStateTwo.Text.Trim(), Phone3, CheckZIP(txtZIPTwo.Text.Trim()), LegalID, ddlCountryYearTwo.SelectedValue.ConvertToShortInt()); //004-sooraj
            ResetBanking(spnmore1, txtFinancialInstitutionYearTwo, txtCityTwo, txtContactNameTwo, txtFinancialInstitutionYearTwo, txtOtherStateTwo, txtReftwoPhno1, txtReftwoPhno2, txtReftwoPhno3, txtZIPOne1, ddlStateYearTwo, rdlLineofCreditTwo, trOtherStateTwo, ftxtZipTwo, regvZipTwo);
        }

        bool? Credit4 = GetBooleanValue(rdlLineofCreditThree);
        string Phone4 = objCommon.CheckPhone(txtRefthreePhno1.Text.Trim(), txtRefthreePhno2.Text.Trim(), txtRefthreePhno3.Text.Trim(), txtRefthreePhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearThree.Text.Trim()))) || (Phone4 != "") || (rdlLineofCreditThree.SelectedIndex >= 0) || (ddlStateYearThree.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameThree.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearThree.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityThree.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPThree.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearThree.Text.Trim(), txtCityThree.Text.Trim(), txtContactNameThree.Text.Trim(), Credit4, txtFinancialInstitutionYearThree.Text.Trim(), Convert.ToInt16(ddlStateYearThree.SelectedValue), txtOtherStateThree.Text.Trim(), Phone4, CheckZIP(txtZIPThree.Text.Trim()), LegalID, ddlCountryYearThree.SelectedValue.ConvertToShortInt()); //004-sooraj
            ResetBanking(spnmore2, txtAddressYearThree, txtCityThree, txtContactNameThree, txtFinancialInstitutionYearThree, txtOtherStateThree, txtRefthreePhno2, txtRefthreePhno2, txtRefthreePhno3, txtZIPOne1, ddlStateYearThree, rdlLineofCreditThree, trOtherStateThree, ftxtZipThree, regvZipThree);
        }

        bool? Credit5 = GetBooleanValue(rdlLineofCreditFour);
        string Phone5 = objCommon.CheckPhone(txtRefFourPhno1.Text.Trim(), txtRefFourPhno2.Text.Trim(), txtRefFourPhno3.Text.Trim(), txtRefFourPhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearFour.Text.Trim()))) || (Phone5 != "") || (rdlLineofCreditFour.SelectedIndex >= 0) || (ddlStateYearFour.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameFour.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearFour.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityFour.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPFour.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearFour.Text.Trim(), txtCityFour.Text.Trim(), txtContactNameFour.Text.Trim(), Credit5, txtFinancialInstitutionYearFour.Text.Trim(), Convert.ToInt16(ddlStateYearFour.SelectedValue), txtOtherStateFour.Text.Trim(), Phone5, CheckZIP(txtZIPFour.Text.Trim()), LegalID, ddlCountryYearFour.SelectedValue.ConvertToShortInt()); //004-sooraj
            ResetBanking(spnmore3, txtAddressYearFour, txtCityFour, txtContactNameFour, txtFinancialInstitutionYearFour, txtOtherStateFour, txtRefFourPhno1, txtRefFourPhno2, txtRefFourPhno3, txtZIPThree, ddlStateYearFour, rdlLineofCreditFour, trOtherStateFour, ftxtZipFour, regvZipFour);
        }

        bool? Credit6 = GetBooleanValue(rdlLineofCreditFive);
        string Phone6 = objCommon.CheckPhone(txtRefFivePhno1.Text.Trim(), txtRefFivePhno2.Text.Trim(), txtRefFivePhno3.Text.Trim(), txtRefFivePhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearFive.Text.Trim()))) || (Phone6 != "") || (rdlLineofCreditFive.SelectedIndex >= 0) || (ddlStateYearFive.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameFive.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearFive.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityFive.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPFive.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearFive.Text.Trim(), txtCityFive.Text.Trim(), txtContactNameFive.Text.Trim(), Credit6, txtFinancialInstitutionYearFive.Text.Trim(), Convert.ToInt16(ddlStateYearFive.SelectedValue), txtOtherStateFive.Text.Trim(), Phone6, CheckZIP(txtZIPFive.Text.Trim()), LegalID, ddlCountryYearFive.SelectedValue.ConvertToShortInt()); //004-sooraj
            ResetBanking(spnmore4, txtAddressYearFive, txtCityFive, txtContactNameFive, txtFinancialInstitutionYearFive, txtOtherStateFive, txtRefFivePhno1, txtRefFivePhno2, txtRefFivePhno3, txtZIPFive, ddlStateYearFive, rdlLineofCreditFive, trOtherStateFive, ftxtZipFive, regvZipFive);
        }

        bool? Credit7 = GetBooleanValue(rdlLineofCreditSix);
        string Phone7 = objCommon.CheckPhone(txtRefSixPhno1.Text.Trim(), txtRefSixPhno2.Text.Trim(), txtRefSixPhno3.Text.Trim(), txtRefSixPhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearSix.Text.Trim()))) || (Phone7 != "") || (rdlLineofCreditSix.SelectedIndex >= 0) || (ddlStateYearSix.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameSix.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearSix.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCitySix.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPSix.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearSix.Text.Trim(), txtCitySix.Text.Trim(), txtContactNameSix.Text.Trim(), Credit7, txtFinancialInstitutionYearSix.Text.Trim(), Convert.ToInt16(ddlStateYearSix.SelectedValue), txtOtherStateSix.Text.Trim(), Phone7, CheckZIP(txtZIPSix.Text.Trim()), LegalID, ddlCountryYearSix.SelectedValue.ConvertToShortInt()); //004-sooraj
            ResetBanking(spnmore5, txtAddressYearSix, txtCitySix, txtContactNameSix, txtFinancialInstitutionYearSix, txtOtherStateSix, txtRefSixPhno1, txtRefSixPhno2, txtRefSixPhno3, txtZIPSix, ddlStateYearSix, rdlLineofCreditSix, trOtherStateSix, ftxtZipSix, regvZipSix);
        }

        bool? Credit8 = GetBooleanValue(rdlLineofCreditSeven);
        string Phone8 = objCommon.CheckPhone(txtRefSevenPhno1.Text.Trim(), txtRefSevenPhno2.Text.Trim(), txtRefSevenPhno3.Text.Trim(), txtRefSevenPhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearSeven.Text.Trim()))) || (Phone8 != "") || (rdlLineofCreditSeven.SelectedIndex >= 0) || (ddlStateYearSeven.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameSeven.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearSeven.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCitySeven.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPSeven.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearSeven.Text.Trim(), txtCitySeven.Text.Trim(), txtContactNameSeven.Text.Trim(), Credit8, txtFinancialInstitutionYearSeven.Text.Trim(), Convert.ToInt16(ddlStateYearSeven.SelectedValue), txtOtherStateSeven.Text.Trim(), Phone8, CheckZIP(txtZIPSeven.Text.Trim()), LegalID, ddlCountryYearSeven.SelectedValue.ConvertToShortInt()); //004-sooraj
            ResetBanking(spnmore6, txtAddressYearSeven, txtCitySeven, txtContactNameSeven, txtFinancialInstitutionYearSeven, txtOtherStateSeven, txtRefSevenPhno1, txtRefSevenPhno2, txtRefSevenPhno3, txtZIPSeven, ddlStateYearSeven, rdlLineofCreditSeven, trOtherStateSeven, ftxtZipSeven, regvZipSeven);
        }

        bool? Credit9 = GetBooleanValue(rdlLineofCreditEight);
        string Phone9 = objCommon.CheckPhone(txtRefEightPhno1.Text.Trim(), txtRefEightPhno2.Text.Trim(), txtRefEightPhno3.Text.Trim(), txtRefEightPhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearEight.Text.Trim()))) || (Phone9 != "") || (rdlLineofCreditEight.SelectedIndex >= 0) || (ddlStateYearEight.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameEight.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearEight.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityEight.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPEight.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearEight.Text.Trim(), txtCityEight.Text.Trim(), txtContactNameEight.Text.Trim(), Credit9, txtFinancialInstitutionYearEight.Text.Trim(), Convert.ToInt16(ddlStateYearEight.SelectedValue), txtOtherStateEight.Text.Trim(), Phone9, CheckZIP(txtZIPEight.Text.Trim()), LegalID, ddlCountryYearEight.SelectedValue.ConvertToShortInt()); //004-sooraj
            ResetBanking(spnmore7, txtAddressYearEight, txtCityEight, txtContactNameEight, txtFinancialInstitutionYearEight, txtOtherStateEight, txtRefEightPhno1, txtRefEightPhno2, txtRefEightPhno3, txtZIPEight, ddlStateYearEight, rdlLineofCreditEight, trOtherStateEight, ftxtzipEight, regvZipEight);
        }

        bool? Credit10 = GetBooleanValue(rdlLineofCreditNine);
        string Phone10 = objCommon.CheckPhone(txtRefNinePhno1.Text.Trim(), txtRefNinePhno2.Text.Trim(), txtRefNinePhno3.Text.Trim(), txtRefNinePhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearNine.Text.Trim()))) || (Phone10 != "") || (rdlLineofCreditNine.SelectedIndex >= 0) || (ddlStateYearNine.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameNine.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearNine.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityNine.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPNine.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearNine.Text.Trim(), txtCityNine.Text.Trim(), txtContactNameNine.Text.Trim(), Credit10, txtFinancialInstitutionYearNine.Text.Trim(), Convert.ToInt16(ddlStateYearNine.SelectedValue), txtOtherStateNine.Text.Trim(), Phone10, CheckZIP(txtZIPNine.Text.Trim()), LegalID, ddlCountryYearNine.SelectedValue.ConvertToShortInt()); //004-sooraj
            ResetBanking(spnmore8, txtAddressYearNine, txtCityNine, txtContactNameNine, txtFinancialInstitutionYearNine, txtOtherStateNine, txtRefNinePhno1, txtRefNinePhno2, txtRefNinePhno3, txtZIPNine, ddlStateYearNine, rdlLineofCreditNine, trOtherStateNine, ftxtZipNine, regvZipNine);
        }
    }

    /// <summary>
    /// Insert banking for save and next
    /// </summary>
    private void InsertBanking()
    {
        LegalID = Convert.ToInt64(hdnLegalId.Value);
        string Phone1 = objCommon.Phoneformat(txtMainPhno1.Text.Trim(), txtMainPhno2.Text.Trim(), txtMainPhno3.Text.Trim(), txtMainPhno3);//006
        bool? Credit1 = GetBooleanValue(rdlLineofCreditOne);
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearOne.Text.Trim()))) || (Phone1 != "") || (rdlLineofCreditOne.SelectedIndex >= 0) || (ddlStateYearOne.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameOne.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearOne.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityOne.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPOne.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearOne.Text.Trim(), txtCityOne.Text.Trim(), txtContactNameOne.Text.Trim(), Credit1, txtFinancialInstitutionYearOne.Text.Trim(), Convert.ToInt16(ddlStateYearOne.SelectedValue), txtOtherState.Text.Trim(), Phone1, txtZIPOne.Text.Trim(), LegalID, ddlCountryYearOne.SelectedValue.ConvertToShortInt()); //004-sooraj
        }

        bool? Credit2 = GetBooleanValue(rdlLineofCreditOne1);
        string Phone2 = objCommon.Phoneformat(txtRefOne1Phno1.Text.Trim(), txtRefOne1Phno2.Text.Trim(), txtRefOne1Phno3.Text.Trim(), txtRefOne1Phno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearOne1.Text.Trim()))) || (Phone2 != "") || (rdlLineofCreditOne1.SelectedIndex >= 0) || (ddlStateYearOne1.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameOne1.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearOne1.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityOne1.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPOne1.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearOne1.Text.Trim(), txtCityOne1.Text.Trim(), txtContactNameOne1.Text.Trim(), Credit2, txtFinancialInstitutionYearOne1.Text.Trim(), Convert.ToInt16(ddlStateYearOne1.SelectedValue), txtOtherState1.Text.Trim(), Phone2, txtZIPOne1.Text.Trim(), LegalID, ddlCountryYearOne1.SelectedValue.ConvertToShortInt()); //004-sooraj
        }

        bool? Credit3 = GetBooleanValue(rdlLineofCreditTwo);
        string Phone3 = objCommon.Phoneformat(txtReftwoPhno1.Text.Trim(), txtReftwoPhno2.Text.Trim(), txtReftwoPhno3.Text.Trim(), txtReftwoPhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearTwo.Text.Trim()))) || (Phone3 != "") || (rdlLineofCreditTwo.SelectedIndex >= 0) || (ddlStateYearTwo.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameTwo.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearTwo.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityTwo.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPTwo.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearTwo.Text.Trim(), txtCityTwo.Text.Trim(), txtContactNameTwo.Text.Trim(), Credit3, txtFinancialInstitutionYearTwo.Text.Trim(), Convert.ToInt16(ddlStateYearTwo.SelectedValue), txtOtherStateTwo.Text.Trim(), Phone3, txtZIPTwo.Text.Trim(), LegalID, ddlCountryYearTwo.SelectedValue.ConvertToShortInt()); //004-sooraj

        }

        bool? Credit4 = GetBooleanValue(rdlLineofCreditThree);
        string Phone4 = objCommon.Phoneformat(txtRefthreePhno1.Text.Trim(), txtRefthreePhno2.Text.Trim(), txtRefthreePhno3.Text.Trim(), txtRefthreePhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearThree.Text.Trim()))) || (Phone4 != "") || (rdlLineofCreditThree.SelectedIndex >= 0) || (ddlStateYearThree.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameThree.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearThree.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityThree.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPThree.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearThree.Text.Trim(), txtCityThree.Text.Trim(), txtContactNameThree.Text.Trim(), Credit4, txtFinancialInstitutionYearThree.Text.Trim(), Convert.ToInt16(ddlStateYearThree.SelectedValue), txtOtherStateThree.Text.Trim(), Phone4, txtZIPThree.Text.Trim(), LegalID, ddlCountryYearThree.SelectedValue.ConvertToShortInt()); //004-sooraj

        }

        bool? Credit5 = GetBooleanValue(rdlLineofCreditFour);
        string Phone5 = objCommon.Phoneformat(txtRefFourPhno1.Text.Trim(), txtRefFourPhno2.Text.Trim(), txtRefFourPhno3.Text.Trim(), txtRefFourPhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearFour.Text.Trim()))) || (Phone5 != "") || (rdlLineofCreditFour.SelectedIndex >= 0) || (ddlStateYearFour.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameFour.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearFour.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityFour.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPFour.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearFour.Text.Trim(), txtCityFour.Text.Trim(), txtContactNameFour.Text.Trim(), Credit5, txtFinancialInstitutionYearFour.Text.Trim(), Convert.ToInt16(ddlStateYearFour.SelectedValue), txtOtherStateFour.Text.Trim(), Phone5, txtZIPFour.Text.Trim(), LegalID, ddlCountryYearFour.SelectedValue.ConvertToShortInt()); //004-sooraj

        }

        bool? Credit6 = GetBooleanValue(rdlLineofCreditFive);
        string Phone6 = objCommon.Phoneformat(txtRefFivePhno1.Text.Trim(), txtRefFivePhno2.Text.Trim(), txtRefFivePhno3.Text.Trim(), txtRefFivePhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearFive.Text.Trim()))) || (Phone6 != "") || (rdlLineofCreditFive.SelectedIndex >= 0) || (ddlStateYearFive.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameFive.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearFive.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityFive.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPFive.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearFive.Text.Trim(), txtCityFive.Text.Trim(), txtContactNameFive.Text.Trim(), Credit6, txtFinancialInstitutionYearFive.Text.Trim(), Convert.ToInt16(ddlStateYearFive.SelectedValue), txtOtherStateFive.Text.Trim(), Phone6, txtZIPFive.Text.Trim(), LegalID, ddlCountryYearFive.SelectedValue.ConvertToShortInt()); //004-sooraj

        }

        bool? Credit7 = GetBooleanValue(rdlLineofCreditSix);
        string Phone7 = objCommon.Phoneformat(txtRefSixPhno1.Text.Trim(), txtRefSixPhno2.Text.Trim(), txtRefSixPhno3.Text.Trim(), txtRefSixPhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearSix.Text.Trim()))) || (Phone7 != "") || (rdlLineofCreditSix.SelectedIndex >= 0) || (ddlStateYearSix.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameSix.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearSix.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCitySix.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPSix.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearSix.Text.Trim(), txtCitySix.Text.Trim(), txtContactNameSix.Text.Trim(), Credit7, txtFinancialInstitutionYearSix.Text.Trim(), Convert.ToInt16(ddlStateYearSix.SelectedValue), txtOtherStateSix.Text.Trim(), Phone7, txtZIPSix.Text.Trim(), LegalID, ddlCountryYearSix.SelectedValue.ConvertToShortInt()); //004-sooraj

        }

        bool? Credit8 = GetBooleanValue(rdlLineofCreditSeven);

        string Phone8 = objCommon.Phoneformat(txtRefSevenPhno1.Text.Trim(), txtRefSevenPhno2.Text.Trim(), txtRefSevenPhno3.Text.Trim(), txtRefSevenPhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearSeven.Text.Trim()))) || (Phone8 != "") || (rdlLineofCreditSeven.SelectedIndex >= 0) || (ddlStateYearSeven.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameSeven.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearSeven.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCitySeven.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPSeven.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearSeven.Text.Trim(), txtCitySeven.Text.Trim(), txtContactNameSeven.Text.Trim(), Credit8, txtFinancialInstitutionYearSeven.Text.Trim(), Convert.ToInt16(ddlStateYearSeven.SelectedValue), txtOtherStateSeven.Text.Trim(), Phone8, txtZIPSeven.Text.Trim(), LegalID, ddlCountryYearSeven.SelectedValue.ConvertToShortInt()); //004-sooraj

        }

        bool? Credit9 = GetBooleanValue(rdlLineofCreditEight);

        string Phone9 = objCommon.Phoneformat(txtRefEightPhno1.Text.Trim(), txtRefEightPhno2.Text.Trim(), txtRefEightPhno3.Text.Trim(), txtRefEightPhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearEight.Text.Trim()))) || (Phone9 != "") || (rdlLineofCreditEight.SelectedIndex >= 0) || (ddlStateYearEight.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameEight.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearEight.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityEight.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPEight.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearEight.Text.Trim(), txtCityEight.Text.Trim(), txtContactNameEight.Text.Trim(), Credit9, txtFinancialInstitutionYearEight.Text.Trim(), Convert.ToInt16(ddlStateYearEight.SelectedValue), txtOtherStateEight.Text.Trim(), Phone9, txtZIPEight.Text.Trim(), LegalID, ddlCountryYearEight.SelectedValue.ConvertToShortInt()); //004-sooraj

        }

        bool? Credit10 = GetBooleanValue(rdlLineofCreditNine);

        string Phone10 = objCommon.Phoneformat(txtRefNinePhno1.Text.Trim(), txtRefNinePhno2.Text.Trim(), txtRefNinePhno3.Text.Trim(), txtRefNinePhno3);//006
        if ((!(string.IsNullOrEmpty(txtFinancialInstitutionYearNine.Text.Trim()))) || (Phone10 != "") || (rdlLineofCreditNine.SelectedIndex >= 0) || (ddlStateYearNine.SelectedIndex > 0) || (!(string.IsNullOrEmpty(txtContactNameNine.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAddressYearNine.Text.Trim()))) || (!(string.IsNullOrEmpty(txtCityNine.Text.Trim()))) || (!(string.IsNullOrEmpty(txtZIPNine.Text.Trim()))))
        {
            bool result = objLegal.InsertLegalBanking(txtAddressYearNine.Text.Trim(), txtCityNine.Text.Trim(), txtContactNameNine.Text.Trim(), Credit10, txtFinancialInstitutionYearNine.Text.Trim(), Convert.ToInt16(ddlStateYearNine.SelectedValue), txtOtherStateNine.Text.Trim(), Phone10, txtZIPNine.Text.Trim(), LegalID, ddlCountryYearNine.SelectedValue.ConvertToShortInt()); //004-sooraj

        }
    }



    /// <summary>
    /// Insert Financial 
    /// </summary>
    private void InsertFinancial()
    {
        LegalID = Convert.ToInt64(hdnLegalId.Value);
        objLegal.InsertLegalFinancial(txtAnuualCompRevenueYearOne.Text.Trim(), txtMaxContractValueYearOne.Text.Trim(), txtYearOne.Text.Trim(), LegalID, chkNAYear1.Checked, txtExplainYear1.Text.Trim());
        objLegal.InsertLegalFinancial(txtAnuualCompRevenueYearTwo.Text.Trim(), txtMaxContractValueYearTwo.Text.Trim(), txtYearTwo.Text.Trim(), LegalID, chkNAYear2.Checked, txtExplainYesr2.Text.Trim());
        objLegal.InsertLegalFinancial(txtAnuualCompRevenueYearThree.Text.Trim(), txtMaxContractValueYearThree.Text.Trim(), txtYearThree.Text.Trim(), LegalID, chkNAYear3.Checked, txtExplainYear3.Text.Trim());
        //objLegal.InsertLegalFinancial(txtAnuualCompRevenueYear1.Text.Trim(), txtMaxContractValueYearThree.Text.Trim(), txtYearThree.Text.Trim(), LegalID, chkNAYear3.Checked, txtExplainYear3.Text.Trim());

        //013 - need to save year 4 if year one's N/A box is checked even if year for values are empty.
        if ((!(string.IsNullOrEmpty(txtYear1.Text.Trim()))) && (!(string.IsNullOrEmpty(txtMaxContractValueYear1.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAnuualCompRevenueYear1.Text.Trim()))) || ((chkNAYear4.Checked)) || (!(string.IsNullOrEmpty(txtExplainYear4.Text.Trim()))))
        {
            objLegal.InsertLegalFinancial(txtAnuualCompRevenueYear1.Text.Trim(), txtMaxContractValueYear1.Text.Trim(), txtYear1.Text.Trim(), LegalID, chkNAYear4.Checked, txtExplainYear4.Text.Trim());
        }
        else
        {
            //013
            if (chkNAYear1.Checked)
                objLegal.InsertLegalFinancial(txtAnuualCompRevenueYear1.Text.Trim(), txtMaxContractValueYear1.Text.Trim(), txtYear1.Text.Trim(), LegalID, chkNAYear4.Checked, txtExplainYear4.Text.Trim());
        }

        if ((!(string.IsNullOrEmpty(txtYear2.Text.Trim()))) && (!(string.IsNullOrEmpty(txtMaxContractValueYear2.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAnuualCompRevenueYear2.Text.Trim()))) || ((chkNAYear5.Checked)) || (!(string.IsNullOrEmpty(txtExplainYear5.Text.Trim()))))
        {
            objLegal.InsertLegalFinancial(txtAnuualCompRevenueYear2.Text.Trim(), txtMaxContractValueYear2.Text.Trim(), txtYear2.Text.Trim(), LegalID, chkNAYear5.Checked, txtExplainYear5.Text.Trim());
        }

        if ((!(string.IsNullOrEmpty(txtYear3.Text.Trim()))) && (!(string.IsNullOrEmpty(txtMaxContractValueYear3.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAnuualCompRevenueYear3.Text.Trim()))) || ((chkNAYear6.Checked)) || (!(string.IsNullOrEmpty(txtExplainYear6.Text.Trim()))))
        {
            objLegal.InsertLegalFinancial(txtAnuualCompRevenueYear3.Text.Trim(), txtMaxContractValueYear3.Text.Trim(), txtYear3.Text.Trim(), LegalID, chkNAYear6.Checked, txtExplainYear6.Text.Trim());
        }

        if ((!(string.IsNullOrEmpty(txtYear4.Text.Trim()))) && (!(string.IsNullOrEmpty(txtMaxContractValueYear4.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAnuualCompRevenueYear4.Text.Trim()))) || ((chkNAYear7.Checked)) || (!(string.IsNullOrEmpty(txtExplainYear7.Text.Trim()))))
        {
            objLegal.InsertLegalFinancial(txtAnuualCompRevenueYear4.Text.Trim(), txtMaxContractValueYear4.Text.Trim(), txtYear4.Text.Trim(), LegalID, chkNAYear7.Checked, txtExplainYear7.Text.Trim());
        }

        if ((!(string.IsNullOrEmpty(txtYear5.Text.Trim()))) && (!(string.IsNullOrEmpty(txtMaxContractValueYear5.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAnuualCompRevenueYear5.Text.Trim()))) || ((chkNAYear8.Checked)) || (!(string.IsNullOrEmpty(txtExplainYear8.Text.Trim()))))
        {
            objLegal.InsertLegalFinancial(txtAnuualCompRevenueYear5.Text.Trim(), txtMaxContractValueYear5.Text.Trim(), txtYear5.Text.Trim(), LegalID, chkNAYear8.Checked, txtExplainYear8.Text.Trim());
        }

        if ((!(string.IsNullOrEmpty(txtYear6.Text.Trim()))) && (!(string.IsNullOrEmpty(txtMaxContractValueYear6.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAnuualCompRevenueYear6.Text.Trim()))) || ((chkNAYear9.Checked)) || (!(string.IsNullOrEmpty(txtExplainYear9.Text.Trim()))))
        {
            objLegal.InsertLegalFinancial(txtAnuualCompRevenueYear6.Text.Trim(), txtMaxContractValueYear6.Text.Trim(), txtYear6.Text.Trim(), LegalID, chkNAYear9.Checked, txtExplainYear9.Text.Trim());
        }

        if ((!(string.IsNullOrEmpty(txtYear7.Text.Trim()))) && (!(string.IsNullOrEmpty(txtMaxContractValueYear7.Text.Trim()))) || (!(string.IsNullOrEmpty(txtAnuualCompRevenueYear7.Text.Trim()))) || ((chkNAYear10.Checked)) || (!(string.IsNullOrEmpty(txtExplainYear10.Text.Trim()))))
        {
            objLegal.InsertLegalFinancial(txtAnuualCompRevenueYear7.Text.Trim(), txtMaxContractValueYear7.Text.Trim(), txtYear7.Text.Trim(), LegalID, chkNAYear10.Checked, txtExplainYear10.Text.Trim());
        }


    }

    /// <summary>
    /// Validate page and list the validation errors
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                errmsg += "<li>" + validator.ErrorMessage + "</li>";
            }
        }
        return errmsg;
    }

    /// <summary>
    /// Load edit data
    /// </summary>
    /// <param name="IsMyProfile"></param>
    private void LoadEditDatas(bool IsMyProfile)
    {
        //VendorId = 1;
        // VendorId = Convert.ToInt64(Session["VendorId"]);
        int CountLegal = objLegal.GetLegalCount(VendorId);
        if (CountLegal > 0)
        {
            EditData = objLegal.GetSingleVQLData(VendorId);
            ViewState["EditStatus"] = EditData.EditStatus;
            if (EditData.EditStatus == 1)
            {
                //txtExplainYear3.ReadOnly = true;
                //txtExplainYesr2.ReadOnly = true;
                //foreach (Control ctrl in pnlContent.Controls)
                //{
                //    if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                //    {
                //        CheckBox chk = (CheckBox)ctrl;
                //        chk.Enabled = false;
                //    }
                //    if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.Text.Trim()Box"))
                //    {
                //        TextBox txt = (TextBox)ctrl;
                //        txt.ReadOnly = true;
                //    }
                //    if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
                //    {
                //        LinkButton lnk = (LinkButton)ctrl;
                //        lnk.Visible = false;
                //        lnkYearAddMore.Visible = true;
                //    }
                //}
            }
            hdnLegalId.Value = Convert.ToString(EditData.PK_LegalID);
            bool? legalComplete = EditData.Legal1;
            if (legalComplete != null)
            {
                if (legalComplete == true)
                {
                    rdlListOne.SelectedIndex = 0;
                    txtWorkAwd.Text = EditData.ExplainLegal1;
                    trWorkAwdOne.Style["display"] = "";
                }
                else
                {
                    rdlListOne.SelectedIndex = 1;
                    trWorkAwdOne.Style["display"] = "none";
                }
            }
            bool? legalOwner = EditData.Legal2;
            if (legalOwner != null)
            {
                if (legalOwner == true)
                {
                    rdlListTwo.SelectedIndex = 0;
                    txtStake.Text = EditData.ExplainLegal2;
                    trStock.Style["display"] = "";
                }
                else
                {
                    rdlListTwo.SelectedIndex = 1;
                    trStock.Style["display"] = "none";
                }
            }
            bool? legalBankRupts = EditData.Legal3;
            if (legalBankRupts != null)
            {
                if (legalBankRupts == true)
                {
                    rdlListThree.SelectedIndex = 0;
                    txtBankruptcy.Text = EditData.ExplainLegal3;
                    trBankruptcy.Style["display"] = "";
                }
                else
                {
                    rdlListThree.SelectedIndex = 1;
                    trBankruptcy.Style["display"] = "none";
                }
            }

            bool? legalCriminal = EditData.Legal4;
            if (legalCriminal != null)
            {
                if (legalCriminal == true)
                {
                    rdlListFour.SelectedIndex = 0;
                    txtRisk.Text = EditData.ExplainLegal4;
                    trConviction.Style["display"] = "";
                }
                else
                {
                    rdlListFour.SelectedIndex = 1;
                    trConviction.Style["display"] = "none";
                }
            }

            ddlCurrency.SelectedValue = EditData.FK_CurrencyID.ConvertToString();//003
            rdbDenominateInDollars.SelectedValue = EditData.IsDenominateInDollar.ConvertToString();//003

            txtCurrentProjectedRevenue.Text = EditData.CurrentYearProjectedRevenue;
            txtCurrentTotalBacking.Text = EditData.CurrentTotalBacklog;

            LegalID = EditData.PK_LegalID;
            string Currentyear;
            if (DateTime.Now >= Convert.ToDateTime("05/01/" + Convert.ToString(DateTime.Now.Year)))
            {
                Currentyear = Convert.ToString(DateTime.Now.Year - 1);
            }
            else
            {
                Currentyear = Convert.ToString(DateTime.Now.Year - 2);
            }

            //LegalFinancialData = objLegal.GetLegalFinancialData(LegalID, Currentyear);
            objFinancial1 = objLegal.SelectLegalFinancial(LegalID, txtYearOne.Text.Trim());
            if (objFinancial1 != null)
            {
                chkNAYear1.Checked = Convert.ToBoolean(objFinancial1.NotAvailable);
                txtExplainYear1.Text = objFinancial1.Explanation;
                txtYearOne.Text = objFinancial1.Year;
                txtMaxContractValueYearOne.Text = objFinancial1.ContractValue;
                txtAnuualCompRevenueYearOne.Text = objFinancial1.AnnualRevenue;
            }
            objFinancial2 = objLegal.SelectLegalFinancial(LegalID, txtYearTwo.Text.Trim());
            if (objFinancial2 != null)
            {
                chkNAYear2.Checked = Convert.ToBoolean(objFinancial2.NotAvailable);
                txtExplainYesr2.Text = objFinancial2.Explanation;
                txtYearTwo.Text = objFinancial2.Year;
                txtMaxContractValueYearTwo.Text = objFinancial2.ContractValue;
                txtAnuualCompRevenueYearTwo.Text = objFinancial2.AnnualRevenue;
            }

            objFinancial3 = objLegal.SelectLegalFinancial(LegalID, txtYearThree.Text.Trim());
            if (objFinancial3 != null)
            {
                chkNAYear3.Checked = Convert.ToBoolean(objFinancial3.NotAvailable);
                txtExplainYear3.Text = objFinancial3.Explanation;
                txtYearThree.Text = objFinancial3.Year;
                txtMaxContractValueYearThree.Text = objFinancial3.ContractValue;
                txtAnuualCompRevenueYearThree.Text = objFinancial3.AnnualRevenue;
            }

            //005
            objFinancial4 = objLegal.SelectLegalFinancial(LegalID, txtYear1.Text.Trim());
            if (objFinancial4 != null)
            {
                txtYear1.Text = objFinancial4.Year;
                chkNAYear4.Checked = Convert.ToBoolean(objFinancial4.NotAvailable);
                txtExplainYear4.Text = objFinancial4.Explanation;
                txtMaxContractValueYear1.Text = objFinancial4.ContractValue;
                txtAnuualCompRevenueYear1.Text = objFinancial4.AnnualRevenue;
            }
            if (chkNAYear1.Checked)
            {
                spnYearMore1.Style["display"] = "";
            }
            else
            {
                spnYearMore1.Style["display"] = "none";
            }

            objFinancial5 = objLegal.SelectLegalFinancial(LegalID, txtYear2.Text.Trim());
            if (objFinancial5 != null)
            {
                txtYear2.Text = objFinancial5.Year;
                chkNAYear5.Checked = Convert.ToBoolean(objFinancial5.NotAvailable);
                txtExplainYear5.Text = objFinancial5.Explanation;
                txtMaxContractValueYear2.Text = objFinancial5.ContractValue;
                txtAnuualCompRevenueYear2.Text = objFinancial5.AnnualRevenue;
                if ((txtYear2.Text.Trim() != "") || (txtMaxContractValueYear2.Text.Trim() != "") || (txtAnuualCompRevenueYear2.Text.Trim() != ""))
                {
                    spnYearMore2.Style["display"] = "";
                    hdnCount.Value = "2";
                }
            }

            objFinancial6 = objLegal.SelectLegalFinancial(LegalID, txtYear3.Text.Trim());
            if (objFinancial6 != null)
            {
                txtYear3.Text = objFinancial6.Year;
                chkNAYear6.Checked = Convert.ToBoolean(objFinancial6.NotAvailable);
                txtExplainYear6.Text = objFinancial6.Explanation;
                txtMaxContractValueYear3.Text = objFinancial6.ContractValue;
                txtAnuualCompRevenueYear3.Text = objFinancial6.AnnualRevenue;
                if ((txtYear3.Text.Trim() != "") || (txtMaxContractValueYear3.Text.Trim() != "") || (txtAnuualCompRevenueYear3.Text.Trim() != ""))
                {
                    spnYearMore3.Style["display"] = "";
                    hdnCount.Value = "3";
                }
            }

            objFinancial7 = objLegal.SelectLegalFinancial(LegalID, txtYear4.Text.Trim());
            if (objFinancial7 != null)
            {
                txtYear4.Text = objFinancial7.Year;
                chkNAYear7.Checked = Convert.ToBoolean(objFinancial7.NotAvailable);
                txtExplainYear7.Text = objFinancial7.Explanation;
                txtMaxContractValueYear4.Text = objFinancial7.ContractValue;
                txtAnuualCompRevenueYear4.Text = objFinancial7.AnnualRevenue;
                if ((txtYear4.Text.Trim() != "") || (txtMaxContractValueYear4.Text.Trim() != "") || (txtAnuualCompRevenueYear4.Text.Trim() != ""))
                {
                    spnYearMore4.Style["display"] = "";
                    hdnCount.Value = "4";
                }
            }

            objFinancial8 = objLegal.SelectLegalFinancial(LegalID, txtYear5.Text.Trim());
            if (objFinancial8 != null)
            {
                txtYear5.Text = objFinancial8.Year;
                chkNAYear8.Checked = Convert.ToBoolean(objFinancial8.NotAvailable);
                txtExplainYear8.Text = objFinancial8.Explanation;
                txtMaxContractValueYear5.Text = objFinancial8.ContractValue;
                txtAnuualCompRevenueYear5.Text = objFinancial8.AnnualRevenue;
                if ((txtYear5.Text.Trim() != "") || (txtMaxContractValueYear5.Text.Trim() != "") || (txtAnuualCompRevenueYear5.Text.Trim() != ""))
                {
                    spnYearMore5.Style["display"] = "";
                    hdnCount.Value = "5";
                }
            }

            objFinancial9 = objLegal.SelectLegalFinancial(LegalID, txtYear6.Text.Trim());
            if (objFinancial9 != null)
            {
                txtYear6.Text = objFinancial9.Year;
                chkNAYear9.Checked = Convert.ToBoolean(objFinancial9.NotAvailable);
                txtExplainYear9.Text = objFinancial9.Explanation;
                txtMaxContractValueYear6.Text = objFinancial9.ContractValue;
                txtAnuualCompRevenueYear6.Text = objFinancial9.AnnualRevenue;
                if ((txtYear6.Text.Trim() != "") || (txtMaxContractValueYear6.Text.Trim() != "") || (txtAnuualCompRevenueYear6.Text.Trim() != ""))
                {
                    spnYearMore6.Style["display"] = "";
                    hdnCount.Value = "6";
                }
            }

            objFinancial10 = objLegal.SelectLegalFinancial(LegalID, txtYear7.Text.Trim());
            if (objFinancial10 != null)
            {
                txtYear7.Text = objFinancial10.Year;
                chkNAYear10.Checked = Convert.ToBoolean(objFinancial10.NotAvailable);
                txtExplainYear10.Text = objFinancial10.Explanation;
                txtMaxContractValueYear7.Text = objFinancial10.ContractValue;
                txtAnuualCompRevenueYear7.Text = objFinancial10.AnnualRevenue;
                if ((txtYear7.Text.Trim() != "") || (txtMaxContractValueYear7.Text.Trim() != "") || (txtAnuualCompRevenueYear7.Text.Trim() != ""))
                {
                    spnYearMore7.Style["display"] = "";
                    hdnCount.Value = "7";
                }
            }
            if (hdnCount.Value.Equals("7"))
            {
                lnkYearAddMore.Visible = false;
            }
        }
        if ((!string.IsNullOrEmpty(hdnVendorStatus.Value)) && (Convert.ToInt32(hdnVendorStatus.Value) > 0))
        {
            if (string.IsNullOrEmpty(txtMaxContractValueYearOne.Text) && string.IsNullOrEmpty(txtAnuualCompRevenueYearOne.Text) && string.IsNullOrEmpty(txtExplainYear1.Text))
            {
                chkNAYear1.Enabled = true;
            }
            if (string.IsNullOrEmpty(txtMaxContractValueYearTwo.Text) && string.IsNullOrEmpty(txtAnuualCompRevenueYearTwo.Text) && string.IsNullOrEmpty(txtExplainYesr2.Text))
            {
                chkNAYear2.Enabled = true;
            }
            if (string.IsNullOrEmpty(txtMaxContractValueYearThree.Text) && string.IsNullOrEmpty(txtAnuualCompRevenueYearThree.Text) && string.IsNullOrEmpty(txtExplainYear3.Text))
            {
                chkNAYear3.Enabled = true;
            }
            if (chkNAYear1.Checked)
            {
                txtMaxContractValueYearOne.Enabled = false;
                txtAnuualCompRevenueYearOne.Enabled = false;
            }
            else
            {
                txtMaxContractValueYearOne.Enabled = true;
                txtAnuualCompRevenueYearOne.Enabled = true;
            }
            if (chkNAYear2.Checked)
            {
                txtMaxContractValueYearTwo.Enabled = false;
                txtAnuualCompRevenueYearTwo.Enabled = false;
            }
            else
            {
                txtMaxContractValueYearTwo.Enabled = true;
                txtAnuualCompRevenueYearTwo.Enabled = true;
            }
            if (chkNAYear3.Checked)
            {
                txtMaxContractValueYearThree.Enabled = false;
                txtAnuualCompRevenueYearThree.Enabled = false;
            }
            else
            {
                txtMaxContractValueYearThree.Enabled = true;
                txtAnuualCompRevenueYearThree.Enabled = true;
            }
        }
        else
        {
            if (chkNAYear1.Checked)
            {
                txtMaxContractValueYearOne.Enabled = false;
                txtAnuualCompRevenueYearOne.Enabled = false;
            }
            else
            {
                txtMaxContractValueYearOne.Enabled = true;
                txtAnuualCompRevenueYearOne.Enabled = true;
            }
            if (chkNAYear2.Checked)
            {
                txtMaxContractValueYearTwo.Enabled = false;
                txtAnuualCompRevenueYearTwo.Enabled = false;
            }
            else
            {
                txtMaxContractValueYearTwo.Enabled = true;
                txtAnuualCompRevenueYearTwo.Enabled = true;
            }
            if (chkNAYear3.Checked)
            {
                txtMaxContractValueYearThree.Enabled = false;
                txtAnuualCompRevenueYearThree.Enabled = false;
            }
            else
            {
                txtMaxContractValueYearThree.Enabled = true;
                txtAnuualCompRevenueYearThree.Enabled = true;
            }
        }
        LegalBankingData = objLegal.GetLegalBankingData(LegalID);

        for (int i = 0; i <= LegalBankingData.Tables[0].Rows.Count - 1; i++)
        {
            switch (i)
            {
                case 0:
                    txtFinancialInstitutionYearOne.Text = LegalBankingData.Tables[0].Rows[0]["FinancialInstitution"].ToString();
                    string EstablishCredit = LegalBankingData.Tables[0].Rows[0]["Credit"].ToString();
                    if (EstablishCredit != "")
                    {
                        rdlLineofCreditOne.SelectedIndex = EstablishCredit.Equals("True") ? 0 : 1;
                    }
                    txtContactNameOne.Text = LegalBankingData.Tables[0].Rows[0]["ContactName"].ToString();
                  
                    txtAddressYearOne.Text = LegalBankingData.Tables[0].Rows[0]["Address"].ToString();
                    txtCityOne.Text = LegalBankingData.Tables[0].Rows[0]["City"].ToString();
                  
                    ddlCountryYearOne.SelectedValue = LegalBankingData.Tables[0].Rows[0]["FK_Country"].ConvertToString(); //004-Sooraj
                    BindAssociateState(ddlCountryYearOne);//004-Sooraj
                    ddlStateYearOne.SelectedValue = Convert.ToString(LegalBankingData.Tables[0].Rows[0]["FK_State"].ToString()); //004-Sooraj

                      string ContactPhone = LegalBankingData.Tables[0].Rows[0]["Phone"].ToString();
                    objCommon.loadphone(txtMainPhno1, txtMainPhno2, txtMainPhno3, ContactPhone);

                    if (ddlStateYearOne.SelectedItem.Text.Trim().Equals("Other"))
                    {
                        txtOtherState.Text = LegalBankingData.Tables[0].Rows[0]["OtherState"].ToString();
                    }
                    txtZIPOne.Text = LegalBankingData.Tables[0].Rows[0]["ZipCode"].ToString();
                    break;
                case 1:
                    txtFinancialInstitutionYearOne1.Text = LegalBankingData.Tables[0].Rows[1]["FinancialInstitution"].ToString();
                    string EstablishCredit2 = LegalBankingData.Tables[0].Rows[1]["Credit"].ToString();

                    if (EstablishCredit2 != "")
                    {
                        rdlLineofCreditOne1.SelectedIndex = EstablishCredit2.Equals("True") ? 0 : 1;
                    }
                    txtContactNameOne1.Text = LegalBankingData.Tables[0].Rows[1]["ContactName"].ToString();
                 
                    txtAddressYearOne1.Text = LegalBankingData.Tables[0].Rows[1]["Address"].ToString();
                    txtCityOne1.Text = LegalBankingData.Tables[0].Rows[1]["City"].ToString();
                   
                    ddlCountryYearOne1.SelectedValue = LegalBankingData.Tables[0].Rows[1]["FK_Country"].ConvertToString(); //004-Sooraj
                    BindAssociateState(ddlCountryYearOne1);//004-Sooraj
                    ddlStateYearOne1.SelectedValue = Convert.ToString(LegalBankingData.Tables[0].Rows[1]["FK_State"].ToString()); //004-Sooraj
                       string ContactPhone2 = LegalBankingData.Tables[0].Rows[1]["Phone"].ToString();
                    objCommon.loadphone(txtRefOne1Phno1, txtRefOne1Phno2, txtRefOne1Phno3, ContactPhone2);


                    if (ddlStateYearOne1.SelectedItem.Text.Trim().Equals("Other"))
                    {
                        txtOtherState1.Text = LegalBankingData.Tables[0].Rows[1]["OtherState"].ToString();
                    }
                    txtZIPOne1.Text = LegalBankingData.Tables[0].Rows[1]["ZipCode"].ToString();
                    break;
                case 2:
                    txtFinancialInstitutionYearTwo.Text = LegalBankingData.Tables[0].Rows[2]["FinancialInstitution"].ToString();
                    string EstablishCredit3 = LegalBankingData.Tables[0].Rows[2]["Credit"].ToString();
                    if (EstablishCredit3 != "")
                    {
                        rdlLineofCreditTwo.SelectedIndex = EstablishCredit3.Equals("True") ? 0 : 1;
                    }
                    txtContactNameTwo.Text = LegalBankingData.Tables[0].Rows[2]["ContactName"].ToString();
                 
                    txtAddressYearTwo.Text = LegalBankingData.Tables[0].Rows[2]["Address"].ToString();
                    txtCityTwo.Text = LegalBankingData.Tables[0].Rows[2]["City"].ToString();
                  
                    ddlCountryYearTwo.SelectedValue = LegalBankingData.Tables[0].Rows[2]["FK_Country"].ConvertToString(); //004-Sooraj
                    BindAssociateState(ddlCountryYearTwo);//004-Sooraj
                    ddlStateYearTwo.SelectedValue = Convert.ToString(LegalBankingData.Tables[0].Rows[2]["FK_State"].ToString()); //004-Sooraj
                       string ContactPhone3 = LegalBankingData.Tables[0].Rows[2]["Phone"].ToString();
                    objCommon.loadphone(txtReftwoPhno1, txtReftwoPhno2, txtReftwoPhno3, ContactPhone3);
                    if (ddlStateYearTwo.SelectedItem.Text.Trim().Equals("Other"))
                    {
                        txtOtherStateTwo.Text = LegalBankingData.Tables[0].Rows[2]["OtherState"].ToString();
                    }
                    txtZIPTwo.Text = LegalBankingData.Tables[0].Rows[2]["ZipCode"].ToString();
                    if ((txtFinancialInstitutionYearTwo.Text.Trim() != "") || (rdlLineofCreditTwo.SelectedIndex >= 0) || (txtContactNameTwo.Text.Trim() != "") || (ContactPhone3 != "") || (txtAddressYearTwo.Text.Trim() != "") || (txtCityTwo.Text.Trim() != "") || (ddlStateYearTwo.SelectedIndex > 0) || (txtZIPTwo.Text.Trim() != ""))
                    {
                        spnmore1.Style["display"] = "";
                        hdnCountValue.Value = "1";
                    }
                    break;
                case 3:
                    txtFinancialInstitutionYearThree.Text = LegalBankingData.Tables[0].Rows[3]["FinancialInstitution"].ToString();
                    string EstablishCredit4 = LegalBankingData.Tables[0].Rows[3]["Credit"].ToString();
                    if (EstablishCredit4 != "")
                    {
                        rdlLineofCreditThree.SelectedIndex = EstablishCredit4.Equals("True") ? 0 : 1;
                    }
                    txtContactNameThree.Text = LegalBankingData.Tables[0].Rows[3]["ContactName"].ToString();
                  
                    txtAddressYearThree.Text = LegalBankingData.Tables[0].Rows[3]["Address"].ToString();
                    txtCityThree.Text = LegalBankingData.Tables[0].Rows[3]["City"].ToString();
                  
                    ddlCountryYearThree.SelectedValue = LegalBankingData.Tables[0].Rows[3]["FK_Country"].ConvertToString(); //004-Sooraj
                    BindAssociateState(ddlCountryYearThree);//004-Sooraj
                    ddlStateYearThree.SelectedValue = Convert.ToString(LegalBankingData.Tables[0].Rows[3]["FK_State"].ToString()); //004-Sooraj

                      string ContactPhone4 = LegalBankingData.Tables[0].Rows[3]["Phone"].ToString();
                    objCommon.loadphone(txtRefthreePhno1, txtRefthreePhno2, txtRefthreePhno3, ContactPhone4);

                    if (ddlStateYearThree.SelectedItem.Text.Trim().Equals("Other"))
                    {
                        txtOtherStateThree.Text = LegalBankingData.Tables[0].Rows[3]["OtherState"].ToString();
                    }
                    txtZIPThree.Text = LegalBankingData.Tables[0].Rows[3]["ZipCode"].ToString();
                    if ((txtFinancialInstitutionYearThree.Text.Trim() != "") || (rdlLineofCreditThree.SelectedIndex >= 0) || (txtContactNameThree.Text.Trim() != "") || (ContactPhone4 != "") || (txtAddressYearThree.Text.Trim() != "") || (txtCityThree.Text.Trim() != "") || (ddlStateYearThree.SelectedIndex > 0) || (txtZIPThree.Text.Trim() != ""))
                    {
                        spnmore2.Style["display"] = "";
                        hdnCountValue.Value = "2";
                    }
                    break;
                case 4:
                    txtFinancialInstitutionYearFour.Text = LegalBankingData.Tables[0].Rows[4]["FinancialInstitution"].ToString();
                    string EstablishCredit5 = LegalBankingData.Tables[0].Rows[4]["Credit"].ToString();
                    if (EstablishCredit5 != "")
                    {
                        rdlLineofCreditFour.SelectedIndex = EstablishCredit5.Equals("True") ? 0 : 1;
                    }
                    txtContactNameFour.Text = LegalBankingData.Tables[0].Rows[4]["ContactName"].ToString();
                  
                    txtAddressYearFour.Text = LegalBankingData.Tables[0].Rows[4]["Address"].ToString();
                    txtCityFour.Text = LegalBankingData.Tables[0].Rows[4]["City"].ToString();
                  
                    ddlCountryYearFour.SelectedValue = LegalBankingData.Tables[0].Rows[4]["FK_Country"].ConvertToString(); //004-Sooraj
                    BindAssociateState(ddlCountryYearFour);//004-Sooraj
                    ddlStateYearFour.SelectedValue = Convert.ToString(LegalBankingData.Tables[0].Rows[4]["FK_State"].ToString()); //004-Sooraj

                      string ContactPhone5 = LegalBankingData.Tables[0].Rows[4]["Phone"].ToString();
                    objCommon.loadphone(txtRefFourPhno1, txtRefFourPhno2, txtRefFourPhno3, ContactPhone5);

                    if (ddlStateYearFour.SelectedItem.Text.Trim().Equals("Other"))
                    {
                        txtOtherStateFour.Text = LegalBankingData.Tables[0].Rows[4]["OtherState"].ToString();
                    }
                    txtZIPFour.Text = LegalBankingData.Tables[0].Rows[4]["ZipCode"].ToString();
                    if ((txtFinancialInstitutionYearFour.Text.Trim() != "") || (rdlLineofCreditFour.SelectedIndex >= 0) || (txtContactNameFour.Text.Trim() != "") || (ContactPhone5 != "") || (txtAddressYearFour.Text.Trim() != "") || (txtCityFour.Text.Trim() != "") || (ddlStateYearFour.SelectedIndex > 0) || (txtZIPFour.Text.Trim() != ""))
                    {
                        spnmore3.Style["display"] = "";
                        hdnCountValue.Value = "3";
                    }
                    break;
                case 5:
                    txtFinancialInstitutionYearFive.Text = LegalBankingData.Tables[0].Rows[5]["FinancialInstitution"].ToString();
                    string EstablishCredit6 = LegalBankingData.Tables[0].Rows[5]["Credit"].ToString();
                    if (EstablishCredit6 != "")
                    {
                        rdlLineofCreditFive.SelectedIndex = EstablishCredit6.Equals("True") ? 0 : 1;
                    }
                    txtContactNameFive.Text = LegalBankingData.Tables[0].Rows[5]["ContactName"].ToString();
                 
                    txtAddressYearFive.Text = LegalBankingData.Tables[0].Rows[5]["Address"].ToString();
                    txtCityFive.Text = LegalBankingData.Tables[0].Rows[5]["City"].ToString();
                  
                    ddlCountryYearFive.SelectedValue = LegalBankingData.Tables[0].Rows[5]["FK_Country"].ConvertToString(); //004-Sooraj
                    BindAssociateState(ddlCountryYearFive);//004-Sooraj
                    ddlStateYearFive.SelectedValue = Convert.ToString(LegalBankingData.Tables[0].Rows[5]["FK_State"].ToString()); //004-Sooraj

                       string ContactPhone6 = LegalBankingData.Tables[0].Rows[5]["Phone"].ToString();
                    objCommon.loadphone(txtRefFivePhno1, txtRefFivePhno2, txtRefFivePhno3, ContactPhone6);

                    if (ddlStateYearFive.SelectedItem.Text.Trim().Equals("Other"))
                    {
                        txtOtherStateFive.Text = LegalBankingData.Tables[0].Rows[5]["OtherState"].ToString();
                    }
                    txtZIPFive.Text = LegalBankingData.Tables[0].Rows[5]["ZipCode"].ToString();
                    if ((txtFinancialInstitutionYearFive.Text.Trim() != "") || (rdlLineofCreditFive.SelectedIndex >= 0) || (txtContactNameFive.Text.Trim() != "") || (ContactPhone6 != "") || (txtAddressYearFive.Text.Trim() != "") || (txtCityFive.Text.Trim() != "") || (ddlStateYearFive.SelectedIndex > 0) || (txtZIPFive.Text.Trim() != ""))
                    {
                        spnmore4.Style["display"] = "";
                        hdnCountValue.Value = "4";
                    }
                    break;
                case 6:
                    txtFinancialInstitutionYearSix.Text = LegalBankingData.Tables[0].Rows[6]["FinancialInstitution"].ToString();
                    string EstablishCredit7 = LegalBankingData.Tables[0].Rows[6]["Credit"].ToString();
                    if (EstablishCredit7 != "")
                    {
                        rdlLineofCreditSix.SelectedIndex = EstablishCredit7.Equals("True") ? 0 : 1;
                    }
                    txtContactNameSix.Text = LegalBankingData.Tables[0].Rows[6]["ContactName"].ToString();
                  
                    txtAddressYearSix.Text = LegalBankingData.Tables[0].Rows[6]["Address"].ToString();
                    txtCitySix.Text = LegalBankingData.Tables[0].Rows[6]["City"].ToString();
                   
                    ddlCountryYearSix.SelectedValue = LegalBankingData.Tables[0].Rows[6]["FK_Country"].ConvertToString(); //004-Sooraj
                    BindAssociateState(ddlCountryYearSix);//004-Sooraj
                    ddlStateYearSix.SelectedValue = Convert.ToString(LegalBankingData.Tables[0].Rows[6]["FK_State"].ToString()); //004-Sooraj

                      string ContactPhone7 = LegalBankingData.Tables[0].Rows[6]["Phone"].ToString();
                    objCommon.loadphone(txtRefSixPhno1, txtRefSixPhno2, txtRefSixPhno3, ContactPhone7);

                    if (ddlStateYearSix.SelectedItem.Text.Trim().Equals("Other"))
                    {
                        txtOtherStateSix.Text = LegalBankingData.Tables[0].Rows[6]["OtherState"].ToString();
                    }
                    txtZIPSix.Text = LegalBankingData.Tables[0].Rows[6]["ZipCode"].ToString();
                    if ((txtFinancialInstitutionYearSix.Text.Trim() != "") || (rdlLineofCreditSix.SelectedIndex >= 0) || (txtContactNameSix.Text.Trim() != "") || (ContactPhone7 != "") || (txtAddressYearSix.Text.Trim() != "") || (txtCitySix.Text.Trim() != "") || (ddlStateYearSix.SelectedIndex > 0) || (txtZIPSix.Text.Trim() != ""))
                    {
                        spnmore5.Style["display"] = "";
                        hdnCountValue.Value = "5";
                    }
                    break;
                case 7:
                    txtFinancialInstitutionYearSeven.Text = LegalBankingData.Tables[0].Rows[7]["FinancialInstitution"].ToString();
                    string EstablishCredit8 = LegalBankingData.Tables[0].Rows[7]["Credit"].ToString();
                    if (EstablishCredit8 != "")
                    {
                        rdlLineofCreditSeven.SelectedIndex = EstablishCredit8.Equals("True") ? 0 : 1;
                    }
                    txtContactNameSeven.Text = LegalBankingData.Tables[0].Rows[7]["ContactName"].ToString();
                
                    txtAddressYearSeven.Text = LegalBankingData.Tables[0].Rows[7]["Address"].ToString();
                    txtCitySeven.Text = LegalBankingData.Tables[0].Rows[7]["City"].ToString();
                  
                    ddlCountryYearSeven.SelectedValue = LegalBankingData.Tables[0].Rows[7]["FK_Country"].ConvertToString(); //004-Sooraj
                    BindAssociateState(ddlCountryYearSeven);//004-Sooraj
                    ddlStateYearSeven.SelectedValue = Convert.ToString(LegalBankingData.Tables[0].Rows[7]["FK_State"].ToString()); //004-Sooraj

                        string ContactPhone8 = LegalBankingData.Tables[0].Rows[7]["Phone"].ToString();
                    objCommon.loadphone(txtRefSevenPhno1, txtRefSevenPhno2, txtRefSevenPhno3, ContactPhone8);

                    if (ddlStateYearSeven.SelectedItem.Text.Trim().Equals("Other"))
                    {
                        txtOtherStateSeven.Text = LegalBankingData.Tables[0].Rows[7]["OtherState"].ToString();
                    }
                    txtZIPSeven.Text = LegalBankingData.Tables[0].Rows[7]["ZipCode"].ToString();
                    if ((txtFinancialInstitutionYearSeven.Text.Trim() != "") || (rdlLineofCreditSeven.SelectedIndex >= 0) || (txtContactNameSeven.Text.Trim() != "") || (ContactPhone8 != "") || (txtAddressYearSeven.Text.Trim() != "") || (txtCitySeven.Text.Trim() != "") || (ddlStateYearSeven.SelectedIndex > 0) || (txtZIPSeven.Text.Trim() != ""))
                    {
                        spnmore6.Style["display"] = "";
                        hdnCountValue.Value = "6";
                    }
                    break;
                case 8:
                    txtFinancialInstitutionYearEight.Text = LegalBankingData.Tables[0].Rows[8]["FinancialInstitution"].ToString();
                    string EstablishCredit9 = LegalBankingData.Tables[0].Rows[8]["Credit"].ToString();
                    if (EstablishCredit9 != "")
                    {
                        rdlLineofCreditEight.SelectedIndex = EstablishCredit9.Equals("True") ? 0 : 1;
                    }
                    txtContactNameEight.Text = LegalBankingData.Tables[0].Rows[8]["ContactName"].ToString();
                  
                    txtAddressYearEight.Text = LegalBankingData.Tables[0].Rows[8]["Address"].ToString();
                    txtCityEight.Text = LegalBankingData.Tables[0].Rows[8]["City"].ToString();

                    ddlCountryYearEight.SelectedValue = LegalBankingData.Tables[0].Rows[8]["FK_Country"].ConvertToString(); //004-Sooraj
                    BindAssociateState(ddlCountryYearEight);//004-Sooraj
                    ddlStateYearEight.SelectedValue = Convert.ToString(LegalBankingData.Tables[0].Rows[8]["FK_State"].ToString()); //004-Sooraj

                      string ContactPhone9 = LegalBankingData.Tables[0].Rows[8]["Phone"].ToString();
                    objCommon.loadphone(txtRefEightPhno1, txtRefEightPhno2, txtRefEightPhno3, ContactPhone9);

                    if (ddlStateYearEight.SelectedItem.Text.Trim().Equals("Other"))
                    {
                        txtOtherStateEight.Text = LegalBankingData.Tables[0].Rows[8]["OtherState"].ToString();
                    }
                    txtZIPEight.Text = LegalBankingData.Tables[0].Rows[8]["ZipCode"].ToString();
                    if ((txtFinancialInstitutionYearEight.Text.Trim() != "") || (rdlLineofCreditEight.SelectedIndex >= 0) || (txtContactNameEight.Text.Trim() != "") || (ContactPhone9 != "") || (txtAddressYearEight.Text.Trim() != "") || (txtCityEight.Text.Trim() != "") || (ddlStateYearEight.SelectedIndex > 0) || (txtZIPEight.Text.Trim() != ""))
                    {
                        spnmore7.Style["display"] = "";
                        hdnCountValue.Value = "7";
                    }
                    break;
                case 9:
                    txtFinancialInstitutionYearNine.Text = LegalBankingData.Tables[0].Rows[9]["FinancialInstitution"].ToString();

                    string EstablishCredit10 = LegalBankingData.Tables[0].Rows[9]["Credit"].ToString();
                    if (EstablishCredit10 != "")
                    {
                        rdlLineofCreditNine.SelectedIndex = EstablishCredit10.Equals("True") ? 0 : 1;
                    }
                    txtContactNameNine.Text = LegalBankingData.Tables[0].Rows[9]["ContactName"].ToString();
                 
                    txtAddressYearNine.Text = LegalBankingData.Tables[0].Rows[9]["Address"].ToString();
                    txtCityNine.Text = LegalBankingData.Tables[0].Rows[9]["City"].ToString();
                
                    ddlCountryYearNine.SelectedValue = LegalBankingData.Tables[0].Rows[9]["FK_Country"].ConvertToString(); //004-Sooraj
                    BindAssociateState(ddlCountryYearNine);//004-Sooraj
                    ddlStateYearNine.SelectedValue = Convert.ToString(LegalBankingData.Tables[0].Rows[9]["FK_State"].ToString()); //004-Sooraj

                    string ContactPhone10 = LegalBankingData.Tables[0].Rows[9]["Phone"].ToString();
                    objCommon.loadphone(txtRefNinePhno1, txtRefNinePhno2, txtRefNinePhno3, ContactPhone10);

                    if (ddlStateYearNine.SelectedItem.Text.Trim().Equals("Other"))
                    {
                        txtOtherStateNine.Text = LegalBankingData.Tables[0].Rows[9]["OtherState"].ToString();
                    }
                    txtZIPNine.Text = LegalBankingData.Tables[0].Rows[9]["ZipCode"].ToString();
                    if ((txtFinancialInstitutionYearNine.Text.Trim() != "") || (rdlLineofCreditNine.SelectedIndex >= 0) || (txtContactNameNine.Text.Trim() != "") || (ContactPhone10 != "") || (txtAddressYearNine.Text.Trim() != "") || (txtCityNine.Text.Trim() != "") || (ddlStateYearNine.SelectedIndex > 0) || (txtZIPNine.Text.Trim() != ""))
                    {
                        spnmore8.Style["display"] = "";
                        hdnCountValue.Value = "8";
                    }
                    break;
            }
        }

        if (hdnCountValue.Value.Equals("8"))
        {
            imgAddMore.Visible = false;
        }
    }

    #endregion

    #region Validation Events

    protected void cusphone6_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if ((txtMainPhno1.Text.Trim() == "") && ((txtMainPhno2.Text.Trim() == "") && (txtMainPhno3.Text.Trim() == "")))
        {
            //cusphone6.ErrorMessage = "Please enter contact phone number for financial institution one";
            args.IsValid = true;
        }
        else
        {
            bool isValid = objCommon.ValidatePhoneformat(txtMainPhno1.Text.Trim(), txtMainPhno2.Text.Trim(), txtMainPhno3.Text.Trim(), txtMainPhno3);//006
            if (!isValid)
            {
                cusphone6.ErrorMessage = "Please enter contact phone number in required format for financial institution one";
                args.IsValid = false;
            }
        }
    }

    protected void cusphone7_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if ((txtRefOne1Phno1.Text.Trim() != "") || ((txtRefOne1Phno2.Text.Trim() != "") || (txtRefOne1Phno3.Text.Trim() != "")))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtRefOne1Phno1.Text.Trim(), txtRefOne1Phno2.Text.Trim(), txtRefOne1Phno3.Text.Trim(), txtRefOne1Phno3);//006
            if (!isValid)
            {
                cusphone7.ErrorMessage = "Please enter contact phone number in required format for financial institution two";
                args.IsValid = false;
            }
        }
    }

    protected void cusphone8_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if ((txtReftwoPhno1.Text.Trim() != "") || ((txtReftwoPhno2.Text.Trim() != "") || (txtReftwoPhno3.Text.Trim() != "")))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtReftwoPhno1.Text.Trim(), txtReftwoPhno2.Text.Trim(), txtReftwoPhno3.Text.Trim(), txtReftwoPhno3);//006
            if (!isValid)
            {
                cusphone8.ErrorMessage = "Please enter contact phone number in required format for financial institution three";
                args.IsValid = false;
            }
        }
    }

    protected void cusphone17_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if ((txtRefthreePhno1.Text.Trim() != "") || ((txtRefthreePhno2.Text.Trim() != "") || (txtRefthreePhno3.Text.Trim() != "")))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtRefthreePhno1.Text.Trim(), txtRefthreePhno2.Text.Trim(), txtRefthreePhno3.Text.Trim(), txtRefthreePhno3);//006
            if (!isValid)
            {
                cusphone17.ErrorMessage = "Please enter contact phone number in required format for financial institution four";
                args.IsValid = false;
            }
        }
    }

    protected void cusphone9_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if ((txtRefFourPhno1.Text.Trim() != "") || ((txtRefFourPhno2.Text.Trim() != "") || (txtRefFourPhno3.Text.Trim() != "")))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtRefFourPhno1.Text.Trim(), txtRefFourPhno2.Text.Trim(), txtRefFourPhno3.Text.Trim(), txtRefFourPhno3);//006
            if (!isValid)
            {
                cusphone9.ErrorMessage = "Please enter contact phone number in required format for financial institution five";
                args.IsValid = false;
            }
        }
    }

    protected void cusphone10_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if ((txtRefFivePhno1.Text.Trim() != "") || ((txtRefFivePhno2.Text.Trim() != "") || (txtRefFivePhno3.Text.Trim() != "")))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtRefFivePhno1.Text.Trim(), txtRefFivePhno2.Text.Trim(), txtRefFivePhno3.Text.Trim(), txtRefFivePhno3);//006
            if (!isValid)
            {
                cusphone10.ErrorMessage = "Please enter contact phone number in required format for financial institution six";
                args.IsValid = false;
            }
        }
    }

    protected void cusphone11_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if ((txtRefSixPhno1.Text.Trim() != "") || ((txtRefSixPhno2.Text.Trim() != "") || (txtRefSixPhno3.Text.Trim() != "")))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtRefSixPhno1.Text.Trim(), txtRefSixPhno2.Text.Trim(), txtRefSixPhno3.Text.Trim(), txtRefSixPhno3);//006
            if (!isValid)
            {
                cusphone11.ErrorMessage = "Please enter contact phone number in required format for financial institution seven";
                args.IsValid = false;
            }
        }
    }

    protected void cusphone12_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if ((!string.IsNullOrEmpty(txtRefSevenPhno1.Text.Trim())) || (!string.IsNullOrEmpty(txtRefSevenPhno2.Text.Trim())) || (!string.IsNullOrEmpty(txtRefSevenPhno3.Text.Trim())))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtRefSevenPhno1.Text.Trim(), txtRefSevenPhno2.Text.Trim(), txtRefSevenPhno3.Text.Trim(), txtRefSevenPhno3);//006
            if (!isValid)
            {
                cusphone12.ErrorMessage = "Please enter contact phone number in required format for financial institution eight";
                args.IsValid = false;
            }
        }
    }

    protected void cusphone13_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if ((!string.IsNullOrEmpty(txtRefEightPhno1.Text.Trim())) || (!string.IsNullOrEmpty(txtRefEightPhno2.Text.Trim())) || (!string.IsNullOrEmpty(txtRefEightPhno3.Text.Trim())))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtRefEightPhno1.Text.Trim(), txtRefEightPhno2.Text.Trim(), txtRefEightPhno3.Text.Trim(), txtRefEightPhno3);//006
            if (!isValid)
            {
                cusphone13.ErrorMessage = "Please enter contact phone number in required format for financial institution nine";
                args.IsValid = false;
            }
        }
    }

    protected void cusphone14_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if ((!string.IsNullOrEmpty(txtRefNinePhno1.Text.Trim())) || (!string.IsNullOrEmpty(txtRefNinePhno2.Text.Trim())) || (!string.IsNullOrEmpty(txtRefNinePhno3.Text.Trim())))
        {
            bool isValid = objCommon.ValidatePhoneformat(txtRefNinePhno1.Text.Trim(), txtRefNinePhno2.Text.Trim(), txtRefNinePhno3.Text.Trim(), txtRefNinePhno3);//006
            if (!isValid)
            {
                cusphone14.ErrorMessage = "Please enter contact phone number in required format for financial institution ten";
                args.IsValid = false;
            }
        }
    }



    protected void custStateOne1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateYearOne1.SelectedIndex > 0)
        {
            if (ddlStateYearOne1.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherState1.Text.Trim()))
                {
                    //cusStateOne1.ErrorMessage = "Please select a state for financial institution two";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void custState1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateYearOne.SelectedIndex > 0)
        {
            if (ddlStateYearOne.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherState.Text.Trim()))
                {
                   // cusState1.ErrorMessage = "Please enter a state for financial institution one";
                    args.IsValid = true;
                }
            }
        }
        else if (ddlStateYearOne.SelectedIndex <= 0)
        {
            //cusState1.ErrorMessage = "Please select a state for financial institution one";
            args.IsValid = true;
        }
    }

    protected void custState2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateYearTwo.SelectedIndex > 0)
        {
            if (ddlStateYearTwo.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateTwo.Text.Trim()))
                {
                    //cusState2.ErrorMessage = "Please enter state for financial institution three";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void custState3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateYearThree.SelectedIndex > 0)
        {
            if (ddlStateYearThree.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateThree.Text.Trim()))
                {
                   // cusState3.ErrorMessage = "Please enter state for financial institution four";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void custState4_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateYearFour.SelectedIndex > 0)
        {
            if (ddlStateYearFour.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateFour.Text.Trim()))
                {
                   // cusState4.ErrorMessage = "Please enter state for financial institution five";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void custState5_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateYearFive.SelectedIndex > 0)
        {
            if (ddlStateYearFive.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateFive.Text.Trim()))
                {
                   // cusState5.ErrorMessage = "Please enter state for financial institution six";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void custState6_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateYearSix.SelectedIndex > 0)
        {
            if (ddlStateYearSix.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateSix.Text.Trim()))
                {
                   // cusState6.ErrorMessage = "Please enter state for financial institution seven";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void custState7_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateYearSeven.SelectedIndex > 0)
        {
            if (ddlStateYearSeven.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateSeven.Text.Trim()))
                {
                   // cusState7.ErrorMessage = "Please enter state for financial institution eight";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void custState8_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateYearEight.SelectedIndex > 0)
        {
            if (ddlStateYearEight.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateEight.Text.Trim()))
                {
                   // cusState8.ErrorMessage = "Please enter state for financial institution nine";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void custState9_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlStateYearNine.SelectedIndex > 0)
        {
            if (ddlStateYearNine.SelectedItem.Text.Trim().Equals("Other"))
            {
                if (string.IsNullOrEmpty(txtOtherStateNine.Text.Trim()))
                {
                   // cusState9.ErrorMessage = "Please enter state for financial institution ten";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void cusvListOne_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rdlListOne.SelectedIndex >= 0)
        {
            if (rdlListOne.SelectedItem.Text.Trim().Equals("Yes"))
            {
                if (string.IsNullOrEmpty(txtWorkAwd.Text.Trim()))
                {
                    //cusvListOne.ErrorMessage = "Please enter explanation under legal one";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void cusvListTwo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rdlListTwo.SelectedIndex >= 0)
        {
            if (rdlListTwo.SelectedItem.Text.Trim().Equals("Yes"))
            {
                if (string.IsNullOrEmpty(txtStake.Text.Trim()))
                {
                    //cusvListTwo.ErrorMessage = "Please enter explanation under legal two";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void cusvListThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rdlListThree.SelectedIndex >= 0)
        {
            if (rdlListThree.SelectedItem.Text.Trim().Equals("Yes"))
            {
                if (string.IsNullOrEmpty(txtBankruptcy.Text.Trim()))
                {
                    //cusvListThree.ErrorMessage = "Please enter explanation under legal three";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void cusvListFour_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rdlListFour.SelectedIndex >= 0)
        {
            if (rdlListFour.SelectedItem.Text.Trim().Equals("Yes"))
            {
                if (string.IsNullOrEmpty(txtRisk.Text.Trim()))
                {
                    //cusvListFour.ErrorMessage = "Please enter explanation under legal four";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void cusvLegalQuestion_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((rdlListFour.SelectedIndex < 0) || (rdlListOne.SelectedIndex < 0) || (rdlListTwo.SelectedIndex < 0) || (rdlListThree.SelectedIndex < 0))
        {
            //cusvLegalQuestion.ErrorMessage = "Please respond to all questions under legal";
            args.IsValid = true;
        }
    }

    protected void cusvYearOne_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNAYear1.Checked)
        {
            if (string.IsNullOrEmpty(txtExplainYear1.Text.Trim()))
            {
                //cusvYearOne.ErrorMessage = "Please enter the explanation under year one";
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtMaxContractValueYearOne.Text.Trim()) && string.IsNullOrEmpty(txtAnuualCompRevenueYearOne.Text.Trim()))
            {
                //cusvYearOne.ErrorMessage = "Please enter maximum contract value completed under year one";
                //cusvYearOne.ErrorMessage += "<li>Please enter annual company revenue under year one</li>";
                args.IsValid = true;
            }
            else if (string.IsNullOrEmpty(txtMaxContractValueYearOne.Text.Trim()) || string.IsNullOrEmpty(txtAnuualCompRevenueYearOne.Text.Trim()))
            {
                if (string.IsNullOrEmpty(txtMaxContractValueYearOne.Text.Trim()))
                {
                    //cusvYearOne.ErrorMessage = "Please enter maximum contract value completed under year one";
                    args.IsValid = true;
                }
                if (string.IsNullOrEmpty(txtAnuualCompRevenueYearOne.Text.Trim()))
                {
                    //cusvYearOne.ErrorMessage = "Please enter annual company revenue under year one";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void cusvYearTwo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNAYear2.Checked)
        {
            if (string.IsNullOrEmpty(txtExplainYesr2.Text.Trim()))
            {
                //cusvYearTwo.ErrorMessage = "Please enter the explanation under year two";
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtMaxContractValueYearTwo.Text.Trim()) && string.IsNullOrEmpty(txtAnuualCompRevenueYearTwo.Text.Trim()))
            {
                //cusvYearTwo.ErrorMessage = "Please enter maximum contract value completed under year two";
                //cusvYearTwo.ErrorMessage += "<li>Please enter annual company revenue under year two</li>";
                args.IsValid = true;
            }
            else if (string.IsNullOrEmpty(txtMaxContractValueYearTwo.Text.Trim()) || string.IsNullOrEmpty(txtAnuualCompRevenueYearTwo.Text.Trim()))
            {
                if (string.IsNullOrEmpty(txtMaxContractValueYearTwo.Text.Trim()))
                {
                   // cusvYearTwo.ErrorMessage = "Please enter maximum contract value completed under year two";
                    args.IsValid = true;
                }
                if (string.IsNullOrEmpty(txtAnuualCompRevenueYearTwo.Text.Trim()))
                {
                   // cusvYearTwo.ErrorMessage = "Please enter annual company revenue under year two";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void cusvYearThree_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNAYear3.Checked)
        {
            if (string.IsNullOrEmpty(txtExplainYear3.Text.Trim()))
            {
                //cusvYearThree.ErrorMessage = "Please enter the explanation under year three";
                args.IsValid = true;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtMaxContractValueYearThree.Text.Trim()) && string.IsNullOrEmpty(txtAnuualCompRevenueYearThree.Text.Trim()))
            {
               // cusvYearThree.ErrorMessage = "Please enter maximum contract value completed under year three";
                //cusvYearThree.ErrorMessage += "<li>Please enter annual company revenue under year three</li>";
                args.IsValid = true;
            }
            else if (string.IsNullOrEmpty(txtMaxContractValueYearThree.Text.Trim()) || string.IsNullOrEmpty(txtAnuualCompRevenueYearThree.Text.Trim()))
            {
                if (string.IsNullOrEmpty(txtMaxContractValueYearThree.Text.Trim()))
                {
                    //cusvYearThree.ErrorMessage = "Please enter maximum contract value completed under year three";
                    args.IsValid = true;
                }
                if (string.IsNullOrEmpty(txtAnuualCompRevenueYearThree.Text.Trim()))
                {
                    //cusvYearThree.ErrorMessage = "Please enter annual company revenue under year three";
                    args.IsValid = true;
                }
            }
        }
    }

    protected void cusvYearFour_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNAYear4.Checked)
        {
            if (string.IsNullOrEmpty(txtExplainYear4.Text.Trim()))
            {
                cusvYearFour.ErrorMessage = "Please enter the explanation under year four";
                args.IsValid = true;
            }
            else
            {
                args.IsValid = true;
            }
        }

    }
    protected void cusvYearFive_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNAYear5.Checked)
        {
            if (string.IsNullOrEmpty(txtExplainYear5.Text.Trim()))
            {
                cusvYearFive.ErrorMessage = "Please enter the explanation under year five";
                args.IsValid = true;
            }
            else
            {
                args.IsValid = true;
            }
        }

    }
    protected void cusvYearSix_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNAYear6.Checked)
        {
            if (string.IsNullOrEmpty(txtExplainYear6.Text.Trim()))
            {
                cusvYearSix.ErrorMessage = "Please enter the explanation under year six";
                args.IsValid = true;
            }
            else
            {
                args.IsValid = true;
            }
        }

    }
    protected void cusvYearSeven_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNAYear7.Checked)
        {
            if (string.IsNullOrEmpty(txtExplainYear7.Text.Trim()))
            {
                cusvYearSeven.ErrorMessage = "Please enter the explanation under year seven";
                args.IsValid = true;
            }
            else
            {
                args.IsValid = true;
            }
        }

    }
    protected void cusvYearEight_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNAYear8.Checked)
        {
            if (string.IsNullOrEmpty(txtExplainYear8.Text.Trim()))
            {
                cusvYearEight.ErrorMessage = "Please enter the explanation under year eight";
                args.IsValid = true;
            }
            else
            {
                args.IsValid = true;
            }
        }

    }
    protected void cusvYearnine_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNAYear9.Checked)
        {
            if (string.IsNullOrEmpty(txtExplainYear9.Text.Trim()))
            {
                cusvYearnine.ErrorMessage = "Please enter the explanation under year nine";
                args.IsValid = true;
            }
            else
            {
                args.IsValid = true;
            }
        }

    }
    protected void cusvYearTen_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (chkNAYear10.Checked)
        {
            if (string.IsNullOrEmpty(txtExplainYear10.Text.Trim()))
            {
                cusvYearTen.ErrorMessage = "Please enter the explanation under year ten";
                args.IsValid = true;
            }
            else
            {
                args.IsValid = true;
            }
        }

    }

    // Imman

    protected void cusvAnnualRevenueYear1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtMaxContractValueYearOne.Text != "" && txtAnuualCompRevenueYearOne.Text != "")
        {
            if (Convert.ToDecimal(txtMaxContractValueYearOne.Text) >= Convert.ToDecimal(txtAnuualCompRevenueYearOne.Text))
            {
                args.IsValid = true;
            }

        }
    }

    protected void cusvAnnualRevenueYear2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtMaxContractValueYearTwo.Text != "" && txtAnuualCompRevenueYearTwo.Text != "")
        {
            if (Convert.ToDecimal(txtMaxContractValueYearTwo.Text) >= Convert.ToDecimal(txtAnuualCompRevenueYearTwo.Text))
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvAnnualRevenueYear3_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtMaxContractValueYearThree.Text != "" && txtAnuualCompRevenueYearThree.Text != "")
        {
            if (Convert.ToDecimal(txtMaxContractValueYearThree.Text) >= Convert.ToDecimal(txtAnuualCompRevenueYearThree.Text))
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvAnnualRevenueYear4_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtMaxContractValueYear1.Text != "" && txtAnuualCompRevenueYear1.Text != "")
        {
            if (Convert.ToDecimal(txtMaxContractValueYear1.Text) >= Convert.ToDecimal(txtAnuualCompRevenueYear1.Text))
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvAnnualRevenueYear5_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtMaxContractValueYear2.Text != "" && txtAnuualCompRevenueYear2.Text != "")
        {
            if (Convert.ToDecimal(txtMaxContractValueYear2.Text) >= Convert.ToDecimal(txtAnuualCompRevenueYear2.Text))
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvAnnualRevenueYear6_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtMaxContractValueYear3.Text != "" && txtAnuualCompRevenueYear3.Text != "")
        {
            if (Convert.ToDecimal(txtMaxContractValueYear3.Text) >= Convert.ToDecimal(txtAnuualCompRevenueYear3.Text))
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvAnnualRevenueYear7_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtMaxContractValueYear4.Text != "" && txtAnuualCompRevenueYear4.Text != "")
        {
            if (Convert.ToDecimal(txtMaxContractValueYear4.Text) >= Convert.ToDecimal(txtAnuualCompRevenueYear4.Text))
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvAnnualRevenueYear8_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtMaxContractValueYear5.Text != "" && txtAnuualCompRevenueYear5.Text != "")
        {
            if (Convert.ToDecimal(txtMaxContractValueYear5.Text) >= Convert.ToDecimal(txtAnuualCompRevenueYear5.Text))
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvAnnualRevenueYear9_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtMaxContractValueYear6.Text != "" && txtAnuualCompRevenueYear6.Text != "")
        {
            if (Convert.ToDecimal(txtMaxContractValueYear6.Text) >= Convert.ToDecimal(txtAnuualCompRevenueYear6.Text))
            {
                args.IsValid = true;
            }
        }
    }

    protected void cusvAnnualRevenueYear10_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtMaxContractValueYear7.Text != "" && txtAnuualCompRevenueYear7.Text != "")
        {
            if (Convert.ToDecimal(txtMaxContractValueYear7.Text) >= Convert.ToDecimal(txtAnuualCompRevenueYear7.Text))
            {
                args.IsValid = true;
            }
        }
    }

    #endregion

    private void SetValidCharsAndLength(DropDownList dropDownList, TextBox textBox, AjaxControlToolkit.FilteredTextBoxExtender filteredTextBoxExtender, RegularExpressionValidator regvValidator, HtmlTableRow tableRow)
    {

        //002-Sooraj commented to apply Internationalization

        //if (dropDownList.SelectedIndex > 0)
        //{
        //    if (dropDownList.SelectedItem.Text.Trim().Equals("Other"))
        //    {
        //        tableRow.Style["display"] = "";
        //        regvValidator.ValidationGroup = "State";
        //        textBox.MaxLength = 10;
        //        textBox.Width = Unit.Pixel(80);
        //        filteredTextBoxExtender.ValidChars = " ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0";
        //    }
        //    else
        //    {
        //        tableRow.Style["display"] = "None";
        //        regvValidator.ValidationGroup = "Banking";
        //        filteredTextBoxExtender.ValidChars = "0,1,2,3,4,5,6,7,8,9";
        //        textBox.MaxLength = 5;
        //        textBox.Width = Unit.Pixel(40);
        //    }
        //}

        //002-Sooraj added 
        objCommon.SetValidCharsAndLength(dropDownList, textBox, filteredTextBoxExtender, regvValidator, tableRow, "Banking");
    }

    private bool? GetBooleanValue(RadioButtonList radioButtonList)
    {
        switch (radioButtonList.SelectedIndex)
        {
            case 0:
                return true;
            case 1:
                return false;
            default:
                return null;
        }
    }

    private void ResetBanking(HtmlTableCell tableCell, TextBox textBox1, TextBox textBox2, TextBox textBox3, TextBox textBox4, TextBox textBox5, TextBox textBox6,
                            TextBox textBox7, TextBox textBox8, TextBox ziptextBox, DropDownList dropDownList, RadioButtonList radioButtonList, HtmlTableRow tableRow2,
                            AjaxControlToolkit.FilteredTextBoxExtender filteredExtender, RegularExpressionValidator regularExpressionValidator)
    {
        if (tableCell != null)
            tableCell.Style["display"] = "none";

        textBox1.Text = "";
        textBox2.Text = "";
        textBox3.Text = "";
        textBox4.Text = "";
        textBox5.Text = "";
        textBox6.Text = "";
        textBox7.Text = "";
        textBox8.Text = "";
        ziptextBox.Text = "";
        dropDownList.SelectedIndex = 0;
        radioButtonList.Items[0].Selected = false;
        radioButtonList.Items[1].Selected = false;

        tableRow2.Style["display"] = "none";
        regularExpressionValidator.ValidationGroup = "Banking";

        //  filteredTextBoxExtender.ValidChars = "0,1,2,3,4,5,6,7,8,9";
        filteredExtender.ValidChars = objCommon.ZipCodeValidCharacters;//002-sooraj

        ziptextBox.MaxLength = 5;
        ziptextBox.Width = Unit.Pixel(40);
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(Session["ReturnQuery"].ToString());
    }

    protected void imbOkoutlook_Click(object sender, ImageClickEventArgs e)
    {
        modalExtnd.Hide();
        imbOkoutlook.Visible = false;
        imbOk.Style["display"] = "";
        //Response.Redirect(Session["ReturnQuery"].ToString());
        LoadEditDatas(true);
    }
    
 
}