﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.ApplicationBlocks.Data;
using System.Data.Sql;
using System.Data.SqlClient;

#region Change Comments
/**************************************************************************************************************
 * 001 - G. Vera 06/13/2012 Phase 3 Item 4.9 (JIRA:VPI17)
 * 002 - G. Vera 08/22/2012: VMS Stabilization Release 1.2 (JIRA:VPI-52)
 * 
 ***************************************************************************************************************/
#endregion


/// <summary>
/// Summary description for clsRating
/// </summary>
public class clsRating:Common
{
	public clsRating()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    /// <summary>
    /// Select SRF details
    /// </summary>
    /// <param name="VendorId">Based on the vendorid</param>
    /// <param name="EmployeeNumber">Employee number</param>
    /// <param name="ProjectNumber">Project number</param>
    /// <returns></returns>
    public DAL.SRFProjectRating SelectRating(long VendorId,string EmployeeNumber, string ProjectNumber,string Status)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (Status != "1")
            {
                int vendorRating = dcHaskell.SRFProjectRatings.Count(m => m.FK_VendorID == VendorId && m.FK_EmployeeNumber == EmployeeNumber && m.ProjectNumber == ProjectNumber);
                if (vendorRating > 0)
                {
                    DAL.SRFProjectRating objRating = dcHaskell.SRFProjectRatings.Single(m => m.FK_VendorID == VendorId && m.FK_EmployeeNumber == EmployeeNumber && m.ProjectNumber == ProjectNumber);
                    return objRating;
                }
                else
                {
                    return null;

                }
            }
            else
            {
                int vendorRating = dcHaskell.SRFProjectRatings.Count(m => m.FK_VendorID == VendorId && m.FK_EmployeeNumber == EmployeeNumber && m.ProjectNumber == ProjectNumber && m.RatingStatus == 0);
                if (vendorRating > 0)
                {
                    DAL.SRFProjectRating objRating = dcHaskell.SRFProjectRatings.Single(m => m.FK_VendorID == VendorId && m.FK_EmployeeNumber == EmployeeNumber && m.ProjectNumber == ProjectNumber && m.RatingStatus == 0);
                    return objRating;
                }
                else
                {
                    return null;

                }
            }

        }
    }

    public DAL.HaskellProject[] GetProject()
    {

        using (DAL.HaskellAdminDataContext context = new DAL.HaskellAdminDataContext())
        {
            var project = from Projects in context.HaskellProjects
                          orderby Projects.ProjectNumber ascending
                          select Projects;
            return project.ToArray();
        }
    }

    //001
    public DAL.HaskellProject[] GetProject(string projectPrefix)
    {

        using (DAL.HaskellAdminDataContext context = new DAL.HaskellAdminDataContext())
        {
            DAL.HaskellProject[] project = (from Projects in context.HaskellProjects
                          where Projects.ProjectNumber.Trim().StartsWith(projectPrefix.Trim())
                          orderby Projects.ProjectNumber ascending
                          select Projects).ToArray();
            return project;
        }
    }

    /// <summary>
    /// Get the Rating based on the Rating id, vendorid, Employeenumber
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="EmployeeNumber"></param>
    /// <param name="RatingId"></param>
    /// <returns></returns>
    public DAL.SRFProjectRating SelectRatingById(long VendorId, string EmployeeNumber, int RatingId)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            int vendorRating = dcHaskell.SRFProjectRatings.Count(m => m.FK_VendorID == VendorId && m.FK_EmployeeNumber == EmployeeNumber && m.PK_RatingID == RatingId);
            if (vendorRating > 0)
            {
                DAL.SRFProjectRating objRating = dcHaskell.SRFProjectRatings.Single(m => m.FK_VendorID == VendorId && m.FK_EmployeeNumber == EmployeeNumber && m.PK_RatingID == RatingId);
                return objRating;
            }
            else
            {
                return null;

            }

        }
    }

    /// <summary>
    /// Get the Rating based on the rating id
    /// </summary>
    /// <param name="RatingId"></param>
    /// <returns></returns>
    public DAL.SRFProjectRating SelectRatingbyId(long RatingId)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (Convert.ToInt32(dcHaskell.SRFProjectRatings.Count(m => m.PK_RatingID == RatingId)) > 0)
            {
                 DAL.SRFProjectRating objRating = dcHaskell.SRFProjectRatings.Single(m => m.PK_RatingID == RatingId);
                 return objRating;
            }
            else
            {
                return null;
            }
           
        }
    }

    /// <summary>
    /// Returns the project list
    /// </summary>
    /// <returns></returns>
    public DataSet DisplayProject()
    {
        using (DAL.HaskellAdminDataContext objDataContent = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DataSet dsProject = new DataSet();
            dsProject = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetProjects");
            return dsProject;

        
        }
    }

    public string GetProjectDescription(string ProjectNumber)
    {
        using (DAL.HaskellAdminDataContext objDataContent = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            try
            {
                return objDataContent.HaskellProjects.Single(m => m.ProjectNumber.Trim() == ProjectNumber.Trim()).ProjectDescription;
            }
            catch (Exception e)
            {
                return " ";
            }


        }
    }
    /// <summary>
    /// Get the project numberbased on the rating id
    /// </summary>
    /// <param name="RatingId"></param>
    /// <returns></returns>
    public string SelectProjectNumber(long RatingId)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (Convert.ToInt32(dcHaskell.SRFProjectRatings.Count(m => m.PK_RatingID == RatingId)) > 0)
            {
                DAL.SRFProjectRating objRating = dcHaskell.SRFProjectRatings.Single(m => m.PK_RatingID == RatingId);
                return Convert.ToString(objRating.ProjectNumber);
            }
            else
            {
                return null;
            }

        }
    }

    /// <summary>
    /// Get the count of rating for the vendor id
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public int GetCountOfRating(long VendorId)
    {
        using (DAL.HaskellAdminDataContext objAdmin = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            int vendorRating = objAdmin.SRFProjectRatings.Count(m => m.FK_VendorID == VendorId && m.RatingStatus>0);
            return vendorRating;
            
        }
    }

    /// <summary>
    /// Fetch the Rating details based on the vendorid, pagenumber and page size
    /// </summary>
    /// <param name="PageNumber"></param>
    /// <param name="PageSize"></param>
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public DAL.UspGetRatingByVendorResult[] getRatingByVendor(int PageNumber,int PageSize, long VendorId)
    {

        using (DAL.HaskellAdminDataContext objHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetRatingByVendorResult[] objgetRatingResult = objHaskell.UspGetRatingByVendor(PageSize, PageNumber, VendorId).ToArray();
            return objgetRatingResult;
        }
    }

    /// <summary>
    /// Get Rating result based on the Vendor name and vendor number
    /// </summary>
    /// <param name="PageSize"></param>
    /// <param name="PageNumber"></param>
    /// <param name="VendorName"></param>
    /// <param name="VendorNumber"></param>
    /// <returns></returns>
    public DAL.USPDisplayVendorNameResult[] DisplayVendorName(int PageSize, int PageNumber, string VendorName, string VendorNumber, string SortExp)
    {
            using (DAL.HaskellAdminDataContext objHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
            {
                DAL.USPDisplayVendorNameResult[] objgetRatingResult = objHaskell.USPDisplayVendorName(PageSize, PageNumber, VendorName, VendorNumber, SortExp).ToArray();

                return objgetRatingResult;
            }

    }

    /// <summary>
    /// 002 - Implemented
    /// </summary>
    /// <param name="PageSize"></param>
    /// <param name="PageNumber"></param>
    /// <param name="VendorName"></param>
    /// <param name="VendorNumber"></param>
    /// <param name="SortExp"></param>
    /// <returns></returns>
    public DataSet DisplayExistingRatingVendors(int PageSize, int PageNumber, string VendorName, string VendorNumber, string SortExp)
    {
        return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "USPDisplayExistingRatingVendors",
                                       new SqlParameter("@PageSize", PageSize), new SqlParameter("@PageNumber", PageNumber),
                                       new SqlParameter("@VendorName", VendorName), new SqlParameter("@VendorNumber", VendorNumber), 
                                       new SqlParameter("@SortExp", SortExp));
    }


    //public DataTable  DisplayVendorName(int PageSize, int PageNumber, string VendorName, string VendorNumber, string SortExp)
    //{
    //    SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
    //    objConnection.Open();
    //    SqlCommand objCommand = new SqlCommand("USPDisplayVendorName", objConnection);
    //    objCommand.CommandType = CommandType.StoredProcedure;
    //    objCommand.Parameters.AddWithValue("@PageSize", PageSize);
    //    objCommand.Parameters.AddWithValue("@PageNumber", PageNumber);
    //    objCommand.Parameters.AddWithValue("@VendorName", VendorName);
    //    objCommand.Parameters.AddWithValue("@VendorNumber", VendorNumber);
    //    objCommand.Parameters.AddWithValue("@SortExp", SortExp);

    //    objCommand.ExecuteNonQuery();
    //    SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
    //    DataTable objTable = new DataTable();
    //    objAdapter.Fill(objTable);
    //    return objTable;
      
  
    //}

    /// <summary>
    /// Get the rating count based on the vendorname and vendor number
    /// </summary>
    /// <param name="VendorName"></param>
    /// <param name="VendorNumber"></param>
    /// <returns></returns>
    public int DisplayVendorNameCount(string VendorName,string VendorNumber)
    {
        using (DAL.HaskellAdminDataContext objHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.USPDisplayVendorNameCountResult[] objgetRatingResult = objHaskell.USPDisplayVendorNameCount(VendorName,VendorNumber).ToArray();
            if (objgetRatingResult.Count() > 0)
            {
                return Convert.ToInt32(objgetRatingResult[0].CountValue);
            }
            else
            {
                return 0;
            }
        }
    }

    /// <summary>
    /// 002 - Implemented
    /// </summary>
    /// <param name="VendorName"></param>
    /// <param name="VendorNumber"></param>
    /// <returns></returns>
    public int DisplayExistingRatingVendorsCount(string VendorName, string VendorNumber)
    {
        DataSet result = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "USPDisplayExisitngRatingVendorsCount",
                                      new SqlParameter("@VendorName", VendorName), new SqlParameter("@VendorNumber", VendorNumber));

        int res = Convert.ToInt32(result.Tables[0].Rows[0]["CountValue"]);

        return res;
    }

    /// <summary>
    /// delete the rating
    /// </summary>
    /// <param name="RatingId"></param>
    public void DeleteRating(long RatingId)
    {
        using (DAL.HaskellAdminDataContext objHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        { 
            objHaskell.SRFProjectRatings.DeleteAllOnSubmit(objHaskell.SRFProjectRatings.Where(m=>m.PK_RatingID == RatingId));
            objHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }
    }

    //Insert Rating
    public int InsertRating(Byte ChangeOrdersClaims, string Comments, Byte Cooperation, string EmployeeTitle, Byte FieldManagement, Byte FinancialCapability, string FK_EmployeeNumber, long FK_VendorID, bool? InsuranceLevels, double OverallPerformance, Byte ProjectManagement, string ProjectName, string ProjectNumber, Byte QualityOfWork, Byte RatingStatus, byte Safety, bool? SpecialPayment, string SubContractorScope, bool? Subs_Vendors,int Insert,string Access,string RatingId,string FinalSubContractValue,Byte ScheduleControl)
    {

        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (Insert == 1)
            {
                int vendorCount = dcHaskell.SRFProjectRatings.Count(m => m.FK_EmployeeNumber == FK_EmployeeNumber && m.FK_VendorID == FK_VendorID && m.ProjectNumber == ProjectNumber && m.RatingStatus == 1);  //002 - changed the status from 0 to 1 as that is tha value used when inserting
                    if (vendorCount != 0)
                    {
                        //002 - this needs to change.  If UI sends an Insert = "1" then UI really wants to insert a new record and not update.

                        #region Archived
                        //DAL.SRFProjectRating objRating = dcHaskell.SRFProjectRatings.Single(m => m.FK_EmployeeNumber == FK_EmployeeNumber && m.FK_VendorID == FK_VendorID && m.ProjectNumber == ProjectNumber && m.RatingStatus == 1); //002 - changed the status from 0 to 1 as that is tha value used when inserting
                        //objRating.ChangeOrdersClaims = ChangeOrdersClaims;
                        //objRating.Comments = Comments;
                        //objRating.Cooperation = Cooperation;
                        //objRating.EmployeeTitle = EmployeeTitle;
                        //objRating.FieldManagement = FieldManagement;
                        //objRating.FinancialCapability = FinancialCapability;
                        //objRating.FK_EmployeeNumber = FK_EmployeeNumber;
                        //objRating.FK_VendorID = FK_VendorID;
                        //objRating.InsuranceLevels = InsuranceLevels;
                        //objRating.OverallPerformance = OverallPerformance;
                        //objRating.ProjectManagement = ProjectManagement;
                        //objRating.ProjectName = ProjectName;
                        //objRating.ProjectNumber = ProjectNumber;
                        //objRating.QualityOfWork = QualityOfWork;
                        //objRating.RatingStatus = RatingStatus;
                        //objRating.Safety = Safety;
                        //objRating.SpecialPayment = SpecialPayment;
                        //objRating.SubcontractorScope = SubContractorScope;
                        //objRating.Subs_Vendors = Subs_Vendors;
                        //objRating.FinalSubcontract = FinalSubContractValue;
                        //objRating.ScheduleControl = ScheduleControl;
                        //dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict); 
                        #endregion

                        return 2;       //002 - need to return something different than insert so UI knows what to display user.

                    }
                    else
                    {
                        // insert
                        DAL.SRFProjectRating objRating = new DAL.SRFProjectRating
                        {
                            ChangeOrdersClaims = ChangeOrdersClaims,
                            Comments = Comments,
                            Cooperation = Cooperation,
                            EmployeeTitle = EmployeeTitle,
                            FieldManagement = FieldManagement,
                            FinancialCapability = FinancialCapability,
                            FK_EmployeeNumber = FK_EmployeeNumber,
                            FK_VendorID = FK_VendorID,
                            InsuranceLevels = InsuranceLevels,
                            OverallPerformance = OverallPerformance,
                            ProjectManagement = ProjectManagement,
                            ProjectName = ProjectName,
                            ProjectNumber = ProjectNumber,
                            QualityOfWork = QualityOfWork,
                            RatingStatus = RatingStatus,
                            Safety = Safety,
                            SpecialPayment = SpecialPayment,
                            SubcontractorScope = SubContractorScope,
                            Subs_Vendors = Subs_Vendors,
                            FinalSubcontract = FinalSubContractValue,
                            ScheduleControl = ScheduleControl,

                        };
                        dcHaskell.SRFProjectRatings.InsertOnSubmit(objRating);
                        dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                        return 0;
                    }
                
               

            }
            else if (Insert == 0)
            {
                // update
                DAL.SRFProjectRating objRating = dcHaskell.SRFProjectRatings.Single(m => m.PK_RatingID == Convert.ToInt64(RatingId));
                objRating.ChangeOrdersClaims = ChangeOrdersClaims;
                objRating.Comments = Comments;
                objRating.Cooperation = Cooperation;
                objRating.EmployeeTitle = EmployeeTitle;
                objRating.FieldManagement = FieldManagement;
                objRating.FinancialCapability = FinancialCapability;
                objRating.FK_EmployeeNumber = FK_EmployeeNumber;
                objRating.FK_VendorID = FK_VendorID;
                objRating.InsuranceLevels = InsuranceLevels;
                objRating.OverallPerformance = OverallPerformance;
                objRating.ProjectManagement = ProjectManagement;
                objRating.ProjectName = ProjectName;
                objRating.ProjectNumber = ProjectNumber;
                objRating.QualityOfWork = QualityOfWork;
                objRating.RatingStatus = RatingStatus;
                objRating.Safety = Safety;
                objRating.SpecialPayment = SpecialPayment;
                objRating.SubcontractorScope = SubContractorScope;
                objRating.Subs_Vendors = Subs_Vendors;
                objRating.FinalSubcontract = FinalSubContractValue;
                objRating.ScheduleControl = ScheduleControl;
                dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                return 2;
            }
            else { return 0; }
        }
       


      
    }
}
