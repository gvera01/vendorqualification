﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;

#region Change Comments
/**************************************************************************************************
 * 001 G. Vera 07/16/2012 - New search by fields (JIRA:VPI-18)
 * 002 G. Vera 10/17/2012: VMS Stabilization Release 1.3 (refer to JIRA issues with this release)
 * 003 G. Vera 02/26/2013:  VMS Enhancment Release 1.3.1 (BIM Questions)
 * 004 G. Vera 07/14/2014: Search by type of company as well
 ***************************************************************************************************/
#endregion

/// <summary>
/// Summary description for clsSearchVendor
/// </summary>
public class clsSearchVendor:Common
{
	public clsSearchVendor()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /// <summary>
    /// Get CompanyId based on the company name
    /// </summary>
    /// <param name="CompanyName"></param>
    /// <returns></returns>
    public int GetCompanyId(string CompanyName)
    {
        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (dcHaskell.Vendors.Count(m => m.Company == CompanyName) > 0)
            {
                return Convert.ToInt32(dcHaskell.Vendors.Single(m => m.Company == CompanyName).PK_VendorID);
            }
            else
            {
                return 0;
            }


        }
    }

    //Insert Searched record
    //001 - commented it out as the DAL is not updatable for new fields.
    //public bool InsertSearchVendor(string CSICodes, string SearchRecord, string SearchRoot, long FK_EmployeeID, string JDEVendorNo, string ProjectTypes, 
    //                               string RegionCodes, string TaxId, string TrackingNo, string VendorId, string OtherState,long Count,string Status,
    //                               string Certification,string FederalSB)
    //{
      
    //    using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
    //    {

    //        DAL.SearchHistory objSearchHistory = new DAL.SearchHistory
    //        {
    //          CSICodes=CSICodes,
    //          FK_EmployeeID=FK_EmployeeID,
    //          JDEVendorNo=JDEVendorNo,
    //          ProjectTypes=ProjectTypes,
    //          RegionCodes=RegionCodes,
    //          SearchRoot=SearchRoot,
    //          TaxId=TaxId,
    //          TrackingNo=TrackingNo,
    //          VendorId=VendorId,
    //          SearchedRecord=SearchRecord,
    //          OtherState=OtherState,
    //          Count=Count,
    //          Status=Status,
    //          CompanyCertification=Certification,
    //          FederalSB=FederalSB
    //        };
    //        dcHaskell.SearchHistories.InsertOnSubmit(objSearchHistory);
    //        dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
    //        return true;
            
    //    }
       
    //}

    /// <summary>
    /// Insert Searched record
    /// 003 - added new parameter
    /// </summary>
    /// <param name="CSICodes"></param>
    /// <param name="SearchRecord"></param>
    /// <param name="SearchRoot"></param>
    /// <param name="FK_EmployeeID"></param>
    /// <param name="JDEVendorNo"></param>
    /// <param name="ProjectTypes"></param>
    /// <param name="RegionCodes"></param>
    /// <param name="TaxId"></param>
    /// <param name="TrackingNo"></param>
    /// <param name="VendorId"></param>
    /// <param name="OtherState"></param>
    /// <param name="Count"></param>
    /// <param name="Status"></param>
    /// <param name="Certification"></param>
    /// <param name="FederalSB"></param>
    /// <param name="FirstName"></param>
    /// <param name="LastName"></param>
    /// <param name="Email"></param>
    /// <param name="OtherProject"></param>
    /// <param name="ISO"></param>
    /// <param name="QS"></param>
    /// <param name="providesBIM"></param>
    /// <returns></returns>
    public int InsertSearchVendor(string CSICodes, string SearchRecord, string SearchRoot, long FK_EmployeeID, string JDEVendorNo, string ProjectTypes,
                                   string RegionCodes, string TaxId, string TrackingNo, string VendorId, string OtherState, long Count, string Status,
                                   string Certification, string FederalSB, string FirstName, string LastName, string Email,string OtherProject,string ISO, 
                                   string QS, bool providesBIM,string continentCodes,string countryCodes, string CompanyTypes)
    {

            SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
            objConnection.Open();
            string query = "INSERT INTO SearchHistory " +
                           "(SearchRoot, CSICodes, RegionCodes, ContinentCodes,CountryCodes, ProjectTypes, VendorId, JDEVendorNo, TrackingNo, " +
                                     "TaxId, VendorStatus, FK_EmployeeID, CreateDate, ModifyDate, Active, SearchedRecord, OtherState, " +
                                     "Count, Status, CompanyCertification, FederalSB, FirstName, LastName, VendorEmail, OtherProject, ISO, QS, ProvidesBIM, CompanyTypes) " +
                           "VALUES (@SearchRoot, @CSICodes, @RegionCodes, @ContinentCodes,@CountryCodes, @ProjectTypes, @VendorId, @JDEVendorNo, @TrackingNo, @TaxId, @VendorStatus, " + 
                                   "@FK_EmployeeID, @CreateDate, @ModifyDate, @Active, @SearchRecord,  @OtherState, @Count, @Status, @Certification, " +
                                   "@FederalSB, @FirstName, @LastName, @Email, @OtherProject, @ISO, @QS, @ProvidesBIM, @CompanyTypes)";     //004

            ;
            // need to be in the same column order
            SqlCommand objCommand = new SqlCommand(query, objConnection);
            objCommand.Parameters.AddWithValue("@CSICodes", (CSICodes != null) ? CSICodes : "");
            objCommand.Parameters.AddWithValue("@SearchRecord", (SearchRecord != null) ? SearchRecord : "");
            objCommand.Parameters.AddWithValue("@SearchRoot", (SearchRoot != null) ? SearchRoot : "");
            objCommand.Parameters.AddWithValue("@FK_EmployeeID", FK_EmployeeID);
            objCommand.Parameters.AddWithValue("@JDEVendorNo", (JDEVendorNo != null) ? JDEVendorNo : "");
            objCommand.Parameters.AddWithValue("@ProjectTypes", (ProjectTypes != null) ? ProjectTypes : "");
            objCommand.Parameters.AddWithValue("@RegionCodes", (RegionCodes != null) ? RegionCodes : "");
         
            objCommand.Parameters.AddWithValue("@TaxId", (TaxId != null) ? TaxId : "");
            objCommand.Parameters.AddWithValue("@VendorStatus", 7); //can't use null so using a non-existent status number
            objCommand.Parameters.AddWithValue("@CreateDate", DateTime.Now);
            objCommand.Parameters.AddWithValue("@ModifyDate", DateTime.Now);
            objCommand.Parameters.AddWithValue("@Active", true);
            objCommand.Parameters.AddWithValue("@TrackingNo", (TrackingNo != null) ? TrackingNo : "");
            objCommand.Parameters.AddWithValue("@VendorId", (VendorId != null) ? VendorId : "");
            objCommand.Parameters.AddWithValue("@OtherState", (OtherState != null) ? OtherState : "");
            objCommand.Parameters.AddWithValue("@Count", Count);
            objCommand.Parameters.AddWithValue("@Status", (Status != null) ? Status : "");
            objCommand.Parameters.AddWithValue("@Certification", (Certification != null) ? Certification : "");
            objCommand.Parameters.AddWithValue("@FederalSB", (FederalSB != null) ? FederalSB : "");
            objCommand.Parameters.AddWithValue("@FirstName", (FirstName != null) ? FirstName : "");
            objCommand.Parameters.AddWithValue("@LastName", (LastName != null) ? LastName : "");
            objCommand.Parameters.AddWithValue("@Email", (Email != null) ? Email : "");
            objCommand.Parameters.AddWithValue("@OtherProject", (OtherProject != null) ? OtherProject : "");    //002
            objCommand.Parameters.AddWithValue("@ISO", (ISO != null) ? ISO : "");                               //002
            objCommand.Parameters.AddWithValue("@QS", (QS != null) ? QS : "");                                  //002
            objCommand.Parameters.AddWithValue("@ProvidesBIM", (providesBIM) ? "Y" : "");                       //003
            objCommand.Parameters.AddWithValue("@ContinentCodes", (continentCodes != null) ? continentCodes : "");//008
            objCommand.Parameters.AddWithValue("@CountryCodes", (countryCodes != null) ? countryCodes : "");//008
            //004
            objCommand.Parameters.AddWithValue("@CompanyTypes", CompanyTypes);

            int result = objCommand.ExecuteNonQuery();

            return result;

    }

    /// <summary>
    /// View prior seach result
    /// </summary>
    /// <param name="EmployeeId"></param>
    /// <param name="PageIndex"></param>
    /// <param name="PageSize"></param>
    /// <returns></returns>
    public DAL.UspViewPriorHistoryResult[] ViewPriorSearch(string EmployeeName,int PageIndex,int PageSize,string SortExpression)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {long EmployeeId;
        if (string.IsNullOrEmpty(EmployeeName))
        { EmployeeId = 0; }
        else
        {
            EmployeeId = dcHaskell.Employees.Count(m => m.EmployeeName == EmployeeName) == 1 ? dcHaskell.Employees.Single(m => m.EmployeeName == EmployeeName).PK_EmployeeID : -1;
        }
            DAL.UspViewPriorHistoryResult[] ObjResult = dcHaskell.UspViewPriorHistory(PageSize,PageIndex,SortExpression,EmployeeId).ToArray();
            return ObjResult;
        }
    }

    /// <summary>
    /// Check the saved name aleady exists
    /// </summary>
    /// <param name="EmployeeId"></param>
    /// <param name="Name"></param>
    /// <returns></returns>
    public int CheckNameExists(long EmployeeId, string Name)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspCheckNameExistsResult[] ObjResult = dcHaskell.UspCheckNameExists(EmployeeId, Name).ToArray();
            if (ObjResult.Length > 0)
            {
                return Convert.ToInt32(ObjResult[0].CountValue);
            }
            else
            {
                return 0;
            }
        }
    }

    /// <summary>
    /// delete search history
    /// </summary>
    /// <param name="HistoryId"></param>
    public void DeleteSearchHistory(long HistoryId)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            dcHaskell.UspDeletesearcHistory(HistoryId);
           
        }
    }

    /// <summary>
    /// Get count of search history
    /// 002 - modified
    /// </summary>
    /// <param name="HistoryId"></param>
    /// <param name="ParentId"></param>
    /// <returns></returns>
    public int GetHistoryCount(long HistoryId,int ParentId)
    {
        VMSDAL.USPGetSearchedRecordResult[] objresult = GetSearchHistory(HistoryId);
        
        return Convert.ToInt32(objresult.Count(m => m.RootNode == ParentId));
    }

    /// <summary>
    /// Display Search history
    /// 002 - modified
    /// </summary>
    /// <param name="HistoryId"></param>
    /// <returns></returns>
    public VMSDAL.USPGetSearchedRecordResult[] GetSearchHistory(long HistoryId)
    {
        using (VMSDAL.Haskell_VQF dcHaskell = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.USPGetSearchedRecordResult[] ObjResult = dcHaskell.USPGetSearchedRecord(HistoryId).ToArray();
            return ObjResult;
        }
    }

    /// <summary>
    /// Get the count of view prior search 
    /// </summary>
    /// <param name="EmployeeId"></param>
    /// <returns></returns>
    public int GetSearchPriorCount(string EmployeeName)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            long EmployeeId = dcHaskell.Employees.Count(m => m.EmployeeName == EmployeeName) == 1 ? dcHaskell.Employees.Single(m => m.EmployeeName == EmployeeName).PK_EmployeeID : 0;


            DAL.UspViewHistoryCountResult[] objResult=dcHaskell.UspViewHistoryCount(EmployeeId).ToArray();
            if(objResult.Count()>0)
            {
                return Convert.ToInt32(objResult[0].CountValue);
            }
            else
            {
                return 0;
            }
        }
    }

  /// <summary>
  /// Get Searched Regions 
  /// </summary>
  /// <param name="historyId"></param>
  /// <returns></returns>
    public DataTable GetSearchedContinents(long historyId, bool IsSelectCountry, string CountryName)
    {
        SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand("[USPGetSearchedContinent]", objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.AddWithValue("@HistoryId", historyId);
        objCommand.Parameters.AddWithValue("@IsSelectCountry", IsSelectCountry);
        objCommand.Parameters.AddWithValue("@ContinentName", CountryName);

        objCommand.ExecuteNonQuery();
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objTable = new DataTable();
        objAdapter.Fill(objTable);
        return objTable;

    }

    

    /// <summary>
    /// Get searched regions
    /// </summary>
    /// <param name="HistoryId"></param>
    /// <returns></returns>
    public DAL.USPGetSearchedRegionsResult[] GetSearchRegions(long HistoryId)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.USPGetSearchedRegionsResult[] ObjResult = dcHaskell.USPGetSearchedRegions(HistoryId).ToArray();
            return ObjResult;
        }
    }

    /// <summary>
    /// 001 - Commneted out.  Not in Use
    /// Get the search results based on the saved name
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="EmployeeId"></param>
    /// <returns></returns>
    //public DAL.UspGetSearchByNameResult[] GetSearchByName(string Name, int EmployeeId)
    //{
    //    using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
    //    {
    //        DAL.UspGetSearchByNameResult[] ObjResult = dcHaskell.UspGetSearchByName(Name, EmployeeId).ToArray();
    //        return ObjResult;
    //    }
    //}

    /// <summary>
    /// 001 - Created new as DAL is currently not updatable
    /// Gets search history data based on the saved search history name
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="EmployeeId"></param>
    /// <returns></returns>
    public List<SearchHistory> GetSearchByName(string Name, int EmployeeId)
    {
        SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand("UspGetSearchByName", objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.AddWithValue("@Name", Name);
        objCommand.Parameters.AddWithValue("@EmployeeId", EmployeeId);

        objCommand.ExecuteNonQuery();
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objTable = new DataTable();
        objAdapter.Fill(objTable);
        List<SearchHistory> tempList = new List<SearchHistory>();
        SearchHistory tempObj;
        foreach (DataRow row in objTable.Rows)
        {
            tempObj = new SearchHistory();
            tempObj.Id = (long)row["Id"];
            tempObj.SearchRoot = (row["SearchRoot"] is DBNull) ? "" : (string)row["SearchRoot"];
            tempObj.CSICodes = (row["CSICodes"] is DBNull) ? "" : (string)row["CSICodes"];
            tempObj.RegionCodes = (row["RegionCodes"] is DBNull) ? "" :(string)row["RegionCodes"];
            tempObj.ContinentCodes = (row["ContinentCodes"] is DBNull) ? "" : (string)row["ContinentCodes"];
            tempObj.CountryCodes = (row["CountryCodes"] is DBNull) ? "" : (string)row["CountryCodes"];
            tempObj.ProjectTypes = (row["ProjectTypes"] is DBNull) ? "" : (string)row["ProjectTypes"];
            tempObj.VendorId = (row["VendorId"] is DBNull) ? "" : (string)row["VendorId"];
            tempObj.JDEVendorNo = (row["JDEVendorNo"] is DBNull) ? "" : (string)row["JDEVendorNo"];
            tempObj.TrackingNo = (row["TrackingNo"] is DBNull) ? "" : (string)row["TrackingNo"];
            tempObj.TaxId = (row["TaxId"] is DBNull) ? "" : (string)row["TaxId"];
            tempObj.VendorStatus = (row["VendorStatus"] is DBNull) ? null : Convert.ToInt32(row["VendorStatus"]).ToString();
            tempObj.Fk_Employeeid = (row["Fk_Employeeid"] is DBNull) ? 0 : (long)row["Fk_Employeeid"];
            tempObj.CreateDate = (row["CreateDate"] is DBNull) ? DateTime.MinValue : (DateTime)row["CreateDate"];
            tempObj.ModifyDate = (row["ModifyDate"] is DBNull) ? null : (DateTime?)row["ModifyDate"];
            tempObj.Active = (row["Active"] is DBNull) ? false : (bool)row["Active"];
            tempObj.SearchedRecord = (row["SearchedRecord"] is DBNull) ? "" : (string)row["SearchedRecord"];
            tempObj.OtherState = (row["OtherState"] is DBNull) ? "" : (string)row["OtherState"];
            tempObj.Count = (row["Count"] is DBNull) ? 0 : (long)row["Count"];
            tempObj.Status = (row["Status"] is DBNull) ? "" : (string)row["Status"];
            tempObj.CompanyCertification = (row["CompanyCertification"] is DBNull) ? "" : (string)row["CompanyCertification"];
            tempObj.FederalSB = (row["FederalSB"] is DBNull) ? "" : (string)row["FederalSB"];
            tempObj.FirstName = (row["FirstName"] is DBNull) ? "" : (string)row["FirstName"];
            tempObj.LastName = (row["LastName"] is DBNull) ? "" : (string)row["LastName"];
            tempObj.VendorEmail = (row["VendorEmail"] is DBNull) ? "" : (string)row["VendorEmail"];
            tempObj.OtherProject = (row["OtherProject"] is DBNull) ? "" : (string)row["OtherProject"];       //002
            tempObj.ISO = (row["ISO"] is DBNull) ? "" : (string)row["ISO"];                         //002
            tempObj.QS = (row["QS"] is DBNull) ? "" : (string)row["QS"];                           //002
            tempObj.ProvidesBIM = (row["ProvidesBIM"] is DBNull) ? "" : (string)row["ProvidesBIM"];     //003
            tempObj.CompanyTypes = (row["CompanyTypes"] is DBNull) ? "" : (string)row["CompanyTypes"];     //004
            tempList.Add(tempObj);
        }

        return tempList;
        
    }

    /// <summary>
    /// Get search results based on the selected project types
    /// </summary>
    /// <param name="HistoryId"></param>
    /// <returns></returns>
    public DAL.USPGetSearchedProjectTypesResult[] GetSearchProjectTypes(long HistoryId)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.USPGetSearchedProjectTypesResult[] ObjResult = dcHaskell.USPGetSearchedProjectTypes(HistoryId).ToArray();
            return ObjResult;
        }
    }  

    /// <summary>
    /// 001 - Modified as DAL is not updatable
    /// Get search details based on the history id
    /// </summary>
    /// <param name="HistoryId"></param>
    /// <returns></returns>
    public List<SearchHistory> GetSearchDetails(long HistoryId)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.SearchHistory tempObj = dcHaskell.SearchHistories.Single(h => h.Id == HistoryId);
            List<SearchHistory> sHistoryObj = this.GetSearchByName(tempObj.SearchedRecord, Convert.ToInt32(tempObj.FK_EmployeeID));
            //DAL.UspGetSearchedDetailsResult[] ObjResult = dcHaskell.UspGetSearchedDetails(HistoryId).ToArray();
            //return ObjResult;

            return sHistoryObj;
        }
    }

    /// <summary>
    /// Get the rating based on the vendorid
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public long SubContratorRatingCount(long VendorId)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            return Convert.ToInt64(dcHaskell.SRFProjectRatings.Count(m => m.FK_VendorID == VendorId));
                
        }
    }

    /// <summary>
    /// Get average  rating
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public decimal GetAverageRating(long VendorId)
    {

        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {



             DAL.UspAverageRatingResult[] objResult= dcHaskell.UspAverageRating(VendorId).ToArray();
             if (objResult[0].AverageRating == null)
             {
                 return Convert.ToDecimal(0.0);
             }
             else
             {
                 return Convert.ToDecimal(objResult[0].AverageRating);
             }

        }
      
    }

    /// <summary>
    /// Display the search records
    /// 001 - Changed
    /// 003 - added
    /// 004 - company types
    /// </summary>
    /// <param name="CheckType"></param>
    /// <param name="VendorId"></param>
    /// <param name="vendorJDENumber"></param>
    /// <param name="TradingNumber"></param>
    /// <param name="TaxId"></param>
    /// <param name="Status"></param>
    /// <param name="CSICodes"></param>
    /// <param name="States"></param>
    /// <param name="ProjectTypes"></param>
    /// <param name="PageIndex"></param>
    /// <param name="NumberOfRecords"></param>
    /// <param name="SortExp"></param>
    /// <param name="OtherState"></param>
    /// <param name="OtherStateId"></param>
    /// <param name="Certification"></param>
    /// <param name="FederalSB"></param>
    /// <param name="FirstName"></param>
    /// <param name="LastName"></param>
    /// <param name="Email"></param>
    /// <returns></returns>
    public DataTable SearchVendor(string CheckType, string VendorId, string vendorJDENumber, string TradingNumber, string TaxId, string Status, string CSICodes, 
                                  string States, string ProjectTypes, int PageIndex, int NumberOfRecords, string SortExp, string OtherState, int OtherStateId,
                                  string Certification, string FederalSB, string FirstName, string LastName, string Email, string OtherProject = "", string iso = "", 
                                  string qs = "", string tradeQuestId = "", bool providesBIM = false, string Continents = "", string Countries = "", string CompanyTypes = "")  
    {
        SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
        objConnection.Open();
        //GV Test: SqlCommand objCommand = new SqlCommand("UspSearchVendor", objConnection);
        SqlCommand objCommand = new SqlCommand("UspSearchVendor1", objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.AddWithValue("@Checktype",CheckType);
        objCommand.Parameters.AddWithValue("@VendorID",VendorId);
        objCommand.Parameters.AddWithValue("@VendorNoJDE",vendorJDENumber);
        objCommand.Parameters.AddWithValue("@TradingNo",TradingNumber);
        objCommand.Parameters.AddWithValue("@TaxId",TaxId);
        objCommand.Parameters.AddWithValue("@Status", Status);
        objCommand.Parameters.AddWithValue("@Csicodes",CSICodes);
        objCommand.Parameters.AddWithValue("@states",States);
        objCommand.Parameters.AddWithValue("@Continents", Continents); //008 -Sooraj
        objCommand.Parameters.AddWithValue("@Countries", Countries); //008 -Sooraj
        objCommand.Parameters.AddWithValue("@Prjtypes",ProjectTypes);
        objCommand.Parameters.AddWithValue("@PrjOtherExpl", OtherProject);            //002
        objCommand.Parameters.AddWithValue("@PageIndex",PageIndex);
        objCommand.Parameters.AddWithValue("@NoRecords", NumberOfRecords);
        objCommand.Parameters.AddWithValue("@SortExp", SortExp);
        objCommand.Parameters.AddWithValue("@otherStateid", OtherStateId);
        objCommand.Parameters.AddWithValue("@otherstate", OtherState);
        objCommand.Parameters.AddWithValue("@Certification", Certification);
        objCommand.Parameters.AddWithValue("@FederalSB", FederalSB);
        objCommand.Parameters.AddWithValue("@FirstName", FirstName);    //001
        objCommand.Parameters.AddWithValue("@LastName", LastName);      //001   
        objCommand.Parameters.AddWithValue("@Email", Email);            //001
        objCommand.Parameters.AddWithValue("@ISO", iso);                //002
        objCommand.Parameters.AddWithValue("@QS", qs);                  //002
        objCommand.Parameters.AddWithValue("@TradeQuestId", tradeQuestId);                  //002
        objCommand.Parameters.AddWithValue("@ProvidesBIM", (providesBIM) ? "Y" : "");       //004
        //010
        objCommand.Parameters.AddWithValue("@CompanyTypes", CompanyTypes);                   

        objCommand.ExecuteNonQuery();
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objTable = new DataTable();
        objAdapter.Fill(objTable);
        return objTable;
        
    }

    /// <summary>
    /// export to excel
    /// 003 - added extra parameter
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public DataTable ExportToExcel(string CheckType, string VendorId, string vendorJDENumber, string TradingNumber, string TaxId, string Status, 
        string CSICodes, string States, string ProjectTypes, int PageIndex, int NumberOfRecords, string SortExp, string OtherState, 
        int OtherStateId, string Certification, string FederalSB, string FirstName="",string LastName="",string Email="",string OtherProject="",
        string iso="",string qs="", bool providesBIM = false,string Continents="",string Countries="", string CompanyTypes="")
    {
        SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
        objConnection.Open();

        //Changed by N Schoenberger 10-03-2011 for excel export summary *********************
        //SqlCommand objCommand = new SqlCommand("UspGetVendorExportToExcel", objConnection);
        SqlCommand objCommand = new SqlCommand("UspGetVendorSummaryByCompanyExportToExcel", objConnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Parameters.AddWithValue("@Checktype", CheckType);
            objCommand.Parameters.AddWithValue("@VendorID", VendorId);
            objCommand.Parameters.AddWithValue("@VendorNoJDE", vendorJDENumber);
            objCommand.Parameters.AddWithValue("@TradingNo", TradingNumber);
            objCommand.Parameters.AddWithValue("@TaxId", TaxId);
            objCommand.Parameters.AddWithValue("@Status", Status);
            objCommand.Parameters.AddWithValue("@Csicodes", CSICodes);
            objCommand.Parameters.AddWithValue("@states", States);
            objCommand.Parameters.AddWithValue("@Prjtypes", ProjectTypes);
            objCommand.Parameters.AddWithValue("@PrjOtherExpl", OtherProject);            //002
            objCommand.Parameters.AddWithValue("@PageIndex", PageIndex);
            objCommand.Parameters.AddWithValue("@NoRecords", NumberOfRecords);
            objCommand.Parameters.AddWithValue("@SortExp", SortExp);
            objCommand.Parameters.AddWithValue("@otherStateid", OtherStateId);
            objCommand.Parameters.AddWithValue("@otherstate", OtherState);
            objCommand.Parameters.AddWithValue("@Continents", (Continents != "") ? Continents : null); //008
            objCommand.Parameters.AddWithValue("@Countries", (Countries != "") ? Countries : null); //008
            objCommand.Parameters.AddWithValue("@Certification", Certification);
            objCommand.Parameters.AddWithValue("@FederalSB", FederalSB);
            objCommand.Parameters.AddWithValue("@FirstName", FirstName);    //001
            objCommand.Parameters.AddWithValue("@LastName", LastName);      //001   
            objCommand.Parameters.AddWithValue("@Email", Email);            //001
            objCommand.Parameters.AddWithValue("@ISO", iso);                //002
            objCommand.Parameters.AddWithValue("@QS", qs);                  //002
            objCommand.Parameters.AddWithValue("@ProvidesBIM", (providesBIM) ? "Y" : "");       //003
            //004
            objCommand.Parameters.AddWithValue("@CompanyTypes", CompanyTypes); 

            objCommand.ExecuteNonQuery();
            SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
            DataTable objTable = new DataTable();
            objAdapter.Fill(objTable);
            return objTable;
        //************************************************************************************
    }

    /// <summary>
    /// Check Vendorname exists in JDE database
    /// </summary>
    /// <param name="VendorName"></param>
    /// <returns></returns>
    public string CheckJDENumber(string VendorName)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DataTable dt = new DataTable();

            dt = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "CheckVendorNumberExists",
                 new SqlParameter("@VendorName", VendorName)).Tables[0];
            if (dt.Rows.Count > 0)
            {
                return Convert.ToString(dt.Rows[0]["Result"]);
            }
            else
            {
                return null;
            }
        }
    }

    //Get comments and tags in company view page
    public DataTable GetCommentsAndTag(long VendorId)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DataTable dt = new DataTable();

            dt = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetTagForVendor",
                 new SqlParameter("@VendorId", VendorId)).Tables[0];
            return dt;

        }
    }
    /// <summary>
    /// Get JDE vendornumber
    /// </summary>
    /// <param name="JDEVendorNumber"></param>
    /// <returns></returns>
    public DataTable GetJDEVendorNumbers(string JDEVendorNumber)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DataTable dt = new DataTable();

            dt = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetJDEVendorNumber",
                 new SqlParameter("@JDEVendorNumber", JDEVendorNumber)).Tables[0];
            return dt;

        }
    }

    //Get the count of search details
    public int GetDetailsCount(string CheckType, string VendorId, string vendorJDENumber, string TradingNumber, string TaxId, string Status, string CSICodes, string States, string ProjectTypes, string SortExp, string OtherState, int OtherStateId,string Certification,string FederalSB)
    {
        SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand("UspSearchVendorTotal", objConnection);
        //GV Test: SqlCommand objCommand = new SqlCommand("UspSearchVendorTotalAdmin", objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.AddWithValue("@Checktype", CheckType);
        objCommand.Parameters.AddWithValue("@VendorID", VendorId);
        objCommand.Parameters.AddWithValue("@VendorNoJDE", vendorJDENumber);
        objCommand.Parameters.AddWithValue("@TradingNo", TradingNumber);
        objCommand.Parameters.AddWithValue("@TaxId", TaxId);
        objCommand.Parameters.AddWithValue("@Status", Status);
        objCommand.Parameters.AddWithValue("@Csicodes", CSICodes);
        objCommand.Parameters.AddWithValue("@states", States);
        objCommand.Parameters.AddWithValue("@Prjtypes", ProjectTypes);
        
        objCommand.Parameters.AddWithValue("@OtherState", OtherState);
        objCommand.Parameters.AddWithValue("@OtherStateId",OtherStateId);
        objCommand.Parameters.AddWithValue("@Certification", Certification);
        objCommand.Parameters.AddWithValue("@FederalSB", FederalSB);
        return Convert.ToInt32(objCommand.ExecuteScalar());
       
        
    }

    /// <summary>
    /// G. Vera 04/18/2012 Item 4.4 Phase 3 - Need to use different Stored proc for Admin search now due to initital poor code design by SGS
    /// 001 - Modified
    /// Get the count of search details
    /// 004 - Modified
    /// </summary>
    /// <param name="CheckType"></param>
    /// <param name="VendorId"></param>
    /// <param name="vendorJDENumber"></param>
    /// <param name="TradingNumber"></param>
    /// <param name="TaxId"></param>
    /// <param name="Status"></param>
    /// <param name="CSICodes"></param>
    /// <param name="States"></param>
    /// <param name="ProjectTypes"></param>
    /// <param name="SortExp"></param>
    /// <param name="OtherState"></param>
    /// <param name="OtherStateId"></param>
    /// <param name="Certification"></param>
    /// <param name="FederalSB"></param>
    /// <param name="FirstName"></param>
    /// <param name="LastName"></param>
    /// <param name="Email"></param>
    /// <returns></returns>
    public int GetDetailsCountAdmin(string CheckType, string VendorId, string vendorJDENumber, string TradingNumber, string TaxId, string Status, 
                               string CSICodes, string States, string ProjectTypes, string SortExp, string OtherState, int OtherStateId,
                               string Certification, string FederalSB, string FirstName, string LastName, string Email, string OtherProject = "", 
                               string iso = "", string qs = "", string tradeQuestId = "", bool providesBIM = false, string Continents = "", string Countries = "", string CompanyTypes="")
    {
        SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand("UspSearchVendorTotal", objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.AddWithValue("@Checktype", CheckType);
        objCommand.Parameters.AddWithValue("@VendorID", (VendorId != "") ? VendorId : null);
        objCommand.Parameters.AddWithValue("@VendorNoJDE", (vendorJDENumber != "") ? vendorJDENumber: null);
        objCommand.Parameters.AddWithValue("@TradingNo", (TradingNumber != "") ?TradingNumber : null);
        objCommand.Parameters.AddWithValue("@TaxId", (TaxId != "") ?TaxId : null);
        objCommand.Parameters.AddWithValue("@Status", (Status != "") ?Status : null);
        objCommand.Parameters.AddWithValue("@Csicodes", (CSICodes != "") ?CSICodes : null);
        objCommand.Parameters.AddWithValue("@states", (States != "") ?States : null);
        objCommand.Parameters.AddWithValue("@Continents", (Continents != "") ? Continents : null); //008
        objCommand.Parameters.AddWithValue("@Countries", (Countries != "") ? Countries : null); //008
        objCommand.Parameters.AddWithValue("@Prjtypes", (ProjectTypes != "") ?ProjectTypes : null);
        objCommand.Parameters.AddWithValue("@PrjOtherExpl", (OtherProject != "") ?OtherProject : null);  //004
        objCommand.Parameters.AddWithValue("@OtherState", (OtherState != "") ?OtherState : null);
        objCommand.Parameters.AddWithValue("@OtherStateId", OtherStateId);
        objCommand.Parameters.AddWithValue("@Certification", Certification);
        objCommand.Parameters.AddWithValue("@FederalSB", (FederalSB != "") ?FederalSB : null);
        objCommand.Parameters.AddWithValue("@FirstName", (FirstName != "") ?FirstName : null);        //001
        objCommand.Parameters.AddWithValue("@LastName", (LastName != "") ?LastName : null);          //001   
        objCommand.Parameters.AddWithValue("@Email", (Email != "") ?Email : null);                //001
        objCommand.Parameters.AddWithValue("@ISO", (iso != "") ?iso : null);                //002
        objCommand.Parameters.AddWithValue("@QS", (qs != "") ? qs : null);                //002
        objCommand.Parameters.AddWithValue("@TradeQuestId", (tradeQuestId != "") ? tradeQuestId : null);                //002
        objCommand.Parameters.AddWithValue("@ProvidesBIM", (providesBIM) ? "Y" : "");       //003
        //004
        objCommand.Parameters.AddWithValue("@CompanyTypes", CompanyTypes);


        //int r = 0;
        //using (VMSDAL.Haskell_VQF dcHaskell = new VMSDAL.Haskell_VQF())
        //{
        //    dcHaskell.Connection.Open();
        //    r =  dcHaskell.UspSearchVendorTotalAdmin(CheckType, VendorId, vendorJDENumber, TradingNumber, 
        //             TaxId, Status, CSICodes, States, ProjectTypes, OtherProject,OtherStateId, OtherState,        
        //             Certification, 
        //             FederalSB, 
        //             FirstName, 
        //             LastName, 
        //             Email, 
        //             iso,
        //             qs
        //         );
        //    dcHaskell.Connection.Close();
        //}

        //int r = objCommand.ExecuteNonQuery();
        int r = Convert.ToInt32(objCommand.ExecuteScalar());

        return r;


    }

    //display employee details
    public DAL.Employee[] DisplayEmployees()
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.Employee[] objEmployee = dcHaskell.Employees.ToArray();
            return objEmployee;
        }
    }


   //Get the vendorid based on the company name
    public DataSet GetvendorID(string VendorName)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DataSet dt = new DataSet();

            dt = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetVendorId",
                 new SqlParameter("@CompanyName", VendorName));
            return dt;

        }
    }

}
