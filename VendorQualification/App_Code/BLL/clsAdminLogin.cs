﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

/**************************************************************************************************
 * 001 G. Vera 09/21/2012: Modification (JIRA:VPI-69 & VPI-71)
 * 002 G. Vera 12/29/2016: New EMployee Role for admin sre
 * 
 **************************************************************************************************/

/// <summary>
/// Summary description for clsAdminLogin
/// </summary>
public class clsAdminLogin:Common
{
    //002
    public enum EmployeeRole
    {
        Admin_SRE, Admin, PM
    }

	public clsAdminLogin()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /// <summary>
    /// Get the userdetails based on the username and password given
    /// G. Vera 12/05/2012 - Changed as this is not working with password
    /// </summary>
    /// <param name="email"></param>
    /// <param name="Password"></param>
    /// <returns></returns>
    public VMSDAL.GetEmployeeAccessResult[] GetLoginDetails(string email, string Password)
    {
        using (VMSDAL.Haskell_VQF dcHaskell = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.GetEmployeeAccessResult[] objEmployeeAccess = dcHaskell.GetEmployeeAccess(email, Password).ToArray();
            return objEmployeeAccess;
         
        }
    }

    /// <summary>
    /// Get the employee emailid based on the employee id
    /// </summary>
    /// <param name="employeeId"></param>
    /// <returns></returns>
    public string GetEmployeeEmail(long employeeId)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (dcHaskell.Employees.Count(m => m.PK_EmployeeID == employeeId) == 1)
            {
                return Convert.ToString(dcHaskell.Employees.Single(m => m.PK_EmployeeID == employeeId).Email);
            }
            else { return null; }
        }
    }

    /// <summary>
    /// Get the employee name based on the employee number
    /// </summary>
    /// <param name="EmployeeNumber"></param>
    /// <returns></returns>
    public string GetEmployeeName(string EmployeeNumber)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (dcHaskell.Employees.Count(m => m.EmployeeNumber == EmployeeNumber) == 1)
            {
                return Convert.ToString(dcHaskell.Employees.Single(m => m.EmployeeNumber == EmployeeNumber).EmployeeName);
            }
            else { return null; }
        }
    }

    /// <summary>
    /// Get the employee name based on the employee ID
    /// 001 - Added
    /// </summary>
    /// <param name="EmployeeNumber"></param>
    /// <returns></returns>
    public string GetEmployeeName(long employeeID)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (dcHaskell.Employees.Count(m => m.PK_EmployeeID == employeeID) == 1)
            {
                return Convert.ToString(dcHaskell.Employees.Single(m => m.PK_EmployeeID == employeeID).EmployeeName);
            }
            else { return null; }
        }
    }

    /// <summary>
    /// Get user login information
    /// </summary>
    /// <param name="Email"></param>
    /// <returns></returns>
    public DataTable GetEmployeeInformation(string email)
    {
        DataSet ds = new DataSet();

        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "uspGetEmployeeInformation",
             new SqlParameter("@Email", email));

        return ds.Tables[0];
    }
}
