﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Xml.Linq;

/**************************************************************************************************
 * 001 G. Vera 10/25/2012 - VMS Stabilization Release 1.3 (see JIRA issues for details)
 * 
 * 
 **************************************************************************************************/

/// <summary>
/// Summary description for clsInsurance
/// </summary>
public class clsInsurance : CommonPage
{
    public clsInsurance()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// Insert Insurance
    /// </summary>
    /// <param name="Broker_Companyname"></param>
    /// <param name="InsuranceAgent"></param>
    /// <param name="AgentTelephoneNumber"></param>
    /// <param name="InsuranceCarrier"></param>
    /// <param name="GeneralEachoccurance"></param>
    /// <param name="Product_CompletedOperations"></param>
    /// <param name="GeneralAggregate"></param>
    /// <param name="AdditionalInsured"></param>
    /// <param name="EachAccident"></param>
    /// <param name="StateRequirements"></param>
    /// <param name="EmployeesLiability"></param>
    /// <param name="DiseaseEachEmployee"></param>
    /// <param name="DiseasePolicyLimit"></param>
    /// <param name="ProfessionalEachOccurance"></param>
    /// <param name="BondingSuretyCompanyName"></param>
    /// <param name="CompanyContactName"></param>
    /// <param name="ContactTelephoneNumber"></param>
    /// <param name="TotalBondingCapacity"></param>
    /// <param name="CurrentBondingCapacity"></param>
    /// <param name="VendorId"></param>
    /// <param name="InsuranceStatus"></param>
    /// <returns>Inserted or Error</returns>
    public bool InsertInsurance(string Broker_Companyname, string InsuranceAgent, string AgentTelephoneNumber, string InsuranceCarrier, string GeneralEachoccurance, 
                                string Product_CompletedOperations, string GeneralAggregate, bool? AdditionalInsured, string EachAccident, string EmployersLiability, 
                                string DiseaseEachEmployee, string DiseasePolicyLimit, string ProfessionalEachOccurance, string BondingSuretyCompanyName, string CompanyContactName, 
                                string ContactTelephoneNumber, string TotalBondingCapacity, string CurrentBondingCapacity,long VendorId,Byte InsuranceStatus,bool? IsPolicy,bool?
                                IsGeneral, bool? UmbrellaExcess, string Umbrella, string Excess, bool? WCpolicy, bool? IsMarineCargoIns, bool? IsInlandTransit, bool? IsInstallEquip,
                                bool? IsMarineCargoStorage,string ConvLimitUSD,string ConvLimitInlandTrans,bool InsuranceAgentIsBasedInUS, bool ContactIsBasedInUS, string pollutionLiabilityAmt, string pollutionLiabExplain)//005-Sooraj
    {
            //001 - modified to use new DAL and added the necesary parameters.
        using (VMSDAL.Haskell_VQF dcHaskell = new VMSDAL.Haskell_VQF())
            {
                try
                {
                    VMSDAL.VQFInsurance objInsurance = new VMSDAL.VQFInsurance
                           {
                               Broker_CompanyName = Broker_Companyname,
                               InsuranceAgent = InsuranceAgent,
                               AgentTelephoneNumber = AgentTelephoneNumber,
                               InsuranceCarrier = InsuranceCarrier,
                               GeneralEachOccurrence = GeneralEachoccurance,
                               Product_CompletedOperations = Product_CompletedOperations,
                               GeneralAggregate = GeneralAggregate,
                               IsPolicy = IsPolicy,
                               IsGeneral = IsGeneral,
                               Umbrella_Excess = UmbrellaExcess,
                               Umbrella = Umbrella,
                               Excess = Excess,
                               WCPolicy = WCpolicy,
                               AdditionalInsured = AdditionalInsured,
                               EachAccident = EachAccident,
                               EmployersLiability = EmployersLiability,
                               DiseaseEachEmployee = DiseaseEachEmployee,
                               DiseasePolicyLimit = DiseasePolicyLimit,
                               ProfessionalEachOccurrence = ProfessionalEachOccurance,
                               Bonding_SuretyCompanyName = BondingSuretyCompanyName,
                               CompanyContactName = CompanyContactName,
                               ContactTelephoneNumber = ContactTelephoneNumber,
                               TotalBondingCapacity = TotalBondingCapacity,
                               CurrentBondingCapacity = CurrentBondingCapacity,
                               FK_VendorID = VendorId,
                               InsuranceStatus = Convert.ToByte(InsuranceStatus),
                               //001 - added below
                               LastModifiedDT = DateTime.Now,
                               CreatedDT = DateTime.Now,
                               IsMarineCargoInsurance = IsMarineCargoIns,   
                               IsInlandTransit = IsInlandTransit,
                               IsInstallationOfEquip = IsInstallEquip,
                               IsStorage = IsMarineCargoStorage,
                               ConveyanceLimitMarineInsurance = ConvLimitUSD,
                               ConveyanceLimitInlandTransit = ConvLimitInlandTrans,
                               InsuranceAgentIsBasedInUS = InsuranceAgentIsBasedInUS,
                               ContactIsBasedInUS=ContactIsBasedInUS,
                               PollutionPolicyAmount = (String.IsNullOrEmpty(pollutionLiabilityAmt)) ? default(decimal?) : Convert.ToDecimal(pollutionLiabilityAmt),
                               PollutionPolicyExplain = pollutionLiabExplain
                    };

                    dcHaskell.VQFInsurances.Add(objInsurance);
                    dcHaskell.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                    throw;
                }
            }
    }



    /// <summary>
    /// Get Insurance Data based on vendor number
    /// 001 - modifed DAL
    /// </summary>
    /// <param name="VendorUserID"></param>
    /// <returns>single row </returns>

    public VMSDAL.VQFInsurance GetSingleVQLInsuranceDate(long VendorUserID)
    {
        using (VMSDAL.Haskell_VQF datacontext = new VMSDAL.Haskell_VQF())
        {
            if (datacontext.VQFInsurances.Count(m => m.FK_VendorID == VendorUserID) == 0)
            {
                return null;
            }
            else
            {

                return datacontext.VQFInsurances.Single(m => m.FK_VendorID == VendorUserID);
            }
        }
    }
    /// <summary>
    /// Get Count of insurance based on vendorid, If more than 1 value is return record is updated else inserted
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns>Count of Vendor</returns>
    public int GetInsuranceCount(long VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetCountInsuranceResult[] Result = datacontext.UspGetCountInsurance(VendorId).ToArray();
            return Convert.ToInt32(Result[0].InsuranceCount);
        }
    }

    //To get Insurance status for particular vendor
    public byte GetInsuranceStatusCount(long VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetInsuranceStatusResult[] Result = datacontext.UspGetInsuranceStatus(VendorId).ToArray();
            if (Result.Length > 0)
            {
                return Convert.ToByte(Result[0].InsuranceStatus);
            }
            else
            {
                return 0;
            }
        }
    }
    /// <summary>
    /// Update the insurance record based on the vendor id
    /// </summary>
    /// <param name="Broker_Companyname"></param>
    /// <param name="InsuranceAgent"></param>
    /// <param name="AgentTelephoneNumber"></param>
    /// <param name="InsuranceCarrier"></param>
    /// <param name="GeneralEachoccurance"></param>
    /// <param name="Product_CompletedOperations"></param>
    /// <param name="GeneralAggregate"></param>
    /// <param name="AdditionalInsured"></param>
    /// <param name="EachAccident"></param>
    /// <param name="StateRequirements"></param>
    /// <param name="EmployeesLiability"></param>
    /// <param name="DiseaseEachEmployee"></param>
    /// <param name="DiseasePolicyLimit"></param>
    /// <param name="ProfessionalEachOccurance"></param>
    /// <param name="BondingSuretyCompanyName"></param>
    /// <param name="CompanyContactName"></param>
    /// <param name="ContactTelephoneNumber"></param>
    /// <param name="TotalBondingCapacity"></param>
    /// <param name="CurrentBondingCapacity"></param>
    /// <param name="InsuranceId"></param>
    /// <param name="InsuranceStatus"></param>
    /// <returns>true or false to know record is inserted or error thrown</returns>
    public bool UpdateInsurance(string Broker_Companyname, string InsuranceAgent, string AgentTelephoneNumber, string InsuranceCarrier, string GeneralEachoccurance, 
                                string Product_CompletedOperations, string GeneralAggregate, bool? AdditionalInsured, string EachAccident, string StateRequirements, 
                                string EmployersLiability, string DiseaseEachEmployee, string DiseasePolicyLimit, string ProfessionalEachOccurance, string BondingSuretyCompanyName, 
                                string CompanyContactName, string ContactTelephoneNumber, string TotalBondingCapacity, string CurrentBondingCapacity, long InsuranceId,Byte InsuranceStatus,
                                bool? IsPolicy,bool? IsGeneral,bool? IsUmbrellaExcess, string Umbrella,string Excess,bool? IsWCPolicy, bool? IsMarineCargoIns, bool? IsInlandTransit,
                                bool? IsInstallEquip, bool? IsMarineCargoStorage, string ConvLimitUSD, string ConvLimitUSDInlandTrans, bool InsuranceAgentIsBasedInUS,bool contactIsBasedInUS, string pollutionLiabilityAmt, string pollutionLiabExplain)//005
    {
        //001 - modified to use new DAL and added necesary parameters
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {

            try
            {
                VMSDAL.VQFInsurance updInsurance = Context.VQFInsurances.Single(m => m.PK_InsuranceID == InsuranceId);

                updInsurance.Broker_CompanyName = Broker_Companyname;
                updInsurance.InsuranceAgent = InsuranceAgent;
                updInsurance.AgentTelephoneNumber = AgentTelephoneNumber;
                updInsurance.InsuranceCarrier = InsuranceCarrier;
                updInsurance.GeneralEachOccurrence = GeneralEachoccurance;
                updInsurance.Product_CompletedOperations = Product_CompletedOperations;
                updInsurance.GeneralAggregate = GeneralAggregate;
                updInsurance.IsPolicy = IsPolicy;
                updInsurance.IsGeneral = IsGeneral;
                updInsurance.Umbrella_Excess = IsUmbrellaExcess;
                updInsurance.Umbrella = Umbrella;
                updInsurance.Excess = Excess;
                updInsurance.WCPolicy = IsWCPolicy;
                updInsurance.AdditionalInsured = AdditionalInsured;
                updInsurance.EachAccident = EachAccident;
                updInsurance.EmployersLiability = EmployersLiability;
                updInsurance.DiseaseEachEmployee = DiseaseEachEmployee;
                updInsurance.DiseasePolicyLimit = DiseasePolicyLimit;
                updInsurance.ProfessionalEachOccurrence = ProfessionalEachOccurance;
                updInsurance.Bonding_SuretyCompanyName = BondingSuretyCompanyName;
                updInsurance.CompanyContactName = CompanyContactName;
                updInsurance.ContactTelephoneNumber = ContactTelephoneNumber;
                updInsurance.TotalBondingCapacity = TotalBondingCapacity;
                updInsurance.CurrentBondingCapacity = CurrentBondingCapacity;
                updInsurance.InsuranceStatus = Convert.ToByte(InsuranceStatus);
                updInsurance.LastModifiedDT = DateTime.Now;
                //001 - added below
                updInsurance.IsMarineCargoInsurance = IsMarineCargoIns;
                updInsurance.IsInlandTransit = IsInlandTransit;
                updInsurance.IsInstallationOfEquip = IsInstallEquip;
                updInsurance.IsStorage = IsMarineCargoStorage;
                updInsurance.ConveyanceLimitMarineInsurance = ConvLimitUSD;
                updInsurance.ConveyanceLimitInlandTransit = ConvLimitUSDInlandTrans;
                updInsurance.InsuranceAgentIsBasedInUS = InsuranceAgentIsBasedInUS;
                updInsurance.ContactIsBasedInUS = contactIsBasedInUS;
                updInsurance.PollutionPolicyAmount = (String.IsNullOrEmpty(pollutionLiabilityAmt)) ? default(decimal?) : Convert.ToDecimal(pollutionLiabilityAmt);
                updInsurance.PollutionPolicyExplain = pollutionLiabExplain;

                Context.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
                
            return true;
        }
    }


}
