﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsScopes
/// </summary>
public class clsScopes:Common
{
    public clsScopes()
    {
        //
        // TODO: Add constructor logic here
        //
    }
   //Insert Scopes
    public string InsertScopes(string ScopesName,string ParentId,string Root,string RootId,int Status,long ScopeId)
    {
       
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "USPSaveScopes",
                 new SqlParameter("@Name", ScopesName),
                 new SqlParameter("@Parent", ParentId),
                 new SqlParameter("@Root", Root),
                  new SqlParameter("@RootId", RootId),
                 new SqlParameter("@Pk_ScopeId", ScopeId),
                 new SqlParameter("@Flag", Status));
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToString(ds.Tables[0].Rows[0]["Result"]);
            }
            else
            {
                return null;
            }
        
        
    }
}