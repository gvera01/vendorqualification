﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Linq;
using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;

/*************************************************************************************************
 * 001 - G. Vera 10/16/2012: VMS Stabilization Release 1.3 (JIRA:VPI-58)
 * 002 - G. Vera 02/13/2013: New Features 1.3.1 (JIRA:VPI-93)
 * 003 - G. Vera 05/29/2014:  Design BIM Questions
 * 
 *************************************************************************************************/

/// <summary>
/// Summary description for clsTradeInfomation
/// </summary>

public class clsTradeInfomation : Common
{
    public long TradeID { get; set; }
    public long VentID { get; set; }

    public clsTradeInfomation()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// Get Scopes based on the parent Id
    /// </summary>
    /// <param name="ParentId"></param>
    /// <returns>Scopes</returns>
    public DAL.UspGetTradeScopesResult[] GetTradeScope(int ParentId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetTradeScopesResult[] ResTradeScopes = datacontext.UspGetTradeScopes(ParentId).ToArray();
            return ResTradeScopes;
        }
    }


    /// <summary>
    /// Get Scopes based on parent id and code type (i.e. PMMI, Master Format, etc...)
    /// 001 - Implemented
    /// </summary>
    /// <param name="ParentId"></param>
    /// <param name="CodeType"></param>
    /// <returns></returns>
    public VMSDAL.UspGetTradeScopesByTypeResult[] GetTradeScopeByType(int ParentId, ScopesCodeType CodeType)
    {
        using (VMSDAL.Haskell_VQF datacontext = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.UspGetTradeScopesByTypeResult[] ResTradeScopes = datacontext.UspGetTradeScopesByType(ParentId, CodeType.ToString()).ToArray();
            return ResTradeScopes;
        }
    }

    /// <summary>
    /// Fetch regions
    /// </summary>
    /// <returns></returns>
    public DataSet GetRegions()
    {

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetState");
        return ds;

    }
    /// <summary>
    /// Fetch regions1
    /// </summary>
    /// <returns></returns>
    public DataSet GetRegionsState()
    {

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetRegionsState");
        return ds;

    }
    /// <summary>
    /// Get Trade Date based on the vendor id
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public DAL.UspGetVQFTradeResult[] GetVQFTrade(int VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetVQFTradeResult[] ResTrade = datacontext.UspGetVQFTrade(VendorId).ToArray();
            return ResTrade;
        }
    }
    /// <summary>
    /// Get License Number based on the vendor id
    /// </summary>
    /// <param name="TradeId"></param>
    /// <returns></returns>
    public DAL.UspGetVQFTradeLicenseNumberResult[] GetVQFTradeLicenseNumber(long TradeId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetVQFTradeLicenseNumberResult[] ResTradeLicenseNumber = datacontext.UspGetVQFTradeLicenseNumber(TradeId).ToArray();
            return ResTradeLicenseNumber;
        }
    }
    /// <summary>
    /// Get trade projects based on the vendor id
    /// 001 - modified
    /// </summary>
    /// <param name="TradeId"></param>
    /// <returns></returns>
    public VMSDAL.UspGetVQFTradeProjectsResult[] GetVQFTradeProjects(long TradeId)
    {
        using (VMSDAL.Haskell_VQF datacontext = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.UspGetVQFTradeProjectsResult[] ResTradeProjects = datacontext.UspGetVQFTradeProjects(TradeId).ToArray();
            return ResTradeProjects;
        }
    }


    /// <summary>
    /// Get trade regions
    /// </summary>
    /// <param name="TradeId"></param>
    /// <returns></returns>
    public DAL.UspGetVQFTradeRegionsResult[] GetVQFTradeRegions(long TradeId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetVQFTradeRegionsResult[] ResTradeRegions = datacontext.UspGetVQFTradeRegions(TradeId).ToArray();
            return ResTradeRegions;
        }
    }
    /// <summary>
    /// Get trade scopes based on the trade id
    /// </summary>
    /// <param name="TradeId"></param>
    /// <returns></returns>
    public DAL.UspGetVQFTradeScopesResult[] GetVQFTradeScopes(long TradeId, long ParentId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetVQFTradeScopesResult[] ResTradeScopes = datacontext.UspGetVQFTradeScopes(TradeId, ParentId).ToArray();
            return ResTradeScopes;
        }
    }

    /// <summary>
    /// Get Level 1 scopes.
    /// </summary>
    /// <param name="Level1"></param>
    /// <param name="RootId"></param>
    /// <returns></returns>
    public DAL.UspGetTradeScopesLevel1Result[] GetTradeScopeLevel1(string Level1, int RootId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetTradeScopesLevel1Result[] ResTradeScopesLevel1 = datacontext.UspGetTradeScopesLevel1(Level1, RootId).ToArray();
            return ResTradeScopesLevel1;
        }
    }


    /// <summary>
    /// Get Level Heading for scopes
    /// </summary>
    /// <param name="RootId"></param>
    /// <returns></returns>
    public DAL.UspGetTradeScopesHeadingResult GetTradeScopeHeading(int RootId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetTradeScopesHeadingResult[] ResTradeScopesHeading = datacontext.UspGetTradeScopesHeading(RootId).ToArray();

            return ResTradeScopesHeading[0];
        }
    }
    /// <summary>
    /// Get trade scopes selected by the logged in vendor
    /// </summary>
    /// <param name="TradeId"></param>
    /// <returns></returns>
    public DAL.USPGetSelectedTradescopesHeadingResult[] GetSelectedTradeScopesHeading(long TradeId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.USPGetSelectedTradescopesHeadingResult[] ResTradeScopesHeading = datacontext.USPGetSelectedTradescopesHeading(TradeId).ToArray();

            return ResTradeScopesHeading;
        }
    }
    /// <summary>
    /// Get trade projects selected by the logged in vendor
    /// </summary>
    /// <param name="TradeId"></param>
    /// <returns></returns>
    public DAL.UspGetSelectedProjectsResult[] GetSelectedTradeProjects(long TradeId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetSelectedProjectsResult[] ResTradeScopesHeading = datacontext.UspGetSelectedProjects(TradeId).ToArray();

            return ResTradeScopesHeading;
        }
    }

    /// <summary>
    /// 001 - implemented
    /// </summary>
    /// <param name="TradeId"></param>
    /// <returns></returns>
    public VMSDAL.UspGetSelectedProjectsResult[] GetSelectedTradeProjects(long TradeId, bool other = true)
    {
        using (VMSDAL.Haskell_VQF datacontext = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.UspGetSelectedProjectsResult[] ResTradeScopesHeading = datacontext.UspGetSelectedProjects(TradeId).ToArray();

            return ResTradeScopesHeading;
        }
    }
    /// <summary>
    /// Get selected license number by the logged in vendor
    /// </summary>
    /// <param name="TradeId"></param>
    /// <returns></returns>
    public DAL.UspGetSelectedLicenseNumberResult[] GetSelectedTradeLicenseNumbers(long TradeId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            var list = datacontext.UspGetSelectedLicenseNumber(TradeId);
            if (list != null)
            {
                DAL.UspGetSelectedLicenseNumberResult[] ResTradeScopesHeading = list.ToArray();

                return ResTradeScopesHeading;
            }
            return null;
        }
    }
    /// <summary>
    /// Get selected trade regions by the vendor
    /// </summary>
    /// <param name="ParentId"></param>
    /// <param name="TradeId"></param>
    /// <returns></returns>
    public DAL.UspGetSelectedRegionsResult[] GetSelectedTradeRegions(int ParentId, long TradeId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetSelectedRegionsResult[] ResTradeScopesHeading = datacontext.UspGetSelectedRegions(ParentId, TradeId).ToArray();

            return ResTradeScopesHeading;
        }
    }
    /// <summary>
    /// get selected scopes by the vendor
    /// </summary>
    /// <param name="ParentId"></param>
    /// <returns></returns>
    public DAL.USPGetSelectedTradescopesResult[] GetSelectedTradeScopes(long ParentId, long VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.USPGetSelectedTradescopesResult[] ResTradeScopesHeading = datacontext.USPGetSelectedTradescopes(ParentId, VendorId).ToArray();

            return ResTradeScopesHeading;
        }
    }
    /// <summary>
    /// Get trade projects
    /// </summary>
    /// <returns></returns>
    public DAL.UspGetTradeProjectsResult[] GetTradeProjects()
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetTradeProjectsResult[] retval = datacontext.UspGetTradeProjects().ToArray();
            //001 - we need to exclude the "Other" type of project
            var q = retval.Where(p => p.ProjectName.Trim().ToUpper() != "OTHER");
            retval = q.ToArray<DAL.UspGetTradeProjectsResult>();
            return retval;
        }
    }

    /// <summary>
    /// Get trade project by name
    /// 001 - Implemented
    /// </summary>
    /// <returns></returns>
    public DAL.UspGetTradeProjectsResult[] GetTradeProject(string ProjectName)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetTradeProjectsResult[] retval = datacontext.UspGetTradeProjects().Where(p => p.ProjectName.Trim().ToUpper() == ProjectName.ToUpper()).ToArray();
            return retval;
        }
    }
    /// <summary>
    /// Get trade count
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public int GetTradeCount(int VendorId)
    {
        using (DAL.HaskellDataContext datacontact = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetCountTradeResult[] retval = datacontact.UspGetCountTrade(VendorId).ToArray();
            return Convert.ToInt32(retval[0].TradeCount);
        }
    }

    //To get Trade status for particular vendor
    public byte GetTradeStatusCount(long VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetTradeStatusResult[] Result = datacontext.UspGetTradeStatus(VendorId).ToArray();
            if (Result.Length > 0)
            {
                return Convert.ToByte(Result[0].TradeStatus);
            }
            else
            {
                return 0;
            }
        }
    }
    /// <summary>
    /// insert trade scopes
    /// </summary>
    /// <param name="scopes"></param>
    /// <param name="TradeId"></param>
    /// <returns>return false for error and true for successful insertion</returns>
    public bool InsertTradeScopes(int scopes, long TradeId)
    {
        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.VQFTradeScope objTradeScope = new DAL.VQFTradeScope
            {
                ScopeID = scopes,
                FK_TradeID = TradeID
            };
            dcHaskell.VQFTradeScopes.InsertOnSubmit(objTradeScope);
            dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }

    }
    /// <summary>
    /// Insert trade projects
    /// </summary>
    /// <param name="ProjectId"></param>
    /// <param name="TradeId"></param>
    /// <returns>return false for error and true for successful insertion</returns>
    public bool InsertTradeProjects(int ProjectId, long TradeId)
    {

        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.VQFTradeProject objTradeProject = new DAL.VQFTradeProject
            {
                FK_ProjectID = ProjectId,
                FK_TradeID = TradeID,
                //001 - added
                CreatedDT = DateTime.Now
            };
            dcHaskell.VQFTradeProjects.InsertOnSubmit(objTradeProject);
            dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }

    }

    /// <summary>
    /// Insert "Other" trade project
    /// </summary>
    /// <param name="ProjectId"></param>
    /// <param name="TradeId"></param>
    /// <param name="Explanation"></param>
    /// <returns></returns>
    public bool InsertOtherTradeProject(long TradeId, string Explanation)
    {
        using (VMSDAL.Haskell_VQF dcHaskell = new VMSDAL.Haskell_VQF())
        {

            VMSDAL.VQFTradeProject objTradeProject = new VMSDAL.VQFTradeProject
            {
                FK_ProjectID = (this.GetTradeProject("Other"))[0].PK_ProjectID,
                FK_TradeID = TradeID,
                OtherProjectName = Explanation.Trim(),
                CreatedDT = DateTime.Now
            };
            dcHaskell.VQFTradeProjects.Add(objTradeProject);
            dcHaskell.SaveChanges();
            return true;

        }
    }
    /// <summary>
    /// Insert trade regions
    /// </summary>
    /// <param name="RegionId"></param>
    /// <param name="OtherRegion"></param>
    /// <param name="TradeId"></param>
    /// <returns>return false for error and true for successful insertion</returns>
    public bool InsertTradeRegions(short RegionId, string OtherRegion, long TradeId)
    {

        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.VQFTradeRegion objTradeRegions = new DAL.VQFTradeRegion
            {
                FK_State = RegionId,
                OtherSate = OtherRegion,
                FK_TradeID = TradeID
            };
            dcHaskell.VQFTradeRegions.InsertOnSubmit(objTradeRegions);
            dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }

    }

    /// <summary>
    /// insert trade license numbers
    /// </summary>
    /// <param name="ExpirationDate"></param>
    /// <param name="State"></param>
    /// <param name="LicenseNumber"></param>
    /// <param name="OtherState"></param>
    /// <param name="TradeId"></param>
    /// <returns>return false for error and true for successful insertion</returns>
    public bool InsertTradeLicenseNumber(DateTime? ExpirationDate, short State, string LicenseNumber, string OtherState, long TradeId,short Country)
    {


        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DateTime? expiration;
            if (Convert.ToString(ExpirationDate) == "1/1/0001 12:00:00 AM")
            {
                expiration = Convert.ToDateTime("1/1/1753");
            }
            else
            {
                expiration = ExpirationDate;
            }
            DAL.VQFTradeLicenseNumber objTradeLicenseNumber = new DAL.VQFTradeLicenseNumber
            {


                ExpirationDate = expiration,
                FK_State = State,
                LicenseNumber = LicenseNumber,
                OtherState = OtherState,
                FK_TradeID = TradeID,
                FK_Country = Country

            };
            dcHaskell.VQFTradeLicenseNumbers.InsertOnSubmit(objTradeLicenseNumber);
            dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }

    }
    /// <summary>
    /// Insert trade
    /// </summary>
    /// <param name="OwnForces"></param>
    /// <param name="Explain"></param>
    /// <param name="VendorId"></param>
    /// <param name="Status"></param>
    /// <returns>return trade id</returns>
    public long InsertTrade(bool? OwnForces, string Explain, long VendorId, Byte Status)
    {

        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.VQFTrade objTrade = new DAL.VQFTrade
            {
                OwnForces = OwnForces,
                Explain = Explain,
                FK_VendorID = VendorId,
                TradeStatus = Status

            };
            dcHaskell.VQFTrades.InsertOnSubmit(objTrade);
            dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);


            TradeID = dcHaskell.VQFTrades.Single(m => m.FK_VendorID == VendorId).PK_TradeID;

            return TradeID;


        }



    }
    /// <summary>
    /// Update trade
    /// </summary>
    /// <param name="OwnForces"></param>
    /// <param name="Explain"></param>
    /// <param name="TradeId"></param>
    /// <param name="status"></param>
    /// <returns>return false for error and true for successful updation</returns>
    public bool UpdateTrade(bool? OwnForces, string Explain, long TradeId, Byte status)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {


            DAL.VQFTrade updTrade = Context.VQFTrades.Single(m => m.PK_TradeID == TradeId);
            updTrade.Explain = Explain;
            updTrade.OwnForces = OwnForces;
            updTrade.TradeStatus = status;

            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }
    }

    //public bool UpdateTradeScopes()
    //{
    //    using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
    //    { 
    //        DAL.VQFTradeScope updTradeScopes=Context.VQFTradeScopes.Single(m=> m.PK_ScopesID
    //    }
    //}




    /// <summary>
    /// Delete all related trade
    /// </summary>
    /// <param name="TradeID"></param>
    /// <returns>return false for error and true for successful deletion</returns>
    public bool DeleteAllRelationsofTradeID(long TradeID)
    {
        //001 - added try catch
        try
        {
            using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
            {

                Context.VQFTradeScopes.DeleteAllOnSubmit(Context.VQFTradeScopes.Where(j => j.FK_TradeID == TradeID));
                //001 - changed to use the new DAL: Context.VQFTradeProjects.DeleteAllOnSubmit(Context.VQFTradeProjects.Where(j => j.FK_TradeID == TradeID));
                Context.VQFTradeRegions.DeleteAllOnSubmit(Context.VQFTradeRegions.Where(j => j.FK_TradeID == TradeID));
                Context.VQFTradeLicenseNumbers.DeleteAllOnSubmit(Context.VQFTradeLicenseNumbers.Where(j => j.FK_TradeID == TradeID));
                Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            }


            //001 - added
            using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
            {
                foreach (var o in Context.VQFTradeProjects.Where(j => j.FK_TradeID == TradeID))
                {
                    Context.VQFTradeProjects.Remove(o);
                }

                Context.SaveChanges();
            }

        }
        catch (Exception e)
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get Single VQF Trade Result
    /// 002 - changed DAL
    /// </summary>
    /// <param name="VendorID"></param>
    /// <returns></returns>
    public VMSDAL.VQFTrade GetSingleVQFTradeResult(long VendorID)
    {
        using (VMSDAL.Haskell_VQF datacontextTradeResult = new VMSDAL.Haskell_VQF())
        {
            if (datacontextTradeResult.VQFTrades.Count(C => C.FK_VendorID == VendorID) == 0)
            {
                return null;
            }
            else
            {

                return datacontextTradeResult.VQFTrades.Single(C => C.FK_VendorID == VendorID);
            }
        }
    }


    /// <summary>
    /// Compare Trade Scope
    /// </summary>
    /// <param name="TradeID"></param>
    /// <returns></returns>
    public DataSet GetTradeScopeCompare(Int64 TradeID)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspCompareScopes", new SqlParameter("@Trade_Id", TradeID));
        return ds;
    }


    /// <summary>
    /// Compare Trade License Number
    /// </summary>
    /// <param name="TradeID"></param>
    /// <returns></returns>
    public DataSet GetTradeLicenseCompare(Int64 TradeID)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetVQFTradeLicenseNumber", new SqlParameter("@TradeId", TradeID));
        return ds;
    }

    /// <summary>
    /// 001 - implemented
    /// 002 - changed by adding section parameter
    /// </summary>
    /// <returns></returns>
    public List<VMSDAL.VQFQuestion> GetVQFQuestions(VQFSection section)
    {
        string qSection = section.ToString();

        using (VMSDAL.Haskell_VQF datacontextTradeResult = new VMSDAL.Haskell_VQF())
        {

            var result = datacontextTradeResult.VQFQuestions.Where(q => q.IsActive == true && q.VQFSection == qSection).ToList();

            return result;
        }
    }

    /// <summary>
    /// 001 - implemented
    /// </summary>
    /// <returns></returns>
    public List<VMSDAL.VQFTradeQuestion> GetVQFTradeQuestions(long tradeId)
    {
        using (VMSDAL.Haskell_VQF datacontextTradeResult = new VMSDAL.Haskell_VQF())
        {
            return datacontextTradeResult.VQFTradeQuestions.Where(q => q.FK_TradeID == tradeId).ToList();
        }
    }

    /// <summary>
    /// 001 - implemeted
    /// Will return an object to be used in VQF Trade Scope screen with the question and the answer(s)
    /// 002 - modified
    /// </summary>
    /// <param name="tradeId"></param>
    /// <returns></returns>
    public List<VendorTradeQuestions> GetTradeScopeQuestions(long tradeId)
    {
        using (VMSDAL.Haskell_VQF datacontextTradeResult = new VMSDAL.Haskell_VQF())
        {
            VendorTradeQuestions vendorQ;
            List<VendorTradeQuestions> vendorQs = new List<VendorTradeQuestions>();

            var questions = this.GetVQFQuestions(VQFSection.TradeScope);        //002
            var tradeQ = datacontextTradeResult.VQFTradeQuestions.Where(q => q.FK_TradeID == tradeId).ToList();

            foreach (VMSDAL.VQFQuestion item in questions)
            {
                vendorQ = new VendorTradeQuestions();
                vendorQ.Question = item.Question;
                vendorQ.QuestionID = item.PK_QuestionID;
                vendorQ.VqfSection = item.VQFSection;                   //002
                vendorQ.QuestionOrder = (double)item.QuestionOrder;     //002

                if (tradeQ.Count > 0)
                {
                    var answers = tradeQ.Where(vq => vq.FK_QuestionId == item.PK_QuestionID).ToList();
                    if (answers.Count > 0)
                    {
                        vendorQ.YesNoAnswer = answers[0].YesNoAnswer;
                        vendorQ.OtherAnswer = answers[0].OtherAnswer;
                    }
                    else
                    {
                        vendorQ.YesNoAnswer = string.Empty;
                        vendorQ.OtherAnswer = string.Empty;
                    }
                }
                else
                {
                    vendorQ.YesNoAnswer = string.Empty;
                    vendorQ.OtherAnswer = string.Empty;
                }

                vendorQs.Add(vendorQ);
            }

            return vendorQs;
        }
    }

    /// <summary>
    /// 002 - implemeted
    /// Will return an object to be used in VQF Trade BIM screen with the question and the answer(s)
    /// 003
    /// </summary>
    /// <param name="tradeId"></param>
    /// <returns></returns>
    public List<VendorTradeQuestions> GetTradeBIMQuestions(long tradeId, VQFSection section = VQFSection.TradeBIM)
    {
        using (VMSDAL.Haskell_VQF datacontextTradeResult = new VMSDAL.Haskell_VQF())
        {
            VendorTradeQuestions vendorQ;
            List<VendorTradeQuestions> vendorQs = new List<VendorTradeQuestions>();

            var questions = this.GetVQFQuestions(section);  //003
            var tradeQ = datacontextTradeResult.VQFTradeQuestions.Where(q => q.FK_TradeID == tradeId).ToList();
            questions = questions.OrderBy(o => o.QuestionOrder).ToList();

            foreach (VMSDAL.VQFQuestion item in questions)
            {
                vendorQ = new VendorTradeQuestions();
                vendorQ.Question = item.Question;
                vendorQ.QuestionID = item.PK_QuestionID;
                vendorQ.VqfSection = item.VQFSection;
                vendorQ.QuestionOrder = (double)item.QuestionOrder;

                if (tradeQ.Count > 0)
                {
                    var answers = tradeQ.Where(vq => vq.FK_QuestionId == item.PK_QuestionID).ToList();
                    if (answers.Count > 0)
                    {
                        vendorQ.YesNoAnswer = answers[0].YesNoAnswer;
                        vendorQ.OtherAnswer = answers[0].OtherAnswer;
                    }
                    else
                    {
                        vendorQ.YesNoAnswer = string.Empty;
                        vendorQ.OtherAnswer = string.Empty;
                    }
                }
                else
                {
                    vendorQ.YesNoAnswer = string.Empty;
                    vendorQ.OtherAnswer = string.Empty;
                }

                vendorQs.Add(vendorQ);
            }

            return vendorQs;
        }
    }


    /// <summary>
    /// 001 - implemented
    /// If vendor has records it will delete all related questions first and then re-insert
    /// values.  Otherwise, it will only insert values.
    /// 002 - modified delete logic
    /// </summary>
    /// <param name="vendorQuestions"></param>
    /// <param name="tradeId"></param>
    /// <returns></returns>
    public int InsertVQFTradeQuestion(List<VendorTradeQuestions> vendorQuestions, long tradeId)
    {
        //sooraj: Modified to delete question with Question id and Trade id 
        if (vendorQuestions.Count > 0)
        {
            using (VMSDAL.Haskell_VQF datacontextTradeResult = new VMSDAL.Haskell_VQF())
            {
                int deleted = 0;    //002
                foreach (var item in vendorQuestions)
                {

                    var tq = datacontextTradeResult.VQFTradeQuestions.FirstOrDefault(q => q.FK_TradeID == tradeId && q.FK_QuestionId == item.QuestionID);
                       
                           if(tq!=null)
                                datacontextTradeResult.VQFTradeQuestions.Remove(tq);
                                deleted++;
                }

                if (deleted > 0) datacontextTradeResult.SaveChanges();     //002
                    

                foreach (var item in vendorQuestions)
                {
                    VMSDAL.VQFTradeQuestion tQ = new VMSDAL.VQFTradeQuestion();
                    tQ.FK_QuestionId = item.QuestionID;
                    tQ.FK_TradeID = tradeId;
                    tQ.YesNoAnswer = item.YesNoAnswer;
                    tQ.OtherAnswer = item.OtherAnswer;
                    tQ.CreatedDate = DateTime.Now;
                    tQ.ModifiedDate = DateTime.Now;

                    datacontextTradeResult.VQFTradeQuestions.Add(tQ);
                }

                return datacontextTradeResult.SaveChanges();
            }
        }
        else return 0;
    }


    /// <summary>
    /// 002 - implemented
    /// </summary>
    /// <param name="scopeID"></param>
    /// <returns></returns>
    public string GetTradeScopeTOPParentID(int scopeID, ScopesCodeType type)
    {
        string scopeType = type.ToString();

        using (VMSDAL.Haskell_VQF datacontext = new VMSDAL.Haskell_VQF())
        {
            var scope = (from s in datacontext.VQFScopes where s.PK_ScopeID == scopeID && s.ScopeCodeType == scopeType  select s).ToList();
            if (scope.Count > 0)
            {
                int? rootNode = scope[0].RootNode;

                var rootParent = (from s1 in datacontext.VQFScopes
                                  where s1.RootNode == rootNode &&
                                  s1.ParentID.Trim() == "-1" &&
                                  s1.ScopeCodeType.Trim() == scopeType
                                  select s1).ToList();

                if (rootParent.Count > 0) return rootParent[0].PK_ScopeID.ToString();
                else return "";
            }
            return "";
        }
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="tradeId"></param>
    /// <returns></returns>
    public List<VendorTradeContient> GetVQFTradeRegionContinents(long tradeId)
    {
        using (VMSDAL.Haskell_VQF datacontextTradeResult = new VMSDAL.Haskell_VQF())
        {

            var result = (from c in datacontextTradeResult.VQFTradeRegions
                          join m in datacontextTradeResult.Continents
                          on c.FK_ContinentID equals m.PK_ContinentId
                          where c.FK_TradeID == tradeId
                          select new VendorTradeContient
                          {
                              TradeID = c.FK_TradeID,
                              ContinentID = c.FK_ContinentID,
                              CountryID = c.FK_CountryID,
                              TradeRegionContinentID = c.PK_RegionsID,
                              SpecificRegion = c.OtherSate,
                              ContinentName = m.ContinentName
                          }

                          ).ToList();
            return result;
        }
    }


    /// <summary>
    /// 006 - Sooraj - Trade Region query for selecting country inside continent 
    /// </summary>
    /// <param name="tradeId"></param>
    /// <returns></returns>
    public List<VendorTradeContient> GetVQFTradeRegionContinentsView(long tradeId)
    {
        using (VMSDAL.Haskell_VQF datacontextTradeResult = new VMSDAL.Haskell_VQF())
        {

            var result =(from r in datacontextTradeResult.VQFTradeRegions
                          where r.FK_TradeID == tradeId
                          group r by r.FK_ContinentID into g
                          join c in datacontextTradeResult.Continents
                          on g.Key equals c.PK_ContinentId
                          let keyValue=g.Select(y=>y.FK_CountryID.Value)
                          select new VendorTradeContient
                          {
                              ContinentID = g.Key,
                              ContinentName =c.ContinentName,
                              CountryList = c.Countries.Where(cv => keyValue.Contains(cv.PK_CountryId)).Select(x => x.CountryName)
                          }
                          ).ToList();
            var finalList= result.Select(x=> new VendorTradeContient
                          {
                              ContinentName = x.ContinentName,
                              CountryName = string.Format("<li>{0}", x.CountryList.Aggregate((m,n)=>m+"</li><li>"+n+"</li>"))
        
                        }).ToList();
            return finalList.OrderBy(x => x.ContinentName).ToList();
        }
    }

    public List<VendorTradeContient> GetVQFTradeRegionContinentsCompare(long tradeId)
    {
        using (VMSDAL.Haskell_VQF datacontextTradeResult = new VMSDAL.Haskell_VQF())
        {

            var result = (from r in datacontextTradeResult.VQFTradeRegions
                          join c in datacontextTradeResult.Continents
                         on r.FK_ContinentID equals c.PK_ContinentId
                          join j in datacontextTradeResult.Countries
                          on r.FK_CountryID equals j.PK_CountryId
                          where r.FK_TradeID == tradeId
                          select new VendorTradeContient
                          {
                              ContinentName = c.ContinentName,
                              CountryName = j.CountryName,
                          }
                          ).ToList();

            return result;
        }
    }

    public  string TrimEnd( string input, string suffixToRemove)
    {
        if (input != null && suffixToRemove != null
          && input.EndsWith(suffixToRemove))
        {
            return input.Substring(0, input.Length - suffixToRemove.Length);
        }
        else return input;
    }



    /// <summary>
    /// 006 - Insert Regions 
    /// </summary>
    /// <param name="continent"></param>
    public bool InsertVQFTradeRegionContinents(VendorTradeContient continent)
    {

        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.VQFTradeRegion objTradeRegions = new DAL.VQFTradeRegion
            {
          FK_ContinentID = continent.ContinentID,
            FK_CountryID = continent.CountryID,
          OtherSate = continent.SpecificRegion,
                FK_TradeID = TradeID
            };
            dcHaskell.VQFTradeRegions.InsertOnSubmit(objTradeRegions);
            dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
       
        }

        return true;
    }
}

/// <summary>
/// 001 - implemented
/// 002 - add
/// </summary>
public class VendorTradeContient
{
    public long TradeID { get; set; }
    public int? ContinentID { get; set; }
    public int? CountryID { get; set; }
    public long TradeRegionContinentID { get; set; }
    public string ContinentName { get; set; }
    public string CountryName { get; set; }
    public IEnumerable<string> CountryList { get; set; }
    public string SpecificRegion { get; set; }
   
}

/// <summary>
/// 001 - implemented
/// 002 - add
/// </summary>
public class VendorTradeQuestions
{
    private string question;

    public string Question
    {
        get { return question; }
        set { question = value; }
    }
    private string yesNoAnswer;

    public string YesNoAnswer
    {
        get { return yesNoAnswer; }
        set { yesNoAnswer = value; }
    }
    private string otherAnswer;

    public string OtherAnswer
    {
        get { return otherAnswer; }
        set { otherAnswer = value; }
    }

    private long questionID;

    public long QuestionID
    {
        get { return questionID; }
        set { questionID = value; }
    }

    private string vqfSection;

    public string VqfSection
    {
        get { return vqfSection; }
        set { vqfSection = value; }
    }

    private double questionOrder;

    public double QuestionOrder
    {
        get { return questionOrder; }
        set { questionOrder = value; }
    }
}


/// <summary>
/// 002 - implemented
/// </summary>
public enum VQFSection
{
    TradeScope, TradeBIM, TradeBIMDesign
}
