﻿
using DAL;
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Data.Linq;

/********************************************************************************************************
 * 001 - G. Vera 05/25/2012: Change the UpdateVendorBit() method to handle the ExpiryDT value
 *       correctly. 
 * 002 - G. Vera 12/04/2012: VMS Stabilization Release 1.3 (see JIRA issues for more details)
 * 003 - G. Vera 02/21/2013: Emergency Hot Fix on DeleteVendor function
 * 004 - G. Vera 07/01/2014:  New overload method to GetVendorDetails with Id as parameter
 * 005 G. vera 07/09/2014: Modify Last Modifed Date when vendor saves information
 *********************************************************************************************************/



/// <summary>
/// Summary description for clsRegistration
/// </summary>
public class clsRegistration : CommonPage
{
	public clsRegistration()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    /// <summary>
    /// Get company details based on company name and tax number
    /// </summary>
    /// <param name="CompanyName"></param>
    /// <param name="TaxNumber"></param>
    /// <returns>company with name and tax number</returns>
    public DAL.USPGetCompanyResult[] GetCompany(string CompanyName, string TaxNumber,bool IsBasedInUS)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.USPGetCompanyResult[] ResGetCompany = datacontext.USPGetCompany(CompanyName, TaxNumber, IsBasedInUS).ToArray();

            return ResGetCompany;
        }
    }

    /// <summary>
    /// Get the vendor details based on the name and tax number
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="TaxNumber"></param>
    /// <returns></returns>
    public string GetCompanyDetails(string Name, string TaxNumber)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            return Convert.ToString(datacontext.Vendors.Count(m => m.Company == Name || m.TaxID == TaxNumber));
        }
    }

    /// <summary>
    /// Get the vendorid by name
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public DAL.Vendor GetVendorIDByName(string Name)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (datacontext.Vendors.Count(m => m.Company == Name) > 0)
            {
                DAL.Vendor objVendor = datacontext.Vendors.Single(m => m.Company == Name);
                return objVendor;
            }
            else
            {
                return null;
            }
        }
    }

    /// <summary>
    /// Change the password status
    /// </summary>
    /// <param name="VendorID"></param>
    public void ChangePasswordStatus(long VendorID)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.Vendor updVendor = datacontext.Vendors.Single(m => m.PK_VendorID == VendorID);
            updVendor.PasswordStatus = false;


            datacontext.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }
    }

    /// <summary>
    /// Get vendor status
    /// </summary>
    /// <param name="VendorID"></param>
    public byte GetVendorStatus(long VendorID)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.Vendor updVendor = datacontext.Vendors.Single(m => m.PK_VendorID == VendorID);
            return updVendor.VendorStatus;
        }
    }

    /// <summary>
    /// Get vendor details based on the vendorid
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns>returns the vendor details</returns>
    public DAL.UspGetVendorDetailsByIdResult[] GetVendor(long VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetVendorDetailsByIdResult[] resGetVendor = datacontext.UspGetVendorDetailsById(VendorId).ToArray();

            return resGetVendor;
        }
    }

    public string GetVendorCompanyType(long VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            string resGetVendorType = datacontext.VQFCompanies.Where(m => m.FK_VendorID == VendorId).ToArray().Length > 0 ? datacontext.VQFCompanies.Where(m => m.FK_VendorID == VendorId).ToArray()[0].TypeOfCompany : "";

            return resGetVendorType;
        }
    }
    /// <summary>
    /// Get vendor details based on the vendorid
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns>returns the vendor details</returns>
    public DAL.Vendor GetVendorByName(string VendorName)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            if (datacontext.Vendors.Count(m => m.Company == VendorName) == 1)
            {
                return datacontext.Vendors.Single(m => m.Company == VendorName);
            }
            else
            {
                return null;
            }
          
        }
    }
    /// <summary>
    /// Get the vendor details
    /// </summary>
    /// <returns></returns>
    public DAL.Vendor[] GetVendorDetails()
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.Vendor[] objVendor = datacontext.Vendors.ToArray();
            return objVendor;
        }
    }

    /// <summary>
    /// Display the vendor using wildcards
    /// </summary>
    /// <param name="Prefix"></param>
    /// <returns></returns>
    public DataSet GetVendor(string Prefix)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetVendorToSearch",
             new SqlParameter("@prefix", Prefix));
        return ds;
    }
    /// <summary>
    /// Display the vendor using wildcards
    /// </summary>
    /// <param name="Prefix"></param>
    /// <returns></returns>
    public DataSet GetEmployee(string Prefix)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetEmployee",
             new SqlParameter("@prefix", Prefix));
        return ds;
    }
    /// <summary>
    /// Get password for email address
    /// </summary>
    /// <param name="emailaddress"></param>
    /// <returns></returns>
    public DAL.UspGetPasswordResult[] GetPassword(string emailaddress)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetPasswordResult[] resGetPassword = datacontext.UspGetPassword(emailaddress).ToArray();
            return resGetPassword;
           
        } 
    }

/// <summary>
/// Get the Vendor Id for the given username and password
/// </summary>
/// <param name="email"></param>
/// <param name="password"></param>
/// <returns>vendorid</returns>
    public long GetVendorId(string email,string password)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.GetVendorIdResult[] resGetVendorId = datacontext.GetVendorId(email,password).ToArray();
            if (resGetVendorId.Length > 0)
            {
                return resGetVendorId[0].PK_VendorID;
            }
            else
            {
                return 0;
            }
        }
    }
    /// <summary>
    /// Update teh Vendor status 
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public bool UpdateVendorBit(long VendorId,byte status)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            /***********************
             * Changed by G. Vera on 02/14/2012
             * Reverted back the try/catch/finally
             * since is in use now.
             ***********************/
            try
            {
                DAL.Vendor updVendor = Context.Vendors.Single(m => m.PK_VendorID == VendorId);
            
                updVendor.VendorStatus = status;
                updVendor.LastModifiedDT = DateTime.Now;


                //Changed by N Schoenberger on 10/28/2011 ************************************
                //001 - Commented out this code: updVendor.ExpiryDT = DateTime.Now.AddYears(1);
                // ***************************************************************************

                if (status == 2 || status == 1)
                {
                    updVendor.ExpiryDT = DateTime.Now.AddYears(1);  //001 Added - The ExpiryDT field should be updated based on the status passed
                    updVendor.SubmittedDate = DateTime.Now;
                }
                else if (status == 3)
                {
                    updVendor.ExpiryDT = DateTime.Now.AddDays(10);  //001 - If changed to Revision, ExpiryDT should be 10 days from current date
                }
                else if (status == 0)
                {
                    if(updVendor.RegisteredDate != null)
                        updVendor.ExpiryDT = ((DateTime)updVendor.RegisteredDate).AddDays(90);  //001 - If status is in progress, expiry date should be 90 days from registration.
                }

                Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                Context.Dispose();
            }
        }
    }

    /// <summary>
    /// Added by N Schoenberger on 11/30/2011 to allow admin vendor match NOT to update modify/ expiry dates
    /// Update the Vendor status - Do NOT update last modified/ expiry dates
    /// </summary>  
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public bool UpdateVendorBitStatus(long VendorId, byte status)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.Vendor updVendor = Context.Vendors.Single(m => m.PK_VendorID == VendorId);

            updVendor.VendorStatus = status;

            if (status == 2 || status == 1)
            {
                updVendor.SubmittedDate = DateTime.Now;
            }
            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;
        }
    }

    /// <summary>
    /// Activate the vendor
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    public bool ActivateVendor(long VendorId, byte status,byte VendorStatus)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
          
            DAL.Vendor updVendor = Context.Vendors.Single(m => m.PK_VendorID == VendorId);
            updVendor.VendorStatus = status;
            updVendor.ExpiryStatus = VendorStatus;  
            //SGS changes on 08/25/2011
            //updVendor.LastModifiedDT = DateTime.Now;
            //

            updVendor.RegisteredDate = DateTime.Now;
            //SGS changes on 08/26/2011
            updVendor.ExpiryDT = DateTime.Now.AddDays(10); 
            //
            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;
          
        }
    }


    /// <summary>
    /// Expired the vendor
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    public bool ExpiredActivateVendor(long VendorId, byte status, byte VendorStatus)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.Vendor updVendor = Context.Vendors.Single(m => m.PK_VendorID == VendorId);
        
            updVendor.VendorStatus = status;
            updVendor.ExpiryStatus = VendorStatus;   
            //updVendor.LastModifiedDT = DateTime.Now;
            //updVendor.RegisteredDate = DateTime.Now;
            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }
    }


    /// <summary>
    /// Expired Activate vendor Date 
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public bool ExpiredActivateDate(long VendorId)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.Vendor updVendor = Context.Vendors.Single(m => m.PK_VendorID == VendorId);
            //updVendor.LastModifiedDT = DateTime.Now;
            updVendor.RegisteredDate = DateTime.Now;
            updVendor.ExpiryDT = DateTime.Now.AddYears(1); 
            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }
    }


    /// <summary>
    /// Change password of the vendor
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="Password"></param>
    /// <returns></returns>
    public bool UpdateVendorPassword(long VendorId, string Password)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.Vendor updVendor = Context.Vendors.Single(m => m.PK_VendorID == VendorId);
            updVendor.Password = Password;
            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;
         }
    }

    public DAL.GetVendorStatusResult[] RetrieveVendorStatus()
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.GetVendorStatusResult[] objVendorResult = Context.GetVendorStatus().ToArray();
            return objVendorResult;
        }
    }

    /// <summary>
    /// delete vendor from haskell
    /// </summary>
    /// <param name="VendorId">unique key for the vendor</param>
    /// <returns>int</returns>
    //public int Deletevendor(long VendorId)
    //{
       
    //    using (DAL.HaskellAdminDataContext Context = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
    //    {
            
    //        UspDeleteVendorResult[] objResult = Context.UspDeleteVendor(VendorId).ToArray();
    //       //ISingleResult<= Context.UspDeleteVendor(VendorId);
    //        return Convert.ToInt32(objResult[0].DeletedRows);
    //      //returnvalue = Convert.ToInt32(objDeleteResult.DeletedRows);
        
    //    }
        
      
    //}

    /// <summary>
    /// delete vendor from haskell
    /// 003 - changed to use the new DAL dll.
    /// </summary>
    /// <param name="VendorId">unique key for the vendor</param>
    /// <returns>int</returns>
    public int Deletevendor(long VendorId)
    {

        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {

            int res = Context.UspDeleteVendor(VendorId);
            //ISingleResult<= Context.UspDeleteVendor(VendorId);
            return res;
            //returnvalue = Convert.ToInt32(objDeleteResult.DeletedRows);

        }


    }

    //public bool UpdateVendorPassword(string FirstName,string LastName,long VendorId,string email, string Password,string PreviousPassword)

    //The above code was commented and the below code was included by SGS for Task no : 13

    /// <summary>
    /// UPdate vendor profile
    /// </summary>
    /// <param name="FirstName"></param>
    /// <param name="LastName"></param>
    /// <param name="VendorId"></param>
    /// <param name="email"></param>
    /// <param name="Password"></param>
    /// <returns></returns>
    public string UpdateVendorPassword(string VendorName,string Company,string TaxId,string FirstName,string LastName,long VendorId,string email, string Password,string PreviousPassword,bool IsBasedInUS)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
           
            int count = Context.Vendors.Count(m => m.Email == email && m.PK_VendorID != VendorId);

            if (count == 0)
            {
                if (Context.Vendors.Count(m => m.Company == Company && m.PK_VendorID != VendorId) == 0)
                {
                    //if (Context.Vendors.Count(m => m.TaxID == TaxId && m.PK_VendorID != VendorId) == 0)
                    //{
                        DAL.Vendor updVendor = Context.Vendors.Single(m => m.PK_VendorID == VendorId);
                        //Included by SGS for Inssue no : 13 Starts here
                        updVendor.TaxID = TaxId;
                        updVendor.Company = Company;
                        //Ends here
                        updVendor.FirstName = FirstName;
                        updVendor.LastName = LastName;
                        updVendor.Email = email;
                        updVendor.Password = Password;
                        updVendor.IsBasedInUS = IsBasedInUS;
                        //updVendor.DuplicateTaxId = DuplicateTaxID;
                        if (PreviousPassword != Password)
                        {
                            updVendor.PasswordStatus = true;
                        }
                        Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);

                        if (Context.VQFCompanies.Count(m => m.FK_VendorID == VendorId) == 1)
                        {
                            DAL.VQFCompany updCompany = Context.VQFCompanies.Single(m => m.FK_VendorID == VendorId);
                            //Included by SGS for Inssue no : 13 Starts here
                            updCompany.FederalTaxID = TaxId;
                            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                        }
                        return "Profile has been changed for the vendor " + VendorName;
                    //}
                    //else
                    //{
                    //    return "<li> Taxid already been used by another vendor" + "</li>";
                    //}
                }
                else
                { return "<li> Company name already been used by another vendor" + "</li>"; }
            }
            else
            {
                return "<li> Email address already been used by another vendor" + "</li>";
            }
        
        }
    }


    /// <summary>
    /// get the count of tax id, if tax is 0 the record can be inserted msg should be thrown stating tax id already exits
    /// </summary>
    /// <param name="TaxId"></param>
    /// <returns>tax id</returns>
    public string CheckRedundancy(string CompanyName,string TaxId,string emailAddress,bool IsBasedInUS)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspCheckRedundancyResult[] objCheckRedundancy = datacontext.UspCheckRedundancy(CompanyName, emailAddress, TaxId, IsBasedInUS).ToArray();
           return Convert.ToString(objCheckRedundancy[0].result);
        }

    }

   
       

/// <summary>
/// Check document
/// </summary>
/// <param name="VendorId"></param>
/// <param name="FileName"></param>
/// <returns></returns>
    public int CheckDocument(long VendorId, string FileName)
    {
        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspCheckDocumentResult[] objDocument = dcHaskell.UspCheckDocument(VendorId, FileName).ToArray();
            if (objDocument.Length > 0)
            {
                return Convert.ToInt32(objDocument[0].CountDocument);
            }
            else
            {
                return 0;
            }

        }
    }

    /// <summary>
    /// // Added by N Schoenberger 12/28/2011 for "Terms and Conditions Agreement" 
    /// </summary>
    public DataSet GetTermsAndConditionsAgreement(string MessageName)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "uspGetTermsAndConditionsAgreement",
             new SqlParameter("@MessageName", MessageName));
        return ds;
    }

    public DataSet GetSignedTermsAndConditionsAgreement(Int64 vendorId, Int64 supplierId)
    {
        DataSet ds = new DataSet();

        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "uspGetSignedTermsAndConditionsAgreement",
             new SqlParameter("@vendorId", vendorId),
             new SqlParameter("@supplierId", supplierId));

        return ds;
    }

    public bool HasAcceptedTermsAgreement(Int64 vendorId)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.Vendor oVendor = Context.Vendors.Single(m => m.PK_VendorID == vendorId);

            return oVendor.HasAcceptedTermsConditions;
        }
    }

    /// <summary>
    /// 002 - Implemented
    /// </summary>
    /// <param name="vendorId"></param>
    /// <returns></returns>
    public bool? IsNewRequirementMessage(Int64 vendorId)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.Vendor oVendor = Context.Vendors.Single(m => m.PK_VendorID == vendorId);

            return oVendor.NewRequirementAlert;
        }
    }

    /// <summary>
    /// 002 - implemented
    /// </summary>
    /// <param name="vendorId"></param>
    /// <returns></returns>
    public bool UpdateNewRequirmentMessageField(Int64 vendorId, bool value)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            try
            {
                VMSDAL.Vendor oVendor = Context.Vendors.Single(m => m.PK_VendorID == vendorId);
                oVendor.NewRequirementAlert = value;
                Context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    public bool UpdateHasAcceptedTermsAgreement(Int64 vendorId, Int32 messageId)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DateTime acceptDate = DateTime.Now;

            DAL.Vendor oVendor = Context.Vendors.Single(m => m.PK_VendorID == vendorId);
            {
            oVendor.HasAcceptedTermsConditions = true;
            oVendor.HasAcceptedTermsConditionsDate = acceptDate;

            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            }
            //Insert terms & conditions history table
            DAL.TermsConditionsAccepted oTermsConditions = new DAL.TermsConditionsAccepted();
            {
                oTermsConditions.FK_VendorId = oVendor.PK_VendorID;
                oTermsConditions.FK_MessageId = messageId;
                oTermsConditions.VendorFirstName = oVendor.FirstName;
                oTermsConditions.VendorLastName = oVendor.LastName;
                oTermsConditions.VendorEmail = oVendor.Email;
                oTermsConditions.AcceptedDate = acceptDate;

            }
            Context.TermsConditionsAccepteds.InsertOnSubmit(oTermsConditions);
            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);

            return oVendor.HasAcceptedTermsConditions;           
        }
    }
    
    public bool ChangeHasAcceptedTermsAgreement(String messageTitle, String messageText, bool vendorRenew)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.MessageInfo oMessageInfo = new MessageInfo();
            {
                oMessageInfo.MessageName = "TermsAndConditions";
                oMessageInfo.MessageTitle = messageTitle;
                oMessageInfo.MessageText = messageText;
            }

            Context.MessageInfos.InsertOnSubmit(oMessageInfo);
            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }

        if (vendorRenew == true)
        {
            //update all vendors to force terms & conditions re-agreement
            int updated = 0;
            updated = SqlHelper.ExecuteNonQuery(con, CommandType.StoredProcedure, "uspResetVendorsMustAgreeTermsAndConditionsAgreement");
        }

        return true;
    }

    /// <summary>
    /// // End ddded by N Schoenberger 12/28/2011 for "Terms and Conditions Agreement" 
    /// </summary>

    #region AttachDoc-Part
    /// <summary>
    /// Insert Document
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="FileName"></param>
    /// <param name="Filepath"></param>
    /// <param name="PageName"></param>
    /// <returns>true or false to know record inserted or not</returns>
    public bool InsertInsuranceDocument(long VendorId, string FileName, string Filepath, bool isAdmin, bool isSRAdmin)
    {
       using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.VQFAttachDocument objDocument = new DAL.VQFAttachDocument
            {
                FK_VendorID = VendorId,
                FileName = FileName,
                FilePath = Filepath,
                isAdminUploaded = isAdmin,
                isSRAdminUploaded=isSRAdmin  
           

            };
            dcHaskell.VQFAttachDocuments.InsertOnSubmit(objDocument);
            dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;
        }
    }
    #endregion


    public VMSDAL.Vendor GetVendorDetails(long vendorId)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            try
            {
                VMSDAL.Vendor oVendor = Context.Vendors.Single(m => m.PK_VendorID == vendorId);

                return oVendor;
            }
            catch (Exception e)
            {
                 throw e;
            }
        }
    }

    /// <summary>
    /// 005 - added
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="date"></param>
    public void UpdateLastModifiedDate(long ID, DateTime date)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            try
            {
                VMSDAL.Vendor oVendor = Context.Vendors.Single(m => m.PK_VendorID == ID);

                oVendor.LastModifiedDT = (DateTime?)date;

                Context.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
