﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsCompanyCertification
/// </summary>
public class clsCompanyCertification
{
	public clsCompanyCertification()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DAL.uspListCompanyCertificationResult[] ListCompanyCertification()
    { 
        using(DAL.HaskellAdminDataContext objHaskell=new DAL.HaskellAdminDataContext())
        {
            return objHaskell.uspListCompanyCertification().ToArray();
        }
    }
    public bool InsertCompanyCertification(string Certification, bool isFederal,string Actions,int CertificationID)
    {
        using (DAL.HaskellAdminDataContext objHaskell = new DAL.HaskellAdminDataContext())
        {
            if (Actions == "Add")
            {
                if (!CheckDuplicate(Certification, isFederal, CertificationID))
                {
                    DAL.CompanyCertification objCertification = new DAL.CompanyCertification
                    {
                        Certification = Certification,
                        IsFederalSB = isFederal
                    };
                    objHaskell.CompanyCertifications.InsertOnSubmit(objCertification);
                    objHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                    return true;
                }
                else { return false; }
            }
            else if (Actions == "Edit")
            {
                if (!CheckDuplicate(Certification, isFederal, CertificationID))
                {
                    DAL.CompanyCertification objCertification = objHaskell.CompanyCertifications.Single(m => m.Pk_CompanyCertificationID == CertificationID);
                    objCertification.Certification = Certification;
                    objCertification.IsFederalSB = isFederal;
                    objHaskell.SubmitChanges();


                    return true;
                }
                else { return false; }
            }
            else if (Actions == "Delete")
            {
                objHaskell.CompanyCertifications.DeleteAllOnSubmit(objHaskell.CompanyCertifications.Where(m => m.Pk_CompanyCertificationID == CertificationID));
                objHaskell.SubmitChanges();
                return true;
            }
            else { return false; }
        }
    }
    public bool CheckDuplicate(string Certification, bool isFederal,int CertificationID)
    {
        using (DAL.HaskellAdminDataContext objHaskell = new DAL.HaskellAdminDataContext())
        {
            if (CertificationID == -1)
            {
                if (objHaskell.CompanyCertifications.Count(l => l.Certification == Certification && l.IsFederalSB == isFederal) > 0)
                {
                    return true;
                }
                else { return false; }
            }
            else
            {
                if (objHaskell.CompanyCertifications.Count(l => l.Certification == Certification && l.IsFederalSB == isFederal && l.Pk_CompanyCertificationID != CertificationID) > 0)
                {
                    return true;
                }
                else { return false; }
            }
        }  
    }
}
