﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;

/* 001 G. Vera 06/12/2014:  New FilePath column to store emr letter and osha document
 * 002 G. Vera 12/14/2016:  Add New Question answers to VQFSaftey
 */

/// <summary>
/// Summary description for clsSafety
/// </summary>
public class clsSafety : Common
{
    public long SafetyID { get; set; }
    public long VentID { get; set; }

    public clsSafety()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// Insert safety records
    /// </summary>
    /// <param name="CompanySafety"></param>
    /// <param name="CompanySafetyContactName"></param>
    /// <param name="CompanySafetyContactNumber"></param>
    /// <param name="EMROtherState1"></param>
    /// <param name="EMROtherState2"></param>
    /// <param name="EMROtherState3"></param>
    /// <param name="EMRRate1"></param>
    /// <param name="EMRRate2"></param>
    /// <param name="EMRRate3"></param>
    /// <param name="EMRYear1"></param>
    /// <param name="EMRYear2"></param>
    /// <param name="EMRYear3"></param>
    /// <param name="FK_EMRState1"></param>
    /// <param name="FK_EMRState2"></param>
    /// <param name="FK_EMRState3"></param>
    /// <param name="OSHAYear1"></param>
    /// <param name="OSHAYear2"></param>
    /// <param name="OSHAYear3"></param>
    /// <param name="SafetyProgram"></param>
    /// <param name="SafetyRequirements"></param>
    /// <param name="TotalFatalities1"></param>
    /// <param name="TotalFatalities2"></param>
    /// <param name="TotalFatalities3"></param>
    /// <param name="TotalNoDays1"></param>
    /// <param name="TotalNoDays2"></param>
    /// <param name="TotalNoDays3"></param>
    /// <param name="TotalNoJobTransfer1"></param>
    /// <param name="TotalNoJobTransfer2"></param>
    /// <param name="TotalNoJobTransfer3"></param>
    /// <param name="TotalNoOtherRecord1"></param>
    /// <param name="TotalNoOtherRecord2"></param>
    /// <param name="TotalNoOtherRecord3"></param>
    /// <param name="VendorId"></param>
    /// <param name="SafetyStatus"></param>
    /// <param name="SafetyRepresentation"></param> //005
    /// <param name="SafetyRepresentContact"></param>//005
    /// <param name="SafetyRepresentPhone"></param>//005
    /// <returns>return false for error and true for successful insertion</returns>
    public bool InsertSafety(bool? CompanySafety, string CompanySafetyContactName, string CompanySafetyContactNumber, bool? SafetyProgram, bool? SafetyRequirements, long VendorId, byte SafetyStatus, string SICNAIC, 
        bool? SICNAICNA, string Explanation, bool? SafetyRepresentation, string SafetyRepresentContact, string SafetyRepresentPhone, bool IsBasedInUS = true, bool IsRepersentBasedInUS = true, string ProLevelOfTaraining = "",
        bool? IsProvideEmployeeTrainingDocumentation = null, bool? IsWrittenDrugAlcoholProgram = null, 
        bool? IsPeriodicSafetyMeetings = null, string SafetyMeetingsPeriods = "", bool? IsSafetyInspectionOfWIP = null, string WhoConductsInspectionOfWIP = "", string InspectionOfWIPPeriods = "", 
        bool? isConductHazardsAnalysis = null, string conductHazardAnalysisPeriods = "") //002 new fields
    {
        using (VMSDAL.Haskell_VQF dcHaskell = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.VQFSafety objSafety = new VMSDAL.VQFSafety
            {
                FK_VendorID = VendorId,
                SafetyProgram = SafetyProgram,
                SafetyRequirements = SafetyRequirements,
                CompanySafety = CompanySafety,
                CompanySafetyContactName = CompanySafetyContactName,
                CompanySafetyContactNumber = CompanySafetyContactNumber,
                SafetyStatus = SafetyStatus,
                SICNAICSCode = SICNAIC,
                SICNA = SICNAICNA,
                Explanation = Explanation,
                SafetyRepresentation=SafetyRepresentation, //005
                SafetyRepresentationContactName = SafetyRepresentContact,//005
                SafetyRepresentationContactNumber = SafetyRepresentPhone,//005
                CompanyContactIsBasedInUS=IsBasedInUS, //006  
                RepresentContactIsBasedInUS=IsRepersentBasedInUS, //006
                //GV: edit status is being updated when vendor clicks submit or save to complete in the insurance section
                //002 - START
                ProfessionalLevelOfTraining = ProLevelOfTaraining,
                IsProvideEmployeeTrainingDocumentation = IsProvideEmployeeTrainingDocumentation,
                IsWrittenDrugAlcoholProgram = IsWrittenDrugAlcoholProgram,
                IsPeriodicSafetyMeetings = IsPeriodicSafetyMeetings,
                SafetyMeetingsPeriods = SafetyMeetingsPeriods,
                IsSafetyInspectionOfWIP = IsSafetyInspectionOfWIP,
                WhoConductsInspectionOfWIP = WhoConductsInspectionOfWIP,
                InspectionOfWIPPeriods = InspectionOfWIPPeriods,
                CreatedDT = DateTime.Now,
                LastModifiedDT = DateTime.Now,
                IsConductHazardAnalysis = isConductHazardsAnalysis,
                ConductHazardAnalysisPeriods = conductHazardAnalysisPeriods
                //002 - END



            };

            dcHaskell.VQFSafeties.Add(objSafety);
            dcHaskell.SaveChanges();

            if (dcHaskell.VQFSafeties.Count(m => m.FK_VendorID == VendorId) > 0)
            {
                SafetyID = Convert.ToInt64(dcHaskell.VQFSafeties.Single(m => m.FK_VendorID == VendorId).PK_SafetyID);
            }
            return true;


        }

    }

    //Insert EMR Records
    //001
    public void insertSafetyEMR(string EMRRate1, string EMRYear1, bool NA, string Explain)
    {
        using (VMSDAL.Haskell_VQF dcHaskell = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.VQFSafetyEMRRate objemrYearRate = new VMSDAL.VQFSafetyEMRRate
            {
                FK_SafetyID = SafetyID,
                EMRRate = EMRRate1,
                EMRYear = EMRYear1,
                NotAvailable = NA,
                Explanation = Explain,
                PastYearStatus=1,
                CreatedDT=DateTime.Now
            };

            dcHaskell.VQFSafetyEMRRates.Add(objemrYearRate);
            dcHaskell.SaveChanges();
        }
    }

    /// <summary>
    /// Get EMR based on the safety Id
    /// </summary>
    /// <param name="SafetyId"></param>
    /// <returns></returns>
    public DataTable GetSafetyEMR(long SafetyId)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "USPSelectSafetyEMRRate",
             new SqlParameter("@SafetyId", SafetyId));
        return ds.Tables[0];
    }

    /// <summary>
    /// Get OSHA based on the safetyId
    /// </summary>
    /// <param name="SafetyId"></param>
    /// <returns></returns>
    public DataTable GetSafetyOSHA(long SafetyId)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "USPSelectSafetyOsha",
             new SqlParameter("@SafetyId", SafetyId));
        return ds.Tables[0];
    }



    /// <summary>
    /// Update SafetyPastYear
    /// </summary>
    /// <param name="SafetyId"></param>
    /// <returns></returns>
    public Int32 SafetyPastYear(long SafetyId)
    {
        Int32 i = 0;
        i = SqlHelper.ExecuteNonQuery(con, CommandType.StoredProcedure, "UspUpdateSafetyPastYear", new SqlParameter("@SafetyId", SafetyId));
        return i;

    }

    //Insert OSHA
    //001
    public void InsertSafetyOsha(string OSHAYear1, string TotalFatalities1, string TotalNoDays1, string TotalNoJobTransfer1, string TotalNoOtherRecord1, 
                                 bool Na, string Explain, string TotalHours, bool hasOSHALog = true)
    {
        using (VMSDAL.Haskell_VQF dcHaskell = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.VQFSafetyOSHA objOsha = new VMSDAL.VQFSafetyOSHA
            {
                FK_SafetyID = SafetyID,
                OSHAYear = OSHAYear1,
                TotalFatalities = TotalFatalities1,
                TotalNoDays = TotalNoDays1,
                TotalNoJobTransfer = TotalNoJobTransfer1,
                TotalNoOtherRecord = TotalNoOtherRecord1,
                NotAvailable = Na,
                Explanation = Explain,
                TotalHoursWorked = TotalHours,
                PastYearStatus =1,
                CreatedDT=DateTime.Now,
                HasOSHALog = hasOSHALog
            };

            dcHaskell.VQFSafetyOSHAs.Add(objOsha);
            dcHaskell.SaveChanges();
        }
    }

    /// <summary>
    /// Get the safety id based on the vendor id
    /// </summary>
    /// <param name="VendorUserID"></param>
    /// <returns>safter data based on the vendor id</returns>
    public DAL.VQFSafety GetSingleVQLSafetyData(long VendorUserID)
    {
        using (DAL.HaskellDataContext datacontextSafety = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (datacontextSafety.VQFSafeties.Count(s => s.FK_VendorID == VendorUserID) == 0)
            {
                return null;
            }
            else
            {
                return datacontextSafety.VQFSafeties.Single(s => s.FK_VendorID == VendorUserID);
            }
        }

    }

    /// <summary>
    /// Get the safety id based on the vendor id
    /// </summary>
    /// <param name="VendorUserID"></param>
    /// <returns>safter data based on the vendor id</returns>
    public VMSDAL.VQFSafety GetSingleVQFSafetyData(long VendorUserID)
    {
        using (VMSDAL.Haskell_VQF datacontextSafety = new VMSDAL.Haskell_VQF())
        {
            if (datacontextSafety.VQFSafeties.Count(s => s.FK_VendorID == VendorUserID) == 0)
            {
                return null;
            }
            else
            {
                return datacontextSafety.VQFSafeties.Single(s => s.FK_VendorID == VendorUserID);
            }
        }

    }



    /// <summary>
    /// Get safety count
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns>get count of records in safety</returns>
    public int GetSafetyCount(long VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetCountSafetyResult[] Result = datacontext.UspGetCountSafety(VendorId).ToArray();
            return Convert.ToInt32(Result[0].SafetyCount);
        }
    }

    //To get Safety status for particular vendor
    public byte GetSafetyStatusCount(long VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetSafetyStatusResult[] Result = datacontext.UspGetSafetyStatus(VendorId).ToArray();
            if (Result.Length > 0)
            {
                return Convert.ToByte(Result[0].SafetyStatus);
            }
            else
            {
                return 0;
            }
        }
    }

    /// <summary>
    /// Get safety count
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns>get count of records in safety</returns>
    public void UpdateSafetyBit(long SafetyId, Byte Status)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.VQFSafety updSafety = Context.VQFSafeties.Single(m => m.PK_SafetyID == SafetyId);

            updSafety.EditStatus = Status;

            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }

    }

    /// <summary>
    /// 001 - added
    /// </summary>
    /// <param name="safetyID"></param>
    /// <param name="status"></param>
    public void UpdateSafetyStatus(long safetyID, Byte status)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.VQFSafety safety = Context.VQFSafeties.Single(s => s.PK_SafetyID == safetyID);
            safety.SafetyStatus = status;
            Context.SaveChanges();            
        }
    }



    /// <summary>
    /// UpdateSafetyOSHA past Year
    /// </summary>
    /// <param name="VendorId"></param>
    public void UpdateSafetyOSHABit(long SafetyId,Byte PastYearStatus,string Year )
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.VQFSafetyOSHA updSafetyOSHA = Context.VQFSafetyOSHAs.Single(m => m.FK_SafetyID == SafetyId && m.OSHAYear == Year);

            updSafetyOSHA.PastYearStatus = PastYearStatus;

            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }

    }



    /// <summary>
    /// UpdateSafetyEMRRate past Year
    /// </summary>
    /// <param name="VendorId"></param>
    public void UpdateSafetyEMRRateBit(long SafetyId,Byte PastYearStatus,string Year )
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.VQFSafetyEMRRate updSafetyEMRRate = Context.VQFSafetyEMRRates.Single(m => m.FK_SafetyID == SafetyId && m.EMRYear == Year);

            updSafetyEMRRate.PastYearStatus = PastYearStatus;

            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }

    }

    /// <summary>
    /// Update safety records
    /// </summary>
    /// <param name="CompanySafety"></param>
    /// <param name="CompanySafetyContactName"></param>
    /// <param name="CompanySafetyContactNumber"></param>
    /// <param name="EMROtherState1"></param>
    /// <param name="EMROtherState2"></param>
    /// <param name="EMROtherState3"></param>
    /// <param name="EMRRate1"></param>
    /// <param name="EMRRate2"></param>
    /// <param name="EMRRate3"></param>
    /// <param name="EMRYear1"></param>
    /// <param name="EMRYear2"></param>
    /// <param name="EMRYear3"></param>
    /// <param name="FK_EMRState1"></param>
    /// <param name="FK_EMRState2"></param>
    /// <param name="FK_EMRState3"></param>
    /// <param name="OSHAYear1"></param>
    /// <param name="OSHAYear2"></param>
    /// <param name="OSHAYear3"></param>
    /// <param name="SafetyProgram"></param>
    /// <param name="SafetyRequirements"></param>
    /// <param name="TotalFatalities1"></param>
    /// <param name="TotalFatalities2"></param>
    /// <param name="TotalFatalities3"></param>
    /// <param name="TotalNoDays1"></param>
    /// <param name="TotalNoDays2"></param>
    /// <param name="TotalNoDays3"></param>
    /// <param name="TotalNoJobTransfer1"></param>
    /// <param name="TotalNoJobTransfer2"></param>
    /// <param name="TotalNoJobTransfer3"></param>
    /// <param name="TotalNoOtherRecord1"></param>
    /// <param name="TotalNoOtherRecord2"></param>
    /// <param name="TotalNoOtherRecord3"></param>
    /// <param name="SafetyId"></param>
    /// <param name="SafetyStatus"></param>
    /// <param name="SafetyRepresentation"></param> //005
    /// <param name="SafetyRepresentContact"></param>//005
    /// <param name="SafetyRepresentPhone"></param>//005
    /// <returns>return false for error and true for successful updation</returns>
    public bool UpdateSafety(bool? CompanySafety, string CompanySafetyContactName, string CompanySafetyContactNumber, bool? SafetyProgram, bool? SafetyRequirements, long SafetyId, byte SafetyStatus,
        string SICNAIC, bool SICNA, string Explanation, bool? SafetyRepresentation, string SafetyRepresentContact, string SafetyRepresentPhone, bool IsBasedInUS = true, 
        bool IsRepresentBasedInUS = true, string ProLevelOfTaraining = "",
        bool? IsProvideEmployeeTrainingDocumentation = null, bool? IsWrittenDrugAlcoholProgram = null, bool? IsPeriodicSafetyMeetings = null, string SafetyMeetingsPeriods = "",
        bool? IsSafetyInspectionOfWIP = null, string WhoConductsInspectionOfWIP = "", string InspectionOfWIPPeriods = "", bool? isConductHazardsAnalysis = null, string conductHazardAnalysisPeriods = "") //002 new fields
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.VQFSafety updSafety = Context.VQFSafeties.SingleOrDefault(m => m.PK_SafetyID == SafetyId);
            updSafety.CompanySafety = CompanySafety;
            updSafety.CompanySafetyContactName = CompanySafetyContactName;
            updSafety.CompanySafetyContactNumber = CompanySafetyContactNumber;
            updSafety.SafetyProgram = SafetyProgram;
            updSafety.SafetyRequirements = SafetyRequirements;
            updSafety.SafetyStatus = SafetyStatus;
            updSafety.SICNAICSCode = SICNAIC;
            updSafety.SICNA = SICNA;
            updSafety.Explanation = Explanation;
            SafetyID = SafetyId;
            updSafety.LastModifiedDT = DateTime.Now;
            updSafety.SafetyRepresentation = SafetyRepresentation; //005
            updSafety.SafetyRepresentationContactName = SafetyRepresentContact;//005
            updSafety.SafetyRepresentationContactNumber = SafetyRepresentPhone;//005
            updSafety.CompanyContactIsBasedInUS = IsBasedInUS;//006 
            updSafety.RepresentContactIsBasedInUS = IsRepresentBasedInUS;//006
            //GV: edit status is being updated when vendor clicks submit or save to complete in the insurance section

            //002 - START
            updSafety.ProfessionalLevelOfTraining = ProLevelOfTaraining;
            updSafety.IsProvideEmployeeTrainingDocumentation = IsProvideEmployeeTrainingDocumentation;
            updSafety.IsWrittenDrugAlcoholProgram = IsWrittenDrugAlcoholProgram;
            updSafety.IsPeriodicSafetyMeetings = IsPeriodicSafetyMeetings;
            updSafety.SafetyMeetingsPeriods = SafetyMeetingsPeriods;
            updSafety.IsSafetyInspectionOfWIP = IsSafetyInspectionOfWIP;
            updSafety.WhoConductsInspectionOfWIP = WhoConductsInspectionOfWIP;
            updSafety.InspectionOfWIPPeriods = InspectionOfWIPPeriods;
            updSafety.IsConductHazardAnalysis = isConductHazardsAnalysis;
            updSafety.ConductHazardAnalysisPeriods = conductHazardAnalysisPeriods;
            //002 - END

            Context.SaveChanges();

            return true;

        }
    }

    //Update OSHA
    //001
    public void UpdateSafetyOsha(string OSHAYear1, string TotalFatalities1, string TotalNoDays1, string TotalNoJobTransfer1, 
                                 string TotalNoOtherRecord1, bool Na, string Explain, string TotalHours, bool hasOSHALog = true)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            if (Context.VQFSafetyOSHAs.Count(m => m.FK_SafetyID == SafetyID && m.OSHAYear == OSHAYear1) > 0)
            {
                VMSDAL.VQFSafetyOSHA updSafetyOsha = Context.VQFSafetyOSHAs.Single(m => m.FK_SafetyID == SafetyID && m.OSHAYear == OSHAYear1);
                updSafetyOsha.OSHAYear = OSHAYear1;
                updSafetyOsha.TotalFatalities = TotalFatalities1;
                updSafetyOsha.TotalNoDays = TotalNoDays1;
                updSafetyOsha.TotalNoJobTransfer = TotalNoJobTransfer1;
                updSafetyOsha.TotalNoOtherRecord = TotalNoOtherRecord1;
                updSafetyOsha.NotAvailable = Na;
                updSafetyOsha.Explanation = Explain;
                updSafetyOsha.TotalHoursWorked = TotalHours;
                updSafetyOsha.HasOSHALog = hasOSHALog;

                Context.SaveChanges();
            }
            else
            { InsertSafetyOsha(OSHAYear1, TotalFatalities1, TotalNoDays1, TotalNoJobTransfer1, TotalNoOtherRecord1, Na, Explain, TotalHours); }

        }
    }

    /// <summary>
    /// 001 - implemented
    /// </summary>
    /// <param name="OSHAYear1"></param>
    /// <param name="TotalFatalities1"></param>
    /// <param name="TotalNoDays1"></param>
    /// <param name="TotalNoJobTransfer1"></param>
    /// <param name="TotalNoOtherRecord1"></param>
    /// <param name="Na"></param>
    /// <param name="Explain"></param>
    /// <param name="TotalHours"></param>
    /// <param name="Path"></param>
    public void UpdateSafetyOsha(VMSDAL.VQFSafetyOSHA oshaObject)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            if (Context.VQFSafetyOSHAs.Count(m => m.FK_SafetyID == oshaObject.FK_SafetyID && m.OSHAYear == oshaObject.OSHAYear) > 0)
            {
                VMSDAL.VQFSafetyOSHA updSafetyOsha = Context.VQFSafetyOSHAs.Single(m => m.FK_SafetyID == oshaObject.FK_SafetyID && m.OSHAYear == oshaObject.OSHAYear);
                updSafetyOsha.OSHAYear = oshaObject.OSHAYear;
                updSafetyOsha.TotalFatalities = oshaObject.TotalFatalities;
                updSafetyOsha.TotalNoDays = oshaObject.TotalNoDays;
                updSafetyOsha.TotalNoJobTransfer = oshaObject.TotalNoJobTransfer;
                updSafetyOsha.TotalNoOtherRecord = oshaObject.TotalNoOtherRecord;
                updSafetyOsha.NotAvailable = oshaObject.NotAvailable;
                updSafetyOsha.Explanation = oshaObject.Explanation;
                updSafetyOsha.TotalHoursWorked = oshaObject.TotalHoursWorked;
                updSafetyOsha.FilePath = oshaObject.FilePath;
                updSafetyOsha.HasOSHALog = oshaObject.HasOSHALog;

                Context.SaveChanges();
            }
        }
    }

    //Update EMR
    //001
    public void UpdateSafetyEMR(string EMRRate1, string EMRYear1, bool chkNA, string Explain)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {


            if (Context.VQFSafetyEMRRates.Count(m => m.FK_SafetyID == SafetyID && m.EMRYear == EMRYear1) > 0)
            {
                VMSDAL.VQFSafetyEMRRate updSafetyEMR = Context.VQFSafetyEMRRates.Single(m => m.FK_SafetyID == SafetyID && m.EMRYear == EMRYear1);

                updSafetyEMR.EMRRate = EMRRate1;
                updSafetyEMR.EMRYear = EMRYear1;
                updSafetyEMR.NotAvailable = chkNA;
                updSafetyEMR.Explanation = Explain;

                Context.SaveChanges();
            }
            else
            { insertSafetyEMR(EMRRate1, EMRYear1, chkNA, Explain); }

        }
    }

    /// <summary>
    /// 001 - Implemented
    /// </summary>
    /// <param name="emrObject"></param>
    public void UpdateSafetyEMR(VMSDAL.VQFSafetyEMRRate emrObject)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {


            if (Context.VQFSafetyEMRRates.Count(m => m.FK_SafetyID == emrObject.FK_SafetyID && m.EMRYear == emrObject.EMRYear) > 0)
            {
                VMSDAL.VQFSafetyEMRRate updSafetyEMR = Context.VQFSafetyEMRRates.Single(m => m.FK_SafetyID == emrObject.FK_SafetyID && m.EMRYear == emrObject.EMRYear);

                updSafetyEMR.EMRRate = emrObject.EMRRate;
                updSafetyEMR.EMRYear = emrObject.EMRYear;
                updSafetyEMR.NotAvailable = emrObject.NotAvailable;
                updSafetyEMR.Explanation = emrObject.Explanation;
                updSafetyEMR.FilePath = emrObject.FilePath;

                Context.SaveChanges();
            }
        }
    }

    //Select Rates based on the Safety and the Year
    //001
    public VMSDAL.VQFSafetyEMRRate SelectSafetyEMRRates(long SafetyId, string Year)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            if (Context.VQFSafetyEMRRates.Count(m => m.FK_SafetyID == SafetyId && m.EMRYear == Year) == 1)
            {
                VMSDAL.VQFSafetyEMRRate objEMRRates = Context.VQFSafetyEMRRates.Single(m => m.FK_SafetyID == SafetyId && m.EMRYear == Year);
                return objEMRRates;
            }
            else
            { return null; }
        }
    }

    //Select OSHA based on the safetyid and year
    //001
    public VMSDAL.VQFSafetyOSHA SelectSafetyOsha(long SafetyId, string Year)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            if (Context.VQFSafetyOSHAs.Count(m => m.FK_SafetyID == SafetyId && m.OSHAYear == Year) == 1)
            {
                VMSDAL.VQFSafetyOSHA objOsha = Context.VQFSafetyOSHAs.Single(m => m.FK_SafetyID == SafetyId && m.OSHAYear == Year);
                return objOsha;
            }
            else
            { return null; }
        }
    }

    /// <summary>
    /// 002 Added
    /// New Update method using entity framework object
    /// </summary>
    /// <param name="SAFETY"></param>
    public void UpdateSafety(VMSDAL.VQFSafety SAFETY)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            if (Context.VQFSafeties.Count(m => m.PK_SafetyID == SAFETY.PK_SafetyID) > 0)
            {
                VMSDAL.VQFSafety updateSafety = Context.VQFSafeties.Single(m => m.PK_SafetyID == SAFETY.PK_SafetyID);

                updateSafety.CompanyContactIsBasedInUS = SAFETY.CompanyContactIsBasedInUS;
                updateSafety.CompanySafety = SAFETY.CompanySafety;
                updateSafety.CompanySafetyContactName = SAFETY.CompanySafetyContactName;
                updateSafety.CompanySafetyContactNumber = SAFETY.CompanySafetyContactNumber;
                updateSafety.CreatedDT = SAFETY.CreatedDT;
                updateSafety.EditStatus = SAFETY.EditStatus;
                updateSafety.Explanation = SAFETY.Explanation;
                updateSafety.InspectionOfWIPPeriods = SAFETY.InspectionOfWIPPeriods;
                updateSafety.IsPeriodicSafetyMeetings = SAFETY.IsPeriodicSafetyMeetings;
                updateSafety.IsProvideEmployeeTrainingDocumentation = SAFETY.IsProvideEmployeeTrainingDocumentation;
                updateSafety.IsSafetyInspectionOfWIP = SAFETY.IsSafetyInspectionOfWIP;
                updateSafety.IsWrittenDrugAlcoholProgram = SAFETY.IsWrittenDrugAlcoholProgram;
                updateSafety.LastModifiedDT = DateTime.Now;
                updateSafety.ProfessionalLevelOfTraining = SAFETY.ProfessionalLevelOfTraining;
                updateSafety.RepresentContactIsBasedInUS = SAFETY.RepresentContactIsBasedInUS;
                updateSafety.SafetyMeetingsPeriods = SAFETY.SafetyMeetingsPeriods;
                updateSafety.SafetyProgram = SAFETY.SafetyProgram;
                updateSafety.SafetyRepresentation = SAFETY.SafetyRepresentation;
                updateSafety.SafetyRepresentationContactName = SAFETY.SafetyRepresentationContactName;
                updateSafety.SafetyRepresentationContactNumber = SAFETY.SafetyRepresentationContactNumber;
                updateSafety.SafetyRequirements = SAFETY.SafetyRequirements;
                updateSafety.SafetyStatus = SAFETY.SafetyStatus;
                updateSafety.SICNA = SAFETY.SICNA;
                updateSafety.SICNAICSCode = SAFETY.SICNAICSCode;
                updateSafety.WhoConductsInspectionOfWIP = SAFETY.WhoConductsInspectionOfWIP;
                updateSafety.IsConductHazardAnalysis = SAFETY.IsConductHazardAnalysis;
                updateSafety.ConductHazardAnalysisPeriods = SAFETY.ConductHazardAnalysisPeriods;

                Context.SaveChanges();
            }
        }
    }

    public VMSDAL.VQFSafetyCitation NewCitationRecord(long safetyID, string year)
    {
        var citation = new VMSDAL.VQFSafetyCitation();
        citation.CitationFilePath = string.Empty;
        citation.CitationYear = year;
        citation.FK_SafetyID = safetyID;
        return citation;
    }

    public VMSDAL.VQFSafetyCitation UpdateCitations(VMSDAL.VQFSafetyCitation citation)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            if (Context.VQFSafetyCitations.Count(m => m.FK_SafetyID.Equals(citation.FK_SafetyID) && m.CitationYear.Equals(citation.CitationYear)) > 0)
            {
                //UPDATE
                VMSDAL.VQFSafetyCitation updateSafety = Context.VQFSafetyCitations.Single(m => m.FK_SafetyID.Equals(citation.FK_SafetyID) && m.CitationYear.Equals(citation.CitationYear));

                updateSafety.CitationFilePath = citation.CitationFilePath;
                updateSafety.CitationYear = citation.CitationYear;
                updateSafety.IsCitation = citation.IsCitation;
                updateSafety.ModifiedDT = DateTime.Now;
                updateSafety.PastYearStatus = citation.PastYearStatus;               

                Context.SaveChanges();
            }
            else
            {
                //INSERT
                VMSDAL.VQFSafetyCitation updateSafety = new VMSDAL.VQFSafetyCitation();

                updateSafety.CitationFilePath = citation.CitationFilePath;
                updateSafety.CreatedDT = DateTime.Now;
                updateSafety.CitationYear = citation.CitationYear;
                updateSafety.IsCitation = citation.IsCitation;
                updateSafety.ModifiedDT = DateTime.Now;
                updateSafety.PastYearStatus = citation.PastYearStatus;
                updateSafety.FK_SafetyID = SafetyID;    //global variable gets updated when VQFSafety is inserted

                Context.VQFSafetyCitations.Add(updateSafety);
                Context.SaveChanges();
            }
        }

        return this.SelectSafetyCitation(SafetyID, citation.CitationYear);
    }

    public VMSDAL.VQFSafetyCitation SelectSafetyCitation(long SafetyId, string Year)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            if (Context.VQFSafetyCitations.Count(m => m.FK_SafetyID.Equals(SafetyId) && m.CitationYear.Equals(Year)) == 1)
            {
                VMSDAL.VQFSafetyCitation citation = Context.VQFSafetyCitations.Single(m => m.FK_SafetyID.Equals(SafetyId) && m.CitationYear.Equals(Year));
                return citation;
            }
            else
            {
                return new VMSDAL.VQFSafetyCitation() { FK_SafetyID = SafetyId, CitationYear = Year };
            } 
        }
    }
}
