﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Xml.Linq;

/// <summary>
/// Summary description for clsLegal
/// </summary>
public class clsLegal : CommonPage
{
    public clsLegal()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// Insert Legal Date
    /// </summary>
    /// <param name="CurrentTotalBacklog"></param>
    /// <param name="CurrentYearProjectedRevenue"></param>
    /// <param name="ExplainLegal1"></param>
    /// <param name="ExplainLegal2"></param>
    /// <param name="ExplainLegal3"></param>
    /// <param name="ExplainLegal4"></param>
    /// <param name="Legal1"></param>
    /// <param name="Legal2"></param>
    /// <param name="Legal3"></param>
    /// <param name="Legal4"></param>
    /// <param name="VendorId"></param>
    /// <param name="LegalStatus"></param>
    /// <param name="CurrencyID"></param>
    /// <param name="IsDenominatedInDollar"></param>
    /// <returns>true or false to know record inserted or error thrown</returns>
    //003
    //Sooraj -Modified for adding International Currency 
    public bool InsertLegal(string CurrentTotalBacklog, string CurrentYearProjectedRevenue, string ExplainLegal1, string ExplainLegal2, string ExplainLegal3, string ExplainLegal4, bool? Legal1, bool? Legal2, bool? Legal3, bool? Legal4, long VendorId, byte LegalStatus,short CurrencyID,bool IsDenominatedInDollar)
    {
        //try
        //{

        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.VQFLegal objLegal = new DAL.VQFLegal
            {
                CurrentTotalBacklog = CurrentTotalBacklog,
                CurrentYearProjectedRevenue = CurrentYearProjectedRevenue,
                ExplainLegal1 = ExplainLegal1,

                ExplainLegal2 = ExplainLegal2,
                ExplainLegal3 = ExplainLegal3,
                ExplainLegal4 = ExplainLegal4,

                Legal1 = Legal1,
                Legal2 = Legal2,
                Legal3 = Legal3,
                Legal4 = Legal4,
                LegalStatus = LegalStatus,

                IsDenominateInDollar=IsDenominatedInDollar,  //003-Sooraj
                FK_CurrencyID = CurrencyID,//003-Sooraj
                
                FK_VendorID = VendorId
            };
            dcHaskell.VQFLegals.InsertOnSubmit(objLegal);
            dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }
        //}
        //catch
        //{
        //    return false;
        //}
    }
    /// <summary>
    /// Insert Legal Financial
    /// </summary>
    /// <param name="AnnualRevenue"></param>
    /// <param name="ContractValue"></param>
    /// <param name="Year"></param>
    /// <returns>return true or false to know record inserted or error thrown</returns>
    public bool InsertLegalFinancial(string AnnualRevenue, string ContractValue, string Year, long LegalID, bool NotApplicable, string Explain)
    {
        //try
        //{

        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.VQFLegalFinancial objLegalFinancial = new DAL.VQFLegalFinancial
            {
                AnnualRevenue = AnnualRevenue,
                ContractValue = ContractValue,
                FK_LegalID = LegalID,
                Year = Year,
                NotAvailable = NotApplicable,
                Explanation = Explain,
               
            };
            dcHaskell.VQFLegalFinancials.InsertOnSubmit(objLegalFinancial);
            dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }
        //}
        //catch
        //{
        //    return false;
        //}
    }
    /// <summary>
    /// Insert Banking Details
    /// </summary>
    /// <param name="Address"></param>
    /// <param name="City"></param>
    /// <param name="ContactName"></param>
    /// <param name="Credit"></param>
    /// <param name="FinancialInstitution"></param>
    /// <param name="State"></param>
    /// <param name="OtherState"></param>
    /// <param name="Phone"></param>
    /// <param name="Zipcode"></param>
    /// <returns>return false for error and true for successful insertion</returns>
    public bool InsertLegalBanking(string Address, string City, string ContactName, bool? Credit, string FinancialInstitution, short State, string OtherState, string Phone, string Zipcode, long LegalID,short countryID)
    {
        //try
        //{

        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.VQFLegalBanking objLegalBanking = new DAL.VQFLegalBanking
            {
                Address = Address,
                City = City,
                ContactName = ContactName,
                Credit = Credit,
                FinancialInstitution = FinancialInstitution,
                FK_State = State,
                FK_LegalID = LegalID,
                OtherState = OtherState,
                Phone = Phone,
                ZipCode = Zipcode,
                FK_Country=countryID //006

            };
            dcHaskell.VQFLegalBankings.InsertOnSubmit(objLegalBanking);
            dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }
        //}
        //catch (Exception ex)
        //{
        //    return false;
        //}
    }


    /// <summary>
    /// method to Get LegalFinancial Data
    /// </summary>
    public DataSet GetLegalFinancialData(long LegalID, string Currentyear)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetLegalFinancialData",
             new SqlParameter("@legalID", LegalID),
             new SqlParameter("@CurrentYear", Currentyear));
        return ds;
    }

    /// <summary>
    /// method to Get LegalBanking Data
    /// </summary>
    public DataSet GetLegalBankingData(long LegalID)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetLegalBankingData",
             new SqlParameter("@legalID", LegalID));
        return ds;
    }
    /// <summary>
    /// Get Legal Data based on the given vendor Id
    /// </summary>
    /// <param name="SingleUserID"></param>
    /// <returns>Data of the entered vendor Id</returns>
    public DAL.VQFLegal GetSingleVQLData(long SingleUserID)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (datacontext.VQFLegals.Count(m => m.FK_VendorID == SingleUserID) > 0)
            {

                return datacontext.VQFLegals.Single(m => m.FK_VendorID == SingleUserID);
            }
            else
            {
                return null;
            }

        }
    }
    /// <summary>
    /// Get the Banking Data
    /// </summary>
    /// <param name="SingleUserID"></param>
    /// <returns>Banking Data</returns>
    public static DAL.VQFLegalBanking GetSingleVQFBanking(int SingleUserID)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            return datacontext.VQFLegalBankings.Single(m => m.FK_LegalID == SingleUserID);
        }
    }
    public static DAL.VQFLegalFinancial GetSingleVQFFinancial(long SingleUserID)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            return datacontext.VQFLegalFinancials.Single(m => m.FK_LegalID == SingleUserID);

        }
    }

    //To get Legal status for particular vendor
    public byte GetLegalStatusCount(long VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetLegalStatusResult[] Result = datacontext.UspGetLegalStatus(VendorId).ToArray();
            if (Result.Length > 0)
            {
                return Convert.ToByte(Result[0].LegalStatus);
            }
            else
            {
                return 0;
            }
        }
    }

    //To get Legal count for particular vendor
    public int GetLegalCount(long VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetCountLegalResult[] Result = datacontext.UspGetCountLegal(VendorId).ToArray();
            return Convert.ToInt32(Result[0].LegalCount);
        }
    }
    public bool UpdateLegalBit(long VendorId, Byte Status)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.VQFLegal updLegal = Context.VQFLegals.Single(m => m.FK_VendorID == VendorId);
            updLegal.EditStatus = Status;

            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }
    }


    /// <summary>
    /// Update LegalPastYear
    /// </summary>
    /// <param name="SafetyId"></param>
    /// <returns></returns>
    public Int32 LegalPastYear(long LegalId)
    {
        Int32 i = 0;
        i = SqlHelper.ExecuteNonQuery(con, CommandType.StoredProcedure, "UspUpdateLegalPastYear", new SqlParameter("@LegalId", LegalId));
        return i;

    }


    //public bool UpdateLegalYearBit(long LegalId, Byte PastYearStatus, string Year)
    //{
    //    using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
    //    {

    //        DAL.VQFLegalFinancial Legal = Context.VQFLegalFinancials.Single(m => m.FK_LegalID == LegalId && m.Year == Year);

    //        LegalFinancial.PastYearStatus = PastYearStatus;
    //        Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
    //        return true;

    //    }
    //}




    //For Updating Legal Details
    //003
    //Sooraj -Modified for adding International Currency 
    public bool UpdateLegal(string CurrentTotalBacklog, string CurrentYearProjectedRevenue, string ExplainLegal1, string ExplainLegal2, string ExplainLegal3, string ExplainLegal4, bool? Legal1, bool? Legal2, bool? Legal3, bool? Legal4, long LegalID, byte LegalStatus,short CurrencyID,bool IsDenominatedInDollar)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.VQFLegal updLegal = Context.VQFLegals.Single(m => m.PK_LegalID == LegalID);
            updLegal.CurrentTotalBacklog = CurrentTotalBacklog;
            updLegal.CurrentYearProjectedRevenue = CurrentYearProjectedRevenue;
            updLegal.ExplainLegal1 = ExplainLegal1;

            updLegal.ExplainLegal2 = ExplainLegal2;
            updLegal.ExplainLegal3 = ExplainLegal3;
            updLegal.ExplainLegal4 = ExplainLegal4;

            updLegal.Legal1 = Legal1;
            updLegal.Legal2 = Legal2;
            updLegal.Legal3 = Legal3;
            updLegal.Legal4 = Legal4;
            updLegal.LegalStatus = LegalStatus;
	    updLegal.LastModifiedDT = DateTime.Now;
        updLegal.FK_CurrencyID = CurrencyID;//003-Sooraj
        updLegal.IsDenominateInDollar = IsDenominatedInDollar;//003-Sooraj
            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }
    }


    //To get Legal banking count 
    public int GetLegalBankingCount(long LegalId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetCountLegalbankingResult[] Result = datacontext.UspGetCountLegalbanking(LegalId).ToArray();
            return Convert.ToInt32(Result[0].LegalbankingCount);
        }
    }


    //For Updating Legal Details
    public bool UpdateLegalBanking(string Address, string City, string ContactName, bool Credit, string FinancialInstitution, short State, string OtherState, string Phone, string Zipcode, long LegalId,short countryID)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {

            DAL.VQFLegalBanking updLegalBanking = Context.VQFLegalBankings.Single(m => m.FK_LegalID == LegalId);

            updLegalBanking.Address = Address;
            updLegalBanking.City = City;
            updLegalBanking.ContactName = ContactName;
            updLegalBanking.Credit = Credit;
            updLegalBanking.FinancialInstitution = FinancialInstitution;
            updLegalBanking.FK_State = State;
            updLegalBanking.OtherState = OtherState;
            updLegalBanking.Phone = Phone;
            updLegalBanking.ZipCode = Zipcode;
            updLegalBanking.FK_Country = countryID;

            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }
    }


    // To Delete the sub table details of legal
    public bool DeleteAllRelationsofLegal(long LegalID)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            Context.VQFLegalBankings.DeleteAllOnSubmit(Context.VQFLegalBankings.Where(l => l.FK_LegalID == LegalID));
            Context.VQFLegalFinancials.DeleteAllOnSubmit(Context.VQFLegalFinancials.Where(l => l.FK_LegalID == LegalID));

            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;

        }
    }



    //Select Rates based on the LegalFinancial and the Year
    public DAL.VQFLegalFinancial SelectLegalFinancial(long LegalID, string Year)
    {

        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (Context.VQFLegalFinancials.Count(m => m.FK_LegalID == LegalID && m.Year == Year) == 1)
            {
                DAL.VQFLegalFinancial objLegalFinancial = Context.VQFLegalFinancials.Single(m => m.FK_LegalID == LegalID && m.Year == Year);
                return objLegalFinancial;
            }
            else
            { return null; }
        }
    }
    /// <summary>
    /// Select Rates based on the LegalFinancial and the Year for Comparison
    /// </summary>
    /// <param name="LegalID"></param>
    /// <returns></returns>
    public DataSet SelectLegalFinancial(long LegalID)
    {
        DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetLegalFinancial", new SqlParameter("@LegalID", LegalID));
            return ds;
    }
   
    /// <summary>
    /// Get the legal id based on the vendor id
    /// </summary>
    /// <param name="VendorUserID"></param>
    /// <returns>safter data based on the vendor id</returns>
    public DAL.VQFLegal GetSingleVQLlegalData(long VendorUserID)
    {
        using (DAL.HaskellDataContext datacontextLegals = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (datacontextLegals.VQFLegals.Count(s => s.FK_VendorID == VendorUserID) == 0)
            {
                return null;
            }
            else
            {
                return datacontextLegals.VQFLegals.Single(s => s.FK_VendorID == VendorUserID);
            }
        }

    }

}
