﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for clsLogin
/// </summary>
public class clsLogin
{
	public clsLogin()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public int CheckUsernameandPassword(string username, string password)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetAdminUsersResult[] resGetAdminUser = datacontext.UspGetAdminUsers(username,password).ToArray();
            if (resGetAdminUser.Count() > 0)
            {
                return Convert.ToInt32(resGetAdminUser[0].CountUser);
            }
            else
            {
                return 0;
            }
            
        }
    }
}
