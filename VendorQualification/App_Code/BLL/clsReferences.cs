﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text.RegularExpressions;
using Microsoft.ApplicationBlocks.Data;
using System.IO;
using System.Xml.Linq;
using System.Net.Mail;
using System.Text;



/// <summary>
/// Summary description for clsReferences
/// </summary>
public class clsReferences:CommonPage
{
	public clsReferences()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    //public long ReferencesId { get; set; }

    public long _ReferencesId;
    public long ReferencesId
    {
        get
        {
            return _ReferencesId;
        }
        set
        {
            _ReferencesId = value;
        }
    }

    #region ReferencesInsert-Part
    /// <summary>
    /// Insert Reference Details
    /// </summary>
    /// <param name="AppoximateContract1"></param>
    /// <param name="AppoximateContract2"></param>
    /// <param name="AppoximateContract3"></param>
    /// <param name="City1"></param>
    /// <param name="City2"></param>
    /// <param name="City3"></param>
    /// <param name="CompletedProjects"></param>
    /// <param name="CompletionDT1"></param>
    /// <param name="CompletionDT2"></param>
    /// <param name="CompletionDT3"></param>
    /// <param name="ContractorCompanyName1"></param>
    /// <param name="ContractorCompanyName2"></param>
    /// <param name="ContractorCompanyName3"></param>
    /// <param name="ContractorContactName1"></param>
    /// <param name="ContractorContactName2"></param>
    /// <param name="ContractorContactName3"></param>
    /// <param name="ContractorPhoneNumber1"></param>
    /// <param name="ContractorPhoneNumber2"></param>
    /// <param name="ContractorPhoneNumber3"></param>
    /// <param name="FK_State1"></param>
    /// <param name="FK_State2"></param>
    /// <param name="FK_State3"></param>
    /// <param name="OtherState1"></param>
    /// <param name="OtherState2"></param>
    /// <param name="OtherState3"></param>
    /// <param name="FK_VendorID"></param>
    /// <param name="ProjectName1"></param>
    /// <param name="ProjectName2"></param>
    /// <param name="ProjectName3"></param>
    /// <param name="ScopeWorkCompleted1"></param>
    /// <param name="ScopeWorkCompleted2"></param>
    /// <param name="ScopeWorkCompleted3"></param>
    /// <param name="ReferencesStatus"></param>
    /// <returns>Returns the reference Id to insert the reference Id for sub tables.</returns>
    public long InsertReferences(bool? CompletedProjects,long FK_VendorID,byte ReferencesStatus)
    {
           using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
           {

                    DAL.VQFReference objReferences = new DAL.VQFReference
                    {
                   
                     CompletedProjects=CompletedProjects,
                     FK_VendorID=FK_VendorID,
                     ReferencesStatus = ReferencesStatus,
                     
                    };
                    dcHaskell.VQFReferences.InsertOnSubmit(objReferences);
                    dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);                
                    long ReferenceId = dcHaskell.VQFReferences.Single(m => m.FK_VendorID == FK_VendorID).PK_ReferencesID;
                    ReferencesId = ReferenceId;
                    return ReferenceId;

          }
         
        
    }

    //Insert Project references
    public void insertProjectReferences(long ReferencesId,string ProjectName,string City,short FK_State,string CompletionDate,string OtherState,string Approximate,short ProjectRefrenceType, string GerenalContrator,string ContactName,string ContactPhoneNumber,string workCompleted,string AddNote,bool NA,string Explain,bool IsSpeaksEnglish,short CountryID)
    {
        using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.VQFProjectReference objRefProject = new DAL.VQFProjectReference 
            {
                AddNote=AddNote,
                ApproximateContract=Approximate,
                City=City,
                CompletionDT=CompletionDate,
                ContactName=ContactName,
                ContactPhoneNumber=ContactPhoneNumber,
                FK_ReferencesID = ReferencesId,
                FK_State=FK_State,
                FK_ProjectReferenceType = ProjectRefrenceType,  //Sooraj - Project reference type for design consultant 
                GeneralContractor=GerenalContrator,
                OtherState=OtherState,
                ProjectName=ProjectName,
                WorkCompleted=workCompleted,
                NotAvailable=NA,
                Explanation=Explain,
                FK_Country=CountryID,
                IsSpeaksEnglish=IsSpeaksEnglish//Sooraj - Modified to add Is Speaks English Question
             
            };
            dcHaskell.VQFProjectReferences.InsertOnSubmit(objRefProject);
            dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);                
        }
    
    }
    /// <summary>
    /// Insert Reference Projects
    /// </summary>
    /// <param name="ProjectName"></param>
    /// <param name="ProjectManager"></param>
    /// <param name="Year"></param>
    /// <param name="PrimaryId"></param>
    /// <returns>return false for error and true for successful insertion</returns>
    public bool InsertVQFReferencesProject(string ProjectName,string ProjectManager, string Year, long PrimaryId)
    {
           using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
                {

                    DAL.VQFReferencesProject objRefProject = new DAL.VQFReferencesProject
                    {
                       CreatedDT=DateTime.Now,
                       FK_ReferencesID = PrimaryId,
                       
                       ProjectName = ProjectName,
                       ProjectManager=ProjectManager,                       
                       Year=Year,
                       

                    };
                    dcHaskell.VQFReferencesProjects.InsertOnSubmit(objRefProject);
                    dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);                    
                    return true;

                }
         
        }
    /// <summary>
    /// Insert Reference Suppliers
    /// </summary>
    /// <param name="CompanyName"></param>
    /// <param name="ContactName"></param>
    /// <param name="City"></param>
    /// <param name="FK_State"></param>
    /// <param name="OtherState"></param>
    /// <param name="CompanyAddress"></param>
    /// <param name="ZipCode"></param>
    /// <param name="Phone"></param>
    /// <param name="PrimaryId"></param>
    ///  <param name="CountryID"></param> //Sooraj Modified for International Country
    ///  <param name="IsSpeaksEnglish"></param> //Sooraj Modified for Speaks English Question
    /// <returns>return false for error and true for successful insertion</returns>
    public bool InsertVQFReferencesSupplier(string CompanyName, string ContactName, string City, short FK_State, string OtherState, string CompanyAddress, string ZipCode, string Phone, long PrimaryId,short CountryID,bool IsSpeaksEnglish)
    {
     
            using (DAL.HaskellDataContext dcHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
            {

                DAL.VQFReferencesSupplier objRefSupl = new DAL.VQFReferencesSupplier
                {
                    FK_ReferencesID = PrimaryId,
                    CompanyName=CompanyName,
                    ContactName=ContactName,
                    City=City,
                    FK_State=FK_State,
                    OtherState=OtherState,
                    CompanyAddress=CompanyAddress,                    
                    ZipCode=ZipCode,              
                    Phone=Phone,      
                    FK_Country=CountryID,
                    IsSpeaksEnglish=IsSpeaksEnglish  //Sooraj- Modified to add Is Speaks English
                    
                };
                dcHaskell.VQFReferencesSuppliers.InsertOnSubmit(objRefSupl);
                dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                return true;

            }
      
    }
    #endregion
    #region ReferencesUpadate-Part
    /// <summary>
    /// Get References Count
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns>Count of record. If Record has more than 0 records the data will be used for updation else used for insertion</returns>
    public int GetRefCount(int VendorId)
    { 
        using(DAL.HaskellDataContext datacontact = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetReferenceCountResult[] retval= datacontact.UspGetReferenceCount(VendorId).ToArray();
            return Convert.ToInt32(retval[0].Referencescount);
        }
        
    }

    //To get Reference status for particular vendor
    public byte GetReferenceStatusCount(long VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetReferencesStatusResult[] Result = datacontext.UspGetReferencesStatus(VendorId).ToArray();
            if (Result.Length > 0)
            {
                return Convert.ToByte(Result[0].ReferencesStatus);
            }
            else
            {
                return 0;
            }
        }
    }

    /// <summary>
    /// Get references data based on the vendor logged in 
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns>dataset of the references</returns>
    public DAL.UspGetVQFReferencesResult[] GetVQFReferences(int VendorId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetVQFReferencesResult[] ResRef = datacontext.UspGetVQFReferences(VendorId).ToArray();
            return ResRef;
        }
    }

    //For Updating Trace Details
    public bool UpdateReferences(bool? CompletedProjects, long ExistingRefId, Byte ReferencesStatus)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
           DAL.VQFReference updReferences = Context.VQFReferences.Single(m => m.PK_ReferencesID == ExistingRefId);
           updReferences.CompletedProjects=CompletedProjects;
           updReferences.LastModifiedDT = DateTime.Now;
           updReferences.ReferencesStatus=ReferencesStatus;
            Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;
          
        }
    }

    /// <summary>
    /// Delete all details in sub tables
    /// </summary>
    /// <param name="ExistingRefId"></param>
    /// <returns>return false for error and true for successful updation</returns>

    public bool DeleteAllRelationsofRefId(long ExistingRefId)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
           Context.VQFReferencesProjects.DeleteAllOnSubmit(Context.VQFReferencesProjects.Where(j => j.FK_ReferencesID == ExistingRefId));
           Context.VQFProjectReferences.DeleteAllOnSubmit(Context.VQFProjectReferences.Where(j => j.FK_ReferencesID == ExistingRefId));
           Context.VQFReferencesSuppliers.DeleteAllOnSubmit(Context.VQFReferencesSuppliers.Where(j => j.FK_ReferencesID == ExistingRefId));
           Context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;
           
        }
    }

    #endregion
    #region LoadReferenceData-Part
    /// <summary>
    /// Get the details of references who logged in 
    /// </summary>
    /// <param name="VendorUserID"></param>
    /// <returns>single row record of vendor</returns>
    public DAL.VQFReference GetSingleVQFReferenceData(long VendorUserID)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (datacontext.VQFReferences.Count(m => m.FK_VendorID == VendorUserID) == 0)
            {
                return null;
            }
            else
            {

                return datacontext.VQFReferences.Single(m => m.FK_VendorID == VendorUserID);
            }
        }
    }
    /// <summary>
    /// Get references Project
    /// </summary>
    /// <param name="ReferencesId"></param>
    /// <returns></returns>
    public DAL.USPGetReferencesProjectResult[] GetReferencesProject(long ReferencesId)
    {

        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.USPGetReferencesProjectResult[] resProjectresult = datacontext.USPGetReferencesProject(ReferencesId).ToArray();
            return resProjectresult;
        }
    }

    //Get project references based on the reference id
    public DAL.UspGetProjectReferencesResult[] GetProjectReferences(long ReferencesId)
    {

        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetProjectReferencesResult[] resProjectresult = datacontext.UspGetProjectReferences(ReferencesId).ToArray();
            return resProjectresult;
        }
    }
    /// <summary>
    /// Get references supplier details
    /// </summary>
    /// <param name="ReferencesId"></param>
    /// <returns></returns>
    public DAL.USPGetReferencesSupplierResult[] GetReferencesSupplier(long ReferencesId)
    {

        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.USPGetReferencesSupplierResult[] resRefSuplresult = datacontext.USPGetReferencesSupplier(ReferencesId).ToArray();
            return resRefSuplresult;
        }
    }
    #endregion
   
    #region ReferencesView-part

    //update notes on  VQFReferences table on Dec29-2009
    public bool UpdateNotesVQFReferences(long Pkrefid, string Note1)
    {
        using (DAL.HaskellDataContext context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.VQFProjectReference updnoteref = context.VQFProjectReferences.Single(m => m.PK_ProjectReferencesID == Pkrefid );
            updnoteref.AddNote = Note1;
            //updnoteref.AddNote1 = Note1;
            //updnoteref.AddNote2 = Note2;
            //updnoteref.AddNote3 = Note3;
            //updnoteref.LastModifiedDT = DateTime.Now;
            context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;
        }
    }
    //update notes on  VQFSupplier table on Dec29-2009
    public bool UpdateNotesVQFSupplier(long Pksuplid, string Note1)
    {
        using (DAL.HaskellDataContext context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.VQFReferencesSupplier updnotesupl = context.VQFReferencesSuppliers.Single(m => m.PK_SupplierID == Pksuplid) ;

            updnotesupl.AddNote = Note1;
           // updnotesupl.LastModifiedDT= DateTime.Now;
            context.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            return true;
        }
    }



    #endregion

    #region ExportExcelpart
    /// <summary>
    /// To Return project References Details
    /// </summary>
    /// <returns>Project References Details</returns>
    public DataTable GetRefPrjdetails(long VendorId)
    {
        DataTable dt = new DataTable();
        
        dt = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspPrjRefdetails",
             new SqlParameter("@VendorId", VendorId)).Tables[0];
        return dt;
    }

    /// <summary>
    /// Export to excel the supplier details
    /// </summary>
    /// <param name="Refid"></param>
    /// <returns></returns>
    public DataTable GetRefSupldetails(long Refid)
    {
        DataTable dts = new DataTable();

        dts = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspRefsupplierprintdetails",
             new SqlParameter("@ReferenceId", Refid)).Tables[0];
        return dts;
    }
    #endregion

    /// <summary>
    /// method to Get Project Reference Data
    /// </summary>
    public DataSet GetProjectReferenceData(long ReferenceID)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetProjectReferences",
             new SqlParameter("@ReferencesId", ReferenceID));
        return ds;
    }

    /// <summary>
    /// method to Get Additional Project Reference Data
    /// </summary>
    public DataSet GetReferenceProjectData(long ReferenceID)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "USPGetReferencesProject",
             new SqlParameter("@ReferenceId", ReferenceID));
        return ds;
    }

    /// <summary>
    /// method to Get Reference Supplier Data
    /// </summary>
    public DataSet GetReferenceSupplierData(long ReferenceID)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "USPGetReferencesSupplier",
             new SqlParameter("@ReferenceId", ReferenceID));
        return ds;
    }
}
