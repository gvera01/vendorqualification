﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsUserMaintenance
/// CHANGES:
/// 001 G. Vera 03/29/2017: Add new field Employee Role as part of the User maintenance
/// </summary>
public class clsUserMaintenance:Common
{
	public clsUserMaintenance()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /// <summary>
    /// List User details
    /// </summary>
    /// <param name="Group"></param>
    /// <param name="Access"></param>
    /// <param name="PageIndex"></param>
    /// <param name="NumberOfRecords"></param>
    /// <returns></returns>
    public DataTable DisplayUserMaintenance(string Group,Byte Access,string EmployeeNumber,int PageIndex,int NumberOfRecords,string SortExpression)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "uspListUserMaintenance",
             new SqlParameter("@Group",Group),
             new SqlParameter("@Admin", Access),
             new SqlParameter("@EmployeeNumber", EmployeeNumber),
             new SqlParameter("@Pageindex",PageIndex),
             new SqlParameter("@NoRecords",NumberOfRecords),
             new SqlParameter("@SortExp",SortExpression));
        
        return ds.Tables[0];
    }

    /// <summary>
    /// Get the total of the employee details
    /// </summary>
    /// <param name="Group"></param>
    /// <param name="Access"></param>
    /// <returns></returns>
    public int DisplayUserMaintenanceTotal(string Group, Byte Access,string EmployeeNumber)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "uspListUserMaintenanceTotal",
             new SqlParameter("@Group", Group),
             new SqlParameter("@Admin", Access),
             new SqlParameter("@EmployeeNumber", EmployeeNumber));
          return Convert.ToInt32(ds.Tables[0].Rows[0]["CountValue"]);
    }
    //Update Employee access
    //001 new parameter, logic to handle it correctly, and changed DAL to VMSDAL
    public bool UpdateEmployeeAccess(string EmployeeNumber, Byte Access, string employeeRole)
    {
        bool result = false;

        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
         
            if (Context.EmployeeAccesses.Count(m => m.EmployeeNumber == EmployeeNumber) > 0)
            {
                VMSDAL.EmployeeAccess updEmployee = Context.EmployeeAccesses.Single(m => m.EmployeeNumber == EmployeeNumber);
                updEmployee.Access = Access;
                Context.SaveChanges();
                result = true;
            }
            else
            {
                VMSDAL.EmployeeAccess insEmployee = new VMSDAL.EmployeeAccess
               {
                   Access = Access,
                   EmployeeNumber = EmployeeNumber,
                   CreatedDt = DateTime.Now
               };
                Context.EmployeeAccesses.Add(insEmployee);
                Context.SaveChanges();
                result = true;     
            
            }

            if (result)
            {
                if (Context.Employees.Count(m => m.EmployeeNumber == EmployeeNumber) > 0)
                {
                    VMSDAL.Employee updEmployee = Context.Employees.Single(m => m.EmployeeNumber == EmployeeNumber);
                    updEmployee.EmployeeRole = employeeRole;
                    Context.SaveChanges();
                    result = true;
                }
            }

            return result;
        }
    }

    //001
    /// <summary>
    /// Will return a static list of employee roles (hardcoded)
    /// </summary>
    /// <returns></returns>
    public static List<AccessRoleItem> GetEmployeeRoles()
    {
        var list = new List<AccessRoleItem>();

        var role1 = new AccessRoleItem() { RoleValue = "PM", RoleDescription = "Employee/PM" };
        var role2 = new AccessRoleItem() { RoleValue = "Admin", RoleDescription = "Administrator" };
        var role3 = new AccessRoleItem() { RoleValue = "Admin_SRE", RoleDescription = "Administrator & SRE" };
        var role4 = new AccessRoleItem() { RoleValue = "", RoleDescription = "no roles for this access" };

        list.Add(role1);
        list.Add(role2);
        list.Add(role3);
        list.Add(role4);

        return list;

    }

    //001
    /// <summary>
    /// EmployeeRole object
    /// </summary>
    public class AccessRoleItem
    {
        public string RoleValue { get; set; }
        public string RoleDescription { get; set; }

    }
}
