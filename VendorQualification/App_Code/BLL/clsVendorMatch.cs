﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;

/**************************************************************************************************************
 * 001 G. Vera 07/26/2012: VMS Stabilization Release 1.2 (JIRA:VPI-19)
 **************************************************************************************************************/

/// <summary>
/// Summary description for clsVendorMatch
/// </summary>
public class clsVendorMatch
{
    /// <summary>
    /// 001- added
    /// </summary>
    public enum  ActiveFlag
    {
        Active = 1, 
        Ignored = 0
    }

	public clsVendorMatch()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    /// <summary>
    /// Get HaskellJDE information given the vendor name
    /// G. Vera 4/19/2012 - Edit Vendor Number Phase 3 Enhancements
    /// </summary>
    /// <param name="TaxId"></param>
    /// <returns></returns>
    public List<DAL.HaskellJDE> HaskellJDEByName(string Vendorname)
    {
        using (DAL.HaskellAdminDataContext datacontext = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            List<DAL.HaskellJDE> objHaskellJdeList = (from h in datacontext.HaskellJDEs
                                                      where h.VendorName.Trim().ToUpper() == Vendorname.Trim().ToUpper()
                                                      select h).ToList < DAL.HaskellJDE>();
            
            return objHaskellJdeList;

        }
    }

    /// <summary>
    /// Get HaskellJDE information given the tax id
    /// G. Vera 4/19/2012 - Edit Vendor Number Phase 3 Enhancements
    /// </summary>
    /// <param name="TaxId"></param>
    /// <returns></returns>
    public List<DAL.HaskellJDE> HaskellJDEByTaxID(string TaxId)
    {
        using (DAL.HaskellAdminDataContext datacontext = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            List<DAL.HaskellJDE> objHaskellJdeList = (from h in datacontext.HaskellJDEs
                                                      where h.TaxID.Trim().ToUpper() == TaxId.Trim().ToUpper()
                                                      select h).ToList<DAL.HaskellJDE>();
            return objHaskellJdeList;

        }
    }

    /// <summary>
    /// Display the records in vendor match form
    /// </summary>
    /// <param name="PageIndex"></param>
    /// <param name="NumberOfRecords"></param>
    /// <returns></returns>
    public DataTable GetVendorMatch(int PageIndex, int NumberOfRecords)
    {
        SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand("uspListVendorMatch", objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.AddWithValue("@Pageindex", PageIndex);
        objCommand.Parameters.AddWithValue("@NoRecords", NumberOfRecords);
        objCommand.ExecuteNonQuery();
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objTable = new DataTable();
        objAdapter.Fill(objTable);
        return objTable;
    }

    /// <summary>
    /// Get the count of vendor match
    /// </summary>
    /// <returns></returns>
    public long GetVendorMatchCount()
    {
        SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand("uspListVendorMatchTotal", objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;

       return Convert.ToInt64(objCommand.ExecuteScalar());

    }

    /// <summary>
    /// Get the details for vebdor maintenance
    /// </summary>
    /// <param name="VendorName"></param>
    /// <param name="VendorNumber"></param>
    /// <param name="TaxId"></param>
    /// <param name="TrackingNumber"></param>
    /// <param name="status"></param>
    /// <param name="PageIndex"></param>
    /// <param name="NumberOfRecords"></param>
    /// <returns></returns>
    public DataTable GetVendorMaintenance(string VendorName, string VendorNumber, string TaxId, string TrackingNumber,int status, int PageIndex, int NumberOfRecords, string SortExpression)
    {
        SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand("uspListVendorMaintenance", objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.AddWithValue("@VendorName", VendorName);
        objCommand.Parameters.AddWithValue("@VendorNumber", VendorNumber);
        objCommand.Parameters.AddWithValue("@TaxID", TaxId);
        objCommand.Parameters.AddWithValue("@TrackingNumber", TrackingNumber);
         objCommand.Parameters.AddWithValue("@status", status);
        objCommand.Parameters.AddWithValue("@Pageindex", PageIndex);
        objCommand.Parameters.AddWithValue("@NoRecords", NumberOfRecords);
        objCommand.Parameters.AddWithValue("@SortExp", SortExpression);
        objCommand.ExecuteNonQuery();
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objTable = new DataTable();
        objAdapter.Fill(objTable);   
        return objTable;
    }

    /// <summary>
    /// Get the count of vendor maintenance
    /// </summary>
    /// <param name="VendorName"></param>
    /// <param name="VendorNumber"></param>
    /// <param name="TaxId"></param>
    /// <param name="TrackingNumber"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    public long GetVendorMaintenanceCount(string VendorName, string VendorNumber, string TaxId, string TrackingNumber,int status)
    {
        SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand("uspListVendorMaintenanceTotal", objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.AddWithValue("@VendorName", VendorName);
        objCommand.Parameters.AddWithValue("@VendorNumber", VendorNumber);
        objCommand.Parameters.AddWithValue("@TaxID", TaxId);
        objCommand.Parameters.AddWithValue("@TrackingNumber", TrackingNumber);
        objCommand.Parameters.AddWithValue("@status", status);
        return Convert.ToInt64(objCommand.ExecuteScalar());

    }

    /// <summary>
    /// Display vendor number based on the taxid
    /// G. Vera 05/23/2014 - was pointing to OLD DAL
    /// </summary>
    /// <param name="TaxId"></param>
    /// <returns></returns>
    public VMSDAL.GetVendorNumberResult[] DisplayVendorNumber(string TaxId, string vendorName)
    {
        using (VMSDAL.Haskell_VQF datacontext = new VMSDAL.Haskell_VQF())
        {
            VMSDAL.GetVendorNumberResult[] objVendorNumber = datacontext.GetVendorNumber(TaxId).ToArray();

            //objVendorNumber = objVendorNumber.Where(v => v.VendorName.ToUpper().Trim().StartsWith(vendorName)).ToArray();
            
            return objVendorNumber;

        }
    }

    //Update vendor number
    public bool UpdateVendorNumber(string Company,string TaxId,string VendorNumber,bool hasJDE)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            if (Context.Vendors.Count(m => m.Company == Company && m.TaxID == TaxId) == 1)
            {
                VMSDAL.Vendor updVendor = Context.Vendors.Single(m => m.Company == Company && m.TaxID == TaxId);
                //updVendor.VendorNumber = VendorNumber;
                updVendor.JDE = hasJDE;
                Context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    //Update vendor number by ID
    //GV - added
    public bool UpdateVendorNumber(long VendorID, string VendorNumber, bool hasJDE)
    {
        using (VMSDAL.Haskell_VQF Context = new VMSDAL.Haskell_VQF())
        {
            if (Context.Vendors.Count(m => m.PK_VendorID == VendorID) == 1)
            {
                VMSDAL.Vendor updVendor = Context.Vendors.Single(m => m.PK_VendorID == VendorID);
                //updVendor.VendorNumber = VendorNumber;
                updVendor.JDE = hasJDE;
                Context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    //Update vendor number
    /// <summary>
    /// Basically, the same as UpdateVendorNumber but actually updating the vendor number.
    /// G. Vera 4/19/2012 - Edit Vendor Number Phase 3 Enhancements - Implemented as do not want to touch what is existing.
    /// 001 - Modified
    /// </summary>
    /// <param name="Company"></param>
    /// <param name="TaxId"></param>
    /// <param name="VendorNumber"></param>
    /// <param name="JDE"></param>
    /// <returns></returns>
    public int EditVendorNumber(string Company, string TaxId, string VendorNumber, bool JDE, ActiveFlag activeFlag)
    {
        using (DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            if (Context.Vendors.Count(m => m.Company.ToUpper() == Company.ToUpper() && m.TaxID == TaxId) == 1)
            {
                DAL.Vendor vendor = Context.Vendors.Single(m => m.Company == Company && m.TaxID == TaxId);

                SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
                objConnection.Open();

                SqlCommand objCommand = new SqlCommand("UPDATE vendor set vendornumber = @VendorNumber, JDE = @HASJDE " +
                                                       "WHERE PK_VendorId = @VendorID", objConnection);
                objCommand.Parameters.AddWithValue("@HASJDE", JDE);
                objCommand.Parameters.AddWithValue("@VendorID", vendor.PK_VendorID);
                objCommand.Parameters.AddWithValue("@VendorNumber", VendorNumber);
                objCommand.ExecuteNonQuery();
                
                //001
                using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
                {
                    if (!string.IsNullOrEmpty(VendorNumber))
                    {
                        var vendorNumber = from m in dcHaskell.VendorNumbers
                                           where m.FK_VendorID == vendor.PK_VendorID && m.JDEVendorNumber == VendorNumber
                                           select m;

                        if (vendorNumber.ToList().Count > 0)
                        {
                            objCommand = new SqlCommand("UPDATE vendornumber set ActiveFlag = @ActiveFlag" +
                                                                   " WHERE FK_VendorID = @VendorID and JDEVendorNumber = @VendorNumber", objConnection);
                            objCommand.Parameters.AddWithValue("@VendorID", vendor.PK_VendorID);
                            objCommand.Parameters.AddWithValue("@VendorNumber", VendorNumber);
                            objCommand.Parameters.AddWithValue("@ActiveFlag", (activeFlag == 0) ? "I" : "A");
                        }
                        else
                        {
                            objCommand = new SqlCommand("INSERT INTO vendornumber (FK_VendorID,JDEVendorNumber,CreatedDate,ActiveFlag) " +
                                                        "VALUES (@FK_VendorID,@JDEVendorNumber,@CreatedDate,@ActiveFlag)", objConnection);
                            objCommand.Parameters.AddWithValue("@FK_VendorID", vendor.PK_VendorID);
                            objCommand.Parameters.AddWithValue("@JDEVendorNumber", VendorNumber);
                            objCommand.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                            objCommand.Parameters.AddWithValue("@ActiveFlag", (activeFlag == 0) ? "I" : "A");
                        }

                        return objCommand.ExecuteNonQuery();
                    }
                    else return 0;
                }

            }
            else return 0;
        }
    }

    //Insert vendor number
    public bool InsertVendorNumber(long Pk_vendorID,string PK_JDEID, ActiveFlag activeFlag)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            // G. Vera 4/19/2012 Edit Vendor Number Phase 3 Enhancements - Added this check, this should not be inserting the same JDE number if it exists
            int count = dcHaskell.VendorNumbers.Count(m => m.JDEVendorNumber.Trim() == PK_JDEID.Trim() && m.FK_VendorID == Pk_vendorID);
            if (count <= 0)
            {
                //001 - can't modify DAL so...
                //DAL.VendorNumber objvendorNumber = new DAL.VendorNumber
                //{
                //    JDEVendorNumber = PK_JDEID,
                //    FK_VendorID = Pk_vendorID,
                //    CreatedDate = DateTime.Now      //G. Vera 4/19/2012 Edit Vendor Number Phase 3 Enhancements
                //};

                //dcHaskell.VendorNumbers.InsertOnSubmit(objvendorNumber);
                //dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                SqlConnection objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
                objConnection.Open();

                SqlCommand objCommand = new SqlCommand("INSERT INTO vendornumber (FK_VendorID,JDEVendorNumber,CreatedDate,ActiveFlag) " +
                                                    "VALUES (@FK_VendorID,@JDEVendorNumber,@CreatedDate,@ActiveFlag)",objConnection);
                        objCommand.Parameters.AddWithValue("@FK_VendorID", Pk_vendorID);
                        objCommand.Parameters.AddWithValue("@JDEVendorNumber", PK_JDEID);
                        objCommand.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                        objCommand.Parameters.AddWithValue("@ActiveFlag", (activeFlag == 0) ? "I" : "A");

                return (objCommand.ExecuteNonQuery() == 1);
            }
            else
            {
                return false;
            }
        
        }
    }


    /// <summary>
    /// Delete this vendor number record
    /// G. Vera 4/19/2012 Edit Vendor Number Phase 3 Enhancements
    /// 001 - modified
    /// </summary>
    /// <param name="Pk_vendorID"></param>
    /// <param name="PK_JDEID"></param>
    /// <returns></returns>
    public bool DeleteVendorNumber(long Pk_vendorID, string JDEVendorNumber)
    {
        using (DAL.HaskellAdminDataContext dcHaskell = new DAL.HaskellAdminDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            // G. Vera 4/19/2012 Edit Vendor Number Phase 3 Enhancements - Added this check, this should not be inserting the same JDE number if it exists
            int count = dcHaskell.VendorNumbers.Count(m => m.JDEVendorNumber.Trim() == JDEVendorNumber.Trim() && m.FK_VendorID == Pk_vendorID);

            //DAL.VendorNumber objvendorNumber = dcHaskell.VendorNumbers.Single(m => m.JDEVendorNumber.Trim() == JDEVendorNumber.Trim() &&
            //                                                                        m.FK_VendorID == Pk_vendorID);
            List<DAL.VendorNumber> objvendorNumbers = (from vn in dcHaskell.VendorNumbers
                                                      where vn.JDEVendorNumber.Trim() == JDEVendorNumber.Trim()
                                                      && vn.FK_VendorID == Pk_vendorID
                                                      select vn).ToList();



            if (objvendorNumbers.Count > 0)
            {
                //001 - need to ignore instead of deleteing now.
                var objvendorNumber = objvendorNumbers.First();
                DAL.HaskellDataContext Context = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
                DAL.Vendor vendorObj = Context.Vendors.Single(v => v.PK_VendorID == Pk_vendorID);
                this.EditVendorNumber(vendorObj.Company, vendorObj.TaxID, objvendorNumber.JDEVendorNumber, true, ActiveFlag.Ignored);
                
                //dcHaskell.VendorNumbers.DeleteOnSubmit(objvendorNumber);
                //dcHaskell.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                return true;
            }
            else
            {
                return false;
            }
        }
    }

}
