﻿using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Web.Script.Serialization;
using AjaxControlToolkit;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;
using System.Linq;

#region Change Comments
/**************************************************************************************************************
 * 001 - G. Vera 06/13/2012 Phase 3 Item 4.9 (JIRA:VPI17)
 * 002 - G. Vera 10/24/2012 VMS Stabilization Release 1.3 (see JIRA issues for more details)
 * 
 ***************************************************************************************************************/
#endregion




/// <summary>
/// load text with names from database-Autocompleteextender webservice
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]

public class AutoComplete : System.Web.Services.WebService
{
    clsRegistration objRegistration = new clsRegistration();

    public AutoComplete()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    /// <summary>
    /// companies from database for auto complete extender
    /// </summary>
    /// <param name="prefixText">text to be typed before the autocomplete is activated</param>
    /// <param name="count"></param>
    /// <returns></returns>
    [WebMethod]
    public string[] GetCompletionList(string prefixText)
    {
        StringBuilder obj = new StringBuilder();
        DataSet ds = objRegistration.GetVendor(prefixText);
        List<string> items = new List<string>();
        DataTable dTable = ds.Tables[0];

        foreach (DataRow dRow in dTable.Rows)
        {
            obj.AppendLine(dRow["Name"].ToString());
            items.Add(obj.ToString());
            obj.Remove(0, obj.Length);
        }

        dTable.Dispose();
        ds.Dispose();
        return items.ToArray();
    }

    /// <summary>
    /// gets industry certificates starting with the parameter passed from text box typing.
    /// 002 - added
    /// </summary>
    /// <param name="prefixText"></param>
    /// <returns></returns>
    [WebMethod]
    public string[] GetIndustryCertStartingWith(string prefixText)
    {
        List<string> result = new List<string>();
        using (VMSDAL.Haskell_VQF dcHaskell = new VMSDAL.Haskell_VQF())
        {
           var q = (from c in dcHaskell.VQFCompanyCertifications 
                               where c.Certification.Trim().ToUpper().StartsWith(prefixText.Trim().ToUpper())
                               select c).ToList();
           if (q.Count > 0)
           {
               foreach (var item in q)
               {
                   if (!result.Contains(item.Certification))
                        result.Add(item.Certification);
               }
           }

           return result.ToArray();

        }
    }

    /// <summary>
    /// companies from database for auto complete extender
    /// </summary>
    /// <param name="prefixText">text to be typed before the autocomplete is activated</param>
    /// <param name="count"></param>
    /// <returns></returns>
    [WebMethod]
    public string[] GetEmployeeCompletionList(string prefixText)
    {
        StringBuilder obj = new StringBuilder();
        DataSet ds = objRegistration.GetEmployee(prefixText);
        List<string> items = new List<string>();
        DataTable dTable = ds.Tables[0];

        foreach (DataRow dRow in dTable.Rows)
        {
            obj.AppendLine(dRow["EmployeeName"].ToString());
            items.Add(obj.ToString());
            obj.Remove(0, obj.Length);
        }

        dTable.Dispose();
        ds.Dispose();
        return items.ToArray();
    }

    [WebMethod]
    public string[] GetVendorList(string prefixText)
    {
        StringBuilder obj = new StringBuilder();
        DataSet ds = objRegistration.GetVendor(prefixText);
        List<string> items = new List<string>();
        DataTable dTable = ds.Tables[0];

        foreach (DataRow dRow in dTable.Rows)
        {
            string item = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dRow["Name"].ToString(), dRow["PK_VendorId"].ToString());
            //obj.AppendLine(dRow["Name"].ToString());
            items.Add(item);
            //obj.Remove(0, obj.Length);
        }
        dTable.Dispose();
        ds.Dispose();
        return items.ToArray();
    }

    [WebMethod]
    public string[] GetBidPackageList(string prefixText)
    {
        List<string> items = new RiskEvaluation().GetBidPackageNumber(prefixText);
        return items.ToArray();
    }

    //Added by N Schoenberger on 8/17/2011
    [WebMethod]
    public string[] GetActiveBidPackageList(string prefixText)
    {
        DataSet ds = new RiskEvaluation().GetActiveBidPackageNumber(prefixText);

        List<string> items = new List<string>();
        DataTable dTable = ds.Tables[0];

        foreach (DataRow dRow in dTable.Rows)
        {
            items.Add(Convert.ToString(dRow[0]));
        }

        dTable.Dispose();
        ds.Dispose();
        return items.ToArray();
    }

    //001
    [WebMethod]
    public string[] GetProjectListCompletion(string prefixText)
    {
        List<string> items = new List<string>();
        DAL.HaskellProject[] ds = new clsRating().GetProject(prefixText);

        foreach (var dRow in ds)
        {
            string item = dRow.ProjectNumber.ToString().Trim();
            items.Add(item);
        }
        return items.ToArray();
    }

}

