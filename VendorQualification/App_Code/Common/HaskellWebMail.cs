﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Net.Mime;

    public class HaskellWebMail
    {
        public HaskellWebMail()
        {

        }

        /// <summary>
        /// Please catch exception as this method will return exception if send mail fails
        /// </summary>
        /// <param name="Body">The body of the email</param>
        /// <param name="isHtml">Is the body of the email HTML</param>
        /// <param name="subject"></param>
        /// <param name="emailFrom">Array of email addresses</param>
        /// <param name="emailTo"></param>
        public string SendMail(string Body, bool isHtml, string subject, string emailFrom, string[] emailTo, string[] EmailCopyTo = null)
        {
            MailMessage email = new MailMessage();

            try
            {
                email.Subject = subject;
                email.Body = Body;


                foreach (string item in emailTo)
                {
                    email.To.Add(new MailAddress(item));
                }
                if (EmailCopyTo != null)
                {
                    foreach (string item2 in EmailCopyTo)
                    {
                        if (!string.IsNullOrEmpty(item2)) email.CC.Add(new MailAddress(item2));
                    }
                }

                email.From = new MailAddress(emailFrom);
                email.IsBodyHtml = isHtml;

                SmtpClient client = new SmtpClient("smtp.haskell.com", 25);
                client.Send(email);
                return "Success";

            }
            catch (Exception e)
            {
                //TODO: do something
                return e.Message + "\n\n" + e.StackTrace;
            }

        }

        /// <summary>
        /// Please catch exception as this method will return exception if send mail fails
        /// </summary>
        /// <param name="Body">The body of the email</param>
        /// <param name="isHtml">Is the body of the email HTML</param>
        /// <param name="subject"></param>
        /// <param name="emailFrom">Array of email addresses</param>
        /// <param name="emailTo"></param>
        public void SendMail(string Body, bool isHtml, string subject, string emailFrom, string[] emailTo, string file, string[] EmailCopyTo = null)
        {
            MailMessage email = new MailMessage();
            Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);

            try
            {
                email.Subject = subject;
                email.Body = Body;


                foreach (string item in emailTo)
                {
                    email.To.Add(new MailAddress(item));
                }
                if (EmailCopyTo != null)
                {
                    foreach (string item2 in EmailCopyTo)
                    {
                        if (!string.IsNullOrEmpty(item2)) email.CC.Add(new MailAddress(item2));
                    }
                }

                email.From = new MailAddress(emailFrom);
                email.IsBodyHtml = isHtml;

                if (!string.IsNullOrEmpty(file))
                {
                    email.Attachments.Add(data);
                }

                SmtpClient client = new SmtpClient("smtp.haskell.com", 25);
                client.Send(email);

            }
            catch (Exception e)
            {
                //TODO: do something
            }

            data.Dispose();
        }

    }