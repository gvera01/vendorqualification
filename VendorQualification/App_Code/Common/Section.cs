﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Section
/// </summary>
public class Section:CommonPage
{
	public Section()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Section(string sectionName,SectionStatus secStatus)
    {
        _sectionName = sectionName;
        _sectionStatusName = secStatus.ToString();
    }

    private string _sectionName;

    public string SectionName
    {
        get { return _sectionName; }
        set { _sectionName = value; }
    }


    private string _sectionStatusName;

    public string SectionStatusName
    {
        get { return _sectionStatusName; }
        set { _sectionStatusName = value; }
    }

    public enum SectionStatus
    {
        Complete ,
        PartiallyComplete,
        Incomplete
    }
}
