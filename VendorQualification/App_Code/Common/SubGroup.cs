﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SubGroup
/// </summary>
public class SubGroup
{
	public SubGroup()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public SubGroup(string sectionName)
    {
        _sectionName = sectionName;
     
    }

    private string _sectionName;

    public string SectionName
    {
        get { return _sectionName; }
        set { _sectionName = value; }
    }
}
