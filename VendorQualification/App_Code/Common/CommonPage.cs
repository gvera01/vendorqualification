﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Data.SqlTypes;
using System.Collections;

/// <summary>
/// Summary description for CommonPage
/// </summary>
public class CommonPage : System.Web.UI.Page
{
    public string con = ConfigurationManager.ConnectionStrings["HaskellVQFConnectionString"].ToString();
    public CommonPage()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private string _userType;
    /// <summary>
    /// user of the page
    /// </summary>
    public string UserType
    {
        get
        {
            return _userType;
        }
        set
        {
            _userType = value;
        }
    }
     /// <summary>
    /// apply title in preinit event of the page
    /// </summary>
    protected override void OnPreInit(EventArgs e)
    {

        Page.Title = "Haskell";
       // this.Page.Theme = "Haskell";  
    }
    /// <summary>
    /// to load state
    /// </summary>
    public object GetStates()
    {
        List<State> lstStates = new List<State>();       
        lstStates.Add(new State("AL"));
        lstStates.Add(new State("AK"));
        lstStates.Add(new State("AZ"));
        lstStates.Add(new State("AR"));
        lstStates.Add(new State("CA"));
        lstStates.Add(new State("CO"));
        lstStates.Add(new State("CT"));
        lstStates.Add(new State("DE"));
        lstStates.Add(new State("FL"));
        lstStates.Add(new State("GA"));
        lstStates.Add(new State("HI"));
        lstStates.Add(new State("ID"));
        lstStates.Add(new State("IN"));
        lstStates.Add(new State("IA"));
        lstStates.Add(new State("KS"));
        lstStates.Add(new State("KY"));
        lstStates.Add(new State("LA"));
        lstStates.Add(new State("ME"));
        lstStates.Add(new State("MD"));
        lstStates.Add(new State("MA"));
        lstStates.Add(new State("MN"));
        lstStates.Add(new State("MS"));
        lstStates.Add(new State("MO"));
        lstStates.Add(new State("MT"));
        lstStates.Add(new State("NE"));
        lstStates.Add(new State("NV"));
        lstStates.Add(new State("NH"));
        lstStates.Add(new State("NJ"));
        lstStates.Add(new State("NM"));
        lstStates.Add(new State("NY"));
        lstStates.Add(new State("NC"));
        lstStates.Add(new State("ND"));
        lstStates.Add(new State("OH"));
        lstStates.Add(new State("OK"));
        lstStates.Add(new State("OR"));
        lstStates.Add(new State("PA"));
        lstStates.Add(new State("RI"));
        lstStates.Add(new State("SC"));
        lstStates.Add(new State("SD"));
        lstStates.Add(new State("TN"));
        lstStates.Add(new State("TX"));
        lstStates.Add(new State("UT"));
        lstStates.Add(new State("VT"));
        lstStates.Add(new State("VA"));
        lstStates.Add(new State("WA"));
        lstStates.Add(new State("WV"));
        lstStates.Add(new State("WI"));
        lstStates.Add(new State("WY"));
        //lstStates.Add(new State("Washington DC"));
        //lstStates.Add(new State("Puerto Rico"));
        //lstStates.Add(new State("International"));
        //lstStates.Add(new State("Canada"));
        //lstStates.Add(new State("Mexico"));
        lstStates.Add(new State("Other"));
        return lstStates;
    }

    /// <summary>
    /// method to add option --Select-- for dropdownlist
    /// </summary>
    public void AddListItem(DropDownList ddlName)
    {
        ListItem lst = new ListItem();
        lst.Text = "Select";
        lst.Value = "0";
        ddlName.Items.Insert(0, lst);
    }
    /// <summary>
    /// To control Side Menu for the page admin/superadmin/employee
    /// </summary>
    public void ControlVisibility(UserControl objUserControlSuperAdmin, UserControl objUserControlAdmin, UserControl objUserControlEmployee)
    {
       
        _userType = this.Page.Request.QueryString["user"];

        switch (_userType)
        {
            case "superadmin":
                objUserControlSuperAdmin.Visible = true;
                break;
            case "admin":
                objUserControlAdmin.Visible = true;             
                break;
            case "employee":
                objUserControlEmployee.Visible = true;              
                break;           
            default:
                break;
        }
    }
    /// <summary>
    /// To control Top Menu for the page
    /// </summary>
    public void ControlTopMenuVisibility(UserControl objOuter, UserControl objInner)
    {
        _userType = this.Page.Request.QueryString["menu"];

        if (!string.IsNullOrEmpty(_userType))
        {
            objInner.Visible = true;
        }
        else
        {
            objOuter.Visible = true;
        }

    }
    /// <summary>
    /// Visible top menu
    /// </summary>
    /// <param name="objOuter"></param>
    /// <param name="objInner"></param>
    public void VisibleTopMenu(UserControl objOuter, UserControl objInner)
    {
        objInner.Visible = true;
        objOuter.Visible = false;
    }
    /// <summary>
    /// DataContext of Haskell01 database
    /// </summary>
    private DAL.HaskellDataContext m_DbContext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString);
    public DAL.HaskellDataContext DbContext
    {
        get
        {
            return m_DbContext;
        }
        set
        {
            m_DbContext = value;
        }
    }


    /// <summary>
    /// G. Vera 06/02/2014: Implemented
    /// Will disable the page's validator controls based on validation group
    /// </summary>
    /// <param name="pageObject"></param>
    /// <param name="validationGroup"></param>
    public void DisableValidators(Page pageObject, string validationGroup)
    {
        ValidatorCollection vCollection = pageObject.GetValidators(validationGroup);

        foreach (BaseValidator  item in vCollection)
        {
            item.Enabled = false;
        }
    }


    


    #region Methods for Prompting the User to Save Before Leaving the Page
    /// <summary>
    /// G. Vera 06/27/2012 - Added method
    /// Will add attribute to specific web control so that value changes can be monitored.
    /// </summary>
    /// <param name="wc"></param>
    protected void MonitorChanges(WebControl wc)
    {
        if (wc is TextBox)
        {
            int c = wc.Attributes.Count;
            if (c > 0)
            {
                foreach (var item in wc.Attributes.Keys)
                {
                    var s = item.ToString();
                    if (s.ToLower() == "onkeyup") wc.Attributes["onkeyup"] += ";HasSaved('false');";
                }
            }
            else wc.Attributes["onkeyup"] = "Javascript:HasSaved('false');";
        }
        else if (wc is RadioButtonList || wc is CheckBoxList || wc is RadioButton || wc is CheckBox || wc is ListItem)
        {
            int c = wc.Attributes.Count;
            if (c > 0)
            {
                foreach (var item in wc.Attributes.Keys)
                {
                    var s = item.ToString();
                    if (s.ToLower() == "onclick") wc.Attributes["onClick"] += ";HasSaved('false');";
                }
            }
            else wc.Attributes["onClick"] = "Javascript:HasSaved('false');";
        }
        else
        {
            int c = wc.Attributes.Count;
            if (c > 0)
            {
                foreach (var item in wc.Attributes.Keys)
                {
                    var s = item.ToString();
                    if (s.ToLower() == "onchange") wc.Attributes["onchange"] += ";HasSaved('false');";
                }
            }
            else wc.Attributes["onchange"] = "Javascript:HasSaved('false');";
        }
    }

    /// <summary>
    /// G. Vera - Added
    /// Main method to be called from aspx page
    /// </summary>
    /// <param name="yourPage"></param>
    /// <param name="IsAssignClientScripts"></param>
    protected void ConfirmToSave(Control yourPage, bool IsAssignClientScripts)
    {
        if (yourPage.HasControls())
        {
            LoopThroughParentControl(yourPage);
        }
    }

    /// <summary>
    /// G. Vera - Added
    /// Will loop through page controls and assign to corresponding controls
    /// a monitor changes attribute
    /// </summary>
    /// <param name="control"></param>
    private void LoopThroughParentControl(Control control)
    {
        foreach (Control ctrl in control.Controls)
        {
            if (ctrl.HasControls())
            {
                LoopChildren(ctrl);
            }

            if (ctrl.GetType() == typeof(TextBox) ||
                ctrl.GetType() == typeof(DropDownList) ||
                ctrl.GetType() == typeof(CheckBox) ||
                ctrl.GetType() == typeof(CheckBoxList) ||
                ctrl.GetType() == typeof(RadioButton) ||
                ctrl.GetType() == typeof(RadioButtonList) ||
                ctrl.GetType() == typeof(ListItem))
            {
                MonitorChanges((WebControl)ctrl);
            }
        }
    }

    private void LoopChildren(Control control)
    {
        foreach (Control ctrlCh in control.Controls)
        {
            if (ctrlCh.HasControls()) LoopThroughParentControl(ctrlCh);

            if (ctrlCh.GetType() == typeof(TextBox) ||
                ctrlCh.GetType() == typeof(DropDownList) ||
                ctrlCh.GetType() == typeof(CheckBox) ||
                ctrlCh.GetType() == typeof(CheckBoxList) ||
                ctrlCh.GetType() == typeof(RadioButton) ||
                ctrlCh.GetType() == typeof(RadioButtonList) || 
                ctrlCh.GetType() == typeof(ListItem))
            {
                MonitorChanges((WebControl)ctrlCh);
            }
           
        }
    }

    /// <summary>
    /// G. Vera - Added method
    /// Not in use - Will register the client scripts needed for the "save changes" confirmation upon exit.
    /// </summary>
    private void AssignMonitorChangeValuesOnPageLoad()
    {
        if (!Page.IsStartupScriptRegistered("monitorChangesAssignment"))
        {
            Page.RegisterStartupScript("monitorChangesAssignment",
                @"<script language=""javascript"" type=""text/javascript"">
						window.onbeforeunload = windowOnBeforeUnload;
                        function windowOnBeforeUnload() {
	                        var hasSaved = document.getElementById(""<%=hasSaved.ClientID %>"").value;
                                if (hasSaved == 'true')
        	                        return;   // Let the page unload
                                if (window.event)
        	                        window.event.returnValue = 'Your changes will be lost if not saved.'; // IE
                                else
        	                        return 'Your changes will be lost if not saved'; // FX
                        }

                        function HasSaved(result) {
                        document.getElementById(""<%=hasSaved.ClientID %>"").value = result;
                        }

				</script>");

                        

        }
    }
    /// <summary>
    /// G. Vera - Added method
    /// Will add attribute to bypass confirmation upon page exit
    /// <param name="wc"></param>
    protected void BypassModifiedMethod(WebControl wc)
    {
        if (wc is ImageButton)
        {
            int c = wc.Attributes.Count;
            if (c > 0)
            {
                foreach (var item in wc.Attributes.Keys)
                {
                    var s = item.ToString();
                    if (s.ToLower() == "onclientclick") ((ImageButton)wc).OnClientClick += ";HasSaved('true');";
                }
            }
            else ((ImageButton)wc).OnClientClick = "Javascript:HasSaved('true')";
        }
        else if (wc is Button)
        {
            int c = wc.Attributes.Count;
            if (c > 0)
            {
                foreach (var item in wc.Attributes.Keys)
                {
                    var s = item.ToString();
                    if (s.ToLower() == "onclientclick") ((Button)wc).OnClientClick += ";HasSaved('true');";
                }
            }
            else ((Button)wc).OnClientClick = "Javascript:HasSaved('true')";
        }
        else
        {
            int c = wc.Attributes.Count;
            if (c > 0)
            {
                foreach (var item in wc.Attributes.Keys)
                {   
                    var s = item.ToString();
                    if (s.ToLower() == "onkeydown") wc.Attributes["onkeydown"] += ";HasSaved('true');";
                }
            }
            else wc.Attributes["onkeydown"] = "Javascript:HasSaved('true');";
        }
    }
    

    #endregion

}
