﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for State
/// </summary>
public class State
{
	public State()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public State(string name)
    {
        _stateName = name;
    }

    private string _stateName;

    public string StateName
    {
        get { return _stateName; }
        set { _stateName = value; }
    }
}
