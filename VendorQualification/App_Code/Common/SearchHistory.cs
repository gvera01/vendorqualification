using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

#region
/**********************************************************************************************************************
 * 001 G. Vera 07/14/2014: Add company types search by
 **********************************************************************************************************************/
#endregion

public class SearchHistory
{
    private long id;
    private string searchroot;
    private string csicodes;
    private string regioncodes;
    private string projecttypes;
    private string vendorid;
    private string jdevendorno;
    private string trackingno;
    private string taxid;
    private string vendorstatus;
    private long fk_employeeid;
    private System.Nullable<System.DateTime> createdate;
    private System.Nullable<System.DateTime> modifydate;
    private bool active;
    private string searchedrecord;
    private string otherstate;
    private long count;
    private string status;
    private string companycertification;
    private string federalsb;
    private string firstname;
    private string lastname;
    private string vendoremail;
    private string otherproject;
    private string iso;
    private string qs;
    private string providesBIM;
    private string companyTypes;    //001

    //001
    public string CompanyTypes
    {
        get { return companyTypes; }
        set { companyTypes = value; }
    }


    public string ProvidesBIM
    {
        get { return providesBIM; }
        set { providesBIM = value; }
    }
    public SearchHistory()
    {
    }
    public long Id
    {

        get
        {
            return this.id;
        }
        set
        {
            this.id = value;
        }
    }
    public string SearchRoot
    {
        get
        {
            return this.searchroot;
        }
        set
        {
            this.searchroot = value;
        }
    }
    public string CSICodes
    {
        get
        {
            return this.csicodes;
        }
        set
        {
            this.csicodes = value;
        }
    }
    public string ContinentCodes { get; set; }
          public string CountryCodes { get; set; }
    public string RegionCodes
    {
        get
        {
            return this.regioncodes;
        }
        set
        {
            this.regioncodes = value;
        }
    }
    public string ProjectTypes
    {
        get
        {
            return this.projecttypes;
        }
        set
        {
            this.projecttypes = value;
        }
    }
    public string VendorId
    {
        get
        {
            return this.vendorid;
        }
        set
        {
            this.vendorid = value;
        }
    }
    public string JDEVendorNo
    {
        get
        {
            return this.jdevendorno;
        }
        set
        {
            this.jdevendorno = value;
        }
    }
    public string TrackingNo
    {
        get
        {
            return this.trackingno;
        }
        set
        {
            this.trackingno = value;
        }
    }
    public string TaxId
    {
        get
        {
            return this.taxid;
        }
        set
        {
            this.taxid = value;
        }
    }
    public string VendorStatus
    {
        get
        {
            return this.vendorstatus;
        }
        set
        {
            this.vendorstatus = value;
        }
    }
    public long Fk_Employeeid
    {
        get
        {
            return this.fk_employeeid;
        }
        set
        {
            this.fk_employeeid = value;
        }
    }
    public System.Nullable<System.DateTime> CreateDate
    {
        get
        {
            return this.createdate;
        }
        set
        {
            this.createdate = value;
        }
    }
    public System.Nullable<System.DateTime> ModifyDate
    {
        get
        {
            return this.modifydate;
        }
        set
        {
            this.modifydate = value;
        }
    }
    public bool Active
    {
        get
        {
            return this.active;
        }
        set
        {
            this.active = value;
        }
    }
    public string SearchedRecord
    {
        get
        {
            return this.searchedrecord;
        }
        set
        {
            this.searchedrecord = value;
        }
    }
    public string OtherState
    {
        get
        {
            return this.otherstate;
        }
        set
        {
            this.otherstate = value;
        }
    }
    public long Count
    {
        get
        {
            return this.count;
        }
        set
        {
            this.count = value;
        }
    }
    public string Status
    {
        get
        {
            return this.status;
        }
        set
        {
            this.status = value;
        }
    }
    public string CompanyCertification
    {
        get
        {
            return this.companycertification;
        }
        set
        {
            this.companycertification = value;
        }
    }
    public string FederalSB
    {
        get
        {
            return this.federalsb;
        }
        set
        {
            this.federalsb = value;
        }
    }
    public string FirstName
    {
        get
        {
            return this.firstname;
        }
        set
        {
            this.firstname = value;
        }
    }
    public string LastName
    {
        get
        {
            return this.lastname;
        }
        set
        {
            this.lastname = value;
        }
    }
    public string VendorEmail
    {
        get
        {
            return this.vendoremail;
        }
        set
        {
            this.vendoremail = value;
        }
    }
    public string OtherProject
    {
        get
        {
            return this.otherproject;
        }
        set
        {
            this.otherproject = value;
        }
    }
    public string ISO
    {
        get
        {
            return this.iso;
        }
        set
        {
            this.iso = value;
        }
    }
    public string QS
    {
        get
        {
            return this.qs;
        }
        set
        {
            this.qs = value;
        }
    }
}
