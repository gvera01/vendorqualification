﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

/// <summary>
/// Summary description for RiskManagementChecklist
/// Change Comments:
/// 001 G. Vera 12/29/2016: Replace the word Subguard with Subcontractor Default Insurance
/// 002 G. Vera 11/29/2018: Impleent Risk Management Checklist into the SRE
/// 003 G. Vera 06/25/2020: Add new section for Average Risk Determination
/// </summary>
public class RiskManagementChecklist
{
    private List<VMSDAL.RiskManagementChecklist> _averageRiskSection = new List<VMSDAL.RiskManagementChecklist>();
    public List<VMSDAL.RiskManagementChecklist> AverageRiskSection
    {
        get { return _averageRiskSection; }
        set { _averageRiskSection = value; }
    }

    private List<VMSDAL.RiskManagementChecklist> _highRiskSection = new List<VMSDAL.RiskManagementChecklist>();
    public List<VMSDAL.RiskManagementChecklist> HighRiskSection
    {
        get { return _highRiskSection; }
        set { _highRiskSection = value; }
    }

    private List<VMSDAL.RiskManagementChecklist> _highRiskDetermination = new List<VMSDAL.RiskManagementChecklist>();
    public List<VMSDAL.RiskManagementChecklist> HighRiskDetermination
    {
        get { return _highRiskDetermination; }
        set { _highRiskDetermination = value; }
    }

    //003
    private List<VMSDAL.RiskManagementChecklist> _avgRiskDetermination = new List<VMSDAL.RiskManagementChecklist>();
    public List<VMSDAL.RiskManagementChecklist> AverageRiskDetermination
    {
        get { return _avgRiskDetermination; }
        set { _avgRiskDetermination = value; }
    }



    public RiskManagementChecklist()
	{
        //001 - START
        // high risk determination
        //002
        var checklist = new RiskEvaluation().GetRiskManagementChecklist();
        HighRiskDetermination.AddRange(checklist.Where(c => c.AppliesToRisk == "High" && c.Section == "Determination").OrderBy(o => o.SortOrder).ToList());

        //HighRiskDetermination.Add(@"For subcontracts ≥ $250,000, Subcontractor to remain enrolled in Subcontractor Default Insurance. PM shall implement the risk management controls identified in section B of the Risk Management Controls section below.");
        //HighRiskDetermination.Add(@"For subcontracts ≥ $250,000 and less than $1,000,000, PM to exclude Subcontractor from Subcontractor Default Insurance and enroll in SSDI. PM shall implement the risk management controls identified in section B of the Risk Management Controls section below.");
        //HighRiskDetermination.Add(@"For subcontracts < $250,000, Subcontractor to remain in the SSDI Program. PM shall implement the risk management controls identified in section B of the Risk Management Controls section below.");
        //HighRiskDetermination.Add(@"Exclude Subcontractor from both Subcontractor Default Insurance and SSDI and require a traditional payment and performance bond.  PM shall implement the risk management controls identified in section B of the Risk Management Controls section below.");

        // average risk checklist controls
        AverageRiskSection.AddRange(checklist.Where(c => c.AppliesToRisk == "Average" && c.Section == "Controls").OrderBy(o => o.SortOrder).ToList());
        AverageRiskDetermination.AddRange(checklist.Where(c => c.AppliesToRisk == "Average" && c.Section == "Determination").OrderBy(o => o.SortOrder).ToList());

        //AverageRiskSection.Add("Eligible for inclusion in the Subcontractor Default Insurance/SSDI program. ");
        //AverageRiskSection.Add("Enforce the terms of the subcontract agreement.");
        //AverageRiskSection.Add("Review the Schedule of Values to ensure payments are not front-end loaded prior\n" + "to the execution of subcontract.");
        //AverageRiskSection.Add("Monitor the Subcontractor&#39;s actual progress vs. scheduled progress on a weekly\n" + "basis.");
        //AverageRiskSection.Add("Require Subcontractor to provide second tier lien releases.");
        //AverageRiskSection.Add("Confirm status of completion by physical inspection prior to each progress payment.");

        // high risk checklist controls
        HighRiskSection.AddRange(checklist.Where(c => c.AppliesToRisk == "High" && c.Section == "Controls").OrderBy(o => o.SortOrder).ToList());
        //HighRiskSection.Add("Hold an individual pre-construction conference with Subcontractor");
        //HighRiskSection.Add("Establish and monitor a Subcontractor schedule, including manpower requirements\n" + 
        //                  "and critical deliveries");
        //HighRiskSection.Add("Verify progress of off-site fabrication activities");
        //HighRiskSection.Add("Require a more detailed Schedule of Values for monthly review");
        //HighRiskSection.Add("Enforce the Attachment A requirement of Sub-Subcontractor/Suppplier listing. Obtain list of Sub-Subcontractors and vendors");
        //HighRiskSection.Add("Call Sub-Subcontractors/Suppliers at random to ensure they are being paid");
        //HighRiskSection.Add("Enforce payment of Sub-Subcontractors/Suppliers through joint check agreements. Obtain 2nd and 3rd tier releases");

        //HighRiskSection.Add("Establish payment controls. Submit monthly cost to complete estimates to Group DOC for approval of Subcontractor payments.");
        //HighRiskSection.Add("Require Subcontractor to submit monthly sworn statements of payment obligations with supporting 2nd and 3rd tier releases");
        //HighRiskSection.Add("Hold safety preconstruction meeting and require regional safety supervisor to attend");
        //HighRiskSection.Add("Require Subcontractor to submit monthly safety inspection reports specific to his work being performed on the project");
        //001 - END
       


        
        
	}
}