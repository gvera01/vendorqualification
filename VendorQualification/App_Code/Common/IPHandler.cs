﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Web.Administration;
/// <summary>
/// Summary description for IPRestrict
/// </summary>
public class IPHandler
{
    public static string ErrMessageStartsWith = "The file '";
    public static string ErrMessageEndsWith = "' does not exist.";

	public static string Deny(string IPAddress)
    {
        using (ServerManager serverManager = new ServerManager())
        {
            Configuration config = serverManager.GetApplicationHostConfiguration();
            ConfigurationSection ipSecuritySection = config.GetSection("system.webServer/security/ipSecurity", "Default Web Site");
            ConfigurationElementCollection ipSecurityCollection = ipSecuritySection.GetCollection();

            ConfigurationElement addElement = ipSecurityCollection.CreateElement("add");
            addElement["ipAddress"] = IPAddress;
            addElement["allowed"] = false;
            try
            {
                ipSecurityCollection.Add(addElement);

                //ConfigurationElement addElement1 = ipSecurityCollection.CreateElement("add");
                //addElement1["ipAddress"] = @"169.254.0.0";
                //addElement1["subnetMask"] = @"255.255.0.0";
                //addElement1["allowed"] = false;
                //ipSecurityCollection.Add(addElement1);

                serverManager.CommitChanges();
            }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }

        return string.Empty;
    }


}