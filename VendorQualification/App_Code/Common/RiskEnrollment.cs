﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

/// <summary>
/// Summary description for RiskManagementChecklist
/// 001 G. Vera:  11/29/2018 Implement Risk Management Checklist
/// </summary>
public class RiskEnrollment
{
    private List<VMSDAL.RiskManagementChecklist> _checkList = new List<VMSDAL.RiskManagementChecklist>();
    public List<VMSDAL.RiskManagementChecklist> CheckList
    {
        get { return _checkList; }
        set { _checkList = value; }
    }

    public RiskEnrollment()
	{
        // average risk checklist
        //001
        var enrollment = new RiskEvaluation().GetRiskManagementChecklist().Where(e => e.Section == "Enrollment").OrderBy(o => o.SortOrder).ToList();
        CheckList.AddRange(enrollment);

        //CheckList.Add(" Subcontractor Default Insurance Program");
        //CheckList.Add(" SSDI Program");
        //CheckList.Add(" Traditional Payment and Performance Bond");
        //CheckList.Add(" Subcontractor Default Insurance / SSDI Waiver Form");
	}
}