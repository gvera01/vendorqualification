﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SnapshotHistory.aspx.cs"
    Inherits="SnapshotHistory" EnableEventValidation="false" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>

    <script language="javascript" type="text/javascript">
          window.onerror = ErrHndl;

          function ErrHndl()
          { return true; }
    </script>

    <link href="../css/Haskell.css" rel="Stylesheet" />
    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />

    <script src="../Script/jquery.js" type="text/javascript"></script>

    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script language="javascript" type="text/javascript">
        window.history.go(1);
    </script>

    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                    </td>
                                    <td class="contenttd">
                                        <Haskell:AdminMaintananceMenu ID="mnuMain" runat="server" />
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:HyperLink ID="hlCSICODES" CssClass="scopelink" NavigateUrl="~/Admin/AddScopes.aspx"
                                                        Text="MasterFormat" runat="server"> </asp:HyperLink>
                                                    |
                                                    <asp:HyperLink ID="HyperLink1" CssClass="scopelink" NavigateUrl="~/Admin/AddCertification.aspx"
                                                        Text=" Company Certification" runat="server"></asp:HyperLink>
                                                    |
                                                    <asp:HyperLink ID="hlVQfsnapshot" CssClass="scopelink" NavigateUrl="~/Admin/SnapshotHistory.aspx"
                                                        Text="VQF Snapshot History" runat="server"></asp:HyperLink>
                                                         |
                                                    <asp:HyperLink ID="hlSREsnapshot" CssClass="scopelink" NavigateUrl="~/Admin/SnapshotHistorySRE.aspx"
                                                        Text="SRE Snapshot History" runat="server"></asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="formhead">
                                                    VQF Snapshot History
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                            
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                        <tr>
                                                            <td class="formtdlt" width="41%" valign="top">
                                                                Vendor Name from Vendor Database
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtVendor" runat="server" CssClass="ajaxtxtbox" MaxLength="60" TabIndex="1"></asp:TextBox>
                                                                <div id="listPlacement" style="height: 100px; overflow: scroll;">
                                                                </div>
                                                                <Ajax:AutoCompleteExtender ID="autoComplete1" runat="server" BehaviorID="AutoCompleteEx"
                                                                    CompletionListElementID="listPlacement" EnableCaching="false" FirstRowSelected="True"
                                                                    MinimumPrefixLength="1" ServiceMethod="GetCompletionList" ServicePath="~/AutoComplete.asmx"
                                                                    TargetControlID="txtVendor" CompletionSetCount="10">
                                                                    <Animations>
                                                            <OnShow>
                                                              <Sequence>
                                                                <OpacityAction Opacity="0" />
                                                                <HideAction Visible="true" />
                                                            
                                                                    <ScriptAction Script="
                                                                        var behavior = $find('AutoCompleteEx');
                                                                        if (!behavior._height) {
                                                                            var target = behavior.get_completionList();
                                                                            behavior._height = target.offsetHeight - 2;
                                                                            target.style.height = '0px';
                                                                        }" />
                                                                <Parallel Duration=".4">
                                                                <FadeIn />
                                                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                                                                </Parallel>
                                                                </Sequence>
                                                                 </OnShow>
                                                                  <OnHide>
                                                                 <Parallel Duration=".4">
                                                                 <FadeOut />
                                                                 <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                                                                 </Parallel>
                                                                 </OnHide>
                                                                    </Animations>
                                                                </Ajax:AutoCompleteExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdlt">
                                                                From Date
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtFDate" CssClass="txtboxmedium" runat="server" MaxLength="10"
                                                                    TabIndex="2"></asp:TextBox>
                                                                 <Ajax:FilteredTextBoxExtender ID="ftxtFDate" TargetControlID="txtFDate"   FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0,/" InvalidChars="A-Z,a-z,-,.," runat="server">
                                                                        </Ajax:FilteredTextBoxExtender>
                                                                <asp:ImageButton ID="imgFDate" ImageAlign="AbsMiddle" ImageUrl="~/Images/cal.gif"
                                                                    runat="server" />
                                                                <Ajax:CalendarExtender ID="caleFDate" TargetControlID="txtFDate" PopupButtonID="imgFDate"
                                                                    runat="server" Format="MM/dd/yyyy">
                                                                </Ajax:CalendarExtender>
                                                                <asp:CustomValidator ID="cusFDate" runat="server" 
                                                                    onservervalidate="cusFDate_ServerValidate" ControlToValidate="txtFDate"   ></asp:CustomValidator>
                                                           
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdlt" valign="top">
                                                                To Date
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtTDate" CssClass="txtboxmedium" runat="server" MaxLength="10"
                                                                    TabIndex="3"></asp:TextBox>
                                                                <Ajax:FilteredTextBoxExtender ID="ftxtTDate" TargetControlID="txtTDate" FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0,/" InvalidChars="A-Z,a-z,-,.," runat="server">
                                                                </Ajax:FilteredTextBoxExtender>
                                                                <asp:ImageButton ID="imgTDate" ImageAlign="AbsMiddle" ImageUrl="~/Images/cal.gif"
                                                                    runat="server" TabIndex="4" />
                                                                <Ajax:CalendarExtender ID="caleTDate" TargetControlID="txtTDate" PopupButtonID="imgTDate"
                                                                    runat="server" Format="MM/dd/yyyy">
                                                                </Ajax:CalendarExtender>
                                                                <asp:CustomValidator ID="cusTDate" runat="server" 
                                                                    onservervalidate="cusTDate_ServerValidate" ControlToValidate="txtTDate"   ></asp:CustomValidator>
                                                                     
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td class="formtdrt">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:ImageButton ID="imbSearchVQF" ImageUrl="~/Images/search.jpg" ImageAlign="AbsMiddle"
                                                                    runat="server" ToolTip="Searh VQF" OnClick="imbSearchVQF_Click" TabIndex="3" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodytextcenter" colspan="2">
                                                                <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                                <asp:Label ID="lblmsgInfo" runat="server" CssClass="errormsg "></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="tdheight">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="tdheight">
                                                            </td>
                                                        </tr>
                                                        <tr id="Search" runat="server">
                                                            <td class="bodytextcenter" valign="middle">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                Total Number Of Records:
                                                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                            </td>
                                                            <td class="bodytextcenter" valign="middle" align="center">
                                                                Number Of Records Per Page:
                                                                <%--</td>
                                                                            <td width="5%">--%>
                                                                <asp:DropDownList ID="ddlNumber" runat="server" AutoPostBack="True" CssClass="ddlbox"
                                                                    OnSelectedIndexChanged="ddlNumber_SelectedIndexChanged">
                                                                    <asp:ListItem>10</asp:ListItem>
                                                                    <asp:ListItem>20</asp:ListItem>
                                                                    <asp:ListItem>30</asp:ListItem>
                                                                    <asp:ListItem>40</asp:ListItem>
                                                                    <asp:ListItem>50</asp:ListItem>
                                                                    <%-- Included by SGS as Per Issue No 5--%>
                                                                    <asp:ListItem>100</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodytextcenter" valign="middle">
                                                                &nbsp;
                                                            </td>
                                                            <td class="bodytextcenter" valign="middle" align="center">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr id="trSRE" runat="server">
                                                            <td colspan="2" align="center">
                                                                <asp:GridView ID="grvVQFHistory" runat="server" CellPadding="4" GridLines="None"
                                                                    Width="70%" CellSpacing="1" AutoGenerateColumns="False" OnRowCommand="grvVQFHistory_RowCommand"
                                                                    OnRowDataBound="grvVQFHistory_RowDataBound" AllowSorting="true" 
                                                                    OnSorting="grvVQFHistory_Sorting">
                                                                    <RowStyle CssClass="listone" />
                                                                    <AlternatingRowStyle CssClass="listtwo" />
                                                                    <HeaderStyle CssClass="listheading" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Vendor Name" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                            ItemStyle-CssClass="gridpad" SortExpression="Company">
                                                                            <ItemTemplate>
                                                                                <%#Eval("Company") %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Created Date" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                            ItemStyle-CssClass="gridpad" SortExpression="CreatedDate">
                                                                            <ItemTemplate>
                                                                                <%#Eval("CreatedDate") %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Snapshot VQF" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                            ItemStyle-CssClass="gridpad">
                                                                            <ItemTemplate>
                                                                              <%--  <asp:ImageButton ID="imbVQFSnapshot" runat="server" CommandName="VQFSnapshot" ImageAlign="AbsMiddle"
                                                                                    Visible="true" ImageUrl="~/Images/vqf.png" CommandArgument='<%#Eval("PdfName") %>' />--%>
                                                                                    <asp:LinkButton ID="imbVQFSnapshot" runat="server" CommandName="VQFSnapshot" CommandArgument='<%#Eval("PdfName") %>'
                                                                                                         ToolTip="VQF Snapshot" ><img src="../Images/vqf.png" alt="VQF Snapshot"    />  </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                      
                                                                        </asp:TemplateField>
                                                                     <%--   <asp:TemplateField HeaderText="Snapshot SRE" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imbSRESnapshot" runat="server" CommandName="SRESnapshot" ImageAlign="AbsMiddle"
                                                                                    Visible="true" ImageUrl="~/Images/sre.png" CommandArgument='<%#Eval("FilePath") %>' />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>--%>
                                                                    </Columns>
                                                                    <%-- <EmptyDataTemplate>
                                                                                No Records to Display...
                                                                            </EmptyDataTemplate>--%>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="center">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="center" class="bodytextcenter" id="tdnextPrevious1" runat="server">
                                                                Page&nbsp;
                                                                <asp:ImageButton ID="imbPrevious" runat="server" ImageUrl="~/Images/pre.jpg" OnClick="imbPrevious_Click" />
                                                                &nbsp;&nbsp;<asp:PlaceHolder ID="plcCrntPage1" runat="server"></asp:PlaceHolder>
                                                                of
                                                                <asp:Label ID="lblTotalPage" runat="server"></asp:Label>
                                                                <asp:ImageButton ID="imbNext" runat="server" ImageUrl="~/Images/next1.jpg" OnClick="imbNext_Click" />
                                                                <asp:HiddenField ID="hdnPage" runat="server" />
                                                                <asp:HiddenField ID="hdnCount" runat="server" />
                                                                <asp:HiddenField ID="hdnFlag" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <Ajax:ModalPopupExtender ID="mpopAlert" runat="server" BackgroundCssClass="popupbg"
                                            TargetControlID="hdnAlert" PopupControlID="divAlert">
                                        </Ajax:ModalPopupExtender>
                                        <div id="divAlert" class="popupdivbg">
                                            <table cellpadding="3" cellspacing="1" width="100%">
                                                <tr>
                                                    <td class="Messageheading">
                                                        Message
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdHeight">&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl" style="padding-right: 0px;">
                                                        <asp:Label ID="lblMessage" CssClass="errormsg" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl tdHeight">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" >
                                                        <asp:ImageButton ID="imbOK" runat="server" ImageUrl="~/Images/ok.jpg" ImageAlign="AbsMiddle"
                                                            ToolTip="OK" OnClick="imbOK_Click" />
                                                    </td>
                                                </tr>
                                                <tr class="popupdivcl tdHeight">
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <asp:HiddenField ID="hdnAlert" runat="server" />
                                        <asp:HiddenField ID="hdnSort" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>

<script language="javascript" type="text/javascript">
function GetValue(i, ctrl) {
        var ctrl1 = document.getElementById(ctrl);
        ctrl1.value = i;
    }
</script>

