﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using VMSDAL;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 07/18/2012: VMS Stabilization Release 1.2 (JIRA:VPI-18)
 * 003 G. Vera 11/20/2012: VMS Stabilization Release 1.3 (see JIRA issues for details)
 * 004 G. vera 02/28/2013: VMS Enhancement Release 1.3.1 (BIM Questions)
 * 005 Sooraj Sudhakaran.T 10/04/2013: VMS Enhancements Phase 3 (International Tax ID/ Business ID)
 * 006 Sooraj Sudhakaran.T 10/04/2013: VMS Enhancements Phase 3 (Search by continent and countries)
 ****************************************************************************************************************/
#endregion

public partial class SavedSearches : System.Web.UI.Page
{

    //Instantiate the class
    clsSearchVendor objSearchVendor = new clsSearchVendor();
    clsRegistration objRegistration = new clsRegistration();
    Common objCommon = new Common();
    public int flag = 0;
    public string SortExp;
    public string SearchSortExp;
    public string SortVendorExp;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        string strPageRequest = Request.Form["__EVENTTARGET"];
        //Call method for paging
        CreateSearchPaging(true, strPageRequest);
        //Call method for paging
        CreatePaging(true, strPageRequest);
    }

    /// <summary>
    /// Search Paging to create controls dynamically
    /// </summary>
    /// <param name="FromLoad"></param>
    /// <param name="strPageRequest"></param>
    private void CreateSearchPaging(bool FromLoad, string strPageRequest)
    {
        if(FromLoad)
        {
            if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page"]))) && strPageRequest.ToUpperInvariant() != "DDLSEARCHNUMBER" && hdnFlag.Value != "1")
            {
                for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
                {
                    if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                    {
                        LinkButton lnkPage = new LinkButton();
                        lnkPage.Click += new EventHandler(lnk1_Click);
                        if (i == Convert.ToInt32(hdnPage.Value))
                        {
                            lnkPage.CssClass = "Selectlnkcolor";
                        }
                        else
                        {
                            lnkPage.CssClass = "lnkcolor";
                        }
                        plcCrntPage1.Controls.Add(lnkPage);
                        lnkPage.ID = "lnk" + Convert.ToString(i);
                        lnkPage.Text = Convert.ToString(i);
                        lnkPage.Attributes.Add("onClick", "GetValue(" + lnkPage.Text.Trim() + ",'" + hdnPage.ClientID + "')");
                    }
                }
            }
        }
        else
        {
            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {
                    LinkButton lnkPage = new LinkButton();
                    lnkPage.Click += new EventHandler(lnk1_Click);
                    lnkPage.Text = Convert.ToString(i);
                    if (i == Convert.ToInt32(hdnPage.Value))
                    {
                        lnkPage.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnkPage.CssClass = "lnkcolor";
                    }
                    plcCrntPage1.Controls.Add(lnkPage);
                    lnkPage.Text = Convert.ToString(i);
                    lnkPage.ID = "lnk" + Convert.ToString(i);
                    lnkPage.Attributes.Add("onClick", "GetValue(" + lnkPage.Text.Trim() + ",'" + hdnPage.ClientID + "')");
                }
            }
        }
    }

    /// <summary>
    /// Apply css for the selected link
    /// </summary>
    /// <param name="CurrentPage"></param>
    private void StyleForPaging(string CurrentPage)
    {
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnkPage = (LinkButton)ctrl;
                if (lnkPage.Text == CurrentPage)
                {
                    lnkPage.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnkPage.CssClass = "lnkcolor";
                }
            }
        }
    }

    /// <summary>
    /// Create Paging
    /// </summary>
    /// <param name="FromLoad"></param>
    /// <param name="strPageRequest"></param>
    private void CreatePaging(bool FromLoad, string strPageRequest)
    {
        if (FromLoad)
        {
            if ((!string.IsNullOrEmpty(hdnPage1.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page1"]))) && strPageRequest.ToUpperInvariant() != "DDLNUMBER" && strPageRequest.Contains("grdSaveSearch") == false )
            {
                for (int i = (Convert.ToInt32(hdnPage1.Value) - 5); i <= (Convert.ToInt32(hdnPage1.Value) + 5); i++)
                {
                    if (i > 0 && i <= Convert.ToInt32(Session["Page1"]))
                    {
                        LinkButton lnk = new LinkButton();
                        lnk.Click += new EventHandler(lnk_Click);
                        lnk.Text = i + "";
                        if (i == Convert.ToInt32(hdnPage1.Value))
                        {
                            lnk.CssClass = "Selectlnkcolor";
                        }
                        else
                        {
                            lnk.CssClass = "lnkcolor";
                        }
                        plcCrntPage.Controls.Add(lnk);
                        lnk.Text = Convert.ToString(i);
                        lnk.ID = "lnkPage" + Convert.ToString(i);
                        lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text + ",'" + hdnPage1.ClientID + "')");
                    }
                }
            }
        }
        else
        {
            if (Convert.ToInt32(Session["Page1"]) >= 5)
            {
                for (int i = 1; i <= 5; i++)
                {
                    LinkButton lnk2 = new LinkButton();
                    lnk2.Click += new EventHandler(lnk_Click);
                    lnk2.Text = i + "";
                    if (i == Convert.ToInt32(hdnPage1.Value))
                    {
                        lnk2.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnk2.CssClass = "lnkcolor";
                    }
                    plcCrntPage.Controls.Add(lnk2);
                    lnk2.Text = Convert.ToString(i);
                    lnk2.ID = "lnkPage" + Convert.ToString(i);
                    lnk2.Attributes.Add("onClick", "GetValue(" + lnk2.Text.Trim() + ",'" + hdnPage1.ClientID + "')");
                }
            }
            else
            {
                for (int i = (Convert.ToInt32(hdnPage1.Value) - 5); i <= (Convert.ToInt32(hdnPage1.Value) + 5); i++)
                {
                    if (i > 0 && i <= Convert.ToInt32(Session["Page1"]))
                    {
                        LinkButton lnk1 = new LinkButton();
                        lnk1.Click += new EventHandler(lnk_Click);
                        lnk1.Text = i + "";
                        if (i == Convert.ToInt32(hdnPage1.Value))
                        {
                            lnk1.CssClass = "Selectlnkcolor";
                        }
                        else
                        {
                            lnk1.CssClass = "lnkcolor";
                        }
                        plcCrntPage.Controls.Add(lnk1);
                        lnk1.Text = Convert.ToString(i);
                        lnk1.ID = "lnkPage" + Convert.ToString(i);
                        lnk1.Attributes.Add("onClick", "GetValue(" + lnk1.Text.Trim() + ",'" + hdnPage1.ClientID + "')");
                    }
                }
            }
        }
    }

    /// <summary>
    /// Display Previous and next button
    /// </summary>
    private void DisplaySearchPreviousNext()
    {
        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbSearchNext.Visible = false;
        }
        else
        {
            imbSearchNext.Visible = true;
        }
        if ((Convert.ToString(hdnPage.Value) == "1") || (Convert.ToString(hdnPage.Value) == "0"))
        {
            imbSearchPrevious.Visible = false;
        }
        else
        {
            imbSearchPrevious.Visible = true;
        }
        if (Convert.ToInt32(hdnPage.Value) == Convert.ToInt32(Session["Page"]))
        {
            imbSearchNext.Visible = false;
        }
    }

    /// <summary>
    /// Apply css for paging
    /// </summary>
    /// <param name="CurrentPage"></param>
    private void StyleForSearchPaging(string CurrentPage)
    {
        foreach (Control ctrl in plcCrntPage1.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnkPage = (LinkButton)ctrl;
                if (lnkPage.Text.Trim() == CurrentPage)
                {
                    lnkPage.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnkPage.CssClass = "lnkcolor";
                }
            }
        }
    }

    /// <summary>
    /// Method to display previous and next button
    /// </summary>
    private void DisplayPreviousNext()
    {
        if (Convert.ToInt32(Session["Page1"]) <= Convert.ToInt32(hdnPage1.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if ((Convert.ToString(hdnPage1.Value) == "1") || (Convert.ToString(hdnPage1.Value) == "0"))
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        if (Convert.ToInt32(hdnPage1.Value) == Convert.ToInt32(Session["Page1"]))
        {
            imbNext.Visible = false;
        }
    }

    /// <summary>
    /// Page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }

        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");
            
            imbSearchCancel.Attributes.Add("onClick", "ClosePopUp();");
            imgClosePopup.Attributes.Add("onClick", "ClosePopUp();");
            if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
            }
            else
            {
                hdnFlag.Value = "0";
                Session["Page1"] = null;
                mnuMain.DisplayButton(3);
                if (Convert.ToString(Session["Access"]) == "2")
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                {
                    AdminMenu.DisplayMenu("Admin");
                }
                Session["Page"] = null;
                //BindVendor();
                imgSearchButton.Attributes.Add("onClick", "AssignValue('" + hdnFlag.ClientID + "');");
                hdnPage.Value = "1";
                ddlSearchNumber.SelectedIndex = 0;
                hdnCount.Value = Convert.ToString(objSearchVendor.GetSearchPriorCount(txtEmployeeName.Text.Trim()));
                int page = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlSearchNumber.SelectedItem.Value))));
                lblSearchTotal.Text = Convert.ToString(hdnCount.Value);
                Session["Page"] = Convert.ToString(page);
                lblSearchTotalPage.Text = Convert.ToString(Session["Page"]);

                DisplaySearchPreviousNext();

                if (Convert.ToString(Session["Access"]) == "2")
                {
                    trVendorStatus.Style["display"] = "none";
                    trTrackingNumber.Style["display"] = "none";
                }
                else
                {
                    trVendorStatus.Style["display"] = "";
                    trTrackingNumber.Style["display"] = "";
                }
                SaveBind(txtEmployeeName.Text.Trim());
            }
        }
       
        string strPageRequest = Request.Form["__EVENTTARGET"];
        if (hdnPage1.Value != "-1")
        {
            CreatePaging(true, strPageRequest);
        }
        if (((hdnPage1.Value == "-1") || string.IsNullOrEmpty(hdnPage1.Value)) && (strPageRequest !=null && strPageRequest.Contains("grdSaveSearch") == false))
        {
            CreateSearchPaging(true, strPageRequest);
        }
        
        hdnFlag.Value = "0";
        //txtEmployeeName.Text = "";   
    }

    /// <summary>
    /// Display Search details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSaveSearch_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        modalExtnd2.Show();
        tblTradeInfo.Width = Unit.Percentage(100);
        HiddenField hdn = new HiddenField();
        hdn = (HiddenField)grdSaveSearch.Rows[e.RowIndex].FindControl("hdnHistoryId");
        long Rowvalue = Convert.ToInt64(hdn.Value);
        ViewState["RowValue"] = Rowvalue;
        LinkButton lblSearch = (LinkButton)grdSaveSearch.Rows[e.RowIndex].FindControl("lblsearch");

        //002 DAL.UspGetSearchedDetailsResult[] objdetails = objSearchVendor.GetSearchDetails(Rowvalue);
        List<SearchHistory> objdetails = objSearchVendor.GetSearchDetails(Rowvalue);  //002
        if (objdetails.Count > 0)   //002
        {
            lblVendorNumber.Text = objdetails[0].JDEVendorNo;
            ViewState["OtherState"] = objdetails[0].OtherState;

            lbloName.Text = objdetails[0].VendorId;
            lblTrackingNumber.Text = objdetails[0].TrackingNo;
            lblFederalTaxId.Text = objdetails[0].TaxId; //005-Sooraj
            lblStatus.Text = objdetails[0].Status;
            lblCertification.Text = objdetails[0].CompanyCertification;
            lblFederalSB.Text = objdetails[0].FederalSB;
            lblFirstName.Text = objdetails[0].FirstName;    //002
            lblLastName.Text = objdetails[0].LastName;      //002
            lblEmail.Text = objdetails[0].VendorEmail;      //002
            lblSavedISO.Text = (!string.IsNullOrEmpty(objdetails[0].ISO)) ? (" ISO " + objdetails[0].ISO) : string.Empty;    //003
            lblSavedQS.Text = (!string.IsNullOrEmpty(objdetails[0].QS)) ? "  QS " + objdetails[0].QS : string.Empty;      //003
            lblSavedOtherProjects.Text = (!string.IsNullOrEmpty(objdetails[0].OtherProject)) ? (" Other:&nbsp;&nbsp;&nbsp;" + objdetails[0].OtherProject) : string.Empty;      //003
            lblBIM.Text = (objdetails[0].ProvidesBIM == "Y") ? "Y" : "";        //005

        }
        else
        {
            lblVendorNumber.Text = string.Empty;
            lbloName.Text = string.Empty;
            lblTrackingNumber.Text = string.Empty;
            lblFederalTaxId.Text = string.Empty;
            lblStatus.Text = string.Empty;
            lblCertification.Text = string.Empty;
            lblFederalSB.Text = string.Empty;
            lblFirstName.Text = objdetails[0].FirstName;    //002
            lblLastName.Text = objdetails[0].LastName;      //002
            lblEmail.Text = objdetails[0].VendorEmail;      //002
        }
        //003
        VMSDAL.USPGetSearchedRecordResult[] objResult =
            objSearchVendor.GetSearchHistory(Rowvalue).Where(c => c.ScopeCodeType.ToUpper() != ScopesCodeType.PMMI.ToString().ToUpper()).ToArray(); int count = objResult.Count(m => m.ParentID == "-1");
        int flag = count - 1;
        int StaticFlag = 0;

        if (count > 0)
        {
            int count1 = count - (Convert.ToInt32(Math.Round(Convert.ToDouble(count / 2))));
            if (count1 >= 0 && count1 <= count)
            {
                if (count1 == 0)
                {
                    count1 = 1;
                }
                for (int i = 0; i < count1; i++)
                {
                    StaticFlag += 1;
                    flag += 1;
                    Table tbl = new Table();
                    TableRow tr = new TableRow();
                    TableCell tc = new TableCell();
                    Label lbl = new Label();
                    TreeView tv = new TreeView();
                    TreeNode tn = new TreeNode();
                    TreeNode tn1 = new TreeNode();
                    TreeNode tn2 = new TreeNode();
                    int leafCount = 0;
                    int flag1 = 0;
                    int? RootId = 0;
                    if (objResult[StaticFlag - 1].ParentID == "-1")
                    {
                        lbl.CssClass = "tree_head";
                        lbl.Text = objResult[StaticFlag - 1].Name;
                        RootId = objResult[StaticFlag - 1].RootNode;
                        leafCount = objSearchVendor.GetHistoryCount(Rowvalue, Convert.ToInt32(RootId));
                        tc.Controls.Add(lbl);
                    }

                    //003

                    #region Get Children
                    //004
                    foreach (var ch in objResult.Where(s => s.RootNode == Convert.ToInt32(RootId)).ToArray())
                    {
                        tv.ShowExpandCollapse = false;
                        tv.CssClass = "Treestyle";
                        tv.NodeStyle.CssClass = "list_display";
                        tv.LeafNodeStyle.CssClass = "tree_display1";
                        tv.Attributes.Add("onClick", "return test();");

                        if (!string.IsNullOrEmpty(Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID)))
                        {
                            if (ch.ParentID == Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID))
                            {
                                tn = new TreeNode();
                                tn.Text = ch.Name;
                                tn.Value = Convert.ToString(ch.PK_ScopeID);
                                tv.Nodes.Add(tn);
                            }
                        }
                        if (!string.IsNullOrEmpty(tn.Value))
                        {
                            if (ch.ParentID == tn.Value)
                            {
                                tn1 = new TreeNode();
                                tn1.Text = ch.Name;
                                tn1.Value = Convert.ToString(ch.PK_ScopeID);
                                tn.ChildNodes.Add(tn1);

                            }
                        }

                    }

                    //for (int j = flag; j < ((flag) + leafCount - 1); j++)
                    //{
                    //    tv.ShowExpandCollapse = false;

                    //    tv.CssClass = "Treestyle";
                    //    tv.NodeStyle.CssClass = "list_display";
                    //    tv.LeafNodeStyle.CssClass = "tree_display1";
                    //    tv.Attributes.Add("onClick", "return test();");

                    //    if (!string.IsNullOrEmpty(Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID)))
                    //    {
                    //        if (objResult[j].ParentID == Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID))
                    //        {
                    //            tn = new TreeNode();
                    //            tn.Text = objResult[j].Name;
                    //            tn.Value = Convert.ToString(objResult[j].PK_ScopeID);
                    //            tv.Nodes.Add(tn);

                    //        }
                    //    }

                    //    if (!string.IsNullOrEmpty(tn.Value))
                    //    {
                    //        if (objResult[j].ParentID == tn.Value)
                    //        {
                    //            tn1 = new TreeNode();
                    //            tn1.Text = objResult[j].Name;
                    //            tn1.Value = Convert.ToString(objResult[j].PK_ScopeID);
                    //            tn.ChildNodes.Add(tn1);
                    //        }
                    //    }
                    //    flag1 += 1;
                    //}
                    #endregion

                    flag = flag + (flag1 - 1);
                    tv.ExpandAll();

                    tc.Controls.Add(tv);
                    tr.Cells.Add(tc);
                    tbl.Rows.Add(tr);
                    tdCell1.Controls.Add(tbl);
                }

                for (int i = count1; i < count; i++)
                {
                    StaticFlag += 1;
                    flag += 1;
                    Table tbl = new Table();
                    TableRow tr = new TableRow();
                    TableCell tc = new TableCell();
                    Label lbl = new Label();
                    TreeView tv = new TreeView();
                    TreeNode tn = new TreeNode();
                    TreeNode tn1 = new TreeNode();
                    TreeNode tn2 = new TreeNode();
                    int leafCount = 0;
                    int flag1 = 0;
                    int? RootId = 0;
                    if (objResult[StaticFlag - 1].ParentID == "-1")
                    {
                        lbl.CssClass = "tree_head";
                        lbl.Text = objResult[StaticFlag - 1].Name;
                        RootId = objResult[StaticFlag - 1].RootNode;
                        leafCount = objSearchVendor.GetHistoryCount(Rowvalue, Convert.ToInt32(RootId));
                        tc.Controls.Add(lbl);
                    }

                    //003
                    #region Get Children for next column
                    foreach (var ch in objResult.Where(s => s.RootNode == Convert.ToInt32(RootId)).ToArray())
                    {
                        tv.ShowExpandCollapse = false;
                        tv.CssClass = "Treestyle";
                        tv.NodeStyle.CssClass = "list_display";
                        tv.LeafNodeStyle.CssClass = "tree_display1";
                        tv.Attributes.Add("onClick", "return test();");

                        if (!string.IsNullOrEmpty(Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID)))
                        {
                            if (ch.ParentID == Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID))
                            {
                                tn = new TreeNode();
                                tn.Text = ch.Name;
                                tn.Value = Convert.ToString(ch.PK_ScopeID);
                                tv.Nodes.Add(tn);
                            }
                        }
                        if (!string.IsNullOrEmpty(tn.Value))
                        {
                            if (ch.ParentID == tn.Value)
                            {
                                tn1 = new TreeNode();
                                tn1.Text = ch.Name;
                                tn1.Value = Convert.ToString(ch.PK_ScopeID);
                                tn.ChildNodes.Add(tn1);

                            }
                        }

                    }

                    //for (int j = flag; j < ((flag) + leafCount - 1); j++)
                    //{
                    //    tv.ShowExpandCollapse = false;
                    //    tv.CssClass = "Treestyle";
                    //    tv.NodeStyle.CssClass = "list_display";
                    //    tv.LeafNodeStyle.CssClass = "tree_display1";
                    //    tv.Attributes.Add("onClick", "return test();");

                    //    if (!string.IsNullOrEmpty(Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID)))
                    //    {
                    //        if (objResult[j].ParentID == Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID))
                    //        {
                    //            tn = new TreeNode();
                    //            tn.Text = objResult[j].Name;
                    //            tn.Value = Convert.ToString(objResult[j].PK_ScopeID);
                    //            tv.Nodes.Add(tn);
                    //        }
                    //    }

                    //    if (!string.IsNullOrEmpty(tn.Value))
                    //    {
                    //        if (objResult[j].ParentID == tn.Value)
                    //        {
                    //            tn1 = new TreeNode();
                    //            tn1.Text = objResult[j].Name;
                    //            tn1.Value = Convert.ToString(objResult[j].PK_ScopeID);
                    //            tn.ChildNodes.Add(tn1);

                    //        }
                    //    }
                    //    flag1 += 1;
                    //}

                    #endregion

                    flag = flag + (flag1 - 2);
                    tv.ExpandAll();
                    tc.Controls.Add(tv);
                    tr.Cells.Add(tc);
                    tbl.Rows.Add(tr);
                    tdCell2.Controls.Add(tbl);
                }
            }
        }

        //003 - PMMI
        VMSDAL.USPGetSearchedRecordResult[] objResult2 =
            objSearchVendor.GetSearchHistory(Rowvalue).Where(c => c.ScopeCodeType.ToUpper() == ScopesCodeType.PMMI.ToString().ToUpper()).ToArray();
        foreach (var pmmi in objResult2)
        {
            Table tbl = new Table();
            TableRow tr = new TableRow();
            TableCell tc = new TableCell();
            Label lbl = new Label();

            lbl.CssClass = "tree_head";
            lbl.Text = pmmi.Name;
            tc.Controls.Add(lbl);
            tr.Cells.Add(tc);
            tbl.Rows.Add(tr);
            tdCell2.Controls.Add(tbl);
        }

        // Display trade continents
        dlContinents.DataSource = objSearchVendor.GetSearchedContinents(Rowvalue,false,string.Empty);
        dlContinents.DataBind();

        // Display trade regions
        DAL.USPGetSearchedRegionsResult[] objRegions = objSearchVendor.GetSearchRegions(Rowvalue);
        dlRegions.DataSource = objRegions.Where(m => m.ParentID == -1);
        dlRegions.DataBind();
     
        dlProjects.DataSource = objSearchVendor.GetSearchProjectTypes(Rowvalue);
        dlProjects.DataBind();

        string[] SearchRoot = lblSearch.Text.Split(',');
        trTypesOfProjects.Style["display"] = "none";
        dlProjects.Style["display"] = "none";
        trGeographicalLocations.Style["display"] = "none";
        dlRegions.Style["display"] = "none";

        trscopes.Style["display"] = "none";
        tblTradeInfo.Style["display"] = "none";

        tblVendor.Style["display"] = "none";
        VendorNameHead.Style["display"] = "none";

        trCertification.Style["display"] = "none";
        tblCertification.Style["display"] = "none";

        for (int i = 0; i < SearchRoot.Count(); i++)
        {
            if (SearchRoot[i].Trim() == "All")
            {
                tblVendor.Style["display"] = "";
                VendorNameHead.Style["display"] = "";
                trscopes.Style["display"] = "";
                tblTradeInfo.Style["display"] = "";
                trGeographicalLocations.Style["display"] = "";
                dlRegions.Style["display"] = "";
                trTypesOfProjects.Style["display"] = "";
                dlProjects.Style["display"] = "";
            }
            if (SearchRoot[i].Trim() == "Types Of Projects")
            {
                trTypesOfProjects.Style["display"] = "";
                dlProjects.Style["display"] = "";
            }
            if (SearchRoot[i].Trim() == " Types Of Projects")
            {
                trTypesOfProjects.Style["display"] = "";
                dlProjects.Style["display"] = "";
            }

            if (SearchRoot[i].Trim() == "Geographic Locations")
            {
                trGeographicalLocations.Style["display"] = "";
               dlRegions.Style["display"] = "";
            }
        

            if (SearchRoot[i].Trim() == "CSI Codes")
            {
                trscopes.Style["display"] = "";
                tblTradeInfo.Style["display"] = "";
            }
            if (SearchRoot[i].Trim() == " CSI Codes")
            {
                trscopes.Style["display"] = "";
                tblTradeInfo.Style["display"] = "";
            }
            if (SearchRoot[i].Trim() == "Vendor Name")
            {
                tblVendor.Style["display"] = "";
                VendorNameHead.Style["display"] = "";
            }
            if (SearchRoot[i] == "Certification")
            {
                trCertification.Style["display"] = "";
                tblCertification.Style["display"] = "";
            }
            if (SearchRoot[i] == " Certification")
            {
                trCertification.Style["display"] = "";
                tblCertification.Style["display"] = "";
            }
        }
    }

    /// <summary>
    /// Display the content in the gridview
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSaveSearch_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lblSearch = new LinkButton();
            lblSearch = (LinkButton)e.Row.FindControl("lblsearch");
            HiddenField hdn = (HiddenField)e.Row.FindControl("hdnCount");
            Label lblRoot = (Label)e.Row.FindControl("lblRoot");
            string Search;
            string SearchRoot = string.Empty;
            Search = lblSearch.Text;

            //005 to find out if they checked BIM
            HiddenField hdnRowId = new HiddenField();
            hdnRowId = (HiddenField)e.Row.FindControl("hdnHistoryId");
            long Rowvalue = Convert.ToInt64(hdnRowId.Value);
            List<SearchHistory> objdetails = objSearchVendor.GetSearchDetails(Rowvalue);

            if ((string.IsNullOrEmpty(hdn.Value)) || (hdn.Value.Trim() == "0"))
            {
                string[] SearchCriteria = Search.Split(',');
                foreach (string strSearch in SearchCriteria)
                {
                    if (strSearch.ToUpper() == "C")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "CSI Codes (ALL)";
                        }
                        else
                        {
                            SearchRoot += ", " + "CSI Codes (All)";
                        }
                    }
                    if (strSearch.ToUpper() == "V")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Vendor Names (All)";
                        }
                        else
                        {
                            SearchRoot += ", " + "Vendor Names (All)";
                        }
                    }
                    if (strSearch.ToUpper() == "R")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Geographic Locations (All)";
                        }
                        else
                        {
                            SearchRoot += ", " + "Geographic Locations (All)";
                        }
                    }
                    if (strSearch.ToUpper() == "P")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Types Of Projects (All)";
                        }
                        else
                        {
                            SearchRoot += ", " + "Types Of Projects (All)";
                        }
                    }
                    if (strSearch.ToUpper() == "F")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Certification (All)";
                        }
                        else
                        {
                            SearchRoot += ", " + "Certification (All)";
                        }
                    }
                    if (strSearch.ToUpper() == "N")
                    {
                        SearchRoot = "No Search Criteria Selected";
                    }
                    if (strSearch.ToUpper() == "A")
                    {
                        SearchRoot = "Vendor Names (All), CSI Codes (All), Geographic Locations (All), Types Of Projects (All), Certificaion (All)";
                    }
                }

                //005
                if (objdetails.Count > 0)
                {
                    if (objdetails[0].ProvidesBIM.Trim() == "Y")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Provides BIM";
                        }
                        else
                        {
                            SearchRoot += ", " + "Provides BIM";
                        }
                    }
                }
                lblSearch.Visible = false;
                lblRoot.Visible = true;
                lblRoot.Text = SearchRoot;
            }
            else
            {
                string[] SearchCriteria = Search.Split(',');
                foreach (string strSearch in SearchCriteria)
                {
                    if (strSearch.ToUpper() == "C")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "CSI Codes";
                        }
                        else
                        {
                            SearchRoot += ", " + "CSI Codes";
                        }
                    }
                    if (strSearch.ToUpper() == "V")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Vendor Name";
                        }
                        else
                        {
                            SearchRoot += ", " + "Vendor Name";
                        }
                    }
                    if (strSearch.ToUpper() == "R")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Geographic Locations";
                        }
                        else
                        {
                            SearchRoot += ", " + "Geographic Locations";
                        }
                    }
                    if (strSearch.ToUpper() == "P")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Types Of Projects";
                        }
                        else
                        {
                            SearchRoot += ", " + "Types Of Projects";
                        }
                    }
                    if (strSearch.ToUpper() == "F")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Certification";
                        }
                        else
                        {
                            SearchRoot += ", " + "Certification";
                        }
                    }
                    if (strSearch.ToUpper() == "N")
                    {
                        SearchRoot = "No Search Criteria Selected";
                    }
                    if (strSearch.ToUpper() == "A")
                    {
                        SearchRoot = "Vendor Name, CSI Codes, Geographic Locations, Types Of Projects, Certification";
                    }
                }

                //005
                if (objdetails.Count > 0)
                {
                    if (objdetails[0].ProvidesBIM.Trim() == "Y")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Provides BIM";
                        }
                        else
                        {
                            SearchRoot += ", " + "Provides BIM";
                        }
                    }
                }

                lblSearch.Visible = true;
                lblRoot.Visible = false;
                lblSearch.Text = SearchRoot;
            }
        }
    }

    /// <summary>
    /// Get confirmation before deleting
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSaveSearch_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdn = new HiddenField();
        hdn = (HiddenField)grdSaveSearch.Rows[e.RowIndex].FindControl("hdnHistoryId");
        Session["HistoryId"] = hdn.Value;
        lblConfirm.Text = "Are you sure you want to delete the record?";
        modalConfirm.Show();

    }

    /// <summary>
    /// Delete the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgOK_Click(object sender, ImageClickEventArgs e)
    {
        objSearchVendor.DeleteSearchHistory(Convert.ToInt64(Session["HistoryId"]));
        modalConfirm.Hide();
        SaveBind(txtEmployeeName.Text);

    }

    /// <summary>
    /// Get the number of records to be displayed in a page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlSearchNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPage.Value = "1";
        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlSearchNumber.SelectedItem.Value))));
        Session["Page"] = Convert.ToString(page);
        lblSearchTotalPage.Text = Convert.ToString(Session["Page"]);

        DisplaySearchPreviousNext();
        
        SaveBind(txtEmployeeName.Text.Trim());
        plcCrntPage1.Controls.Clear();
        CreateSearchPaging(false, "");
        StyleForSearchPaging(hdnPage.Value);
    }

    /// <summary>
    /// Display the next record in the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSearchNext_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) + 1);
        lblSearchTotalPage.Text = Convert.ToString(Session["Page"]);

        DisplaySearchPreviousNext();
        SaveBind(txtEmployeeName.Text.Trim());
        StyleForSearchPaging(hdnPage.Value);
    }

    /// <summary>
    /// Display the previous record in the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSearchPrevious_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) - 1);
        lblSearchTotalPage.Text = Convert.ToString(Session["Page"]);
        DisplaySearchPreviousNext();
        SaveBind(txtEmployeeName.Text);
        StyleForSearchPaging(hdnPage.Value);
    }

    /// <summary>
    /// Bind data in the gridview
    /// </summary>
    /// <param name="EmployeeId"></param>
    private void SaveBind(string EmployeeName)
    {
        SortExp = hdnSaveSort.Value;   
        if (SortExp == null)
        {
            SortExp = "";
        }
        hdnCount.Value = Convert.ToString(objSearchVendor.GetSearchPriorCount(EmployeeName));
        grdSaveSearch.DataSource = objSearchVendor.ViewPriorSearch(EmployeeName, Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlSearchNumber.Text.Trim()), SortExp);
        grdSaveSearch.DataBind();
        lblSearchTotal.Text = hdnCount.Value;
        if (grdSaveSearch.Rows.Count <= 0)
        {
            trGriddetails.Style["Display"] = "none";
            tdNextPrevious.Style["Display"] = "None";
            lbldisplayInfo.Text = "Sorry no records found";
        }
        else
        {
            trGriddetails.Style["Display"] = "";
            tdNextPrevious.Style["Display"] = "";
            lbldisplayInfo.Text = string.Empty;
        }
    }

    /// <summary>
    /// Display search records in the modal pop up
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSaveSearch_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        hdnPage1.Value = "1";
        LinkButton lnk = new LinkButton();
        lnk = (LinkButton)grdSaveSearch.Rows[e.NewSelectedIndex].FindControl("lblname");
        Session["Name"] = lnk.Text.Trim();
        ddlNumber.SelectedIndex = 0;
        HiddenField hdn = new HiddenField();
        hdn = (HiddenField)grdSaveSearch.Rows[e.NewSelectedIndex].FindControl("hdnEmployeeId");
        Session["EmployeeSearchedId"] = hdn.Value;

        Bind();
        if (grdVendor.Rows.Count <= 0)
        {
            lblValidation.Text = "Sorry no record found";
            txtName.Visible = false;
            modalExtnd.Show();
        }
        else
        {
            plcCrntPage.Controls.Clear();
            CreatePaging(false, "");
            StyleForPaging(hdnPage1.Value);
            Label1.Text = string.Empty;
            modalExtndSearch.Show();
            DisplayPreviousNext();
        }
    }
    /// <summary>
    /// Sorting the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSaveSearch_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;
        if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortSaveDirection"])))
        {
            SearchGridViewSortDirection = SortDirection.Ascending;
            ViewState["SortSaveDirection"] = Convert.ToString(SortDirection.Ascending);
            SortSearchGridView(sortExpression, "ASC");
        }
        else if (Convert.ToString(ViewState["SortSaveDirection"]) == Convert.ToString(SortDirection.Ascending))
        {
            SearchGridViewSortDirection = SortDirection.Descending;
            ViewState["SortSaveDirection"] = Convert.ToString(SortDirection.Descending);
            SortSearchGridView(sortExpression, "DESC");
        }
        else
        {
            SearchGridViewSortDirection = SortDirection.Ascending;
            ViewState["SortSaveDirection"] = Convert.ToString(SortDirection.Ascending);
            SortSearchGridView(sortExpression, "ASC");
        }
        CreateSearchPaging(false,"");
    }

    /// <summary>
    /// Method to sort gridview
    /// </summary>
    /// <param name="sortExpression"></param>
    /// <param name="direction"></param>
    private void SortSearchGridView(string sortExpression, string direction)
    {
        SearchSortExp = sortExpression + " " + direction;
        hdnSaveSort.Value = SearchSortExp;
        SaveBind(txtEmployeeName.Text);
    }

    /// <summary>
    /// Get sorting direction
    /// </summary>
    public SortDirection SearchGridViewSortDirection
    {
        get
        {
            if (ViewState["SearchSortDirection"] == null)
            {
                ViewState["SearchSortDirection"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["SearchSortDirection"];
        }
        set
        {
            ViewState["SearchSortDirection"] = value;
        }
    }

    /// <summary>
    /// Get sorting direction
    /// </summary>
    public SortDirection GridViewVendorSortDirection
    {
        get
        {
            if (ViewState["sortVendorDirection"] == null)
            {
                ViewState["sortVendorDirection"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["sortVendorDirection"];
        }
        set
        {
            ViewState["sortVendorDirection"] = value;
        }
    }

    /// <summary>
    /// Method to sort gridview
    /// </summary>
    /// <param name="sortExpression"></param>
    /// <param name="direction"></param>
    private void SortVendorGridView(string sortExpression, string direction)
    {
        SortVendorExp = sortExpression + " " + direction;
        ViewState["SortVendor"] = SortVendorExp;
        Bind();
    }

    /// <summary>
    /// Display Previous record in tha page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrevious_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage1.Value = Convert.ToString(Convert.ToInt32(hdnPage1.Value) - 1);
        lblTotalPage.Text = Convert.ToString(Session["Page1"]);

        DisplayPreviousNext();
        Bind();
        if (grdVendor.Rows.Count <= 0)
        {
            lblValidation.Text = "Sorry no record found";
            modalExtnd.Show();
        }
        else
        {
            Label1.Text = string.Empty;
            modalExtndSearch.Show();
        }
        StyleForPaging(hdnPage1.Value);
    }

    /// <summary>
    /// raise event to dynamically created controls
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void lnk_Click(object sender, EventArgs e)
    {
        LinkButton Currentlnk = new LinkButton();
        Currentlnk = (LinkButton)sender;
        hdnPage1.Value = Currentlnk.Text.Trim();

        StyleForPaging(Currentlnk.Text.Trim());
        DisplayPreviousNext();
        Bind();

        modalExtndSearch.Show();
    }

    /// <summary>
    /// Raise event to dynamically created controls
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void lnk1_Click(object sender, EventArgs e)
    {
        LinkButton Currentlnk = new LinkButton();
        Currentlnk = (LinkButton)sender;
        hdnPage.Value = Currentlnk.Text.Trim();

        StyleForSearchPaging(Currentlnk.Text.Trim());
        DisplaySearchPreviousNext();
        SaveBind(txtEmployeeName.Text.Trim());
    }

    /// <summary>
    /// Display next record in the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbNext_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage1.Value = Convert.ToString(Convert.ToInt32(hdnPage1.Value) + 1);
        lblTotalPage.Text = Convert.ToString(Session["Page1"]);

        DisplayPreviousNext();
        Bind();
        if (grdVendor.Rows.Count <= 0)
        {
            lblValidation.Text = "Sorry no record found";
            modalExtnd.Show();
        }
        else
        {
            Label1.Text = string.Empty;
            modalExtndSearch.Show();
        }
        StyleForPaging(hdnPage1.Value);
    }

    /// <summary>
    /// Get number of records in the page and bind the data accordingly
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPage1.Value = "1";
        int page1 = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount1.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Value))));
        Session["Page1"] = Convert.ToString(page1);
        lblTotalPage.Text = Convert.ToString(Session["Page1"]);
        DisplayPreviousNext();

        Bind();
        if (grdVendor.Rows.Count <= 0)
        {
            lblValidation.Text = "Sorry no record found";
            modalExtnd.Show();
        }
        else
        {
            Label1.Text = string.Empty;
            Bind();
            plcCrntPage.Controls.Clear();
            CreatePaging(false, "");
            StyleForPaging(hdnPage1.Value);
            modalExtndSearch.Show();
        }
    }

    /// <summary>
    /// Bind record in the grid
    /// </summary>
    private void Bind()
    {
        //002 - DAL.UspGetSearchByNameResult[] objResult = objSearchVendor.GetSearchByName(Convert.ToString(Session["Name"]), Convert.ToInt32(Session["EmployeeSearchedId"]));
        List<SearchHistory> objResult = objSearchVendor.GetSearchByName(Convert.ToString(Session["Name"]), Convert.ToInt32(Session["EmployeeSearchedId"]));

        //002 - if (objResult.Length > 0)
        if (objResult.Count > 0)
        {

            string SearchBy = "", Vendor = "", VendorNumber = "", TrackingNumber = "",VendorStatus="", FederalTaxId = "", 
                   Scopes = "", Regions = "",Continents="",Countries="", Projects = "", OtherState = "",Certification="",FederalSB="",FirstName = "",LastName = "", Email = "";    //002


            if (string.IsNullOrEmpty(Convert.ToString(hdnSort.Value)))
            {
                SortVendorExp = "";
            }
            else
            {
                SortVendorExp = hdnSort.Value;
            }
            //002
            if (!string.IsNullOrEmpty(objResult[0].FirstName))
            {
                flag++;
                FirstName = objResult[0].FirstName;
            }
            //002
            if (!string.IsNullOrEmpty(objResult[0].LastName))
            {
                flag++;
                LastName = objResult[0].LastName;
            }
            //002
            if (!string.IsNullOrEmpty(objResult[0].VendorEmail))
            {
                flag++;
                Email = objResult[0].VendorEmail;
            }
            if (!string.IsNullOrEmpty(Convert.ToString(objResult[0].VendorId)))
            {
                flag++;
                Vendor = Convert.ToString(objResult[0].VendorId);
            }
            if (!string.IsNullOrEmpty(objResult[0].JDEVendorNo))
            {
                flag++;
                VendorNumber = Convert.ToString(objResult[0].JDEVendorNo);
            }
            if (!string.IsNullOrEmpty(objResult[0].TrackingNo))
            {
                flag++;
                TrackingNumber = Convert.ToString(objResult[0].TrackingNo);
            }
            if (!string.IsNullOrEmpty(objResult[0].TaxId))
            {
                flag++;
                FederalTaxId = Convert.ToString(objResult[0].TaxId);
            }
            string Status = "";
            if (!string.IsNullOrEmpty(objResult[0].Status))
            {
                flag++;
                Status = Convert.ToString(objResult[0].Status);
            }

            if (Status == "--Select--")
            {
                Status = "";
            }
            else if (Status == "InProgress")
            {

                Status = "0";
            }
            else if (Status == "Pending")
            {

                Status = "1";
            }
            else if (Status == "Complete")
            {

                Status = "2";
            }
            else if (Status == "Revision")
            {

                Status = "3";
            }
            // G. Vera 4/20/2012 - Phase 3 Enhancement allowing Admin to search expired vendors.
            else if (Status == "Expired")
            {
                Status = "6";
            }
            else
            {
                Status = "0,1,2,3";
            } 
            if (flag > 0)
            {
                SearchBy = "V";
                flag = 1;
            }
            if (!string.IsNullOrEmpty(objResult[0].CSICodes))
            {
                Scopes = Convert.ToString(objResult[0].CSICodes);
                if (!string.IsNullOrEmpty(Scopes))
                {
                    SearchBy += "C";
                    flag++;
                }
                else
                {
                    flag = 0;
                }
            }
            if (!string.IsNullOrEmpty(objResult[0].RegionCodes))
            {
                Regions = Convert.ToString(objResult[0].RegionCodes);
                if (!string.IsNullOrEmpty(Regions))
                {
                    SearchBy += "R";
                    flag++;
                }
                else
                {
                    flag = 0;
                }

            }
            if (!string.IsNullOrEmpty(objResult[0].ContinentCodes)) //006
            {
                Continents = Convert.ToString(objResult[0].ContinentCodes);
                if (!string.IsNullOrEmpty(Continents))
                {
                    SearchBy += "R";
                    flag++;
                }
                else
                {
                    flag = 0;
                }
            }
            if (!string.IsNullOrEmpty(objResult[0].CountryCodes)) //006
            {
                Countries = Convert.ToString(objResult[0].CountryCodes);
                if (!string.IsNullOrEmpty(Countries))
                {
                    SearchBy += "R";
                    flag++;
                }
                else
                {
                    flag = 0;
                }
            }
            if (!string.IsNullOrEmpty(objResult[0].ProjectTypes))
            {
                Projects = Convert.ToString(objResult[0].ProjectTypes);
                if (!string.IsNullOrEmpty(Projects))
                {
                    SearchBy += "P";
                    flag++;
                }
                else
                {
                    flag = 0;
                }
            }
            if ((!string.IsNullOrEmpty(objResult[0].CompanyCertification)) || (!string.IsNullOrEmpty(objResult[0].FederalSB)))
            {
                Certification = Convert.ToString(objResult[0].CompanyCertification);
                FederalSB = Convert.ToString(objResult[0].FederalSB);
                if ((!string.IsNullOrEmpty(Certification)) || (!string.IsNullOrEmpty(FederalSB)))
                {
                    SearchBy += "F";
                    flag++;
                }
                else
                {
                    flag = 0;
                }
            }
            int OtherStateId = 0;
            if (!string.IsNullOrEmpty(objResult[0].OtherState))
            {
                OtherStateId = 51;
                OtherState = Convert.ToString(objResult[0].OtherState);
            }

            if (flag == 5)
            {
                SearchBy = "A";
            }
            if (flag == 0)
            {
                SearchBy = "N";
            }
            //002 - changed
            //004
            //005
            bool bim = (objResult[0].ProvidesBIM.Trim() == "Y") ? true : false;
            hdnCount1.Value = Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects, "", OtherState, OtherStateId, Certification, FederalSB, FirstName, LastName, Email, (Session["OtherProject"] as String), objResult[0].ISO, objResult[0].QS, "", bim,Continents,Countries )); //006
            // G. Vera 4/20/2012 - Phase 3 Enhancement allowing Admin to search expired vendors.
            //002
            //if (Convert.ToString(Session["Access"]).Equals("1"))
            //    hdnCount1.Value = Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects, "", OtherState, OtherStateId, Certification, FederalSB));
            lblTotal.Text = hdnCount1.Value;
            int page1 = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount1.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Value))));
            Session["Page1"] = Convert.ToString(page1);
            lblTotalPage.Text = Convert.ToString(Session["Page1"]);
            DisplayPreviousNext();

            //004
            //005
            grdVendor.DataSource = objSearchVendor.SearchVendor(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects, 
                                                                Convert.ToInt32(hdnPage1.Value), Convert.ToInt32(ddlNumber.Text.Trim()), SortVendorExp, OtherState,
                                                                OtherStateId, Certification, FederalSB, FirstName, LastName, Email, (Session["OtherProject"] as String), objResult[0].ISO, objResult[0].QS, "", bim, Continents,Countries); //006
            grdVendor.DataBind();
        }
        else
        {
            grdVendor.DataSource = null;
            grdVendor.DataBind();
        }
    }

    /// <summary>
    /// To filter the record and display in the gridview
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgSearchButton_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = "1";
        SaveBind(txtEmployeeName.Text.Trim());
        hdnPage.Value = "1";
        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlSearchNumber.SelectedItem.Value))));
        Session["Page"] = Convert.ToString(page);
        lblSearchTotalPage.Text = Convert.ToString(Session["Page"]);

        DisplaySearchPreviousNext();
        plcCrntPage1.Controls.Clear();

        CreateSearchPaging(false, "");
        StyleForSearchPaging(hdnPage.Value);
    }

    /// <summary>
    /// Sorting the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;
        if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortDirection"])))
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
        else if (Convert.ToString(ViewState["SortDirection"]) == Convert.ToString(SortDirection.Ascending))
        {
            GridViewSortDirection = SortDirection.Descending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Descending);
            SortGridView(sortExpression, "DESC");
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
        modalExtndSearch.Show();
    }

    /// <summary>
    /// Get sorting direction
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
            {
                ViewState["sortDirection"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["sortDirection"];
        }
        set
        {
            ViewState["sortDirection"] = value;
        }
    }

    /// <summary>
    /// Method to sort gridview
    /// </summary>
    /// <param name="sortExpression"></param>
    /// <param name="direction"></param>
    private void SortGridView(string sortExpression, string direction)
    {
        SortExp = sortExpression + " " + direction;
        hdnSort.Value = SortExp;
        Bind();

    }

    /// <summary>
    /// To change the text
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnVendorId = new HiddenField();
            hdnVendorId = (HiddenField)e.Row.FindControl("hdnVendorId");
            AjaxControlToolkit.Rating rtg = new AjaxControlToolkit.Rating();
            rtg = (AjaxControlToolkit.Rating)e.Row.FindControl("rtgStar");

            rtg.CurrentRating = Convert.ToInt32(Math.Round(objSearchVendor.GetAverageRating(Convert.ToInt64(hdnVendorId.Value))));
            Label lnkSRF = new Label();
            lnkSRF = (Label)e.Row.FindControl("lnkSRF");

            Label lnk = new Label();
            lnk = (Label)e.Row.FindControl("lnkVQF");
            Label lblStatus = new Label();
            lblStatus = (Label)e.Row.FindControl("lblStatusDesc");
            if (objCommon.GetDocumentCount(Convert.ToInt64(hdnVendorId.Value)) > 0)
            {
                ((ImageButton)e.Row.FindControl("imgPaperClip")).ImageUrl = "~/Images/paperclip.png";
            }
            else
            {
                ((ImageButton)e.Row.FindControl("imgPaperClip")).ImageUrl = "~/Images/paperclip_gray.png";
            }
            if (lblStatus.Text == "Complete")
            {
                if (objSearchVendor.SubContratorRatingCount(Convert.ToInt64(hdnVendorId.Value)) > 0)
                {
                    lnkSRF.Visible = true;
                    rtg.Visible = true;
                }
                else
                {
                    lnkSRF.Visible = false;
                    rtg.Visible = false;
                }

                lblStatus.CssClass = "greenbold";
            }
            else if (lblStatus.Text == "Revision")
            {
                if (objSearchVendor.SubContratorRatingCount(Convert.ToInt64(hdnVendorId.Value)) > 0)
                {
                    lnkSRF.Visible = true;
                    rtg.Visible = true;
                }
                else
                {
                    lnkSRF.Visible = false;
                    rtg.Visible = false;
                }
               lblStatus.CssClass = "redbold";
            }
            else if (lblStatus.Text == "PreComplete")
            {
                lnkSRF.Visible = false;
                rtg.Visible = false;
                lblStatus.CssClass = "redbold";
                lblStatus.Text = "Pending";
            }
            else if (lblStatus.Text == "JDE")
            {
                lnkSRF.Visible = false;
                rtg.Visible = false;
                lblStatus.Text = string.Empty;
                Label lbltrack = new Label();
                lbltrack = (Label)e.Row.FindControl("lblTrackingNumber");
                lbltrack.Text = string.Empty;
            }
            else
            {
                lnkSRF.Visible = false;
                rtg.Visible = false;
                lblStatus.CssClass = "redbold";
            }

            Label lblVendorName = new Label();
            lblVendorName = (Label)e.Row.FindControl("lblVendorName");
            Label lbljde = new Label();
            lbljde = (Label)e.Row.FindControl("JDER");
            lbljde.Text = objSearchVendor.CheckJDENumber(lblVendorName.Text.Trim());
            if (lbljde.Text.Trim() == "Y")
            {
                lbljde.CssClass = "greenbold";
            }
            else if (lbljde.Text.Trim() == "N")
            {
                lbljde.CssClass = "redbold";
            }
            DataTable dt = objSearchVendor.GetCommentsAndTag(Convert.ToInt64(hdnVendorId.Value));
            Image img = new Image();
            img = (Image)e.Row.FindControl("imgTag");
            Image imgVPP = new Image();
            imgVPP = (Image)e.Row.FindControl("imgVPP");
            if (dt.Rows.Count > 0)
            {
                if ((string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["VPPFlag"]))) || (Convert.ToString(dt.Rows[0]["VPPFlag"])) == "False")
                {
                    imgVPP.Visible = false;
                }
                else { imgVPP.Visible = true; }
                if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["Tag"])))
                {
                    img.Visible = false;
                }
                else if (Convert.ToString(dt.Rows[0]["Tag"]) == "0")
                {
                    img.Visible = true;
                    img.ImageUrl = @"../Images/red.png";
                    HtmlGenericControl panel = (HtmlGenericControl)e.Row.FindControl("gridPopup1");
                    string showPopup = "ShowPopup('" + panel.ClientID + "')";
                    string hidePopup = "HidePopup('" + panel.ClientID + "')";
                    Label lblComments = new Label();
                    lblComments = (Label)e.Row.FindControl("lbtngrdPrint1");
                    lblComments.Text = Convert.ToString(dt.Rows[0]["AddNotes"]);
                    e.Row.Cells[0].Attributes.Add("onmouseover", "javascript:" + showPopup);
                    e.Row.Cells[0].Attributes.Add("onmouseout", "javascript:" + hidePopup);
                }
                else if (Convert.ToString(dt.Rows[0]["Tag"]) == "1")
                {
                    img.Visible = true;
                    img.ImageUrl = @"../Images/yellow.png";
                    HtmlGenericControl panel = (HtmlGenericControl)e.Row.FindControl("gridPopup1");
                    string showPopup = "ShowPopup('" + panel.ClientID + "')";
                    string hidePopup = "HidePopup('" + panel.ClientID + "')";
                    Label lblComments = new Label();
                    lblComments = (Label)e.Row.FindControl("lbtngrdPrint1");
                    lblComments.Text = Convert.ToString(dt.Rows[0]["AddNotes"]);
                    e.Row.Cells[0].Attributes.Add("onmouseover", "javascript:" + showPopup);
                    e.Row.Cells[0].Attributes.Add("onmouseout", "javascript:" + hidePopup);
                }
                else if (Convert.ToString(dt.Rows[0]["Tag"]) == "2")
                {
                    img.Visible = false;
                }
                else
                {
                    img.Visible = false;
                }
            }
            else
            {
                img.Visible = false;
                imgVPP.Visible = false;
            }
        }
    }


    /// <summary>
    /// 008 Raise event to display countries based on the selected text
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlContinents_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DataTable objRegions;
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)    //005 add to check for both..hello?
        {
            //005 - modified the way they were hardcoding the check instead of just looking for the parent.
            Label lbl = (Label)e.Item.FindControl("lblContinent");
            Label ltr = (Label)e.Item.FindControl("lstCountries");

            if (!string.IsNullOrEmpty(lbl.Text))
            {
                objRegions = objSearchVendor.GetSearchedContinents(Convert.ToInt64(ViewState["RowValue"]), true,lbl.Text);
                foreach (DataRow drow in objRegions.Rows)
                {
                    ltr.Text += ", " + drow[0];
                }
                if (!string.IsNullOrEmpty(ltr.Text.Trim()))
                {
                    ltr.Text = ltr.Text.TrimStart(',');
                }
            }

        }
    }


    /// <summary>
    /// Display regions
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlRegions_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DAL.USPGetSearchedRegionsResult[] objRegions;
        if (e.Item.ItemType == ListItemType.Item)
        {
            Label lbl = (Label)e.Item.FindControl("lblName");
            if (lbl.Text.Trim() == "CA")
            {
                Label ltr = (Label)e.Item.FindControl("lstRegions");
                objRegions = objSearchVendor.GetSearchRegions(Convert.ToInt64(ViewState["RowValue"])).Where(m => m.ParentID == 5).ToArray();
                for (int i = 0; i < objRegions.Length; i++)
                {
                    ltr.Text += ", " + objRegions[i].StateCode;
                }
                if (!string.IsNullOrEmpty(ltr.Text))
                {
                    ltr.Text = ltr.Text.Remove(0, 1);
                }
            }
            else if (lbl.Text == "FL")
            {
                Label ltr = (Label)e.Item.FindControl("lstRegions");

                objRegions = objSearchVendor.GetSearchRegions(Convert.ToInt64(ViewState["RowValue"])).Where(m => m.ParentID == 9).ToArray();
                for (int i = 0; i < objRegions.Length; i++)
                {
                    ltr.Text += ", " + objRegions[i].StateCode;
                }
                if (!string.IsNullOrEmpty(ltr.Text))
                {
                    ltr.Text = ltr.Text.Remove(0, 1);
                }
            }
            else if (lbl.Text == "TX")
            {
                Label ltr = (Label)e.Item.FindControl("lstRegions");

                objRegions = objSearchVendor.GetSearchRegions(Convert.ToInt64(ViewState["RowValue"])).Where(m => m.ParentID == 43).ToArray();
                for (int i = 0; i < objRegions.Length; i++)
                {
                    ltr.Text += ", " + objRegions[i].StateCode;
                }
                if (!string.IsNullOrEmpty(ltr.Text))
                {
                    ltr.Text = ltr.Text.Remove(0, 1);
                }
            }
            else if (lbl.Text == "Other")
            {
                Label ltr = (Label)e.Item.FindControl("lstRegions");
                ltr.Text = Convert.ToString(ViewState["OtherState"]);
            }
        }
    }

    /// <summary>
    /// Export to Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbexcel_Click(object sender, ImageClickEventArgs e)
    {
        string d_FilePath = Server.MapPath("SearchExport") + "\\test.xls";

        if (File.Exists(d_FilePath))
            File.Delete(d_FilePath);
        Microsoft.Office.Interop.Excel.Application xlsapp = null;
        Microsoft.Office.Interop.Excel._Workbook xlsbook = null;
        Microsoft.Office.Interop.Excel._Worksheet xlssheet = null;

        xlsapp = new Microsoft.Office.Interop.Excel.Application();
        xlsapp.Visible = false;

        xlsbook = (Microsoft.Office.Interop.Excel._Workbook)(xlsapp.Workbooks.Add(System.Reflection.Missing.Value));
        xlssheet = (Microsoft.Office.Interop.Excel._Worksheet)xlsbook.ActiveSheet;

        int i = 0;

        i++;

        xlssheet.Cells[i, 1] = grdSaveSearch.Columns[0].HeaderText.ToString();
        xlssheet.Cells[i, 2] = grdSaveSearch.Columns[1].HeaderText.ToString();
        xlssheet.Cells[i, 3] = grdSaveSearch.Columns[2].HeaderText.ToString();
        xlssheet.Cells[i, 4] = grdSaveSearch.Columns[3].HeaderText.ToString();

        foreach (GridViewRow row in grdSaveSearch.Rows)
        {
            i++;

            xlssheet.Cells[i, 1] = ((Label)row.FindControl("lblemployee")).Text.ToString();
            xlssheet.Cells[i, 2] = ((LinkButton)row.FindControl("lblname")).Text.ToString();
            if (((Label)row.FindControl("lblRoot")).Text.ToString() == string.Empty) {
                xlssheet.Cells[i, 3] = ((LinkButton)row.FindControl("lblsearch")).Text.ToString();
            }
            else {
                xlssheet.Cells[i, 3] = ((Label)row.FindControl("lblRoot")).Text.ToString();
            }
            
            xlssheet.Cells[i, 4] = ((Label)row.FindControl("lbldate")).Text.ToString();
        }

        //neaten the workbook up
        xlssheet.Columns.AutoFit();
        Microsoft.Office.Interop.Excel.Range xlRng = xlssheet.UsedRange.get_Range("A1", Type.Missing).EntireRow;
        xlRng.Font.Bold = true;

        xlsbook.SaveAs(d_FilePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, null, null, null, null, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlShared, null, null, null, null, null);
        //xlsbook.SaveCopyAs(d_FilePath);

        xlssheet = null;
        NAR(xlssheet);
        //xlsbook.Close(null, d_FilePath, null);
        NAR(xlsbook);
        xlsbook = null;
        xlsapp.Quit();
        NAR(xlsapp);
        xlsapp = null;

        //foreach (Process clsprocess in Process.GetProcesses())
        //{
        //    string str = clsprocess.ProcessName;
        //    if (clsprocess.ProcessName.StartsWith("EXCEL"))
        //    {
        //        clsprocess.Kill();
        //    }
        //}
        string PDFPath = Convert.ToString(d_FilePath);
        FileStream liveStream = new FileStream(PDFPath, FileMode.Open,
            FileAccess.Read);

        byte[] buffer = new byte[(int)liveStream.Length];
        liveStream.Read(buffer, 0, (int)liveStream.Length);
        liveStream.Close();

        Response.Clear();
        Response.ContentType = "application/octet-stream";
        Response.AddHeader("Content-Length", buffer.Length.ToString());
        Response.AddHeader("Content-Disposition", "attachment; filename=VendorSearch" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
        Response.BinaryWrite(buffer);
        Response.End();
    }

    /// <summary>
    /// Inbuild method
    /// </summary>
    /// <param name="o"></param>
    private void NAR(object o)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(o);
        }
        catch
        { }
        finally
        {
            o = null;
        }
    }
}
