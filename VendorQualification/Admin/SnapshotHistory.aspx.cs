﻿using System;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Data;
using System.Web; 
using System.IO;
using System.Data.SqlClient;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 
 *
 ****************************************************************************************************************/
#endregion

public partial class SnapshotHistory : System.Web.UI.Page
{

    #region Instantiate the class and declare varaibles

    Common objCommon = new Common();
    string VQFPath = string.Empty;
    string SREPath = string.Empty;
    string strPageRequest;
    public string SortExp;
    private const string TAG_ERROR_MSG = "<li>{}</li>";

    #endregion

    #region Page Events

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (hdnFlag.Value != "1")
        {
        strPageRequest = Request.Form["__EVENTTARGET"];
        }
        CreatePaging(true, strPageRequest);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
        {
            //001
            if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");
            }
            Response.Redirect("~/Admin/Login.aspx");
        }
        else
        {
            VQFPath = ConfigurationManager.AppSettings["VQFSnapshot"].ToString();
            SREPath = ConfigurationManager.AppSettings["SRESnapshot"].ToString();

            if (!Page.IsPostBack)
            {
                //001
                Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");


                if (Convert.ToString(Session["Access"]) == "2")
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                {
                    AdminMenu.DisplayMenu("Admin");
                }
                tdnextPrevious1.Style["display"] = "none";
                Search.Style["display"] = "none";
                Session["Page11"] = null;
                hdnFlag.Value = "0";
                hdnPage.Value = null;

                //imbSearchVQF.Attributes.Add("onClick", "AssignValue('" + hdnFlag.ClientID + "');");
                CreatePaging(true, strPageRequest);
                if (hdnPage.Value != string.Empty)
                {
                    BindHistory();
                    strPageRequest = Request.Form["__EVENTTARGET"];
                    StyleforPaging(hdnPage.Value, plcCrntPage1);
                    DisplayPreviousNext();
                }
                hdnFlag.Value = "0";
            }
            else
            {
                CreatePaging(true, strPageRequest);
                if (hdnPage.Value != string.Empty)
                {
                    BindHistory();
                    StyleforPaging(hdnPage.Value, plcCrntPage1);
                    DisplayPreviousNext();
                }
              
            }

        //strPageRequest = Request.Form["__EVENTTARGET"];

        //if (hdnFlag.Value != "1")
        //{

            //CreatePaging(true, strPageRequest);

        //StyleforPaging(hdnPage.Value, plcCrntPage1);
        //}
        // hdnFlag.Value = "0";

        }
      
    }

    #endregion

    #region Button Events

    /// <summary>
    /// Search the VQF snapshop history Event 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSearchVQF_Click(object sender, ImageClickEventArgs e)
    {
        lblMessage.Text = ""; 
        Page.Validate();
       
        if (Page.IsValid)
        {
            hdnPage.Value = "1";
            if (validate().ToString() == string.Empty)
            {
               
                BindHistory();
            }
            else
            {
                mpopAlert.Show();
            }
        }
    }

    /// <summary>
   /// hide the error listing modal popup Method
   /// </summary>
   /// <param name="sender"></param>
   /// <param name="e"></param>
    protected void imbOK_Click(object sender, ImageClickEventArgs e)
    {
        mpopAlert.Hide();
    }

    /// <summary>
    /// Display the previous record in the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrevious_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) - 1);
        foreach (Control ctrl in plcCrntPage1.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";

                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }

        if (Convert.ToInt32(Session["Page11"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if (hdnPage.Value == "1")
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        BindHistory();
        foreach (Control ctrl in plcCrntPage1.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnPage.Value.ToString())
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }
    }

    /// <summary>
    /// Display the next record of the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbNext_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) + 1);
        foreach (Control ctrl in plcCrntPage1.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }

        if (Convert.ToInt32(Session["Page11"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if (hdnPage.Value == "1")
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        BindHistory();
        foreach (Control ctrl in plcCrntPage1.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnPage.Value.ToString())
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }
    }
    #endregion

    #region GridView and DropDownList Events

        
    protected void grvVQFHistory_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "VQFSnapshot")
        {

            ShowDocument(e.CommandArgument.ToString() + ".pdf");
        }
       

    }

    protected void grvVQFHistory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            LinkButton lbDownload = e.Row.FindControl("imbVQFSnapshot") as LinkButton;
            ScriptManager current = ScriptManager.GetCurrent(Page);
            if (current != null)
            {
                current.RegisterPostBackControl(lbDownload);
            }
       
       
        }
    }
    protected void grvVQFHistory_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;
        if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortDirection"])))
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView1(sortExpression, "ASC");
        }
        else if (Convert.ToString(ViewState["SortDirection"]) == Convert.ToString(SortDirection.Ascending))
        {
            GridViewSortDirection = SortDirection.Descending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Descending);
            SortGridView1(sortExpression, "DESC");
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView1(sortExpression, "ASC");
        }
    }
  
    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPage.Value = "1";
        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));
        Session["Page11"] = Convert.ToString(page);
        lblTotalPage.Text = Convert.ToString(Session["Page11"]);
        if (Convert.ToInt32(Session["Page11"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if (hdnPage.Value == "1")
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }

        BindHistory();
        plcCrntPage1.Controls.Clear();
        for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
        {
            if (i > 0 && i <= Convert.ToInt32(Session["Page11"]))
            {
                LinkButton lnk = new LinkButton();
                lnk.Click += new EventHandler(lnk1_Click);
                lnk.Text = Convert.ToString(i);
                if (i == Convert.ToInt32(hdnPage.Value))
                {
                    lnk.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk.CssClass = "lnkcolor";
                }
                plcCrntPage1.Controls.Add(lnk);
                lnk.Text = Convert.ToString(i);
                lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnPage.ClientID + "')");
            }
        }
        foreach (Control ctrl in plcCrntPage1.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// Open the document Method
    /// </summary>
    /// <param name="fileName"></param>
    private void ShowDocument(string fileName)
    {
        string strFileName = VQFPath + fileName;
        FileInfo file = new FileInfo(strFileName);

        // Checking if file exists
        if (file.Exists)
        {
            // Clear the content of the response
            Response.ClearContent();
            // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            // Add the file size into the response header
            Response.AddHeader("Content-Length", file.Length.ToString());
            // Set the ContentType
            Response.ContentType = Common.GetDocumentContentType(file.Extension.ToLower());
            Response.TransmitFile(file.FullName);
            // End the response
            Response.Flush();
        }
    }


   
    /// <summary>
    /// Display the page of the selected link
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void lnk1_Click(object sender, EventArgs e)
    {
        LinkButton Currentlnk = new LinkButton();
        Currentlnk = (LinkButton)sender;
       // hdnPage.Value = Currentlnk.Text.Trim();
        if (Convert.ToInt32(Session["Page11"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if (hdnPage.Value == "1")
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
    }

    /// <summary>
    /// Create paging for SRE search 
    /// </summary>
    /// <param name="FromLoad"></param>
    /// <param name="strPageRequest"></param>
    private void CreatePaging(bool FromLoad, string strPageRequest)
    {
        if (FromLoad)
        {
            if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page11"]))) && ((string.IsNullOrEmpty(strPageRequest)) || (strPageRequest.ToUpperInvariant() != "DDLNUMBER")) && hdnFlag.Value != "1")
            {

                for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
                {
                    if (i > 0 && i <= Convert.ToInt32(Session["Page11"]))
                    {
                        LinkButton lnk = new LinkButton();
                        lnk.Click += new EventHandler(lnk1_Click);
                        lnk.Text = Convert.ToString(i);
                        if (i == Convert.ToInt32(hdnPage.Value))
                        {
                            lnk.CssClass = "Selectlnkcolor";
                        }
                        else
                        {
                            lnk.CssClass = "lnkcolor";
                        }
                        plcCrntPage1.Controls.Add(lnk);
                        lnk.Text = Convert.ToString(i);
                        lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnPage.ClientID + "')");


                    }
                }
            }
        }
        else
        {
            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page11"]))
                {
                    LinkButton lnk = new LinkButton();
                    lnk.Click += new EventHandler(lnk1_Click);
                    lnk.Text = Convert.ToString(i);
                    if (i == Convert.ToInt32(hdnPage.Value))
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnk.CssClass = "lnkcolor";
                    }
                    plcCrntPage1.Controls.Add(lnk);
                    lnk.Text = Convert.ToString(i);
                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnPage.ClientID + "')");


                }
            }
        }
    }

    /// <summary>
    /// validate page and list the errors
    /// </summary>
    /// <returns></returns>
    private string validate()
    {
        lblMessage.Text = "";
        
        if (txtFDate.Text.Trim() != string.Empty)
        {
            DateTime dt;
            if (!DateTime.TryParse(txtFDate.Text.Trim(), out dt))
                lblMessage.Text += TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ValidFromdate").ToString());
            trSRE.Style["display"] = "none";
            Search.Style["display"] = "none";
            tdnextPrevious1.Style["display"] = "none";

        }
        if (txtTDate.Text.Trim() != string.Empty)
        {
            DateTime dt;
            if (!DateTime.TryParse(txtTDate.Text.Trim(), out dt))
                lblMessage.Text += TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ValidToDate").ToString());
            trSRE.Style["display"] = "none";
            Search.Style["display"] = "none";
            tdnextPrevious1.Style["display"] = "none";
        }
        if (lblMessage.Text.Trim() == string.Empty && txtFDate.Text.Trim() != string.Empty && txtTDate.Text.Trim() != string.Empty)
        {
            if (txtFDate.Text.Trim() != txtTDate.Text.Trim())
            {
                int a = Convert.ToDateTime(txtTDate.Text.Trim()).Subtract(Convert.ToDateTime(txtFDate.Text.Trim())).Days;
                //int a = Convert.ToDateTime(txtTDate.Text.Trim()).Equals(Convert.ToDateTime(txtFDate.Text.Trim().ToString() );
                if (a < 0)

                    lblMessage.Text += TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "DateCheck").ToString());
                trSRE.Style["display"] = "none";
                Search.Style["display"] = "none";
                tdnextPrevious1.Style["display"] = "none";
                    
            }
        }
        return lblMessage.Text;
    }

    /// <summary>
    /// Bind the Snapshot history method
    /// </summary>
    private void BindHistory()
    {
        lblMessage.Text = "";
        Page.Validate();
        if (Page.IsValid)
        {

            SortExp = hdnSort.Value;
            if (string.IsNullOrEmpty(SortExp))
            {
                SortExp = "";
            }
            DataTable dsHistory;
            DataTable dtn;
            int StartRec = (Convert.ToInt32(hdnPage.Value) * Convert.ToInt32(ddlNumber.SelectedValue)) - Convert.ToInt32(ddlNumber.SelectedValue);
            int LastRec = StartRec + Convert.ToInt32(ddlNumber.SelectedValue);
            int i = 0;
            if (txtVendor.Text.Trim() == "" && txtFDate.Text.Trim() == "" && txtTDate.Text.Trim() == "")
            {
                dsHistory = objCommon.GetViewHistory("", Convert.ToDateTime("01/01/1753"), Convert.ToDateTime("01/01/1753"), SortExp).Tables[0];
                hdnCount.Value = dsHistory.Rows.Count.ToString();
                dtn = dsHistory.Clone();
                for (i = StartRec; i < LastRec && i < dsHistory.Rows.Count; i++)
                {
                    dtn.ImportRow(dsHistory.Rows[i]);
                }
                grvVQFHistory.DataSource = dtn;
                grvVQFHistory.DataBind();

            }
            else if (txtVendor.Text.Trim() != "" && txtFDate.Text.Trim() == "" && txtTDate.Text.Trim() == "")
            {


                dsHistory = objCommon.GetViewHistory(txtVendor.Text.Trim(), Convert.ToDateTime("01/01/1753"), Convert.ToDateTime("01/01/1753"), SortExp).Tables[0];
                hdnCount.Value = dsHistory.Rows.Count.ToString();
                dtn = dsHistory.Clone();
                for (i = StartRec; i < LastRec && i < dsHistory.Rows.Count; i++)
                {
                    dtn.ImportRow(dsHistory.Rows[i]);
                }
                grvVQFHistory.DataSource = dtn;
                grvVQFHistory.DataBind();

            }
            else if (txtVendor.Text.Trim() != "" && txtFDate.Text.Trim() != "" && txtTDate.Text.Trim() == "")
            {

                dsHistory = objCommon.GetViewHistory(txtVendor.Text.Trim(), Convert.ToDateTime(txtFDate.Text.Trim()), DateTime.Now.AddDays(1), SortExp).Tables[0];
                hdnCount.Value = dsHistory.Rows.Count.ToString();
                dtn = dsHistory.Clone();
                for (i = StartRec; i < LastRec && i < dsHistory.Rows.Count; i++)
                {
                    dtn.ImportRow(dsHistory.Rows[i]);
                }
                grvVQFHistory.DataSource = dtn;
                grvVQFHistory.DataBind();

            }
            else if (txtVendor.Text.Trim() != "" && txtFDate.Text.Trim() != "" && txtTDate.Text.Trim() != "")
            {
                grvVQFHistory.DataSource = objCommon.GetViewHistory(txtVendor.Text.Trim(), Convert.ToDateTime(txtFDate.Text.Trim()), Convert.ToDateTime(txtTDate.Text.Trim()), SortExp).Tables[0];
                grvVQFHistory.DataBind();

            }
            else if (txtVendor.Text.Trim() == "" && txtFDate.Text.Trim() != "" && txtTDate.Text.Trim() == "")
            {
                dsHistory = objCommon.GetViewHistory("", Convert.ToDateTime(txtFDate.Text.Trim()), DateTime.Now.AddDays(1), SortExp).Tables[0];
                hdnCount.Value = dsHistory.Rows.Count.ToString();
                dtn = dsHistory.Clone();
                for (i = StartRec; i < LastRec && i < dsHistory.Rows.Count; i++)
                {
                    dtn.ImportRow(dsHistory.Rows[i]);
                }
                grvVQFHistory.DataSource = dtn;
                grvVQFHistory.DataBind();

            }
            else if (txtVendor.Text.Trim() == "" && txtFDate.Text.Trim() != "" && txtTDate.Text.Trim() != "")
            {
                dsHistory = objCommon.GetViewHistory("", Convert.ToDateTime(txtFDate.Text.Trim()), Convert.ToDateTime(txtTDate.Text.Trim()), SortExp).Tables[0];
                hdnCount.Value = dsHistory.Rows.Count.ToString();
                dtn = dsHistory.Clone();
                for (i = StartRec; i < LastRec && i < dsHistory.Rows.Count; i++)
                {
                    dtn.ImportRow(dsHistory.Rows[i]);
                }
                grvVQFHistory.DataSource = dtn;
                grvVQFHistory.DataBind();

            }
            else if (txtVendor.Text.Trim() != "" && txtFDate.Text.Trim() == "" && txtTDate.Text.Trim() != "")
            {
                lblMessage.Text = "";
                lblMessage.Text += TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "FromDate").ToString()); 
                mpopAlert.Show();
            }
            else if (txtVendor.Text.Trim() == "" && txtFDate.Text.Trim() == "" && txtTDate.Text.Trim() != "")
            {
                lblMessage.Text = "";
                lblMessage.Text += TAG_ERROR_MSG.Replace("{}", HttpContext.GetGlobalResourceObject("ErrorMessages", "ToDate").ToString()); 
                mpopAlert.Show();
            }


            if (grvVQFHistory.Rows.Count <= 0)
            {
                trSRE.Style["display"] = "none";

            }
            if (grvVQFHistory.Rows.Count > 0)
            {
                lblmsgInfo.Text = string.Empty;
                tdnextPrevious1.Style["display"] = "";
                Search.Style["display"] = "";
                trSRE.Style["display"] = "";
            }
            else
            {
                lblmsgInfo.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "NoRecords").ToString(); 
                tdnextPrevious1.Style["display"] = "none";
                Search.Style["display"] = "none";
            }

            plcCrntPage1.Controls.Clear();
            int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));
            lblTotal.Text = Convert.ToString(hdnCount.Value);
            Session["Page11"] = Convert.ToString(page);
            lblTotalPage.Text = Convert.ToString(Session["Page11"]);
            if (Convert.ToInt32(Session["Page11"]) <= Convert.ToInt32(hdnPage.Value))
            {
                imbNext.Visible = false;
            }
            else
            {
                imbNext.Visible = true;
            }
            if (hdnPage.Value == "1")
            {
                imbPrevious.Visible = false;
            }
            else
            {
                imbPrevious.Visible = true;
            }
            if (Convert.ToInt32(Session["Page11"]) >= 5)
            {
                for (i = 1; i <= 5; i++)
                {
                    LinkButton lnk = new LinkButton();
                    lnk.Click += new EventHandler(lnk1_Click);
                    lnk.Text = Convert.ToString(i);
                    if (i == 1)
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnk.CssClass = "lnkcolor";
                    }
                    plcCrntPage1.Controls.Add(lnk);
                    lnk.Text = Convert.ToString(i);
                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnPage.ClientID + "')");
                }
            }
            else
            {
                for (i = 1; i <= Convert.ToInt32(Session["Page11"]); i++)
                {
                    LinkButton lnk = new LinkButton();
                    lnk.Click += new EventHandler(lnk1_Click);
                    lnk.Text = Convert.ToString(i);
                    if (i == 1)
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnk.CssClass = "lnkcolor";
                    }
                    plcCrntPage1.Controls.Add(lnk);
                    lnk.Text = Convert.ToString(i);
                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnPage.ClientID + "')");
                }
            }
        }
    }


    private void SubFuctionOfDateChecker(string process)
    {
        lblMessage.Text += TAG_ERROR_MSG.Replace("{}", process);
        lblmsgInfo.Text = string.Empty;
        tdnextPrevious1.Style["display"] = "none";
        Search.Style["display"] = "none";
        trSRE.Style["display"] = "none";
        mpopAlert.Show();

    }

    private void DateChecker(TextBox txtDate, ServerValidateEventArgs args, string process)
    {
        DateTime attempttdate;
        string DateTemp = txtDate.Text;
        if ((DateTime.TryParse(txtDate.Text.Trim(), out attempttdate)))
        {

            //    SGS Changes on 09/03/2011 - date format            
            try
            {
                txtDate.Text = Convert.ToDateTime(txtDate.Text).ToString("MM/dd/yyyy");
                if ((Convert.ToDateTime(txtDate.Text) > Convert.ToDateTime("01/01/1753")) && (Convert.ToDateTime(txtDate.Text) < Convert.ToDateTime("01/01/2500")))
                    args.IsValid = true;
                else
                {
                    txtDate.Text = DateTemp;
                    args.IsValid = false;
                }
            }
            catch
            {
                txtDate.Text = DateTemp;
                args.IsValid = false;
            }
            
        }
    }
    

    #endregion


    #region Sorting methods
   
    /// <summary>
    /// Method to sort gridview
    /// </summary>
    /// <param name="sortExpression"></param>
    /// <param name="direction"></param>
    private void SortGridView1(string sortExpression, string direction)
    {
        SortExp = sortExpression + " " + direction;
        hdnSort.Value = SortExp;
        plcCrntPage1.Controls.Clear();
        BindHistory();

        //Call method to create paging controls dynamically
       // CreatePaging(false, "");
        //Display previous and next button
      //  DisplayPreviousNext();
        //Apply css for the current page
       StyleforPaging(hdnPage.Value, plcCrntPage1);
       // lblTotalPage.Text = Convert.ToString(Session["Page11"]);

    }

    /// <summary>
    /// Get sorting direction
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
            {
                ViewState["sortDirection"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["sortDirection"];
        }
        set
        {
            ViewState["sortDirection"] = value;
        }
    }

    #endregion


    #region paging methods
    /// <summary>
    /// Display Previous/Next Page SRE Method
    /// </summary>
    private void DisplayPreviousNext()
    {
        imbPrevious.Visible = !hdnPage.Value.Equals("1");
        imbNext.Visible = !(Convert.ToInt32(Session["Page11"]) <= Convert.ToInt32(hdnPage.Value));
    }
    /// <summary>
    /// paginf style method
    /// </summary>
    /// <param name="CurrentLink"></param>
    private void StyleforPaging(string CurrentLink, PlaceHolder plc)
    {
        foreach (Control ctrl in plc.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                lnk1.CssClass = lnk1.Text.Equals(CurrentLink) ? "Selectlnkcolor" : "lnkcolor";
            }
        }
    }


    #endregion

    #region Validate Methods

    protected void cusFDate_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime attemptfdate;
        if ((DateTime.TryParse(txtFDate.Text.Trim(), out attemptfdate)))
        {
            DateChecker(txtFDate, args, "Invalid from date");
            //args.IsValid = true;

        }
        else
        {
            lblMessage.Text = TAG_ERROR_MSG.Replace("{}", "Invalid from date");
            lblmsgInfo.Text = string.Empty;
            tdnextPrevious1.Style["display"] = "none";
            Search.Style["display"] = "none";
            trSRE.Style["display"] = "none";
            mpopAlert.Show();
            args.IsValid = false;
        }
    }


    protected void cusTDate_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime attempttdate;
        if ((DateTime.TryParse(txtTDate.Text.Trim(), out attempttdate)))
        {
            DateChecker(txtTDate, args, "Invalid to date");
           

        }
        else
        {
            SubFuctionOfDateChecker("Invalid To date");
            args.IsValid = false;
        }
    }


    #endregion
}