﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="print.aspx.cs" Inherits="Admin_print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print</title>
    <script language ="javascript" type="text/javascript">
          window.onerror = ErrHndl;

          function ErrHndl()
          { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>

    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />

    <script src="../Script/jquery.js" type="text/javascript"></script>

    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>

    <script type="text/javascript">

//        function printfn() {

//            window.setInterval("window.close()", 10);
//        }
    </script>

    <%--<style>
       @media print
        {
            body
            {
                zoom: 100%;
            }
        }
    </style>--%>

    <%--<script language="javascript" type="text/javascript">
        if (navigator.appVersion.indexOf("MSIE 6") != -1) {
            document.styleSheets[document.styleSheets.length - 1].rules[0].style.zoom = "74%";
        }
        else if (navigator.appVersion.indexOf("MSIE 8") != -1) {


            document.styleSheets[document.styleSheets.length - 1].rules[0].style.zoom;
        }
        else
        { document.styleSheets[document.styleSheets.length - 1].rules[0].style.zoom; }

    </script>--%>

</head>
<body >
    <form id="form1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" width="900px" style="border: 1px solid #cccccc;" align="center">
            <tr>
                <td class="container">
                    <asp:Panel ID="pnlPrint" runat="server">
                        <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" id="SubcontractorRating"
                            runat="server">
                            <tr>
                                <td>
                                    <Haskell:TopMenu ID="TopMenu" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="table_border">
                                    <table width="100%" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="bodytextbold " width="13%">
                                                            Subcontractor:&nbsp;
                                                        </td>
                                                        <td class="bodytextleft" width="65%">
                                                            <asp:Label ID="lblSubcontractorname" CssClass="bodytextheadbold" runat="server"> </asp:Label>
                                                        </td>
                                                        <td class="bodytextbold_right" width="10%">
                                                            Rating Date
                                                        </td>
                                                        <td class="bodytextright left_border" width="8%">
                                                            <asp:Label ID="lblRatingDate" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span id="spnedit" runat="server">
                                                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                         <td width="1%">  <span class="mandatorystar">*</span></td>
                                                            <td class="bodytextheadbold" width="19%">
                                                                Project Number
                                                            </td>
                                                            <td width="15%">
                                                                <asp:Label ID="lblProjectNumber" runat="server" Visible="true" CssClass="bodytextheadbold"></asp:Label>
                                                            </td>
                                                            <td class="bodytextbold" width="12%">
                                                                Project Name
                                                            </td>
                                                            <td class="bodytextleft">
                                                                <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                           <td></td>
                                                            <td class="bodytextbold ">
                                                                Rater Name
                                                            </td>
                                                            <td class="bodytextleft">
                                                                <asp:Label ID="lblRaterName" runat="server"></asp:Label>
                                                            </td>
                                                            <td class="bodytextbold">
                                                                Rater Title
                                                            </td>
                                                            <td class="bodytextleft">
                                                                <asp:Label ID="lblRaterTitle" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                                                                  <td></td>
                                                                                                 <td class="bodytextbold " nowrap="nowrap">
                                                                                                        Final Subcontract Value :
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                      <asp:Label ID="lblFinalSubcontractor" runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="bodytextbold">
                                                                                                       
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                       
                                                                                                    </td>
                                                                                                </tr>
                                                        <%--  <tr>
                                                                                        <td class="bodytextbold">
                                                                                            Rater Title
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:Label ID="lblRaterTitle1" runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="2">
                                                                                            <span id="spnModDate" runat="server" style="display: none">
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextleft">
                                                                                                            Last Modified Date
                                                                                                        </td>
                                                                                                        <td class="bodytextleft">
                                                                                                           <asp:Label ID="lblLastModified" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>--%>
                                                    </table>
                                                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                           <td width="1%"></td>
                                                            <td class="bodytextbold " width="20%" valign="top">
                                                               &nbsp;Subcontractor scope
                                                            </td>
                                                            <td class="bodytextleft" valign="top">
                                                                <asp:TextBox ID="txtSubContractorScope" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                    onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                    TabIndex="1" runat="server" TextMode="MultiLine" CssClass="txtboxlarge" Width="555px"
                                                                    Height="40px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" class="sectionpad">
                                    <table width="100%" cellpadding="5" cellspacing="0">
                                        <tr>
                                            <td style="padding-top: 0">
                                                <table cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="bodytextheadcenter" colspan="5">
                                                            Ratings
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" align="center">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td class="bodytextheadright" width="50%"> <span class="mandatorystar">* </span>
                                                                        Overall Performance
                                                                    </td>
                                                                    <td style="padding: 0 50px 0 15px" align=left >
                                                                        <asp:Image ID="imgOverall" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bodytextleft" width="35%">
                                                            Safety
                                                        </td>
                                                        <td>
                                                            <asp:Image ID="imgSafety" runat="server" />
                                                        </td>
                                                        <td width="15px">
                                                        </td>
                                                        <td class="bodytextleft">
                                                            Project Management
                                                        </td>
                                                        <td>
                                                            <asp:Image ID="imgProject" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bodytextleft">
                                                            Quality Of Work
                                                        </td>
                                                        <td>
                                                            <asp:Image ID="imgQuality" runat="server" />
                                                        </td>
                                                        <td width="15px">
                                                        </td>
                                                        <td class="bodytextleft">
                                                            Field Management
                                                        </td>
                                                        <td>
                                                            <asp:Image ID="imgField" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bodytextleft">
                                                            Financial Capability
                                                        </td>
                                                        <td>
                                                            <asp:Image ID="imgFinancial" runat="server" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="bodytextleft">
                                                            Change Orders/Claims
                                                        </td>
                                                        <td>
                                                            <asp:Image ID="imgOrders" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bodytextleft">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Did S/C pay sub-subs/vendors on time?
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rblpay" runat="server" RepeatDirection="Horizontal" TabIndex="8"
                                                                CssClass="textboxsmall">
                                                                <asp:ListItem>Y</asp:ListItem>
                                                                <asp:ListItem>N</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td class="bodytextleft">
                                                             Schedule Control
                                                        </td>
                                                        <td>
                                                            <asp:Image ID="imgSchedule" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bodytextleft">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Did Haskell have to waive insurance levels?
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rblLevels" runat="server" RepeatDirection="Horizontal" TabIndex="9">
                                                                <asp:ListItem>Y</asp:ListItem>
                                                                <asp:ListItem>N</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td class="bodytextleft">
                                                            Cooperation
                                                        </td>
                                                        <td>
                                                            <asp:Image ID="imgCorporation" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bodytextleft">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Did sub require special payment terms?
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rblPayment" runat="server" RepeatDirection="Horizontal"
                                                                TabIndex="10">
                                                                <asp:ListItem>Y</asp:ListItem>
                                                                <asp:ListItem>N</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td class="bodytextleft">
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bodytextbold">
                                                Comments
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtComments" runat="server" onFocus="javascript:textCounter(this,1000,'yes');"
                                                    onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                    Height="125px" TextMode="MultiLine" TabIndex="13" Width="850px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdheight">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
