﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VQFView.aspx.cs" Inherits="Admin_VQFView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <link href="../css/ScreenStyle.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <script type="text/javascript">

        function printfn() {
            window.setInterval("window.close()", 10);
        }
    </script>
    <style media="print" type="text/css">
        .hide_print
        {
            display: none;
        }
    </style>
    <style type="text/css">
        @media print
        {
            body
            {
                zoom: 100%;
            }
        }
    </style>
    <script language="javascript" type="text/javascript">
        if (navigator.appVersion.indexOf("MSIE 6") != -1) {
            document.styleSheets[document.styleSheets.length - 1].rules[0].style.zoom = "74%";
        }
        else if (navigator.appVersion.indexOf("MSIE 8") != -1) {

            //   alert('8');
            //  document.styleSheets[document.styleSheets.length - 1].rules[0].style.zoom;
        }
        else {
            //    alert('7');
            //   document.styleSheets[document.styleSheets.length - 1].rules[0].style.zoom;
        }

    </script>
</head>
<body onload="printfn()">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" style="border: 1px solid #cccccc;"
        align="left">
        <tr>
            <td class="container ">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <%-- <Haskell:VQFAdminCompanyViewMenu ID="CompanyTopMenu" runat="server" Visible="false">
                            </Haskell:VQFAdminCompanyViewMenu>--%>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="contenttdview">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="bodytextheadbold" valign="top">
                                                    <asp:Label ID="lblVendorName" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <tr>
                                                                    <td class="tdheight">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formhead" colspan="4">
                                                                        Vendor Qualification Form - Company
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdheight">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass ">
                                                                            <tr>
                                                                                <td class="sectionpad">
                                                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="25%">
                                                                                                Registration Date:
                                                                                            </td>
                                                                                            <td class="bodytextleftView" width="25%">
                                                                                                <asp:Label CssClass="bodytextleft" ID="lblDate" runat="server" Width="80px"></asp:Label>&nbsp;
                                                                                            </td>
                                                                                            <td class="bodytextbold_rightv" width="10%">
                                                                                                Last Update:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label CssClass="bodytextleft" ID="lblLastUpdateDate" runat="server" Width="80px"></asp:Label>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="25%">
                                                                                                Project / If Applicable:
                                                                                            </td>
                                                                                            <td class="bodytextleftView" colspan="3" width="75%">
                                                                                                <asp:Label ID="lblProjectIfAvailable" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                            <tr>
                                                                                <td class="Header">
                                                                                    Business Section
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" class="sectionpad">
                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td colspan="2">
                                                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold_rightv" width="25%">
                                                                                                                        Legal Business Name:
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleftView" width="75%" colspan="3">
                                                                                                                        <asp:Label CssClass="txtbox" ID="lblLegalBusinessName" runat="server"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold_rightv">
                                                                                                                        Other Business Name / DBA:
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleftView" colspan="3">
                                                                                                                        <asp:Label CssClass="txtbox" ID="lblOtherBusinessName" runat="server"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="4">
                                                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                                            <tr>
                                                                                                                                <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                                                    Registered As:
                                                                                                                                    <!--004-Sooraj-->
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleftView" valign="top" width="10%">
                                                                                                                                    <asp:Label ID="lblRegisteredAs" runat="server"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td class="bodytextbold_rightv" valign="top" width="22%">
                                                                                                                                    Federal Tax ID / Business ID:
                                                                                                                                    <!--004-Sooraj-->
                                                                                                                                </td>
                                                                                                                                <td class="bodytextleftView" valign="top" width="43%">
                                                                                                                                    <asp:Label ID="lblFederalTax1" runat="server" Width="150"></asp:Label>
                                                                                                                                    <asp:Label CssClass="txtboxmedium" ID="lblFederalTax2" runat="server" MaxLength="7">
                                                                                                                                    </asp:Label>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold_rightv" nowrap="nowrap">
                                                                                                                        Type of Company:
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleftView" colspan="3">
                                                                                                                        <asp:Label CssClass="txtbox" ID="lblTypeOfCompany" runat="server"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold_rightv" nowrap="nowrap">
                                                                                                                        D U N S Number:
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleftView" colspan="3">
                                                                                                                        <asp:Label CssClass="txtboxsmall" ID="lblDunsNumber" runat="server" MaxLength="2"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                 <tr>
                                                                                                                        <td colspan="4">
                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                                                <tr>
                                                                                                                                    <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                                                        Physical Address:
                                                                                                                                    </td>
                                                                                                                                    <td class="bodytextleftView" valign="top" width="40%">
                                                                                                                                        <asp:Label ID="lblAddress" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td class="bodytextbold_rightv" valign="top" width="5%">
                                                                                                                                        City:
                                                                                                                                    </td>
                                                                                                                                    <td class="bodytextleftView" valign="top" width="30%">
                                                                                                                                        <asp:Label ID="lblCity" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                                        Country:
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleftView" valign="top" width="25%">
                                                                                                                        <asp:Label ID="lblCountry" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextbold_rightv" valign="top" width="7%">
                                                                                                                        State:
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleftView" valign="top" width="20%">
                                                                                                                        <asp:Label ID="lblState" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextbold_rightv" valign="top" width="13%">
                                                                                                                        ZIP/Postal Code:
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleftView" valign="top" width="10%">
                                                                                                                        <asp:Label ID="lblZip" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold_rightv" valign="top">
                                                                                                                        Phone:
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                                        <asp:Label ID="lblMainPhno1" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextbold_rightv" valign="top">
                                                                                                                        Fax:
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleftView" valign="top" colspan="3">
                                                                                                                        <asp:Label ID="lblMainFaxno1" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                <tr>
                                                                                                                    <td class="bodytextbold_rightv" width="25%">
                                                                                                                        Mailing Address:
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleftView" width="40%">
                                                                                                                        &nbsp;<asp:Label ID="lblMAddress" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td class="bodytextbold_rightv" width="5%">
                                                                                                                        City:
                                                                                                                    </td>
                                                                                                                    <td class="bodytextleftView" width="30%">
                                                                                                                        <asp:Label ID="lblMCity" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table align="right" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                            Country:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" valign="top" width="25%">
                                                                                                            <asp:Label ID="lblMCountry" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td class="bodytextbold_rightv" valign="top" width="7%">
                                                                                                            State:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" valign="top" width="20%">
                                                                                                            <asp:Label ID="lblMState" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td class="bodytextbold_rightv" valign="top" width="13%">
                                                                                                            ZIP/Postal Code:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" valign="top" width="10%">
                                                                                                            <asp:Label ID="lblMZip" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" width="25%">
                                                                                                            Payment Remittance Address:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="40%">
                                                                                                            <asp:Label ID="lblPAddress" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                        </td>
                                                                                                        <td class="bodytextbold_rightv" width="5%">
                                                                                                            City:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="30%">
                                                                                                            <asp:Label ID="lblPCity" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table align="right" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                            Country:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" valign="top" width="25%">
                                                                                                            <asp:Label ID="lblPCountry" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td class="bodytextbold_rightv" valign="top" width="7%">
                                                                                                            State:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" valign="top" width="20%">
                                                                                                            <asp:Label ID="lblPState" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td class="bodytextbold_rightv" valign="top" width="13%">
                                                                                                            ZIP/Postal Code:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" valign="top" width="10%">
                                                                                                            <asp:Label ID="lblPZip" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td id="tdAddLocations" runat="server" class="bodytextbold_rightv1" style="display: none"
                                                                                                            width="25%">
                                                                                                            Additional Locations
                                                                                                        </td>
                                                                                                        <td width="75%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:DataList ID="dlAdditionalLocations" runat="server" OnItemDataBound="dlAdditionalLocations_ItemDataBound"
                                                                                                    Width="100%">
                                                                                                    <HeaderTemplate>
                                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                        </table>
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemTemplate>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                                            Address:
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView" valign="top" width="40%">
                                                                                                                            <%# Eval("Address")%>&nbsp;
                                                                                                                        </td>
                                                                                                                        <td class="bodytextbold_rightv" valign="top" width="5%">
                                                                                                                            City:
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView" valign="top" width="30%">
                                                                                                                            <%# Eval("City")%>&nbsp;
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                                            Country:
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView" valign="top" width="25%">
                                                                                                                            <%# Eval("CountryCode")%>&nbsp;
                                                                                                                        </td>
                                                                                                                        <td class="bodytextbold_rightv" valign="top" width="7%">
                                                                                                                            State:
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView" valign="top" width="20%">
                                                                                                                            <asp:Label ID="lblState" runat="server" Text='<%# Eval("StateCode") %>'></asp:Label>
                                                                                                                            <asp:Label ID="lblOtherState" runat="server" Text='<%# Eval("OtherState") %>'></asp:Label>
                                                                                                                            &nbsp;
                                                                                                                        </td>
                                                                                                                        <td class="bodytextbold_rightv" valign="top" width="13%">
                                                                                                                            ZIP/Postal Code:
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView" valign="top" width="10%">
                                                                                                                            <%# Eval("ZipCode")%>&nbsp;
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextbold_rightv" valign="top">
                                                                                                                            Phone:
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView" valign="top">
                                                                                                                            <%# Eval("Phone")%>&nbsp;
                                                                                                                        </td>
                                                                                                                        <td class="bodytextbold_rightv" valign="top">
                                                                                                                            Fax:
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView" valign="top" colspan="3">
                                                                                                                            <%# Eval("fax")%>&nbsp;
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        </Table>
                                                                                                    </FooterTemplate>
                                                                                                </asp:DataList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="sectionpad">
                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" width="25%">
                                                                                                            Company Website Address:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView">
                                                                                                            <asp:Label ID="lblCWebAddress" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv">
                                                                                                            Years in Business:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView">
                                                                                                            <asp:Label ID="lblYearinBusiness" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv">
                                                                                                            &nbsp; No of Employees:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView">
                                                                                                            <asp:Label ID="lblNoEmp" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <%--G. Vera 05/07/2013 - display always--%>
                                                                                                        <td id="trdesigncapabCtl" runat="server" class="bodytextbold_rightv" style="display: "
                                                                                                            valign="top">
                                                                                                            Do you have Design-Build Capabilities?
                                                                                                        </td>
                                                                                                        <td id="trdesigncapabCtlText" runat="server" class="bodytextleftView">
                                                                                                            <asp:Label ID="lbldesignBuild" runat="server">&nbsp;&nbsp;&nbsp;</asp:Label>
                                                                                                            <asp:Label ID="rdoCapabilitiesY" runat="server"></asp:Label>&nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="sectionpad">
                                                                                                <span id="salestax" runat="server" style="display: " width="100%">
                                                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv1" width="25%">
                                                                                                                State Sales Tax&nbsp;
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                State:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="ddlState" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                State Tax Number:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <span id="spnStateTax" runat="server">
                                                                                                                    <asp:Label ID="lblStateTaxNo1" runat="server" CssClass="txtboxsmall"></asp:Label>&nbsp;
                                                                                                                </span>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                            <tr>
                                                                                <td class="Header">
                                                                                    Business Type
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-right: 15px">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="25%">
                                                                                                Select Business Type:
                                                                                            </td>
                                                                                            <td class="bodytextleftView" width="75%" style="padding-right: 15px">
                                                                                                <asp:Label ID="rdoBusinessType" runat="server" Text="&nbsp;"> </asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr id="OtherBusiness" runat="server" style="display: none">
                                                                                            <td class="bodytextbold_rightv">
                                                                                                Please Specify:
                                                                                            </td>
                                                                                            <td class="bodytextleftView" style="padding-right: 15px">
                                                                                                <asp:Label CssClass="txtbox" ID="lblOther1" runat="server"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight" colspan="2">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table id="tblLabour" runat="server" style="display: " align="center" border="0"
                                                                            cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                            <tr>
                                                                                <td width="100%" class="Header">
                                                                                    Labor Affiliation
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="15px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-right: 15px">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="26%">
                                                                                                Union Shop?
                                                                                            </td>
                                                                                            <td class="bodytextleftView" width="74%">
                                                                                                <asp:Label ID="lblUnionShop" runat="server" CssClass="txtbox">
                                                                                                      
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td nowrap="nowrap" width="26%" class="bodytextbold_rightv">
                                                                                                <span id="tdUnionAffiliation" runat="server" style="display: none">Union Affiliation:
                                                                                                </span>
                                                                                            </td>
                                                                                            <td width="74%" align="left">
                                                                                                <table id="tdTxtUnionAffiliation" runat="server" style="display: none" cellpadding="0"
                                                                                                    cellspacing="0" border="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextleftView">
                                                                                                            <asp:Label ID="lblUnionAffiliation" runat="server" CssClass="txtbox">
                                                                                                      
                                                                                                            </asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <%--     <td style="padding-right: 15px">
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv" width="35%">
                                                                                        Labor Affiliation:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" width="65%">
                                                                                        <asp:Label ID="rdoLAYes" runat="server" CssClass="txtbox">
                                                                                                      
                                                                                        </asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>--%>
                                                                            </tr>
                                                                        </table>
                                                                        <table id="tblCompany" runat="server" style="display: " align="center" border="0"
                                                                            cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                            <tr id="CompanyCertificationHeader">
                                                                                <td class="Header">
                                                                                    Company Certifications
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="15px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-right: 15px">
                                                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="26%">
                                                                                                Company Certifications:
                                                                                            </td>
                                                                                            <td class="bodytextleftView" width="74%">
                                                                                                <asp:Label ID="rdoCompanyCertiY" runat="server" CssClass="txtbox">                                                                                                    
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                Federal SB:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label ID="lblFederalSB" runat="server" CssClass="txtbox">                                                                                                    
                                                                                                </asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2">
                                                                                                <span id="trCertifyingAgency" runat="server" style="display: ">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="26%">
                                                                                                                Certifying Agency:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                                <asp:Label ID="rdoCertifyingAgency" runat="server">
                                                                                                                       
                                                                                                                </asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <%--G. Vera 10/24/2012 - Added--%>
                                                                                        <tr>
                                                                                            <td colspan="2">
                                                                                                <span id="Span1" runat="server" style="display: ">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="26%">
                                                                                                                Industry Certifications:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblISO" runat="server">
                                                                                                                       
                                                                                                                </asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="26%">
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblQS" runat="server">
                                                                                                                       
                                                                                                                </asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="15px">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                            <tr>
                                                                                <td class="Header">
                                                                                    Key Personnel Contacts
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="15px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-right: 15px">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td width="30%">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td class="bodytextleftView bodytextbold">
                                                                                                Name
                                                                                            </td>
                                                                                            <td class="bodytextleftView bodytextbold">
                                                                                                Title
                                                                                            </td>
                                                                                            <td class="bodytextleftView bodytextbold">
                                                                                                E-Mail
                                                                                            </td>
                                                                                            <td class="bodytextleftView bodytextbold" nowrap="nowrap">
                                                                                                Phone Number
                                                                                            </td>
                                                                                            <td class="bodytextleftView bodytextbold" nowrap="nowrap">
                                                                                                Fax Number
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" align="right" valign="top" width="30%">
                                                                                                Principal Contact:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label CssClass="" ID="lblPrincipal" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label CssClass="" ID="lblPTitle" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label CssClass="" ID="lblPEmail" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView" nowrap="nowrap">
                                                                                                <asp:Label ID="lblPPhone1" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView" nowrap="nowrap">
                                                                                                <asp:Label CssClass="" ID="lblPFax" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" align="right" valign="top" width="30%">
                                                                                                Project Management Contact:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label class="txtbox" ID="lblProjName" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label class="txtbox" ID="lblProjTitle" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label class="txtbox" ID="lblProjemail" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView" nowrap="nowrap">
                                                                                                <asp:Label CssClass="txtboxsmall" ID="lblProjPhone1" runat="server" MaxLength="3"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView" nowrap="nowrap">
                                                                                                <asp:Label CssClass="" ID="lblProjFax1" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" align="right" valign="top" width="30%">
                                                                                                Account Receivable Contact:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label ID="lblArName" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label ID="lblARTitle" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label ID="lblAREmail" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView" nowrap="nowrap">
                                                                                                <asp:Label CssClass="txtboxsmall" ID="lblARPhone1" runat="server" MaxLength="3"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView" nowrap="nowrap">
                                                                                                <asp:Label CssClass="" ID="lblARFax1" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" align="right" valign="top" width="30%">
                                                                                                Account Payable Contact:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label class="txtbox" ID="lblApName" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label class="txtbox" ID="lblApTitle" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label class="txtbox" ID="lblApEmail" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView" nowrap="nowrap">
                                                                                                <asp:Label CssClass="txtboxsmall" ID="lblApPhone1" runat="server" MaxLength="3"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView" nowrap="nowrap">
                                                                                                <asp:Label CssClass="" ID="lblApFax1" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <%--G. Vera 11/01/2012 - added--%>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" align="right" valign="top" width="30%">
                                                                                                Equipment Supplier Onsite Support Contact:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label class="txtbox" ID="lblOnsiteSupportName" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label class="txtbox" ID="lblOnsiteSupportTitle" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                <asp:Label class="txtbox" ID="lblOnsiteSupportEmail" runat="server"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView" nowrap="nowrap">
                                                                                                <asp:Label CssClass="txtboxsmall" ID="lblOnsiteSupportPhone" runat="server" MaxLength="3"></asp:Label>
                                                                                            </td>
                                                                                            <td class="bodytextleftView" nowrap="nowrap">
                                                                                                <asp:Label CssClass="" ID="lblOnsiteSupportFax" class="txtbox" runat="server"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <%--G. Vera 11/01/2012 - End--%>
                                                                                        <!-- Changed by N Schoenberger 10-01-2011 - Formatting alignment problem with additional contacts -->
                                                                                        <asp:Repeater ID="rptKeyContact" runat="server" OnItemDataBound="rptKeyContact_ItemDataBound">
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv" align="right" valign="top" width="30%">
                                                                                                        <asp:Label ID="lblKeyContact" runat="server" Text="Additional Key Contacts:"></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:Label class="txtbox" ID="lblApName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:Label class="txtbox" ID="lblApTitle" runat="server" Text='<%# Eval("Title")%>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:Label class="txtbox" ID="lblApEmail" runat="server" Text='<%# Eval("Email")%>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" nowrap="nowrap">
                                                                                                        <asp:Label ID="lblPhone" CssClass="txtboxsmall" runat="server" MaxLength="3" Text='<%# Eval("Phone")%>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" nowrap="nowrap">
                                                                                                        <asp:Label CssClass="" ID="lblFax" class="txtbox" runat="server" Text='<%# Eval("fax")%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="15px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                            <tr>
                                                                                <td class="Header">
                                                                                    Officers
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-right: 15px">
                                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td class="bodytextbold left_border">
                                                                                                Corporate officers, partners or proprietors of your firm
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr id="tdOfficerData" runat="server" style="display: none;">
                                                                                            <td align="right">
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" align="left" width="24%">
                                                                                                            Name:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="20%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold_rightv" align="left" width="5%">
                                                                                                            Title:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="15%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold_rightv" width="19%">
                                                                                                            Percentage of Ownership:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="4%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" align="left">
                                                                                                            Name:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold_rightv">
                                                                                                            Title:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold_rightv" align="left">
                                                                                                            Percentage of Ownership:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" align="left">
                                                                                                            Name:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold_rightv" align="left">
                                                                                                            Title:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold_rightv" align="left">
                                                                                                            Percentage of Ownership:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="right">
                                                                                                <asp:Repeater ID="rptCompanyOfficers" runat="server" OnItemDataBound="rptCompanyOfficers_ItemDataBound">
                                                                                                    <HeaderTemplate>
                                                                                                        <table width="75%" align="right">
                                                                                                            <tr>
                                                                                                                <th class="bodytextbold" width="15%">
                                                                                                                    Name
                                                                                                                </th>
                                                                                                                <th class="bodytextbold " width="15%">
                                                                                                                    Title
                                                                                                                </th>
                                                                                                                <th class="bodytextbold" width="13%">
                                                                                                                    Percentage Of Ownership
                                                                                                                </th>
                                                                                                            </tr>
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemTemplate>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                                <%# Eval("Name")%>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                                <%# Eval("Title")%>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                                <asp:Label ID="lblPercentageOwnership" runat="server" Text=' <%# Eval("PercentageOfOwnerShip")%> '></asp:Label>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </ItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                        </table></FooterTemplate>
                                                                                                </asp:Repeater>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_right" width="57%">
                                                                                                            Have any of the officers ever done business with Haskell through another company?&nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="43%">
                                                                                                            <asp:Label ID="lblPContact" runat="server" CssClass="txtbox">                                                                                                              
                                                                                                            </asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="3">
                                                                                                            <span style="display: none" runat="server" id="trbussiness">
                                                                                                                <table width="100%">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextbold_right" colspan="1" width="20%" valign="top">
                                                                                                                            Please Explain:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView" colspan="2" valign="top">
                                                                                                                            <asp:Label CssClass="txtboxlarge" ID="lblPContactY" runat="server"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="15px">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr style="page-break-before: always">
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formhead">
                                                                            Vendor Qualification Form - Legal & Financial
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr>
                                                                                    <td class="Header" colspan="2">
                                                                                        Legal
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="sectionpad">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold" width="51%">
                                                                                                    Have you failed to complete awarded work or been terminated for cause?
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="51%">
                                                                                                    <asp:Label ID="lblListOne" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trWorkAwdOne" runat="server" style="display: none">
                                                                                                <td colspan="2">
                                                                                                    <span>
                                                                                                        <table width="100%">
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold left_border" valign="top" width="15%" nowrap="nowrap">
                                                                                                                    If Yes, Please Explain:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label CssClass="txtbox" onFocus="javascript:textCounter(this,150,'yes');" onKeyDown="javascript:textCounter(this,150,'yes');"
                                                                                                                        onKeyUp="javascript:textCounter(this,150,'yes');" ID="txtWorkAwd" TextMode="MultiLine"
                                                                                                                        runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="sectionpad">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextleft " width="87%">
                                                                                                    <span class="bodytextbold">Is your company or any of its Owners, Officers or Major Stockholders
                                                                                                        currently involved in any arbitration, litigation, suits or liens?</span>
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="13%">
                                                                                                    <asp:Label ID="lblListTwo" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trStock" runat="server" style="display: none">
                                                                                                <td colspan="2">
                                                                                                    <span>
                                                                                                        <table width="100%">
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold left_border" valign="top" width="15%" nowrap="nowrap">
                                                                                                                    If Yes, Please Explain:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label CssClass="txtbox" ID="txtStake" onFocus="javascript:textCounter(this,150,'yes');"
                                                                                                                        onKeyDown="javascript:textCounter(this,150,'yes');" onKeyUp="javascript:textCounter(this,150,'yes');"
                                                                                                                        TextMode="MultiLine" runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="sectionpad">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextleft" width="50%">
                                                                                                    <span class="bodytextbold">Has the company had any judgments, bankruptcies or reorganizations?</span>
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="lblListThree" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trBankruptcy" runat="server" style="display: none">
                                                                                                <td colspan="2">
                                                                                                    <span>
                                                                                                        <table width="100%">
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold left_border" valign="top" width="15%" nowrap="nowrap">
                                                                                                                    If Yes, Please Explain:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label CssClass="txtbox" ID="txtBankruptcy" TextMode="MultiLine" runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="sectionpad">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold" width="94%">
                                                                                                    Have any of the Owner, Officers or Major Stockholders of your company ever been
                                                                                                    indicted or convicted of felony or other criminal conduct?
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="lblListFour" runat="server">                                                                                          
                                                                                                    </asp:Label>
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trConviction" runat="server" style="display: none">
                                                                                                <td colspan="2">
                                                                                                    <span>
                                                                                                        <table width="100%">
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold left_border" valign="top" width="15%" nowrap="nowrap">
                                                                                                                    If Yes, Please Explain:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label CssClass="txtbox" ID="txtRisk" TextMode="MultiLine" runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr>
                                                                                    <td class="Header" colspan="5">
                                                                                        Financial
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtdbold_rt sectionpad" colspan="4">
                                                                                        List data for the three most recent completed fiscal years
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="tdFinancialData" runat="server" style="display: none;">
                                                                                    <td colspan="4">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    &nbsp;&nbsp;Year One:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    <asp:Label ID="lblFinancialYear1" runat="server"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    N/A:
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="60%">
                                                                                                    <asp:CheckBox ID="chkFinancialNA1" runat="server" Enabled="false" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    Please Explain For N/A:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    &nbsp;&nbsp;Maximum Contract Value Completed $:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    &nbsp;&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    &nbsp;&nbsp;Annual Company Revenue $:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    &nbsp;&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    &nbsp;&nbsp;Year Two:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    <asp:Label ID="lblFinancialYear2" runat="server"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    N/A:
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    <asp:CheckBox ID="chkFinancialNA2" runat="server" Enabled="false" />
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    Please Explain For N/A:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    &nbsp;&nbsp;Maximum Contract Value Completed $:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    &nbsp;&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    &nbsp;&nbsp;Annual Company Revenue $:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    &nbsp;&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    &nbsp;&nbsp;Year Three:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    <asp:Label ID="lblFinancialYear3" runat="server"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    N/A:
                                                                                                </td>
                                                                                                <td class="bodytextleft">
                                                                                                    <asp:CheckBox ID="chkFinancialNA3" runat="server" Enabled="false" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    Please Explain For N/A:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    &nbsp;&nbsp;Maximum Contract Value Completed $:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    &nbsp;&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    &nbsp;&nbsp;Annual Company Revenue $:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                    &nbsp;&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4" style="padding-left: 10px">
                                                                                        <table width="100%">
                                                                                            <tr id="tblFinancial" runat="server" style="display: none;">
                                                                                                <td class="bodytextbold" width="15%">
                                                                                                    Year
                                                                                                </td>
                                                                                                <td class="bodytextbold" width="15%" id="trFinanceNA" runat="server">
                                                                                                    N/A
                                                                                                </td>
                                                                                                <td class="bodytextbold" width="30%">
                                                                                                    Maximum Contract Value Completed $
                                                                                                </td>
                                                                                                <td class="bodytextbold" width="40%">
                                                                                                    Annual Company Revenue $
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="5">
                                                                                                    <asp:Repeater ID="rptFinancial" runat="server" OnItemDataBound="rptFinancial_ItemDataBound">
                                                                                                        <HeaderTemplate>
                                                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="2" width="100%">
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <tr>
                                                                                                                <td class="bodytextleftView" width="15%">
                                                                                                                    <%# Eval("Year")%>
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView" width="15%" id="tdFinanceNA" runat="server">
                                                                                                                    <asp:CheckBox ID="chkFinancialNA" runat="server" Enabled="false" Checked='<%#Convert.ToBoolean(Eval("NotAvailable"))%>' />
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView" width="30%">
                                                                                                                    &nbsp;
                                                                                                                    <asp:Label ID="lblContractValue" CssClass="txtbox" runat="server" Text='<%# Eval("ContractValue") %>'></asp:Label>
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView" width="40%">
                                                                                                                    &nbsp;
                                                                                                                    <asp:Label ID="lblAnnualRevenue" CssClass="txtbox" runat="server" Text='<%# Eval("AnnualRevenue") %>'></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="trExplain" runat="server">
                                                                                                                <td colspan="4">
                                                                                                                    <table width="100%" border="0" cellpadding="0" cellspacing="2" width="100%">
                                                                                                                        <tr>
                                                                                                                            <td class="bodytextbold" width="140px">
                                                                                                                                Please Explain For N/A:
                                                                                                                            </td>
                                                                                                                            <td class="bodytextleftView">
                                                                                                                                <%# Eval("Explanation")%>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </ItemTemplate>
                                                                                                        <FooterTemplate>
                                                                                                            </table></FooterTemplate>
                                                                                                    </asp:Repeater>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td width="10%">
                                                                                                </td>
                                                                                                <td width="30%" class="bodytextbold_rightv">
                                                                                                    Current Year Projected Revenue $:
                                                                                                </td>
                                                                                                <td width="70%" class="bodytextleftView" colspan="3" align="left">
                                                                                                    <asp:Label ID="lblProjectRevenue" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                </td>
                                                                                                <td width="10%">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="10%">
                                                                                                </td>
                                                                                                <td width="30%" class="bodytextbold_rightv">
                                                                                                    Current Total Backlog $:
                                                                                                </td>
                                                                                                <td width="50%" class="bodytextleftView" colspan="3">
                                                                                                    <asp:Label ID="lblTotalBackLog" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                </td>
                                                                                                <td width="10%">
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr>
                                                                                    <td class="Header" colspan="4">
                                                                                        Banking
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4" class="formtdbold_rt sectionpad">
                                                                                        Banking References
                                                                                    </td>
                                                                                </tr>
                                                                                <%-- 003- Sooraj--%>
                                                                                <tr id="trDenominateInDollar" runat="server" style="display: none">
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 0 2px 0 0">
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    Base Currency:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="30%">
                                                                                                    <asp:Label ID="lblBaseCurrency" runat="server"></asp:Label>&nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv" width="20%">
                                                                                                    Denominate in US Dollars ?
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="10%">
                                                                                                    <asp:Label ID="lblDenominate" runat="server"></asp:Label>&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="tdBankingData" runat="server" style="display: none;">
                                                                                    <td>
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0 2px 0 0">
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                    Financial Institution:&nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="60%" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Established Line of Credit?
                                                                                                </td>
                                                                                                <td width="60%" class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Contact Name:
                                                                                                </td>
                                                                                                <td width="60%" class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Contact Phone Number:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Address:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="30%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv" width="5%">
                                                                                                    City:
                                                                                                </td>
                                                                                                <td width="25%" class="bodytextleftView">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="40%">
                                                                                                                Country:
                                                                                                            </td>
                                                                                                            <td width="18%" class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="6%">
                                                                                                                State:
                                                                                                            </td>
                                                                                                            <td width="14%" class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="13%">
                                                                                                                ZIP/Postal Code:
                                                                                                            </td>
                                                                                                            <td width="10%" class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="100%" colspan="4" style="border-bottom: 1px solid #cccccc">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Repeater ID="rptBanking" runat="server" OnItemDataBound="rptBanking_ItemDataBound">
                                                                                            <HeaderTemplate>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table width="100%">
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                                    Financial Institution:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView" width="60%" colspan="3">
                                                                                                                    <%# Eval("FinancialInstitution") %>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                                    Established Line of Credit?
                                                                                                                </td>
                                                                                                                <td width="60%" class="bodytextleftView" colspan="3">
                                                                                                                    <asp:Label ID="lblCredit" runat="server" Text='<%# Eval("Credit") %>'></asp:Label>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                                    Contact Name:
                                                                                                                </td>
                                                                                                                <td width="60%" class="bodytextleftView" colspan="3">
                                                                                                                    <%# Eval("ContactName") %>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                                    Contact Phone Number:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView" width="60%" colspan="3">
                                                                                                                    <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("Phone") %>'></asp:Label>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                                    Address:
                                                                                                                </td>
                                                                                                                <td width="30%" class="bodytextleftView">
                                                                                                                    <%# Eval("Address") %>&nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextbold_rightv" width="5%">
                                                                                                                    City:
                                                                                                                </td>
                                                                                                                <td width="25%" class="bodytextleftView">
                                                                                                                    <%#Eval("City") %>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="4">
                                                                                                                    <table width="100%">
                                                                                                                        <tr>
                                                                                                                            <td class="bodytextbold_rightv" width="40%" style="padding-left: 5px;">
                                                                                                                                Country:
                                                                                                                            </td>
                                                                                                                            <td width="20%" class="bodytextleftView">
                                                                                                                                <%#Eval("CountryName")%>&nbsp;
                                                                                                                            </td>
                                                                                                                            <td class="bodytextbold_rightv" width="5%">
                                                                                                                                State:
                                                                                                                            </td>
                                                                                                                            <td width="15%" class="bodytextleftView">
                                                                                                                                <asp:Label ID="lblState" runat="server" Text='<%#Eval("Fk_State") %>'></asp:Label>
                                                                                                                                <asp:Label ID="lblOtherState" runat="server" Text='<%# Eval("OtherState") %>'></asp:Label>&nbsp;
                                                                                                                            </td>
                                                                                                                            <td class="bodytextbold_rightv" width="13%">
                                                                                                                                ZIP/Postal Code:
                                                                                                                            </td>
                                                                                                                            <td width="14%" class="bodytextleftView">
                                                                                                                                <%#Eval("Zipcode") %>&nbsp;
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td width="100%" colspan="7" style="border-bottom: 1px solid #cccccc">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                            </FooterTemplate>
                                                                                        </asp:Repeater>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr style="page-break-before: always">
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formhead">
                                                                            Vendor Qualification Form - Trade Information
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table id="tblTradeScopeHeader" runat="server" align="center" border="0" cellpadding="0"
                                                                                cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr id="TradeScopeHeader" runat="server">
                                                                                    <td class="Header" colspan="2">
                                                                                        Scopes of Work
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trScopeEmptyDetails" runat="server" style="display: none">
                                                                                    <td class="sectionpad">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold left_border" colspan="2">
                                                                                                    No data selected
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trScopeCompleteDetails" runat="server" style="display: none">
                                                                                    <td colspan="2" class="sectionpad">
                                                                                        <asp:Table ID="tblTradeInfo" runat="server" Width="100%">
                                                                                            <asp:TableRow ID="trRow1" runat="server">
                                                                                                <asp:TableCell ID="tdCell1" runat="server" VerticalAlign="Top" Width="50%"></asp:TableCell>
                                                                                                <asp:TableCell ID="tdCell2" runat="server" VerticalAlign="Top" Width="50%"></asp:TableCell>
                                                                                            </asp:TableRow>
                                                                                        </asp:Table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table id="tblTradeRegionHeader" runat="server" align="center" border="0" cellpadding="0"
                                                                                cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr id="TradeRegionHeader" runat="server">
                                                                                    <td class="Header" colspan="2">
                                                                                        Regions
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trCompleteRegions" runat="server" style="display: none">
                                                                                    <td colspan="2" style="padding-left: 15px">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextleft">
                                                                                                    <asp:DataList ID="dlRegions" runat="server" RepeatColumns="4" ItemStyle-CssClass="pad_left"
                                                                                                        AlternatingItemStyle-CssClass="pad_left" RepeatDirection="Horizontal" Width="100%"
                                                                                                        RepeatLayout="Table" ItemStyle-Wrap="true" ItemStyle-Width="150px" ItemStyle-VerticalAlign="Top"
                                                                                                        AlternatingItemStyle-VerticalAlign="Top" OnItemDataBound="dlRegions_ItemDataBound">
                                                                                                        <HeaderTemplate>
                                                                                                            <asp:Label ID="Label1" runat="server" CssClass="tree_head1" Text="United States of America"></asp:Label>
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblName" runat="server" CssClass="tree_head1" Text='<%# Eval("StateCode") %>'></asp:Label>
                                                                                                            <div style="padding-left: 15px;">
                                                                                                                <asp:Label ID="lstRegions" runat="server" Text='<%# Eval("OtherSate") %>'></asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:DataList>
                                                                                                    <asp:DataList ID="dlContinents" runat="server" RepeatColumns="4" ItemStyle-CssClass="pad_left"
                                                                                                        AlternatingItemStyle-CssClass="pad_left" RepeatDirection="Horizontal" Width="100%"
                                                                                                        RepeatLayout="Table" ItemStyle-Wrap="true" ItemStyle-Width="150px" ItemStyle-VerticalAlign="Top"
                                                                                                        AlternatingItemStyle-VerticalAlign="Top">
                                                                                                        <%--    <HeaderTemplate>
                                                                                        <span class="text">Continents</span>
                                                                                    </HeaderTemplate>--%>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblContinent" runat="server" CssClass="tree_head1" Text='<%# Eval("ContinentName") %>'></asp:Label>
                                                                                                            <div style="padding-left: 15px;">
                                                                                                                <asp:Label ID="lstCountries" runat="server" Text='<%# Eval("CountryName") %>'></asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:DataList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <%-- <asp:CheckBoxList ID="testcheck1" runat="server" RepeatColumns="4" RepeatDirection="Horizontal"
                                                                   CssClass="lstbox" Width="100%" AutoPostBack="true"
                                                                       onselectedindexchanged="testcheck1_SelectedIndexChanged" >
                                                                                        --%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trEmptyRegions" runat="server" style="display: none">
                                                                                    <td colspan="2" class="sectionpad">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold left_border" colspan="2">
                                                                                                    No data selected
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" style="border-bottom: 1px solid #cccccc">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="bodytextbold">
                                                                                        &nbsp;&nbsp;List License numbers in which your company is legally qualified to work
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trLicenseComplete" runat="server" style="display: none;">
                                                                                    <td colspan="2" class="left_border">
                                                                                        <asp:Repeater ID="rptLicenseNumber" runat="server" OnItemDataBound="rptLicenseNumber_ItemDataBound">
                                                                                            <HeaderTemplate>
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td width="10%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold" width="20%" valign="top">
                                                                                                            Country
                                                                                                        </td>
                                                                                                        <td width="2%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold" width="25%" valign="top">
                                                                                                            State
                                                                                                        </td>
                                                                                                        <td width="2%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold" width="20%" valign="top">
                                                                                                            License Number
                                                                                                        </td>
                                                                                                        <td width="2%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold" width="15%" valign="top">
                                                                                                            Expiration Date
                                                                                                        </td>
                                                                                                    </tr>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        <asp:Label ID="Label3" runat="Server" Text='<%# Eval("CountryName") %>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        &nbsp;<asp:Label ID="lblState" runat="Server" Text='<%# Eval("StateCode") %>'></asp:Label>
                                                                                                        <asp:Label ID="lblOtherState" runat="Server" Text='<%# Eval("OtherState") %>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        <%# Eval("LicenseNumber")%>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        <asp:Label ID="lblExpirationDate" runat="Server" Text='<%# Convert.ToDateTime(Eval("Expirationdate")).ToShortDateString()%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                </table></FooterTemplate>
                                                                                        </asp:Repeater>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trEmptyLicense" runat="server" style="display: none">
                                                                                    <td colspan="2" class="sectionpad">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td width="15%">
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv" width="20%">
                                                                                                    State:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="65%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="10%">
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv" width="20%">
                                                                                                    License Number:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="70%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="10%">
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv" width="20%">
                                                                                                    Expiry Date:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="70%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" class="sectionpad">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td width="5%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextbold" width="32%">
                                                                                                    Will you perform all work with your own forces?
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="63%">
                                                                                                    <asp:Label ID="lblOwnForces" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trExplain" runat="server" style="display: none" valign="top">
                                                                                                <td>
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    If No, Please Explain:&nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="lblExplain" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr id="ProjectTypesHeader" runat="server">
                                                                                    <td class="Header" colspan="4">
                                                                                        Types of Projects
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="ProjectTypesContent" runat="server" style="display: none">
                                                                                    <td class="exampletext">
                                                                                        &nbsp;&nbsp;Project types that your company performs work or in which it specializes
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trEmptyProjects" runat="server" style="display: none">
                                                                                    <td class="sectionpad">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold left_border" colspan="2">
                                                                                                    No data selected
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" style="border-bottom: 1px solid #cccccc">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trCompleteProjects" runat="server" style="display: none;">
                                                                                    <td class="sectionpad">
                                                                                        <asp:DataList ID="dlProjects" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                                                                                            RepeatColumns="3" RepeatDirection="Horizontal" RepeatLayout="Table">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblProjects" CssClass="bodytextleft" runat="server" Text='<%# Eval("ProjectName") %>'></asp:Label></ItemTemplate>
                                                                                        </asp:DataList>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <!--003-Sooraj-->
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="trSafety" runat="server">
                                                        <tr style="page-break-before: always">
                                                            <td>
                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formhead">
                                                                                        Vendor Qualification Form - Safety
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                                            <tr>
                                                                                                <td class="Header" colspan="7">
                                                                                                    Current EMR Rates
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="tblEmrRate" runat="server" style="display: ;">
                                                                                                <td class="sectionpad">
                                                                                                    <table cellpadding="5" cellspacing="3" border="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold" width="5%">
                                                                                                                Year
                                                                                                            </td>
                                                                                                            <td class="bodytextbold" width="20%">
                                                                                                                Rate
                                                                                                            </td>
                                                                                                            <td class="bodytextbold" width="5%">
                                                                                                                N/A
                                                                                                            </td>
                                                                                                            <td class="bodytextbold" width="70%">
                                                                                                                Please Explain
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="4">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblYear1" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblRate1" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                <asp:CheckBox ID="chkNA1" runat="server" Enabled="false" />&nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblExplain1" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblYear2" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblRate2" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                <asp:CheckBox ID="chkNA2" runat="server" Enabled="false" />&nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblExplain2" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblYear3" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblRate3" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                <asp:CheckBox ID="chkNA3" runat="server" Enabled="false" />&nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblExplain3" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                            <tr>
                                                                                                <td class="searchhdrbarbold">
                                                                                                    Safety History/OSHA 300, 300A
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="Safetytextbold">
                                                                                                    We understand that OSHA may not apply to your company. However, Haskell would like
                                                                                                    to obtain the information below.
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="padding-left: 5px; padding-right: 10px; padding-bottom: 10px">
                                                                                                    <table cellpadding="5" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="36%">
                                                                                                                Year:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="64%">
                                                                                                                <asp:Label ID="lblOshaYear1" runat="server"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                N/A:
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                <asp:CheckBox ID="chkOSHANA1" runat="server" Enabled="false" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Please Explain For N/A:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblExplainOsha1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of fatalities:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblOshafatalities1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of Cases with Days Away from Work:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblOshaCase1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of Cases with Job Transfer or Restriction:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblJobTranfer1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of Other Recordable Cases:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblRecordableCases1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Hours Worked By All Employees:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblTotalHours1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="" colspan="4" style="border-bottom: 1px solid #cccccc">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Year:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblOshaYear2" runat="server"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                N/A:
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                <asp:CheckBox ID="chkOSHANA2" runat="server" Enabled="false" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Please Explain For N/A:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblExplainOSHA2" runat="server" CssClass="txtbox"> </asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of fatalities:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblOshafatalities2" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of Cases with Days Away from Work:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblOshaCase2" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of Cases with Job Transfer or Restriction:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblJobTranfer2" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of Other Recordable Cases:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblRecordableCases2" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Hours Worked By All Employees:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblTotalHours2" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="" colspan="4" style="border-bottom: 1px solid #cccccc">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Year:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblOshaYear3" runat="server"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                N/A:
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                <asp:CheckBox ID="chkNAOSHA3" runat="server" Enabled="false" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Please Explain For N/A:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblExplainOSHA3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of fatalities:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblOshafatalities3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of Cases with Days Away from Work:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblOshaCase3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of Cases with Job Transfer or Restriction:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblJobTranfer3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Number of Other Recordable Cases:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblRecordableCases3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Total Hours Worked By All Employees:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblTotalHours3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="" colspan="4" style="border-bottom: 1px solid #cccccc">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-left: 5px; padding-right: 10px; padding-bottom: 10px" colspan="4">
                                                                                                                <table cellpadding="5" cellspacing="0" width="100%" border="0">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextbold_rightv" width="36%">
                                                                                                                            SIC or NAICS Code (From OSHA 300A Form)
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView">
                                                                                                                            <asp:Label ID="lblSIC" runat="server"></asp:Label>&nbsp;
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextbold_rightv">
                                                                                                                            N/A:
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView">
                                                                                                                            <asp:CheckBox ID="chkSICNA" runat="server" Enabled="false" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextbold_rightv">
                                                                                                                            Please Explain For N/A:
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView">
                                                                                                                            <asp:Label ID="lblSICExplain" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                                            <tr>
                                                                                                <td class="Header" colspan="2">
                                                                                                    Questionnaire
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" class="sectionpad">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold" width="35%">
                                                                                                                Does your company have a written Safety Program?
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="63%">
                                                                                                                <asp:Label ID="lblSafetyProgram" runat="server" CssClass="txtbox"> </asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" class="sectionpad">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold" width="34%">
                                                                                                                <span class="bodytextbold">Are all employees trained in safety requirements?</span>
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="65%">
                                                                                                                <asp:Label ID="lblSafetytraining" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" class="sectionpad">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold" width="54%">
                                                                                                                <span class="bodytextbold">Do you have a Company Safety Director or other Safety Professionals
                                                                                                                    on Staff?</span>
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="47%">
                                                                                                                <asp:Label ID="lblSafetyDirector" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="sectionpad" colspan="2">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr id="spnContact" runat="server" style="display: none">
                                                                                                            <td colspan="2">
                                                                                                                <span>
                                                                                                                    <table width="100%">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                            </td>
                                                                                                                            <td class="bodytextbold_rightv" width="40%">
                                                                                                                                If Yes, Please Specify Contact Name:
                                                                                                                            </td>
                                                                                                                            <td class="bodytextleftView" width="60%">
                                                                                                                                <asp:Label ID="lblContactName" runat="server" CssClass="txtbox"></asp:Label>&nbsp;
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                            </td>
                                                                                                                            <td class="bodytextbold_rightv" width="40%">
                                                                                                                                Contact Number:
                                                                                                                            </td>
                                                                                                                            <td class="bodytextleftView" width="60%">
                                                                                                                                <asp:Label ID="lblMainPhno2" runat="server" CssClass="txtboxsmall" MaxLength="3"></asp:Label>&nbsp;
                                                                                                                                <%--      <asp:Label ID="lblMainPhno2" runat="server" CssClass="txtboxsmall" MaxLength="3"></asp:Label>
                                                                                           
                                                                                            
                                                                                            <asp:Label ID="lblMainPhno3" runat="server" CssClass="txtboxsmall" MaxLength="4"></asp:Label>--%>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </span>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <%--Sooraj 10/25/2013 - New Question added--%>
                                                                                            <tr id="trNonUSSafetyQuestion" runat="server" style="display: none">
                                                                                                <td colspan="2" class="sectionpad">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold" width="42%">
                                                                                                                <span class="bodytextbold">Are you able to provide onsite qualified safety representation?</span>
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="57%">
                                                                                                                <asp:Label ID="lblRepresentation" runat="server" RepeatDirection="Horizontal">                                                                                        
                                                                                                                </asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" class="sectionpad">
                                                                                                    <span id="spnContactNonUS" runat="server" style="display: none">
                                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv" width="40%">
                                                                                                                    If Yes, Please Specify Contact Name:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView" width="60%">
                                                                                                                    <asp:Label ID="lblRepresentationContactName" runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv">
                                                                                                                    Contact Phone Number:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label ID="lblRepresentationContactNumber" runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        &nbsp
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr style="page-break-before: always">
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="formhead">
                                                                            Vendor Qualification Form - References
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr>
                                                                                    <td class="searchhdrbarbold" colspan="2">
                                                                                        Project References
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" width="100%" style="padding: 5px;">
                                                                                        <table width="100%" cellpadding="0" cellspacing="0" id="tblProjectReference" runat="server">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="keyheader" colspan="2">
                                                                                                                Reference 1
                                                                                                                <asp:Label ID="Label2" runat="server"></asp:Label>
                                                                                                                &nbsp;&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="tdheight" colspan="2">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                N/A: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                <asp:CheckBox ID="chkReferencesNA1" runat="server" Enabled="false" />&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Please Explain For N/A: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Project Name: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                City: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Country:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                State: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Completion Date: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Approximate Contract Value $: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                General Contractor: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Please give a brief description of work completed: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Contact Name: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Contact Phone Number: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trowSpeaksEnglishOne" runat="server" style="display: none">
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Speaks English?
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="border-bottom: 1px solid #cccccc" class="style1">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="keyheader" colspan="2">
                                                                                                                Reference 2
                                                                                                                <asp:Label ID="Label1" runat="server"></asp:Label>
                                                                                                                &nbsp;&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="tdheight" colspan="2">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                N/A: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                <asp:CheckBox ID="chkReferencesNA2" runat="server" Enabled="false" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Please Explain For N/A: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Project Name: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                City: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Country:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                State: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Completion Date: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Approximate Contract Value $: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                General Contractor: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Please give a brief description of work completed: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Contact Name: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Contact Phone Number: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trowSpeaksEnglishTwo" runat="server" style="display: none">
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Speaks English?
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="border-bottom: 1px solid #cccccc" class="style1">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="keyheader" colspan="2">
                                                                                                                Reference 3
                                                                                                                <asp:Label ID="lblCount" runat="server"></asp:Label>
                                                                                                                &nbsp;&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="tdheight" colspan="2">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                N/A: &nbsp;
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:CheckBox ID="chkReferencesNA3" runat="server" Enabled="false" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Please Explain For N/A: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Project Name: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                City: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Country:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                State: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Completion Date: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Approximate Contract Value $: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                General Contractor: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv">
                                                                                                                Please give a brief description of work completed: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Contact Name: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Contact Phone Number: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trowSpeaksEnglishThree" runat="server" style="display: none">
                                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                                Speaks English?
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" width="100%" style="padding-left: 5px">
                                                                                        <asp:Repeater ID="rptProjectReferences" runat="server" OnItemDataBound="rptProjectReferences_ItemDataBound">
                                                                                            <HeaderTemplate>
                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td style="padding: 5px;">
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                            <tr>
                                                                                                                <td class="keyheader" colspan="2">
                                                                                                                    Reference
                                                                                                                    <asp:Label ID="lblCount" runat="server"></asp:Label>
                                                                                                                    &nbsp;&nbsp;&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="tdheight" colspan="2">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="trExplainHead" runat="server">
                                                                                                                <td class="bodytextbold_rightv">
                                                                                                                    N/A:
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:CheckBox ID="chkReferencesNA" runat="server" Enabled="false" Checked='<%#Convert.ToBoolean(Eval("NotAvailable"))%>' />
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="trExplain" runat="server">
                                                                                                                <td class="bodytextbold_rightv" width="35%" valign="top">
                                                                                                                    Please Explain For N/A:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label ID="lblExplanation" runat="server" TabIndex="0" Text='<%# Eval("Explanation") %>'></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                                    Project Name:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label ID="txtProjectNameOne" runat="server" TabIndex="0" Text='<%# Eval("ProjectName") %>'></asp:Label>
                                                                                                                    <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("PK_ProjectReferencesID") %>'>
                                                                                                                    </asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv">
                                                                                                                    City:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label ID="txtCity" CssClass="txtbox" runat="server" TabIndex="0" Text='<%# Eval("City") %>'></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv">
                                                                                                                    Country: &nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label ID="lblCountry" runat="server" CssClass="txtbox" TabIndex="0" Text='<%# Eval("CountryName") %>'></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv">
                                                                                                                    State:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label ID="lblState" CssClass="txtbox" runat="server" Text='<%# Eval("FK_State") %>'
                                                                                                                        TabIndex="0">
                                                                                                                    </asp:Label>
                                                                                                                    <asp:Label ID="lblOtherState" CssClass="txtbox" runat="server" Text='<%# Eval("OtherState") %>'
                                                                                                                        TabIndex="0" Visible="false">
                                                                                                                    </asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv">
                                                                                                                    Completion Date:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label ID="lblCompletionDateOne" CssClass="txtbox" runat="server" TabIndex="0"
                                                                                                                        Text='<%# Eval("CompletionDT") %>'></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                                    Approximate Contract Value $:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                                    <asp:Label ID="lblApproximateContractAmtOne" runat="server" Text='<%# Eval("ApproximateContract") %>'
                                                                                                                        TabIndex="0"></asp:Label>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv">
                                                                                                                    General Contractor:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                                    <asp:Label ID="lblContractorNameOne" runat="server" Text='<%# Eval("GeneralContractor") %>'
                                                                                                                        TabIndex="0"></asp:Label>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv">
                                                                                                                    Please give a brief description of work completed:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label ID="lblWorkCompletedOne" CssClass="txtbox" runat="server" Text='<%# Eval("WorkCompleted") %>'></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                                    Contact Name:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label ID="lblCtrContactNameOne" runat="server" TabIndex="0" Text='<%# Eval("ContactName") %>'></asp:Label>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                                    Contact Phone Number:
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label ID="lblMainPhnoOne" runat="server" TabIndex="0" Text='<%# Eval("ContactPhoneNumber") %>'></asp:Label>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="trowSpeaksEnglish" runat="server" style="display: none">
                                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                                    Speaks English?
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <asp:Label ID="lblQuestionOne" runat="server" TabIndex="0" Text='<%# Eval("IsSpeaksEnglishText") %>'></asp:Label>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trAdminView" runat="server" style="display: none">
                                                                                                    <td>
                                                                                                        <table cellpadding="3" cellspacing="3" width="100%" border="0">
                                                                                                            <tr>
                                                                                                                <td width="35%" class="left_border ">
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView">
                                                                                                                    <%# Eval("AddNote") %>
                                                                                                                    <asp:TextBox ID="txtNotes" runat="server" CssClass="txtboxmultilarge" TextMode="MultiLine"
                                                                                                                        Visible="false" Height="50px" Width="98%" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                                        onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                                                                        Text='<%# Eval("AddNote") %>'></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hdnStatus" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="tdheight">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%-- <tr>
                                                                                                    <td colspan="2" style="border-bottom: 1px solid #cccccc" class="style1">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>--%>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                </table></FooterTemplate>
                                                                                        </asp:Repeater>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td align="right">
                                                                                                    <span class="hide_print">
                                                                                                        <asp:ImageButton ID="imgExportexcelproject" runat="server" Visible="false" ImageUrl="~/Images/extoexcel.jpg" /></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="1%">
                                                                                    </td>
                                                                                    <td class="bodytextleft">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="34%">
                                                                                                    Have you ever completed any projects<br />
                                                                                                    with Haskell?
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="LblcompletePrjstatus" runat="server"></asp:Label>&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td class="bodytextbold" align="right">
                                                                                        <table width="66%" align="right" id="tblProjectName" runat="server">
                                                                                            <tr>
                                                                                                <td width="30%">
                                                                                                    Project Name
                                                                                                </td>
                                                                                                <td width="1%">
                                                                                                </td>
                                                                                                <td width="28%">
                                                                                                    Project Manager
                                                                                                </td>
                                                                                                <td width="1%">
                                                                                                </td>
                                                                                                <td width="7%" nowrap="nowrap">
                                                                                                    Year Completed
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="1%">
                                                                                    </td>
                                                                                    <td class="bodytextleft" align="right">
                                                                                        <asp:Repeater ID="rptAddProjectref" runat="server">
                                                                                            <HeaderTemplate>
                                                                                                <table width="66%" align="right">
                                                                                                    <tr>
                                                                                                        <td width="30%" class="bodytextbold">
                                                                                                            Project Name
                                                                                                        </td>
                                                                                                        <td width="1%" class="bodytextbold">
                                                                                                        </td>
                                                                                                        <td width="30%" class="bodytextbold">
                                                                                                            Project Manager
                                                                                                        </td>
                                                                                                        <td width="1%">
                                                                                                        </td>
                                                                                                        <td width="4%" class="bodytextbold" nowrap="nowrap">
                                                                                                            Year Completed
                                                                                                        </td>
                                                                                                    </tr>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        <%# Eval("ProjectName") %>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        <%#Eval("ProjectManager") %>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        <%#Eval("Year") %>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                </table></FooterTemplate>
                                                                                        </asp:Repeater>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <!--003-Sooraj-->
                                                                            <table id="tblSupplierCredit" runat="server" align="center" border="0" cellpadding="0"
                                                                                cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr>
                                                                                    <td class="searchhdrbarbold">
                                                                                        Supplier/Credit
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <%-- <tr>
                                                                                    <td class="subHeadtext">
                                                                                        &nbsp;&nbsp;&nbsp;Please provide us at least 3  Supplier/Credit references
                                                                                    </td>
                                                                                </tr>--%>
                                                                                <tr id="tdSupplierData" runat="server" style="display: none;">
                                                                                    <td style="padding: 5px;">
                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td class="keyheader" colspan="4" width="100%">
                                                                                                    Supplier/Credit 1
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="34%">
                                                                                                    Company Name:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="34%">
                                                                                                    Company Address:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="36%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextbold" width="5%">
                                                                                                    City:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="25%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="30%">
                                                                                                                Country:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="7%">
                                                                                                                State:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="10%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="12%">
                                                                                                                ZIP/Postal Code:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Contact Name:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Phone Number:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trowSupplierSpeaksEnglishOne" runat="server" style="display: none">
                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                    Speaks English?
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="keyheader" colspan="4">
                                                                                                    Supplier/Credit 2
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Company Name:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv" width="34%">
                                                                                                        Company Address:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" width="36%">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextbold" width="5%">
                                                                                                        City:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" width="25%">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" align="right">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="30%">
                                                                                                                Country:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="7%">
                                                                                                                State:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="10%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="12%">
                                                                                                                ZIP/Postal Code:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Contact Name:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Phone Number:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trowSupplierSpeaksEnglishTwo" runat="server" style="display: none">
                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                    Speaks English?
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="keyheader" colspan="4">
                                                                                                    Supplier/Credit 3
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Company Name:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="34%">
                                                                                                    Company Address:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="36%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv" width="5%">
                                                                                                    City:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="25%">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" align="right">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="30%">
                                                                                                                Country:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="7%">
                                                                                                                State:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="10%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="12%">
                                                                                                                ZIP/Postal Code:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Contact Name:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Phone Number:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trowSupplierSpeaksEnglishThree" runat="server" style="display: none">
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Speaks English?
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td id="SuplDetails" runat="server" style="display: none; padding: 5px 10px 0px 10px">
                                                                                        <asp:Repeater ID="rptsupl" runat="server" OnItemDataBound="rptSupl_ItemDataBound"
                                                                                            OnItemCommand="rptSupl_ItemCommand">
                                                                                            <HeaderTemplate>
                                                                                                <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td class="keyheader" colspan="4">
                                                                                                        Supplier/Credit
                                                                                                        <asp:Label ID="lblCount1" runat="server"></asp:Label>&nbsp
                                                                                                        <asp:HyperLink ID="hlReqSupReq" runat="server" Target="_blank" Text="(Reference Request)"
                                                                                                            CssClass=""></asp:HyperLink>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="tdheight" colspan="4">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv" width="35%">
                                                                                                        Company Name: &nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" colspan="3">
                                                                                                        <%# Eval("CompanyName")%>
                                                                                                        <asp:HiddenField ID="hdnSupplierId" runat="server" Value='<%# Eval("PK_SupplierId") %>' />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv">
                                                                                                        Contact Name: &nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" colspan="3">
                                                                                                        <%# Eval("ContactName")%>&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv" width="35%">
                                                                                                        Company Address: &nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" width="35%">
                                                                                                        <%# Eval("CompanyAddress")%>&nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextbold_rightv" width="5%">
                                                                                                        City:&nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" width="25%">
                                                                                                        <%# Eval("City")%>&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv" width="35%">
                                                                                                        Country:&nbsp;
                                                                                                    </td>
                                                                                                    <td colspan="3" width="65%">
                                                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                            <tr>
                                                                                                                <td class="bodytextleftView" width="15%">
                                                                                                                    <%# Eval("CountryName")%>&nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextbold_rightv" width="7%">
                                                                                                                    State:&nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView" width="16%">
                                                                                                                    <asp:Label ID="LblState" runat="server" Text='<%# Eval("Fk_State") %>'></asp:Label>&nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextbold" width="13%">
                                                                                                                    &nbsp; ZIP/Postal Code:&nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView" width="12%">
                                                                                                                    <%# Eval("ZipCode")%>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="trOther" runat="server" style="display: none">
                                                                                                                <td class="bodytextbold_rightv" width="65%" colspan="2">
                                                                                                                    Please Specify State
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" colspan="2">
                                                                                                                    <asp:Label ID="LblOtherState" runat="server" Text='<%# Eval("OtherState") %>'></asp:Label>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv">
                                                                                                        Phone Number: &nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" colspan="3">
                                                                                                        <%# Eval("Phone")%>&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="trowSupplierSpeaksEnglish" runat="server" style="display: none">
                                                                                                    <td class="bodytextbold_rightv">
                                                                                                        Speaks English?
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" colspan="3">
                                                                                                        <asp:Label ID="lblQuestionOne" runat="server" TabIndex="0" Text='<%# Eval("IsSpeaksEnglishText") %>'></asp:Label>&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--  /  <tr>
                                                                                                    <td valign="top" colspan="2">
                                                                                                        <table width="100%">
                                                                                                            <tr id="tdlnkl1" runat="server" style="display: none">
                                                                                                                <td class="bodytextbold_rightv">
                                                                                                                    Notes: &nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView ">
                                                                                                                    <asp:Label ID="Label55" runat="server">Notes</asp:Label>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>////--%>
                                                                                                <span class="hide_print">
                                                                                                    <tr>
                                                                                                        <td colspan="2" valign="top">
                                                                                                            <%--<span id="spannworkcompletedsupl" runat="server">--%>
                                                                                                            <table cellpadding="3" cellspacing="3" width="100%" border="0">
                                                                                                                <% if (Request.QueryString["user"] == "admin")
                                                                                                                   { %>
                                                                                                                <tr>
                                                                                                                    <td id="tdlblsupl" runat="server" width="14%" style="display: none" class="left_border hide_print">
                                                                                                                        <asp:LinkButton ID="tdlnksupl" runat="server" Text="Click to add notes" CommandName="cmdnamelnksupl"></asp:LinkButton>
                                                                                                                    </td>
                                                                                                                    <td id="tdtxtsupl" runat="server" width="86%" style="display: none; text-align: right;">
                                                                                                                        <asp:TextBox TextMode="MultiLine" ID="txtsuplnotes" Text='<%# Eval("AddNote") %>'
                                                                                                                            runat="server" Height="50px" Width="609px" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                                            onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <!-- TEMP REMOVED *** hlReqSup: Supplier Reference Request *** -->
                                                                                                    </tr>
                                                                                                    <% }%>
                                                                                                    <tr>
                                                                                                        <td colspan="2" valign="top">
                                                                                                            <table cellpadding="15" cellspacing="10" width="100%" border="0">
                                                                                                                <tr id="tdsuplnotetitle" runat="server" style="display: none">
                                                                                                                    <td width="33%" class="special" valign="top">
                                                                                                                        <asp:Label ID="lblusernotetitle3" runat="server" Text="Haskell Admin Notes:"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td id="tdsuplnotedesc" runat="server" class="bodytextleftView" width="67%">
                                                                                                                        &nbsp;
                                                                                                                        <asp:Label ID="lblsuplnotedesc" Text='<%# Eval("AddNote") %>' runat="server"></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    </table>
                                                                                                    <%--</span>--%>
                                                                                                    </td> </tr> </span>
                                                                                                <tr>
                                                                                                    <td class="tdheight">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" style="border-top: 1px solid #cccccc">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                </table></FooterTemplate>
                                                                                        </asp:Repeater>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right" style="padding-right: 10px">
                                                                                        <span class="hide_print">
                                                                                            <asp:ImageButton ID="imgExportexcelsupplier" runat="server" Visible="false" ImageUrl="~/Images/extoexcel.jpg" /></span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr style="page-break-before: always">
                                                            <td>
                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="formhead">
                                                                                        Vendor Qualification Form - Insurance & Bonding
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                            <tr id="ContactInformationHeader" runat="server">
                                                                                                <td class="Header" colspan="3">
                                                                                                    Contact Information
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="3" class="sectionpad">
                                                                                                    <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right" width="35%" valign="top">
                                                                                                                Broker / Company Name:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblBrokerorCompanyName" runat="server"></asp:Label>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right" valign="top">
                                                                                                                Insurance Carrier:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblInsuranceCarrier" runat="server"></asp:Label>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right" valign="top">
                                                                                                                Insurance Agent:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblInsuranceAgent" runat="server"></asp:Label>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right" valign="top">
                                                                                                                Agent Phone Number:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblAgentPhone" runat="server"></asp:Label>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                                            <tr>
                                                                                                <td class="Header" colspan="3">
                                                                                                    Aggregate Limits / Coverage
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="3">
                                                                                                    <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td colspan="2" class="hdrbold">
                                                                                                                General Liability
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right" width="36%">
                                                                                                                Each Occurance $:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblEachOccurances" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Product/Completed Operations Aggregate $:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblOPerations" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                General Aggregate $:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblGenralAggregate" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Is Policy "Occurrence Based"?
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblIsPolicy" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Is General Aggregate Limit "Per Project"?
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblIsGeneral" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="hdrbold" colspan="3">
                                                                                                                Additional Insured Endorsements
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="left_border" colspan="3">
                                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <span class="bodytextbold " width="64%">Will you provide additional insured endorsements
                                                                                                                                for ongoing and completed operations:</span>
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleftView" width="36%">
                                                                                                                            <asp:Label ID="lblInsured" runat="server"></asp:Label>&nbsp;
                                                                                                                        </td>
                                                                                                                        <td width="1%">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="trnotes" runat="server" style="display: none">
                                                                                                                        <td class="formtdrt" colspan="2">
                                                                                                                            Contractor shall be included as an additional insured under the CGL policy for both
                                                                                                                            ongoing and completed operations. ISO additional insured endorsements CG 20 10 (for
                                                                                                                            ongoing operations) and CG 20 37 (for completed operations) or substitute endorsements
                                                                                                                            providing equivalent coverage, will be attached to Subcontractor's CGL. The umbrella
                                                                                                                            insurance, if any, shall provide following form of additional insured coverage.
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="hdrbold" colspan="3">
                                                                                                                Umbrella/Excess Liability
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Do You Have Umbrella/Excess Liability?
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblUmbrellaExcess" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trUmbrella" runat="server" style="display: none">
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Umbrella Each Occurrence/Aggregate:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblUmbrella" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="trExcess" runat="server" style="display: none">
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Excess Each Occurrence/Aggregate:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblExcess" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="hdrbold" colspan="3">
                                                                                                                Auto
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Each Accident $:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblEachAccident" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="hdrbold" colspan="2">
                                                                                                                Workers Compensation And Employers' Liability
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Does WC Policy Meet Statutory Limits?
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblWCPolicy" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Employers Liability-Each Accident $:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblEmployees" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Employers Liability Disease-Each Employee $:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblDiseaseEmployee" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Employers Liability Disease-Policy Limit $:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblDiseasePolicy" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="hdrbold" colspan="2">
                                                                                                                Professional Liability
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Each Occurance $:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblEachProfessionalOccur" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <%--G. Vera 06/11/2021 - Added below--%>
                                                                                                         <tr>
                                                                                                            <td class="hdrbold" colspan="2">
                                                                                                                Pollution Liability
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Each Occurrence/Aggregate $:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblPollutionLiability" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <%--G. Vera 10/26/2012 - Added below--%>
                                                                                                        <tr>
                                                                                                            <td class="hdrbold" colspan="2">
                                                                                                                Marine Cargo Insurance
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right" valign="top">
                                                                                                                Marine Cargo/Inland Transit Insurance? &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblMarineCargoIns" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right" valign="top">
                                                                                                                Per Conveyance Limit USD: &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblMarineCargoConvLimit" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right" valign="top">
                                                                                                                Inland Transit? &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblMarineCargoInlandTransit" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right" valign="top">
                                                                                                                Storage? &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblMarineCargoStorage" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right" valign="top">
                                                                                                                Installation of Equipment? &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblMarineCargoInstallEquip" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                            <td width="1%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="tdheight" colspan="2">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--003-Sooraj-->
                                                                                        <table id="tblBondingAndSurety" runat="server" align="center" border="0" cellpadding="0"
                                                                                            cellspacing="0" width="100%" class="searchtableclass">
                                                                                            <tr>
                                                                                                <td class="Header" colspan="3">
                                                                                                    Bonding and Surety
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="3" class="sectionpad">
                                                                                                    <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right" valign="top" width="36%">
                                                                                                                Bonding/Surety Company Name:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblBonding" runat="server"></asp:Label>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Company Contact Name:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblCompanyContactName" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Contact Telephone Number:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblContactPhonenumber" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Total Bonding Capacity $:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblTotalBonding" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_right">
                                                                                                                Available Bonding Capacity $:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label ID="lblCurrentBondingcapacity" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                    </table>
                                                    <table id="tblAttached" runat="server" border="0" cellpadding="0" cellspacing="0"
                                                        width="100%">
                                                        <tr style="page-break-before: always">
                                                            <td>
                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                                <tr>
                                                                                    <td class="Header">
                                                                                        Attached Document files
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="left_border">
                                                                                                    <asp:GridView runat="server" ID="GridAttchView" OnRowCommand="GridAttchView_RowCommand"
                                                                                                        DataKeyNames="Pk_documentID" AllowSorting="True" AutoGenerateColumns="False"
                                                                                                        CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="0" Width="100%">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField>
                                                                                                                <ItemTemplate>
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextcenter" width="8px">
                                                                                                                            <%# Eval("Row")%>
                                                                                                                            .
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft" width="400px">
                                                                                                                            <asp:LinkButton ID="lnkdowload" runat="server" CommandName="Lnk" CssClass="hyplinks"
                                                                                                                                CommandArgument='<%#Eval("Filepath")%>'><%#Eval("FileName")%></asp:LinkButton>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                    </asp:GridView>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </td> </tr> </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
