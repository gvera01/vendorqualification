﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WelcomePage.aspx.cs" Inherits="Admin_WelcomePage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>

    <script language="javascript" type="text/javascript">
          window.onerror = ErrHndl;

          function ErrHndl()
          { return true; }
    </script>

    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache">

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>

    <style type="text/css">
        body
        {
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" >
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd"  >
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td height="150px">
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="bodytextheadbold">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    Welcome to Haskell's Vendor Management System Database												
                                                </td>
                                            </tr>
											<tr style="display: none">
												<td style="color: #800000; font-size: 14px;padding: 10PX 0px 0px 145px;font-family: Tahoma;" valign="top" >
													<p >
                                                                We will be conducting an upgrade to the Vendor Management System<br/>
																that will require the system to be offline starting <br/>
																January 10, 2013 at 3:30 p.m. (EST) <br/>
																until January 11, 2013 at 8:00 a.m. (EST)<br/><br/>

																Thank you for your continued patience while we improve our system.
                                                    </p>
												</td>
											</tr>
                                            <tr>
                                                <td height="150px">
                                                </td>
                                            </tr>
                                            <%--G. Vera 07/05/2012 - Added modal dialog--%>
                                            <tr>
                                                <td>
                                                    <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                                        PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg">
                                                    </Ajax:ModalPopupExtender>
                                                    <asp:HiddenField ID="lblHidden" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="modalPopupDiv" class="popupdivsmall" style="display: none">
                                                        <table cellpadding="5" cellspacing="0" border="0" width="350" align="center">
                                                            <tr>
                                                                <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                                    Message
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="paddingright">
                                                                    <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="ModalPopupTd">
                                                                    <asp:ImageButton ID="imbOK" ToolTip="Ok" runat="server" 
                                                                        ImageUrl="~/Images/ok.jpg" onclick="imbOK_Click" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
