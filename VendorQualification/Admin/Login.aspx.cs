﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Resources; 
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Principal;
using System.Net.Mail;
using System.Diagnostics;

/***************************************************************************************
 * 001 G. Vera - modifications to make this page work from outside the network.
 * 002 G. Vera 12/29/2016:  New Employee Role
 ***************************************************************************************/


public partial class Admin_Login : CommonPage
{
    private string _UseADAuthorization = ConfigurationSettings.AppSettings["UseADAuthorization"].ToString();
    RiskEvaluation riskEvaluation = new RiskEvaluation();
    bool isExpired = false;
 
    /// <summary>
    /// Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //G. Vera 07/03/2012 - Added code below
        if (!String.IsNullOrEmpty(Request.QueryString["timeout"]))
        {
            isExpired = true;
        }

        //Make session variables to null
        if (!Page.IsPostBack)
        {
            //if (Session["EmployeeId"] != null)
            //{
            //    Session.Abandon();
            //    Response.Write("<script>javascript: parent.opener=''; " +
            //                                     "parent.close();</script>");
            //}
            if (  (Session["RISKIDLOGOUT"]!=null && !string.IsNullOrEmpty(Session["RISKIDLOGOUT"].ToString()) )  && (Session["LOCKEDBYLOGOUT"]!=null && !string.IsNullOrEmpty(Session["LOCKEDBYLOGOUT"].ToString()) ) )
            {
                Int64 Lockedby = riskEvaluation.LockebByUser(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                if (Lockedby == Convert.ToInt64(Session["LOCKEDBYLOGOUT"]))
                {
                    riskEvaluation.UpdateStatus(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                    Session["RISKIDLOGOUT"] = null;
                }
            }
            Session["EmployeeId"] = null;
            Session["EmployeeName"] = null;
            Session["EmployeeTitle"] = null;
            Session["EmployeeNumber"] = null;
            Session["Access"] = null;
            Session["EmailUser"] = null;
        }
        if (_UseADAuthorization == "Yes")
        {
            CheckADforRedirect();
        }
    }

    /// <summary>
    /// Get Login details and assign to the session valiables
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbsubmit_Click(object sender, ImageClickEventArgs e)
    {
       //Validate the page
        Page.Validate("Login");

        //If valid Login and assign session variables and redirect to the welcome page
        if (Page.IsValid)
        {
            clsAdminLogin objLogin = new clsAdminLogin();
            if (!new clsADHelper().IsUserValid(txtUsername.Text.Trim(), txtPassword.Text.Trim()))
            {
                //lblMsg.Text = "<li>User name and password does not exists";
                lblMsg.Text = HttpContext.GetGlobalResourceObject("ErrorMessages", "CheckUserName").ToString();
                modalExtnd.Show();
            }
            else
            {

                //001 - moved and modified
                DataSet ds = new clsADHelper().GetUserDataSet(txtUsername.Text.Trim());
                string email = ds.Tables[0].Rows[0]["EmailAddress"].ToString();
                
                VMSDAL.GetEmployeeAccessResult[] objResult = objLogin.GetLoginDetails(email, null);

                if (objResult.Count() > 0)
                {
                    Session["EmployeeId"] = objResult[0].Pk_EmployeeId;
                    Session["EmployeeName"] = objResult[0].EmployeeName;
                    Session["EmployeeTitle"] = objResult[0].JobFunction;
                    Session["EmployeeNumber"] = objResult[0].EmployeeNumber;
                    Session["Access"] = objResult[0].Access;
                    Session["EmailUser"] =email;

                    if (Session["Access"].ToString() == "4")
                    {
                        lblMsg.Text = "<li>" + "Your Account has been Disabled. Kindly contact Haskell &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;administrator for further details (904) 357-4998" + "</li>";
                        modalExtnd.Show();
                    }
                    else
                    {
                        Response.Redirect("WelcomePage.aspx");
                    }
                }
                else
                {
                    lblMsg.Text = "Your Account has not been setup for VMS. Kindly contact Haskell &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;administrator for further details (904) 357-4998";
                    modalExtnd.Show();
                }
            }
        }
        //Else show the validation message
        else
        {
            lblMsg.Text = Errormsg();
            modalExtnd.Show();
        }
    }

    /// <summary>
    /// Included by the client
    /// </summary>
    protected void CheckADforRedirect()
    {

        //bool CheckAD = (HttpContext.Current.User.IsInRole("BUILTIN\\Administrators"));
	bool CheckAD = (HttpContext.Current.User.IsInRole("HASKELL\\Domain Users"));
	//bool CheckAD = true;
        if (CheckAD)
        {
            string userName = (string)(HttpContext.Current.User.Identity.Name.Split('\\')[1]).ToString();

        //Session["userName1"]  = (string)(HttpContext.Current.User.Identity.Name.Split('\\')[1]).ToString();

            clsADHelper oUser = new clsADHelper();
            bool bExists = false;
            bExists = oUser.UserExists(userName);
            if (bExists)
            {
                DataSet ds = new DataSet();
                ds = oUser.GetUserDataSet(userName);
                string userEmail = ds.Tables[0].Rows[0].ItemArray[15].ToString();

                clsAdminLogin objLogin = new clsAdminLogin();
                DataTable dt = objLogin.GetEmployeeInformation(userEmail);
                if (dt.Rows.Count == 0)
                {
                    lblMsg.Text = "<li>Vendor Qualification - User name and password does not exist.  Please contact the help desk at 791-4646 with this message.";
                    modalExtnd.Show();
                }
                else
                {
                    Session["EmployeeId"] = dt.Rows[0].ItemArray[0]; //.Pk_EmployeeId;
                    Session["EmployeeName"] = dt.Rows[0].ItemArray[1]; //.EmployeeName;
                    Session["EmployeeTitle"] = dt.Rows[0].ItemArray[4]; //.JobFunction;
                    Session["EmployeeNumber"] = dt.Rows[0].ItemArray[2]; //.EmployeeNumber;
                    Session["Access"] = dt.Rows[0].ItemArray[5]; //.Access;
		            Session["EmailUser"] = dt.Rows[0].ItemArray[3]; //Email;
                    Session["EmployeeRole"] = dt.Rows[0]["EmployeeRole"] as String; //002

                    if (Session["Access"].ToString() == "4")
                    {
                        lblMsg.Text = "<li>" + "Your Account has been Disabled. Kindly contact Haskell &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;administrator for further details (904) 357-4998" + "</li>";
                        modalExtnd.Show();
                    }
                    else
                    {
                        //G. Vera 07/03/2012 - Added parameter to welcome page.
                        if (!isExpired) Response.Redirect("WelcomePage.aspx");
                        else Response.Redirect("WelcomePage.aspx?timeout=1");
                    }
                }
            }
        }
    }

    /// <summary>
    /// Method to validate the page and return the validation list
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                errmsg += validator.ErrorMessage + "<Br/>";
            }
        }
        return errmsg;
    }


    /// <summary>
    /// G. Vera 07/03/2012 - Added
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbOK_Click(object sender, ImageClickEventArgs e)
    {
        isExpired = false;
        if (_UseADAuthorization == "Yes")
        {
            CheckADforRedirect();
        }

        modalExtnd.Hide();
    }
}