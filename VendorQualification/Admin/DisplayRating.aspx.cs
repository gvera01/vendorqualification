﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 09/15/2015:  Modification to allow loading from new VMS search application
 *
 ****************************************************************************************************************/
#endregion

public partial class Admin_DisplayRating : System.Web.UI.Page
{

    //Initialize object for classes
    clsRating objRating = new clsRating();
    Common objCommon = new Common();

    /// <summary>
    /// Create controls dyamically while page is been initialized
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        string StrPageRequest = Request.Form["__EVENTTARGET"];
        CreatePagingControls(true, StrPageRequest);
    }

    /// <summary>
    /// Page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //002 - check the FromPage query string
        ViewState["FromPage"] = (Request.QueryString["FromPage"] != null) ? Request.QueryString["FromPage"] : string.Empty;
        if (ViewState["FromPage"].ToString().ToLower() == "search")
        {
            objCommon.AuthenticateUser();
        }

        //Check for the session state if it is null redirect to the login page else continue
        if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
        {
            //001
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }

        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }

        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            Session["Page"] = null;
            //Check for the session state if it is null redirect to the login page else continue
            if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
            {
                //G. Vera 07/3/2012 - Added
                if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
                {
                    Response.Redirect("~/Admin/Login.aspx?timeout=1");
                }
                Response.Redirect("~/Admin/Login.aspx");
            }
            else
            {
                //If logged in person is an employee
                if (Convert.ToString(Session["Access"]) == "2")
                {
                    //Call method to display the employee menu
                    AdminMenu.DisplayMenu("Employee");
                }
                if (Request.UrlReferrer != null && Request.QueryString.Count > 0 && (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["FromPage"]))))
                {
                    //Get the Page url to know from which the place is been transmitted
                    Session["PreviousPageSRF"] = Request.UrlReferrer.ToString();
                }
                //Check Querystring count is greater than zero
                if (Request.QueryString.Count > 0)
                {
                    string SVendorID;
                    //Check Querystring of VendorId is not null
                    if ((!string.IsNullOrEmpty(Request.QueryString["VendorId"])))
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["display"]))
                            SVendorID = (ViewState["FromPage"].ToString().ToLower() != "search") ? objCommon.decode(Request.QueryString["VendorId"]) : Request.QueryString["VendorId"];  //002
                        else
                            SVendorID = (ViewState["FromPage"].ToString().ToLower() != "search") ? objCommon.encode(Request.QueryString["VendorId"]) : Request.QueryString["VendorId"]; //002
                            

                        //Create Instance for the class
                        clsRegistration objRegistration = new clsRegistration();
                        SVendorID = (ViewState["FromPage"].ToString().ToLower() != "search") ? objCommon.decode(Request.QueryString["VendorId"]) : Request.QueryString["VendorId"]; //002
                        //Call Method to get the vendor details
                        DAL.UspGetVendorDetailsByIdResult[] objResult = objRegistration.GetVendor(Convert.ToInt64(SVendorID));

                        //Check for count of records
                        if (objResult.Count() > 0)
                        {
                            //Assign VendorId to the variable
                            ViewState["VendorId"] = objResult[0].PK_VendorID;
                            //Assign company name to the vendorname 
                            lblVendorName.Text = objResult[0].Company;
                            if (!string.IsNullOrEmpty(Convert.ToString(Session["CurrentPage"])))
                            {
                                string[] CurrentPage = Convert.ToString(Session["CurrentPage"]).Split('~');
                                if (CurrentPage.Length == 2)
                                {
                                    hdnPage.Value = CurrentPage[0];
                                    ddlNumber.SelectedItem.Text = CurrentPage[1];
                                }
                                else
                                {
                                    hdnPage.Value = "1";
                                    ddlNumber.SelectedIndex = 0;
                                }
                            }
                            else
                            {
                                hdnPage.Value = "1";
                                ddlNumber.SelectedIndex = 0;
                            }

                            Session["CurrentPage"] = string.Empty;
                            //Get the count of records 
                            hdnCount.Value = Convert.ToString(objRating.GetCountOfRating(Convert.ToInt32(ViewState["VendorId"])));

                            //Calculate for number of pages as per the records per page
                            int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Value))));

                            //Display total number of pages
                            lblTotal.Text = Convert.ToString(hdnCount.Value);
                            lblTotalPage.Text = Convert.ToString(page);
                            Session["Page"] = Convert.ToString(page);
                            //Call method 
                            DisplayNextPrevious();
                            //Call method to bind record to the grid
                            Bind();
                        }
                        else
                        {
                            //if vendor count is zero or less than that display this message
                            lblDisplayMsg.Text = "Sorry no record found";
                            Contenttd.Style["Display"] = "none";
                        }
                    }
                    else
                    {
                        //If querystring count is greater than zero but vendor id is null, display this message
                        lblDisplayMsg.Text = "Sorry no record found";
                        Contenttd.Style["Display"] = "none";
                    }
                }
                else
                {
                    // if querystring count is less than and equal to zero, display this message
                    lblDisplayMsg.Text = "Sorry no record found";
                    Contenttd.Style["Display"] = "none";
                }
                if (Convert.ToString(Session["Access"]).Equals("2"))
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                { AdminMenu.DisplayMenu("Admin"); }
            }
        }

        string StrPageRequest;
        //Get id of the control that raises the event
        StrPageRequest = Request.Form["__EVENTTARGET"];
        CreatePagingControls(true, StrPageRequest);
    }

    /// <summary>
    /// Method to create controls dynamically for paging
    /// </summary>
    /// <param name="FromLoad"></param>
    /// <param name="StrPageRequest"></param>
    private void CreatePagingControls(bool FromLoad, string StrPageRequest)
    {
        if (FromLoad)
        {
            //Paging
            //1. Current page is assigned in hdnpage
            //2. Total number of Pages is in session["page"]
            //3. Check Whether Current page and total number of pages are not null 
            //Since the number-of-pages changes as per the Records-per-page count in dropdown we dynamically create controls there
            //controls created in dropdown change event will be replaced here and so first time teh event does not occur
            //to avoid that problem, get the id of the event raised by the control is not the dropdown of Records-per-page
            if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page"]))) && (string.IsNullOrEmpty(StrPageRequest) || (StrPageRequest.ToLower() != "ddlnumber")))
            {

                //Loop to create controls. condition to display only 5 number in a page
                for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
                {

                    // check i greater than zero and less than the total number of pages
                    if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                    {

                        //instantiate link button
                        LinkButton lnkPage = new LinkButton();
                        //Dynamically raise event for the controls created
                        lnkPage.Click += new EventHandler(lnkPage_Click);
                        //Assign text to the created controls
                        lnkPage.Text = Convert.ToString(i);
                        //if i value is equal to the current page displayed if so show that as a selected page
                        lnkPage.CssClass = i == Convert.ToInt32(hdnPage.Value) ? "Selectlnkcolor" : "lnkcolor";
                        //Add controls to the placeholder
                        plcCrntPage.Controls.Add(lnkPage);
                        //Call javascript method on client client to assign value to get the current page selected
                        lnkPage.Attributes.Add("onClick", "AssignValue(" + lnkPage.Text.Trim() + ")");
                    }
                }
            }
        }
        else
        {
            //Clear the controls in placeholder
            plcCrntPage.Controls.Clear();
            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {
                    LinkButton lnkPage = new LinkButton();
                    lnkPage.Click += new EventHandler(lnkPage_Click);
                    lnkPage.CssClass = i == Convert.ToInt32(hdnPage.Value) ? "Selectlnkcolor" : "lnkcolor";
                    plcCrntPage.Controls.Add(lnkPage);
                    lnkPage.Text = Convert.ToString(i);
                    lnkPage.Attributes.Add("onClick", "AssignValue(" + lnkPage.Text.Trim() + ")");
                }
            }
        }
    }

    /// <summary>
    /// Method to display next and previous buttons
    /// </summary>
    private void DisplayNextPrevious()
    {
        // Display next button
        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }

        //Display previous button
        imbPrevious.Visible = hdnPage.Value.Equals("1") ? false : true;
    }

    /// <summary>
    /// Change the css for the selected control
    /// </summary>
    private void StyleForSelectedControl(string CurrentPage)
    {
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                lnk1.CssClass = lnk1.Text.Trim() == CurrentPage ? "Selectlnkcolor" : "lnkcolor";
            }
        }
    }

    /// <summary>
    /// Event raises when clicking on the controls created dynamically for paging
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void lnkPage_Click(object sender, EventArgs e)
    {
        //Get the control that is clicked
        LinkButton Currentlnk = new LinkButton();
        Currentlnk = (LinkButton)sender;
        //Assign the text of the clicked control to hdnpage
        hdnPage.Value = Currentlnk.Text.Trim();
        //Call method to apply style for the current page
        StyleForSelectedControl(Currentlnk.Text.Trim());
        //Call method to display next and previous button
        DisplayNextPrevious();
        //Call method to bind gridview
        Bind();
    }

    /// <summary>
    /// Event raises when the dropdown changes to select records per page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPage.Value = "1";
        //calculation to get the total number of pages
        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Value))));
        Session["Page"] = Convert.ToString(page);
        lblTotalPage.Text = Convert.ToString(page);
        //Call method to create controls dynamically
        CreatePagingControls(false, "");
        //Display Previous and next button
        DisplayNextPrevious();
        //Apply CSS for the current page
        StyleForSelectedControl(hdnPage.Value);
        //Bind gridview
        Bind();
    }

    /// <summary>
    /// Event Raises when clicking on the next button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbNext_Click(object sender, ImageClickEventArgs e)
    {
        //Increament the current page value by 1 to go to the next page
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) + 1);
        lblTotalPage.Text = Convert.ToString(Session["Page"]);
        //Apply style for the selected page
        StyleForSelectedControl(hdnPage.Value);
        //Display next and previous buttons
        DisplayNextPrevious();
        //Bind Gridview
        Bind();
    }

    /// <summary>
    /// Method to Bind Gridview
    /// </summary>
    private void Bind()
    {
        if (Request.QueryString.Count > 0)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["VendorId"]))
            {
                //Call method to bind Gridview
                //002
                grdVendor.DataSource = (ViewState["FromPage"].ToString().ToLower() != "search") ? 
                    objRating.getRatingByVendor(Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.SelectedItem.Text.Trim()), Convert.ToInt64(objCommon.decode(Request.QueryString["VendorId"]))) :
                    objRating.getRatingByVendor(Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.SelectedItem.Text.Trim()), Convert.ToInt64(Request.QueryString["VendorId"]));
                grdVendor.DataBind();
                if (grdVendor.Rows.Count <= 0)
                {
                    lblDisplayMsg.Text = "Sorry no record found";
                    Contenttd.Style["Display"] = "None";
                }
                else
                {
                    lblDisplayMsg.Text = string.Empty;
                    Contenttd.Style["Display"] = "";
                    //If Loggedin person is an employee, Should not display the update and delete button
                    if (Convert.ToString(Session["Access"]) == "2")
                    {
                        if (grdVendor.Columns.Count >= 7)
                        {
                            grdVendor.Columns[7].Visible = false;
                        }
                    }
                }
            }
        }
        //Call method to Apply styles
        StyleForSelectedControl(hdnPage.Value);
    }

    /// <summary>
    /// Display the previous records
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrevious_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) - 1);
        lblTotalPage.Text = Convert.ToString(Session["Page"]);
        // Call method to apply styles
        StyleForSelectedControl(hdnPage.Value);
        //Call method to display previous and next button
        DisplayNextPrevious();
        //call method to bind gridview
        Bind();
    }

    /// <summary>
    /// Move to Previous Page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbBack_Click(object sender, ImageClickEventArgs e)
    {
        if (Convert.ToString(Session["PreviousPageSRF"]).Contains("SearchVendor.aspx"))
        {
            Response.Redirect("SearchVendor.aspx?user=admin");
        }
        else
        {
            Response.Redirect("SubContractorRating.aspx?page=insert");
        }
    }

    /// <summary>
    /// Redirect to insert rating page 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAddNew_Click(object sender, ImageClickEventArgs e)
    {
        //002
        Session["VendorAdminId"] = (ViewState["FromPage"].ToString().ToLower() != "search") ? 
            objCommon.decode(Request.QueryString["VendorId"]) :
            Request.QueryString["VendorId"];
        Session["CurrentPage"] = hdnPage.Value + "~" + ddlNumber.SelectedItem.Text;
        Response.Redirect("InsertRating.aspx");
    }

    /// <summary>
    /// Redirect to insert rating page for the sake of updating the records
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hdnRatingId = new HiddenField();
        hdnRatingId = (HiddenField)grdVendor.Rows[e.RowIndex].FindControl("hdnRatingId");
        Session["CurrentPage"] = hdnPage.Value + "~" + ddlNumber.SelectedItem.Text;
        //002
        Session["VendorAdminId"] = (ViewState["FromPage"].ToString().ToLower() != "search") ?
            objCommon.decode(Request.QueryString["VendorId"]) :
            Request.QueryString["VendorId"];
        Response.Redirect("InsertRating.aspx?RatingId=" + objCommon.encode(hdnRatingId.Value) + "&VendorId=" + 
            ((ViewState["FromPage"].ToString().ToLower() != "search") ? Request.QueryString["VendorId"] : objCommon.encode(Request.QueryString["VendorId"])) );  //002
    }

    /// <summary>
    /// Get Confirmation before deleteing the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdnRatingId = new HiddenField();
        hdnRatingId = (HiddenField)grdVendor.Rows[e.RowIndex].FindControl("hdnRatingId");
        Session["hdnRatingId"] = hdnRatingId.Value;
        //lblConfirm.Text = "Are you sure you want to delete the record?";
        modalConfirm.Show();
    }

    /// <summary>
    /// if ok is selected the record will be deleted
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgOK_Click(object sender, ImageClickEventArgs e)
    {
        //call method to delete the record
        objRating.DeleteRating(Convert.ToInt64(Session["hdnRatingId"]));
        hdnCount.Value = Convert.ToString(objRating.GetCountOfRating(Convert.ToInt32(ViewState["VendorId"])));
        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Value))));
        Session["Page"] = Convert.ToString(page);
        lblTotalPage.Text = Convert.ToString(page);
        lblTotal.Text = hdnCount.Value;
        //Call method to create controls dynamically
        CreatePagingControls(false, "");
        //Display Previous and next button
        DisplayNextPrevious();
        //Apply CSS for the current page
        StyleForSelectedControl(hdnPage.Value);
        //Call method to bind gridview
        Bind();
    }

    /// <summary>
    /// Redirect to view page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        HiddenField hdnRatingId = new HiddenField();
        hdnRatingId = (HiddenField)grdVendor.Rows[e.NewSelectedIndex].FindControl("hdnRatingId");
        if (!string.IsNullOrEmpty(Request.QueryString["VendorId"]))
        {
            Response.Redirect("ViewRating.aspx?RatingId=" + objCommon.encode(hdnRatingId.Value) + "&VendorId=" + 
                ((ViewState["FromPage"].ToString().ToLower() != "search") ? Request.QueryString["VendorId"] : objCommon.encode(Request.QueryString["VendorId"])));  //002
        }
    }

    protected void grdVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (Convert.ToString(Session["Access"]).Equals("2"))
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnEmployeeNumber = new HiddenField();
                hdnEmployeeNumber = (HiddenField)e.Row.FindControl("hdnEmployeeNumber");
                if (hdnEmployeeNumber.Value != Convert.ToString(Session["EmployeeNumber"]))
                {
                    ((LinkButton)e.Row.FindControl("lnkEdit")).Enabled = false;
                }
                else
                {
                    ((LinkButton)e.Row.FindControl("lnkEdit")).Enabled = true;
                }
            }
        }
    }
}