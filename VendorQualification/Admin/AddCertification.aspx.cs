﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 11/20/2012: VMS Stabilization Release 1.3
 * 
 *
 ****************************************************************************************************************/
#endregion

public partial class Admin_AddCertification : System.Web.UI.Page
{
    #region Declaration
    //instantiate the class
    clsCompanyCertification objCertification = new clsCompanyCertification();

    #endregion

    #region Page Events
    /// <summary>
    /// page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
        {
            //001
            Response.Redirect("~/Admin/Login.aspx?timeout=1");

        }
        else
        {
            if (!Page.IsPostBack)
            {
                //001
                Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");


                if (Convert.ToString(Session["Access"]) == "2")
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                {
                    AdminMenu.DisplayMenu("Admin");
                }
                //call bind method
                Bind();
                //call script methid
                CallScript();
                //To show the selected menu
                mnuMain.DisplayButton(6);
            }
            //display controls in postback
            DisplayControls();
        }
    }

    #endregion

    #region Button Events

    /// <summary>
    /// Submit 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSubmit_Click(object sender, ImageClickEventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            bool isDuplicate;
            if (string.IsNullOrEmpty(Convert.ToString(ViewState["CertificationID"])))
            {
                isDuplicate = objCertification.InsertCompanyCertification(txtCompanyCertification.Text.Trim(), chkisFederalSb.Checked, "Add", -1);
            }
            else
            {
                isDuplicate = objCertification.InsertCompanyCertification(txtCompanyCertification.Text.Trim(), chkisFederalSb.Checked, "Edit", Convert.ToInt32(ViewState["CertificationID"]));
                ViewState["CertificationID"] = string.Empty;
            }
            if (isDuplicate)
            {
                lblMsg.Text = "Certification Saved Successfully";
                trlnkAddCertification.Style["display"] = "";
                tblCertification.Style["display"] = "None";
                grdCertification.EditIndex = -1;    //002
                Bind();
                Clear();
            }
            else
            {
                lblMsg.Text = "Certification Already Exists";
            }
            modalExtnd.Show();
        }
        else
        {
            lblMsg.Text = Errormsg();
            modalExtnd.Show();
        }
    }

    /// <summary>
    /// Hide
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbClose_Click(object sender, ImageClickEventArgs e)
    {
        //Call to clear the controls
        grdCertification.EditIndex = -1;    //002
        Clear();
        Bind(); //002
    }

    /// <summary>
    /// Submit and add more
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSubmitAddMore_Click(object sender, ImageClickEventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            bool isDuplicate;
            if (string.IsNullOrEmpty(Convert.ToString(ViewState["CertificationID"])))
            {
                //Insert Certification
                isDuplicate = objCertification.InsertCompanyCertification(txtCompanyCertification.Text.Trim(), chkisFederalSb.Checked, "Add", -1);
            }
            else
            {
                //Insert Certification
                isDuplicate = objCertification.InsertCompanyCertification(txtCompanyCertification.Text.Trim(), chkisFederalSb.Checked, "Edit", Convert.ToInt32(ViewState["CertificationID"]));
                ViewState["CertificationID"] = string.Empty;
            }
            if (isDuplicate)
            {
                lblMsg.Text = "Certification Saved Successfully";
                tdHeader.InnerHtml = "Add Certification";
            }
            else 
            {
                lblMsg.Text = "Certification Already Exists"; 
            }
            modalExtnd.Show();
            grdCertification.EditIndex = -1;    //002
            //Bind the gridview
            Bind();
            //Clear the controls
            Clear();
        }
        else
        {
            lblMsg.Text = Errormsg();
            modalExtnd.Show();
        }
    }

    /// <summary>
    /// Add certification
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkAddCertification_Click(object sender, EventArgs e)
    {
        tdHeader.InnerHtml = "Add Certification";
    }

    /// <summary>
    /// Delete the certification
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgOK_Click(object sender, EventArgs e)
    {
        bool isDuplicate = objCertification.InsertCompanyCertification("", false, "Delete", Convert.ToInt32(ViewState["CertificationID"]));
        ViewState["CertificationID"] = string.Empty;
        Bind();
    }

    #endregion

    #region Gridview Events

    /// <summary>
    /// Edit the certification
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdCertification_RowEditing(object sender, GridViewEditEventArgs e)
    {
        hdnStatus.Value = "Edit";
        trlnkAddCertification.Style["display"] = "None";
        tblCertification.Style["display"] = "";
        tdHeader.InnerHtml = "Edit Certification";
        txtCompanyCertification.Text = ((Label)grdCertification.Rows[e.NewEditIndex].FindControl("lblCertification")).Text;
        chkisFederalSb.Checked = grdCertification.Rows[e.NewEditIndex].Cells[1].Text == "Yes" ? true : false;
        ViewState["CertificationID"] = grdCertification.DataKeys[e.NewEditIndex].Value;
    }

    /// <summary>
    /// Deleting the control
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdCertification_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        ViewState["CertificationID"] = grdCertification.DataKeys[e.RowIndex].Value;
        lblConfirm.Text = "<li>" + "Are you sure you want to delete the record?";
        modalConfirm.Show();
    }

    #endregion

    #region Other Methods

    /// <summary>
    /// Clear controls
    /// </summary>
    private void Clear()
    {
        txtCompanyCertification.Text = string.Empty;
        chkisFederalSb.Checked = false;
    }

    /// <summary>
    /// Validate the page and return thr error message
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                errmsg += "<li>" + validator.ErrorMessage + "</li>";
            }
        }
        return errmsg;

    }

    /// <summary>
    /// Display controls
    /// </summary>
    private void DisplayControls()
    {
        switch (hdnStatus.Value)
        {
            case "Add":
                trlnkAddCertification.Style["display"] = "none";
                tblCertification.Style["display"] = "";
                break;
            case "Edit":
                trlnkAddCertification.Style["display"] = "none";
                tblCertification.Style["display"] = "";
                break;
            default:
                trlnkAddCertification.Style["display"] = "";
                tblCertification.Style["display"] = "none";
                break;
        }
    }

    /// <summary>
    /// Call Script
    /// </summary>
    private void CallScript()
    {
        lnkAddCertification.Attributes.Add("onClick", "return VisibleControls('" + trlnkAddCertification.ClientID + "','" + tblCertification.ClientID + "','Add');");
        imbClose.Attributes.Add("onClick", "return VisibleControls('" + tblCertification.ClientID + "','" + trlnkAddCertification.ClientID + "','false');");
    }

    /// <summary>
    /// Bind grid
    /// </summary>
    private void Bind()
    {
        grdCertification.DataSource = objCertification.ListCompanyCertification();
        grdCertification.DataBind();
    }

    #endregion

}