﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TermsConditions.aspx.cs" Inherits="Common_TermsConditions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
        .style3
        {
            font-family: Arial;
            font-size: 14px;
            color: black;
            text-align: left;
        }
        .style4
        {
            font-family: Arial;
            font-size: 11px;
            color: black;
            text-align: left;
            width: 408px;
        }
    </style>
    
    <script type="text/javascript">
        function enableSubmit() {

            var title = window.document.getElementById("txtTermsConditionsTitle").value;
            var message = window.document.getElementById("txtTermsConditionsMessage").value;
            
            if (title == '' || message == '')
            {
                alert('Please provide title and message to continue.');
                return false;
            }
            else 
            {
                return true;
            }
        }
    
    </script>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbSubmit">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:AdminMaintananceMenu ID="mnuMain" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="formhead">
                                                    Vendor - Terms and Conditions</td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="95%">
                                                        <tr>
                                                            <td class="style3" align="center">
                                                                <asp:TextBox ID="txtTermsConditionsTitle" runat="server" Width="650px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: small; font-weight: normal; font-family: arial; color: #000080">
                                                                <asp:TextBox ID="txtTermsConditionsMessage" runat="server" Height="400px" 
                                                                    Width="650px" TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMessage" runat="server" CssClass="redtextleft" Width="100%"></asp:Label>
                                                            </td>
                                                            <%--<td class="formtdrt">
                                                                        <span class="mandatorystar">*</span>Comments
                                                                    </td>--%>
                                                        </tr>
                                                        </table>
                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="style4">
                                                                <asp:CheckBox ID="chkTermsConditionsForceRenew" runat="server" 
                                                                    Text="Force users to be prompted for Terms and Conditions Agreement" />
                                                                &nbsp;
                                                                </td>
                                                            <td style="text-align: right">
                                                                <asp:ImageButton ID="imbSubmit" runat="server" ImageUrl="~/images/save.jpg" OnClick="imbSubmit_Click"
                                                                    CausesValidation="false" onclientclick="enableSubmit()" />&nbsp;<asp:ImageButton 
                                                                    ID="imbClose" CausesValidation="false" runat="server" ImageUrl="~/Images/close.jpg"
                                                                    ToolTip="Close" OnClick="imbClose_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
