﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web;
using System.Collections;
using System.Web.UI.WebControls;
using VMSDAL;

#region Change Comments
/************************************************************************************************
 * 001 - G. Vera 06/27/2012 - Changes for Phase 3 Item 1.16 Printing Issues
 * 002 - G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21 & JIRA:VPI-15)
 * 003 - G. Vera 08/06/2012: VMS Stabilization Release 1.1 (JIRA:VPI-46)
 * 004 - G. Vera 
 ************************************************************************************************/
#endregion

public partial class Admin_ViewRating : System.Web.UI.Page
{
    //Instantiate the class
    clsRating objRating = new clsRating();
    Common objCommon = new Common();

    /// <summary>
    /// Page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //Check for the session varaible if null redirect to the login page
        if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
        {
            //002
            if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");
            }
            Response.Redirect("~/Admin/Login.aspx");
        }
        else
        {
            //002
            if (!IsPostBack)
            {
                Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");
            }
            //002 - Changed to check for the actual parameter name not the count
            if (!String.IsNullOrEmpty(Request.QueryString["RatingId"]))
            {
                //Get the details of the vendor id and display the corresponding details to the corresponding controls
                DAL.SRFProjectRating objGetRating = objRating.SelectRatingbyId(Convert.ToInt64(objCommon.decode(Request.QueryString["RatingId"])));
                if (objGetRating != null)
                {
                    //002 - addded
                    Session["VendorKey"] = objGetRating.FK_VendorID;
                    Session["RatingKey"] = objGetRating.PK_RatingID;

                    clsRegistration objRegistration = new clsRegistration();
                        DAL.UspGetVendorDetailsByIdResult[] obj1 = objRegistration.GetVendor(Convert.ToInt64(objGetRating.FK_VendorID));
                    if (obj1.Count() > 0)
                    {
                        lblSubcontractorname.Text = obj1[0].Company;
                    }

                    //004 - start
                    string typeOfCompany = new clsCompany().GetTypeOfCompany(Convert.ToInt64(objGetRating.FK_VendorID));

                    if (typeOfCompany == TypeOfCompany.DesignConsultant)
                    {
                        if (Request.UrlReferrer != null)
                        {
                            //Get the Page url to know from which the place is been transmitted
                            Session["Previous"] = Request.UrlReferrer.ToString().Trim();
                        }

                        string toUrl = "PrintConsultantRating.aspx";
                        if (Request.QueryString.Count > 0)
                        {
                            toUrl += "?" + "RatingId=" + objGetRating.PK_RatingID.ToString() + "&IsView=1";
                        }

                        this.Response.Redirect(toUrl);
                    }
                    //004 - end


                    lblRaterTitle.Text = objGetRating.EmployeeTitle;
                    clsAdminLogin objAdminLogin = new clsAdminLogin();
                    lblRaterName.Text = objAdminLogin.GetEmployeeName(Convert.ToString(objGetRating.FK_EmployeeNumber));
                    lblProjectNumber.Text = objGetRating.ProjectNumber;
                    lblProjectName.Text = objGetRating.ProjectName;
                    lblRatingDate.Text = Convert.ToDateTime(objGetRating.CreatedDT).ToShortDateString(); //DateTime.Now.ToShortDateString();
                    lblSubcontractValue.Text = objGetRating.FinalSubcontract;

                    string Comments = objGetRating.Comments;
                    //003 Commented all this
                    //Comments = Comments.Replace("\r\n", "<BR/>");
                    //Comments = Comments.Trim().Replace(" ", " &nbsp;").Trim();
                    //Comments = Comments.Replace(" ", " &nbsp;");
                    //003 lblComments.InnerHtml = Comments;
                    lblComments.Text = Comments;
                    lblComments.Rows = Convert.ToInt32(Math.Round(Convert.ToDouble(((lblComments.Text.Length / 85) + 1))));

                    string Scope = objGetRating.SubcontractorScope;
                    //003  Replace("\r\n", "<BR/>");
                    // 003 Scope = Scope.Replace(" ", "&nbsp;");
                    lblSubcontractorscope.Text = Scope;
                    lblSubcontractorscope.Rows = Convert.ToInt32(Math.Round(Convert.ToDouble(((lblSubcontractorscope.Text.Length / 85) + 1))));

                    if (objGetRating.InsuranceLevels == null)
                    {
                        lblLevels.Text = "Not Available";
                    }
                    else if (objGetRating.InsuranceLevels == true)
                    {
                        lblLevels.Text = "Yes";
                    }
                    else
                    {
                        lblLevels.Text = "No";
                    }
                    if (objGetRating.Subs_Vendors == null)
                    {
                        lblPay.Text = "Not Available";
                    }
                    else if (objGetRating.Subs_Vendors == true)
                    {
                        lblPay.Text = "Yes";
                    }
                    else
                    {
                        lblPay.Text = "No";
                    }
                    if (objGetRating.SpecialPayment == null)
                    {
                        lblPayment.Text = "Not Available";
                    }
                    else if (objGetRating.SpecialPayment == true)
                    {
                        lblPayment.Text = "Yes";
                    }
                    else
                    {
                        lblPayment.Text = "No";
                    }
                    imgCorporation.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.Cooperation)) + ".jpg";
                    imgFileManagement.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.FieldManagement)) + ".jpg";
                    imgFinancialCapability.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.FinancialCapability)) + ".jpg";
                    imgOrderClaim.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.ChangeOrdersClaims)) + ".jpg";
                    imgOverallPerformance.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.OverallPerformance)) + ".jpg";
                    imgProjectManagement.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.ProjectManagement)) + ".jpg";
                    imgQuality.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.QualityOfWork)) + ".jpg";
                    imgSafety.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.Safety)) + ".jpg";
                    imgScheduleControl.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.ScheduleControl)) + ".jpg";

                }
            }
        }
    }

    /// <summary>
    /// Go back to teh previous page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgback_Click(object sender, ImageClickEventArgs e)
    {
        //002 - Changed to check for the actual parameter name and not the count
        if (!String.IsNullOrEmpty(Request.QueryString["VendorId"]))
        {
            Response.Redirect("DisplayRating.aspx?VendorId=" + Request.QueryString["VendorId"]);
        }
    }

    /// <summary>
    /// 001 - Added
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrint_Click(object sender, ImageClickEventArgs e)
    {       
        //002 - changed
        Common objCommon = new Common();
        string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port +
                     "/Admin/PrintRating.aspx?RatingId=" + Session["RatingKey"] as string + "&VendorId=" + Session["VendorKey"] as string;
        string baseURI = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;

        objCommon.Print2PDF(url, baseURI, true, "Rating");  //GV
    }
}