﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SavedSearches.aspx.cs" Inherits="SavedSearches" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell </title>
    <script language ="javascript" type="text/javascript">
          window.onerror = ErrHndl;

          function ErrHndl()
          { return true; }
          function ClosePopUp() {
              document.getElementById("<%= hdnPage1.ClientID %>").value = "-1";
              $find('modalExtndSearch').hide();
          }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />

    <script src="../Script/jquery.js" type="text/javascript"></script>

    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>

    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
              margin-top:0px;
        }
    </style>

    

</head>
<body>
    <form id="form1" runat="server" defaultbutton="imgSearchButton">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <Haskell:AdminMaintananceMenu ID="mnuMain" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <asp:HiddenField ID="hdnFlag" runat="server" />
                                                 
                                                    <asp:HiddenField ID="hdnSaveSort" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    Save Vendor Search
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td class="formtdlt" width="40%" valign="top">
                                                                Employee Name
                                                            </td>
                                                            <td class="formtdrt" valign="top">
                                                                <asp:TextBox ID="txtEmployeeName" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox>
                                                                 <div id="listPlacement" style="height:100px; overflow:scroll;" ></div>
                                                                <Ajax:AutoCompleteExtender ID="txtEmployee" runat="server" BehaviorID="AutoCompleteEx"
                                                                    CompletionInterval="0" CompletionListElementID="listPlacement" EnableCaching="true"
                                                                    FirstRowSelected="True" MinimumPrefixLength="1" OnClientItemSelected="" ServiceMethod="GetEmployeeCompletionList"
                                                                    ServicePath="~/AutoComplete.asmx" TargetControlID="txtEmployeeName">
                                                                    <Animations>
                                                            <OnShow>
                                                              <Sequence>
                                                                <%-- Make the completion list transparent and then show it --%>
                                                                <OpacityAction Opacity="0" />
                                                                <HideAction Visible="true" />
                                                            
                                                                 <%--Cache the original size of the completion list the first time
                                                                  the animation is played and then set it to zero --%>
                                                                  <ScriptAction Script="
                                                                // Cache the size and setup the initial size
                                                                var behavior = $find('AutoCompleteEx');
                                                                if (!behavior._height) {
                                                                    var target = behavior.get_completionList();
                                                                    behavior._height = target.offsetHeight - 2;
                                                                    target.style.height = '0px';
                                                                  }" />
                                                            
                                                            <%-- Expand from 0px to the appropriate size while fading in --%>
                                                                <Parallel Duration=".4">
                                                                <FadeIn />
                                                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                                                                </Parallel>
                                                                </Sequence>
                                                                 </OnShow>
                                                                  <OnHide>
                                                          <%-- Collapse down to 0px and fade out --%>
                                                                 <Parallel Duration=".4">
                                                                 <FadeOut />
                                                                 <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                                                                 </Parallel>
                                                                 </OnHide>
                                                                    </Animations>
                                                                </Ajax:AutoCompleteExtender>
                                                                <asp:HiddenField ID="hdnMatch" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight" colspan="2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:ImageButton ID="imgSearchButton" runat="server" ImageUrl="~/Images/search.jpg"
                                                                    OnClick="imgSearchButton_Click" ToolTip="Search" />&nbsp;
                                                                <asp:ImageButton ID="imbexcel" ToolTip="Export To Excel" runat="server" ImageUrl="~/Images/extoexcel.jpg"
                                                                    OnClick="imbexcel_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodytextcenter" colspan="2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight" colspan="2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <asp:Label ID="lbldisplayInfo" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr id="trGriddetails" runat="server">
                                                <td>
                                                    <table width="100%">
                                                        <tr id="tr1" runat="server">
                                                            <td class="bodytextleft">
                                                                <asp:HiddenField ID="hdnPage" runat="server" />
                                                                Total Number Of Records:
                                                                <asp:Label ID="lblSearchTotal" runat="server"></asp:Label>
                                                            </td>
                                                            <td class="bodytextright">
                                                                Number Of Records Per Page:
                                                            </td>
                                                            <td width="5%">
                                                                <asp:DropDownList ID="ddlSearchNumber" runat="server" CssClass="ddlbox" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="ddlSearchNumber_SelectedIndexChanged">
                                                                    <asp:ListItem>10</asp:ListItem>
                                                                    <asp:ListItem>20</asp:ListItem>
                                                                    <asp:ListItem>30</asp:ListItem>
                                                                    <asp:ListItem>40</asp:ListItem>
                                                                    <asp:ListItem>50</asp:ListItem>
                                                                    <%--Included by SGS as per issue 5--%>
                                                                    <asp:ListItem>100</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="grdSaveSearch" runat="server" HeaderStyle-CssClass="grid_heading"
                                                        Width="100%" BorderWidth="0px" CellSpacing="1" CellPadding="5" GridLines="None"
                                                        AutoGenerateColumns="False" AllowSorting="true" OnSorting="grdSaveSearch_Sorting"
                                                        OnRowDataBound="grdSaveSearch_RowDataBound" OnRowDeleting="grdSaveSearch_RowDeleting"
                                                        OnRowUpdating="grdSaveSearch_RowUpdating" OnSelectedIndexChanging="grdSaveSearch_SelectedIndexChanging">
                                                        <RowStyle CssClass="listone" />
                                                        <AlternatingRowStyle CssClass="listtwo" />
                                                        <HeaderStyle CssClass="listheading" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Employee Name" HeaderStyle-CssClass="gridpad" HeaderStyle-ForeColor="White"
                                                                ItemStyle-CssClass="gridpad" HeaderStyle-Width="20%" SortExpression="EmployeeName">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblemployee" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                                                                    <asp:HiddenField ID="hdnHistoryId" runat="server" Value='<%# Bind("Id") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name" HeaderStyle-CssClass="gridpad" HeaderStyle-ForeColor="White"
                                                                ItemStyle-CssClass="gridpad" SortExpression="SearchedRecord">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lblname" runat="server" Text='<%# Bind("SearchedRecord") %>'
                                                                        CommandName="Select"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnEmployeeId" runat="server" Value='<%# Bind("Fk_EmployeeId") %>' />
                                                                    <asp:HiddenField ID="hdnCount" runat="server" Value='<%# Bind("Count") %>' />
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-ForeColor="White" HeaderText="Search Criteria Used"
                                                                HeaderStyle-CssClass="gridpad" SortExpression="SearchRoot" ItemStyle-CssClass="gridpad">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRoot" runat="server" Visible="false"></asp:Label>
                                                                    <asp:LinkButton ID="lblsearch" runat="server" Text='<%# Bind("SearchRoot") %>' CommandName="Update"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Date" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                ItemStyle-CssClass="gridpad" SortExpression="CreateDate">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkdelete" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="tdNextPrevious" runat="server" class="bodytextcenter" colspan="2">
                                                    Page&nbsp;<asp:ImageButton ID="imbSearchPrevious" runat="server" ImageUrl="~/Images/pre.jpg"
                                                        OnClick="imbSearchPrevious_Click" />
                                                    &nbsp;&nbsp;<asp:PlaceHolder ID="plcCrntPage1" runat="server"></asp:PlaceHolder>
                                                    of
                                                    <asp:Label ID="lblSearchTotalPage" runat="server"> </asp:Label>
                                                    <asp:ImageButton ID="imbSearchNext" runat="server" ImageUrl="~/Images/next1.jpg"
                                                        OnClick="imbSearchNext_Click" />
                                                    <asp:HiddenField ID="hdnSearchPage" runat="server" />
                                                    <asp:HiddenField ID="hdnCount" runat="server" />
                                                     <asp:HiddenField ID="hdnSort" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="hdnName" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                            CancelControlID="imbCancel" PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg"
                                            RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden" runat="server" />
                                                                    <asp:GridView ID="grdVendor" runat="server" 
                                            HeaderStyle-CssClass="grid_heading" Width="100%"
                                                                        BorderWidth="0px" CellSpacing="1" 
                                            CellPadding="5" GridLines="None" AutoGenerateColumns="False"
                                                                        OnRowDataBound="grdVendor_RowDataBound" OnSorting="grdVendor_Sorting" 
                                                                        AllowSorting="true">
                                                                        <RowStyle CssClass="listone" />
                                                                        <AlternatingRowStyle CssClass="listtwo" />
                                                                        <HeaderStyle CssClass="listheading" />
                                                                        <Columns>
                                                                          <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                        <asp:ImageButton ID="imgPaperClip" runat="server" ImageUrl="~/Images/paperclip.png" 
                                                                                CommandName="Edit" Enabled=false />
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                               <asp:TemplateField  HeaderStyle-CssClass="gridpad" 
                                                                                ItemStyle-CssClass="gridpad">
                                                                            
                                                                            <ItemTemplate>
                                                                            
                                                                                <asp:Image ID="imgTag" runat="server" />
                                                                                <div id="gridPopup1" class="floatDiv" runat="server">
                                                                                    <asp:Label class="HoverOnGrid" ID="lbtngrdPrint1" runat="server" />
                                                                                </div>
                                                                                <asp:HiddenField ID="hdnVendorId" runat="server" 
                                                                                    Value='<%# Bind("VendorId") %>' />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                          <asp:TemplateField HeaderText="VPP" HeaderStyle-ForeColor="White" 
                                                                                SortExpression="Tag" HeaderStyle-CssClass="gridpad" 
                                                                                ItemStyle-CssClass="gridpad">
                                                                            
                                                                            <ItemTemplate>
                                                                                <asp:Image ID="imgVPP" runat="server" ImageUrl="~/Images/greenbox.jpg" />
                                                                                
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Vendor Name" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad"
                                                                                HeaderStyle-ForeColor="White" SortExpression="VendorName">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblVendorName" runat="server" CssClass="bodytextbold_right" 
                                                                                        Text='<%# Bind("VendorName") %>'></asp:Label>
                                                                                    <br />
                                                                                    <div id="gridPopup" class="floatDiv" runat="server">
                                                                                        <asp:Label class="HoverOnGrid" ID="lbtngrdPrint" runat="server" 
                                                                                            Text="VQF has been submitted and is in the Admin list for approval. " />
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="VQF" HeaderStyle-CssClass="gridpad" 
                                                                                ItemStyle-CssClass="gridpad">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lnkVQF" runat="server" CommandName="Select">VQF</asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="&nbsp;Sub&nbsp;contractor&nbsp;Rating" ItemStyle-CssClass="gridpad"
                                                                                HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="25px">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lnkSRF" runat="server" CommandName="Update">SRF</asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Average Overall Rating" ItemStyle-CssClass="gridpad"
                                                                                SortExpression="subContractorrating" HeaderStyle-Width="70px" HeaderStyle-ForeColor="White"
                                                                                HeaderStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <Ajax:Rating ID="rtgStar" runat="server" CssClass="ratingStar" CurrentRating="0"
                                                                                        HorizontalAlign="Center" EmptyStarCssClass="Empty" FilledStarCssClass="Filled"
                                                                                        MaxRating="5" ReadOnly="true" StarCssClass="ratingItem" 
                                                                                        WaitingStarCssClass="Saved">
                                                                                    </Ajax:Rating>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad"
                                                                                SortExpression="StatusDesc" HeaderStyle-ForeColor="White">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblStatusDesc" runat="server" Text='<%# Bind("StatusDesc") %>' 
                                                                                        CssClass="greenbold"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="JDE" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad"
                                                                                SortExpression="JDE" HeaderStyle-ForeColor="White">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="JDER" runat="server" Text="" CssClass="greenbold"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Tracking Number" HeaderStyle-CssClass="gridpad" 
                                                                                ItemStyle-CssClass="gridpad">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblTrackingNumber" runat="server" 
                                                                                        Text='<%# Bind("TrackingNumber") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div id="modalPopupDiv" class="popupdiv" style="display: none" height="50">
                                            <table cellpadding="5" cellspacing="0" border="0" width="350" align="center">
                                                <tr>
                                                    <td class="searchhdrbarbold">
                                                        Message
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextcenter">
                                                        <asp:Label ID="lblValidation" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextcenter">
                                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextcenter">
                                                        &nbsp;
                                                        <asp:ImageButton ID="imbCancel" runat="server" ImageUrl="~/Images/cancel.jpg" TabIndex="5" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <Ajax:ModalPopupExtender ID="modalExtndSearch" runat="server" TargetControlID="hdnSearch"
                                             PopupControlID="SeachDiv" BackgroundCssClass="popupbg"
                                            RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="hdnSearch" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Panel ID="SeachDiv" runat="server" class="popupspc" Style="display: none" Height="500"
                                            Width="775" ScrollBars="Both">
                                            <table cellpadding="5" cellspacing="0" border="0" width="100%" align="center">
                                                <tr>
                                                    <td class="searchhdrbarbold">
                                                        Saved Searches
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextcenter">
                                                        <asp:Label ID="Label1" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextcenter">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="Totaltd" runat="server">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr id="trGrid" runat="server">
                                                                            <td class="bodytextleft" valign="middle">
                                                                                Total Number Of Records:
                                                                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                            </td>
                                                                            <td class="bodytextright" valign="middle">
                                                                                Number Of Records Per Page:
                                                                            </td>
                                                                            <td class="bodytextright" width="7%">
                                                                                <asp:DropDownList ID="ddlNumber" runat="server" CssClass="ddlbox" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="ddlNumber_SelectedIndexChanged">
                                                                                    <asp:ListItem>10</asp:ListItem>
                                                                                    <asp:ListItem>20</asp:ListItem>
                                                                                    <asp:ListItem>30</asp:ListItem>
                                                                                    <asp:ListItem>40</asp:ListItem>
                                                                                    <asp:ListItem>50</asp:ListItem>
                                                                                    <%--Included by SGS as per issue 5--%>
                                                                                    <asp:ListItem>100</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    &nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class=" popupdivcl bodytextcenter" id="pagingtd" runat="server">
                                                                    Page&nbsp;&nbsp;<asp:ImageButton ID="imbPrevious" runat="server" ImageUrl="~/Images/pre.jpg"
                                                                        OnClick="imbPrevious_Click" />
                                                                    <asp:PlaceHolder ID="plcCrntPage" runat="server"></asp:PlaceHolder>
                                                                    of
                                                                    <asp:Label ID="lblTotalPage" runat="server"> </asp:Label>
                                                                    <asp:ImageButton ID="imbNext" runat="server" ImageUrl="~/Images/next1.jpg" OnClick="imbNext_Click" />
                                                                    <asp:HiddenField ID="hdnCount1" runat="server" />
                                                                    <asp:HiddenField ID="hdnPage1" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="popupdivcl bodytextcenter">
                                                                    &nbsp;
                                                                    <asp:ImageButton ID="imbSearchCancel" ToolTip="Cancel" runat="server" ImageUrl="~/Images/cancel.jpg"
                                                                        TabIndex="5" />
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <Ajax:ModalPopupExtender ID="modalExtnd1" runat="server" TargetControlID="lblHidden1"
                                            PopupControlID="modalPopupDiv1" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div id="modalPopupDiv1" class="popupdivcl" style="display: none">
                                            <table cellpadding="5" cellspacing="0" border="0" width="400" align="center">
                                                <tr>
                                                    <td class="searchhdrbarbold" colspan="2">
                                                        Message
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight" colspan="2">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="errormsg left_border" colspan="2">
                                                        The Vendor Number for SGS is already matched by the Administrator as VR0001.<br />
                                                        Would you like to proceed with this record?
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ModalPopupTd" colspan="2">
                                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/yes.jpg" TabIndex="4" />
                                                        &nbsp;
                                                        <asp:ImageButton ID="ImbNo" runat="server" ImageUrl="~/Images/no.jpg" TabIndex="5" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd2" runat="server" TargetControlID="HiddenField3"
                                            PopupControlID="modalPopupDiv2" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="HiddenField3" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="modalPopupDiv2" runat="server" Width="900" Height="500" ScrollBars="Both"
                                            class="popupspc" Style="display: none">
                                            <table cellpadding="0" cellspacing="0" border="0" width="900">
                                                <tr>
                                                    <td class="searchhdrbarbold" width="100%">
                                                        Search Criteria Used
                                                    </td>
                                                </tr>
                                                <tr id="VendorNameHead" runat="server">
                                                    <td class="formhead ">
                                                        &nbsp; &nbsp;Vendor Name
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px">
                                                        <table id="tblVendor" runat="server">
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Vendor Name from Vendor Database:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbloName" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Vendor Number from E1 / JDE:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblVendorNumber" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trTrackingNumber" runat="server">
                                                                <td class="bodytextbold_right">
                                                                    Tracking Number:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblTrackingNumber" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Federal Tax ID / Business ID: 
                                                                    <%--005-Sooraj--%>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblFederalTaxId" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 07/20/2012 - Added--%>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    First Name:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 07/20/2012 - Added--%>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Last Name:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblLastName" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 07/20/2012 - Added--%>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Email:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trVendorStatus" runat="server">
                                                                <td class="bodytextbold_right">
                                                                    Status:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 07/20/2012 - Added--%>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    BIM (Y/N)?
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblBIM" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="trscopes" runat="server">
                                                    <td class="formhead">
                                                        &nbsp; &nbsp;Scopes of Work
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px">
                                                        <asp:Table ID="tblTradeInfo" runat="server" Width="100%">
                                                            <asp:TableRow ID="trRow1" runat="server">
                                                                <asp:TableCell ID="tdCell1" runat="server" VerticalAlign="Top" Width="50%"></asp:TableCell>
                                                                <asp:TableCell ID="tdCell2" runat="server" VerticalAlign="Top" Width="50%"></asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="trGeographicalLocations" runat="server">
                                                    <td class="formhead">
                                                        &nbsp; &nbsp;Geographical Locations
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px">
                                           
                                                        <asp:DataList ID="dlRegions" runat="server" RepeatColumns="4" RepeatDirection="Horizontal"
                                                            Width="100%" RepeatLayout="Table" ItemStyle-Wrap="true" ItemStyle-Width="150px"
                                                            ItemStyle-VerticalAlign="Top" AlternatingItemStyle-VerticalAlign="Top" OnItemDataBound="dlRegions_ItemDataBound">
                                                              <HeaderTemplate>
                                                                                                        <asp:Label ID="Label1" runat="server" CssClass="tree_head1" Text="United States of America"></asp:Label>
                                                                                                    </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblName" runat="server" CssClass="tree_head" Text='<%# Eval("StateCode") %>'></asp:Label>
                                                                <br />
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:Label ID="lstRegions" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:DataList>

                                                         <br />
                                                                                 <asp:DataList ID="dlContinents" runat="server" RepeatColumns="4" ItemStyle-CssClass="pad_left"
                                                                                    AlternatingItemStyle-CssClass="pad_left" RepeatDirection="Horizontal" Width="100%"
                                                                                    RepeatLayout="Table" ItemStyle-Wrap="true" ItemStyle-Width="150px" ItemStyle-VerticalAlign="Top"
                                                                                    AlternatingItemStyle-VerticalAlign="Top"
                                                                                   OnItemDataBound="dlContinents_ItemDataBound" >
                                                                                   <%-- <HeaderTemplate>
                                                                                        <span class="text">Continents</span>
                                                                                    </HeaderTemplate>--%>
                                                                                    <ItemTemplate>
                                                                                        
                                                                                        <asp:Label ID="lblContinent" runat="server" CssClass="tree_head1" Text='<%# Eval("ContinentName") %>'></asp:Label>
                                                                                        <div style="padding-left: 15px;">
                                                  <asp:Label ID="lstCountries" runat="server" ></asp:Label>
                                                                                        </div>
                                                                                   
                                                                                    </ItemTemplate>
                                                                                </asp:DataList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="trTypesOfProjects" runat="server">
                                                    <td class="formhead">
                                                        &nbsp; &nbsp;Types of Projects
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px">
                                                        <asp:DataList ID="dlProjects" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                                                            RepeatColumns="3" RepeatDirection="Horizontal" RepeatLayout="Table">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblProjects" runat="server" Text='<%# Eval("ProjectName") %>'></asp:Label></ItemTemplate>
                                                        </asp:DataList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                  <tr id="trCertification" runat="server">
                                                    <td class="formhead">
                                                        &nbsp; &nbsp;Company Certification
                                                    </td>
                                                </tr>
                                                <%--G. Vera 10/30/2012 -Added--%>
                                                <tr>
                                                    <td style="padding: 15px">
                                                        <asp:Literal ID="lblSavedOtherProjects" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px">
                                                        <table id="tblCertification" runat="server">
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Certification: 
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblCertification" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Federal SB:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblFederalSB" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 10/30/2012 - Added--%>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Industry Certifications:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblSavedISO" runat="server">
                                                                                                                       
                                                                    </asp:Label>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblSavedQS" runat="server">
                                                                                                                       
                                                                    </asp:Label>&nbsp;
                                                                </td>
                                                            </tr>
                                                           
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ModalPopupTd">
                                                        <asp:ImageButton ID="imgClosePopup" ToolTip="Close" runat="server" ImageUrl="~/Images/close.jpg"
                                                            TabIndex="5" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <Ajax:ModalPopupExtender ID="modalConfirm" runat="server" TargetControlID="hdnModal"
                                            PopupControlID="modalDiv" BackgroundCssClass="popupbg" CancelControlID="imbCancel"
                                            RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="hdnModal" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="sectionpad">
                                        <div id="modalDiv" class="popupdiv" style="display: none">
                                            <table cellpadding="0" cellspacing="0" border="0" width="300">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="Td1">
                                                        Confirm
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        <asp:Label ID="lblConfirm" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextcenter">
                                                        <asp:ImageButton ID="imbOk" runat="server" ImageUrl="~/Images/ok.jpg" OnClick="imgOK_Click"
                                                            ToolTip="Ok" />
                                                        &nbsp;
                                                        <asp:ImageButton ID="imbCanceldelete" runat="server" ImageUrl="~/Images/cancel.jpg"
                                                            ToolTip="Cancel" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="Footer" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>

<script language="javascript" type="text/javascript">
    function AssignValue(hdnflag) {
        document.getElementById(hdnflag).value = "1";
    }
    function GetValue(i, ctrl) {
        var ctrl1 = document.getElementById(ctrl);
        ctrl1.value = i;
    }
    function ShowPopup(panel) {
        var pnl = document.getElementById(panel);
        pnl.style.display = "Block";
    }
    //Hides DIV popup commands for gridview
    function HidePopup(panel) {
        var pnl = document.getElementById(panel);
        pnl.style.display = "none";
    }
    function test() {
        return false;
    }
</script>

