﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResetPassword.aspx.cs" Inherits="Admin_ResetPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <script src="../Script/jquery.js" type="text/javascript"></script>
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <script language="javascript" type="text/javascript">
        window.history.go(1);
        function ChangeUpper(txt) {
            document.getElementById(txt).value = document.getElementById(txt).value.toUpperCase();
        } 
    </script>
    <style type="text/css">
        body
        {
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbsubmit">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" />
                                    </td>
                                    <td class="contenttd">
                                        <Haskell:AdminMaintananceMenu ID="mnuMain" runat="server" />
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextbold_right">
                                                    <asp:LinkButton ID="inkprofilereset" CssClass="scopelink" runat="server" Text="Profile Reset"
                                                        PostBackUrl="~/Admin/ResetPassword.aspx"></asp:LinkButton>&nbsp;|&nbsp;
                                                    <asp:LinkButton ID="lnkAllowDuplicateTaxID" CssClass="scopelink" runat="server" Text="Allow Duplicate TaxID"
                                                        PostBackUrl="~/Admin/DuplicateTaxID.aspx"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:UpdatePanel ID="upnlPassword" runat="server">
                                                        <ContentTemplate>
                                                            <table width="100%" class="searchtableclass" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="searchhdrbarbold" colspan="2">
                                                                        Profile Reset
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdheight" colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt" width="35%" valign="top">
                                                                        <span class="mandatorystar">*</span>Vendor Name
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <%--                                             <asp:DropDownList ID="ddlName" runat="server" CssClass="ddlbox" AutoPostBack="True"
                                                                            OnSelectedIndexChanged="ddlName_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                        <asp:CustomValidator ID="cusVendorName" Display="None" runat="server" OnServerValidate="cusVendorName_ServerValidate"></asp:CustomValidator>--%>
                                                                        <asp:TextBox ID="txtVendor" runat="server" CssClass="ajaxtxtbox" AutoPostBack="true"
                                                                            MaxLength="60" OnTextChanged="ddlName_SelectedIndexChanged"></asp:TextBox>
                                                                        <br />
                                                                        <div id="listPlacement" style="height: 100px; overflow: scroll;">
                                                                        </div>
                                                                        <asp:RequiredFieldValidator EnableClientScript="false" ID="reqvVendorName" runat="server"
                                                                            Display="None" ControlToValidate="txtVendor" ErrorMessage="Please enter vendor name"></asp:RequiredFieldValidator>
                                                                        <Ajax:AutoCompleteExtender ID="autoComplete1" runat="server" BehaviorID="AutoCompleteEx"
                                                                            CompletionInterval="0" CompletionListElementID="listPlacement" EnableCaching="true"
                                                                            FirstRowSelected="True" MinimumPrefixLength="1" OnClientItemSelected="" ServiceMethod="GetCompletionList"
                                                                            ServicePath="~/AutoComplete.asmx" TargetControlID="txtVendor">
                                                                            <Animations>
                                                            <OnShow>
                                                              <Sequence>
                                                                <%-- Make the completion list transparent and then show it --%>
                                                                <OpacityAction Opacity="0" />
                                                                <HideAction Visible="true" />
                                                            
                                                                 <%--Cache the original size of the completion list the first time
                                                                  the animation is played and then set it to zero --%>
                                                                  <ScriptAction Script="
                                                                // Cache the size and setup the initial size
                                                                var behavior = $find('AutoCompleteEx');
                                                                if (!behavior._height) {
                                                                    var target = behavior.get_completionList();
                                                                    behavior._height = target.offsetHeight - 2;
                                                                    target.style.height = '0px';
                                                                  }" />
                                                            
                                                            <%-- Expand from 0px to the appropriate size while fading in --%>
                                                                <Parallel Duration=".4">
                                                                <FadeIn />
                                                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                                                                </Parallel>
                                                                </Sequence>
                                                                 </OnShow>
                                                                  <OnHide>
                                                          <%-- Collapse down to 0px and fade out --%>
                                                                 <Parallel Duration=".4">
                                                                 <FadeOut />
                                                                 <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                                                                 </Parallel>
                                                                 </OnHide>
                                                                            </Animations>
                                                                        </Ajax:AutoCompleteExtender>
                                                                        <asp:HiddenField ID="hdnVendorId" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt">
                                                                        <span class="mandatorystar">*</span>Company
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:TextBox ID="txtCompany" runat="server" CssClass="txtboxupper" MaxLength="60"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator EnableClientScript="false" ID="reqvCompany" runat="server"
                                                                            Display="None" ControlToValidate="txtCompany" ErrorMessage="Please enter company"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt" valign="middle" width="40%">
                                                                        <span class="mandatorystar">*</span>Country
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:DropDownList ID="ddlCountry" runat="server" Width="150px" CssClass="ddlbox"
                                                                            OnChange="javascript:ShowHide(this.value,'trUSA','trOther')">
                                                                            <asp:ListItem Text="USA" Value="USA"></asp:ListItem>
                                                                            <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trUSA" runat="server">
                                                                    <td class="formtdlt">
                                                                        <span class="mandatorystar">*</span>U.S. Tax ID
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:TextBox ID="txtTax1" runat="server" CssClass="txtboxtosmall" MaxLength="2"></asp:TextBox>
                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtTax" runat="server" TargetControlID="txtTax1"
                                                                            FilterType="Numbers">
                                                                        </Ajax:FilteredTextBoxExtender>
                                                                        <asp:TextBox ID="txtTax2" runat="server" CssClass="txtboxmedium" MaxLength="7"></asp:TextBox>
                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtTax1" runat="server" TargetControlID="txtTax2"
                                                                            FilterType="Numbers">
                                                                        </Ajax:FilteredTextBoxExtender>
                                                                        <asp:HiddenField ID="hdnTax" runat="server" />
                                                                        <asp:CustomValidator ID="Cusfederaltax" runat="server" Display="None" ValidationGroup="VendorReg"
                                                                            EnableClientScript="false" OnServerValidate="Cusfederaltax_ServerValidate"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trOther" runat="server">
                                                                    <td class="formtdlt" valign="middle">
                                                                        <span class="mandatorystar">*</span>Business ID
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:TextBox ID="txtBusinessTax" runat="server" MaxLength="50" CssClass="txtbox"></asp:TextBox>
                                                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtBusinessTax"
                                                                            FilterType="Numbers,UppercaseLetters, LowercaseLetters">
                                                                        </Ajax:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt">
                                                                        <span class="mandatorystar">*</span>First Name
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:TextBox ID="txtFirstName" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator EnableClientScript="false" ID="reqvFirstName" runat="server"
                                                                            Display="None" ControlToValidate="txtFirstName" ErrorMessage="Please enter first name"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt">
                                                                        <span class="mandatorystar">*</span>Last Name
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:TextBox ID="txtLastName" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator EnableClientScript="false" ID="reqvLastName" runat="server"
                                                                            Display="None" ControlToValidate="txtLastName" ErrorMessage="Please enter last name"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt">
                                                                        <span class="mandatorystar">*</span>E-mail
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:TextBox CssClass="txtbox" ID="txtEmail" runat="server" MaxLength="100"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator EnableClientScript="false" ID="revPEmail" runat="server"
                                                                            Display="None" ErrorMessage="Please enter vaild e-mail" ControlToValidate="txtEmail"
                                                                            SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator EnableClientScript="false" ID="reqvemail" runat="server"
                                                                            Display="None" ControlToValidate="txtEmail" ErrorMessage="Please enter email address"></asp:RequiredFieldValidator>
                                                                        <asp:HiddenField ID="hdnemail" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt">
                                                                        <span class="mandatorystar">*</span>New Password
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:TextBox ID="txtNewPassword" CssClass="txtbox" runat="server" TextMode="Password"
                                                                            MaxLength="20"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnPassword" runat="server" />
                                                                        <asp:RequiredFieldValidator ID="reqNewPassword" ControlToValidate="txtNewPassword"
                                                                            Display="None" runat="server" SetFocusOnError="true" ErrorMessage="Please enter new password"
                                                                            EnableClientScript="false">
                                                                        </asp:RequiredFieldValidator>
                                                                        <asp:CustomValidator ID="custNewPassword" runat="server" Display="None" EnableClientScript="false"
                                                                            OnServerValidate="custNewPassword_ServerValidate"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt">
                                                                        <span class="mandatorystar">*</span>Confirm Password
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:TextBox ID="txtCPassword" CssClass="txtbox" runat="server" TextMode="Password"
                                                                            MaxLength="20"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="reqCPassword" EnableClientScript="false" ControlToValidate="txtCPassword"
                                                                            Display="None" runat="server" SetFocusOnError="true" ErrorMessage="Please enter confirm password">
                                                                        </asp:RequiredFieldValidator>
                                                                        <asp:CompareValidator ID="cmpPassword" runat="server" EnableClientScript="false"
                                                                            ControlToCompare="txtNewPassword" ControlToValidate="txtCPassword" Display="None"
                                                                            ErrorMessage="Password and confirm password should be same"></asp:CompareValidator>
                                                                        <asp:CustomValidator ID="custPassword" runat="server" Display="None" OnServerValidate="custPassword_ServerValidate"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:ImageButton ID="imbsubmit" runat="server" ImageUrl="~/images/submit.jpg" CausesValidation="False"
                                                                            ValidationGroup="VendorReg" OnClick="imbsubmit_Click" ToolTip="Submit" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                                                            PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                                                        </Ajax:ModalPopupExtender>
                                                                        <asp:HiddenField ID="lblHidden" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div id="modalPopupDiv" class="popupdiv" style="display: none">
                                                                            <table cellpadding="5" cellspacing="0" border="0" width="350">
                                                                                <tr>
                                                                                    <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                                                        Message
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight popupdivcl">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="popupdivcl">
                                                                                        <asp:Label ID="lblErrMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight popupdivcl">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextcenter popupdivcl">
                                                                                        <input type="button" value="OK" title="Ok" class="ModalPopupButton" onclick="$find('modalExtnd').hide();" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight popupdivcl">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

    <script type="text/javascript" language="javascript">
        function ShowHide(val, usaPnl, otherPnl) {

            var trOther = document.getElementById(otherPnl);
            var trUSA = document.getElementById(usaPnl);

            if (val == 'Other') {
                trUSA.style.display = "none";
                trOther.style.display = "";
            }
            else {
                trOther.style.display = "none";
                trUSA.style.display = "";
            }
        }
    </script>
</body>
</html>
