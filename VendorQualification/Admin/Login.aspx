﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Admin_Login"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>

    <script language="javascript" type="text/javascript">
          window.onerror = ErrHndl;

          function ErrHndl()
          { return true; }
    </script>

    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script language="javascript" type="text/javascript">
        window.history.go(1);   
            
    </script>

    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbsubmit" defaultfocus="txtUsername">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                    </td>
                                    <td class="contenttd">
                                        <asp:UpdatePanel ID="udtPnlLogin" runat="server">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="tdheight">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="45%" class="searchtableclass">
                                                                <tr>
                                                                    <td class="searchhdrbarbold" colspan="2">
                                                                        Login
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" height="20px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt" width="35%">
                                                                        <span class="mandatorystar">*</span>User Name
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:TextBox CssClass="txtbox" ID="txtUsername" runat="server" MaxLength="50"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="reqUsername" ControlToValidate="txtUsername" Display="none"
                                                                            ValidationGroup="Login" runat="server" ErrorMessage="<%$ Resources:ErrorMessages,AdminUserName%>">
                                                                        </asp:RequiredFieldValidator>
                                                                        
                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtUsername" runat="server" TargetControlID="txtUsername"
                                                                         FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9,@,.,-,_,"></Ajax:FilteredTextBoxExtender>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt">
                                                                        <span class="mandatorystar">*</span>Password
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:TextBox ID="txtPassword" CssClass="txtbox" runat="server" TextMode="Password"
                                                                            MaxLength="15"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="reqPassword" ControlToValidate="txtPassword" Display="none"
                                                                            ValidationGroup="Login" runat="server" ErrorMessage="<%$ Resources:ErrorMessages,AdminPassword%>">
                                                                        </asp:RequiredFieldValidator>
                                                                        <%--<Ajax:FilteredTextBoxExtender ID="ftxtPass" runat="server" TargetControlID="txtPassword"
                                                                          FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9,.,"></Ajax:FilteredTextBoxExtender>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:ImageButton ID="imbsubmit" runat="server" ToolTip="Login" ImageUrl="~/images/login.jpg"
                                                                            OnClick="imbsubmit_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdheight">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                                                PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg">
                                                            </Ajax:ModalPopupExtender>
                                                            <asp:HiddenField ID="lblHidden" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="modalPopupDiv" class="popupdivsmall" style="display: none">
                                                                <table cellpadding="5" cellspacing="0" border="0" width="350" align="center">
                                                                    <tr>
                                                                        <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                                            Message
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="paddingright">
                                                                            <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="ModalPopupTd">
                                                                            <asp:ImageButton ID="imbOK" ToolTip="Ok" runat="server" 
                                                                                ImageUrl="~/Images/ok.jpg" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>

</html>