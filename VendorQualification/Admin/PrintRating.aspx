﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintRating.aspx.cs" Inherits="Admin_PrintRating" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language ="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
     <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache">

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
            </script>   
   
      <style type="text/css">

body 
{
	font-family: Arial;
	font-size: 11px;
	color: #333333;
	background: url(../images/body_bg.png) repeat-x;
	background-color: #2672b8;
	margin-top:0px;
}



</style>
 
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                       
                                            <td class="contenttd">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="formhead">
                                                            Subcontractor Rating
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="right">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div id="Div1" runat="server">
                                                                            <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="table_border">
                                                                                        <table width="100%" cellpadding="3" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold" width="11%">
                                                                                                                Subcontractor :
                                                                                                            </td>
                                                                                                            <td class="bodytextleft" width="65%">
                                                                                                                <asp:Label ID="lblSubcontractorname" CssClass="bodytextheadbold" runat="server"> </asp:Label>
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_right" width="10%">
                                                                                                                Rating Date
                                                                                                            </td>
                                                                                                            <td class="bodytextright left_border" width="5%">
                                                                                                                <asp:Label ID="lblRatingDate" runat="server"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <span id="spnedit" runat="server">
                                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td class="bodytextheadbold" width="19%">
                                                                                                                    Project Number
                                                                                                                </td>
                                                                                                                <td class="bodytextheadbold" width="15%">
                                                                                                                    <asp:Label ID="lblProjectNumber" runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                                <td class="bodytextbold" width="12%">
                                                                                                                    Project Name
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold">
                                                                                                                    Rater Name
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:Label ID="lblRaterName" runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                                <td class="bodytextbold">
                                                                                                                    Rater Title
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:Label ID="lblRaterTitle" runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold">
                                                                                                                    Final Subcontract Value
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                    <asp:Label ID="lblSubcontractValue" runat="server"></asp:Label>
                                                                                                                </td>
                                                                                                                <td class="bodytextbold">
                                                                                                                 
                                                                                                                </td>
                                                                                                                <td class="bodytextleft">
                                                                                                                  
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <%-- <tr>
                                                                                        <td class="bodytextleft">
                                                                                            
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                           
                                                                                        </td>
                                                                                        <td colspan="2">
                                                                                            <span id="spnModDate" runat="server" style="display: none">
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextleft">
                                                                                                            Last Modified Date
                                                                                                        </td>
                                                                                                        <td class="bodytextleft">
                                                                                                            <asp:Label ID="lblLastModified" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>--%>
                                                                                                        </table>
                                                                                                        <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td class="bodytextbold" width="19%" valign="top">
                                                                                                                    Subcontractor scope
                                                                                                                </td>
                                                                                                                <td class="bodytextleft" style="text-align: justify;" valign="top" >  
                                                                                                                    <%--<asp:Label ID="lblSubcontractorscope" style="word-wrap:break-word;" Width="600" runat="server" ></asp:Label>--%>
                                                                                                                    <%--G. Vera 08/30/2012 - VMS Stabilization Release 1.2--%>
                                                                                                                    <asp:TextBox ID="lblSubcontractorscope" TextMode="MultiLine" ReadOnly="true" 
                                                                                                                        CssClass="MultiLabel" Width="520px" runat="server" BorderStyle="None" Height="100%"></asp:TextBox>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                        </table>
                                                                                                    </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="100%">
                                                                                        <table width="100%" cellpadding="5" cellspacing="0">
                                                                                            <tr>
                                                                                                <td class="evenpad" style="padding-top: 0">
                                                                                                    <table cellpadding="10" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextheadcenter" colspan="5">
                                                                                                                Ratings
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="5" align="center">
                                                                                                                <table width="100%">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextheadright" width="50%">
                                                                                                                            Overall Performance
                                                                                                                        </td>
                                                                                                                        <td style="padding: 0 50px 0 15px" class="bodytextleft">
                                                                                                                        <asp:Image ID="imgOverallPerformance" runat="server" />
                                                                                                                           <%-- <Ajax:Rating ID="rtgOverallPerformance" runat="server" CssClass="ratingStar" CurrentRating="0"
                                                                                                                                EmptyStarCssClass="Empty" FilledStarCssClass="Filled" MaxRating="5" OnChanged="Rating1_Changed"
                                                                                                                                ReadOnly="true" StarCssClass="ratingItem" TabIndex="2" WaitingStarCssClass="Saved">
                                                                                                                            </Ajax:Rating>--%>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleft" width="35%">
                                                                                                                Safety
                                                                                                            </td>
                                                                                                            <td>
                                                                                                              <asp:Image ID="imgSafety" runat="server" />
                                                                                                               <%-- <Ajax:Rating ReadOnly="true" ID="rtgSafety" runat="server" CssClass="ratingStar"
                                                                                                                    TabIndex="3" CurrentRating="0" EmptyStarCssClass="Empty" FilledStarCssClass="Filled"
                                                                                                                    MaxRating="5" OnChanged="Rating1_Changed" StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                </Ajax:Rating>--%>
                                                                                                            </td>
                                                                                                            <td width="15px">
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                Project Management
                                                                                                            </td>
                                                                                                            <td>
                                                                                                              <asp:Image ID="imgProjectManagement" runat="server" />
                                                                                                              <%--  <Ajax:Rating ReadOnly="true" ID="rtgProjectManagement" runat="server" CssClass="ratingStar"
                                                                                                                    TabIndex="5" CurrentRating="0" EmptyStarCssClass="Empty" FilledStarCssClass="Filled"
                                                                                                                    MaxRating="5" OnChanged="Rating1_Changed" StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                </Ajax:Rating>--%>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleft">
                                                                                                               Quality Of Work 
                                                                                                            </td>
                                                                                                            <td>
                                                                                                              <asp:Image ID="imgQuality" runat="server" />
                                                                                                          <%--  <Ajax:Rating ReadOnly="true" ID="rtgQualityOfWork" runat="server" CssClass="ratingStar"
                                                                                                                    TabIndex="4" CurrentRating="0" EmptyStarCssClass="Empty" FilledStarCssClass="Filled"
                                                                                                                    MaxRating="5" OnChanged="Rating1_Changed" StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                </Ajax:Rating>--%>
                                                                                                                
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                Field Management
                                                                                                            </td>
                                                                                                            <td>
                                                                                                              <asp:Image ID="imgFileManagement" runat="server" />
                                                                                                               <%-- <Ajax:Rating ReadOnly="true" ID="rtgFileManagement" runat="server" CssClass="ratingStar"
                                                                                                                    TabIndex="6" CurrentRating="0" EmptyStarCssClass="Empty" FilledStarCssClass="Filled"
                                                                                                                    MaxRating="5" OnChanged="Rating1_Changed" StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                </Ajax:Rating>--%>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleft">
                                                                                                                Financial Capability
                                                                                                            </td>
                                                                                                            <td>
                                                                                                              <asp:Image ID="imgFinancialCapability" runat="server" />
                                                                                                              <%--  <Ajax:Rating ReadOnly="true" ID="rtgFinancialCapability" runat="server" CssClass="ratingStar"
                                                                                                                    TabIndex="7" CurrentRating="0" EmptyStarCssClass="Empty" FilledStarCssClass="Filled"
                                                                                                                    MaxRating="5" OnChanged="Rating1_Changed" StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                </Ajax:Rating>--%>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                Change Orders/Claims
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                              <asp:Image ID="imgOrderClaim" runat="server" />
                                                                                                           <%--   <Ajax:Rating ReadOnly="true" ID="rtgChange" runat="server" CssClass="ratingStar"
                                                                                                                    CurrentRating="0" TabIndex="11" EmptyStarCssClass="Empty" FilledStarCssClass="Filled"
                                                                                                                    MaxRating="5" OnChanged="Rating1_Changed" StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                </Ajax:Rating>--%>
                                                                                                                
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleft">
                                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Did S/C pay sub-subs/vendors on time?
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                              <asp:Label ID="lblPay" runat="server"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                               Schedule Control
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                             <asp:Image ID="imgScheduleControl" runat="server" />
                                                                                                             <%-- <Ajax:Rating ReadOnly="true" ID="rtgCorporation" runat="server" CssClass="ratingStar"
                                                                                                                    CurrentRating="0" TabIndex="12" EmptyStarCssClass="Empty" FilledStarCssClass="Filled"
                                                                                                                    MaxRating="5" OnChanged="Rating1_Changed" StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                </Ajax:Rating>--%>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleft">
                                                                                                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Did Haskell have to waive insurance levels?
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                               <asp:Label ID="lblLevels" runat="server"></asp:Label>
                                                                                                               
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                              Cooperation
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                             <asp:Image ID="imgCorporation" runat="server" />
                                                                                              
                                                                                                             <%-- <Ajax:Rating ReadOnly="true" ID="rtgCorporation" runat="server" CssClass="ratingStar"
                                                                                                                    CurrentRating="0" TabIndex="12" EmptyStarCssClass="Empty" FilledStarCssClass="Filled"
                                                                                                                    MaxRating="5" OnChanged="Rating1_Changed" StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                </Ajax:Rating>--%>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleft">
                                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Did sub require special payment terms?</td>
                                                                                                            <td class="bodytextleft">
                                                                                                                <asp:Label ID="lblPayment" runat="server"></asp:Label></td>
                                                                                                            <td>
                                                                                                                &nbsp;</td>
                                                                                                            <td class="bodytextleft">
                                                                                                                &nbsp;</td>
                                                                                                            <td align="left">
                                                                                                                &nbsp;</td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="tdheight">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold">
                                                                                                    Comments
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                           
                                                                                                <td>
                                                                                                <table cellpadding="0" cellspacing="0" width="100%"><tr>
                                                                                                <td class="bodytextleft">
                                                                                              
                                                                                              <%-- G. Vera 08/03/2012:  VMS Stabilization Release 1.2 (JIRA:VPI-46)--%>
                                                                                               <%--<asp:Label ID="lblComments" runat="server" Width="750px" />--%>
                                                                                               <%--<span style="vertical-align:top;  float:left; width: 700px;"  id="lblComments" runat="server" ></span>--%>
                                                                                               <asp:TextBox ID="lblComments" runat="server" Width="700px" ReadOnly="true" 
                                                                                                        TextMode="MultiLine" BorderStyle="None" CssClass="MultiLabelNone" />
                                                                                              
                                                                                                </td></tr></table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="tdheight">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                            </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr align="center">
                                                        <td>
                                                            <asp:HiddenField ID="hdrating" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdheight">
                                            </td>
                                        </tr>
                                    </table>
                                    </td>
                                    </tr> 
                                    
                                    <tr>
                                        <td>
                                            <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                                PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                            </Ajax:ModalPopupExtender>
                                            <asp:HiddenField ID="lblHidden" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="modalPopupDiv" class="popupdiv" style="display: none">
                                                <table cellpadding="5" cellspacing="0" border="0" width="350">
                                                    <tr>
                                                        <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                            Result
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdheight popupdivcl">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="popupdivcl">
                                                            <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="popupdivcl bodytextcenter">
                                                            <input type="button" value="OK" class="ModalPopupButton" onclick="$find('modalExtnd').hide();" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdheight popupdivcl">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>
</body>
</html>
