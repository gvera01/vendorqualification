﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WelcomePage.aspx.cs" Inherits="Admin_WelcomePage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>

    <script language="javascript" type="text/javascript">
          window.onerror = ErrHndl;

          function ErrHndl()
          { return true; }
    </script>

    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache">

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>

    <style type="text/css">
        body
        {
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" >
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd"  >
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td height="10px">
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="font-family: Tahoma; font-size: 15px; font-weight: bold">
                                                    
                                                    Welcome to Haskell's Vendor Management System
                                                    
                                                </td>
                                            </tr>
                                            <tr style="display: none">
												<td style="color: #800000; font-size: 14px;padding: 10PX 0px 0px 145px;font-family: Tahoma;" valign="top" >
													<p >
                                                                The VMS (Vendor Management System) will be temporarily <br />
                                                                unavailable starting Sunday, April 2 9:30 a.m. (ET) and ending <br />
                                                                Sunday, April 2 10:30 a.m. (ET) <br /><br />

																Thank you for your continued patience while we improve our system.
                                                    </p>
												</td>
											</tr>
                                            <tr>
                                                <td height="2px">&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" style="font-family: Tahoma; font-size: 12px; font-weight: bold;">
                                                    
                                                    Please use the link below to refer to Haskell Project Policies and Procedures for Vendor Prequalification
                                                        
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="2px">&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" style="font-family: Tahoma; font-size: 12px; font-weight: bold;">
                                                    
                                                    <a href="https://opg.haskell.com/Content/Project_Procedures_-_3000/3100_-_Strategy/P-3142_Vendor_Prequalification.htm?tocpath=Project%20Procedures%7C3000%20Delivery%7C3100%20Strategy%7C_____12"
                                                        target="_blank" title="Policies and Procedures" style="color: #2672b8">P3142 Vendor Prequalification</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" style="font-family: Tahoma; font-size: 12px; font-weight: bold;">
                                                    
                                                    <a href="https://share.haskell.com/jax/vendorprequal/SitePages/Home.aspx"
                                                        target="_blank" title="Prequalification Request" style="color: #2672b8">Prequalification Request</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="150px">
                                                </td>
                                            </tr>
                                            <%--G. Vera 07/05/2012 - Added modal dialog--%>
                                            <tr>
                                                <td>
                                                    <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                                        PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg">
                                                    </Ajax:ModalPopupExtender>
                                                    <asp:HiddenField ID="lblHidden" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="modalPopupDiv" class="popupdivsmall" style="display: none">
                                                        <table cellpadding="5" cellspacing="0" border="0" width="350" align="center">
                                                            <tr>
                                                                <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                                    Message
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="paddingright">
                                                                    <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="ModalPopupTd">
                                                                    <asp:ImageButton ID="imbOK" ToolTip="Ok" runat="server" 
                                                                        ImageUrl="~/Images/ok.jpg" onclick="imbOK_Click" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
