﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchVendor.aspx.cs" Inherits="Admin_SearchVendor"
    MaintainScrollPositionOnPostback="true"  %>

<%@ Register TagPrefix="cc1" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>

    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }

        // ***** Added by N Schoenberger on 01/05/2012 [Phase II Enhancement] ***** //
        function getConfirmation() {
            if (confirm("Are you sure you want to delete the selected document?"))
                return true;
            return false;
        }
        // ***** End added by N Schoenberger on 01/05/2012 [Phase II Enhancement] ***** //
    </script>

    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />

    <script src="../Script/jquery.js" type="text/javascript"></script>    
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <script language="javascript" type="text/javascript">
        window.history.go(1);
    </script>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="bodytextright">
                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                        <tr>
                                                            <td class="searchhdrbarbold" colspan="2">
                                                                Vendor Search
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="center" class="bodytextbold">
                                                                &nbsp;&nbsp;Please click on one or more selections for your search
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <%--G. Vera - 02/26/2013--%>
                                                            <td class="bodytextbold_right" width="12%">
                                                                Search By
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:CheckBoxList ID="chlSearchBy" runat="server" RepeatDirection="Horizontal" onclick="javascript:ShowSearchBy()">
                                                                    <asp:ListItem Value="0">Vendor Name</asp:ListItem>
                                                                    <asp:ListItem Value="1">Scopes of Work</asp:ListItem>
                                                                    <asp:ListItem Value="2">Geographic Location</asp:ListItem>
                                                                    <asp:ListItem Value="3">Types of Projects</asp:ListItem>
                                                                    <asp:ListItem Value="4">Certification</asp:ListItem>
                                                                    <asp:ListItem Value="5">BIM</asp:ListItem>
                                                                    <%--G. Vera - 02/26/2013--%>
                                                                </asp:CheckBoxList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <span id="spnVendorDetails" runat="server" style="display: ">
                                                                    <table width="50%" cellpadding="0" cellspacing="0" border="0" align="center">
                                                                        <tr>
                                                                            <td class="formtdrt" nowrap="nowrap">
                                                                                Vendor Name from Vendor Database
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtVendor" runat="server" CssClass="ajaxtxtbox" MaxLength="60" AutoComplete="off"></asp:TextBox>
                                                                                <%--  <div id="listPlacement" style="height: 100px; overflow: scroll;">
                                                                                </div>--%>
                                                                                <Ajax:AutoCompleteExtender ID="autoComplete1" runat="server" BehaviorID="AutoCompleteEx"
                                                                                    CompletionListElementID="listPlacement" FirstRowSelected="True" MinimumPrefixLength="1"
                                                                                    OnClientItemSelected="" ServiceMethod="GetCompletionList" ServicePath="~/AutoComplete.asmx"
                                                                                    TargetControlID="txtVendor">
                                                                                    <%-- <Animations>
                                                            <OnShow>
                                                              <Sequence>
                                                             
                                                                <OpacityAction Opacity="0" />
                                                                <HideAction Visible="true" />
                                                            
                                                                
                                                                  <ScriptAction Script="
                                                                // Cache the size and setup the initial size
                                                                var behavior = $find('AutoCompleteEx');
                                                                if (!behavior._height) {
                                                                    var target = behavior.get_completionList();
                                                                    behavior._height = target.offsetHeight - 2;
                                                                    target.style.height = '0px';
                                                                  }" />
                                                            
                                                           
                                                                <Parallel Duration=".4">
                                                                <FadeIn />
                                                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                                                                </Parallel>
                                                                </Sequence>
                                                                 </OnShow>
                                                                  <OnHide>
                                                       
                                                                 <Parallel Duration=".4">
                                                                 <FadeOut />
                                                                 <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                                                                 </Parallel>
                                                                 </OnHide>
                                                                                    </Animations>--%>
                                                                                </Ajax:AutoCompleteExtender>
                                                                                <asp:HiddenField ID="hdnMatch" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdrt" nowrap="nowrap">
                                                                                Vendor Number from E1 / JDE
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtVendorNumber" runat="server" MaxLength="50" CssClass="txtbox"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr runat="server" id="spnTrcNo">
                                                                            <%--   <td colspan="2" >
                                                         <span runat="server" id="spnTrcNo">
                                                         <table width="80%"  cellpadding="0" cellspacing="0" border="0" >
                                                         <tr>--%>
                                                                            <td class="formtdrt" width="20%" nowrap="nowrap">
                                                                                Tracking Number
                                                                            </td>
                                                                            <td class="formtdrt" width="80%">
                                                                                <asp:TextBox ID="txtTrackingNumber" runat="server" MaxLength="50" CssClass="txtbox"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <%-- </table></span>
                                                         </td>                                                           
                                                        </tr>--%>
                                                                        <tr>
                                                                            <td class="formtdrt" nowrap="nowrap">
                                                                                Federal Tax ID / Business ID
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <%--007- Sooraj-- %>
                                                                               <%-- <asp:TextBox CssClass="txtboxtosmall" ID="txtFederalTax1" runat="server" MaxLength="2"></asp:TextBox>&nbsp;-
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtFederalTax1" runat="server" FilterType="Numbers"
                                                                                    TargetControlID="txtFederalTax1">
                                                                                </Ajax:FilteredTextBoxExtender>--%>
                                                                                <asp:TextBox CssClass="txtbox" ID="txtFederalTax2" runat="server" MaxLength="50">
                                                                                </asp:TextBox>
                                                                                <Ajax:FilteredTextBoxExtender ID="ftxtFederalTax2" runat="server" FilterType="Numbers,UppercaseLetters, LowercaseLetters"
                                                                                    TargetControlID="txtFederalTax2">
                                                                                </Ajax:FilteredTextBoxExtender>
                                                                                <%--007- Sooraj-- %>
                                                                             <%--   <asp:CustomValidator ID="custValidator" runat="server" EnableClientScript="false"
                                                                                    Display="None" SetFocusOnError="true" OnServerValidate="custValidator_ServerValidate"></asp:CustomValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <%--G. Vera -  Add new search by fields--%>
                                                                        <tr>
                                                                            <td class="formtdrt" nowrap="nowrap">
                                                                                First Name
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="txtbox" MaxLength="60" AutoComplete="off"></asp:TextBox>
                                                                                <%--<Ajax:AutoCompleteExtender ID="extFirstNameAuto" runat="server" BehaviorID="extFirstNameAuto"
                                                                                    CompletionListElementID="listPlacement" FirstRowSelected="True" MinimumPrefixLength="1"
                                                                                    OnClientItemSelected="" ServiceMethod="GetCompletionList" ServicePath="~/AutoComplete.asmx"
                                                                                    TargetControlID="txtFirstName">

                                                                                </Ajax:AutoCompleteExtender>
                                                                                <asp:HiddenField ID="hdnFirstName" runat="server" />--%>
                                                                            </td>
                                                                        </tr>
                                                                        <%--G. Vera -  Add new search by fields--%>
                                                                        <tr>
                                                                            <td class="formtdrt" nowrap="nowrap">
                                                                                Last Name
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtLastName" runat="server" CssClass="txtbox" MaxLength="60" AutoComplete="off"></asp:TextBox>
                                                                                <%--<Ajax:AutoCompleteExtender ID="extFirstNameAuto" runat="server" BehaviorID="extFirstNameAuto"
                                                                                    CompletionListElementID="listPlacement" FirstRowSelected="True" MinimumPrefixLength="1"
                                                                                    OnClientItemSelected="" ServiceMethod="GetCompletionList" ServicePath="~/AutoComplete.asmx"
                                                                                    TargetControlID="txtFirstName">

                                                                                </Ajax:AutoCompleteExtender>
                                                                                <asp:HiddenField ID="hdnFirstName" runat="server" />--%>
                                                                            </td>
                                                                        </tr>
                                                                        <%--G. Vera -  Add new search by fields--%>
                                                                        <tr>
                                                                            <td class="formtdrt" nowrap="nowrap">
                                                                                Email
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="txtbox" MaxLength="60" AutoComplete="off"></asp:TextBox>
                                                                                <%--<Ajax:AutoCompleteExtender ID="extFirstNameAuto" runat="server" BehaviorID="extFirstNameAuto"
                                                                                    CompletionListElementID="listPlacement" FirstRowSelected="True" MinimumPrefixLength="1"
                                                                                    OnClientItemSelected="" ServiceMethod="GetCompletionList" ServicePath="~/AutoComplete.asmx"
                                                                                    TargetControlID="txtFirstName">

                                                                                </Ajax:AutoCompleteExtender>
                                                                                <asp:HiddenField ID="hdnFirstName" runat="server" />--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trStatus" runat="server">
                                                                            <td class="formtdrt" nowrap="nowrap">
                                                                                Status
                                                                            </td>
                                                                            <%--Included By Sgs for issue 16 --%>
                                                                            <td class="formtdrt">
                                                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ddlbox">
                                                                                    <asp:ListItem>--Select--</asp:ListItem>
                                                                                    <asp:ListItem>InProgress</asp:ListItem>
                                                                                    <asp:ListItem>Pending</asp:ListItem>
                                                                                    <asp:ListItem>Complete</asp:ListItem>
                                                                                    <asp:ListItem>Revision</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <%--G. Vera 07/14/2014: Added--%>
                                                                        <tr id="trTypeOfCompany" runat="server">
                                                                            <td class="formtdrt" nowrap="nowrap">Type of Company
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:DataList ID="rptTypeOfCompany" runat="server" RepeatDirection="Horizontal" RepeatColumns="2">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkTypeOfCompany" runat="server" Text='<%# Bind("CompanyTypeDesc") %>' ToolTip='<%# Bind("CompanyType") %>'/>
                                                                                    </ItemTemplate>
                                                                                </asp:DataList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </span>
                                                                <asp:HiddenField ID="hdnScopeTypeOpen" runat="server" Value="MasterFormat" />
                                                                <asp:UpdatePanel ID="spnCSICode" runat="server" style="display: none;">
                                                                    <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td class="formtdbold_rt">
                                                                                    Scopes of Work
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="tab-container-internal">
                                                                                        <ul class="tabs">
                                                                                            <li id="liMasterFormat" runat="server" class="">
                                                                                                <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="Javascript:SwitchScopePanel('updPnlPMMI', 'updPnlDesign', 'updpnlScope', 'MasterFormat');scroll(0,0); return false;">MasterFormat Codes</asp:LinkButton></li>
                                                                                            <li id="liPMMI" runat="server" class="">
                                                                                                <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="Javascript:SwitchScopePanel('updpnlScope', 'updPnlDesign', 'updPnlPMMI', 'PMMI');scroll(0,0); return false;">Packaging Machinery Manufacturers Institute Codes</asp:LinkButton></li>
                                                                                            <li id="liDesign" runat="server" class="">
                                                                                                <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="Javascript:SwitchScopePanel('updpnlScope', 'updPnlPMMI', 'updPnlDesign', 'Design');scroll(0,0); return false;">Design</asp:LinkButton></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <div id="updpnlScope" runat="server">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="list_head" valign="top" width="50%" style="color: #005785">
                                                                                                    <%-- SubSection1: Scope Codes--%>
                                                                                                    <asp:GridView ID="grdLevel1" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                                                        ShowHeader="False" OnRowDataBound="GridView1_RowDataBound">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField>
                                                                                                                <EditItemTemplate>
                                                                                                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                                                                                </EditItemTemplate>
                                                                                                                <ItemTemplate>
                                                                                                                    <table>
                                                                                                                        <tr>
                                                                                                                            <td style="text-indent: -40px; padding-left: 40px">
                                                                                                                                <asp:Image ID="imgStatus" runat="server" ImageUrl="~/Images/plus.gif" />
                                                                                                                                <asp:Label ID="lblScopeName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hdnFlag" runat="server" />
                                                                                                                    <asp:HiddenField ID="hdnParentScopeId" runat="server" Value='<%# Bind("Pk_ScopeId") %>' />
                                                                                                                    <br />
                                                                                                                    <asp:GridView ID="grdScopeLevele2" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_ScopeId"
                                                                                                                        GridLines="None" ShowHeader="False" OnRowDataBound="grdGeneralReq_RowDataBound">
                                                                                                                        <Columns>
                                                                                                                            <asp:TemplateField>
                                                                                                                                <EditItemTemplate>
                                                                                                                                    <asp:TextBox ID="txtScopeId" runat="server" Text='<%# Bind("PK_ScopeId") %>'></asp:TextBox>
                                                                                                                                </EditItemTemplate>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:HiddenField ID="hdnScopeId" runat="server" Value='<%# Bind("PK_ScopeId") %>' />
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField ShowHeader="False">
                                                                                                                                <ItemStyle VerticalAlign="Top" />
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:CheckBox ID="chkCheck" runat="server" CssClass="display_Check" AutoPostBack="false" />
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField>
                                                                                                                                <EditItemTemplate>
                                                                                                                                    <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                                                                                                                                </EditItemTemplate>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <table>
                                                                                                                                        <tr>
                                                                                                                                            <td style="text-indent: -45px; padding-left: 45px">
                                                                                                                                                <asp:Label ID="lblName" runat="server" CssClass="list_display" Text='<%# Bind("Name") %>'></asp:Label>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                    <asp:CheckBoxList ID="chklGeneralReq" CssClass="list_display1" runat="server">
                                                                                                                                    </asp:CheckBoxList>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                        </Columns>
                                                                                                                    </asp:GridView>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                    </asp:GridView>
                                                                                                </td>
                                                                                                <td class="list_head" valign="top" width="50%" style="color: #005785">
                                                                                                    <asp:GridView ID="grdLevel2" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                                                        ShowHeader="False" OnRowDataBound="GridView1_RowDataBound">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField>
                                                                                                                <EditItemTemplate>
                                                                                                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                                                                                </EditItemTemplate>
                                                                                                                <ItemTemplate>
                                                                                                                    <table>
                                                                                                                        <tr>
                                                                                                                            <td style="text-indent: -40px; padding-left: 40px">
                                                                                                                                <asp:Image ID="imgStatus" runat="server" ImageUrl="~/Images/plus.gif" />
                                                                                                                                <asp:Label ID="lblScopeName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hdnFlag" runat="server" />
                                                                                                                    <asp:HiddenField ID="hdnParentScopeId" runat="server" Value='<%# Bind("Pk_ScopeId") %>' />
                                                                                                                    <br />
                                                                                                                    <asp:GridView ID="grdScopeLevele2" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_ScopeId"
                                                                                                                        GridLines="None" ShowHeader="False" OnRowDataBound="grdGeneralReq_RowDataBound">
                                                                                                                        <Columns>
                                                                                                                            <asp:TemplateField>
                                                                                                                                <EditItemTemplate>
                                                                                                                                    <asp:TextBox ID="txtScopeId" runat="server" Text='<%# Bind("PK_ScopeId") %>'></asp:TextBox>
                                                                                                                                </EditItemTemplate>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:HiddenField ID="hdnScopeId" runat="server" Value='<%# Bind("PK_ScopeId") %>' />
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField ShowHeader="False">
                                                                                                                                <ItemStyle VerticalAlign="Top" />
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:CheckBox ID="chkCheck" runat="server" CssClass="display_Check" AutoPostBack="false" />
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField>
                                                                                                                                <EditItemTemplate>
                                                                                                                                    <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                                                                                                                                </EditItemTemplate>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <table>
                                                                                                                                        <tr>
                                                                                                                                            <td style="text-indent: -45px; padding-left: 45px">
                                                                                                                                                <asp:Label ID="lblName" runat="server" CssClass="list_display" Text='<%# Bind("Name") %>'></asp:Label>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                    <asp:CheckBoxList ID="chklGeneralReq" CssClass="list_display1" runat="server">
                                                                                                                                    </asp:CheckBoxList>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                        </Columns>
                                                                                                                    </asp:GridView>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                    </asp:GridView>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <%--G. Vera - PMMI Codes--%>
                                                                                <td>
                                                                                    <div id="updPnlPMMI" runat="server" style="display: none;">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="list_head1" width="73%" style="color: #005785;" valign="top" align="left">
                                                                                                    <asp:DataList ID="dlPMMICodes1" runat="server" OnItemDataBound="dlPMMICodes1_ItemDataBound">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chkPMMICode" runat="server" Text='<%# Bind("Name") %>' />&nbsp;
                                                                                                            <asp:HiddenField ID="hdnPMMIScopeId" runat="server" Value='<%# Bind("PK_ScopeID") %>' />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:DataList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <%--006-Sooraj - Design Consultant--%>
                                                                                <td>
                                                                                    <div id="updPnlDesign" runat="server">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="list_head" width="100%" style="color: #005785;" valign="top" align="left">
                                                                                                    <asp:DataList ID="dlistDesign" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                                                                                                        OnItemDataBound="dlistDesign_ItemDataBound">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chkDesignCode" CssClass="checkboxthreeColumn" runat="server" Text='<%# Bind("Name") %>' />&nbsp;
                                                                                                            <asp:HiddenField ID="hdnDesignScopeId" runat="server" Value='<%# Bind("PK_ScopeID") %>' />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:DataList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                                <asp:UpdatePanel ID="upnPanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <span id="spnLocation" runat="server" style="display: none">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td class="formtdbold_rt">
                                                                                        Geographic Location
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtdrt">
                                                                                        <span id="pnlRegions" runat="server">
                                                                                            <%--006-Sooraj--%>
                                                                                            <table cellpadding="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="regionTableCell">
                                                                                                        <asp:CheckBox ID="chkUS" runat="server" Text="United States of America" CssClass="list_displaybold" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <div id="divUS" runat="server" style="display: none">
                                                                                                            <table width="100%">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <tr>
                                                                                                                            <td valign="top" bgcolor="#dfdfdf" style="padding: 3px 0px 5px 12px;" colspan="3">
                                                                                                                                <asp:CheckBox ID="chkCheckAll" runat="server" Text="ALL-National" CssClass="list_displaybold" />
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td valign="top" bgcolor="#efefef" width="33%">
                                                                                                                                <asp:GridView ID="grdState1" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_StateId"
                                                                                                                                    GridLines="None" ShowHeader="False" OnRowDataBound="grdState1_RowDataBound">
                                                                                                                                    <Columns>
                                                                                                                                        <asp:TemplateField>
                                                                                                                                            <EditItemTemplate>
                                                                                                                                                <asp:TextBox ID="txtStateID" runat="server" Text='<%# Bind("PK_StateId") %>'></asp:TextBox>
                                                                                                                                            </EditItemTemplate>
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <asp:HiddenField ID="hdnStateID" runat="server" Value='<%# Bind("PK_StateId") %>' />
                                                                                                                                            </ItemTemplate>
                                                                                                                                        </asp:TemplateField>
                                                                                                                                        <asp:TemplateField ShowHeader="False">
                                                                                                                                            <ItemStyle VerticalAlign="Top" />
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <!-- FL CHK BOX -->
                                                                                                                                                <asp:CheckBox ID="chkCheck" runat="server" CssClass="" AutoPostBack="false" />
                                                                                                                                            </ItemTemplate>
                                                                                                                                            <ItemStyle VerticalAlign="Top" />
                                                                                                                                        </asp:TemplateField>
                                                                                                                                        <asp:TemplateField>
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <asp:Label ID="lblName" runat="server" CssClass="list_display" Text='<%# Bind("StateCode") %>'></asp:Label>
                                                                                                                                                <asp:CheckBoxList ID="chklRegions" CssClass="list_displayView" runat="server" CellPadding="0"
                                                                                                                                                    CellSpacing="0">
                                                                                                                                                </asp:CheckBoxList>
                                                                                                                                            </ItemTemplate>
                                                                                                                                        </asp:TemplateField>
                                                                                                                                    </Columns>
                                                                                                                                </asp:GridView>
                                                                                                                            </td>
                                                                                                                            <td valign="top" bgcolor="#efefef" width="33%">
                                                                                                                                <asp:GridView ID="grdState2" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_StateId"
                                                                                                                                    GridLines="None" ShowHeader="False" OnRowDataBound="grdState1_RowDataBound">
                                                                                                                                    <Columns>
                                                                                                                                        <asp:TemplateField>
                                                                                                                                            <EditItemTemplate>
                                                                                                                                                <asp:TextBox ID="txtStateID" runat="server" Text='<%# Bind("PK_StateId") %>'></asp:TextBox>
                                                                                                                                            </EditItemTemplate>
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <asp:HiddenField ID="hdnStateID" runat="server" Value='<%# Bind("PK_StateId") %>' />
                                                                                                                                            </ItemTemplate>
                                                                                                                                        </asp:TemplateField>
                                                                                                                                        <asp:TemplateField ShowHeader="False">
                                                                                                                                            <ItemStyle VerticalAlign="Top" />
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <asp:CheckBox ID="chkCheck" runat="server" CssClass="" AutoPostBack="false" />
                                                                                                                                            </ItemTemplate>
                                                                                                                                        </asp:TemplateField>
                                                                                                                                        <asp:TemplateField>
                                                                                                                                            <EditItemTemplate>
                                                                                                                                                <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("StateCode") %>'></asp:TextBox>
                                                                                                                                            </EditItemTemplate>
                                                                                                                                            <ItemTemplate>
                                                                                                                                                <asp:Label ID="lblName" runat="server" CssClass="list_display" Text='<%# Bind("StateCode") %>'></asp:Label>
                                                                                                                                                <asp:CheckBoxList ID="chklRegions" CssClass="list_displayView" runat="server" CellPadding="0"
                                                                                                                                                    CellSpacing="0">
                                                                                                                                                </asp:CheckBoxList>
                                                                                                                                            </ItemTemplate>
                                                                                                                                        </asp:TemplateField>
                                                                                                                                    </Columns>
                                                                                                                                </asp:GridView>
                                                                                                                            </td>
                                                                                                                            <td valign="top" bgcolor="#efefef" width="33%">
                                                                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                                    <tr>
                                                                                                                                        <td align="left" width="100%">
                                                                                                                                            <asp:GridView ID="grdState3" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_StateId"
                                                                                                                                                GridLines="None" ShowHeader="False" OnRowDataBound="grdState1_RowDataBound">
                                                                                                                                                <Columns>
                                                                                                                                                    <asp:TemplateField>
                                                                                                                                                        <EditItemTemplate>
                                                                                                                                                            <asp:TextBox ID="txtStateID" runat="server" Text='<%# Bind("PK_StateId") %>'></asp:TextBox>
                                                                                                                                                        </EditItemTemplate>
                                                                                                                                                        <ItemTemplate>
                                                                                                                                                            <asp:HiddenField ID="hdnStateID" runat="server" Value='<%# Bind("PK_StateId") %>' />
                                                                                                                                                        </ItemTemplate>
                                                                                                                                                    </asp:TemplateField>
                                                                                                                                                    <asp:TemplateField ShowHeader="False">
                                                                                                                                                        <ItemStyle VerticalAlign="Top" />
                                                                                                                                                        <ItemTemplate>
                                                                                                                                                            <asp:CheckBox ID="chkCheck" runat="server" CssClass="" AutoPostBack="false" />
                                                                                                                                                        </ItemTemplate>
                                                                                                                                                    </asp:TemplateField>
                                                                                                                                                    <asp:TemplateField>
                                                                                                                                                        <EditItemTemplate>
                                                                                                                                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("StateCode") %>'></asp:TextBox>
                                                                                                                                                        </EditItemTemplate>
                                                                                                                                                        <ItemTemplate>
                                                                                                                                                            <asp:Label ID="lblName" runat="server" CssClass="list_display" Text='<%# Bind("StateCode") %>'></asp:Label>
                                                                                                                                                            <asp:CheckBoxList ID="chklRegions" CssClass="list_displayView" runat="server" CellPadding="0"
                                                                                                                                                                CellSpacing="0">
                                                                                                                                                            </asp:CheckBoxList>
                                                                                                                                                        </ItemTemplate>
                                                                                                                                                    </asp:TemplateField>
                                                                                                                                                </Columns>
                                                                                                                                            </asp:GridView>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td id="trRegions" runat="server" style="display: none; padding-left: 35px; padding-bottom: 20px"
                                                                                                                                            class="bodytextbold">
                                                                                                                                            Please specify
                                                                                                                                            <asp:TextBox ID="txtRegions" runat="server" CssClass="txtboxmedium"></asp:TextBox>
                                                                                                                                            <Ajax:FilteredTextBoxExtender runat="server" ID="ftxtOtherRegions" TargetControlID="txtRegions"
                                                                                                                                                FilterType="Custom" ValidChars="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, ,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,@"
                                                                                                                                                InvalidChars="0,1,2,3,4,5,6,7,8,9,_-#.$">
                                                                                                                                            </Ajax:FilteredTextBoxExtender>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--006--%>
                                                                                                <tr>
                                                                                                    <td class="regionTableCell">
                                                                                                        <asp:GridView ID="grdContinent" runat="server" DataKeyNames="PK_ContinentId" AutoGenerateColumns="false"
                                                                                                            GridLines="None" Width="100%" HeaderStyle-CssClass="regionHeaderHidden">
                                                                                                            <Columns>
                                                                                                                <asp:TemplateField>
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:CheckBox ID="chkContinent" runat="server" Text='<%# Bind("ContinentName") %>'
                                                                                                                            CssClass="list_displaybold regionTableBorderBottom" />
                                                                                                                        <div id="divCountries" class="regionCountrySearch" runat="server" style="display: none;">
                                                                                                                            <asp:CheckBoxList ID="chkContinentCountries" runat="server" RepeatDirection="Horizontal"
                                                                                                                                RepeatColumns="3" CssClass="checkboxthreeColumn regionFont" />
                                                                                                                            <asp:TextBox ID="txtContinentCountries" runat="server" CssClass="txtbox" Visible="false" />
                                                                                                                        </div>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                            </Columns>
                                                                                                        </asp:GridView>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="border-bottom: 1px solid #cccccc" class="style1">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <%--006-Sooraj --%>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </span>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                                <span id="SpnTOP" runat="server" style="display: none">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td class="formtdbold_rt">
                                                                                Types of Projects
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdrt">
                                                                                <table width="100%" class="searchtableclass">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:CheckBoxList ID="chkProject" runat="server" CssClass="checkboxthreeColumn" RepeatColumns="3"
                                                                                                RepeatDirection="Horizontal" Width="100%">
                                                                                            </asp:CheckBoxList>
                                                                                            <%--G. Vera 10/16/2012 - Added below--%>
                                                                                            <br />
                                                                                            <asp:CheckBox ID="chkProjectOther" runat="server" Text="Other" CssClass="lstbox"
                                                                                                onclick="Javascript:ClearTextBox(this);" />&nbsp;&nbsp;<asp:TextBox ID="txtChkOther"
                                                                                                    runat="server" MaxLength="250" CssClass="txtbox" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </span>
                                                                <div id="divCertification" runat="server" style="display: none">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdbold_rt">
                                                                                Company Certification
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <table width="80%">
                                                                                    <tr>
                                                                                        <td class="bodytextleft" width="32%" nowrap="nowrap" valign="top">
                                                                                            Company Certification
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="68%" valign="top">
                                                                                            <asp:CheckBoxList ID="rdoCompanyCertiY" CssClass="list_displayView" runat="server"
                                                                                                RepeatDirection="Horizontal" RepeatColumns="4" TabIndex="0">
                                                                                            </asp:CheckBoxList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trotherCertification" runat="server" style="display: none">
                                                                                        <td class="bodytextleft" nowrap="nowrap">
                                                                                            Please Specify the Certification
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            &nbsp;
                                                                                            <asp:TextBox CssClass="txtbox" ID="txtCertiOth" runat="server" MaxLength="100"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="custOtherCertify" runat="server" OnServerValidate="custOtherCertify_ServerValidate"
                                                                                                ValidationGroup="CompanyCertify" Display="None" EnableClientScript="false"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextleft" width="32%" valign="top">
                                                                                            Federal SB
                                                                                        </td>
                                                                                        <td class="bodytextleft" width="68%" valign="top" nowrap="nowrap">
                                                                                            <asp:CheckBoxList ID="chklFederalSB" runat="server" CssClass="list_displayView" RepeatDirection="Horizontal"
                                                                                                RepeatColumns="4" TabIndex="0" onclick="javascript:ShowCertContents()">
                                                                                            </asp:CheckBoxList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="spnFederalSB" runat="server" style="display: none">
                                                                                        <td class="bodytextleft" nowrap="nowrap">
                                                                                            Please Specify the Federal SB
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            &nbsp;
                                                                                            <asp:TextBox CssClass="txtbox" ID="txtOtherFederalSB" runat="server" MaxLength="100"></asp:TextBox>
                                                                                            <asp:CustomValidator ID="cusvFederalSB" runat="server" OnServerValidate="custOtherFederalSB_ServerValidate"
                                                                                                ValidationGroup="CompanyCertify" Display="None" EnableClientScript="false"></asp:CustomValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <%--G. Vera 10/19/2012 - Added--%>
                                                                        <tr>
                                                                            <td class="formtdbold_rt" width="100%" nowrap="nowrap" valign="top">
                                                                                Industry Certifications:
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextleft" width="32%" valign="top" colspan="2">
                                                                                <div style="padding-left: 76px;">
                                                                                    <asp:DataList ID="dlIndustryCerts" runat="server">
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkIndustryCert" runat="server" Text='<%# Bind("Certification") %>'
                                                                                                onclick="Javascript:OpenHideTexBox(this)" Width="60px" />
                                                                                            <asp:TextBox ID="txtIndustryCert" runat="server" MaxLength="28" CssClass="txtbox"
                                                                                                Width="200px" />
                                                                                            <Ajax:FilteredTextBoxExtender ID="ftxtexIndustryCert" runat="server" TargetControlID="txtIndustryCert"
                                                                                                FilterType="Custom" ValidChars="1,2,3,4,5,6,7,8,9,0,:," InvalidChars="A-Z,a-z,-,.,;," />
                                                                                            <asp:HiddenField ID="hdnCertificationID" runat="server" Value='<%# Bind("Pk_CompanyCertificationID") %>' />
                                                                                            <%--<Ajax:AutoCompleteExtender ID="aceCert" runat="server" BehaviorID="AutoCompleteExIndCert"
                                                                                            FirstRowSelected="True" MinimumPrefixLength="1" ServiceMethod="GetIndustryCertStartingWith"
                                                                                            ServicePath="~/AutoComplete.asmx" TargetControlID="txtIndustryCert"></Ajax:AutoCompleteExtender>--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:DataList>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td class="bodytextcenter">
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <asp:ImageButton ID="imbsubmit" ToolTip="Search" runat="server" ImageUrl="~/Images/search.jpg"
                                                                                OnClick="imbsubmit_Click" />
                                                                            <asp:ImageButton ID="imbsearchview" ToolTip="View Prior Saved Search" runat="server"
                                                                                ImageUrl="~/Images/viewprior.jpg" OnClick="imbsearchview_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" class="searchtableclass" id="tblView" cellpadding="0" cellspacing="0"
                                                        runat="server" style="display: none">
                                                        <tr>
                                                            <td class="searchhdrbarbold">
                                                                <div style="float: left;">
                                                                    View Prior Search</div>
                                                                <div style="float: right;">
                                                                    <asp:Image ID="imgCloseThis" runat="server" ImageUrl="~/Images/close-x-white.jpg"
                                                                        ToolTip="Close" onclick="Javascript:document.getElementById('tblView').style.display = 'none';"
                                                                        onmouseover="this.style.cursor='pointer'" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodytextcenter">
                                                                <asp:Label ID="lbldisplayinformation" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr id="trGriddetails" runat="server" style="display: none">
                                                            <td class="even_pad">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr id="tr1" runat="server">
                                                                        <td class="bodytextleft">
                                                                            Total Number Of Records:
                                                                            <asp:Label ID="lblSearchTotal" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextright">
                                                                            Number Of Records Per Page:
                                                                        </td>
                                                                        <td width="5%">
                                                                            <asp:DropDownList ID="ddlSearchNumber" runat="server" AutoPostBack="True" CssClass="ddlbox"
                                                                                OnSelectedIndexChanged="ddlSearchNumber_SelectedIndexChanged">
                                                                                <asp:ListItem>10</asp:ListItem>
                                                                                <asp:ListItem>20</asp:ListItem>
                                                                                <asp:ListItem>30</asp:ListItem>
                                                                                <asp:ListItem>40</asp:ListItem>
                                                                                <asp:ListItem>50</asp:ListItem>
                                                                                <%-- Included by SGS as Per Issue No 5--%>
                                                                                <asp:ListItem>100</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="grdSaveSearch" runat="server" HeaderStyle-CssClass="grid_heading"
                                                                    AllowSorting="true" Width="100%" BorderWidth="0px" CellSpacing="1" CellPadding="5"
                                                                    GridLines="None" OnSorting="grdSaveSearch_Sorting" AutoGenerateColumns="False"
                                                                    OnRowDataBound="grdSaveSearch_RowDataBound" OnRowDeleting="grdSaveSearch_RowDeleting"
                                                                    OnRowUpdating="grdSaveSearch_RowUpdating" OnSelectedIndexChanging="grdSaveSearch_SelectedIndexChanging">
                                                                    <RowStyle CssClass="listone" />
                                                                    <AlternatingRowStyle CssClass="listtwo" />
                                                                    <HeaderStyle CssClass="listheading" />
                                                                    <Columns>
                                                                        <%--<asp:TemplateField HeaderText="No" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                               >
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblno" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                                                                      
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
                                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            </asp:TemplateField>--%>
                                                                        <asp:TemplateField HeaderText="Name" HeaderStyle-CssClass="gridpad" SortExpression="SearchedRecord"
                                                                            HeaderStyle-ForeColor="White" ItemStyle-CssClass="gridpad">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lblname" runat="server" Text='<%# Bind("SearchedRecord") %>'
                                                                                    CommandName="Select"></asp:LinkButton>
                                                                                <asp:HiddenField ID="hdnHistoryId" runat="server" Value='<%# Bind("Id") %>' />
                                                                                <asp:HiddenField ID="hdnCount" runat="server" Value='<%# Bind("Count") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Search Criteria Used" HeaderStyle-CssClass="gridpad"
                                                                            SortExpression="SearchRoot" HeaderStyle-ForeColor="White" ItemStyle-CssClass="gridpad">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblRoot" runat="server" Visible="false"></asp:Label>
                                                                                <asp:LinkButton ID="lblsearch" runat="server" Text='<%# Bind("SearchRoot") %>' CommandName="Update"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Date" HeaderStyle-CssClass="gridpad" SortExpression="CreateDate"
                                                                            ItemStyle-CssClass="gridpad" HeaderStyle-ForeColor="White">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbldate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Delete" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkdelete" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td id="tdNextPrevious" runat="server" class="bodytextcenter" style="display: none">
                                                                <br />
                                                                <br />
                                                                Page&nbsp; &nbsp;<asp:ImageButton ID="imbSearchPrevious" runat="server" ImageUrl="~/Images/pre.jpg"
                                                                    OnClick="imbSearchPrevious_Click" />
                                                                &nbsp;&nbsp;
                                                                <asp:PlaceHolder ID="plcCrntPage" runat="server"></asp:PlaceHolder>
                                                                of
                                                                <asp:Label ID="lblSearchTotalPage" runat="server"></asp:Label>&nbsp;&nbsp;<asp:ImageButton
                                                                    ID="imbSearchNext" runat="server" ImageUrl="~/Images/next1.jpg" OnClick="imbSearchNext_Click" />
                                                                <asp:HiddenField ID="hdnSearchPage" runat="server" />
                                                                <asp:HiddenField ID="hdnSearchCount" runat="server" />
                                                                <asp:HiddenField ID="hdnSaveSort" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table width="100%" id="tblVendorDetails" runat="server">
                                                        <tr>
                                                            <td class="tdheight" align="center">
                                                                <asp:Label ID="lblmsg" runat="server" CssClass="errormsg "></asp:Label>
                                                                <asp:Label ID="lblmsgInfo" runat="server" CssClass="errormsg "></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span id="Search" runat="server">
                                                                    <table width="100%" cellpadding="4" cellspacing="0">
                                                                        <tr id="trGrid" runat="server">
                                                                            <td class="bodytextleft" valign="middle">
                                                                                Total Number Of Records:
                                                                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td class="bodytextright" valign="middle">
                                                                                Number Of Records Per Page:
                                                                            </td>
                                                                            <td width="5%">
                                                                                <asp:DropDownList ID="ddlNumber" runat="server" AutoPostBack="True" CssClass="ddlbox"
                                                                                    OnSelectedIndexChanged="ddlNumber_SelectedIndexChanged">
                                                                                    <asp:ListItem>10</asp:ListItem>
                                                                                    <asp:ListItem>20</asp:ListItem>
                                                                                    <asp:ListItem>30</asp:ListItem>
                                                                                    <asp:ListItem>40</asp:ListItem>
                                                                                    <asp:ListItem>50</asp:ListItem>
                                                                                    <%-- Included by SGS as Per Issue No 5--%>
                                                                                    <asp:ListItem>100</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:GridView ID="grdVendor" runat="server" HeaderStyle-CssClass="grid_heading" Width="100%"
                                                                    BorderWidth="0px" CellSpacing="1" CellPadding="5" GridLines="None" AutoGenerateColumns="False"
                                                                    AllowSorting="True" OnRowDataBound="grdVendor_RowDataBound" OnSelectedIndexChanging="grdVendor_SelectedIndexChanging"
                                                                    OnSorting="grdVendor_Sorting" OnRowUpdating="grdVendor_RowUpdating" DataKeyNames="VendorId"
                                                                    OnRowEditing="grdVendor_RowEditing" OnRowCommand="grdVendor_RowCommand">
                                                                    <RowStyle CssClass="listone" />
                                                                    <AlternatingRowStyle CssClass="listtwo" />
                                                                    <HeaderStyle CssClass="listheading" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgPaperClip" runat="server" ImageUrl="~/Images/paperclip.png"
                                                                                    CommandName="Edit"/>
                                                                                <%--G. Vera 03/24/2017--%>
                                                                                <div id="divAttToolTip" class="floatDiv" runat="server">
                                                                                    <asp:Label class="HoverOnGrid" ID="lblAttToolTip" runat="server" />
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="$" HeaderStyle-ForeColor="White"
                                                                            HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad">
                                                                            <ItemTemplate>
                                                                                <asp:Literal ID="ltrlFinancials" runat="server"></asp:Literal>
                                                                                <div id="divFinToolTip" class="floatDiv" runat="server">
                                                                                    <asp:Label class="HoverOnGrid" ID="lblFinToolTip" runat="server" />
                                                                                </div>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad">
                                                                            <ItemTemplate>
                                                                                <asp:Image ID="imgTag" runat="server" />
                                                                                <div id="gridPopup1" class="floatDiv" runat="server">
                                                                                    <asp:Label class="HoverOnGrid" ID="lbtngrdPrint1" runat="server" />
                                                                                </div>
                                                                                <asp:HiddenField ID="hdnVendorId" runat="server" Value='<%# Bind("VendorId") %>' />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="VPP" HeaderStyle-ForeColor="White" SortExpression="Tag"
                                                                            HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad">
                                                                            <ItemTemplate>
                                                                                <asp:Image ID="imgVPP" runat="server" ImageUrl="~/Images/greenbox.jpg" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Vendor Name" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad"
                                                                            SortExpression="VendorName" HeaderStyle-ForeColor="White" ItemStyle-Wrap="true">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblVendorName" runat="server" CssClass="bodytextbold_right" Text='<%# this.GetVendorName(Convert.ToString(Eval("VendorName"))) %>'
                                                                                    ToolTip='<%# Bind("StreetAddress") %>'></asp:Label><br />
                                                                                <asp:Label ID="lblVendorName1" runat="server" Visible="false" Text='<%# Eval("VendorName") %>'></asp:Label>
                                                                                <div id="gridPopup" class="floatDiv" runat="server">
                                                                                    <%--G. Vera - 04/24/2013--%>
                                                                                    <asp:Label class="HoverOnGrid" ID="lbtngrdPrint" runat="server" Text="" />
                                                                                </div>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="gridpad" ForeColor="White"></HeaderStyle>
                                                                            <ItemStyle CssClass="gridpad"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <%--G. Vera 03/27/2017--%>
                                                                        <asp:TemplateField HeaderText="VAR" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad"
                                                                            HeaderStyle-ForeColor="White" HeaderStyle-Width="10px">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkSRRort" runat="server" CommandName="SR" CommandArgument='<%# Bind("VendorId") %>'>VAR</asp:LinkButton>
                                                                                <%--G. Vera 03/27/2017--%>
                                                                                <div id="divVARToolTip" class="floatDiv" runat="server">
                                                                                    <asp:Label class="HoverOnGrid" ID="lblVARToolTip" runat="server" />
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="VQF" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkVQF" runat="server" CommandName="Select">VQF</asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="gridpad"></HeaderStyle>
                                                                            <ItemStyle CssClass="gridpad"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <%--<asp:TemplateField HeaderText="Sub contractor Rating" HeaderStyle-CssClass="gridpad"
                                                                            ItemStyle-CssClass="gridpad" HeaderStyle-Width="10px">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkSRF" runat="server" CommandName="Update">SRF</asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="gridpad" Width="10px"></HeaderStyle>
                                                                            <ItemStyle CssClass="gridpad"></ItemStyle>
                                                                        </asp:TemplateField>--%>
                                                                        <%--G. Vera 03/28/2017--%>
                                                                        
                                                                        <asp:TemplateField HeaderText="Average Overall Rating" HeaderStyle-CssClass="gridpad"
                                                                            ItemStyle-CssClass="gridpad" HeaderStyle-Width="70px" SortExpression="subContractorrating"
                                                                            HeaderStyle-ForeColor="White">
                                                                            <ItemTemplate>
                                                                                <%--<asp:LinkButton ID="lnkSRF" runat="server" CommandName="Update" Visible="false">  </asp:LinkButton>--%>
                                                                                <asp:Literal ID="ltrlRatings" runat="server"></asp:Literal>
                                                                                <asp:HiddenField ID="hdnRatingCount" runat="server" Value='<%# Convert.ToInt32(Math.Round(Convert.ToDouble(Eval("SubContractorRating")))) %>' />

                                                                                <%--<Ajax:Rating ID="rtgStar" runat="server" CssClass="ratingStar" CurrentRating='<%# Convert.ToInt32(Math.Round(Convert.ToDouble(Eval("SubContractorRating")))) %>'
                                                                                HorizontalAlign="Center" EmptyStarCssClass="Empty" FilledStarCssClass="Filled" Tag='<%# Bind("VendorId") %>'
                                                                                MaxRating="5" StarCssClass="ratingItem" WaitingStarCssClass="Saved" OnChanged="rtgStar_Changed" AutoPostBack="true" Visible="false">
                                                                                </Ajax:Rating>--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="gridpad" ForeColor="White" Width="70px"></HeaderStyle>
                                                                            <ItemStyle CssClass="gridpad"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad"
                                                                            SortExpression="StatusDesc" HeaderStyle-ForeColor="White">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblStatusDesc" runat="server" Text='<%# Bind("StatusDesc") %>' CssClass="greenbold"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="gridpad" ForeColor="White"></HeaderStyle>
                                                                            <ItemStyle CssClass="gridpad"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="JDE" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad"
                                                                            SortExpression="JDE" HeaderStyle-ForeColor="White">
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="hdnJDE" runat="server" Value='<%# Bind("JDE") %>' />
                                                                                <%--  <img src="../Images/right.png" id="JDER" runat="server"/>
                                                         <img src="../Images/wrong.png" id="JDEW" runat="server"/>--%>
                                                                                <asp:Label ID="JDER" runat="server" Text="" CssClass="greenbold"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="gridpad" ForeColor="White"></HeaderStyle>
                                                                            <ItemStyle CssClass="gridpad"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Last Update Date" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad"
                                                                            SortExpression="LastModifiedDT" HeaderStyle-ForeColor="White" ItemStyle-Wrap="true">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLastModifiedDate" runat="server" Text='<%# Bind("LastModifiedDT","{0:MM-dd-yyyy}") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="gridpad"></HeaderStyle>
                                                                            <ItemStyle CssClass="gridpad"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <%-- <asp:TemplateField HeaderText="Tracking Number" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblTrackingNumber" runat="server" Text='<%# Bind("TrackingNumber") %>'></asp:Label>
                                                                            </ItemTemplate>

<HeaderStyle CssClass="gridpad"></HeaderStyle>

<ItemStyle CssClass="gridpad"></ItemStyle>
                                                                        </asp:TemplateField>--%>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodytextcenter" id="tdnextPrevious1" runat="server">
                                                                Page&nbsp;
                                                                <asp:ImageButton ID="imbPrevious" runat="server" ImageUrl="~/Images/pre.jpg" OnClick="imbPrevious_Click" />
                                                                &nbsp;&nbsp;<asp:PlaceHolder ID="plcCrntPage1" runat="server"></asp:PlaceHolder>
                                                                of
                                                                <asp:Label ID="lblTotalPage" runat="server"></asp:Label>
                                                                <asp:ImageButton ID="imbNext" runat="server" ImageUrl="~/Images/next1.jpg" OnClick="imbNext_Click" />
                                                                <asp:HiddenField ID="hdnPage" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="bodytextcenter" id="tdbuttons" runat="server">
                                                                <asp:ImageButton ID="imbActive" ToolTip="Save Searched Results" runat="server" ImageUrl="~/Images/savethis.gif"
                                                                    OnClick="imbActive_Click" />
                                                                <asp:ImageButton ID="imbexcel" ToolTip="Export To Excel" runat="server" ImageUrl="~/Images/extoexcel.jpg"
                                                                    OnClick="imbexcel_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight" align="center">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight" align="center">
                                                    <asp:ImageButton ID="imgBackToSearch" runat="server" ImageUrl="~/Images/back2search.jpg"
                                                        OnClick="imgBackToSearch_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="hdnName" runat="server" />
                                        <asp:HiddenField ID="hdnSort" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                            CancelControlID="imbCancel" PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg"
                                            RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalPopupDiv" class="popupdiv" style="display: none" height="50">
                                            <table cellpadding="5" cellspacing="0" border="0" width="300" align="center">
                                                <tr>
                                                    <td class="searchhdrbarbold">
                                                        Message
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl tdheight">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextleft">
                                                        <asp:Label ID="lblValidation" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextcenter">
                                                        <asp:ImageButton ID="imbCancel" ToolTip="Cancel" runat="server" ImageUrl="~/Images/cancel.jpg"
                                                            TabIndex="5" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="ModalAddDoc" runat="server" TargetControlID="hdnAddDoc"
                                            PopupControlID="divAddDoc" BackgroundCssClass="popupbg">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="hdnAddDoc" runat="server" />
                                        <asp:HiddenField ID="hdnSRAddDoc" runat="server" />
                                        <asp:HiddenField ID="hdnInAddDoc" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="divAddDoc" class="popupdivbg1" runat="server" style="display: none;">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                                            <tr>
                                                                <td class="Messageheading" runat="server" id="trAttachHeader">
                                                                    Attach Document
                                                                </td>
                                                            </tr>
                                                            <tr id="trAttachDocument" runat="server">
                                                                <td style="padding: 0px 5px">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td class="tdheight">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="popupdivcl" align="left" colspan="2">
                                                                                <asp:Label ID="lblattach" runat="server" CssClass="errormsg"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdrt" colspan="2" style="padding: 10px 7px; border: 1px solid #000">
                                                                                <b>NOTE:</b><br />
                                                                                <br />
                                                                                Each file should be less than or equal to 2 MB of size.<br />
                                                                                Please upload files only in the format gif, jpg, jpeg, xls, xlsx, txt, pdf, doc,
                                                                                docx
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="bodytextcenter">
                                                                                <b>Description</b>
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>Upload Path</b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="TxtFileName1" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:FileUpload ID="Flu1" runat="server" CssClass="fileUpload" TabIndex="0" />
                                                                                <asp:HiddenField ID="hdnFile1" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="TxtFileName2" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:FileUpload ID="Flu2" runat="server" CssClass="fileUpload" TabIndex="0" />
                                                                                <asp:HiddenField ID="hdnFile2" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="TxtFileName3" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:FileUpload ID="Flu3" runat="server" CssClass="fileUpload" TabIndex="0" />
                                                                                <asp:HiddenField ID="hdnFile3" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="TxtFileName4" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:FileUpload ID="Flu4" runat="server" CssClass="fileUpload" TabIndex="0" />
                                                                                <asp:HiddenField ID="hdnFile4" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtdrt">
                                                                                <asp:TextBox ID="TxtFileName5" runat="server" CssClass="txtbox" MaxLength="50" TabIndex="0"></asp:TextBox>
                                                                            </td>
                                                                            <td class="formtdrt">
                                                                                <asp:FileUpload ID="Flu5" runat="server" CssClass="fileUpload" TabIndex="0" />
                                                                                <asp:HiddenField ID="hdnFile5" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="ModalPopupTd" colspan="2">
                                                                                <asp:HiddenField runat="server" ID="hdnpath" />
                                                                                <asp:ImageButton ID="imbAddDoc" runat="server" ImageUrl="~/Images/upload.jpg" ToolTip="Upload"
                                                                                    TabIndex="0" OnClick="imbAdddoc_Click" CausesValidation="true" />
                                                                                &nbsp;
                                                                                <asp:ImageButton ID="imbAttachClose" runat="server" ImageUrl="~/Images/close.jpg"
                                                                                    ToolTip="Close" TabIndex="0" OnClick="imbsubmit_Click" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Panel ID="PanAttach" runat="server" CssClass="popupdivbg1">
                                                                        <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                                                            <tr>
                                                                                <td class="Messageheading" runat="server" id="tdattach">
                                                                                    Attached Document files
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding: 0px">
                                                                                    <table cellpadding="5" cellspacing="0" width="100%" runat="server" id="tblVendors">
                                                                                        <tr>
                                                                                            <td class="formhead2">
                                                                                                Posted By Vendor
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:DataList ID="DataListAttchView" runat="server" DataKeyField="Pk_DocumentID"
                                                                                                    OnSelectedIndexChanged="DataListAttchView_SelectedIndexChanged" OnDeleteCommand="DataListAttchView_DeleteCommand"
                                                                                                    OnItemDataBound="DataListAttchView_ItemDataBound1">
                                                                                                    <%--  <HeaderTemplate>    <table>    <tr><td class="bodytextbold" width="50px" nowrap=nowrap>Serial No</td> <td class="bodytextbold" colspan="2" nowrap=nowrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File Name</td>    
                                                 
                                                  
                                                    </tr></table></HeaderTemplate>
                                                                                                    --%>
                                                                                                    <ItemTemplate>
                                                                                                        <tr height="25px">
                                                                                                            <td class="bodytextleft" style="padding-right: 0px; padding-left: 5px; background-color: #efefef;"
                                                                                                                id="tdRow" runat="server">
                                                                                                                <%# Eval("Row") %>.
                                                                                                            </td>
                                                                                                            <td class="bodytextleft" width="360px" style="background-color: #efefef;">
                                                                                                                <asp:LinkButton CssClass="bodytextleft" ID="FileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                                                    CommandName="select"></asp:LinkButton>
                                                                                                                <asp:HiddenField ID="hdnFileName" runat="server" Value='<%# Eval("Filepath") %>' />
                                                                                                            </td>
                                                                                                            <td align="right" style="background-color: #efefef; padding-right: 5px">
                                                                                                                <%--<asp:Button ID="Delete" Runat="server" Text="Delete" ToolTip="Delete" CommandName="delete" />--%>
                                                                                                                <asp:ImageButton ID="Delete" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                                                                                    CommandName="delete" OnClientClick="javascript:return getConfirmation()" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </ItemTemplate>
                                                                                                </asp:DataList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding: 0px">
                                                                                    <table cellpadding="5" cellspacing="0" width="100%" runat="server" id="tblAdmin">
                                                                                        <tr>
                                                                                            <td class="formhead2">
                                                                                                Posted By Admin
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:DataList ID="dlAdminUpload" OnSelectedIndexChanged="dlAdminUpload_SelectedIndexChanged"
                                                                                                    runat="server" DataKeyField="Pk_DocumentID" OnDeleteCommand="dlAdminUpload_DeleteCommand"
                                                                                                    OnItemDataBound="dlAdminUpload_ItemDataBound">
                                                                                                    <%--  <HeaderTemplate>    <table>    <tr><td class="bodytextbold" width="50px" nowrap=nowrap>Serial No</td> <td class="bodytextbold" colspan="2" nowrap=nowrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File Name</td>    
                                                 
                                                  
                                                    </tr></table></HeaderTemplate>
                                                                                                    --%>
                                                                                                    <ItemTemplate>
                                                                                                        <tr height="25px">
                                                                                                            <td class="bodytextleft" style="padding-right: 0px; padding-left: 5px; background-color: #efefef;"
                                                                                                                id="tdRow" runat="server">
                                                                                                                <%# Eval("Row") %>.
                                                                                                            </td>
                                                                                                            <td class="bodytextleft" style="background-color: #efefef;" width="360px">
                                                                                                                <asp:LinkButton ID="FileName" CssClass="bodytextleft" runat="server" Text='<%# Eval("FileName") %>'
                                                                                                                    CommandName="select"></asp:LinkButton>
                                                                                                                <asp:HiddenField ID="hdnFileName" runat="server" Value='<%# Eval("Filepath") %>' />
                                                                                                            </td>
                                                                                                            <td align="right" style="background-color: #efefef; padding-right: 5px">
                                                                                                                <%--<asp:Button ID="Delete" Runat="server" Text="Delete" ToolTip="Delete" CommandName="delete" />--%>
                                                                                                                <asp:ImageButton ID="Delete" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                                                                                    CommandName="delete" OnClientClick="javascript:return getConfirmation()" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </ItemTemplate>
                                                                                                </asp:DataList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="tdSRattach" runat="server">
                                                                    <asp:Panel ID="PaSREAttach" runat="server" CssClass="popupdivbg1">
                                                                        <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                                                            <tr>
                                                                                <td style="padding: 0px">
                                                                                    <table cellpadding="5" cellspacing="0" width="100%" runat="server" id="Table1">
                                                                                        <tr>
                                                                                            <td class="Messageheading" runat="server" id="td2">
                                                                                                Attached Document files
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formhead2">
                                                                                                Summary Report
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:DataList ID="DataListSRAttchView" runat="server" DataKeyField="Pk_DocumentID"
                                                                                                    OnSelectedIndexChanged="DataListSRAttchView_SelectedIndexChanged" OnDeleteCommand="DataListSRAttchView_DeleteCommand"
                                                                                                    OnItemDataBound="DataListSRAttchView_ItemDataBound">
                                                                                                    <%--  <HeaderTemplate>    <table>    <tr><td class="bodytextbold" width="50px" nowrap=nowrap>Serial No</td> <td class="bodytextbold" colspan="2" nowrap=nowrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File Name</td>    
                                                 
                                                  
                                                    </tr></table></HeaderTemplate>
                                                                                                    --%>
                                                                                                    <ItemTemplate>
                                                                                                        <tr height="25px">
                                                                                                            <td class="bodytextleft" style="padding-right: 0px; padding-left: 5px; background-color: #efefef;"
                                                                                                                id="tdRow" runat="server">
                                                                                                                <%# Eval("Row") %>.
                                                                                                            </td>
                                                                                                            <td class="bodytextleft" width="360px" style="background-color: #efefef;">
                                                                                                                <asp:LinkButton CssClass="bodytextleft" ID="FileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                                                    CommandName="select"></asp:LinkButton>
                                                                                                                <asp:HiddenField ID="hdnFileName" runat="server" Value='<%# Eval("Filepath") %>' />
                                                                                                            </td>
                                                                                                            <td align="right" style="background-color: #efefef; padding-right: 5px">
                                                                                                                <%--<asp:Button ID="Delete" Runat="server" Text="Delete" ToolTip="Delete" CommandName="delete" />--%>
                                                                                                                <asp:ImageButton ID="Delete" runat="server" ImageUrl="~/Images/del.gif" ToolTip="Delete"
                                                                                                                    CommandName="delete" OnClientClick="javascript:return getConfirmation()" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </ItemTemplate>
                                                                                                </asp:DataList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="popupdivcl" align="left">
                                                                                                <asp:Label ID="lblSRmessage" runat="server" CssClass="errormsg"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="center">
                                                                                                <asp:ImageButton ID="imgClose" runat="server" ImageUrl="~/Images/close.jpg" ToolTip="Close"
                                                                                                    TabIndex="0" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="tdheight">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd1" runat="server" TargetControlID="lblHidden1"
                                            PopupControlID="modalPopupDiv1" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalPopupDiv1" class="popupspc" style="display: none">
                                            <table cellpadding="5" cellspacing="0" border="0" width="400" align="center">
                                                <tr>
                                                    <td class="searchhdrbarbold" colspan="2">
                                                        Message
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight" colspan="2">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl" colspan="2">
                                                        <asp:Label ID="msg" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ModalPopupTd" colspan="2">
                                                        <asp:ImageButton ID="ImageButton1" ToolTip="Yes" runat="server" ImageUrl="~/Images/yes.jpg"
                                                            TabIndex="4" />
                                                        &nbsp;
                                                        <asp:ImageButton ID="ImbNo" ToolTip="No" runat="server" ImageUrl="~/Images/no.jpg"
                                                            TabIndex="5" OnClick="ImbNo_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd2" runat="server" TargetControlID="HiddenField3"
                                            PopupControlID="modalPopupDiv2" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="HiddenField3" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="modalPopupDiv2" runat="server" Width="900" Height="500" ScrollBars="Both"
                                            class="popupspc" Style="display: none">
                                            <table cellpadding="0" cellspacing="0" border="0" width="900">
                                                <tr>
                                                    <td class="searchhdrbarbold" width="100%">
                                                        Search Criteria Used
                                                    </td>
                                                </tr>
                                                <tr id="VendorNameHead" runat="server">
                                                    <td class="formhead ">
                                                        &nbsp; &nbsp;Vendor Name
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px">
                                                        <table id="tblVendor" runat="server">
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Vendor Name from Vendor Database:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbloName" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Vendor Number from E1 / JDE:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblVendorNumber" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trTrackingNumber" runat="server">
                                                                <td class="bodytextbold_right">
                                                                    Tracking Number:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblTrackingNumber" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Federal Tax ID / Business ID:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblFederalTaxId" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 07/20/2012 - Added--%>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    First Name:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 07/20/2012 - Added--%>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Last Name:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblLastName" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 07/20/2012 - Added--%>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Email:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trVendorStatus" runat="server">
                                                                <td class="bodytextbold_right">
                                                                    Status:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 02/28/2013 - Added--%>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    BIM (Y/N):
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblProvidesBIM" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 07/15/2014 - Added--%>
                                                            <tr>
                                                                <td class="bodytextbold_right">Type of Company:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblTypeOfCompanySaved" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="trscopes" runat="server">
                                                    <td class="formhead">
                                                        &nbsp; &nbsp;Scopes of Work
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px">
                                                        <asp:Table ID="tblTradeInfo" runat="server" Width="100%">
                                                            <asp:TableRow ID="trRow1" runat="server">
                                                                <asp:TableCell ID="tdCell1" runat="server" VerticalAlign="Top" Width="50%"></asp:TableCell>
                                                                <asp:TableCell ID="tdCell2" runat="server" VerticalAlign="Top" Width="50%"></asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="trGeographicalLocations" runat="server">
                                                    <td class="formhead">
                                                        &nbsp; &nbsp;Geographical Locations
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px">
                                                        <asp:DataList ID="dlRegions" runat="server" RepeatColumns="4" ItemStyle-CssClass="pad_left"
                                                            AlternatingItemStyle-CssClass="pad_left" RepeatDirection="Horizontal" Width="100%"
                                                            RepeatLayout="Table" ItemStyle-Wrap="true" ItemStyle-Width="150px" ItemStyle-VerticalAlign="Top"
                                                            AlternatingItemStyle-VerticalAlign="Top" OnItemDataBound="dlRegions_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Label1" runat="server" CssClass="tree_head1" Text="United States of America"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblName" runat="server" CssClass="tree_head1" Text='<%# Eval("StateCode") %>'></asp:Label>
                                                                <div style="padding-left: 15px;">
                                                                    <asp:Label ID="lstRegions" runat="server" Text='<%# Eval("OtherState") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                        <br />
                                                        <asp:DataList ID="dlContinents" runat="server" RepeatColumns="4" ItemStyle-CssClass="pad_left"
                                                            AlternatingItemStyle-CssClass="pad_left" RepeatDirection="Horizontal" Width="100%"
                                                            RepeatLayout="Table" ItemStyle-Wrap="true" ItemStyle-Width="150px" ItemStyle-VerticalAlign="Top"
                                                            AlternatingItemStyle-VerticalAlign="Top" OnItemDataBound="dlContinents_ItemDataBound">
                                                            <%--    <HeaderTemplate>
                                                                                        <span class="text">Continents</span>
                                                                                    </HeaderTemplate>--%>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblContinent" runat="server" CssClass="tree_head1" Text='<%# Eval("ContinentName") %>'></asp:Label>
                                                                <div style="padding-left: 15px;">
                                                                    <asp:Label ID="lstCountries" runat="server"></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="trTypesOfProjects" runat="server">
                                                    <td class="formhead">
                                                        &nbsp; &nbsp;Types of Projects
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 15px">
                                                        <asp:DataList ID="dlProjects" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                                                            RepeatColumns="3" RepeatDirection="Horizontal" RepeatLayout="Table">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblProjects" runat="server" Text='<%# Eval("ProjectName") %>'></asp:Label></ItemTemplate>
                                                        </asp:DataList>
                                                    </td>
                                                </tr>
                                                <%--G. Vera 10/30/2012 -Added--%>
                                                <tr>
                                                    <td style="padding: 15px">
                                                        <asp:Literal ID="lblSavedOtherProjects" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr id="trCertification" runat="server">
                                                    <td class="formhead">
                                                        &nbsp; &nbsp;Company Certification
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 10px 25px 25px 25px">
                                                        <table id="tblCertification" runat="server">
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Certification:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblCertification" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Federal SB:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblFederalSB" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <%--G. Vera 10/30/2012 - Added--%>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                    Industry Certifications:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblSavedISO" runat="server">
                                                                                                                       
                                                                    </asp:Label>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytextbold_right">
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblSavedQS" runat="server">
                                                                                                                       
                                                                    </asp:Label>&nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ModalPopupTd">
                                                        <asp:ImageButton ID="ImageButton3" ToolTip="Close" runat="server" ImageUrl="~/Images/close.jpg"
                                                            TabIndex="5" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalConfirm" runat="server" TargetControlID="hdnModal"
                                            PopupControlID="modalDiv" BackgroundCssClass="popupbg" CancelControlID="imbCancel"
                                            RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="hdnModal" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalDiv" class="popupdiv" style="display: none">
                                            <table cellpadding="5" cellspacing="0" border="0" width="350">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="Td1">
                                                        Confirm
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        <asp:Label ID="lblConfirm" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextcenter">
                                                        <asp:ImageButton ID="imbOk" ToolTip="Ok" runat="server" ImageUrl="~/Images/ok.jpg"
                                                            OnClick="imgOK_Click" />
                                                        &nbsp;
                                                        <asp:ImageButton ID="ImageButton2" ToolTip="Cancel" runat="server" ImageUrl="~/Images/cancel.jpg" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalName" runat="server" TargetControlID="hdndiv" CancelControlID="imbCancel1"
                                            PopupControlID="namediv" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="hdndiv" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="namediv" class="popupspc" style="display: none">
                                            <table cellpadding="5" cellspacing="0" border="0" width="250" align="center">
                                                <tr>
                                                    <td class="searchhdrbarbold" colspan="2">
                                                        Save This Search
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="tdheight">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl" colspan="2">
                                                        <asp:Label ID="lblvalid" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="bodytextright" width="25%">
                                                        Name
                                                    </td>
                                                    <td class="bodytextleft">
                                                        <asp:TextBox ID="txtName" runat="server" MaxLength="500"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td class="bodytextleft">
                                                        <table>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:ImageButton ID="imbAddSave" ToolTip="Submit" runat="server" ImageUrl="~/Images/submit.jpg"
                                                                        TabIndex="4" OnClick="imbAddSave_Click" />
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:ImageButton ID="imbCancel1" ToolTip="Cancel" runat="server" ImageUrl="~/Images/cancel.jpg"
                                                                        TabIndex="5" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalInfo" CancelControlID="imbInfoOK" runat="server"
                                            TargetControlID="hdnInfo" PopupControlID="DivInfo" BackgroundCssClass="popupbg"
                                            RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="hdnInfo" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="DivInfo" class="popupspc" style="display: none">
                                            <table cellpadding="5" cellspacing="0" border="0" width="400" align="center">
                                                <tr>
                                                    <td class="searchhdrbarbold" colspan="2">
                                                        Message
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight" colspan="2">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="left_border" style="padding-left: 20px" colspan="2">
                                                        <asp:Label ID="lblInfo" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ModalPopupTd" colspan="2">
                                                        <asp:ImageButton ID="imbInfoOK" ToolTip="Ok" runat="server" ImageUrl="~/Images/ok.jpg"
                                                            TabIndex="4" />
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                            <asp:HiddenField ID="hdnCount" runat="server" />
                            <asp:HiddenField ID="hdnFlag" runat="server" />
                            <asp:HiddenField ID="hdnFlag1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>


</body>
</html>
<script language="javascript" type="text/javascript">

    

    //G. Vera 10/19/2012 - Added
    function OpenHideTexBox(chkBoxCtrl) {
        var isChecked = chkBoxCtrl.checked;
        var textBox = document.getElementById(chkBoxCtrl.id.replace('chk', 'txt'));
        if (isChecked) {

            textBox.style.display = "";
        }
        else {
            textBox.style.display = "none"
            textBox.value = "";
        }
    }

    //G. Vera 10/23/2012 - Added
    function SwitchScopePanel(closePanelID, closePanelID2, openPanelID, typeIsOpen) {

        var openPanel = document.getElementById(openPanelID);
        var closePanel = document.getElementById(closePanelID);
        var closePanel2 = document.getElementById(closePanelID2);
        var typeOpen = document.getElementById("<%=hdnScopeTypeOpen.ClientID %>");
        openPanel.style.display = '';
        openPanel.visible = true;
        typeOpen.value = typeIsOpen;
        closePanel.style.display = 'none';
        closePanel.visible = false;
        closePanel2.style.display = 'none';
        closePanel2.visible = false;
        if (typeIsOpen == "PMMI") {
            document.getElementById("liPMMI").className = "selected";
            document.getElementById("liMasterFormat").className = "";
            document.getElementById("liDesign").className = "";
        }
        if (typeIsOpen == "MasterFormat") {
            document.getElementById("liMasterFormat").className = "selected";
            document.getElementById("liPMMI").className = "";
            document.getElementById("liDesign").className = "";
        }
        //006-Sooraj - Modified for Design Tab Enhancement 
        if (typeIsOpen == "Design") {
            document.getElementById("liDesign").className = "selected";
            document.getElementById("liPMMI").className = "";
            document.getElementById("liMasterFormat").className = "";
        }
    }


    function unCheckCheckALL(CheckState) {
        if (CheckState == false) {
            document.getElementById("<%=chkCheckAll.ClientID%>").checked = false;
        }
    }
    function AssignValue(hdn) {
        document.getElementById(hdn).value = "1";
    }
    function DisplayRegions(chec, checkStatus, OtherState) {
        var ch1 = document.getElementById(checkStatus);
        if (ch1.checked == true) {
            var ch = document.getElementById(chec);
            ch.style.display = "";
        }
        else {
            var ch = document.getElementById(chec);
            ch.style.display = "none";

            if (OtherState == true) {
                document.getElementById("<%=txtRegions.ClientID%>").value = "";
            }
        }
        unCheckCheckALL(ch1.checked);

    }
    //    function DisplayLevel2(chec, checkStatus) {
    //        debugger;
    //        var ch1 = document.getElementById(checkStatus);

    //        if (ch1.checked == true) {
    //            var ch = document.getElementById(chec);
    //            ch.style.display = "";
    //            var chkBoxCount = ch.getElementsByTagName("input");
    //            for (var i = 0; i < chkBoxCount.length; i++) {
    //                chkBoxCount[i].checked = true;
    //            }
    //        }
    //        else {
    //            var ch = document.getElementById(chec);
    //            ch.style.display = "none";
    //            var chkBoxCount = ch.getElementsByTagName("input");
    //            for (var i = 0; i < chkBoxCount.length; i++) {
    //                chkBoxCount[i].checked = false;
    //            }
    //        }
    //    }
    function GetValue(i, ctrl) {
        var ctrl1 = document.getElementById(ctrl);
        ctrl1.value = i;
    }
    function GetChecked(chk, chkStatus, ctrl) {
        if (chk == "ALL") {
            if (chkStatus == true) {
                var ch = document.getElementById(ctrl);
                var chkBoxCount = ch.getElementsByTagName("input");
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = true;
                }
            }
            else {
                var ch = document.getElementById(ctrl);

                var chkBoxCount = ch.getElementsByTagName("input");
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = false;
                }
            }
        }
        else {
            var ch = document.getElementById(ctrl);
            var chkBoxCount = ch.getElementsByTagName("input");
            //G. Vera - added
            for (var i = 0; i < chkBoxCount.length; i++) {
                chkBoxCount[i].checked = chkStatus;
            }
            //chkBoxCount[0].checked = false;
        }
        unCheckCheckALL(chkStatus);
    }
    function ShowPopup(panel) {
        var pnl = document.getElementById(panel);
        pnl.style.display = "Block";
    }
    //Hides DIV popup commands for gridview
    function HidePopup(panel) {
        var pnl = document.getElementById(panel);
        pnl.style.display = "none";
    }

</script>
<script language="javascript" type="text/javascript">

    //G. Vera 10/16/2012 - Added
    function ClearTextBox(chkBox) {

        if (!chkBox.checked) {
            document.getElementById("<%=txtChkOther.ClientID%>").value = "";
            document.getElementById("<%=txtChkOther.ClientID%>").style.display = "none";
        }

        if (chkBox.checked) document.getElementById("<%=txtChkOther.ClientID%>").style.display = "";

    }

    function ShowFile(fValue, fName) {
        alert(fValue);
        document.getElementById(fName).value = fValue;
    }
    function ShowCertContents(text, ctrl, chk) {
        if (text == "Other") {
            if (chk == true) {
                document.getElementById("<%=trotherCertification.ClientID%>").style.display = "";
            }
            else {
                document.getElementById("<%=trotherCertification.ClientID%>").style.display = "None";
                document.getElementById("<%=txtCertiOth.ClientID%>").value = "";
            }
        }
    }
    function ShowFederalContents(text, ctrl, chk) {
        if (text == "Other") {
            if (chk == true) {
                document.getElementById("<%=spnFederalSB.ClientID%>").style.display = "";
            }
            else {
                document.getElementById("<%=spnFederalSB.ClientID%>").style.display = "None";
                document.getElementById("<%=txtCertiOth.ClientID%>").value = "";
            }
        }
    }
    function CheckDocument() {
        var msg = "";
        //Document 1
        if (trimAll(document.form1.Flu1.value) != "") {
            var ext = document.form1.Flu1.value;
            var len = document.form1.Flu1.value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();

                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = "";
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file one</li> ";
                }
            }
        }
        //Document 2
        if (trimAll(document.form1.Flu2.value) != "") {
            var ext = document.form1.Flu2.value;
            var len = document.form1.Flu2.value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();

                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file two </li> ";
                }
            }
        }
        //Document 3
        if (trimAll(document.form1.Flu3.value) != "") {
            var ext = document.form1.Flu3.value;
            var len = document.form1.Flu3.value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();

                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file three</li> ";
                }
            }
        }
        //Document 4
        if (trimAll(document.form1.Flu4.value) != "") {
            var ext = document.form1.Flu4.value;
            var len = document.form1.Flu4.value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();

                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file four</li> ";
                }
            }
        }
        //Document 5
        if (trimAll(document.form1.Flu5.value) != "") {
            var ext = document.form1.Flu5.value;
            var len = document.form1.Flu5.value.lastIndexOf(".");
            if (ext.length > 0) {
                ext = ext.substring(len, ext.length);
                ext = ext.toLowerCase();

                if (ext != ".gif" && ext != ".jpg" && ext != ".xls" && ext != ".xlsx" && ext != ".jpeg" && ext != ".txt" && ext != ".pdf" && ext != ".doc" && ext != ".docx") {
                    msg = msg + "<li>Please upload only gif/ jpg/ jpeg/ xls/ xlsx/ txt/ pdf/ doc/ docx format for file five</li>";
                }
            }
        }
        if (msg == "") return true;
        else {
            var txt = document.getElementById('<%=lblattach.ClientID%>');
            txt.innerHTML = msg;
            return false;
        }
    }       
</script>
<script language="javascript" type="text/javascript">

    function VisibleRegions(chkid, spnid, rdlid) {
        var chk = document.getElementById(chkid);
        if (chk.checked == true) {
            document.getElementById(spnid).style.display = "";
            document.getElementById(rdlid).focus();
        }
        else {
            var ch = document.getElementById(rdlid);
            var num = ch.childNodes[0].childNodes.length;
            var currentTable = ch.childNodes[0];
            for (var i = 0; i < num; i++) {
                var innetText = currentTable.childNodes[i].childNodes[0].value;
                var currentTd = currentTable.childNodes[i].childNodes[0];
                var listControl = currentTd.childNodes[0];
                listControl.checked = false;
            }
            document.getElementById(spnid).style.display = "none";
        }
    }
    function DisplayLevel2(chec, checkStatus) {
        var ch1 = document.getElementById(checkStatus);
        if (ch1.checked == true) {
            var ch = document.getElementById(chec);
            ch.style.display = "";
            var num = ch.childNodes[0].childNodes.length;
            var currentTable = ch.childNodes[0];
            for (var i = 0; i < num; i++) {
                var innetText = currentTable.childNodes[i].childNodes[0].value;
                var currentTd = currentTable.childNodes[i].childNodes[0];
                var listControl = currentTd.childNodes[0];
                listControl.checked = true;
            }
        }
        else {
            var ch = document.getElementById(chec);
            ch.style.display = "none";
            var num = ch.childNodes[0].childNodes.length;
            var currentTable = ch.childNodes[0];
            for (var i = 0; i < num; i++) {
                var innetText = currentTable.childNodes[i].childNodes[0].value;
                var currentTd = currentTable.childNodes[i].childNodes[0];
                var listControl = currentTd.childNodes[0];
                listControl.checked = false;
            }
        }
    }
    function ShowSearchBy() {
        if (document.getElementById("<%=chlSearchBy.ClientID%>_0").checked) {
            document.getElementById("<%=spnVendorDetails.ClientID %>").style.display = "";
        }
        else {
            document.getElementById("<%=spnVendorDetails.ClientID %>").style.display = "none";
            document.getElementById("<%=txtVendor.ClientID %>").value = "";
            document.getElementById("<%=txtVendorNumber.ClientID %>").value = "";
            document.getElementById("<%=txtTrackingNumber.ClientID %>").value = "";
            document.getElementById("<%=txtFederalTax2.ClientID %>").value = "";
            document.getElementById("<%=txtFederalTax2.ClientID %>").value = "";
            //G. Vera 02/27/2013 - added to clear status drop down
            document.getElementById("<%=ddlStatus.ClientID %>").options[0].selected = true;
        }
        if (document.getElementById("<%=chlSearchBy.ClientID%>_1").checked) {
            document.getElementById("<%=spnCSICode.ClientID %>").style.display = "";
            var typeOpen = document.getElementById("<%=hdnScopeTypeOpen.ClientID %>");
            if (typeOpen.value == "PMMI") {
                document.getElementById("<%=updPnlPMMI.ClientID %>").style.display = "";
                document.getElementById("<%=updpnlScope.ClientID %>").style.display = "none";
                document.getElementById("<%=updPnlDesign.ClientID %>").style.display = "none";
                document.getElementById("liPMMI").className = "selected";
                document.getElementById("liMasterFormat").className = "";
                document.getElementById("liDesign").className = "";
            }
            if (typeOpen.value == "Design") {

                document.getElementById("<%=updPnlDesign.ClientID %>").style.display = "";
                document.getElementById("<%=updpnlScope.ClientID %>").style.display = "none";
                document.getElementById("<%=updPnlPMMI.ClientID %>").style.display = "none";
                document.getElementById("liDesign").className = "selected";
                document.getElementById("liPMMI").className = "";
                document.getElementById("liMasterFormat").className = "";
            }
            if (typeOpen.value == "MasterFormat") {

                document.getElementById("<%=updpnlScope.ClientID %>").style.display = "";
                document.getElementById("<%=updPnlPMMI.ClientID %>").style.display = "none";
                document.getElementById("<%=updPnlDesign.ClientID %>").style.display = "none";
                document.getElementById("liPMMI").className = "";
                document.getElementById("liMasterFormat").className = "selected";
                document.getElementById("liDesign").className = "";

            }
        }
        else {
            var container = document.getElementById("<%= spnCSICode.ClientID %>");
            var single = container.getElementsByTagName("input");
            var size = single.length;
            var typeOpen = document.getElementById("<%=hdnScopeTypeOpen.ClientID %>");
            typeOpen.value = "MasterFormat";

            for (i = 0; i < size; i++) {
                //G. Vera 04/05/2013 - added lines below
                if (single[i].type == "checkbox") {
                    single[i].checked = false;

                }
                if (typeof single[i].onclick == "function") {

                    single[i].onclick.apply(single[i]);
                }
            }
            var container1 = document.getElementById("<%= pnlRegions.ClientID %>");
            var div1 = container1.getElementsByTagName("div");

            for (i = 0; i < div1.length; i++) {
                if (div1[i].id != "" && div1[i].style.display == "")
                    div1[i].style.display = "none";
            }

            //G. Vera 04/05/2013 - Added below:
            var images = container.getElementsByTagName("img")
            for (i = 0; i < images.length; i++) {
                //G. Vera 04/05/2013 - added lines below
                if (images[i].src.toString().indexOf("minus") != -1) {
                    if (typeof images[i].onclick == "function") {
                        images[i].onclick.apply(images[i]);
                    }
                }
            }


            document.getElementById("<%=spnCSICode.ClientID %>").style.display = "none";
            document.getElementById("<%=updpnlScope.ClientID %>").style.display = "none";
            document.getElementById("<%=updPnlPMMI.ClientID %>").style.display = "none";
        }
        if (document.getElementById("<%=chlSearchBy.ClientID%>_2").checked) {
            document.getElementById("<%=spnLocation.ClientID %>").style.display = "";

            //Code for handling collapse issue 
            var chkUS = document.getElementById("<%=chkUS.ClientID%>");
            var divUS = document.getElementById("<%=divUS.ClientID%>");
            if (chkUS.checked)
                divUS.style.display = "";

            var chkContinent = document.getElementById("grdContinent_ctl01_chkContinent");
            var divContinent = document.getElementById("grdContinent_ctl01_divCountries");
            for (var i = 1; i <= 8; i++) {
                chkContinent = document.getElementById("grdContinent_ctl0" + i + "_chkContinent");
                divContinent = document.getElementById("grdContinent_ctl0" + i + "_divCountries");
                if (chkContinent!=null && chkContinent.checked)
                    divContinent.style.display = ""
            }
        }
        else {
            // var container = document.getElementById("<%= spnLocation.ClientID %>");
            var container = document.getElementById("<%=divUS.ClientID %>");
            var single = container.getElementsByTagName("input");
            var size = single.length;
            for (i = 0; i < size; i++) {
                //G. Vera 04/05/2013 - added lines below
                if (single[i].type == "checkbox") single[i].checked = false;
                if (typeof single[i].onclick == "function") {
                    single[i].onclick.apply(single[i]);
                }
            }
            container = document.getElementById("<%= spnLocation.ClientID %>");
            single = container.getElementsByTagName("input");
            size = single.length;
            for (i = 0; i < size; i++) {
                if (single[i].type == "checkbox") single[i].checked = false;
            }
            document.getElementById("<%=spnLocation.ClientID %>").style.display = "none";
        }
        if (document.getElementById("<%=chlSearchBy.ClientID%>_3").checked) {
            document.getElementById("<%=SpnTOP.ClientID %>").style.display = "";
        }
        else {
            var container = document.getElementById("<%= SpnTOP.ClientID %>");
            var single = container.getElementsByTagName("input");
            var size = single.length;
            for (i = 0; i < size; i++) {
                //G. Vera 04/05/2013 - added lines below
                if (single[i].type == "checkbox") single[i].checked = false;
                if (typeof single[i].onclick == "function") {

                    single[i].onclick.apply(single[i]);
                }
            }
            document.getElementById("<%=SpnTOP.ClientID %>").style.display = "none";
        }
        if (document.getElementById("<%=chlSearchBy.ClientID%>_4").checked) {
            document.getElementById("<%=divCertification.ClientID %>").style.display = "";
        } else {
            var container = document.getElementById("<%= divCertification.ClientID %>");
            var single = container.getElementsByTagName("input");
            var size = single.length;
            for (i = 0; i < size; i++) {
                //G. Vera 04/05/2013 - added lines below
                if (single[i].type == "checkbox") single[i].checked = false;
                if (typeof single[i].onclick == "function") {

                    single[i].onclick.apply(single[i]);
                }
            }
            document.getElementById("<%=trotherCertification.ClientID%>").style.display = "None";
            document.getElementById("<%=spnFederalSB.ClientID%>").style.display = "None";
            document.getElementById("<%=divCertification.ClientID %>").style.display = "None";
        }
    }
    function CheckAllRegions(Status, chkl, chkl1, chkl2, txt1) {
        var chk = document.getElementById(Status);
        var container = document.getElementById("<%=divUS.ClientID %>");
        var single = container.getElementsByTagName("input");
        var size = single.length;
        var j = 0;
        if (chk.checked == true) {
            document.getElementById(chkl).style.display = "";
            document.getElementById(chkl1).style.display = "";
            if (document.getElementById(chkl2) != null)
                document.getElementById(chkl2).style.display = "";
            document.getElementById(txt1).style.display = "";
            for (i = 0; i < size; i++) {
                if (single[i].value == "51") {
                    single[i].checked = false;
                    j = i + 1;
                    document.getElementById("<%=trRegions.ClientID%>").style.display = "none";
                }
                else {
                    single[i].checked = true;
                }
            }
            single[j].checked = false;

        }
        else {
            document.getElementById(chkl).style.display = "None";
            document.getElementById(chkl1).style.display = "None";
            if (document.getElementById(chkl2) != null)
                document.getElementById(chkl2).style.display = "None";
            document.getElementById(txt1).style.display = "None";
            for (i = 0; i < size; i++) {
                single[i].checked = false;
                document.getElementById("<%=txtRegions.ClientID%>").value = "";
            }
        }
    }
    function OpenGrid(grd, imgurl, img) {
        if (document.getElementById(imgurl).value == "~/Images/plus.gif") {
            document.getElementById(grd).style.display = "";
            document.getElementById(img).src = "../Images/minus.gif";
            document.getElementById(imgurl).value = "~/Images/minus.gif";
        }
        else {
            document.getElementById(grd).style.display = "none";
            document.getElementById(img).src = "../Images/plus.gif";
            document.getElementById(imgurl).value = "~/Images/plus.gif";
        }
    }
    //Sooraj to Add Continent
    function CheckUSRegions(Status, chkl, chkl1, chkl2, txt1, control, controlToSwitch) {
        var container = document.getElementById("<%=divUS.ClientID %>");
        var single = container.getElementsByTagName("input");
        var size = single.length;
        if (Status == false) {
            document.getElementById(chkl).style.display = "None";
            document.getElementById(chkl1).style.display = "None";
            if (document.getElementById(chkl2) != null)
                document.getElementById(chkl2).style.display = "None";
            document.getElementById(txt1).style.display = "None";
            for (i = 0; i < size; i++) {
                single[i].checked = false;
                document.getElementById("<%=txtRegions.ClientID%>").value = "";
            }

        }
        SwitchView(control, controlToSwitch);
    }
    //Sooraj to Add Continent
    function SwitchView(control, controlToSwitch) {
        var ch = document.getElementById(control);
        var controlToHide = document.getElementById(controlToSwitch);
        if (controlToHide != null && ch != null) {
            if (ch.checked == true) {
                controlToHide.style.display = "";
            }
            else {
                controlToHide.style.display = "None";
                UnCheckContinent(controlToHide);
            }
        }

    }
    function UnCheckContinent(container) {

        var single = container.getElementsByTagName("input");
        if (single != null) {
            var size = single.length;

            for (i = 0; i < size; i++) {
                single[i].checked = false;
            }
        }

    }
</script>
