﻿using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using VMSDAL;
#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)

 * 002 Sooraj Sudhakaran.T 10/04/2013: VMS Modification - Added code to handle International Business Id

 *
 ****************************************************************************************************************/
#endregion

public partial class Admin_ResetPassword : CommonPage
{

    //Instantiate the object for the classes used in this page
    clsRegistration objRegistration = new clsRegistration();
    clsAdminLogin objAdmin = new clsAdminLogin();
    Common objCommon = new Common();

    /// <summary>
    /// Page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        ShowHideTaxControls();
        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            //Check for the session variables if null redirect to the login page else proceed.
            if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
            }
            else
            {
                if (Convert.ToString(Session["Access"]) == "2")
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                {
                    AdminMenu.DisplayMenu("Admin");
                }
                mnuMain.DisplayButton(5);
                txtTax1.Attributes.Add("onkeyup", "javascript:SetFocusOnTax(" + txtTax1.ClientID + "," + txtTax2.ClientID + ")");
                EnableDisableControls(true);
            }
        }
     }


    /// <summary>
    /// Display the tax control based on selected Country
    /// </summary>
    /// <Author>Sooraj Sudhakaran.T</Author>
    /// <Date>3-OCT-2013</Date>
    private void ShowHideTaxControls()
    {
        if (ddlCountry.SelectedIndex == 0)//USA
        {
            txtBusinessTax.Text = string.Empty;
            trUSA.Style["display"] = "";
            trOther.Style["display"] = "none";
        }
        else
        {
            txtTax1.Text = string.Empty;
            txtTax2.Text = string.Empty;
            trOther.Style["display"] = "";
            trUSA.Style["display"] = "none";
        }

    }

    /// <summary>
    /// Method to send e-mail
    /// </summary>
    /// <param name="FirstName"></param>
    /// <param name="LastName"></param>
    /// <param name="email"></param>
    private void MailContant(string FirstName,string LastName,string email)
    {
        StringBuilder Body = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='70%'>");
        Body.Append("<tr><td><font  face='verdana' size='2'>Hi " + txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim()  +", <br></font></td></tr>");
        Body.Append("<tr><td style='Padding-left:25px'><font  face='verdana' size='2'>Your profile has been changed by the Haskell Administrator<br><br> Please find your updated profile below: <br></font></td></tr>");
        Body.Append("<tr><td style='Padding-left:25px'><font  face='verdana' size='2'>FirstName: " + txtFirstName.Text.Trim() + "<br></font></td></tr>");
        Body.Append("<tr><td style='Padding-left:25px'><font  face='verdana' size='2'>LastName : " + txtLastName.Text.Trim() + "<br></font></td></tr>");
        Body.Append("<tr><td style='Padding-left:25px'><font  face='verdana' size='2'>Email    : " + txtEmail.Text.Trim() + "<br></font></td></tr>");
        Body.Append("<tr><td style='Padding-left:25px'><font  face='verdana' size='2'>Password : " + txtNewPassword.Text.Trim() + "<br><br><br></font></td></tr>");
        Body.Append("<tr><td><font  face='verdana' size='2'>Respectfully,</font><br/><br/></td></tr>");
        Body.Append("<tr><td><font  face='verdana' size='2'>Haskell</font><br></td></tr></table>");
        Common objCommon = new Common();
        //Send e-mail
        objCommon.SendMail(ConfigurationManager.AppSettings["adminemail"].ToString(),txtEmail.Text.Trim(), "Haskell: Changed Profile", Body);
    }

    /// <summary>
    /// To Enable or to disable the controls
    /// </summary>
    /// <param name="Status"></param>
    private void EnableDisableControls(bool Status)
    {
        txtCompany.ReadOnly = Status;
        txtTax1.ReadOnly = Status;
        txtTax2.ReadOnly = Status;
        txtFirstName.ReadOnly = Status;
        txtLastName.ReadOnly = Status;
        txtEmail.ReadOnly = Status;
        txtNewPassword.ReadOnly = Status;
        txtCPassword.ReadOnly = Status;
        txtBusinessTax.ReadOnly = Status; //002-Sooraj
        ddlCountry.Enabled=!Status;
    }

    /// <summary>
    /// Method to Update the profile of the selected vendor
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbsubmit_Click(object sender, ImageClickEventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            //Call method to update vendor details
            if (ddlCountry.SelectedIndex == 0)  //002-Sooraj
                lblErrMsg.Text = objRegistration.UpdateVendorPassword(txtVendor.Text.Trim(), txtCompany.Text.Trim().ToUpper(), txtTax1.Text.Trim() + txtTax2.Text.Trim(), txtFirstName.Text.Trim(), txtLastName.Text.Trim(), Convert.ToInt32(hdnVendorId.Value), txtEmail.Text.Trim(), objCommon.encode(txtNewPassword.Text.Trim()), hdnPassword.Value,true);
            else
                lblErrMsg.Text = objRegistration.UpdateVendorPassword(txtVendor.Text.Trim(), txtCompany.Text.Trim().ToUpper(), txtBusinessTax.Text, txtFirstName.Text.Trim(), txtLastName.Text.Trim(), Convert.ToInt32(hdnVendorId.Value), txtEmail.Text.Trim(), objCommon.encode(txtNewPassword.Text.Trim()), hdnPassword.Value,false);
            modalExtnd.Show();
            txtEmail.Text = string.Empty;
            hdnemail.Value = txtEmail.Text;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtTax1.Text = string.Empty;
            txtTax2.Text = string.Empty;
            txtBusinessTax.Text = string.Empty; //002-Sooraj
            ddlCountry.SelectedIndex = 0;
            txtCPassword.Attributes["value"] = "";
            txtNewPassword.Attributes["value"] = "";
            txtCompany.Text = string.Empty;
            txtVendor.Text = string.Empty;
            //MailContant(txtFirstName.Text.Trim(), txtLastName.Text.Trim(), txtEmail.Text.Trim());         
            EnableDisableControls(true);
        }
        else
        {
            lblErrMsg.Text = Errormsg();
            modalExtnd.Show();
        }
    }

    /// <summary>
    /// Method to validate the page and return the result
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                errmsg += "<li>" + validator.ErrorMessage + "</li>";
            }
        }
        return errmsg;
    }

    /// <summary>
    /// Validate the password to have the characters greater than 5
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void custNewPassword_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtNewPassword.Text.Trim()))
        {
            if (txtNewPassword.Text.Trim().Length < 6)
            {
                custNewPassword.ErrorMessage = "Password should contain at least six characters";
                args.IsValid = false;
            }
        }
    }

    /// <summary>
    /// validate the confirm password textbox should contain at least six character 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void custPassword_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(txtCPassword.Text.Trim()))
        {
            if (txtCPassword.Text.Trim().Length < 6)
            {
                custPassword.ErrorMessage = "Password should contain at least six characters";
                args.IsValid = false;
            }
        }
    }

    /// <summary>
    /// Display the details of the selected vendor in the corresponding textbox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlName_SelectedIndexChanged(object sender, EventArgs e)
    {

     

        if (txtVendor.Text.Trim() != "")
        {
            //Call method to get the vendor details
            DAL.Vendor objResult = objRegistration.GetVendorByName(txtVendor.Text.Trim());
            if (objResult != null)
            {
                hdnVendorId.Value = Convert.ToString(objResult.PK_VendorID);
                txtEmail.Text = objResult.Email;
                hdnemail.Value = txtEmail.Text.Trim();
                txtCompany.Text = objResult.Company;
                bool IsBasedInUS = objResult.IsBasedInUS;

                string TaxId = objResult.TaxID.ConvertToTaxId(IsBasedInUS);

                if (objResult.TaxID != null && !IsBasedInUS)  //Business ID 
                {
                    ddlCountry.SelectedIndex = 1;
                    txtBusinessTax.Text = TaxId;//002-Sooraj
                }
                else
                {
                    ddlCountry.SelectedIndex = 0;
                    if (TaxId.Length >= 9)
                    {
                        txtTax1.Text = TaxId.Substring(0, 2);
                        txtTax2.Text = TaxId.Substring(2);  //Fixed an existing bug
                    }
                }
                ShowHideTaxControls();
              
                txtFirstName.Text = objResult.FirstName;
                txtLastName.Text = objResult.LastName;
                hdnPassword.Value = objResult.Password;
                txtCPassword.Attributes["value"] = objCommon.decode(objResult.Password);
                txtNewPassword.Attributes["value"] = objCommon.decode(objResult.Password);

                EnableDisableControls(false);
            }
            else
            {
                txtEmail.Text = string.Empty;
                hdnemail.Value = txtEmail.Text;
                txtFirstName.Text = string.Empty;
                txtLastName.Text = string.Empty;
                txtTax1.Text = string.Empty;
                txtTax2.Text = string.Empty;
                txtCPassword.Attributes["value"] = "";
                txtNewPassword.Attributes["value"] = "";
                txtCompany.Text = string.Empty;
                txtBusinessTax.Text=string.Empty;
                ddlCountry.SelectedIndex=0;
                lblErrMsg.Text = "<li>" + "Please enter correct vendor name" + "</li>";
                modalExtnd.Show();
                EnableDisableControls(true);
            }
        }
        else
        {
            txtEmail.Text = string.Empty;
            hdnemail.Value = txtEmail.Text;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtTax1.Text = string.Empty;
            txtTax2.Text = string.Empty;
            txtCPassword.Attributes["value"] = "";
            txtNewPassword.Attributes["value"] = "";
            txtCompany.Text = string.Empty;
             txtBusinessTax.Text=string.Empty;
                ddlCountry.SelectedIndex=0;
            EnableDisableControls(true);
        }
    }

    //validate federal tax id
    protected void Cusfederaltax_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if (((txtTax1.Text == "") && ((txtTax2.Text == "")) && ((txtBusinessTax.Text == ""))))
        {
            Cusfederaltax.ErrorMessage = "Please enter Tax ID";
            args.IsValid = false;
        }
        else
        {
            if (ddlCountry.SelectedIndex == 0)
            {
                bool isValidFormat = objCommon.Validatefederaltaxformat(txtTax1.Text, txtTax2.Text);
                if ((isValidFormat == false))
                {
                    Cusfederaltax.ErrorMessage = "Please enter Tax ID in valid format";
                    args.IsValid = false;
                }
            }
        }
    }
}