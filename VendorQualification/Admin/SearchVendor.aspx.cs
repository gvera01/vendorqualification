﻿using System;
using System.Data;
using System.Linq;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Configuration;
using System.Collections.Generic;
using VMSDAL;
#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 08/01/2012: VMS Stabilization Release 1.1 (JIRA:VPI-30-Reopened)
 * 003 G. Vera 07/16/2012: VMS Stabilization Release 1.2 - New search by fields (JIRA:VPI-18)
 * 004 G. Vera 10/17/2012: VMS Stabilization Release 1.3 (refer to JIRA issues with this release)
 * 005 G. Vera 02/26/2013: VMS Enhancements Release 1.3.1 (BIM Questions request)
 * 006 Sooraj Sudhakaran.T 02/26/2013: VMS Enhancements Phase 3 (Design Consultant Scope)
 * 007 Sooraj Sudhakaran.T 10/04/2013: VMS Enhancements Phase 3 (International Tax ID/ Business ID)
 * 008 Sooraj Sudhakaran.T 10/11/2013: VMS Modification - Added code to handle continent selection for trade regions 
 * 009 G. Vera 07/07/2014: Store the panel opened by user in Session and display accordingly on re-populate from previous
 * 010 G. Vera 07/14/2014: Search by Type of Company
 * 011 G. Vera 01/05/2016: Modify code to use the default Max File Size from Web.config
 * 012 G. Vera 03/24/2017: Add tool tip to attachments column listing all file names, new financial column, ratings link
 ****************************************************************************************************************/
#endregion

public partial class Admin_SearchVendor : CommonPage 
{
    //Instantiate the class and declare varaibles
    clsTradeInfomation objTradeInformation = new clsTradeInfomation();
    clsRegistration objRegistration = new clsRegistration();
    clsCompany objCompany = new clsCompany();
    Common objCommon = new Common();
    clsSearchVendor objSearchVendor = new clsSearchVendor();
    public DataSet Vendordetails = new DataSet();
    DataSet SRAttch = new DataSet();
    DataSet Attch = new DataSet();
    DataSet Attch1 = new DataSet();
    public string SortExp;
    public string SearchSortExp;
    string DOCUMENT_LOCATION = string.Empty;
    string sMimeType;
    private static int MAXFILESIZE = Convert.ToInt32(ConfigurationManager.AppSettings["MaxFileSizeMBs"].ToString());    //011 - new field
    private const string emptyRatingStar = "<span class=\"ratingStarEmpty\" ><img src=\"/images/rating/ratingStarEmpty.png\"/></span>"; //012
    private const string filledRatingStar = "<span class=\"ratingStarFilled\" ><img src=\"/images/rating/ratingStarFilled.png\"/></span>";  //012
    private const string greenDollar = "<span class=\"dollars\">$</span>";  //012
    private const string greyDollar = "<span class=\"dollars-grey\">$</span>";  //012

    string strFileName1, strFileName2, strFileName3, strFileName4, strFileName5;
    RiskEvaluation riskEvaluation = new RiskEvaluation();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        string str = Request.Form["__EVENTTARGET"];
        if ((!string.IsNullOrEmpty(hdnSearchPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["SavedPage"]))) && str.ToUpperInvariant() != "DDLSEARCHNUMBER" && hdnFlag1.Value != "1")
        {

            for (int i = (Convert.ToInt32(hdnSearchPage.Value) - 5); i <= (Convert.ToInt32(hdnSearchPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["SavedPage"]))
                {

                    LinkButton lnk = new LinkButton();
                    lnk.Click += new EventHandler(lnk_Click);
                    lnk.Text= Convert.ToString(i);
                    if (i == Convert.ToInt32(hdnPage.Value))
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnk.CssClass = "lnkcolor";
                    }
                    plcCrntPage.Controls.Add(lnk);
                    lnk.Text= Convert.ToString(i);
                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnSearchPage.ClientID + "')");
                }
            }
        }
        string str1 = Request.Form["__EVENTTARGET"];
        if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page"]))) && str1.ToUpperInvariant() != "DDLNUMBER" && hdnFlag.Value != "1")
        {
            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {
                    LinkButton lnk = new LinkButton();
                    lnk.Click += new EventHandler(lnk1_Click);
                    lnk.Text= Convert.ToString(i);
                    if (i == Convert.ToInt32(hdnPage.Value))
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnk.CssClass = "lnkcolor";
                    }
                    plcCrntPage1.Controls.Add(lnk);
                    lnk.Text= Convert.ToString(i);
                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnPage.ClientID + "')");
                }
            }
        }
    }

    /// <summary>
    /// Bind States and regions and raise event to display the regions for the selected state
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdn = (HiddenField)e.Row.FindControl("hdnFlag");
            HiddenField hdnId = e.Row.FindControl("hdnParentScopeId") as HiddenField;         //005

            DAL.UspGetTradeScopesResult[] objScopes = objTradeInformation.GetTradeScope(Convert.ToInt32(((HiddenField)e.Row.FindControl("hdnParentScopeId")).Value));

            GridView grd = new GridView();
            grd = (GridView)e.Row.FindControl("grdScopeLevele2");
            grd.DataSource = objScopes;
            grd.DataBind();
            grd.Style["Display"] = "None";
            Image img = new Image();
            img = (Image)e.Row.FindControl("imgStatus");
            hdn.Value = img.ImageUrl;
            if (grd.Rows.Count > 0)
            {
                img.Attributes.Add("onClick", "OpenGrid('" + grd.ClientID + "','" + hdn.ClientID + "','" + img.ClientID + "');");
            }

            //005  
            string[] searchOptions = null;
            
            if (Session["Vendordetails"] != null)
            {
                searchOptions = Convert.ToString(Session["Vendordetails"]).Split('~');
            }
            //005
            if(searchOptions != null)
            {
                string[] selectedScopes = searchOptions[6].Split(',');
                string[] parentScopes = new string[selectedScopes.Length];

                int i = 0;
                foreach (string item in selectedScopes)
                {
                    parentScopes[i] = objTradeInformation.GetTradeScopeTOPParentID((!string.IsNullOrEmpty(item)) ? Convert.ToInt32(item) : 0, ScopesCodeType.MasterFormat);
                    i++;
                }

                if (parentScopes.Contains(hdnId.Value))
                {
                    grd.Style["Display"] = " ";
                    img.ImageUrl = "~/Images/minus.gif";
                    hdn.Value = "~/Images/minus.gif";
                } 
            }                
            
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DOCUMENT_LOCATION = ConfigurationManager.AppSettings["VQFDocument"].ToString();
        
        
        
        //004
        //if (chlSearchBy.Items[1].Selected == true)
        //{
        //    //004, 009
        //    SelectedScopeTab();
        //}
        
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (!IsPostBack)
        {
            txtChkOther.Attributes.Add("style", "display: none;");      //004

            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");
            
            CallScripts();
            //Check for the session varaible of null redirect to the login page
            if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
            {
                    Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
            }
            else
            {
                if (Session["RISKIDLOGOUT"] != null && Session["LOCKEDBYLOGOUT"] != null)
                {
                    Int64 Lockedby = riskEvaluation.LockebByUser(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                    if (Lockedby == Convert.ToInt64(Session["LOCKEDBYLOGOUT"]))
                    {
                        riskEvaluation.UpdateStatus(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                        Session["RISKIDLOGOUT"] = null;
                    }
                }
                if (Convert.ToString(Session["Access"]).Equals("2"))
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                {
                    AdminMenu.DisplayMenu("Admin");
                    /// G. Vera 04/18/2012 Item 4.4 Phase 3: Allow Admins to see "expired" vendors
                    ddlStatus.Items.Add(new ListItem() { Text = "Expired"});
                }
                BindCertification();
                BindCountries();    //008
                BindTypesOfCompany();    //010

                chlSearchBy.Items[0].Selected = true;
                chkCheckAll.Attributes.Add("onClick", "CheckAllRegions('" + chkCheckAll.ClientID + "');");
                Session["SavedPage"] = null;
                Session["Page"] = null;
                hdnFlag.Value = "0";
                hdnFlag1.Value = "0";
                imbsubmit.Attributes.Add("onClick", "Javascript:AssignValue('" + hdnFlag.ClientID + "')");
                imbsearchview.Attributes.Add("onClick", "Javascript:AssignValue('" + hdnFlag1.ClientID + "')");
               
                //007
                txtFederalTax2.Attributes.Add("onkeyup", "javascript:SetFocusOnTax(" + txtFederalTax2.ClientID + "," + txtFederalTax2.ClientID + ")");
                if (Convert.ToString(Session["Access"]) == "2")
                {
                    AdminMenu.DisplayMenu("Employee");
                    spnTrcNo.Style["display"] = "none";
                    trStatus.Style["display"] = "none";
                    trVendorStatus.Style["display"] = "none";
                    trTrackingNumber.Style["display"] = "none";
                }
                else
                {
                    spnTrcNo.Style["display"] = "";
                    trStatus.Style["display"] = "";
                    trVendorStatus.Style["display"] = "";
                    trTrackingNumber.Style["display"] = "";
                }
                clsRegistration objRegistration = new clsRegistration();

                imbPrevious.Visible = false;
                imbNext.Visible = false;
                hdnPage.Value = "1";
                Search.Style["display"] = "none";
                tdnextPrevious1.Style["display"] = "none";
                tdbuttons.Style["display"] = "none";
                imgBackToSearch.Style["display"] = "none";
              
                if ((Request.QueryString.Count > 0) && (!string.IsNullOrEmpty(Request.QueryString["user"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["Vendordetails"])))
                    {
                        //005 created below method
                        RepopulatePreviousSearch();

                        imgBackToSearch.Style["display"] = "";
                        tblVendorDetails.Style["Display"] = "";
                        string[] BindingValue = Convert.ToString(Session["Vendordetails"]).Split('~');
                        if (BindingValue.Length >= 15)
                        {
                            //003 - added new fields 16 - 18
                            //004
                            //005
                            //008
                            //010
                            grdVendor.DataSource = objSearchVendor.SearchVendor(BindingValue[0], BindingValue[1], BindingValue[2], BindingValue[3], BindingValue[4],
                                                                                BindingValue[5], BindingValue[6], BindingValue[7], BindingValue[8],
                                                                                Convert.ToInt32(BindingValue[9]), Convert.ToInt32(BindingValue[10]), BindingValue[11],
                                                                                BindingValue[12], Convert.ToInt32(BindingValue[13]), BindingValue[14],
                                                                                BindingValue[15], BindingValue[16], BindingValue[17], BindingValue[18], BindingValue[19],
                                                                                BindingValue[20], BindingValue[21], BindingValue[22], Convert.ToBoolean(BindingValue[23]),
                                                                                BindingValue[24], BindingValue[25], BindingValue[26]); //008
                            grdVendor.DataBind();
                            //003 - changed
                            //004
                            //005
                            //008
                            //010
                            Session["VendordetailsCount"] = objSearchVendor.GetDetailsCountAdmin(BindingValue[0], BindingValue[1], BindingValue[2], BindingValue[3], BindingValue[4], BindingValue[5], BindingValue[6],
                                                                                                 BindingValue[7], BindingValue[8], BindingValue[11], BindingValue[12], Convert.ToInt32(BindingValue[13]),
                                                                                                 BindingValue[14], BindingValue[15], BindingValue[16], BindingValue[17], BindingValue[18], BindingValue[19],
                                                                                                 BindingValue[20], BindingValue[21], BindingValue[22], Convert.ToBoolean(BindingValue[23]), BindingValue[24],
                                                                                                 BindingValue[25], BindingValue[26]); //008
                            // G. Vera 04/18/2012 Item 4.4 Phase 3
                            //003 - No expired records filtering anymore
                            //if (Convert.ToString(Session["Access"]).Equals("1"))
                            //    Session["VendordetailsCount"] = objSearchVendor.GetDetailsCountAdmin(BindingValue[0], BindingValue[1], BindingValue[2], BindingValue[3], BindingValue[4], BindingValue[5], BindingValue[6], BindingValue[7], BindingValue[8], BindingValue[11], BindingValue[12], Convert.ToInt32(BindingValue[13]), BindingValue[14], BindingValue[15]);

                            hdnPage.Value = Convert.ToString(BindingValue[9]);
                        }
                        if (grdVendor.Rows.Count <= 0)
                        {
                            lblmsg.Text= "Sorry no records found";
                            Search.Style["display"] = "none";
                            tdnextPrevious1.Style["display"] = "none";
                            tdbuttons.Style["display"] = "none";
                        }
                        else
                        {
                            lblmsg.Text= string.Empty;
                            Search.Style["display"] = "";
                            tdnextPrevious1.Style["display"] = "";
                            tdbuttons.Style["display"] = "";
                            if ((Convert.ToString(Session["Access"]) == "2") && (grdVendor.Columns.Count >= 8))
                            {
                                grdVendor.Columns[9].Visible = false;
                            }

                            lblTotal.Text = Convert.ToString(Session["VendordetailsCount"]);
                            trGrid.Style["Display"] = "";

                            hdnCount.Value = Convert.ToString(Session["VendordetailsCount"]);
                            //005
                            int page;
                            if (Session["SelectedNumber"] != null) page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.Items[Convert.ToInt32(Session["SelectedNumber"])].Text))));
                            else page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));

                            Session["Page"] = Convert.ToString(page);
                            //  lblCurrentPage.Text= Convert.ToString(hdnPage.Value);
                            lblTotalPage.Text = Convert.ToString(Session["Page"]);
                            if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
                            {
                                imbNext.Visible = false;
                            }
                            else
                            {
                                imbNext.Visible = true;
                            }
                            if (hdnPage.Value == "1")
                            {
                                imbPrevious.Visible = false;
                            }
                            else
                            {
                                imbPrevious.Visible = true;
                            }
                        }
                    }

                    //005
                    if (Session["SelectedNumber"] != null)
                    {
                        ddlNumber.SelectedIndex = Convert.ToInt32(Session["SelectedNumber"]);
                    }
                }
                else
                {
                    Session["Vendordetails"] = null;    //005 reset on actual load
                    Session["VendordetailsCount"] = null;
                    Session["SelectedNumber"] = null;   //005 reset on actual load
                    Session["ParentScopes"] = null;     //005 reset on actual load
                    Session["ParentRegions"] = null;     //005 reset on actual load
                    Session["Continents"] = null;
                    Session["Countries"] = null;
                    Session["SelectedScopeTab"] = null;   //009
                }


                BindProjects();     //005
                BindScopes();
                BindRegions();
             
            }
        }
        CheckBoxList chkl, chkl1, chkl2;
        chkl = new CheckBoxList();
        chkl = (CheckBoxList)grdState1.Rows[4].FindControl("chklRegions");

        chkl1 = new CheckBoxList();
        chkl1 = (CheckBoxList)grdState1.Rows[8].FindControl("chklRegions");

        chkl2 = new CheckBoxList();
        chkl2 = (CheckBoxList)grdState3.Rows[8].FindControl("chklRegions");
        chkCheckAll.Attributes.Add("onClick", "CheckAllRegions('" + chkCheckAll.ClientID + "','" + chkl.ClientID + "','" + chkl1.ClientID + "','" + chkl2.ClientID + "','" + trRegions.ClientID + "');");

        chkUS.Attributes.Add("onClick", "CheckUSRegions(this.checked,'" + chkl.ClientID + "','" + chkl1.ClientID + "','" + chkl2.ClientID + "','" + trRegions.ClientID + "','" + chkUS.ClientID + "','" + divUS.ClientID + "');"); //008-Sooraj

     
        foreach (GridViewRow gr in grdState1.Rows)
        {
            CheckBoxList chk1 = (CheckBoxList)gr.FindControl("chklRegions");
            if (chk1.Items.Count > 0)
            {
                foreach (ListItem item in chk1.Items)
                {
                    //005 - modified
                    if (item.Text.Trim().ToUpper() == "ALL")
                        item.Attributes.Add("onClick", "GetChecked('" + item.Text.Trim() + "',this.checked,'" + chk1.ClientID + "')");
                }
            }
        }
        foreach (GridViewRow gr in grdState2.Rows)
        {
            CheckBoxList chk1 = (CheckBoxList)gr.FindControl("chklRegions");
            if (chk1.Items.Count > 0)
            {
                foreach (ListItem item in chk1.Items)
                {
                    //005 - modified
                    if (item.Text.Trim().ToUpper() == "ALL")
                        item.Attributes.Add("onClick", "GetChecked('" + item.Text.Trim() + "',this.checked,'" + chk1.ClientID + "')");
                }
            }
        }
        foreach (GridViewRow grd in grdState3.Rows)
        {
            CheckBox chk = (CheckBox)grd.FindControl("chkCheck");
            HiddenField hdnId = (HiddenField)grd.FindControl("hdnStateID");
            if (chk.Checked)
            {
                if (hdnId.Value == "51")
                {
                    trRegions.Style["Display"] = "";
                }
                else
                {
                    CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chklRegions");
                    if (chk1.Items.Count > 0)
                    {
                        chk1.Style["display"] = "";
                    }
                    if (chk1.Items.Count > 0)
                    {
                        foreach (ListItem item in chk1.Items)
                        {
                            //005 - modified
                            if (item.Text.Trim().ToUpper() == "ALL")
                                item.Attributes.Add("onClick", "GetChecked('" + item.Text.Trim() + "',this.checked,'" + chk1.ClientID + "')");
                        }
                    }
                }
            }
        }

        foreach (ListItem lst in rdoCompanyCertiY.Items)
        {
            if (lst.Selected)
            {
                trotherCertification.Style["display"] = lst.Text.Trim().Equals("Other") ? "" : "none";
            }
        }
        foreach (ListItem lst in chklFederalSB.Items)
        {
            if (lst.Selected == true)
            {
                spnFederalSB.Style["display"] = lst.Text.Trim().Equals("Other") ? "" : "none";
            }
        }
        VisibleControls();
        foreach (ListItem item in rdoCompanyCertiY.Items)
        {
            item.Attributes.Add("onClick", "ShowCertContents('" + item.Text.Trim() + "','" + rdoCompanyCertiY.ClientID + "',this.checked)");
        }
        foreach (ListItem item in chklFederalSB.Items)
        {
            item.Attributes.Add("onClick", "ShowFederalContents('" + item.Text.Trim() + "','" + chklFederalSB.ClientID + "',this.checked)");
        }
        string str = Request.Form["__EVENTTARGET"];
        if ((!string.IsNullOrEmpty(hdnSearchPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["SavedPage"]))) && str.ToUpperInvariant() != "DDLSEARCHNUMBER" && hdnFlag1.Value != "1")
        {
            for (int i = (Convert.ToInt32(hdnSearchPage.Value) - 5); i <= (Convert.ToInt32(hdnSearchPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["SavedPage"]))
                {
                    LinkButton lnk = new LinkButton();
                    lnk.Click += new EventHandler(lnk_Click);
                    lnk.Text= Convert.ToString(i);
                    if (i == Convert.ToInt32(hdnSearchPage.Value))
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnk.CssClass = "lnkcolor";
                    }
                    plcCrntPage.Controls.Add(lnk);
                    lnk.Text= Convert.ToString(i);
                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnSearchPage.ClientID + "')");
                }
            }
        }
        string str1 = Request.Form["__EVENTTARGET"];
        if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page"]))) && ((string.IsNullOrEmpty(str1)) || (str1.ToUpperInvariant() != "DDLNUMBER")) && hdnFlag.Value != "1")
        {

            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {
                    LinkButton lnk = new LinkButton();
                    lnk.Click += new EventHandler(lnk1_Click);
                    lnk.Text= Convert.ToString(i);
                    if (i == Convert.ToInt32(hdnPage.Value))
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnk.CssClass = "lnkcolor";
                    }
                    plcCrntPage1.Controls.Add(lnk);
                    lnk.Text= Convert.ToString(i);
                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnPage.ClientID + "')");
                }
            }

            //Inserted by N Schoenberger 10-4-2011
            
        }
        hdnFlag.Value = "0";
        hdnFlag1.Value = "0";
        //004
        if(chkProjectOther.Checked) txtChkOther.Attributes["style"] = @"display: """;
        else txtChkOther.Attributes["style"] = @"display: none";

        //004 - added
        VisibilityIndustryCerts();

        divUS.Style["display"] = chkUS.Checked ? "" : "none";
    }

    /// <summary>
    /// 010 - added
    /// </summary>
    private void BindTypesOfCompany()
    {
        rptTypeOfCompany.DataSource = objCompany.GetTypeOfCompany();
        rptTypeOfCompany.DataBind();
    }

    
    /// <summary>
    /// 005 - implemented
    /// </summary>
    private void RepopulatePreviousSearch()
    {
        //005 - modified
        if (Session["Vendordetails"] != null)
        {

            int page;
            if (Session["SelectedNumber"] != null)
            {
                ddlNumber.SelectedIndex = Convert.ToInt32(Session["SelectedNumber"]);       //005
            }         

            string[] searchOptions = Convert.ToString(Session["Vendordetails"]).Split('~');

            //search by
            if (searchOptions[0].Contains("V")) chlSearchBy.Items[0].Selected = true;
            if (searchOptions[0].Contains("C"))
            {
                chlSearchBy.Items[1].Selected = true;
                spnCSICode.Attributes["style"] = @"display: ";

                SelectedScopeTab();     //009
                hdnScopeTypeOpen.Value = Session["SelectedScopeTab"].ToString();        //009
                Session["SelectedScopeTab"] = null; //009 after use, clear this session varaiable as it should only be used when user navigates out of this screen

            }
            if (searchOptions[0].Contains("R"))
            {
                chlSearchBy.Items[2].Selected = true;
                spnLocation.Attributes["style"] = @"display: """;

                string[] selectedContinent=null;
                string[] selectedCountries=null;
                 searchOptions=  Convert.ToString(Session["Vendordetails"]).Split('~');
                //005
                if (searchOptions != null)
                {
                    selectedContinent = searchOptions[24].Split(',');
                    selectedCountries = searchOptions[25].Split(',');

                }
             
                //008
                foreach (GridViewRow grd in grdContinent.Rows)
                {
                    CheckBox chk = (CheckBox)grd.FindControl("chkContinent");
                    CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chkContinentCountries");
                    HtmlControl div = (HtmlControl)grd.FindControl("divCountries");
                    string  continentID = grdContinent.DataKeys[grd.RowIndex].Value.ConvertToString();

                    if (selectedContinent.Contains(continentID))
                    {
                        chk.Checked = true;
                        div.Style["display"]="";
                        foreach (ListItem item in chk1.Items)
                        {
                            if (selectedCountries.Contains(item.Value))
                            {
                                
                                chk1.Items.FindByValue(item.Value).Selected = true;
                            }
                        }
                    }

                        div.Style["display"] = chk.Checked?  "":"none";
                   
                }


            }
            if (searchOptions[0].Contains("P"))
            {
                chlSearchBy.Items[3].Selected = true;
                SpnTOP.Attributes["style"] = @"display:""";
            }
            if (searchOptions[0].Contains("F"))
            {
                chlSearchBy.Items[4].Selected = true;
                divCertification.Attributes["style"] = @"display: """;
            }

            // ALL were selected
            if (searchOptions[0].Contains("A"))
            {
                chlSearchBy.Items[0].Selected = true;
                chlSearchBy.Items[1].Selected = true;
                spnCSICode.Attributes["style"] = @"display: ";

                SelectedScopeTab(); //009

                chlSearchBy.Items[2].Selected = true;
                spnLocation.Attributes["style"] = @"display: """;

                chlSearchBy.Items[3].Selected = true;
                SpnTOP.Attributes["style"] = @"display:""";

                chlSearchBy.Items[4].Selected = true;
                divCertification.Attributes["style"] = @"display: """;


            }

            chlSearchBy.Items[5].Selected = Convert.ToBoolean(searchOptions[23]);       // bim

            txtVendor.Text = searchOptions[1].ToString();  //vendor name
            txtVendorNumber.Text = searchOptions[2].ToString();  //vendor number (JDE)
            txtTrackingNumber.Text = searchOptions[3].ToString();  //Tracking number

            //010 - Start
            if (!String.IsNullOrEmpty(searchOptions[26]))
            {
                searchOptions[26] = searchOptions[26].Replace("'", "");
                string[] types = searchOptions[26].Split(',');

                foreach (DataListItem item in rptTypeOfCompany.Items)
                {
                    CheckBox chk = (item.FindControl("chkTypeOfCompany") as CheckBox);
                    chk.Checked = (types.Contains(chk.ToolTip));
                }
            }
            //010 - End



            //007-Sooraj
            //txtFederalTax1.Text = (!string.IsNullOrEmpty(searchOptions[4])) ? searchOptions[4].Substring(0, 2).ToString() : ""; // tax ID 1
            //txtFederalTax2.Text = (!string.IsNullOrEmpty(searchOptions[4])) ? searchOptions[4].Substring(2, 7).ToString() : "";  //tax ID 2
            if (!string.IsNullOrEmpty(searchOptions[4]))
            {
                txtFederalTax2.Text = searchOptions[4];
            }

            txtFirstName.Text = searchOptions[16];   //First name
            txtLastName.Text = searchOptions[17];    //Last name
            txtEmail.Text = searchOptions[18];        //Email
            if (searchOptions[5].Length == 1)
            {
                ddlStatus.SelectedIndex = (!string.IsNullOrEmpty(searchOptions[5])) ? (Convert.ToInt32(searchOptions[5]) + 1) : 0;
            }
            else
            {
                ddlStatus.SelectedIndex = 0;
            }
        }
    }

    /// <summary>
    /// 009 - refactored
    /// </summary>
    private void SelectedScopeTab()
    {
        string tabName = (Session["SelectedScopeTab"] != null) ? Session["SelectedScopeTab"].ToString() : hdnScopeTypeOpen.Value;

        if (tabName == "PMMI")
        {
            updPnlPMMI.Attributes["style"] = @"display: """;
            updpnlScope.Attributes["style"] = @"display: none";
            updPnlDesign.Attributes["style"] = @"display: none";
            liMasterFormat.Attributes["class"] = "";
            liPMMI.Attributes["class"] = "selected";
            liDesign.Attributes["class"] = "";

        }
        if (tabName == "MasterFormat")
        {
            updPnlPMMI.Attributes["style"] = @"display: none";
            updpnlScope.Attributes["style"] = @"display: """;
            updPnlDesign.Attributes["style"] = @"display: none";
            liMasterFormat.Attributes["class"] = "selected";
            liPMMI.Attributes["class"] = "";
            liDesign.Attributes["class"] = "";
        }
        //006 Sooraj 
        if (tabName == "Design")
        {
            updPnlDesign.Attributes["style"] = @"display: """;
            updpnlScope.Attributes["style"] = @"display: none";
            updPnlPMMI.Attributes["style"] = @"display: none";

            liDesign.Attributes["class"] = "selected";
            liMasterFormat.Attributes["class"] = "";
            liPMMI.Attributes["class"] = "";
        }
    }


    /// <summary>
    /// 005
    /// </summary>
    private void BindProjects()
    {
        chkProject.DataSource = objTradeInformation.GetTradeProjects();
        chkProject.DataTextField = "ProjectName";
        chkProject.DataValueField = "PK_ProjectID";
        chkProject.DataBind();

        //005
        string[] searchOptions = null;

        if (Session["Vendordetails"] != null)
        {
            searchOptions = Convert.ToString(Session["Vendordetails"]).Split('~');
        }
        //005
        if (searchOptions != null)
        {
            string[] selectedProjects = searchOptions[8].Split(',');
            
            foreach (ListItem li in chkProject.Items)
            {
                if (selectedProjects.Contains(li.Value))
                {
                    li.Selected = true;
                }
            }

            if (!string.IsNullOrEmpty(searchOptions[19]))
            {
                chkProjectOther.Checked = true;
                txtChkOther.Text = searchOptions[19];
                txtChkOther.Visible = true;
            }
        }


        
    }


    /// <summary>
    /// 004 - added
    /// </summary>
    private void VisibilityIndustryCerts()
    {
        foreach (DataListItem i in dlIndustryCerts.Items)
        {
            TextBox txt = i.FindControl("txtIndustryCert") as TextBox;
            CheckBox chk = i.FindControl("chkIndustryCert") as CheckBox;

            if (chk.Checked)
            {
                txt.Attributes["style"] = @"display: """;
            }
            else
            {
                txt.Attributes["style"] = "display: none";
                txt.Text = string.Empty;
            }
        }
    }

    //008
    private void BindCountries()
    {
        var CountryList = objCommon.GetCountryListWithContinent().ToList();
        var continentList = objCommon.GetContinent();
        CheckBoxList chkList = null;
        CheckBox chk = null;
        grdContinent.DataSource = continentList;
        grdContinent.DataBind();
        int continentID = 0;
        HtmlControl divCountry = null;
        foreach (GridViewRow grd in grdContinent.Rows)
        {
            chk = (CheckBox)grd.FindControl("chkContinent");

            chkList = (CheckBoxList)grd.FindControl("chkContinentCountries");
            divCountry = (HtmlControl)grd.FindControl("divCountries");
            chk.Attributes.Add("onclick", "SwitchView('" + chk.ClientID + "','" + divCountry.ClientID + "');");
            continentID = grdContinent.DataKeys[grd.RowIndex].Value.ConvertToString().ConvertToShortInt();
            if (chkList != null)
                objCommon.BindCountryDropDownByContinent(CountryList, chkList, continentID);

        }

    }

    private void BindCertification()
    {
        //005
        string[] searchOptions = null;

        if (Session["Vendordetails"] != null)
        {
            searchOptions = Convert.ToString(Session["Vendordetails"]).Split('~');
        }

        rdoCompanyCertiY.DataSource = objCompany.DisplayCompanyCertification(0);
        rdoCompanyCertiY.DataTextField = "Certification";
        rdoCompanyCertiY.DataBind();

        //005
        if (searchOptions != null)
        {
            string[] selectedCerts = searchOptions[14].Split(',');
            foreach (ListItem item in rdoCompanyCertiY.Items)
            {
                if (selectedCerts.Contains(item.Value))
                {
                    item.Selected = true;
                }
            }
        }
        
        chklFederalSB.DataSource = objCompany.DisplayCompanyCertification(1);
        chklFederalSB.DataTextField = "Certification";
        chklFederalSB.DataBind();

        //005
        if (searchOptions != null)
        {
            string[] selectedCerts = searchOptions[15].Split(',');
            foreach (ListItem item in chklFederalSB.Items)
            {
                if (selectedCerts.Contains(item.Value))
                {
                    item.Selected = true;
                }
            }
        }

        //004
        dlIndustryCerts.ItemDataBound += new DataListItemEventHandler(dlIndustryCerts_ItemDataBound);
        dlIndustryCerts.DataSource = objCompany.GetIndustryCertifications();
        dlIndustryCerts.DataBind();

        //005
        if (searchOptions != null)
        {
            if (!string.IsNullOrEmpty(searchOptions[20]))
            {
                (this.dlIndustryCerts.Items[0].FindControl("chkIndustryCert") as CheckBox).Checked = true;
                (dlIndustryCerts.Items[0].FindControl("txtIndustryCert") as TextBox).Text = searchOptions[20]; //ISO
                (dlIndustryCerts.Items[0].FindControl("txtIndustryCert") as TextBox).Visible = true;
            }
            if (!string.IsNullOrEmpty(searchOptions[21]))
            {
                (this.dlIndustryCerts.Items[1].FindControl("chkIndustryCert") as CheckBox).Checked = true;
                (dlIndustryCerts.Items[1].FindControl("txtIndustryCert") as TextBox).Text = searchOptions[21];     //QS
                (dlIndustryCerts.Items[1].FindControl("txtIndustryCert") as TextBox).Visible = true;
            }
        }
    }

    /// <summary>
    /// 004 - added
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlIndustryCerts_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        TextBox txt = e.Item.FindControl("txtIndustryCert") as TextBox;
        txt.Attributes.Add("style", "display: none");

        string certification = (e.Item.DataItem as VMSDAL.CompanyCertification).Certification;

        switch (certification.ToUpper().Trim())
        {
            case "ISO":
                txt.ToolTip = "List ISO certifications separated by a comma";
                break;
            case "QS":
                txt.ToolTip = "List QS certifications separated by a comma";
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Validate the page and return thr error message
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                errmsg += "<li>" + validator.ErrorMessage + "</li>";
            }
        }
        return errmsg;
    }

    //007-sooraj
    ///// <summary>
    ///// Validate the federal tax id
    ///// </summary>
    ///// <param name="source"></param>
    ///// <param name="args"></param>
    //protected void custValidator_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    if (!( string.IsNullOrEmpty(txtFederalTax2.Text.Trim()))
    //    {
    //        if (!((txtFederalTax1.Text.Trim().Length == 2) && (txtFederalTax2.Text.Trim().Length == 7)))
    //        {
    //            custValidator.ErrorMessage = "Please enter valid federal taxID";
    //            args.IsValid = false;
    //        }
    //        else
    //        {
    //            args.IsValid = true;
    //        }
    //    }
    //}

    /// <summary>
    /// Extract only numbers from the text 
    /// </summary>
    /// <param name="expr"></param>
    /// <returns></returns>
    static string ExtractNumbers(string expr)
    {
        return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
    }

    /// <summary>
    /// Display limited Character in a cell
    /// </summary>
    /// <param name="desc"></param>
    /// <returns></returns>
    public string GetVendorName(string desc)
    {
        if (desc.Length >15)
        {
            return desc.Substring(0, 15) + "...";// +desc.Substring(20);
        }
        else
        {
            return desc;
        }
    }

    /// <summary>
    /// Bind scopes 
    /// </summary>
    private void BindScopes()
    {
        //002 - changed to use new DAL
        VMSDAL.UspGetTradeScopesByTypeResult[] objScopes = objTradeInformation.GetTradeScopeByType(-1, ScopesCodeType.MasterFormat);
        int count = objScopes.Length;
        int count1 = (Convert.ToInt32(Math.Round(Convert.ToDouble(count / 2))));
        grdLevel1.DataSource = objScopes.Take(count1 + 1);
        grdLevel1.DataBind();
        grdLevel2.DataSource = objScopes.Skip(count1 + 1);
        grdLevel2.DataBind();

        //002 - Bind PMMI scopes
        VMSDAL.UspGetTradeScopesByTypeResult[] objPMMIScopes = objTradeInformation.GetTradeScopeByType(-1, ScopesCodeType.PMMI).OrderBy(s => s.RootNode).ToArray();
        dlPMMICodes1.DataSource = objPMMIScopes;
        dlPMMICodes1.DataBind();

        //006 Sooraj  - Bind Design Scope 
        VMSDAL.UspGetTradeScopesByTypeResult[] objDesignScopes = objTradeInformation.GetTradeScopeByType(-1, ScopesCodeType.DesignConsultant).OrderBy(s => s.RootNode).ToArray();
        dlistDesign.DataSource = objDesignScopes;
        dlistDesign.DataBind();
    }

 
    /// <summary>
    /// Display the controls across postback
    /// </summary>
    private void VisibleControls()
    {

        //008
        foreach (GridViewRow grd in grdContinent.Rows)
        {
            CheckBox chk = (CheckBox)grd.FindControl("chkContinent");
            CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chkContinentCountries");
            HtmlControl div = (HtmlControl)grd.FindControl("divCountries");

            if (chk.Checked)
            {
                    div.Style["display"] = "";
            }
            else
            {
                    div.Style["display"] = "none";
            }
        }

      

        foreach (GridViewRow grd in grdState1.Rows)
        {
          
            CheckBox chk = (CheckBox)grd.FindControl("chkCheck");
            if (chk.Checked)
            {
                chkUS.Checked = true;//006 -Sooraj
                divUS.Style["display"] = "";

                CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chklRegions");
                if (chk1.Items.Count > 0)
                {
                    chk1.Style["display"] = "";
                }
            }
            //005 - added
            else
            {
                CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chklRegions");
                if (chk1.Items.Count > 0)
                {
                    chk1.Style["display"] = "none";
                }
            }
        }

        foreach (GridViewRow grd in grdState2.Rows)
        {
            CheckBox chk = (CheckBox)grd.FindControl("chkCheck");
            if (chk.Checked)
            {
                chkUS.Checked = true;//006 -Sooraj
                divUS.Style["display"] = "";


                CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chklRegions");
                if (chk1.Items.Count > 0)
                {
                    chk1.Style["display"] = "";
                }
            }
            //005 - added
            else
            {
                CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chklRegions");
                if (chk1.Items.Count > 0)
                {
                    chk1.Style["display"] = "none";
                }
            }
        }
        foreach (GridViewRow grd in grdState3.Rows)
        {
            CheckBox chk = (CheckBox)grd.FindControl("chkCheck");
            HiddenField hdnId = (HiddenField)grd.FindControl("hdnStateID");
            if (chk.Checked)
            {
                chkUS.Checked = true;//006 -Sooraj
                divUS.Style["display"] = "";


                if (hdnId.Value == "52")
                {
                    trRegions.Style["Display"] = "";
                }
                else
                {
                    CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chklRegions");
                    if (chk1.Items.Count > 0)
                    {
                        chk1.Style["display"] = "";
                    }
                }
            }
            //005 - added
            else
            {
                CheckBoxList chk1 = (CheckBoxList)grd.FindControl("chklRegions");
                if (chk1.Items.Count > 0)
                {
                    chk1.Style["display"] = "none";
                }
            }
        }

        spnVendorDetails.Style["Display"] = chlSearchBy.Items[0].Selected ? "" : "none";
        //004 - modified
        if (chlSearchBy.Items[1].Selected)
        {
            spnCSICode.Attributes["style"] = @"display: """;

            //004, 009
            SelectedScopeTab();
        }
        spnLocation.Style["Display"] = chlSearchBy.Items[2].Selected ? "" : "none";
        SpnTOP.Style["Display"] = chlSearchBy.Items[3].Selected ? "" : "none";
        divCertification.Style["Display"] = chlSearchBy.Items[4].Selected ? "" : "none";
        
        for (int i = 0; i < grdLevel1.Rows.Count; i++)
        {
            HiddenField hdn = (HiddenField)grdLevel1.Rows[i].FindControl("hdnFlag");
            if (hdn.Value.Contains("/minus.gif"))      //005 - modified to use contains instead of =
            {
                GridView grd = (GridView)grdLevel1.Rows[i].FindControl("grdScopeLevele2");
                Image img = (Image)grdLevel1.Rows[i].FindControl("imgStatus");
                img.ImageUrl = hdn.Value;
                grd.Style["Display"] = "";
                for (int j = 0; j < grd.Rows.Count; j++)
                {
                    CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                    if (chk.Checked)
                    {
                        CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                        foreach (ListItem lst in chklLevele3.Items)
                        {
                            if (lst.Selected)
                            {
                                chklLevele3.Style["Display"] = "";
                            }
                        }
                    }
                }
            }
            else
            {
                GridView grd = (GridView)grdLevel1.Rows[i].FindControl("grdScopeLevele2");
                Image img = (Image)grdLevel1.Rows[i].FindControl("imgStatus");
                img.ImageUrl = hdn.Value;
                for (int j = 0; j < grd.Rows.Count; j++)
                {
                    CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                    grd.Style["Display"] = "None";
                    if (!chk.Checked)
                    {
                        CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                        foreach (ListItem lst in chklLevele3.Items)
                        {
                            if (!lst.Selected)
                            {
                                chklLevele3.Style["Display"] = "None";
                            }
                        }
                    }
                }
            }
        }

        for (int i = 0; i < grdLevel2.Rows.Count; i++)
        {

            HiddenField hdn = (HiddenField)grdLevel2.Rows[i].FindControl("hdnFlag");
            if (hdn.Value.Contains("/minus.gif"))       //005 - modified to use contains instead of =
            {
                GridView grd = (GridView)grdLevel2.Rows[i].FindControl("grdScopeLevele2");
                Image img = (Image)grdLevel2.Rows[i].FindControl("imgStatus");
                img.ImageUrl = hdn.Value;
                grd.Style["Display"] = "";
                for (int j = 0; j < grd.Rows.Count; j++)
                {
                    CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                    if (chk.Checked)
                    {
                        CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                        foreach (ListItem lst in chklLevele3.Items)
                        {
                            if (lst.Selected)
                            {
                                chklLevele3.Style["Display"] = "";
                            }
                        }
                    }
                }
            }
            else
            {
                GridView grd = (GridView)grdLevel2.Rows[i].FindControl("grdScopeLevele2");
                Image img = (Image)grdLevel2.Rows[i].FindControl("imgStatus");
                img.ImageUrl = hdn.Value;
                for (int j = 0; j < grd.Rows.Count; j++)
                {
                    CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                        grd.Style["Display"] = "None";
                    if (!chk.Checked)
                    {
                        CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                        foreach (ListItem lst in chklLevele3.Items)
                        {
                            if (!lst.Selected)
                            {
                                chklLevele3.Style["Display"] = "None";
                            }
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Bind Regions
    /// </summary>
    private void BindRegions()
    {
        DataTable RegionsTable = new DataTable();
        RegionsTable = objTradeInformation.GetRegions().Tables[0];

        DataView dv = new DataView();
        dv = RegionsTable.DefaultView;
        dv.RowFilter = "PK_StateID >=1 and PK_StateID <=17";
        grdState1.DataSource = dv;
        grdState1.DataBind();

        DataView dv1 = new DataView();
        dv1 = RegionsTable.DefaultView;
        dv1.RowFilter = "PK_StateID >=18 and PK_StateID <=34";
        grdState2.DataSource = dv1;
        grdState2.DataBind();

        DataView dv4 = new DataView();
        dv4 = RegionsTable.DefaultView;
        dv4.RowFilter = "PK_StateID >=35";
        grdState3.DataSource = dv4;
        grdState3.DataBind();
    }

    /// <summary>
    /// Bind Level 2 and level 3 and write event to display level 2 and level 3 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdGeneralReq_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk = (CheckBox)e.Row.FindControl("chkCheck");

            CheckBoxList chk1 = (CheckBoxList)e.Row.FindControl("chklGeneralReq");
            HiddenField hdnval = (HiddenField)e.Row.FindControl("hdnScopeId");
            chk1.DataSource = objTradeInformation.GetTradeScope(Convert.ToInt32(hdnval.Value));
            chk1.DataTextField = "Name";
            chk1.DataValueField = "PK_ScopeId";
            chk1.DataBind();
            chk1.Style["display"] = "none";
            //005
            string[] searchOptions = null;
            if (Session["Vendordetails"] != null)
            {
                searchOptions = Convert.ToString(Session["Vendordetails"]).Split('~');
            }
            //005
            if (searchOptions != null)
            {
                string[] selectedScopes = searchOptions[6].Split(',');

                if (selectedScopes.Contains(hdnval.Value))
                {
                    chk.Checked = true;
                    chk1.Style["display"] = "";
                }
            }
            foreach (ListItem li in chk1.Items)
            {
                //005
                if(searchOptions != null)
                {
                    string[] selectedScopes = searchOptions[6].Split(',');
                    if (selectedScopes.Contains(li.Value))
                    {
                        li.Selected = true;
                        chk.Checked = true;
                        chk1.Style["display"] = "";

                    }
                }                
            }
            if (chk1.Items.Count > 0)
            {
                chk.Attributes.Add("onclick", "javascript:DisplayLevel2('" + chk1.ClientID + "','" + chk.ClientID + "');");
            }
        }
    }

    /// <summary>
    /// Insert Level1, Level2 and level3 scopeid
    /// </summary>
    private void InsertScopes()
    {
        string sbScopes = string.Empty;
        int flag1 = 0;


       #region Grid Level 1
       for (int i = 0; i < grdLevel1.Rows.Count; i++)
       {
           int flag = 0;
           HiddenField hdnFlag = (HiddenField)grdLevel1.Rows[i].FindControl("hdnFlag");
           if (hdnFlag.Value == "~/Images/minus.gif")
           {
               GridView grd = (GridView)grdLevel1.Rows[i].FindControl("grdScopeLevele2");
               for (int j = 0; j < grd.Rows.Count; j++)
               {
                   CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                   if (chk.Checked)
                   {
                       flag1 = 1;
                       if (flag == 0)
                       {
                           HiddenField hdnScopeId = (HiddenField)grdLevel1.Rows[i].FindControl("hdnParentScopeId");
                           //005 we don't want the parent included in the search results (per Dianne D.) sbScopes += (hdnScopeId.Value); sbScopes += (",");
                           Session["ParentScopes"] += (hdnScopeId.Value) + ",";
                           flag = 1;
                       }
                       HiddenField hdnLevel2ScopeId = (HiddenField)grd.Rows[j].FindControl("hdnScopeId");
                       CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                       //005 - excluding parents sbScopes += (hdnLevel2ScopeId.Value); sbScopes += (",");
                       
                       if (chklLevele3.Items.Count <= 0)
                       {
                           sbScopes += (hdnLevel2ScopeId.Value); sbScopes += (",");
                       }
                       else
                       {
                           Session["ParentScopes"] += (hdnLevel2ScopeId.Value) + ",";
                           if (chklLevele3.SelectedIndex == -1)
                           {
                               sbScopes += (hdnLevel2ScopeId.Value); sbScopes += (",");
                           }
                       }

                       foreach (ListItem lst in chklLevele3.Items)
                       {
                           if (lst.Selected)
                           {
                               sbScopes += (lst.Value); sbScopes += (",");
                           }
                       }
                   }
               }
           }
       } 
       #endregion

       #region Grid Level 2
       for (int i = 0; i < grdLevel2.Rows.Count; i++)
       {
           int flag = 0;
           HiddenField hdnFlag = (HiddenField)grdLevel2.Rows[i].FindControl("hdnFlag");
           if (hdnFlag.Value == "~/Images/minus.gif")
           {
               GridView grd = (GridView)grdLevel2.Rows[i].FindControl("grdScopeLevele2");
               for (int j = 0; j < grd.Rows.Count; j++)
               {
                   CheckBox chk = (CheckBox)grd.Rows[j].FindControl("chkCheck");
                   if (chk.Checked)
                   {
                       flag1 = 1;
                       if (flag == 0)
                       {
                           HiddenField hdnScopeId = (HiddenField)grdLevel2.Rows[i].FindControl("hdnParentScopeId");
                           //005 we don't want the parent included in the search results (per Dianne D.) sbScopes += (hdnScopeId.Value); sbScopes += (",");
                           Session["ParentScopes"] += (hdnScopeId.Value) + ",";
                           flag = 1;
                       }
                       HiddenField hdnLevel2ScopeId = (HiddenField)grd.Rows[j].FindControl("hdnScopeId");
                       CheckBoxList chklLevele3 = (CheckBoxList)grd.Rows[j].FindControl("chklGeneralReq");
                       //005 - excluding parents sbScopes += (hdnLevel2ScopeId.Value); sbScopes += (",");

                       if (chklLevele3.Items.Count <= 0)
                       {
                           sbScopes += (hdnLevel2ScopeId.Value); sbScopes += (",");
                       }
                       else
                       {
                           Session["ParentScopes"] += (hdnLevel2ScopeId.Value) + ",";
                           if (chklLevele3.SelectedIndex == -1)
                           {
                               sbScopes += (hdnLevel2ScopeId.Value); sbScopes += (",");
                           }
                       }

                       foreach (ListItem lst in chklLevele3.Items)
                       {
                           if (lst.Selected)
                           {
                               sbScopes += (lst.Value); sbScopes += (",");
                           }
                       }
                   }
               }
           }
       } 
       #endregion

       #region PMMI Codes
       //004
       foreach (DataListItem item in dlPMMICodes1.Items)
       {
           HiddenField hdn = ((HiddenField)item.FindControl("hdnPMMIScopeId"));
           CheckBox chbx = ((CheckBox)item.FindControl("chkPMMICode"));
           if (chbx.Checked)
           {
               sbScopes += Convert.ToInt32(hdn.Value); sbScopes += (",");
           }
       } 
       #endregion    

       #region Design
       //006-Sooraj
       var designScopes = string.Empty;
      
           foreach (DataListItem item in dlistDesign.Items)
           {
               HiddenField hdn = ((HiddenField)item.FindControl("hdnDesignScopeId"));
               CheckBox chbx = ((CheckBox)item.FindControl("chkDesignCode"));
               if (chbx.Checked)
               {
                  
                   designScopes = string.Format("{0},{1}",designScopes, Convert.ToInt32(hdn.Value));
               }
           }


           if (!string.IsNullOrEmpty(designScopes))  //006 -Sooraj Added logic to ignore other scope items if design scopes selected
           {
               Session["ParentScopes"] = "";
               sbScopes = designScopes.TrimStart(',');
           }
       
       #endregion   

           if (sbScopes.Length > 1)
           {
               sbScopes = sbScopes.TrimEnd(',');
           }

        //Sooraj removed code as we can use built in trim end 
        //if (sbScopes.Length > 1)
        //{
        //    sbScopes = sbScopes.Remove(sbScopes.Length - 1, 1);
        //}
        //if (!string.IsNullOrEmpty(sbScopes))
        //{
        //    if (sbScopes.EndsWith(","))
        //    {
        //        sbScopes = sbScopes.Remove(sbScopes.LastIndexOf(','), 1);
        //    }
        //}

        Session["Scopes"] = sbScopes;
    }

    /// <summary>
    /// Insert ProjectId
    /// </summary>
    private void InsertProjects()
    {
        string sbProjects = string.Empty;
        string sbOtherProjects = string.Empty;  //004

        foreach (ListItem lst in chkProject.Items)
        {
            if (lst.Selected)
            {
                sbProjects += lst.Value;
                sbProjects += ",";
            }
        }

        //004
        if (!string.IsNullOrEmpty(txtChkOther.Text)) sbOtherProjects = txtChkOther.Text.Trim();
        else sbOtherProjects = (chkProjectOther.Checked) ? "ALL" : "";

        if (!string.IsNullOrEmpty(sbProjects))
        {
            if (sbProjects.EndsWith(","))
            {
                sbProjects = sbProjects.Remove(sbProjects.LastIndexOf(","), 1);
            }
        }
        Session["Projects"] = sbProjects;
        Session["OtherProject"] = sbOtherProjects;  //004
    }

    //008 Insert Continent
    private void InsertContinent()
    {
        string selectedContinent = string.Empty;
        string selectedCountries = string.Empty;
        Session["Continents"] = null;      
        Session["Countries"] = null;       
        CheckBox chk = null;
        CheckBoxList chkList = null;
        int continentID = 0;

        foreach (GridViewRow grd in grdContinent.Rows)
        {
            continentID = grdContinent.DataKeys[grd.RowIndex].Value.ConvertToString().ConvertToShortInt();
            chk = (CheckBox)grd.FindControl("chkContinent");

            if (chk.Checked)
            {
                selectedContinent += "," + continentID.ConvertToString();
                chkList = (CheckBoxList)grd.FindControl("chkContinentCountries");

                foreach (ListItem item in chkList.Items)
                {
                    if (item.Selected)
                    {
                        selectedCountries += "," + item.Value;

                    }
                }
            }
        }
      
       selectedContinent=  selectedContinent.TrimStart(',');
       selectedCountries = selectedCountries.TrimStart(',');

        if(!string.IsNullOrEmpty(selectedContinent))
       Session["Continents"] = selectedContinent;


        if (!string.IsNullOrEmpty(selectedCountries))
            Session["Countries"] = selectedCountries;
    }

    /// <summary>
    /// Insert States and Sub regions id
    /// </summary>
    private void InsertRegions()
    {
        string sbRegions = string.Empty;
        Session["ParentRegions"] = null;        //005
        if (chkUS.Checked)  //008-Sooraj
        {
            foreach (GridViewRow grd in grdState1.Rows)
            {
                CheckBox chk = (CheckBox)grd.FindControl("chkCheck");
                CheckBoxList chkl = (CheckBoxList)grd.FindControl("chklRegions");
                HiddenField hdn = (HiddenField)grd.FindControl("hdnStateID");
                if (chk.Checked)
                {

                    //005 - modified to handle the parent state selection
                    Session["ParentRegions"] += "," + hdn.Value;
                    if (chkl.Items.Count > 0 && chkl.SelectedIndex != -1)
                    {
                        foreach (ListItem lst in chkl.Items)
                        {
                            if (lst.Selected)
                            {
                                sbRegions += "," + lst.Value;
                            }
                        }
                    }
                    //005 - so if nothing was selected under the state or the state has no regions
                    //      only then, we select the parent.
                    else
                    {
                        sbRegions += "," + hdn.Value;
                    }
                }
            }
            foreach (GridViewRow gr in grdState2.Rows)
            {
                CheckBox chk = (CheckBox)gr.FindControl("chkCheck");
                if (chk.Checked)
                {
                    HiddenField lbl = (HiddenField)gr.FindControl("hdnStateID");
                    CheckBoxList chkl = (CheckBoxList)gr.FindControl("chklRegions");
                    //005 - modified to handle the parent state selection
                    Session["ParentRegions"] += "," + lbl.Value;

                    if (chkl.Items.Count > 0 && chkl.SelectedIndex != -1)
                    {
                        foreach (ListItem lst in chkl.Items)
                        {
                            sbRegions += "," + lst.Value;
                        }
                    }
                    //005 - so if nothing was selected under the state or the state has no regions
                    //      only then, we select the parent.
                    else
                    {
                        sbRegions += "," + lbl.Value;
                    }
                }
            }
            foreach (GridViewRow gr in grdState3.Rows)
            {
                CheckBox chk = (CheckBox)gr.FindControl("chkCheck");
                if (chk.Checked)
                {
                    HiddenField lbl = (HiddenField)gr.FindControl("hdnStateID");
                    CheckBoxList chkl = (CheckBoxList)gr.FindControl("chklRegions");
                    //005 - modified to handle the parent state selection
                    Session["ParentRegions"] += "," + lbl.Value;

                    if (chkl.Items.Count > 0 && chkl.SelectedIndex != -1)
                    {
                        foreach (ListItem lst in chkl.Items)
                        {
                            sbRegions += "," + lst.Value;
                        }
                    }
                    //005 - so if nothing was selected under the state or the state has no regions
                    //      only then, we select the parent.
                    else
                    {
                        sbRegions += "," + lbl.Value;
                    }
                }
            }

            if (sbRegions.Contains(','))
            {
                sbRegions = sbRegions.Remove(0, 1);
            }
            Session["Regions"] = sbRegions;
        }
    }
      
    /// <summary>
    /// Display the modal popup to get the name to save the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActive_Click(object sender, ImageClickEventArgs e)
    {
       modalName.Show();
    }

    /// <summary>
    /// Display the record to display the prior search results
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbsearchview_Click(object sender, ImageClickEventArgs e)
    {
        lblmsg.Text= string.Empty;
        lblmsgInfo.Text= string.Empty;
        tblVendorDetails.Style["display"] = "none";
        tblView.Style["display"] = "";
        tdNextPrevious.Style["Display"] = "";
        trGriddetails.Style["Display"] = "";
        hdnSearchPage.Value = "1";
        ddlSearchNumber.SelectedIndex = 0;
        hdnSearchCount.Value = Convert.ToString(objSearchVendor.GetSearchPriorCount(Convert.ToString(Session["EmployeeName"])));
        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnSearchCount.Value) / Convert.ToDecimal(ddlSearchNumber.Text.Trim()))));
        lblSearchTotalPage.Text= Convert.ToString(page);
        lblSearchTotal.Text= Convert.ToString(hdnSearchCount.Value);
        Session["SavedPage"] = Convert.ToString(page);
        plcCrntPage.Controls.Clear();
        int i;
        if (Convert.ToInt32(Session["SavedPage"]) >= 5)
        {
            for (i = 1; i <= 5; i++)
            {
                LinkButton lnk = new LinkButton();
                lnk.Click += new EventHandler(lnk_Click);
                lnk.Text= Convert.ToString(i);
                if (i == 1)
                {
                    lnk.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk.CssClass = "lnkcolor";
                }
                plcCrntPage.Controls.Add(lnk);
                lnk.Text= Convert.ToString(i);
                lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnSearchPage.ClientID + "')");
            }
        }
        else
        {
            for (i = 1; i <= Convert.ToInt32(Session["SavedPage"]); i++)
            {
                LinkButton lnk = new LinkButton();
                lnk.Click += new EventHandler(lnk_Click);
                lnk.Text= Convert.ToString(i);
                if (i == 1)
                {
                    lnk.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk.CssClass = "lnkcolor";
                }
                plcCrntPage.Controls.Add(lnk);
                lnk.Text= Convert.ToString(i);
                lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnSearchPage.ClientID + "')");
            }
        }

        if (Convert.ToInt32(Session["SavedPage"]) <= Convert.ToInt32(hdnSearchPage.Value))
        {
            imbSearchNext.Visible = false;
        }
        else
        {
            imbSearchNext.Visible = true;
        }
        if (Convert.ToString(hdnSearchPage.Value) == "1")
        {
            imbSearchPrevious.Visible = false;
        }
        else
        {
            imbSearchPrevious.Visible = true;
        }
        SaveBind();
    }

    /// <summary>
    /// Sorting the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSaveSearch_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;
        if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortSaveDirection"])))
        {
            SearchGridViewSortDirection = SortDirection.Ascending;
            ViewState["SortSaveDirection"] = Convert.ToString(SortDirection.Ascending);
            SortSearchGridView(sortExpression, "ASC");
        }
        else if (Convert.ToString(ViewState["SortSaveDirection"]) == Convert.ToString(SortDirection.Ascending))
        {

            SearchGridViewSortDirection = SortDirection.Descending;
            ViewState["SortSaveDirection"] = Convert.ToString(SortDirection.Descending);
            SortSearchGridView(sortExpression, "DESC");
        }
        else
        {
            SearchGridViewSortDirection = SortDirection.Ascending;
            ViewState["SortSaveDirection"] = Convert.ToString(SortDirection.Ascending);
            SortSearchGridView(sortExpression, "ASC");
        }
    }

    /// <summary>
    /// Display regions for the selected state
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdState1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView grd = (GridView)sender;
        if (grd.ID != "grdState3")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chk = (CheckBox)e.Row.FindControl("chkCheck");

                CheckBoxList chk1 = (CheckBoxList)e.Row.FindControl("chklRegions");
                HiddenField hdnval = (HiddenField)e.Row.FindControl("hdnStateID");
                chk1.DataSource = objCommon.getRegion(Convert.ToInt32(hdnval.Value));
                chk1.DataTextField = "StateCode";
                chk1.DataValueField = "PK_StateID";
                chk1.DataBind();
                chk1.Style["display"] = "none";
                //005  
                string[] searchOptions = null;
                string[] selectedStates = null;

                if (Session["Vendordetails"] != null)
                {
                    searchOptions = Convert.ToString(Session["Vendordetails"]).Split('~');
                }
                //005
                if (searchOptions != null)
                {
                    selectedStates = searchOptions[7].Split(',');

                    if (selectedStates.Contains(hdnval.Value))
                    {
                        chk.Checked = true; //parent
                        chk1.Style["display"] = "";
                    }
                }  

                if (chk1.Items.Count > 0)
                {
                    chk.Attributes.Add("onclick", "javascript:DisplayRegions('" + chk1.ClientID + "','" + chk.ClientID + "',false);javascript:GetChecked('" + chk.Text.Trim() + "',this.checked,'" + chk1.ClientID + "');");
                    foreach (ListItem item in chk1.Items)
                    {
                        //005 - modified
                        if (item.Text.Trim().ToUpper() == "ALL") 
                            item.Attributes.Add("onClick", "javascript:GetChecked('" + item.Text.Trim() + "',this.checked,'" + chk1.ClientID + "')");

                        //005
                        if (selectedStates != null)
                        {
                            if (selectedStates.Contains(item.Value))
                            {
                                item.Selected = true;
                                chk.Checked = true; //parent
                                chk1.Style["display"] = "";
                            }
                        } 
                    }
                }
                else
                {
                    chk.Attributes.Add("onclick", "javascript:unCheckCheckALL(this.checked);");
                }

            }
        }
        else
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chk = (CheckBox)e.Row.FindControl("chkCheck");
                CheckBoxList chk1 = (CheckBoxList)e.Row.FindControl("chklRegions");

                HiddenField hdn = (HiddenField)e.Row.FindControl("hdnStateID");
                trRegions.Style["display"] = "none";

                //005  
                string[] searchOptions = null;
                string[] selectedStates = null;

                if (Session["Vendordetails"] != null)
                {
                    searchOptions = Convert.ToString(Session["Vendordetails"]).Split('~');
                }
                //005
                if (searchOptions != null)
                {
                    selectedStates = searchOptions[7].Split(',');

                    if (selectedStates.Contains(hdn.Value))
                    {
                        chk.Checked = true; //parent
                        chk1.Style["display"] = "";
                    }
                }

                if (hdn.Value == "51")
                {
                    chk.Attributes.Add("onclick", "javascript:DisplayRegions('" + trRegions.ClientID + "','" + chk.ClientID + "',true);");

                    //005
                    if (searchOptions != null)
                    {
                        if (!string.IsNullOrEmpty(searchOptions[12]))
                        {
                            chk.Checked = true;
                            txtRegions.Text = searchOptions[12];
                        } 
                    }
                }
                else
                {
                    chk1.DataSource = objCommon.getRegion(Convert.ToInt32(hdn.Value));
                    chk1.DataTextField = "StateCode";
                    chk1.DataValueField = "PK_StateID";
                    chk1.DataBind();
                    chk1.Style["display"] = "none";
                    if (chk1.Items.Count > 0)
                    {
                        chk.Attributes.Add("onclick", "javascript:DisplayRegions('" + chk1.ClientID + "','" + chk.ClientID + "',false);javascript:GetChecked('" + chk.Text.Trim() + "',this.checked,'" + chk1.ClientID + "');");
                        foreach (ListItem item in chk1.Items)
                        {
                            //005 - modified
                            if (item.Text.Trim().ToUpper() == "ALL") item.Attributes.Add("onClick", "GetChecked('" + item.Text.Trim() + "',this.checked,'" + chk1.ClientID + "')");

                            //005
                            if (selectedStates != null)
                            {
                                if (selectedStates.Contains(item.Value))
                                {
                                    item.Selected = true;
                                    chk.Checked = true; //parent
                                    chk1.Style["display"] = "";
                                }
                            }
                        }
                    }
                    else
                    {
                        chk.Attributes.Add("onclick", "javascript:unCheckCheckALL(this.checked);");
                    }
                }
            }
        }
    }

    /// <summary>
    /// Method to save the Searched record
    /// </summary>
    private void SaveBind()
    {
        SearchSortExp = hdnSaveSort.Value;  
        if (SearchSortExp == null)
        {
            SearchSortExp = "";
        }
        imgBackToSearch.Style["display"] = "";

        hdnSearchCount.Value = Convert.ToString(objSearchVendor.GetSearchPriorCount(Convert.ToString(Session["EmployeeName"])));
        lblSearchTotal.Text= hdnSearchCount.Value;
        grdSaveSearch.DataSource = objSearchVendor.ViewPriorSearch(Convert.ToString(Session["EmployeeName"]), Convert.ToInt32(hdnSearchPage.Value), Convert.ToInt32(ddlSearchNumber.SelectedItem.Text.Trim()), SearchSortExp);
        grdSaveSearch.DataBind();
        if (Convert.ToString(Session["Access"]) == "2")
        {
            if (grdSaveSearch.Columns.Count >= 5)
            {
                grdSaveSearch.Columns[4].Visible = false;
            }
        }
        if (grdSaveSearch.Rows.Count <= 0)
        {
            trGriddetails.Style["display"] = "None";
            tdNextPrevious.Style["display"] = "None";
            lbldisplayinformation.Text= "Sorry no records found";
        }
        else
        {
            trGriddetails.Style["display"] = "";
            tdNextPrevious.Style["display"] = "";
            lbldisplayinformation.Text= string.Empty;
        }
    }

    /// <summary>
    /// Get sorting direction
    /// </summary>
    public SortDirection SearchGridViewSortDirection
    {
        get
        {
            if (ViewState["SearchSortDirection"] == null)
            {
                ViewState["SearchSortDirection"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["SearchSortDirection"];
        }
        set
        {
            ViewState["SearchSortDirection"] = value;
        }
    }

    /// <summary>
    /// Method to sort gridview
    /// </summary>
    /// <param name="sortExpression"></param>
    /// <param name="direction"></param>
    private void SortSearchGridView(string sortExpression, string direction)
    {
        SearchSortExp = sortExpression + " " + direction;
        hdnSaveSort.Value = SearchSortExp;   
        SaveBind();
    }

    /// <summary>
    /// Display the search record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbsubmit_Click(object sender, ImageClickEventArgs e)
    {
        tblView.Style["display"] = "none";
        tdNextPrevious.Style["Display"] = "none";
        trGriddetails.Style["Display"] = "none";
        Page.Validate();
        if (Page.IsValid)
        {
            hdnPage.Value = "1";
            lblmsg.Text= string.Empty;
            Session["ParentScopes"] = null;     //005 reset on actual load
            Session["ParentRegions"] = null;     //005 reset on actual load
            Session["Vendordetails"] = null;    //005 reset on actual load
            Session["VendordetailsCount"] = null;
            Session["SelectedNumber"] = null;   //005 reset on actual load
            Session["Continents"] = null;
            Session["Countries"] = null;
            Session["Regions"] = null;
            Session["Scopes"] = null;
            if (chlSearchBy.Items[1].Selected)
            {
                InsertScopes();
            }
            else
            {
                Session["Scopes"] = string.Empty;
            }
            if (chlSearchBy.Items[2].Selected)
            {
                InsertContinent();
                InsertRegions();
                
            }
            else
            {
                Session["Regions"] = string.Empty;
            }
            if (chlSearchBy.Items[3].Selected)
            {
                InsertProjects();
            }
            else
            {
                Session["Projects"] = string.Empty;
                Session["OtherProject"] = string.Empty;
            }

            Bind();
            int page;
            //005
            if (Session["SelectedNumber"] != null) page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.Items[Convert.ToInt32(Session["SelectedNumber"])].Text))));
            else page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));

            Session["Page"] = Convert.ToString(page);
            //  lblCurrentPage.Text= Convert.ToString(hdnPage.Value);
            lblTotalPage.Text= Convert.ToString(Session["Page"]);
            if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
            {
                imbNext.Visible = false;
            }
            else
            {
                imbNext.Visible = true;
            }
            if (hdnPage.Value == "1")
            {
                imbPrevious.Visible = false;
            }
            else
            {
                imbPrevious.Visible = true;
            }
            plcCrntPage1.Controls.Clear();
            int i;
            if (Convert.ToInt32(Session["Page"]) >= 5)
            {
                for (i = 1; i <= 5; i++)
                {
                    LinkButton lnk = new LinkButton();
                    lnk.Click += new EventHandler(lnk1_Click);
                    lnk.Text= Convert.ToString(i);
                    if (i == 1)
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnk.CssClass = "lnkcolor";
                    }
                    lnk.Text= Convert.ToString(i);
                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnPage.ClientID + "')");
                    plcCrntPage1.Controls.Add(lnk);

                }
            }
            else
            {
                for (i = 1; i <= Convert.ToInt32(Session["Page"]); i++)
                {
                    LinkButton lnk = new LinkButton();
                    lnk.Click += new EventHandler(lnk1_Click);
                    lnk.Text= Convert.ToString(i);
                    if (i == 1)
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnk.CssClass = "lnkcolor";
                    }
                    lnk.Text= Convert.ToString(i);
                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnPage.ClientID + "')");
                    plcCrntPage1.Controls.Add(lnk);

                }
            }

        }
        else
        {
            modalExtnd.Show();
            lblValidation.Text= Errormsg();
        }
        // Added by N Schoenberger 10/03/2011
        if (txtVendor.Text != "")
        {
            DAL.Vendor objResult = objRegistration.GetVendorByName(txtVendor.Text.ToString());
            if (objResult != null)
            {
                if (objResult.VendorStatus == 5)
                {
                    lblmsg.Text = txtVendor.Text + @" - VQF IS IN A STATUS OF INCOMPLETE.";
                }
                // G. Vera 04/18/2012 Item 4.4 Phase 3:  Only PMs will see this message
                if (objResult.VendorStatus == 6 && Convert.ToString(Session["Access"]).Equals("2"))
                {
                    lblmsg.Text = txtVendor.Text + @" - VQF IS IN A STATUS OF EXPIRED.";
                }
            }
        }
    }

    /// <summary>
    /// Bind record based on the search criteria
    /// </summary>
    private void Bind()
    {
        imgBackToSearch.Style["display"] = "";
        tblVendorDetails.Style["Display"] = "";
        string SearchBy = string.Empty, Certification = string.Empty, FederalSB = string.Empty, ISO = string.Empty, QS = string.Empty, companyTypes = string.Empty;     //010 - added companyTypes
        foreach (ListItem Item in rdoCompanyCertiY.Items)
        {
            if (Item.Selected)
            {
                if (Item.Text == "Other")
                {
                    Certification += string.IsNullOrEmpty(Certification) ? txtCertiOth.Text.Trim() : "," + txtCertiOth.Text.Trim();
                }
                else
                {
                    Certification += string.IsNullOrEmpty(Certification) ? Item.Text : "," + Item.Text;
                }
            }
        }
        foreach (ListItem Item in chklFederalSB.Items)
        {
            if (Item.Selected)
            {
                if (Item.Text == "Other")
                {
                    FederalSB += string.IsNullOrEmpty(FederalSB) ? txtOtherFederalSB.Text.Trim() : "," + txtOtherFederalSB.Text.Trim();
                }
                else
                {
                    FederalSB += string.IsNullOrEmpty(FederalSB) ? Item.Text : "," + Item.Text;
                }
            }
        }

        //004
        //ValidateIndustryCerts();

        foreach (DataListItem i in dlIndustryCerts.Items)
        {
            CheckBox chk = (i.FindControl("chkIndustryCert") as CheckBox);
            TextBox txt = (i.FindControl("txtIndustryCert") as TextBox);
            HiddenField hdn = (i.FindControl("hdnCertificationID") as HiddenField);

            if (chk.Checked)
            {
                if (!string.IsNullOrEmpty(txt.Text))
                {
                    if (chk.Text.ToUpper().Trim() == "ISO") ISO = txt.Text;
                    if (chk.Text.ToUpper().Trim() == "QS") QS = txt.Text;
                }
                else
                {
                    if (chk.Text.ToUpper().Trim() == "ISO") ISO = "ALL";
                    if (chk.Text.ToUpper().Trim() == "QS") QS = "ALL";

                }
            }
        }

        //010 - Start
        foreach (DataListItem item in rptTypeOfCompany.Items)
        {
            CheckBox chk = (item.FindControl("chkTypeOfCompany") as CheckBox);
            if (chk.Checked)
            {
                companyTypes += "'" + chk.ToolTip + "',";
            }
        }
        companyTypes = companyTypes.TrimEnd(',');
        //010 - End

        if (chlSearchBy.Items[0].Selected == true)
        {
            SearchBy += "V";
        }
        if (chlSearchBy.Items[1].Selected == true)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["Scopes"])))
            {
                SearchBy += "C";
            }
        }
        if (chlSearchBy.Items[2].Selected == true)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["Regions"])) || !string.IsNullOrEmpty(Convert.ToString(Session["Continents"]))) //008
            {
                SearchBy += "R";
            }

        }
        if (chlSearchBy.Items[3].Selected == true)
        {
            //004
            if (!(string.IsNullOrEmpty(Convert.ToString(Session["Projects"]))) || !string.IsNullOrEmpty(Session["OtherProject"] as String))
            {
                SearchBy += "P";
            }
        }
        if (chlSearchBy.Items[4].Selected == true)
        {
            //004 - added ISO and QS values
            if (!string.IsNullOrEmpty(Convert.ToString(Certification)) || !string.IsNullOrEmpty(Convert.ToString(FederalSB)) ||
                !string.IsNullOrEmpty(ISO) || !string.IsNullOrEmpty(QS))
            {
                SearchBy += "F";
            }
        }
        if ((chlSearchBy.Items[0].Selected == false) && (chlSearchBy.Items[1].Selected == false) && (chlSearchBy.Items[2].Selected == false) && (chlSearchBy.Items[3].Selected == false) && (chlSearchBy.Items[4].Selected == false))
        {
            SearchBy = "N";
        }
        if ((!(string.IsNullOrEmpty(Convert.ToString(Session["Scopes"])))) || (!(string.IsNullOrEmpty(Convert.ToString(Session["Regions"])))) || (!(string.IsNullOrEmpty(Convert.ToString(Session["Continents"])))) || (!(string.IsNullOrEmpty(Convert.ToString(Session["Projects"])))))
        {
            if ((chlSearchBy.Items[0].Selected == true) && (chlSearchBy.Items[1].Selected == true) && (chlSearchBy.Items[2].Selected == true) && (chlSearchBy.Items[3].Selected == true) && (chlSearchBy.Items[4].Selected == true))
            {
                SearchBy = "A";
            }
        }


        string Vendor = "", VendorNumber = "", TrackingNumber = "", FederalTaxId = "", Scopes = "", Regions = "",Continents="",Countries="", Projects = "", OtherState = "", 
        FirstName = string.Empty, LastName = string.Empty, Email = string.Empty, OtherProject = string.Empty;    //003, 004,008

        //003
        if (!string.IsNullOrEmpty(txtFirstName.Text))
        {
            FirstName = txtFirstName.Text.Trim();
        }
        //003
        if (!string.IsNullOrEmpty(txtLastName.Text))
        {
            LastName = txtLastName.Text.Trim();
        }
        //003
        if (!string.IsNullOrEmpty(txtEmail.Text))
        {
            Email = txtEmail.Text.Trim();
        }
        if (!string.IsNullOrEmpty(txtVendor.Text.Trim()))
        {
            Vendor = objCommon.ReplaceStrRevDS(txtVendor.Text.Trim());
            Session["JdeVendorId"] = Vendor;
        }
        else
        {
            Session["JdeVendorId"] = "";
        }

        if (!string.IsNullOrEmpty(txtVendorNumber.Text.Trim()))
        {
            VendorNumber = txtVendorNumber.Text.Trim();
        }
        if (!string.IsNullOrEmpty(txtTrackingNumber.Text.Trim()))
        {
            TrackingNumber = txtTrackingNumber.Text.Trim();
        }
        if (!string.IsNullOrEmpty(txtFederalTax2.Text.Trim()))
        {
            FederalTaxId =  txtFederalTax2.Text.Trim();
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["Scopes"])))
        {
            Scopes = Convert.ToString(Session["Scopes"]);
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["Regions"])))
        {
            Regions = Convert.ToString(Session["Regions"]);
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["Continents"]))) //008
        {
            Continents = Convert.ToString(Session["Continents"]);
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["Countries"]))) //008
        {
            Countries = Convert.ToString(Session["Countries"]);
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["Projects"])))
        {
            Projects = Convert.ToString(Session["Projects"]);
        }


        int OtherStateId = 0;
        foreach (GridViewRow gr in grdState3.Rows)
        {
            CheckBox chk = (CheckBox)gr.FindControl("chkCheck");
            if (chk.Checked)
            {
                Label lbl = (Label)gr.FindControl("lblName");
                if (lbl.Text == "Other")
                {
                    OtherStateId = 51;
                    OtherState = txtRegions.Text.Trim();
                }
            }
        }
        string Status = ddlStatus.SelectedItem.Text.Trim();
        switch (Status)
        {
            case "--Select--":
                Status = "";
                break;
            case "InProgress":
                Status = "0";
                break;
            case "Pending":
                Status = "1";
                break;
            case "Complete":
                Status = "2";
                break;
            case "Revision":
                Status = "3";
                break;
            // G. Vera 04/18/2012 Item 4.4 Phase 3
            case "Expired":
                Status = "6";
                break;
            default:
                Status = "";
                break;
        }

        SortExp = hdnSort.Value;
        if (SortExp == null)
        {
            SortExp = "";
        }

        // G. Vera 04/18/2012 Item 4.4 Phase 3 - Need to filter the Expired vendors for PMs since
        // the stored proc in database need too many changes for this to work.
        //003 - Dianne has asked to eliminate the filtering of expired records from PM access.
        #region Archived
        //if (Convert.ToString(Session["Access"]).Equals("2"))
        //{
        //    //003 - added new fields
        //    DataTable ds = objSearchVendor.SearchVendor(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects,
        //                                                Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.Text.Trim()), SortExp, OtherState,
        //                                                OtherStateId, Certification, FederalSB, FirstName, LastName, Email);
        //    IEnumerable<DataRow> result = from rows in ds.AsEnumerable() where rows.Field<int>("vendorstatus") != 6 select rows;

        //    if (result.Count() > 0) ds = result.CopyToDataTable();
        //    else ds = new DataTable();

        //    grdVendor.DataSource = ds;
        //}
        //else
        //{
        //    //003 - added new fields
        //    grdVendor.DataSource = objSearchVendor.SearchVendor(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects,
        //                                                        Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.Text.Trim()), SortExp, OtherState,
        //                                                        OtherStateId, Certification, FederalSB, FirstName, LastName, Email);
        //} 
        #endregion


        //003 - added new fields
        //004 - added other project field
        //010 - company types
        grdVendor.DataSource = objSearchVendor.SearchVendor(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects,
                                                                Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.Text.Trim()), SortExp, OtherState,
                                                                OtherStateId, Certification, FederalSB, FirstName, LastName, Email, Session["OtherProject"] as String, ISO, QS, "", chlSearchBy.Items[5].Selected,Continents,Countries,companyTypes);
        grdVendor.DataBind();

        //003 - changed
        //004
        //005
        //010
        hdnCount.Value = Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects, SortExp, OtherState, OtherStateId, Certification, FederalSB, FirstName, LastName, Email, (Session["OtherProject"] as String), ISO, QS, "", chlSearchBy.Items[5].Selected, Continents, Countries, companyTypes));//008
        // G. Vera 04/18/2012 Item 4.4 Phase 3
        //003 - commented out
        //if(Convert.ToString(Session["Access"]).Equals("1"))
        //    hdnCount.Value = Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects, SortExp, OtherState, OtherStateId, Certification, FederalSB));


        //003 - added new fields
        //004
        //005
        //010
        Session["Vendordetails"] = SearchBy + "~" + Vendor + "~" + VendorNumber + "~" + TrackingNumber + "~" + FederalTaxId + "~" + 
                                   Status + "~" + Scopes + "~" + Regions + "~" + Projects + "~" + hdnPage.Value + "~" + ddlNumber.Text.Trim() + "~" + 
                                   SortExp + "~" + OtherState + "~" + OtherStateId + "~" + Certification + "~" + FederalSB + "~" + FirstName + "~" +
                                   LastName + "~" + Email + "~" + (Session["OtherProject"] as String) + "~" + ISO + "~" + QS + "~" + "" + "~" + 
                                   chlSearchBy.Items[5].Selected + "~"+Continents +"~"+Countries +"~"+ companyTypes; //008
        if (grdVendor.Rows.Count <= 0)
        {
            lblmsg.Text = "Sorry no records found";
            lblmsgInfo.Text = string.Empty;
            Search.Style["display"] = "none";
            tdnextPrevious1.Style["display"] = "none";
            tdbuttons.Style["display"] = "none";
        }
        else
        {
            lblmsg.Text = string.Empty;
            Search.Style["display"] = "";
            tdnextPrevious1.Style["display"] = "";
            tdbuttons.Style["display"] = "";
            if ((Convert.ToString(Session["Access"]) == "2") && (grdVendor.Columns.Count >= 8))
            {
                grdVendor.Columns[9].Visible = true;
            }
            //003 - changed both lines below
            //004
            Session["DataSetCount"] = hdnCount.Value;       //010: Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects, SortExp, OtherState, OtherStateId, Certification, FederalSB, FirstName, LastName, Email, (Session["OtherProject"] as String), ISO, QS, "", chlSearchBy.Items[5].Selected,Continents,Countries));
            //hdnCount.Value = Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects, SortExp, OtherState, OtherStateId, Certification, FederalSB, FirstName, LastName, Email, (Session["OtherProject"] as String), ISO, QS, "", chlSearchBy.Items[5].Selected, Continents,Countries));
            // G. Vera 04/18/2012 Item 4.4 Phase 3
            //003 - commented out
            //if (Convert.ToString(Session["Access"]).Equals("1"))
            //{
            //    hdnCount.Value = Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects, SortExp, OtherState, OtherStateId, Certification, FederalSB));
            //    Session["DataSetCount"] = Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions, Projects, SortExp, OtherState, OtherStateId, Certification, FederalSB));
            //}

            
            Session["VendorDetailsCount"] = Session["DataSetCount"];
            lblTotal.Text = Convert.ToString(Session["DataSetCount"]);
            trGrid.Style["Display"] = "";
        }
    }

    /// <summary>
    /// 004 - added
    /// </summary>
    private void ValidateIndustryCerts()
    {
        string cert = string.Empty;

        foreach (DataListItem i in dlIndustryCerts.Items)
        {
            CheckBox chk = (i.FindControl("chkIndustryCert") as CheckBox);
            TextBox txtB = (i.FindControl("txtIndustryCert") as TextBox);

            if (chk.Checked)
            {
                if (string.IsNullOrEmpty(txtB.Text))
                {
                    chk.Checked = false;
                    txtB.Attributes["style"] = "display: none";
                }
            }

        }
    }

    void BindVendor()
    {
        string Sort;
        if (string.IsNullOrEmpty(hdnSort.Value))
        {
            Sort = "";
        }
        else
        {
            Sort = hdnSort.Value;
        }
        string[] BindingValue = Convert.ToString(Session["Vendordetails"]).Split('~');
        if (BindingValue.Length >= 15)
        {
            //003 - added new fields 16 - 18
            //004
            //005
            //010
            grdVendor.DataSource = objSearchVendor.SearchVendor(BindingValue[0], BindingValue[1], BindingValue[2], BindingValue[3], BindingValue[4], 
                                                                BindingValue[5], BindingValue[6], BindingValue[7], BindingValue[8], 
                                                                Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.Text.Trim()), Sort, BindingValue[12], 
                                                                Convert.ToInt32(BindingValue[13]), BindingValue[14], BindingValue[15], BindingValue[16],
                                                                BindingValue[17], BindingValue[18], BindingValue[19], BindingValue[20], BindingValue[21], BindingValue[22], 
                                                                Convert.ToBoolean(BindingValue[23]), BindingValue[24].ConvertToString(), BindingValue[25].ConvertToString(), BindingValue[26]);//008
            grdVendor.DataBind();


            //003 - changed
            //004
            //005
            //010
            Session["VendordetailsCount"] = objSearchVendor.GetDetailsCountAdmin(BindingValue[0], BindingValue[1], BindingValue[2], BindingValue[3], BindingValue[4], BindingValue[5], BindingValue[6], BindingValue[7], 
                                                                                 BindingValue[8], Sort, BindingValue[12], Convert.ToInt32(BindingValue[13]), BindingValue[14], BindingValue[15], BindingValue[16], 
                                                                                 BindingValue[17], BindingValue[18], BindingValue[19], BindingValue[20], BindingValue[21], BindingValue[22], 
                                                                                 Convert.ToBoolean(BindingValue[23]), BindingValue[24].ConvertToString(), BindingValue[25].ConvertToString(), BindingValue[26]);//008
            // G. Vera 04/18/2012 Item 4.4 Phase 3
            //003 - changed
            //k (Convert.ToString(Session["Access"]).Equals("1"))
            //    Session["VendordetailsCount"] = objSearchVendor.GetDetailsCountAdmin(BindingValue[0], BindingValue[1], BindingValue[2], BindingValue[3], BindingValue[4], BindingValue[5], BindingValue[6], BindingValue[7], BindingValue[8], Sort, BindingValue[12], Convert.ToInt32(BindingValue[13]), BindingValue[14], BindingValue[15]);
            //003
            //004
            //005
            
            Session["Vendordetails"] = BindingValue[0] + "~" + BindingValue[1] + "~" + BindingValue[2] + "~" + BindingValue[3] + "~" + BindingValue[4] + "~" + BindingValue[5] + "~" + BindingValue[6] + "~" + 
                BindingValue[7] + "~" + BindingValue[8] + "~" + hdnPage.Value + "~" + ddlNumber.Text.Trim() + "~" + Sort + "~" + BindingValue[12] + "~" + BindingValue[13] + "~" + BindingValue[14] + "~" + 
                BindingValue[15] + "~" + BindingValue[16] + "~" + BindingValue[17] + "~" + BindingValue[18] + "~" + BindingValue[19] + "~" + BindingValue[20] + "~" + BindingValue[21] + "~" +
                BindingValue[22] + "~" + BindingValue[23] + "~" + BindingValue[24] + "~" + BindingValue[25] + "~" + BindingValue[26];   //008, 010
        }
        hdnCount.Value = Convert.ToString(Session["VendordetailsCount"]);
        lblTotal.Text= hdnCount.Value;
        //005
        int page;
        if (Session["SelectedNumber"] != null) page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.Items[Convert.ToInt32(Session["SelectedNumber"])].Text))));
        else page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));
        
        Session["Page"] = Convert.ToString(page);
        lblTotalPage.Text= Convert.ToString(Session["Page"]);
        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if (hdnPage.Value == "1")
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
    }

    /// <summary>
    /// Display records per page based on selected value of the dropdownlist
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        //005 do not reset page: hdnPage.Value = "1";
        //005
        int page;
        if (Session["SelectedNumber"] != null) page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.Items[Convert.ToInt32(Session["SelectedNumber"])].Text) )));
        else page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));

        Session["Page"] = Convert.ToString(page);
        Session["SelectedNumber"] = ddlNumber.SelectedIndex; //005

        //  lblCurrentPage.Text= Convert.ToString(hdnPage.Value);
        lblTotalPage.Text= Convert.ToString(Session["Page"]);
        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if (hdnPage.Value == "1")
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }

        BindVendor();
        plcCrntPage1.Controls.Clear();
        for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
        {
            if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
            {
                LinkButton lnk = new LinkButton();
                lnk.Click += new EventHandler(lnk1_Click);
                lnk.Text= Convert.ToString(i);
                if (i == Convert.ToInt32(hdnPage.Value))
                {
                    lnk.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk.CssClass = "lnkcolor";
                }
                plcCrntPage1.Controls.Add(lnk);
                lnk.Text= Convert.ToString(i);
                lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnPage.ClientID + "')");
            }
        }
        foreach (Control ctrl in plcCrntPage1.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }
    }

    /// <summary>
    /// Display records per page based on selected value of the dropdownlist
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlSearchNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnSearchPage.Value = "1";
        hdnSearchCount.Value = Convert.ToString(objSearchVendor.GetSearchPriorCount(Convert.ToString(Session["EmployeeName"])));

        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnSearchCount.Value) / Convert.ToDecimal(ddlSearchNumber.SelectedItem.Text.Trim()))));
        Session["SavedPage"] = Convert.ToString(page);
        lblSearchTotalPage.Text = Convert.ToString(Session["SavedPage"]);
        if (Convert.ToInt32(Session["SavedPage"]) <= Convert.ToInt32(hdnSearchPage.Value))
        {
            imbSearchNext.Visible = false;
        }
        else
        {
            imbSearchNext.Visible = true;
        }
        if (Convert.ToString(hdnSearchPage.Value) == "1")
        {
            imbSearchPrevious.Visible = false;
        }
        else
        {
            imbSearchPrevious.Visible = true;
        }
        plcCrntPage.Controls.Clear();
        for (int i = (Convert.ToInt32(hdnSearchPage.Value) - 5); i <= (Convert.ToInt32(hdnSearchPage.Value) + 5); i++)
        {
            if (i > 0 && i <= Convert.ToInt32(Session["SavedPage"]))
            {
                LinkButton lnk = new LinkButton();
                lnk.Click += new EventHandler(lnk_Click);
                lnk.Text = Convert.ToString(i);
                if (i == Convert.ToInt32(hdnSearchPage.Value))
                {
                    lnk.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk.CssClass = "lnkcolor";
                }
                plcCrntPage.Controls.Add(lnk);
                lnk.Text = Convert.ToString(i);
                lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ",'" + hdnSearchPage.ClientID + "')");
            }
        }
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnSearchPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";

                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }
        SaveBind();
    }

    /// <summary>
    /// Display the next record in the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSearchNext_Click(object sender, ImageClickEventArgs e)
    {
        hdnSearchPage.Value = Convert.ToString(Convert.ToInt32(hdnSearchPage.Value) + 1);
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnSearchPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }
        lblSearchTotalPage.Text= Convert.ToString(Session["SavedPage"]);
        if (Convert.ToInt32(Session["SavedPage"]) <= Convert.ToInt32(hdnSearchPage.Value))
        {
            imbSearchNext.Visible = false;
        }
        else
        {
            imbSearchNext.Visible = true;
        }
        if (Convert.ToString(hdnSearchPage.Value) == "1")
        {
            imbSearchPrevious.Visible = false;
        }
        else
        {
            imbSearchPrevious.Visible = true;
        }
        SaveBind();
    }

    /// <summary>
    /// Display the page of the selected link
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void lnk_Click(object sender, EventArgs e)
    {
        LinkButton Currentlnk = new LinkButton();
        Currentlnk = (LinkButton)sender;
        hdnSearchPage.Value = Currentlnk.Text.Trim();

        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == Currentlnk.Text.Trim())
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }
        if (Convert.ToInt32(Session["SavedPage"]) <= Convert.ToInt32(hdnSearchPage.Value))
        {
            imbSearchNext.Visible = false;
        }
        else
        {
            imbSearchNext.Visible = true;
        }
        if (hdnSearchPage.Value == "1")
        {
            imbSearchPrevious.Visible = false;
        }
        else
        {
            imbSearchPrevious.Visible = true;
        }
        SaveBind();
    }

    //Display the page of the selected link
    void lnk1_Click(object sender, EventArgs e)
    {
        LinkButton Currentlnk = new LinkButton();
        Currentlnk = (LinkButton)sender;
        hdnPage.Value = Currentlnk.Text.Trim();

        foreach (Control ctrl in plcCrntPage1.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == Currentlnk.Text.Trim())
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }
        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if (hdnPage.Value == "1")
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        BindVendor();
    }

    /// <summary>
    /// Display the previous record of teh page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSearchPrevious_Click(object sender, ImageClickEventArgs e)
    {
        hdnSearchPage.Value = Convert.ToString(Convert.ToInt32(hdnSearchPage.Value) - 1);
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnSearchPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }
        lblSearchTotalPage.Text= Convert.ToString(Session["SavedPage"]);

        if (Convert.ToInt32(Session["SavedPage"]) <= Convert.ToInt32(hdnSearchPage.Value))
        {
            imbSearchNext.Visible = false;
        }
        else
        {
            imbSearchNext.Visible = true;
        }
        if (Convert.ToString(hdnSearchPage.Value) == "1")
        {
            imbSearchPrevious.Visible = false;
        }
        else
        {
            imbSearchPrevious.Visible = true;
        }
        SaveBind();
    }

    /// <summary>
    /// Display the next record of the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbNext_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) + 1);
        foreach (Control ctrl in plcCrntPage1.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }

        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if (hdnPage.Value == "1")
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        BindVendor();
    }

    /// <summary>
    /// Display the previous record in the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrevious_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) - 1);
        foreach (Control ctrl in plcCrntPage1.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";

                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }

        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if (hdnPage.Value == "1")
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        BindVendor();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImbNo_Click(object sender, ImageClickEventArgs e)
    {
        lblmsg.Visible = true;
        lblmsg.Text= "Please select another search criteria or contact haskell administrator. ";
        Search.Style["display"] = "none";
        tdnextPrevious1.Style["display"] = "none";
        tdbuttons.Style["display"] = "none";
        // TextBox3.Text= "";
        grdVendor.Visible = false;
        imbActive.Visible = false;
        imbexcel.Visible = false;
        spnVendorDetails.Style["display"] = "";
    }

    /// <summary>
    /// Display the text of the link based on the conditions
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnVendorId = new HiddenField();
            hdnVendorId = (HiddenField)e.Row.FindControl("hdnVendorId");
            //AjaxControlToolkit.Rating rtg = new AjaxControlToolkit.Rating();
            AjaxControlToolkit.Rating rtg = (AjaxControlToolkit.Rating)e.Row.FindControl("rtgStar");
            LinkButton lnkSRF = new LinkButton();
            lnkSRF = (LinkButton)e.Row.FindControl("lnkSRF");

            LinkButton lnk = new LinkButton();
            lnk = (LinkButton)e.Row.FindControl("lnkVQF");
            Label lblStatus = new Label();
            lblStatus = (Label)e.Row.FindControl("lblStatusDesc");

            LinkButton lnkSRRort = new LinkButton();
            lnkSRRort = (LinkButton)e.Row.FindControl("lnkSRRort");
            //if()
            //{
            //}

            // SOC - 12/29/2010
            ImageButton paperclipButton = (ImageButton)e.Row.FindControl("imgPaperClip");
            if (objCommon.GetDocumentCount(Convert.ToInt64(hdnVendorId.Value)) > 0)
            {
                paperclipButton.ImageUrl = "~/Images/paperclip.png";
                //012 - START
                Label lblAttToolTip = (Label)e.Row.FindControl("lblAttToolTip");
                lblAttToolTip.Text = LoadAttachmentToolTip(hdnVendorId.Value);
                HtmlGenericControl panel = (HtmlGenericControl)e.Row.FindControl("divAttToolTip");
                string showPopup = "ShowPopup('" + panel.ClientID + "')";
                string hidePopup = "HidePopup('" + panel.ClientID + "')";
                e.Row.Cells[0].Attributes.Add("onmouseover", "javascript:" + showPopup);
                e.Row.Cells[0].Attributes.Add("onmouseout", "javascript:" + hidePopup);
                //paperclipButton.ToolTip = LoadAttachmentToolTip(hdnVendorId.Value);

                //012 - END
            }
            else
            {
                paperclipButton.ImageUrl = "~/Images/paperclip_gray.png";
            }
            // EOC

            #region 012 Load Rating Stars and Financials tag
            //012
            LoadRatingStars(e.Row);
            LoadFinancialTagNotes(e.Row);
            #endregion

            switch (lblStatus.Text)
            {
                case "Complete":
                    if (objSearchVendor.SubContratorRatingCount(Convert.ToInt64(hdnVendorId.Value)) > 0)
                    {
                        //012 - commented these out
                        //lnkSRF.Visible = true;
                        //rtg.Visible = true;
                        
                    }
                    else
                    {
                        //012 - commented these out
                        //lnkSRF.Visible = false; //012
                        //rtg.Visible = false;
                    }
                    lblStatus.CssClass = "greenbold";
                    break;
                    //002 - added
                case "Expired":
                    if (objSearchVendor.SubContratorRatingCount(Convert.ToInt64(hdnVendorId.Value)) > 0)
                    {
                        //012 - commented these out
                        //lnkSRF.Visible = true;  //012
                        //rtg.Visible = true;

                    }
                    else
                    {
                        //012 - commented these out
                        //lnkSRF.Visible = false;
                        // rtg.Visible = false;
                    }
                    lblStatus.CssClass = "redbold";
                    break;
                case "Revision":
                    if (objSearchVendor.SubContratorRatingCount(Convert.ToInt64(hdnVendorId.Value)) > 0)
                    {
                        //012 - commented these out
                        //lnkSRF.Visible = true;  
                        //rtg.Visible = true;
                        //012

                    }
                    else
                    {
                        //012 - commented these out
                        //lnkSRF.Visible = false; //012
                        //rtg.Visible = false;
                    }
                    HtmlGenericControl panel = (HtmlGenericControl)e.Row.FindControl("gridPopup");
                    lblStatus.CssClass = "redbold";
                    break;
                case "PreComplete":
                    //012 - commented these out
                    //lnkSRF.Visible = false; //012
                    //rtg.Visible = false;
                    lblStatus.CssClass = "redbold";
                    lblStatus.Text = "Pending";
                    break;
                case "JDE":
                    //012 - commented these out
                    //lnkSRF.Visible = false; //012
                    //rtg.Visible = false;
                    lblStatus.Text = string.Empty;
                    Label lbltrack = new Label();
                    lbltrack = (Label)e.Row.FindControl("lblTrackingNumber");
                    lbltrack.Text = string.Empty;
                    break;


                default:
                    //012 - commented these out
                    //lnkSRF.Visible = false; //012
                    //rtg.Visible = false;
                    lblStatus.CssClass = "redbold";
                    break;
            }
            if (Convert.ToString(Session["Access"]).Equals("2"))
            {
                switch (lnkSRRort.Text)
                {

                    case "VAR":

                        SRAttch = objCommon.GetAttchDocumentData(Convert.ToInt64(hdnVendorId.Value), false, true);

                        if (Convert.ToInt32(SRAttch.Tables[0].Rows.Count) > 0)
                        {
                            lnkSRRort.Visible = true;
                            lnkSRRort.CssClass = "green";

                            #region 012
                            //012 - START
                            Label lblVARToolTip = (Label)e.Row.FindControl("lblVARToolTip");
                            lblVARToolTip.Text = LoadVARToolTip(SRAttch);
                            HtmlGenericControl panel = (HtmlGenericControl)e.Row.FindControl("divVARToolTip");
                            string showPopup = "ShowPopup('" + panel.ClientID + "')";
                            string hidePopup = "HidePopup('" + panel.ClientID + "')";
                            e.Row.Cells[5].Attributes.Add("onmouseover", "javascript:" + showPopup);
                            e.Row.Cells[5].Attributes.Add("onmouseout", "javascript:" + hidePopup);
                            //012 - END
                            #endregion

                        }
                        else
                        {
                            lnkSRRort.Visible = false;

                        }
                        //lnkSRRort.CssClass = "greenbold";
                        break;

                }
            }
            else
            {
                switch (lnkSRRort.Text)
                {

                    case "VAR":

                        SRAttch = objCommon.GetAttchDocumentData(Convert.ToInt64(hdnVendorId.Value), false, true);

                        if (Convert.ToInt32(SRAttch.Tables[0].Rows.Count) > 0)
                        {
                            
                            lnkSRRort.CssClass = "green";
                            #region 012
                            //012 - START
                            Label lblVARToolTip = (Label)e.Row.FindControl("lblVARToolTip");
                            lblVARToolTip.Text = LoadVARToolTip(SRAttch);
                            HtmlGenericControl panel = (HtmlGenericControl)e.Row.FindControl("divVARToolTip");
                            string showPopup = "ShowPopup('" + panel.ClientID + "')";
                            string hidePopup = "HidePopup('" + panel.ClientID + "')";
                            e.Row.Cells[5].Attributes.Add("onmouseover", "javascript:" + showPopup);
                            e.Row.Cells[5].Attributes.Add("onmouseout", "javascript:" + hidePopup);
                            //012 - END
                            #endregion

                        }
                        else
                        {
                            lnkSRRort.CssClass = "grey";

                        }
                        //lnkSRRort.CssClass = "greenbold";
                        break;

                }
            }
            Label lblVendorName = new Label();
            lblVendorName = (Label)e.Row.FindControl("lblVendorName1");
            Label lbljde = new Label();
            HiddenField hdnJde = (HiddenField)e.Row.FindControl("hdnJDE");
            lbljde = (Label)e.Row.FindControl("JDER");
            //GV 10/31/2014: not working properly, using the actual data returned from the search vendor stored proc below...why call the database again? ---> 
            //lbljde.Text = objSearchVendor.CheckJDENumber(lblVendorName.Text.Trim());
            lbljde.Text = (hdnJde.Value == "False") ? "N" : "Y";  
            if (lbljde.Text== "Y")
            {
                lbljde.CssClass = "greenbold";
            }
            else if (lbljde.Text== "N")
            {
                lbljde.CssClass = "redbold";
            }
            DataTable dt = objSearchVendor.GetCommentsAndTag(Convert.ToInt64(hdnVendorId.Value));
            Image img = new Image();
            img = (Image)e.Row.FindControl("imgTag");
            Image imgVPP = new Image();
            imgVPP = (Image)e.Row.FindControl("imgVPP");
            if (dt.Rows.Count > 0)
            {
                if ((string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["VPPFlag"]))) || (Convert.ToString(dt.Rows[0]["VPPFlag"])) == "False")
                {
                    imgVPP.Visible = false;
                }
                else
                {
                    imgVPP.Visible = true;
                }
                if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["Tag"])))
                {
                    img.Visible = false;
                }
                else if (Convert.ToString(dt.Rows[0]["Tag"]).Equals("0"))
                {
                    img.Visible = true;
                    img.ImageUrl = @"../Images/red.png";
                    HtmlGenericControl panel = (HtmlGenericControl)e.Row.FindControl("gridPopup1");
                    string showPopup = "ShowPopup('" + panel.ClientID + "')";
                    string hidePopup = "HidePopup('" + panel.ClientID + "')";
                    Label lblComments = new Label();
                    lblComments = (Label)e.Row.FindControl("lbtngrdPrint1");
                    lblComments.Text= Convert.ToString(dt.Rows[0]["AddNotes"]);
                    e.Row.Cells[2].Attributes.Add("onmouseover", "javascript:" + showPopup);
                    e.Row.Cells[2].Attributes.Add("onmouseout", "javascript:" + hidePopup);
                }
                else if (Convert.ToString(dt.Rows[0]["Tag"]).Equals("1"))
                {
                    img.Visible = true;
                    img.ImageUrl = @"../Images/yellow.png";
                    HtmlGenericControl panel = (HtmlGenericControl)e.Row.FindControl("gridPopup1");
                    string showPopup = "ShowPopup('" + panel.ClientID + "')";
                    string hidePopup = "HidePopup('" + panel.ClientID + "')";
                    Label lblComments = new Label();
                    lblComments = (Label)e.Row.FindControl("lbtngrdPrint1");
                    lblComments.Text= Convert.ToString(dt.Rows[0]["AddNotes"]);
                    e.Row.Cells[2].Attributes.Add("onmouseover", "javascript:" + showPopup);
                    e.Row.Cells[2].Attributes.Add("onmouseout", "javascript:" + hidePopup);
                }
                else if (Convert.ToString(dt.Rows[0]["Tag"]).Equals("2"))
                {
                    img.Visible = false;
                }
                else
                {
                    img.Visible = false;
                }
            }
            else
            {
                img.Visible = false;
                imgVPP.Visible = false;
            }
        }
    }

    

    //012
    private void LoadRatingStars(GridViewRow row)
    {
        var literal = (Literal)row.FindControl("ltrlRatings");
        var count = ((HiddenField)row.FindControl("hdnRatingCount")).Value.ConvertToInteger();
        var hdnVendorId = (HiddenField)row.FindControl("hdnVendorId");

        string html = string.Empty;
        
        if (count <= 0)
        {
            for (int i = 0; i < 5; i++)
            {
                html += emptyRatingStar;
            }
        }
        else
        {
            string redirect = "DisplayRating.aspx?VendorId=" + Convert.ToString(objCommon.encode(hdnVendorId.Value)) + "&FromPage=SearchVendor";
            html = "<a href=\"" + redirect + "\">";
            for (int i = 0; i < 5; i++)
            {
                if (i < count)
                    html += filledRatingStar;
                else
                    html += emptyRatingStar;

            }

            html += "<a/>";
        }

        literal.Text = html;
        
    }

    //012
    private void LoadFinancialTagNotes(GridViewRow row)
    {
        var literal = (Literal)row.FindControl("ltrlFinancials");
        var hdnVendorId = (HiddenField)row.FindControl("hdnVendorId");
        var financials = objCompany.Checknotes(Convert.ToInt64(hdnVendorId.Value));
        bool hasFinancials = (financials.Tables[0].Rows.Count > 0) ? ((financials.Tables[0].Rows[0]["IsFinancialsReceieved"] is DBNull) ? false : (bool)(financials.Tables[0].Rows[0]["IsFinancialsReceieved"])) : false;
        string finNotes = (financials.Tables[0].Rows.Count > 0) ? (financials.Tables[0].Rows[0]["FinancialsComment"] as string) : string.Empty;

        if(hasFinancials)
        {
            literal.Text = greenDollar;
            Label lblToolTip = (Label)row.FindControl("lblFinToolTip");
            lblToolTip.Text = finNotes;
            HtmlGenericControl panel = (HtmlGenericControl)row.FindControl("divFinToolTip");
            string showPopup = "ShowPopup('" + panel.ClientID + "')";
            string hidePopup = "HidePopup('" + panel.ClientID + "')";
            row.Cells[1].Attributes.Add("onmouseover", "javascript:" + showPopup);
            row.Cells[1].Attributes.Add("onmouseout", "javascript:" + hidePopup);
        }
        else
        {
            literal.Text = greyDollar;
        }
    }

    /// <summary>
    /// Sorting the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;
        if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortDirection"])))
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
        else if (Convert.ToString(ViewState["SortDirection"]) == Convert.ToString(SortDirection.Ascending))
        {
            GridViewSortDirection = SortDirection.Descending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Descending);
            SortGridView(sortExpression, "DESC");
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
    }

    /// <summary>
    /// Get sorting direction
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
            {
                ViewState["sortDirection"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["sortDirection"];
        }
        set
        {
            ViewState["sortDirection"] = value;
        }
    }

    /// <summary>
    /// Method to sort gridview
    /// </summary>
    /// <param name="sortExpression"></param>
    /// <param name="direction"></param>
    private void SortGridView(string sortExpression, string direction)
    {
        SortExp = sortExpression + " " + direction;
        hdnSort.Value = SortExp;  
        BindVendor();
    }

    /// <summary>
    /// Save the displayed record in the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAddSave_Click(object sender, ImageClickEventArgs e)
    {
        if (string.IsNullOrEmpty(txtName.Text.Trim().Trim()))
        {
            lblvalid.Text= "<li>Please enter name";
            modalName.Show();
        }
        else
        {
            string FirstName = string.Empty, LastName = string.Empty, Email = string.Empty, OtherProject = string.Empty,
                   ISO = string.Empty, QS = string.Empty, companyTypes = string.Empty;     //003, 004, 010

            //004
            if (!String.IsNullOrEmpty(Session["OtherProject"] as String))
            {
                OtherProject = Session["OtherProject"] as String;
            }
            //003
            if (!String.IsNullOrEmpty(txtFirstName.Text))
            {
                FirstName = txtFirstName.Text.Trim();
            }
            //003
            if (!String.IsNullOrEmpty(txtEmail.Text))
            {
                Email = txtEmail.Text.Trim();
            }
            //003
            if (!String.IsNullOrEmpty(txtLastName.Text))
            {
                LastName = txtLastName.Text.Trim();
            }

            //010 - Start
            foreach (DataListItem item in rptTypeOfCompany.Items)
            {
                CheckBox chk = (item.FindControl("chkTypeOfCompany") as CheckBox);
                if (chk.Checked)
                {
                    companyTypes += "'" + chk.ToolTip + "',";
                }
            }
            companyTypes = companyTypes.TrimEnd(',');
            //010 - End

            lblvalid.Text= string.Empty;
            int GetResult = objSearchVendor.CheckNameExists(Convert.ToInt64(Session["EmployeeId"]), txtName.Text.Trim().Trim());
            if (GetResult == 0)
            {
                hdnName.Value = txtName.Text.Trim();
                txtName.Text= string.Empty;

                string SearchBy = string.Empty;
                if (chlSearchBy.Items[0].Selected)
                {
                    if (string.IsNullOrEmpty(SearchBy))
                    {
                        SearchBy = "V";
                    }
                    else
                    {
                        SearchBy += "," + "V";
                    }
                }
                if (chlSearchBy.Items[1].Selected)
                {
                    if (string.IsNullOrEmpty(SearchBy))
                    {
                        SearchBy = "C";
                    }
                    else
                    {
                        SearchBy += "," + "C";
                    }
                }
                if (chlSearchBy.Items[2].Selected)
                {
                    if (string.IsNullOrEmpty(SearchBy))
                    {
                        SearchBy += "R";
                    }
                    else
                    {
                        SearchBy += "," + "R";
                    }
                }
                if (chlSearchBy.Items[3].Selected)
                {
                    if (string.IsNullOrEmpty(SearchBy))
                    {
                        SearchBy += "P";
                    }
                    else
                    {
                        SearchBy += "," + "P";
                    }
                }
                if (chlSearchBy.Items[4].Selected)
                {
                    if (string.IsNullOrEmpty(SearchBy))
                    {
                        SearchBy += "F";
                    }
                    else
                    {
                        SearchBy += "," + "F";
                    }
                }
                if ((!chlSearchBy.Items[0].Selected) && (!chlSearchBy.Items[1].Selected) && (!chlSearchBy.Items[2].Selected) && (!chlSearchBy.Items[3].Selected) && (!chlSearchBy.Items[4].Selected))
                {
                    SearchBy = "N";
                }
                if ((chlSearchBy.Items[0].Selected) && (chlSearchBy.Items[1].Selected) && (chlSearchBy.Items[2].Selected) && (chlSearchBy.Items[3].Selected) && (chlSearchBy.Items[4].Selected))
                {
                    SearchBy = "A";
                }
                string VendorId = null;
                string VendorNumber = null;
                if (!string.IsNullOrEmpty(txtVendorNumber.Text.Trim()))
                {
                    VendorNumber = txtVendorNumber.Text.Trim();
                }
                if (!string.IsNullOrEmpty(txtVendor.Text.Trim()))
                {
                    VendorId = txtVendor.Text.Trim();
                }
                string OtherState = null;
                int OtherStateId = 0;
                foreach (GridViewRow gr in grdState3.Rows)
                {
                    CheckBox chk = (CheckBox)gr.FindControl("chkCheck");
                    if (chk.Checked)
                    {
                        Label lbl = (Label)gr.FindControl("lblName");
                        if (lbl.Text== "Other")
                        {
                            OtherStateId = 51;
                            OtherState = txtRegions.Text.Trim();
                        }
                    }
                }
                string Certification = string.Empty, FederalSB = string.Empty;
                foreach (ListItem Item in rdoCompanyCertiY.Items)
                {
                    if (Item.Selected)
                    {
                        Certification += string.IsNullOrEmpty(Certification) ? Item.Text : "," + Item.Text;
                    }
                }

                foreach (ListItem Item in chklFederalSB.Items)
                {
                    if (Item.Selected)
                    {
                        FederalSB += string.IsNullOrEmpty(FederalSB) ? Item.Text : "," + Item.Text;
                    }
                }

                //004
                //ValidateIndustryCerts();

                foreach (DataListItem i in dlIndustryCerts.Items)
                {
                    CheckBox chk = (i.FindControl("chkIndustryCert") as CheckBox);
                    TextBox txt = (i.FindControl("txtIndustryCert") as TextBox);
                    HiddenField hdn = (i.FindControl("hdnCertificationID") as HiddenField);

                    if (chk.Checked)
                    {
                        if (!string.IsNullOrEmpty(txt.Text))
                        {
                            if (chk.Text.ToUpper().Trim() == "ISO") ISO = txt.Text;
                            if (chk.Text.ToUpper().Trim() == "QS") QS = txt.Text;
                        }
                        else
                        {
                            if (chk.Text.ToUpper().Trim() == "ISO") ISO = "ALL";
                            if (chk.Text.ToUpper().Trim() == "QS") QS = "ALL";

                        }
                    }
                }
                
                long DBCount = 0;

                //003 - added new fields
                //004 - added new fields
                //005
                //010
                if ( string.IsNullOrEmpty(VendorId) && string.IsNullOrEmpty(VendorNumber) && string.IsNullOrEmpty(OtherState) && 
                    string.IsNullOrEmpty(Convert.ToString(Session["Scopes"])) && string.IsNullOrEmpty(Convert.ToString(Session["Projects"])) && 
                    string.IsNullOrEmpty(txtFederalTax2.Text.Trim()) && string.IsNullOrEmpty(txtFederalTax2.Text.Trim()) &&
                    string.IsNullOrEmpty(txtTrackingNumber.Text.Trim()) && string.IsNullOrEmpty(Convert.ToString(Session["Regions"])) && string.IsNullOrEmpty(Convert.ToString(Session["Continents"])) &&   //008
                    (Certification == "" && FederalSB == "")   && ddlStatus.SelectedIndex == 0 &&
                    String.IsNullOrEmpty(FirstName) && String.IsNullOrEmpty(LastName) && String.IsNullOrEmpty(Email) &&
                    String.IsNullOrEmpty(Session["OtherProject"] as String) && string.IsNullOrEmpty(ISO) && string.IsNullOrEmpty(QS) && 
                    !chlSearchBy.Items[5].Selected && string.IsNullOrEmpty(Convert.ToString(Session["Continents"])) && string.IsNullOrEmpty(Convert.ToString(Session["Countries"])) && 
                    string.IsNullOrEmpty(companyTypes) )
                {
                    DBCount = 0;
                }
                else
                {
                    DBCount = 1;
                }
                string Status = null;
                if (ddlStatus.SelectedIndex != 0)
                {
                    Status = ddlStatus.SelectedItem.Text.Trim();
                }
                
                //003 - changed
                //004
                //005
                string scopes = Convert.ToString(Session["Scopes"]);
                string regions = Convert.ToString(Session["Regions"]);
                string continents = Convert.ToString(Session["Continents"]);//008
                string countries = Convert.ToString(Session["Countries"]);//008

                if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentScopes"])))
                {
                    string[] parentS = Convert.ToString(Session["ParentScopes"]).Split(',');
                    string[] test = scopes.Split(',');

                    foreach (string item in parentS)
                    {
                        if (!test.Contains(item) && !string.IsNullOrEmpty(item))
                        {
                            if (!string.IsNullOrEmpty(scopes)) scopes += "," + item;
                            else scopes += item + ",";
                        }
                    }
                    if (scopes.Substring(scopes.Length - 1) == ",") scopes = scopes.Remove(scopes.Length - 1);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentRegions"])))
                {
                    string[] parentR = Convert.ToString(Session["ParentRegions"]).Split(',');
                    string[] test = regions.Split(',');
                    foreach (string item in parentR)
                    {
                        if (!test.Contains(item) && !string.IsNullOrEmpty(item))
                        {
                            regions += "," + item;
                        }
                    }
                    //regions += Convert.ToString(Session["ParentRegions"]);
                }


                //005
                //010
                objSearchVendor.InsertSearchVendor(scopes, hdnName.Value, SearchBy, Convert.ToInt64(Session["EmployeeId"]), 
                                                    VendorNumber, Convert.ToString(Session["Projects"]), regions, 
                                                     txtFederalTax2.Text.Trim(), txtTrackingNumber.Text.Trim(), VendorId,
                                                    OtherState, DBCount, Status, Certification, FederalSB, FirstName, LastName, Email, OtherProject, ISO, QS, chlSearchBy.Items[5].Selected, continents,countries,companyTypes);
                if (grdSaveSearch.Rows.Count > 0)
                {
                    SaveBind();
                }
                lblInfo.Text= "Search saved successfully";
                modalName.Hide();
                modalInfo.Show();
            }
            else
            {
                modalName.Show();
                lblvalid.Text= "Name already exists";
            }
        }
    }

    /// <summary>
    /// Display teh text 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSaveSearch_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lblSearch = new LinkButton();
            lblSearch = (LinkButton)e.Row.FindControl("lblsearch");
            HiddenField hdn = (HiddenField)e.Row.FindControl("hdnCount");
            Label lblRoot = (Label)e.Row.FindControl("lblRoot");
            string Search;
            string SearchRoot = string.Empty;
            Search = lblSearch.Text.Trim();

            LinkButton lnkName = (LinkButton)e.Row.FindControl("lblname");
            lnkName.Attributes.Add("onClick", "Javascript:AssignValue('" + hdnFlag.ClientID + "')");

            //005 to find out if they checked BIM
            HiddenField hdnRowId = new HiddenField();
            hdnRowId = (HiddenField)e.Row.FindControl("hdnHistoryId");
            long Rowvalue = Convert.ToInt64(hdnRowId.Value);
            List<SearchHistory> objdetails = objSearchVendor.GetSearchDetails(Rowvalue);


            if ((string.IsNullOrEmpty(hdn.Value)) || (hdn.Value== "0"))
            {
                string[] SearchCriteria = Search.Split(',');
                foreach (string strSearch in SearchCriteria)
                {
                    if (strSearch.ToUpper() == "C")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Scopes of Work (All)";
                        }
                        else
                        {
                            SearchRoot += ", " + "Scopes of Work (All)";
                        }
                    }
                    if (strSearch.ToUpper() == "V")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Vendor Names (All)";
                        }
                        else
                        {
                            SearchRoot += ", " + "Vendor Names (All)";
                        }
                    }
                    if (strSearch.ToUpper() == "R")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Geographic Locations (All)";
                        }
                        else
                        {
                            SearchRoot += ", " + "Geographic Locations (All)";
                        }
                    }
                    if (strSearch.ToUpper() == "P")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Types Of Projects (All)";
                        }
                        else
                        {
                            SearchRoot += ", " + "Types Of Projects (All)";
                        }
                    }
                    if (strSearch.ToUpper() == "F")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Certification (All)";
                        }
                        else
                        {
                            SearchRoot += ", " + "Certification (All)";
                        }
                    }
                    if (strSearch.ToUpper() == "N")
                    {
                        SearchRoot = "No Search Criteria Selected";
                    }
                    if (strSearch.ToUpper() == "A")
                    {
                        SearchRoot = "Vendor Names(All), Scopes of Work (All), Geographic Locations (All), Types Of Projects (All), Certification (ALL)";
                    }
                }

                //005
                if (objdetails.Count > 0)
                {
                    if (objdetails[0].ProvidesBIM.Trim() == "Y")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Provides BIM";
                        }
                        else
                        {
                            SearchRoot += ", " + "Provides BIM";
                        }
                    } 
                }

                lblSearch.Visible = false;
                lblRoot.Visible = true;
                lblRoot.Text= SearchRoot;
            }
            else
            {

                string[] SearchCriteria = Search.Split(',');
                foreach (string strSearch in SearchCriteria)
                {
                    if (strSearch.ToUpper() == "C")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Scopes of Work";
                        }
                        else
                        {
                            SearchRoot += ", " + "Scopes of Work";
                        }
                    }
                    if (strSearch.ToUpper() == "V")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Vendor Name";
                        }
                        else
                        {
                            SearchRoot += ", " + "Vendor Name";
                        }
                    }
                    if (strSearch.ToUpper() == "R")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Geographic Locations";
                        }
                        else
                        {
                            SearchRoot += ", " + "Geographic Locations";
                        }
                    }
                    if (strSearch.ToUpper() == "P")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Types Of Projects";
                        }
                        else
                        {
                            SearchRoot += ", " + "Types Of Projects";
                        }
                    }
                    if (strSearch.ToUpper() == "F")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Certification";
                        }
                        else
                        {
                            SearchRoot += ", " + "Certification";
                        }
                    }
                    if (strSearch.ToUpper() == "N")
                    {
                        SearchRoot = "No Search Criteria Selected";
                    }
                    if (strSearch.ToUpper() == "A")
                    {
                        SearchRoot = "Vendor Name, Scopes of Work, Geographic Locations, Types Of Projects, Certification";
                    }
                }
                
                //005
                if (objdetails.Count > 0)
                {
                    if (objdetails[0].ProvidesBIM.Trim() == "Y")
                    {
                        if (string.IsNullOrEmpty(SearchRoot))
                        {
                            SearchRoot = "Provides BIM";
                        }
                        else
                        {
                            SearchRoot += ", " + "Provides BIM";
                        }
                    }
                }

                lblSearch.Visible = true;
                lblRoot.Visible = false;
                lblSearch.Text= SearchRoot;
            }
        }
    }

    /// <summary>
    /// Get confirmation for deleting the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSaveSearch_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdn = new HiddenField();
        hdn = (HiddenField)grdSaveSearch.Rows[e.RowIndex].FindControl("hdnHistoryId");
        Session["HistoryId"] = hdn.Value;
        LinkButton lbl = new LinkButton();
        lbl = (LinkButton)grdSaveSearch.Rows[e.RowIndex].FindControl("lblname");

        lblConfirm.Text= "Are you sure you want to delete the record?";
        modalConfirm.Show();
    }

    /// <summary>
    /// Delete teh record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgOK_Click(object sender, ImageClickEventArgs e)
    {
        objSearchVendor.DeleteSearchHistory(Convert.ToInt64(Session["HistoryId"]));
        modalConfirm.Hide();
        SaveBind();
    }

    /// <summary>
    /// Display the search criteria used
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSaveSearch_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        modalExtnd2.Show();
        tblTradeInfo.Width = Unit.Percentage(100);
        HiddenField hdn = new HiddenField();
        hdn = (HiddenField)grdSaveSearch.Rows[e.RowIndex].FindControl("hdnHistoryId");
        long Rowvalue = Convert.ToInt64(hdn.Value);
        ViewState["RowValue"] = Rowvalue;
        LinkButton lblSearch = (LinkButton)grdSaveSearch.Rows[e.RowIndex].FindControl("lblsearch");

        //003 DAL.UspGetSearchedDetailsResult[] objdetails = objSearchVendor.GetSearchDetails(Rowvalue);
        List<SearchHistory> objdetails = objSearchVendor.GetSearchDetails(Rowvalue);
        if (objdetails.Count > 0)   //003
        {
            lblVendorNumber.Text= objdetails[0].JDEVendorNo;
            ViewState["OtherState"] = objdetails[0].OtherState;

            lbloName.Text= objdetails[0].VendorId;

            lblTrackingNumber.Text= objdetails[0].TrackingNo;
            lblFederalTaxId.Text= objdetails[0].TaxId;
            lblStatus.Text= objdetails[0].Status;
            lblCertification.Text = objdetails[0].CompanyCertification;
            lblFederalSB.Text = objdetails[0].FederalSB;
            lblFirstName.Text = objdetails[0].FirstName;    //003
            lblLastName.Text = objdetails[0].LastName;      //003
            lblEmail.Text = objdetails[0].VendorEmail;      //003
            lblSavedISO.Text = (!string.IsNullOrEmpty(objdetails[0].ISO)) ? (" ISO " + objdetails[0].ISO) : string.Empty;    //004
            lblSavedQS.Text = (!string.IsNullOrEmpty(objdetails[0].QS)) ? "  QS " + objdetails[0].QS : string.Empty;      //004
            lblSavedOtherProjects.Text = (!string.IsNullOrEmpty(objdetails[0].OtherProject)) ? (" Other:&nbsp;&nbsp;&nbsp;" + objdetails[0].OtherProject) : string.Empty;      //004
            chlSearchBy.Items[5].Selected = (objdetails[0].ProvidesBIM.Trim().Equals("Y")) ? true : false ;     //005
            lblProvidesBIM.Text = objdetails[0].ProvidesBIM.Trim();     //005
            lblTypeOfCompanySaved.Text = this.SavedCompanyTypesToDisplay(objdetails[0].CompanyTypes.Replace("'", ""));   //010

        }
        else
        {
            lblVendorNumber.Text= string.Empty;
            lbloName.Text= string.Empty;
            lblTrackingNumber.Text= string.Empty;
            lblFederalTaxId.Text= string.Empty;
            lblStatus.Text= string.Empty;
            lblCertification.Text = string.Empty;
            lblFederalSB.Text = string.Empty;
            lblFirstName.Text = objdetails[0].FirstName;    //003
            lblLastName.Text = objdetails[0].LastName;      //003
            lblEmail.Text = objdetails[0].VendorEmail;      //003
        }

        //004
        VMSDAL.USPGetSearchedRecordResult[] objResult = 
            objSearchVendor.GetSearchHistory(Rowvalue).Where(c => c.ScopeCodeType.ToUpper() == ScopesCodeType.MasterFormat.ToString().ToUpper()).ToArray();

        int count = objResult.Count(m => m.ParentID == "-1");
        int flag = count - 1;
        int StaticFlag = 0;

        if (count > 0)
        {
            int count1 = count - (Convert.ToInt32(Math.Round(Convert.ToDouble(count / 2))));
            if (count1 >= 0 && count1 <= count)
            {
                if (count1 == 0)
                {
                    count1 = 1;
                }
                for (int i = 0; i < count1; i++)
                {
                    StaticFlag += 1;
                    flag += 1;
                    Table tbl = new Table();
                    TableRow tr = new TableRow();
                    TableCell tc = new TableCell();
                    Label lbl = new Label();
                    TreeView tv = new TreeView();
                    TreeNode tn = new TreeNode();
                    TreeNode tn1 = new TreeNode();
                    TreeNode tn2 = new TreeNode();
                    int leafCount = 0;
                    int flag1 = 0;
                    int? RootId = 0;
                    if (objResult[StaticFlag - 1].ParentID == "-1")
                    {
                        lbl.CssClass = "tree_head";
                        lbl.Text= objResult[StaticFlag - 1].Name;
                        RootId = objResult[StaticFlag - 1].RootNode;
                        leafCount = objSearchVendor.GetHistoryCount(Rowvalue, Convert.ToInt32(RootId));
                        tc.Controls.Add(lbl);

                    }

                    #region Get Children
                    //004
                    var children = objResult.Where(s => s.RootNode == Convert.ToInt32(RootId)).ToArray();
                    foreach (var ch in children)
                    {
                        tv.ShowExpandCollapse = false;
                        tv.CssClass = "Treestyle";
                        tv.NodeStyle.CssClass = "list_display";
                        tv.LeafNodeStyle.CssClass = "tree_display1";
                        tv.Attributes.Add("onClick", "return test();");

                        if (!string.IsNullOrEmpty(Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID)))
                        {
                            if (ch.ParentID == Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID))
                            {
                                tn = new TreeNode();
                                tn.Text = ch.Name;
                                tn.Value = Convert.ToString(ch.PK_ScopeID);
                                tv.Nodes.Add(tn);
                            }
                        }
                        if (!string.IsNullOrEmpty(tn.Value))
                        {
                            if (ch.ParentID == tn.Value)
                            {
                                tn1 = new TreeNode();
                                tn1.Text = ch.Name;
                                tn1.Value = Convert.ToString(ch.PK_ScopeID);
                                tn.ChildNodes.Add(tn1);

                            }
                        }

                    }


                    //for (int j = flag; j < ((flag) + leafCount - 1); j++)
                    //{
                    //    tv.ShowExpandCollapse = false;
                    //    tv.CssClass = "Treestyle";
                    //    tv.NodeStyle.CssClass = "list_display";
                    //    tv.LeafNodeStyle.CssClass = "tree_display1";
                    //    tv.Attributes.Add("onClick", "return test();");

                    //    if (!string.IsNullOrEmpty(Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID)))
                    //    {
                    //        //004
                    //        if (j <= objResult.Count()-1)
                    //        {
                    //            if (objResult[j].ParentID == Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID))
                    //            {
                    //                tn = new TreeNode();
                    //                tn.Text = objResult[j].Name;
                    //                tn.Value = Convert.ToString(objResult[j].PK_ScopeID);
                    //                tv.Nodes.Add(tn);
                    //            } 
                    //        }
                    //    }
                    //    if (!string.IsNullOrEmpty(tn.Value))
                    //    {
                    //        //004
                    //        if (j <= objResult.Count()-1)
                    //        {
                    //            if (objResult[j].ParentID == tn.Value)
                    //            {
                    //                tn1 = new TreeNode();
                    //                tn1.Text = objResult[j].Name;
                    //                tn1.Value = Convert.ToString(objResult[j].PK_ScopeID);
                    //                tn.ChildNodes.Add(tn1);
                    //            } 
                    //        }
                    //    }
                    //    flag1 += 1;
                    //} 
                    #endregion

                    flag = flag + (flag1 - 1);
                    tv.ExpandAll();
                    tc.Controls.Add(tv);
                    tr.Cells.Add(tc);
                    tbl.Rows.Add(tr);
                    tdCell1.Controls.Add(tbl);
                }

                for (int i = count1; i < count; i++)
                {
                    StaticFlag += 1;
                    flag += 1;
                    Table tbl = new Table();
                    TableRow tr = new TableRow();
                    TableCell tc = new TableCell();
                    Label lbl = new Label();
                    TreeView tv = new TreeView();
                    TreeNode tn = new TreeNode();
                    TreeNode tn1 = new TreeNode();
                    TreeNode tn2 = new TreeNode();
                    int leafCount = 0;
                    int flag1 = 0;
                    int? RootId = 0;
                    if (objResult[StaticFlag - 1].ParentID == "-1")
                    {
                        lbl.CssClass = "tree_head";
                        lbl.Text = objResult[StaticFlag - 1].Name;
                        RootId = objResult[StaticFlag - 1].RootNode;
                        leafCount = objSearchVendor.GetHistoryCount(Rowvalue, Convert.ToInt32(RootId));
                        tc.Controls.Add(lbl);
                        

                    }

                    //004
                    #region Get Children for next column
                    var children2 = objResult.Where(s => s.RootNode == Convert.ToInt32(RootId)).ToArray();
                    foreach (var ch in children2)
                    {
                        tv.ShowExpandCollapse = false;
                        tv.CssClass = "Treestyle";
                        tv.NodeStyle.CssClass = "list_display";
                        tv.LeafNodeStyle.CssClass = "tree_display1";
                        tv.Attributes.Add("onClick", "return test();");

                        if (!string.IsNullOrEmpty(Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID)))
                        {
                            if (ch.ParentID == Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID))
                            {
                                tn = new TreeNode();
                                tn.Text = ch.Name;
                                tn.Value = Convert.ToString(ch.PK_ScopeID);
                                tv.Nodes.Add(tn);
                            }
                        }
                        if (!string.IsNullOrEmpty(tn.Value))
                        {
                            if (ch.ParentID == tn.Value)
                            {
                                tn1 = new TreeNode();
                                tn1.Text = ch.Name;
                                tn1.Value = Convert.ToString(ch.PK_ScopeID);
                                tn.ChildNodes.Add(tn1);

                            }
                        }

                    }

                    //for (int j = flag; j < ((flag) + leafCount - 1); j++)
                    //{
                    //    tv.ShowExpandCollapse = false;
                    //    tv.CssClass = "Treestyle";
                    //    tv.NodeStyle.CssClass = "list_display";
                    //    tv.LeafNodeStyle.CssClass = "tree_display1";
                    //    tv.Attributes.Add("onClick", "return test();");

                    //    if (!string.IsNullOrEmpty(Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID)))
                    //    {
                    //        //004
                    //        if (j <= objResult.Count() - 1)
                    //        {
                    //            if (objResult[j].ParentID == Convert.ToString(objResult[StaticFlag - 1].PK_ScopeID))
                    //            {
                    //                tn = new TreeNode();
                    //                tn.Text = objResult[j].Name;
                    //                tn.Value = Convert.ToString(objResult[j].PK_ScopeID);
                    //                tv.Nodes.Add(tn);
                    //            }
                    //        }
                    //    }

                    //    if (!string.IsNullOrEmpty(tn.Value))
                    //    {
                    //        //004
                    //        if (j <= objResult.Count() - 1)
                    //        {
                    //            if (objResult[j].ParentID == tn.Value)
                    //            {
                    //                tn1 = new TreeNode();
                    //                tn1.Text = objResult[j].Name;
                    //                tn1.Value = Convert.ToString(objResult[j].PK_ScopeID);
                    //                tn.ChildNodes.Add(tn1);

                    //            }
                    //        }
                    //    }
                    //    flag1 += 1;
                    //} 
                    #endregion

                    flag = flag + (flag1 - 2);
                    tv.ExpandAll();

                    tc.Controls.Add(tv);
                    tr.Cells.Add(tc);
                    tbl.Rows.Add(tr);
                    tdCell2.Controls.Add(tbl);
                }
            }
        }

        //004 - PMMI
        VMSDAL.USPGetSearchedRecordResult[] objResult2 =
            objSearchVendor.GetSearchHistory(Rowvalue).Where(c => c.ScopeCodeType.ToUpper() == ScopesCodeType.PMMI.ToString().ToUpper()).ToArray();
        foreach (var pmmi in objResult2)
        {
            Table tbl = new Table();
            TableRow tr = new TableRow();
            TableCell tc = new TableCell();
            Label lbl = new Label();

            lbl.CssClass = "tree_head";
            lbl.Text = pmmi.Name;
            tc.Controls.Add(lbl);
            tr.Cells.Add(tc);
            tbl.Rows.Add(tr);
            tdCell2.Controls.Add(tbl);
        }

        //006 Sooraj- Design Scope 
        VMSDAL.USPGetSearchedRecordResult[] objDesignResult =
            objSearchVendor.GetSearchHistory(Rowvalue).Where(c => c.ScopeCodeType.ToUpper() == ScopesCodeType.DesignConsultant.ToString().ToUpper()).ToArray();
        foreach (var design in objDesignResult)
        {
            Table tbl = new Table();
            TableRow tr = new TableRow();
            TableCell tc = new TableCell();
            Label lbl = new Label();

            lbl.CssClass = "tree_head";
            lbl.Text = design.Name;
            tc.Controls.Add(lbl);
            tr.Cells.Add(tc);
            tbl.Rows.Add(tr);
            tdCell2.Controls.Add(tbl);
        }

        // Display trade continents
        dlContinents.DataSource = objSearchVendor.GetSearchedContinents(Rowvalue,false,string.Empty);
        dlContinents.DataBind();

        DAL.USPGetSearchedRegionsResult[] objRegions = objSearchVendor.GetSearchRegions(Rowvalue);

            dlRegions.Style["display"]= objRegions.Count() <= 0 ? "none":"";
      

        dlRegions.DataSource = objRegions.Where(m => m.ParentID == -1);
        //dlRegions.DataSource = objRegions;
        dlRegions.DataBind();
      
        dlProjects.DataSource = objSearchVendor.GetSearchProjectTypes(Rowvalue);
        dlProjects.DataBind();
      
        string[] SearchRoot = lblSearch.Text.Trim().Split(',');
        trTypesOfProjects.Style["display"] = "none";
        dlProjects.Style["display"] = "none";
        trGeographicalLocations.Style["display"] = "none";

        trscopes.Style["display"] = "none";
        tblTradeInfo.Style["display"] = "none";

        tblVendor.Style["display"] = "none";
        VendorNameHead.Style["display"] = "none";

        trCertification.Style["display"] = "none";
        tblCertification.Style["display"] = "none";
        for (int i = 0; i < SearchRoot.Count(); i++)
        {
            if (SearchRoot[i]== "All")
            {
                tblVendor.Style["display"] = "";
                VendorNameHead.Style["display"] = "";
                trscopes.Style["display"] = "";
                tblTradeInfo.Style["display"] = "";
                trGeographicalLocations.Style["display"] = "";
                trTypesOfProjects.Style["display"] = "";
                dlProjects.Style["display"] = "";
            }
            if (SearchRoot[i]== " Types Of Projects")
            {
                trTypesOfProjects.Style["display"] = "";
                dlProjects.Style["display"] = "";
            }
            if (SearchRoot[i] == "Types Of Projects")
            {
                trTypesOfProjects.Style["display"] = "";
                dlProjects.Style["display"] = "";
            }

            if (SearchRoot[i]== " Geographic Locations")
            {
                trGeographicalLocations.Style["display"] = "";
            }
            if (SearchRoot[i] == "Geographic Locations")
            {
                trGeographicalLocations.Style["display"] = "";
            }
            if (SearchRoot[i]== " Scopes of Work")
            {
                trscopes.Style["display"] = "";
                tblTradeInfo.Style["display"] = "";
            }
            if (SearchRoot[i] == "Scopes of Work")
            {
                trscopes.Style["display"] = "";
                tblTradeInfo.Style["display"] = "";
            }
            if (SearchRoot[i]== "Vendor Name")
            {
                tblVendor.Style["display"] = "";
                VendorNameHead.Style["display"] = "";
            }
            if (SearchRoot[i] == " Certification")
            {
                trCertification.Style["display"] = "";
                tblCertification.Style["display"] = "";
            }
            if (SearchRoot[i] == "Certification")
            {
                trCertification.Style["display"] = "";
                tblCertification.Style["display"] = "";
            }
        }
    }

    /// <summary>
    /// 010 - used to get the long description of the company type ofr display purposes
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    private string SavedCompanyTypesToDisplay(string p)
    {
        string ret = string.Empty;

        if(!string.IsNullOrEmpty(p))
        {
            string[] types = p.Split(',');

            for (int i = 0; i < types.Length; i++)
            {
                ret += new clsCompany().DecodeCompanytype(types[i]) + ",";
            }

            ret = ret.TrimEnd(',');

        }

        return ret;
    }
  
    /// <summary>
    /// Display the Search results found
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSaveSearch_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        hdnPage.Value = "1";
        tblVendorDetails.Style["display"] = "";
        LinkButton lnk = new LinkButton();
        lnk = (LinkButton)grdSaveSearch.Rows[e.NewSelectedIndex].FindControl("lblname");
        //003 - DAL.UspGetSearchByNameResult[] objResult = objSearchVendor.GetSearchByName(lnk.Text.Trim(), Convert.ToInt32(Session["EmployeeId"]));
        List<SearchHistory> objResult = objSearchVendor.GetSearchByName(lnk.Text.Trim(), Convert.ToInt32(Session["EmployeeId"]));

        //003 - if (objResult.Length > 0)
        if (objResult.Count > 0)
        {
            string SearchBy = "", Vendor = "", VendorNumber = "", TrackingNumber = "", FederalTaxId = "", VendorStatus = "",
                   Scopes = "", Regions = "", Continents = "", Countries = "", Projects = "", OtherState = "", Certifications = "", FederalSB = "",
                   FirstName = string.Empty, LastName = string.Empty, Email = string.Empty, OtherProject = string.Empty, ISO = string.Empty, QS = string.Empty, companyTypes = string.Empty;             //003, 004, 010
            int flag = 0;


            //010 - Start
            if (!string.IsNullOrEmpty(objResult[0].CompanyTypes))
            {
                flag++;
                companyTypes = objResult[0].CompanyTypes;
            }

            if (!String.IsNullOrEmpty(companyTypes))
            {
                string[] types = companyTypes.Replace("'", "").Split(',');

                foreach (DataListItem item in rptTypeOfCompany.Items)
                {
                    CheckBox chk = (item.FindControl("chkTypeOfCompany") as CheckBox);
                    chk.Checked = (types.Contains(chk.ToolTip));
                }
            }
            //010 - End

            //004
            if (!string.IsNullOrEmpty(objResult[0].ISO))
            {
                flag++;
                ISO = objResult[0].ISO;
            }
            //004
            if (!string.IsNullOrEmpty(objResult[0].QS))
            {
                flag++;
                QS = objResult[0].QS;
            }
            
            //004
            if (!string.IsNullOrEmpty(objResult[0].OtherProject))
            {
                flag++;
                OtherProject = objResult[0].OtherProject;
            }

            //003
            if (!string.IsNullOrEmpty(objResult[0].FirstName))
            {
                flag++;
                FirstName = objResult[0].FirstName;
            }
            //003
            if (!string.IsNullOrEmpty(objResult[0].LastName))
            {
                flag++;
                LastName = objResult[0].LastName;
            }
            //003
            if (!string.IsNullOrEmpty(objResult[0].VendorEmail))
            {
                flag++;
                Email = objResult[0].VendorEmail;
            }
            if (!string.IsNullOrEmpty(Convert.ToString(objResult[0].VendorId)))
            {
                flag++;
                Vendor = Convert.ToString(objResult[0].VendorId);
            }
            if (!string.IsNullOrEmpty(objResult[0].JDEVendorNo))
            {
                flag++;
                VendorNumber = Convert.ToString(objResult[0].JDEVendorNo);
            }
            if (!string.IsNullOrEmpty(objResult[0].TrackingNo))
            {
                flag++;
                TrackingNumber = Convert.ToString(objResult[0].TrackingNo);
            }
            if (!string.IsNullOrEmpty(objResult[0].TaxId))
            {
                flag++;
                FederalTaxId = Convert.ToString(objResult[0].TaxId);
            }
            string Status="";
            if (!string.IsNullOrEmpty(objResult[0].Status))
            {
                flag++;
                Status = Convert.ToString(objResult[0].Status);
            }

            switch (Status)
            {
                case "--Select--":
                    VendorStatus = "";
                    break;
                case "InProgress":
                    VendorStatus = "0";
                    break;
                case "Pending":
                    VendorStatus = "1";
                    break;
                case "Complete":
                    VendorStatus = "2";
                    break;
                case "Revision":
                    VendorStatus = "3";
                    break;
                // G. Vera 04/18/2012 Item 4.4 Phase 3
                case "Expired":
                    VendorStatus = "6";
                    break;
                //003 - include expired records
                //005 - handled by Database already
                default:
                    VendorStatus = "";
                    break;
            }

            if (SortExp == null)
            {
                SortExp = "";
            }
            if (flag > 0)
            {
                SearchBy = "V";
                flag = 1;
            }
            if (!string.IsNullOrEmpty(objResult[0].CSICodes))
            {
                //Scopes = Convert.ToString(objResult[0].CSICodes);
                //005 - need to remove parent scope for accurate search
                // MasterFormat only
                VMSDAL.USPGetSearchedRecordResult[] objResult2 =
                objSearchVendor.GetSearchHistory(objResult[0].Id).Where(c => c.ParentID != "-1").ToArray();      //005 need to remove parent scopes.

                if (objResult2.Count() > 0)
                {
                    foreach (var item in objResult2)
                    {
                        Scopes += item.PK_ScopeID + ",";
                    } 
                }
                else
                {
                    //PMMI
                    Scopes += objResult[0].CSICodes;
                }

                if (Scopes.Substring(Scopes.Length - 1) == ",")
                    Scopes = Scopes.Remove(Scopes.Length - 1);

                if (!string.IsNullOrEmpty(Scopes))
                {
                    SearchBy += "C";
                    flag++;
                }
            }
            if (!string.IsNullOrEmpty(objResult[0].RegionCodes))
            {
                Regions = Convert.ToString(objResult[0].RegionCodes);
                if (!string.IsNullOrEmpty(Regions))
                {
                    SearchBy += "R";
                    flag++;
                }
            }
            if (!string.IsNullOrEmpty(objResult[0].ContinentCodes)) //008
            {
                Continents = Convert.ToString(objResult[0].ContinentCodes);
                if (!string.IsNullOrEmpty(Continents))
                {
                    SearchBy += "R";
                    flag++;
                }
            }
            if (!string.IsNullOrEmpty(objResult[0].CountryCodes)) //008
            {
                Countries = Convert.ToString(objResult[0].CountryCodes);
                if (!string.IsNullOrEmpty(Countries))
                {
                    SearchBy += "R";
                    flag++;
                }
            }
            if (!string.IsNullOrEmpty(objResult[0].ProjectTypes))
            {
                Projects = Convert.ToString(objResult[0].ProjectTypes);

                //004
                if (!(string.IsNullOrEmpty(Convert.ToString(Session["Projects"]))) || !string.IsNullOrEmpty(Session["OtherProject"] as String))
                {
                    SearchBy += "P";
                    flag++;
                }
            }
            //004
            if ((!string.IsNullOrEmpty(objResult[0].CompanyCertification)) || (!string.IsNullOrEmpty(objResult[0].FederalSB)))
            {
                Certifications = Convert.ToString(objResult[0].CompanyCertification);
                FederalSB = Convert.ToString(objResult[0].FederalSB);
            }
            //004
            if ((!string.IsNullOrEmpty(Certifications)) || (!string.IsNullOrEmpty(FederalSB)) ||
                    !string.IsNullOrEmpty(ISO) || !string.IsNullOrEmpty(QS))
            {
                SearchBy += "F";
                flag++;
            }
                       
            int OtherStateId = 0;
            if (!string.IsNullOrEmpty(objResult[0].OtherState))
            {
                OtherStateId = 51;
                OtherState = Convert.ToString(objResult[0].OtherState);
            }

            if (flag == 5)
            {
                SearchBy = "A";
            }
            if (flag == 0)
            {
                SearchBy = "N";
            }

            //005
            chlSearchBy.Items[5].Selected = (objResult[0].ProvidesBIM.Trim() == "Y") ? true : false;

            //003 - changed
            //004
            //005
            //010
            lblTotal.Text = Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, VendorStatus, Scopes, Regions, Projects, SortExp, 
                                                                                  OtherState, OtherStateId, Certifications, FederalSB, FirstName, LastName, Email, (Session["OtherProject"] as String), ISO, QS, "", 
                                                                                  chlSearchBy.Items[5].Selected, Continents,Countries,companyTypes));//008
            // G. Vera 04/18/2012 Item 4.4 Phase 3
            //003 - commented out
            //if (Convert.ToString(Session["Access"]).Equals("1")) lblTotal.Text = Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, VendorStatus, Scopes, Regions, Projects, SortExp, OtherState, OtherStateId, Certifications, FederalSB));

            hdnCount.Value = Convert.ToString(lblTotal.Text.Trim());
            //005
            int page;
            if (Session["SelectedNumber"] != null) page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.Items[Convert.ToInt32(Session["SelectedNumber"])].Text))));
            else page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));

            hdnPage.Value = "1";
            Session["Page"] = Convert.ToString(page);
            ddlNumber.SelectedIndex = 0;
            // lblCurrentPage.Text= Convert.ToString(hdnPage.Value);
            lblTotalPage.Text= Convert.ToString(Session["Page"]);
            if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
            {
                imbNext.Visible = false;
            }
            else
            {
                imbNext.Visible = true;
            }
            if (hdnPage.Value == "1")
            {
                imbPrevious.Visible = false;
            }
            else
            {
                imbPrevious.Visible = true;
            }
            if (SortExp == null)
            {
                SortExp = "";
            }
            //003 - changed
            //004
            //005
            //010
            Session["DataSetCount"] = Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, VendorStatus, Scopes, Regions, Projects, SortExp, OtherState, 
                                                                                            OtherStateId, Certifications, FederalSB, FirstName, LastName, Email, (Session["OtherProject"] as String), ISO, QS, "", 
                                                                                            chlSearchBy.Items[5].Selected,Continents,Countries,companyTypes));
            // G. Vera 04/18/2012 Item 4.4 Phase 3
            //003 - commented out
            //if (Convert.ToString(Session["Access"]).Equals("1")) 
            //    Session["DataSetCount"] = Convert.ToString(objSearchVendor.GetDetailsCountAdmin(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, VendorStatus, Scopes, Regions, Projects, SortExp, OtherState, OtherStateId, Certifications, FederalSB));
            Session["VendorDetailsCount"] = Session["DataSetCount"];
            lblTotal.Text= Convert.ToString(Session["DataSetCount"]);
            trGrid.Style["Display"] = "";

            //003 - added new fields
            //005
            //010
            Session["Vendordetails"] = SearchBy + "~" + Vendor + "~" + VendorNumber + "~" + TrackingNumber + "~" + FederalTaxId + "~" + 
                                       VendorStatus + "~" + Scopes + "~" + Regions + "~" + Projects + "~" + hdnPage.Value + "~" + ddlNumber.Text.Trim() + "~" +
                                       SortExp + "~" + OtherState + "~" + OtherStateId + "~" + Certifications + "~" + FederalSB + "~" + FirstName + "~" +
                                       LastName + "~" + Email + "~" + OtherProject + "~" + ISO + "~" + QS + "~" + "" + "~" + chlSearchBy.Items[5].Selected + "~" + Continents + "~" + Countries +"~"+ companyTypes; //008

            BindVendor();

            SaveBind();
            plcCrntPage1.Controls.Clear();

            int i;
            if (Convert.ToInt32(Session["Page"]) >= 5)
            {
                for (i = 1; i <= 5; i++)
                {
                    LinkButton lnkPage = new LinkButton();
                    lnkPage.Click += new EventHandler(lnk1_Click);
                    lnkPage.Text= Convert.ToString(i);
                    if (i == 1)
                    {
                        lnkPage.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnkPage.CssClass = "lnkcolor";
                    }
                    plcCrntPage1.Controls.Add(lnkPage);
                    lnkPage.Text= Convert.ToString(i);
                    lnkPage.Attributes.Add("onClick", "GetValue(" + lnkPage.Text.Trim() + ",'" + hdnPage.ClientID + "')");
                }
            }
            else
            {
                for (i = 1; i <= Convert.ToInt32(Session["Page"]); i++)
                {
                    LinkButton lnkPage = new LinkButton();
                    lnkPage.Click += new EventHandler(lnk_Click);
                    lnkPage.Text= Convert.ToString(i);
                    if (i == 1)
                    {
                        lnkPage.CssClass = "Selectlnkcolor";
                    }
                    else
                    {
                        lnkPage.CssClass = "lnkcolor";
                    }
                    plcCrntPage1.Controls.Add(lnkPage);
                    lnkPage.Text= Convert.ToString(i);
                    lnkPage.Attributes.Add("onClick", "GetValue(" + lnkPage.Text.Trim() + ",'" + hdnPage.ClientID + "')");
                }
            }
            trGrid.Style["Display"] = "";
            if (grdVendor.Rows.Count > 0)
            {
                lblmsgInfo.Text= string.Empty;
                Search.Style["display"] = "";
                tdnextPrevious1.Style["display"] = "";
                tdbuttons.Style["display"] = "";
            }
            else
            {
                lblmsgInfo.Text= "Sorry no records found";
                Search.Style["display"] = "none";
                tdnextPrevious1.Style["display"] = "none";
                tdbuttons.Style["display"] = "none";
            }
        }
        else
        {
            grdVendor.DataSource = null;
            grdVendor.DataBind();
            lblmsg.Text= "Sorry no records found";

            Search.Style["display"] = "none";
            tdnextPrevious1.Style["display"] = "none";
            tdbuttons.Style["display"] = "none";
        }
    }

    /// <summary>
    /// Export to excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbexcel_Click(object sender, ImageClickEventArgs e)
    {
        string d_FilePath = Server.MapPath("SearchExport") + "\\test.xlsx";
        string VendorId = null;
        foreach (GridViewRow grd in grdVendor.Rows)
        {
            HiddenField hdn = (HiddenField)grd.FindControl("hdnVendorId");
            VendorId += hdn.Value + ",";
        }
        if (VendorId.Contains(','))
        {
            VendorId = VendorId.Remove(VendorId.LastIndexOf(','), 1);
        }

        // *****************************************************************************
        // ********** Modified By N Schoenberger 12/5/2011
	
        string SearchBy = string.Empty, Certification = string.Empty, FederalSB = string.Empty, ISO = string.Empty, QS = string.Empty;
        foreach (ListItem Item in rdoCompanyCertiY.Items)
        {
            if (Item.Selected)
            {
                if (Item.Text == "Other")
                {
                    Certification += string.IsNullOrEmpty(Certification) ? txtCertiOth.Text.Trim() : "," + txtCertiOth.Text.Trim();
                }
                else
                {
                    Certification += string.IsNullOrEmpty(Certification) ? Item.Text : "," + Item.Text;
                }
            }
        }
        foreach (ListItem Item in chklFederalSB.Items)
        {
            if (Item.Selected)
            {
                if (Item.Text == "Other")
                {
                    FederalSB += string.IsNullOrEmpty(FederalSB) ? txtOtherFederalSB.Text.Trim() : "," + txtOtherFederalSB.Text.Trim();
                }
                else
                {
                    FederalSB += string.IsNullOrEmpty(FederalSB) ? Item.Text : "," + Item.Text;
                }
            }
        }

        //004
        //ValidateIndustryCerts();

        foreach (DataListItem i in dlIndustryCerts.Items)
        {
            CheckBox chk = (i.FindControl("chkIndustryCert") as CheckBox);
            TextBox txt = (i.FindControl("txtIndustryCert") as TextBox);
            HiddenField hdn = (i.FindControl("hdnCertificationID") as HiddenField);

            if (chk.Checked)
            {
                if (!string.IsNullOrEmpty(txt.Text))
                {
                    if (chk.Text.ToUpper().Trim() == "ISO") ISO = txt.Text;
                    if (chk.Text.ToUpper().Trim() == "QS") QS = txt.Text;
                }
                else
                {
                    if (chk.Text.ToUpper().Trim() == "ISO") ISO = "ALL";
                    if (chk.Text.ToUpper().Trim() == "QS") QS = "ALL";

                }
            }
        }

        if (chlSearchBy.Items[0].Selected == true)
        {
            SearchBy += "V";
        }
        if (chlSearchBy.Items[1].Selected == true)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["Scopes"])))
            {
                SearchBy += "C";
            }
        }
        if (chlSearchBy.Items[2].Selected == true)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["Regions"]))) //008
            {
                SearchBy += "R";
            }
        }
        if (chlSearchBy.Items[3].Selected == true)
        {
            //004
            if (!(string.IsNullOrEmpty(Convert.ToString(Session["Projects"]))) || !string.IsNullOrEmpty(Session["OtherProject"] as String))
            {
                SearchBy += "P";
            }
        }
        if (chlSearchBy.Items[4].Selected == true)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Certification)) || !string.IsNullOrEmpty(Convert.ToString(FederalSB)) ||
                !string.IsNullOrEmpty(ISO) || !string.IsNullOrEmpty(QS))
            {
                SearchBy += "F";
            }
        }
        if ((chlSearchBy.Items[0].Selected == false) && (chlSearchBy.Items[1].Selected == false) && (chlSearchBy.Items[2].Selected == false) && (chlSearchBy.Items[3].Selected == false) && (chlSearchBy.Items[4].Selected == false))
        {
            SearchBy = "N";
        }
        if ((!(string.IsNullOrEmpty(Convert.ToString(Session["Scopes"])))) || (!(string.IsNullOrEmpty(Convert.ToString(Session["Regions"])))) || (!(string.IsNullOrEmpty(Convert.ToString(Session["Continents"])))) || (!(string.IsNullOrEmpty(Convert.ToString(Session["Projects"])))))
        {
            if ((chlSearchBy.Items[0].Selected == true) && (chlSearchBy.Items[1].Selected == true) && (chlSearchBy.Items[2].Selected == true) && (chlSearchBy.Items[3].Selected == true) && (chlSearchBy.Items[4].Selected == true))
            {
                SearchBy = "A";
            }
        }

        string Vendor = "", VendorNumber = "", TrackingNumber = "", FederalTaxId = "", Scopes = "", Regions = "", Projects = "", Continents="", //008
            OtherState = "", FirstName = string.Empty, LastName = string.Empty, Email = string.Empty, OtherProject = string.Empty;    //004

        //004
        if (!string.IsNullOrEmpty(txtFirstName.Text))
        {
            FirstName = txtFirstName.Text.Trim();
        }
        //004
        if (!string.IsNullOrEmpty(txtLastName.Text))
        {
            LastName = txtLastName.Text.Trim();
        }
        //004
        if (!string.IsNullOrEmpty(txtEmail.Text))
        {
            Email = txtEmail.Text.Trim();
        }

        if (!string.IsNullOrEmpty(txtVendor.Text.Trim()))
        {
            Vendor = objCommon.ReplaceStrRevDS(txtVendor.Text.Trim());
            Session["JdeVendorId"] = Vendor;
        }
        else
        {
            Session["JdeVendorId"] = "";
        }

        if (!string.IsNullOrEmpty(txtVendorNumber.Text.Trim()))
        {
            VendorNumber = txtVendorNumber.Text.Trim();
        }
        if (!string.IsNullOrEmpty(txtTrackingNumber.Text.Trim()))
        {
            TrackingNumber = txtTrackingNumber.Text.Trim();
        }
        if (!string.IsNullOrEmpty(txtFederalTax2.Text.Trim()))
        {
            FederalTaxId = txtFederalTax2.Text.Trim();
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["Scopes"])))
        {
            Scopes = Convert.ToString(Session["Scopes"]);
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["Regions"])))
        {
            Regions = Convert.ToString(Session["Regions"]);
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["Continents"])))
        {
            Continents = Convert.ToString(Session["Continents"]);
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["Projects"])))
        {
            Projects = Convert.ToString(Session["Projects"]);
        }
        int OtherStateId = 0;
        foreach (GridViewRow gr in grdState3.Rows)
        {
            CheckBox chk = (CheckBox)gr.FindControl("chkCheck");
            if (chk.Checked)
            {
                Label lbl = (Label)gr.FindControl("lblName");
                if (lbl.Text == "Other")
                {
                    OtherStateId = 51;
                    OtherState = txtRegions.Text.Trim();
                }
            }
        }
        string Status = ddlStatus.SelectedItem.Text.Trim();
        switch (Status)
        {
            case "--Select--":
                Status = "";
                break;
            case "InProgress":
                Status = "0";
                break;
            case "Pending":
                Status = "1";
                break;
            case "Complete":
                Status = "2";
                break;
            case "Revision":
                Status = "3";
                break;
            // G. Vera 04/18/2012 Item 4.4 Phase 3
            case "Expired":
                Status = "6";
                break;
            //003 - include expired records
            //005 - handled in database
            default:
                Status = "";
                break;
        }

        SortExp = hdnSort.Value;
        if (SortExp == null)
        {
            SortExp = "";
        }

        try
        {
            if (File.Exists(d_FilePath))
                File.Delete(d_FilePath);

        Microsoft.Office.Interop.Excel.Application xlsapp = null;
        Microsoft.Office.Interop.Excel._Workbook xlsbook = null;
        Microsoft.Office.Interop.Excel._Worksheet xlssheet = null;

        xlsapp = new Microsoft.Office.Interop.Excel.Application();
        xlsapp.Visible = false;

        //xlsapp.UserControl = true;
        //System.Globalization.CultureInfo oldCI = System.Threading.Thread.CurrentThread.CurrentCulture;
        //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        //System.Threading.Thread.CurrentThread.CurrentCulture = oldCI;

        xlsbook = (Microsoft.Office.Interop.Excel._Workbook)(xlsapp.Workbooks.Add(System.Reflection.Missing.Value));
        xlssheet = (Microsoft.Office.Interop.Excel._Worksheet)xlsbook.ActiveSheet;
        
        //004 - need to check first binded values since the controls are never handled when saved searches.
        string[] BindingValue = Convert.ToString(Session["Vendordetails"]).Split('~');
        DataTable objTable = new DataTable();
        if (BindingValue.Length < 15)
        {
            objTable = objSearchVendor.ExportToExcel(SearchBy, Vendor, VendorNumber, TrackingNumber, FederalTaxId, Status, Scopes, Regions,
                Projects, Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.Text.Trim()), SortExp, OtherState, OtherStateId, Certification, FederalSB
                , FirstName, LastName, Email, OtherProject, ISO, QS, chlSearchBy.Items[5].Selected);        //004, 005
        }
        else 
        {
            //005, 010
            objTable = objSearchVendor.ExportToExcel(BindingValue[0], BindingValue[1], BindingValue[2], BindingValue[3], BindingValue[4],
                                                                BindingValue[5], BindingValue[6], BindingValue[7], BindingValue[8],
                                                                Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.Text.Trim()), SortExp, BindingValue[12],
                                                                Convert.ToInt32(BindingValue[13]), BindingValue[14], BindingValue[15], BindingValue[16],
                                                                BindingValue[17], BindingValue[18], BindingValue[19], BindingValue[20], BindingValue[21],
                                                                Convert.ToBoolean(BindingValue[23]), BindingValue[24], BindingValue[25], BindingValue[26]); 
        }

        int cells = 0;
        for (int j = 0; j < objTable.Columns.Count; j++)
        {
            cells += 1;
            string str = objTable.Columns[j].ColumnName;

            xlssheet.Cells[1, cells] = str;
        }

        for (int i = 0; i < objTable.Rows.Count; i++) //Changed by N Schoenberger 10-03-2011
        {
            //G. Vera 05/21/2014 - changed to be generic instead of hard coding the columns
            for (int x = 0; x < objTable.Columns.Count; x++)
            {
                xlssheet.Cells[i + 2, x + 1] = Convert.ToString(objTable.Rows[i][x]);
            }

            //xlssheet.Cells[i + 2, 1] = Convert.ToString(objTable.Rows[i][0]);
            //xlssheet.Cells[i + 2, 2] = Convert.ToString(objTable.Rows[i][1]);
            //xlssheet.Cells[i + 2, 3] = Convert.ToString(objTable.Rows[i][2]);
            //xlssheet.Cells[i + 2, 4] = Convert.ToString(objTable.Rows[i][3]);
            //xlssheet.Cells[i + 2, 5] = Convert.ToString(objTable.Rows[i][4]);
        }

        //neaten the workbook up
        xlssheet.Columns.AutoFit();
        Microsoft.Office.Interop.Excel.Range xlRng = xlssheet.UsedRange.get_Range("A1", Type.Missing).EntireRow;
        xlRng.Font.Bold = true;

            try
            {
                xlsbook.SaveAs(d_FilePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, null, null, null, null, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlShared, null, null, null, null, null);
            }
            catch (Exception ge)
            {
                throw new Exception("Error saving the Excel file " + "\r\n" + ge.Message + "\r\n" + ge.StackTrace);
            }
            //xlsbook.SaveCopyAs(d_FilePath);

            //GV 09/08/2020 Not sure why this is being set to null....this is causing issue:  xlssheet = null;
            //If getting an COM error, please make sure that you create these 2 folders in the host machine:
            //C:\Windows\SysWOW64\config\systemprofile\Desktop
            //C:\Windows\System32\config\systemprofile\Desktop

        NAR(xlssheet);
        //xlsbook.Close(null, d_FilePath, null);
        NAR(xlsbook);
        xlsbook = null;
        xlsapp.Quit();
        NAR(xlsapp);
        xlsapp = null;

        // *****************************************************************************

        GC.Collect();

        //foreach (Process clsprocess in Process.GetProcesses())
        //{
        //    string str = clsprocess.ProcessName;
        //    if (clsprocess.ProcessName.StartsWith("EXCEL"))
        //    {
        //        clsprocess.Kill();
        //    }
        //}
        string PDFPath = Convert.ToString(d_FilePath);
        FileStream liveStream = new FileStream(PDFPath, FileMode.Open, FileAccess.Read);

        byte[] buffer = new byte[(int)liveStream.Length];
        liveStream.Read(buffer, 0, (int)liveStream.Length);
        liveStream.Close();

        Response.Clear();
        Response.ContentType = "application/octet-stream";
        Response.AddHeader("Content-Length", buffer.Length.ToString());
        Response.AddHeader("Content-Disposition", "attachment; filename=VendorSearch" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
        Response.BinaryWrite(buffer);
        Response.End();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    /// <summary>
    /// System method to export to excel
    /// </summary>
    /// <param name="o"></param>
    private void NAR(object o)
    {
        try
        {
        System.Runtime.InteropServices.Marshal.ReleaseComObject(o);
        }
        catch (Exception e)
        {
            //do nothing
        }
        finally
        {
            o = null;
        }
    }

    /// <summary>
    /// Redirect to the view page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        LinkButton lnk = new LinkButton();
        lnk = (LinkButton)grdVendor.Rows[e.NewSelectedIndex].FindControl("lnkVQF");
        HiddenField hdn = new HiddenField();
        hdn = (HiddenField)grdVendor.Rows[e.NewSelectedIndex].FindControl("hdnVendorId");
        Session["VendorAdminId"] = hdn.Value;
        Session["SelectedScopeTab"] = hdnScopeTypeOpen.Value;   //009

        if (Convert.ToString(Session["Access"]) == "1")
        {
            Response.Redirect("../VQFView/CompanyView.aspx?user=admin");
        }
        else
        {
            Response.Redirect("../VQFView/CompanyView.aspx?user=employee");
        }
    }

    /// <summary>
    /// Redirect to the SRF page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        LinkButton lnk = new LinkButton();
        lnk = (LinkButton)grdVendor.Rows[e.RowIndex].FindControl("lnkSRF");
        HiddenField hdn = new HiddenField();
        hdn = (HiddenField)grdVendor.Rows[e.RowIndex].FindControl("hdnVendorId");
        Session["SelectedScopeTab"] = hdnScopeTypeOpen.Value;   //009

        Response.Redirect("DisplayRating.aspx?VendorId=" + Convert.ToString(objCommon.encode(hdn.Value)) + "&FromPage=SearchVendor");

    }

    /// <summary>
    /// Back top seach
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgBackToSearch_Click(object sender, ImageClickEventArgs e)
    {
        Session["SelectedScopeTab"] = null;   //009
        //005
        this.Response.Redirect("~/Admin/SearchVendor.aspx");

        //tblView.Style["display"] = "none";
        //tblVendorDetails.Style["display"] = "none";
        //imgBackToSearch.Style["display"] = "none";
        //chlSearchBy.Items[0].Selected = false;
        //chlSearchBy.Items[1].Selected = false;
        //chlSearchBy.Items[2].Selected = false;
        //chlSearchBy.Items[3].Selected = false;


        //spnVendorDetails.Style["Display"] = "None";
        //spnCSICode.Attributes["style"] = "display: none";
        //spnLocation.Style["Display"] = "none";
        //SpnTOP.Style["Display"] = "none";

        //Session["Vendordetails"] = null;    
    }



    /// <summary>
    /// 008 Raise event to display countries based on the selected text
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlContinents_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DataTable objRegions;
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)    //005 add to check for both..hello?
        {
            //005 - modified the way they were hardcoding the check instead of just looking for the parent.
            Label lbl = (Label)e.Item.FindControl("lblContinent");
            Label ltr = (Label)e.Item.FindControl("lstCountries");

            if (!string.IsNullOrEmpty(lbl.Text))
            {
                objRegions = objSearchVendor.GetSearchedContinents(Convert.ToInt64(ViewState["RowValue"]), true,lbl.Text);
                foreach (DataRow drow in objRegions.Rows)
                {
                    ltr.Text += ", " + drow[0];
                }
                if (!string.IsNullOrEmpty(ltr.Text.Trim()))
                {
                    ltr.Text = ltr.Text.TrimStart(',');
                }
            }

        }
    }

    /// <summary>
    /// Raise event to display sub regions based on the selected text
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlRegions_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DAL.USPGetSearchedRegionsResult[] objRegions;
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)    //005 add to check for both..hello?
        {
            //005 - modified the way they were hardcoding the check instead of just looking for the parent.
            Label lbl = (Label)e.Item.FindControl("lblName");
            Label ltr = (Label)e.Item.FindControl("lstRegions");

            if (lbl.Text == "Other")
            {
                ltr = (Label)e.Item.FindControl("lstRegions");
                ltr.Text = Convert.ToString(ViewState["OtherState"]);
            }
            else
            {
                objRegions = objSearchVendor.GetSearchRegions(Convert.ToInt64(ViewState["RowValue"])).Where(m => m.ParentID == (e.Item.DataItem as DAL.USPGetSearchedRegionsResult).PK_StateID).ToArray();
                for (int i = 0; i < objRegions.Length; i++)
                {
                    ltr.Text += ", " + objRegions[i].StateCode;
                }
                if (!string.IsNullOrEmpty(ltr.Text.Trim()))
                {
                    ltr.Text = ltr.Text.Trim().Remove(0, 1);
                }
            }

            
            //if (lbl.Text== "CA")
            //{
            //    Label ltr = (Label)e.Item.FindControl("lstRegions");

            //    objRegions = objSearchVendor.GetSearchRegions(Convert.ToInt64(ViewState["RowValue"])).Where(m => m.ParentID == 5).ToArray();
            //    for (int i = 0; i < objRegions.Length; i++)
            //    {
            //        ltr.Text += ", " + objRegions[i].StateCode;
            //    }
            //    if (!string.IsNullOrEmpty(ltr.Text.Trim()))
            //    {
            //        ltr.Text= ltr.Text.Trim().Remove(0, 1);
            //    }
            //}
            //else if (lbl.Text== "FL")
            //{
            //    Label ltr = (Label)e.Item.FindControl("lstRegions");

            //    objRegions = objSearchVendor.GetSearchRegions(Convert.ToInt64(ViewState["RowValue"])).Where(m => m.ParentID == 9).ToArray();
            //    for (int i = 0; i < objRegions.Length; i++)
            //    {
            //        ltr.Text += ", " + objRegions[i].StateCode;
            //    }
            //    if (!string.IsNullOrEmpty(ltr.Text.Trim()))
            //    {
            //        ltr.Text= ltr.Text.Trim().Remove(0, 1);
            //    }
            //}
            //else if (lbl.Text== "TX")
            //{
            //    Label ltr = (Label)e.Item.FindControl("lstRegions");

            //    objRegions = objSearchVendor.GetSearchRegions(Convert.ToInt64(ViewState["RowValue"])).Where(m => m.ParentID == 43).ToArray();
            //    for (int i = 0; i < objRegions.Length; i++)
            //    {
            //        ltr.Text += ", " + objRegions[i].StateCode;
            //    }
            //    if (!string.IsNullOrEmpty(ltr.Text.Trim()))
            //    {
            //        ltr.Text= ltr.Text.Trim().Remove(0, 1);
            //    }
            //}
            
        }
    }

    /// <summary>
    /// Add Documents
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAdddoc_Click(object sender, ImageClickEventArgs e)
    {
        lblattach.Visible = false;  
        lblattach.Text = "";
        PaSREAttach.Visible = false;

        //AssignPath();
        //RetrievePath();
        try
        {
            if (Page.IsValid)
            {
                lblattach.Visible = true; 
                TxtFileName1.Focus();
                lblattach.Text = string.Empty;
                if ((TxtFileName1.Text == "") && (TxtFileName2.Text == "") && (TxtFileName3.Text == "") && (TxtFileName4.Text == "") && (TxtFileName5.Text == "") &&
                    !(Flu1.HasFile) && !(Flu2.HasFile) && !(Flu3.HasFile) && !(Flu4.HasFile) && !(Flu5.HasFile))
                {
                    lblattach.Text = "&nbsp;<li>Please upload at least one document </li>";
                    ModalAddDoc.Show();

                }
                else
                {
                    //file1
                    if (TxtFileName1.Text != "")
                    {
                        if (!(Flu1.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document one</li>";
                        }
                    }
                    else if (Flu1.HasFile)
                    {
                        if (TxtFileName1.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description one</li>";
                        }
                    }

                    //file2
                    if (TxtFileName2.Text != "")
                    {
                        if (!(Flu2.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document two</li>";
                        }
                    }
                    else if (Flu2.HasFile)
                    {
                        if (TxtFileName2.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description two</li>";
                        }
                    }

                    //file3
                    if (TxtFileName3.Text != "")
                    {
                        if (!(Flu3.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document three</li>";
                        }
                    }
                    else if (Flu3.HasFile)
                    {
                        if (TxtFileName3.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description three</li>";
                        }
                    }

                    //file4
                    if (TxtFileName4.Text != "")
                    {
                        if (!(Flu4.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document four</li>";
                        }
                    }
                    else if (Flu4.HasFile)
                    {
                        if (TxtFileName4.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description four</li>";
                        }
                    }

                    //file5
                    if (TxtFileName5.Text != "")
                    {
                        if (!(Flu5.HasFile))
                        {
                            lblattach.Text += "&nbsp;<li>Please upload the document five</li>";
                        }
                    }
                    else if (Flu5.HasFile)
                    {
                        if (TxtFileName5.Text == "")
                        {
                            lblattach.Text += "&nbsp;<li>Please enter the description five</li>";
                        }
                    }
                    int Insert = 0;
                    if ((Flu1.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu1.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu1.PostedFile.ContentLength - (MAXFILESIZE * 1024 * 1024) <= 0)       //011 from 2 to private static field
                            {
                                //strFileName1 = Convert.ToString(_vendorID) + Path.GetFileName(Flu1.PostedFile.FileName);
                                strFileName1 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu1.FileName);

                              
                                string filename = DOCUMENT_LOCATION + strFileName1;
                                Flu1.PostedFile.SaveAs(filename);
                                //string filepath = DOCUMENT_LOCATION + strFileName1;
				string filepath = strFileName1;
                                int Count = objRegistration.CheckDocument(_vendorID, TxtFileName1.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(_vendorID, TxtFileName1.Text, filepath,Convert.ToBoolean(hdnInAddDoc.Value), Convert.ToBoolean(hdnSRAddDoc.Value));
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description1 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "&nbsp;<li>File should be less than " + MAXFILESIZE + " MB " + "of size for description1</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "&nbsp;<li>Invalid file type for description1</li>";
                        }
                    }
                    if ((Flu2.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu2.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu2.PostedFile.ContentLength - (MAXFILESIZE * 1024 * 1024) <= 0) //011 from 2 to private static field
                            {
                                //strFileName2 = _vendorID + Path.GetFileName(Flu2.PostedFile.FileName);
                                strFileName2 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu2.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName2;
                                Flu2.PostedFile.SaveAs(filename);
                               // string filepath = DOCUMENT_LOCATION + strFileName2;
				string filepath = strFileName2;
                                int Count = objRegistration.CheckDocument(_vendorID, TxtFileName2.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(_vendorID, TxtFileName2.Text, filepath, Convert.ToBoolean(hdnInAddDoc.Value), Convert.ToBoolean(hdnSRAddDoc.Value));
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description2 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than " + MAXFILESIZE + " MB " + " of size for description2</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description2</li>";
                        }
                    }
                    if ((Flu3.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu3.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu3.PostedFile.ContentLength - (MAXFILESIZE * 1024 * 1024) <= 0)   //011 from 2 to private static field
                            {
                                //strFileName3 = _vendorID + Path.GetFileName(Flu3.PostedFile.FileName);
                                strFileName3 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu3.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName3;
                                Flu3.PostedFile.SaveAs(filename);
                                //string filepath = DOCUMENT_LOCATION + strFileName3;
				string filepath = strFileName3;
                                int Count = objRegistration.CheckDocument(Convert.ToInt64(Session["VendorId"]), TxtFileName3.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(_vendorID, TxtFileName3.Text, filepath, Convert.ToBoolean(hdnInAddDoc.Value), Convert.ToBoolean(hdnSRAddDoc.Value));
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description3 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than " + MAXFILESIZE + " MB " + " of size for description3</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type  for description3</li>";
                        }
                    }
                    if ((Flu4.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu4.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu4.PostedFile.ContentLength - (MAXFILESIZE * 1024 * 1024) <= 0)   //011 from 2 to private static field
                            {
                                //strFileName4 = _vendorID + Path.GetFileName(Flu4.PostedFile.FileName);
                                strFileName4 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu4.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName4;
                                Flu4.PostedFile.SaveAs(filename);
                                //string filepath = DOCUMENT_LOCATION + strFileName4;
				string filepath = strFileName4;

                                int Count = objRegistration.CheckDocument(_vendorID, TxtFileName4.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(_vendorID, TxtFileName4.Text, filepath, Convert.ToBoolean(hdnInAddDoc.Value), Convert.ToBoolean(hdnSRAddDoc.Value));
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "<li>Description4 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than " + MAXFILESIZE + " MB " + " of size for description4</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description4</li>";
                        }
                    }
                    if ((Flu5.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = Flu5.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            if (Flu5.PostedFile.ContentLength - (MAXFILESIZE * 1024 * 1024) <= 0)   //011 from 2 to private static field
                            {
                                //strFileName5 = _vendorID+ Path.GetFileName(Flu5.PostedFile.FileName);
                                strFileName5 = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString().Replace(" ", "").Replace("/", "").Replace(":", "").Replace("AM", "").Replace("PM", "") + System.IO.Path.GetExtension(Flu5.FileName);

                                string filename = DOCUMENT_LOCATION + strFileName5;
                                Flu5.PostedFile.SaveAs(filename);
                               // string filepath = DOCUMENT_LOCATION + strFileName5;
				string filepath = strFileName5;
                                int Count = objRegistration.CheckDocument(_vendorID, TxtFileName5.Text);
                                if (Count == 0)
                                {
                                    InsertDocument(_vendorID, TxtFileName5.Text, filepath, Convert.ToBoolean(hdnInAddDoc.Value), Convert.ToBoolean(hdnSRAddDoc.Value));
                                    Insert = 1;
                                }
                                else
                                {
                                    lblattach.Text += "</li>Description5 already exists</li>";
                                    Insert = 2;
                                }
                            }
                            else
                            {
                                lblattach.Text += "<li>File should be less than " + MAXFILESIZE + " MB " + " of size for description5</li>";
                                Insert = 2;
                            }
                        }
                        else
                        {
                            lblattach.Text += "<li>Invalid file type for description5</li>";
                        }
                    }
                    if (lblattach.Text != "")
                    {
                        ModalAddDoc.Show();
                    }
                    else
                    {
                        if (Insert == 1)
                        {
                            TxtFileName1.Text = "";
                            TxtFileName2.Text = "";
                            TxtFileName3.Text = "";
                            TxtFileName4.Text = "";
                            TxtFileName5.Text = "";
                            LoadAttchDocumentData(_vendorID);
                            if (hdnSRAddDoc.Value == "true")
                            {
                                SRLoadAttchDocumentData(_vendorID);
                            }
                            if (Attch1.Tables[0].Rows.Count > 0)
                            {
                                PanAttach.Visible = true;
                                tdattach.Style["Display"] = "";
                                Attch1.Dispose(); 
                            
                            }

                            if (hdnSRAddDoc.Value == "true")
                            {
                                tdSRattach.Style["Display"] = "";

                                tblVendors.Style["Display"] = "none";
                                tblAdmin.Style["Display"] = "none";
                                tdattach.Style["Display"] = "none";
                                PaSREAttach.Visible = true;

                            }
                            
                            lblattach.Text = "&nbsp;<li>Document uploaded successfully</li>";
                            ModalAddDoc.Show();
                        }
                        else if (Insert == 0)
                        {
                           
                            lblattach.Text = "&nbsp;<li>Error Occured in uploading the document </li>";
                            ModalAddDoc.Show();
                        }
                    }
                }
            }
            else
            {
                ModalAddDoc.Show();
            }
        }
        catch (System.UnauthorizedAccessException ex)
        {
            lblattach.Text = "<li>Access denied to the document attached</li>";
            ModalAddDoc.Show();
        }
        finally
        {
            TxtFileName1.Focus();
        }
    }

    /// <summary>
    /// Insert Documents
    /// </summary>
    /// <param name="FileName"></param>
    /// <param name="FilePath"></param>
    /// <param name="PageName"></param>
    private void InsertDocument(int VendorID, string FileName, string FilePath, bool IsAdmin, bool IsSRAdmin)
    {
        objRegistration.InsertInsuranceDocument(VendorID, FileName, FilePath, IsAdmin, IsSRAdmin);
    }
    
    /// <summary>
    /// Load attached document
    /// </summary>
    private void LoadAttchDocumentData(int VendorId)
    {
      
      
        //VendorId = 1;
        PanAttach.Visible = true;
        PaSREAttach.Visible = false; 
        TxtFileName1.Text = "";
        TxtFileName2.Text = "";
        TxtFileName3.Text = "";
        TxtFileName4.Text = "";
        TxtFileName5.Text = "";
        trAttachDocument.Style["Display"] = "";
        trAttachHeader.Style["Display"] = "";
        DataListAttchView.Visible = false;
        Attch = objCommon.GetAttchDocumentData(VendorId, false,false);
        if (Attch.Tables[0].Rows.Count > 0)
        {
            DataListAttchView.Visible = true;
            PanAttach.Visible = true;
            DataListAttchView.DataSource = Attch;
            DataListAttchView.DataBind();

            tblVendors.Style["Display"] = "";
            tdattach.Style["Display"] = "";
         
            if (Attch.Tables[0].Rows.Count > 3)
            {
                PanAttach.Style["overflow"] = "scroll";
                PanAttach.Style["Height"] = "200px";
                PanAttach.Style["width"] = "425px"; 
 
            }
        }
        else
        {
            tdattach.Style["Display"] = "none";
            tblVendors.Style["Display"] = "None";
            DataListAttchView.Visible = false;
            divAddDoc.Style["Height"] = "";
            //PanAttach.Style["overflow"] = "scroll";
            PanAttach.Style["Height"] = "";
            PanAttach.Style["width"] = "";
            PanAttach.Visible = false;
        }
        dlAdminUpload.Visible = false;
        Attch1 = objCommon.GetAttchDocumentData(VendorId, true, false);
        if (Attch1.Tables[0].Rows.Count > 0)
        {
            dlAdminUpload.Visible = true;
            PanAttach.Visible = true;
            tdattach.Style["Display"] = "";

            PanAttach.Visible = true;
            tblAdmin.Style["Display"] = "";
            dlAdminUpload.DataSource = Attch1;
            dlAdminUpload.DataBind();
       
            if (Attch1.Tables[0].Rows.Count > 3)
            {
                PanAttach.Style["overflow"] = "scroll";
                PanAttach.Style["Height"] = "200px";
                PanAttach.Style["width"] = "425px";  
            }
        }
        else
        {
            tblAdmin.Style["Display"] = "None";
            tdattach.Style["Display"] = "none";
            dlAdminUpload.Visible = false;
            PanAttach.Style["overflow"] = "";
            PanAttach.Style["Height"] = "";
            PanAttach.Style["width"] = "";
            //PanAttach.Visible = false;

        }
        



        if (Attch.Tables[0].Rows.Count > 0)
        {
            tdattach.Style["Display"] = "";
        }
       
    }


    //012 - Created
    private string LoadAttachmentToolTip(string vendorID)
    {
        string toolTipValue = string.Empty;

        Attch = objCommon.GetAttchDocumentData(vendorID.ConvertToInteger(), false, false);
        Attch1 = objCommon.GetAttchDocumentData(vendorID.ConvertToInteger(), true, false);
        Attch.Tables[0].Rows.Cast<DataRow>().ToList().ForEach(r => toolTipValue += r["FileName"].ConvertToString() + "<br/>");
        Attch1.Tables[0].Rows.Cast<DataRow>().ToList().ForEach(r => toolTipValue += r["FileName"].ConvertToString() + "<br/>");
        if (toolTipValue.Length > 0)
        {
            toolTipValue = toolTipValue.Substring(0, toolTipValue.LastIndexOf('<'));
        }
        

        return toolTipValue;
    }

    //012 - Created
    private string LoadVARToolTip(DataSet varAttachments)
    {
        string toolTipValue = string.Empty;

        varAttachments.Tables[0].Rows.Cast<DataRow>().ToList().ForEach(r => toolTipValue += r["FileName"].ConvertToString() + "<br/>");
        toolTipValue = toolTipValue.Substring(0, toolTipValue.LastIndexOf('<'));

        return toolTipValue;
    }

    private void SRLoadAttchDocumentData(int VendorId)
    {

        PaSREAttach.Visible = true;
        PanAttach.Visible = false;
        TxtFileName1.Text = "";
        TxtFileName2.Text = "";
        TxtFileName3.Text = "";
        TxtFileName4.Text = "";
        TxtFileName5.Text = "";
        trAttachDocument.Style["Display"] = "";
        trAttachHeader.Style["Display"] = "";
        SRAttch = objCommon.GetAttchDocumentData(VendorId, false, true);
        if (SRAttch.Tables[0].Rows.Count > 0)
        {
            lblSRmessage.Text = ""; 
            lblattach.Text = "";
            tdattach.Style["Display"] = "";
            tdSRattach.Style["Display"] = "";
            imgClose.Visible = false;
            DataListSRAttchView.Visible = true; 
            DataListSRAttchView.DataSource = SRAttch;
            DataListSRAttchView.DataBind();
            if (SRAttch.Tables[0].Rows.Count > 3)
            {
                PaSREAttach.Style["overflow"] = "scroll";
                PaSREAttach.Style["Height"] = "225px";
                PaSREAttach.Style["width"] = "425px";
            }
            else
            {
                PaSREAttach.Style["overflow"] = "";
                PaSREAttach.Style["Height"] = "";
                PaSREAttach.Style["width"] = "";
               
            }

        }
        else
        {
            lblSRmessage.Text = "No files are uploaded by admin"; 
            tdattach.Style["Display"] = "None";
            tdSRattach.Style["Display"] = "None";
            trAttachHeader.Style["Display"] = "none";
            imgClose.Visible = true;
            DataListSRAttchView.Visible = false; 

        }
         }
    protected void grdVendor_RowEditing(object sender, GridViewEditEventArgs e)
    {
        HiddenField hdnVendorID = (HiddenField)grdVendor.Rows[e.NewEditIndex].FindControl("hdnVendorId");
        ViewState["VendorID"] = Convert.ToInt32(hdnVendorID.Value);
        lblattach.Text = "";
        hdnInAddDoc.Value = "true";
        hdnSRAddDoc.Value = "false";
        LoadAttchDocumentData(_vendorID);
        tdSRattach.Style["Display"] = "None";
        ModalAddDoc.Show();
       
    }

    private int _vendorID
    {
        get { return Convert.ToInt32(ViewState["VendorID"]); }
    }

    /// <summary>
    /// Delete Attached documents
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void DataListAttchView_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        Int64 id = (Int64)DataListAttchView.DataKeys[e.Item.ItemIndex];
        objCommon.DeleteAttchDoc(id);
        
        LoadAttchDocumentData(_vendorID);
        lblattach.Text = "&nbsp;<li>File deleted successfully</li>";
        ModalAddDoc.Show();

    }

    /// <summary>
    /// Delete SRE Attached documents
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void DataListSRAttchView_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        Int64 id = (Int64)DataListSRAttchView.DataKeys[e.Item.ItemIndex];
        objCommon.DeleteAttchDoc(id);

        SRLoadAttchDocumentData(_vendorID);
        lblattach.Text = "&nbsp;<li>File deleted successfully</li>";
        trAttachHeader.Style["Display"] = "";
        ModalAddDoc.Show();

    }

    /// <summary>
    /// Delete Attached documents
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    /// 

    protected void dlAdminUpload_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        Int64 id = (Int64)dlAdminUpload.DataKeys[e.Item.ItemIndex];
        objCommon.DeleteAttchDoc(id);

        LoadAttchDocumentData(_vendorID);
        lblattach.Text = "&nbsp;<li>File deleted successfully</li>";
        ModalAddDoc.Show();

    }

     protected void DataListSRAttchView_SelectedIndexChanged(object sender, EventArgs e)
    {
        string FileName =((HiddenField)DataListSRAttchView.SelectedItem.FindControl("hdnFileName")).Value;
        Response.Redirect("../Common/GenerateFile.aspx?region=" + DOCUMENT_LOCATION + FileName);
        ModalAddDoc.Show();
    }

    protected void DataListAttchView_SelectedIndexChanged(object sender, EventArgs e)
    {
        string FileName = ((HiddenField)DataListAttchView.SelectedItem.FindControl("hdnFileName")).Value;
        Response.Redirect("../Common/GenerateFile.aspx?region=" + DOCUMENT_LOCATION + FileName);
        ModalAddDoc.Show();
    }

    protected void dlAdminUpload_SelectedIndexChanged(object sender, EventArgs e)
    {
        string FileName = ((HiddenField)dlAdminUpload.SelectedItem.FindControl("hdnFileName")).Value;
        Response.Redirect("../Common/GenerateFile.aspx?region=" + DOCUMENT_LOCATION + FileName);
        ModalAddDoc.Show();
    }


    protected void DataListAttchView_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (((HiddenField)e.Item.FindControl("hdnField")).Value == "True")
        {

            ((HtmlTableCell)e.Item.FindControl("tdRow")).Attributes.Add("Class", "greentextleft");
            ((LinkButton)e.Item.FindControl("FileName")).CssClass = "greentextleft";
        }
        else
        {
            ((LinkButton)e.Item.FindControl("FileName")).CssClass = "bodytextleft";
            ((HtmlTableCell)e.Item.FindControl("tdRow")).Attributes.Add("Class", "bodytextleft");
        }
    }

    //Validate companycertification
    protected void custOtherCertify_ServerValidate(object source, ServerValidateEventArgs args)
    {
        foreach (ListItem Item in rdoCompanyCertiY.Items)
        {
            if (Item.Selected)
            {
                if (Item.Text == "Other")
                {
                    if (string.IsNullOrEmpty(txtCertiOth.Text.Trim()))
                    {
                        custOtherCertify.ErrorMessage = "Please enter other company certifications";
                        args.IsValid = false;
                    }
                    else
                    {
                        args.IsValid = true;
                    }
                }
                else
                {
                    args.IsValid = true;
                }
            }
        }
    }

    //Validate FederalSb
    protected void custOtherFederalSB_ServerValidate(object source, ServerValidateEventArgs args)
    {
        foreach (ListItem Item in chklFederalSB.Items)
        {
            if (Item.Selected)
            {
                if (Item.Text == "Other")
                {
                    if (string.IsNullOrEmpty(txtOtherFederalSB.Text.Trim()))
                    {
                        cusvFederalSB.ErrorMessage = "Please enter other federal SB";
                        args.IsValid = false;
                    }
                    else
                    {
                        args.IsValid = true;
                    }
                }
                else
                {
                    args.IsValid = true;

                }
            }
        }
    }

    private void CallScripts()
    {
        imbAddDoc.Attributes.Add("onClick", " return CheckDocument();");

        //008-Sooraj
        //chkAustralia.Attributes.Add("onclick", "SwitchView('" + chkAustralia.ClientID + "','" + divAustralia.ClientID + "');");
        //chkAsia.Attributes.Add("onclick", "SwitchView('" + chkAsia.ClientID + "','" + divAsia.ClientID + "');");
        //chkNorthAmerica.Attributes.Add("onclick", "SwitchView('" + chkNorthAmerica.ClientID + "','" + divNorthAmerica.ClientID + "');");
        //chkSouthAmerica.Attributes.Add("onclick", "SwitchView('" + chkSouthAmerica.ClientID + "','" + divSouthAmerica.ClientID + "');");
        //chkAfrica.Attributes.Add("onclick", "SwitchView('" + chkAfrica.ClientID + "','" + divAfrica.ClientID + "');");
        //chkEurope.Attributes.Add("onclick", "SwitchView('" + chkEurope.ClientID + "','" + divEurope.ClientID + "');");
       
    }
    protected void grdVendor_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "SR")
        {
            hdnSRAddDoc.Value = "true";
            hdnInAddDoc.Value = "false"; 
            lblattach.Text = "";

            ViewState["VendorID"] = e.CommandArgument;

            tdSRattach.Style["Display"] = "";

            SRLoadAttchDocumentData(Convert.ToInt32(e.CommandArgument));
            tblVendors.Style["Display"] = "none";
            tblAdmin.Style["Display"] = "none";
            if (SRAttch.Tables[0].Rows.Count == 0)
            {
                //PanAttach.Visible = true;  
                trAttachHeader.Style["Display"] = "";
            }
            if (Convert.ToString(Session["Access"]).Equals("2"))
            {
                trAttachHeader.Style["Display"] = "none";
                trAttachDocument.Style["Display"] = "none";
                tdSRattach.Style["Display"] = "";
                imgClose.Visible = true; 

            }
           
            
            ModalAddDoc.Show();

        }
        else
        { hdnSRAddDoc.Value = "false"; }
       
    }
    protected void DataListSRAttchView_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        ImageButton imbdel = (ImageButton)e.Item.FindControl("Delete");

        if (imbdel!=null)
        {

            if (Session["Access"].ToString() != "1")
            {
                imbdel.Visible = false;
            }
            else
            {
                imbdel.Visible = true;
            }
        }

    }
    protected void dlAdminUpload_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        ImageButton imbdel = (ImageButton)e.Item.FindControl("Delete");

        if (imbdel != null)
        {

            if (Session["Access"].ToString() != "1")
            {
                imbdel.Visible = false;
            }
            else
            {
                imbdel.Visible = true;
            }
        }
    }
    protected void DataListAttchView_ItemDataBound1(object sender, DataListItemEventArgs e)
    {
        ImageButton imbdel = (ImageButton)e.Item.FindControl("Delete");

        if (imbdel != null)
        {

            if (Session["Access"].ToString() != "1")
            {
                imbdel.Visible = false;
            }
            else
            {
                imbdel.Visible = true;
            }
        }
    }

    /// <summary>
    /// 004 - implemented
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlPMMICodes1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        //005
        CheckBox chk = (e.Item.FindControl("chkPMMICode") as CheckBox);
        HiddenField hdnId = (e.Item.FindControl("hdnPMMIScopeId") as HiddenField);
        string[] searchOptions = null;

        if (Session["Vendordetails"] != null)
        {
            searchOptions = Convert.ToString(Session["Vendordetails"]).Split('~');
        }
        if (searchOptions != null)
        {
            string[] selectedScopes = searchOptions[6].Split(',');

            if (selectedScopes.Contains(hdnId.Value))
            {
                chk.Checked = true;
            }
        } 
    }

    /// <summary>
    /// 006 Sooraj- implemented
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlistDesign_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        CheckBox chk = (e.Item.FindControl("chkDesignCode") as CheckBox);
        HiddenField hdnId = (e.Item.FindControl("hdnDesignScopeId") as HiddenField);
        string[] searchOptions = null;

        if (Session["Vendordetails"] != null)
        {
            searchOptions = Convert.ToString(Session["Vendordetails"]).Split('~');
        }
        if (searchOptions != null)
        {
            string[] selectedScopes = searchOptions[6].Split(',');

            if (selectedScopes.Contains(hdnId.Value))
            {
                chk.Checked = true;
            }
        }
    }


    //012
    protected void rtgStar_Changed(object sender, AjaxControlToolkit.RatingEventArgs e)
    {
        var vendorID = e.Tag;
        Session["SelectedScopeTab"] = hdnScopeTypeOpen.Value;  //009

        Response.Redirect("DisplayRating.aspx?VendorId=" + Convert.ToString(objCommon.encode(vendorID)) + "&FromPage=SearchVendor");

    }
}