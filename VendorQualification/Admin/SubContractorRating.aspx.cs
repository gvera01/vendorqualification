﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 09/05/2012: VMS Stabilization Release 1.2 (JIRA:VPI-47)
 *
 ****************************************************************************************************************/
#endregion

public partial class SubContractorRating : System.Web.UI.Page
{
  
    //Instantiate the classes
    clsRating objRating = new clsRating();
    clsSearchVendor objSearchVendor = new clsSearchVendor();
    public string SortExp;
    public int Flag = 0;
    Common objCommon = new Common();
    RiskEvaluation riskEvaluation = new RiskEvaluation();
    /// <summary>
    /// OnInit
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        string strPageRequest = Request.Form["__EVENTTARGET"];
        CreatePaging(true, strPageRequest);
    }

    /// <summary>
    /// page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }

        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");


            //check for the session variable, if null redirect to the login page
            if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
            {
                    Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
            }
            else
            {
                if (Session["RISKIDLOGOUT"] != null && Session["LOCKEDBYLOGOUT"] != null)
                {
                    Int64 Lockedby = riskEvaluation.LockebByUser(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                    if (Lockedby == Convert.ToInt64(Session["LOCKEDBYLOGOUT"]))
                    {
                        riskEvaluation.UpdateStatus(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                        Session["RISKIDLOGOUT"] = null;
                    }
                }
                hdnPage.Value = "1";
                Session["Page"] = null;
                Session["DataSet"] = null;
                Session["DataSetCount"] = null;

                if (Convert.ToString(Session["Access"]).Equals("2"))
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                {
                    AdminMenu.DisplayMenu("Admin"); 
                }
                tdButtons.Style["display"] = "";
              
                if (Request.QueryString.Count > 0)
                {
                    plcCrntPage.Controls.Clear();
                    tdButtons.Style["display"] = "None";
                    Vendortd.Style["display"] = "";
                    tblContent.Style["display"] = "";
                    txtVendor.Text  = Convert.ToString(Session["txtVendor"]);
                    txtVendorNumber.Text  = Convert.ToString(Session["txtVendorNumber"]);
                    hdnPage.Value = "1";
                    hdnClick.Value =Convert.ToString(Session["SubContractorPage"]);
                    Session["SubContractorPage"] = null;
                    lblmsg.Text  = string.Empty;
                    ddlNumber.SelectedIndex = 0;
                    imbSearch_Click(null, null);
                }
                else
                {
                    txtVendor.Text  = string.Empty;
                    txtVendorNumber.Text  = string.Empty;
                }
            }
        }
        string strPageRequest = Request.Form["__EVENTTARGET"];
        CreatePaging(true, strPageRequest);
        Flag = 0;
    }

    /// <summary>
    /// Get sorting direction
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
            {
                ViewState["sortDirection"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["sortDirection"];
        }
        set
        {
            ViewState["sortDirection"] = value;
        }
    }

    /// <summary>
    /// Sorting the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;
        if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortDirection"])))
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
        else if (Convert.ToString(ViewState["SortDirection"]) == Convert.ToString(SortDirection.Ascending))
        {
            GridViewSortDirection = SortDirection.Descending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Descending);
            SortGridView(sortExpression, "DESC");
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
    }

    /// <summary>
    /// Method to sort gridview
    /// </summary>
    /// <param name="sortExpression"></param>
    /// <param name="direction"></param>
    private void SortGridView(string sortExpression, string direction)
    {
        SortExp = sortExpression + " " + direction;
        hdnSort.Value = SortExp;   
        Bind();
    }

    /// <summary>
    /// Method to create the controls dynamically
    /// </summary>
    /// <param name="FromLoad"></param>
    /// <param name="strPageRequest"></param>
    private void CreatePaging(bool FromLoad, string strPageRequest)
    {
        if (FromLoad)
        {
            if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page"]))) && (string.IsNullOrEmpty(strPageRequest) || strPageRequest.ToUpperInvariant() != "DDLNUMBER") && Flag !=1 )
            {
                for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
                {
                    if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                    {
                        LinkButton lnkPage = new LinkButton();
                        lnkPage.Click += new EventHandler(lnkPage_Click);
                        lnkPage.Text  = Convert.ToString(i);
                        if (i == Convert.ToInt32(hdnPage.Value))
                        {
                            lnkPage.CssClass = "Selectlnkcolor";
                        }
                        else 
                        { 
                            lnkPage.CssClass = "lnkcolor"; 
                        }
                        plcCrntPage.Controls.Add(lnkPage);
                        lnkPage.Text  = Convert.ToString(i);
                        lnkPage.Attributes.Add("onClick", "GetValue(" + lnkPage.Text.Trim() + ")");
                    }
                }
            }
        }
        else
        {
            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {
                    LinkButton lnk = new LinkButton();
                    Literal ltr = new Literal();
                    lnk.Click += new EventHandler(lnkPage_Click);
                    ltr.Text  = ",";
                    lnk.Text  = Convert.ToString(i);
                    
                    if (i == Convert.ToInt32(hdnPage.Value))
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else 
                    {
                        lnk.CssClass = "lnkcolor"; 
                    }
                    
                    plcCrntPage.Controls.Add(lnk);
                    lnk.Text  = Convert.ToString(i);
                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ")");
                }
            }
        }
    }

    /// <summary>
    /// Display the Details of the vendor when clicking on this button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgAdd_Click(object sender, ImageClickEventArgs e)
    {
        hdnClick.Value = "Add";
        tdButtons.Style["display"] = "none";
        Vendortd.Style["display"] = "";
        Session["VendorAdminId"] = 0;
    }

    /// <summary>
    /// When clicking on view existing display the record of the overall rating and the list also
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgExists_Click(object sender, ImageClickEventArgs e)
    {
        hdnClick.Value = "Exists";
        tdButtons.Style["display"] = "none";
        Vendortd.Style["display"] = "";
        imbSearchs.ImageUrl = "../Images/viewrat.jpg";
        imbSearchs.ToolTip = "View Rating";
    }

    /// <summary>
    /// Display the data after filtering the records
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSearch_Click(object sender, ImageClickEventArgs e)
    {
        Flag = 1;
        hdnPage.Value = "1";
        Session["txtVendor"] = txtVendor.Text.Trim();
        Session["txtVendorNumber"] = txtVendorNumber.Text.Trim();
        hdnPage.Value = "1";
        lblmsg.Text = string.Empty;
        ddlNumber.SelectedIndex = 0;
        Session["VendorNumber"] = txtVendor.Text.Trim();
        hdnPage.Value = "1";

        //Call method to bind data
        Bind();
        //002 - check for user action first
        if (hdnClick.Value.ToString() == "Add") hdnCount.Value = Convert.ToString(objRating.DisplayVendorNameCount(txtVendor.Text.Trim(), txtVendorNumber.Text.Trim()));
        else hdnCount.Value = Convert.ToString(objRating.DisplayExistingRatingVendorsCount(txtVendor.Text.Trim(), txtVendorNumber.Text.Trim()));

        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));
        lblTotal.Text = hdnCount.Value;
        Session["Page"] = Convert.ToString(page);
        plcCrntPage.Controls.Clear();


        //Call method to create paging controls dynamically
        CreatePaging(false, "");
        //Display previous and next button
        DisplayPreviousNext();
        //Apply css for the current page
        StyleforPaging(hdnPage.Value);
        lblTotalPage.Text = Convert.ToString(Session["Page"]);
        if (grdVendor.Rows.Count <= 0)
        {
            lblmsg.Text = "Sorry no records found";
            ContentTd.Style["display"] = "none";
        }
        else
        {
            ContentTd.Style["display"] = "";
        }
    }

    /// <summary>
    /// Display previous and next button
    /// </summary>
    private void DisplayPreviousNext()
    {
        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        imbPrevious.Visible = hdnPage.Value.Equals("1") ? false : true;
    }

    /// <summary>
    /// Apply styles for the paging
    /// </summary>
    /// <param name="CurrentLink"></param>
    private void StyleforPaging(string CurrentLink)
    {
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == CurrentLink)
                {
                    lnk1.CssClass = "Selectlnkcolor";
                }
                else
                {
                    lnk1.CssClass = "lnkcolor";
                }
            }
        }
    }

    /// <summary>
    /// Raise event for the controls created dynamically 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void lnkPage_Click(object sender, EventArgs e)
    {
        LinkButton Currentlnk = new LinkButton();
        Currentlnk = (LinkButton)sender;
        hdnPage.Value = Currentlnk.Text.Trim();
        DisplayPreviousNext();
        StyleforPaging(Currentlnk.Text.Trim());
        //Bind Gridview based on the paging
        //002
        if(hdnClick.Value.ToString() == "Add") grdVendor.DataSource = objRating.DisplayVendorName(Convert.ToInt32(ddlNumber.SelectedItem.Text.Trim()), Convert.ToInt32(hdnPage.Value), objCommon.ReplaceStrRevDS(txtVendor.Text.Trim()), txtVendorNumber.Text.Trim(), hdnSort.Value);
        else grdVendor.DataSource = objRating.DisplayExistingRatingVendors(Convert.ToInt32(ddlNumber.SelectedItem.Text.Trim()), Convert.ToInt32(hdnPage.Value), objCommon.ReplaceStrRevDS(txtVendor.Text.Trim()), txtVendorNumber.Text.Trim(), hdnSort.Value);
        grdVendor.DataBind();
    }

    /// <summary>
    /// Select only one checkbox in a gridview displayed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rdbSelect_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox rb = (CheckBox)sender;
        if (rb.Checked == true)
        {
            foreach (GridViewRow oldrow in grdVendor.Rows)
            {
                ((CheckBox)oldrow.FindControl("rdbVendor")).Checked = false;
            }
            GridViewRow row = (GridViewRow)rb.NamingContainer;
            ((CheckBox)row.FindControl("rdbVendor")).Checked = true;
        }
        else
        {
            GridViewRow row = (GridViewRow)rb.NamingContainer;
            ((CheckBox)row.FindControl("rdbVendor")).Checked = false;
        }
    }

    /// <summary>
    /// Bind Gridview
    /// </summary>
    private void Bind()
    {
        SortExp = hdnSort.Value;
        if (string.IsNullOrEmpty(SortExp))
        {
            SortExp = "";
        }
        //The previous line is been comented for SGS for task number: 6
        //002 - checking user action first
        if (hdnClick.Value.ToString() == "Add") grdVendor.DataSource = objRating.DisplayVendorName(Convert.ToInt32(ddlNumber.SelectedItem.Text.Trim()), Convert.ToInt32(hdnPage.Value), txtVendor.Text.Trim(), txtVendorNumber.Text.Trim(), SortExp);
        else grdVendor.DataSource = objRating.DisplayExistingRatingVendors(Convert.ToInt32(ddlNumber.SelectedItem.Text.Trim()), Convert.ToInt32(hdnPage.Value), txtVendor.Text.Trim(), txtVendorNumber.Text.Trim(), SortExp);
        grdVendor.DataBind();
        
        if (grdVendor.Rows.Count <= 0)
        {
            tblContent.Style["display"] = "none";
            lblmsg.Text = "Sorry no records found";
        }
        else
        {
            switch (hdnClick.Value)
            {
                case "Add":
                    imbSearchs.ImageUrl = "~/Images/addnewrat.jpg";
                    imbSearchs.ToolTip = "Add new rating";
                    break;
                case "Exists":
                    imbSearchs.ImageUrl = "~/Images/viewrat.jpg";
                    imbSearchs.ToolTip = "View Rating";
                    break;
                default:
                    imbSearchs.ImageUrl = "~/Images/addnewrat.jpg";
                    imbSearchs.ToolTip = "Add new rating";
                    hdnClick.Value = "Add";
                    break;
            }
            tblContent.Style["display"] = "";
            lblmsg.Text = string.Empty;
        }
    }

    /// <summary>
    /// Display the records based on the selected records-per-page item
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPage.Value = "1";
        hdnCurrentPage.Value = "1";
        plcCrntPage.Controls.Clear();
        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));
        Session["Page"] = Convert.ToString(page);
        //lblCurrentPage.Text  = Convert.ToString(hdnPage.Value);
        lblTotalPage.Text = Convert.ToString(Session["Page"]);
        plcCrntPage.Controls.Clear();
        CreatePaging(false, "");
        DisplayPreviousNext();
        StyleforPaging(hdnPage.Value);
        Bind();
    }

    /// <summary>
    /// Display next page record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbNext_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) + 1);
        DisplayPreviousNext();
        StyleforPaging(hdnPage.Value);
        Bind();
    }

    /// <summary>
    /// Display records of the previous page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrevious_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) - 1);
        DisplayPreviousNext();
        StyleforPaging(hdnPage.Value);
        Bind();
    }

    /// <summary>
    /// Change the text based on the condition in gridview
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnVendorId = new HiddenField();
            hdnVendorId = (HiddenField)e.Row.FindControl("hdnVendorId");
            AjaxControlToolkit.Rating rtg = new AjaxControlToolkit.Rating();
            rtg = (AjaxControlToolkit.Rating)e.Row.FindControl("rtgStar");
            Label lbl = (Label)e.Row.FindControl("lblVendorNumber");
            
            if (lbl.Text.Equals("0"))
            {
                lbl.Text  = string.Empty;
            }
            
            if (string.IsNullOrEmpty(lbl.Text.Trim()))
            {
                CheckBox chk = (CheckBox)e.Row.FindControl("rdbVendor");
                chk.Enabled = false;
            }
            
            if (objRating.GetCountOfRating(Convert.ToInt64(hdnVendorId.Value)) > 0)
            {
                rtg.Visible = true;
            }
            else
            {
                rtg.Visible = false;
            }
        }
    }

    /// <summary>
    /// Display the record based on the filtered record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSearchs_Click(object sender, ImageClickEventArgs e)
    {
        int flag = 0;
        foreach (GridViewRow grdrow in grdVendor.Rows)
        {
            flag++;
            CheckBox rdb = new CheckBox();
            rdb = (CheckBox)grdrow.FindControl("rdbVendor");
            if (rdb.Checked)
            {
                flag++;
                HiddenField hdn = new HiddenField();
                hdn = (HiddenField)grdrow.FindControl("hdnVendorId");
                Session["SubContractorPage"] = hdnClick.Value;
                if (hdnClick.Value == "Add")
                {
                    Session["VendorAdminId"] = hdn.Value;
                    Response.Redirect("InsertRating.aspx");
                }
                else
                {
                    Session["VendorAdminId"] = hdn.Value;
                    Response.Redirect("DisplayRating.aspx?VendorId=" + objCommon.encode(hdn.Value.Trim()) + "&FromPage=SubContractor");
                }
            }
        }
        if (flag == grdVendor.Rows.Count)
        {
            modalExtnd.Show();
        }
    }

}