﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InsertConsultantRating.aspx.cs"
    Inherits="Admin_InsertConsultantRating" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>

    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>

    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script language="javascript" type="text/javascript">
        window.history.go(1);
        function numberFormat(nStr, prefix, ctrl) {
            var txt = document.getElementById(ctrl);
            if (txt.value != "0") {

                var prefix = prefix || '';
                nStr += '';
                nStr = nStr.replace(/^[0]+/g, "");

                y = nStr.split(',');
                var y1 = '';
                for (var i = 0; i < y.length; i++) {
                    y1 += y[i];
                }
                if (y.length > 0) {
                    nStr = y1;
                }
                x = nStr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1))
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                var txt = document.getElementById(ctrl);
                // txt.value="";
                // txt.value= prefix + x1 + x2;
                // alert(prefix + x1 + x2);
                //   return prefix + x1 + x2;
                txt.value = x1 + x2;
            }
        }
    </script>

    <script src="../Script/jquery.js" type="text/javascript"></script>

    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
        .auto-style1
        {
            height: 44px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="960px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
<%--                            <asp:UpdatePanel ID="updRating" runat="server">
                                <ContentTemplate>
--%>                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="leftmenutd">
                                                <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                            </td>
                                            <td class="contenttd">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="formhead" nowrap="nowrap">
                                                            Consultant Rating
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="pnlConsultantRating" runat="server">
                                                                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:ImageButton ID="imgback" runat="server" ImageUrl="~/Images/back.jpg" ToolTip="Back"
                                                                                OnClick="imgback_Click" />
                                                                            <asp:HiddenField ID="hdnRatingId" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="table_border">
                                                                            <table width="100%" cellpadding="3" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="2%">
                                                                                                </td>
                                                                                                <td class="bodytextbold " width="13%">
                                                                                                    Consultant :&nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="61%">
                                                                                                    <asp:Label ID="lblConsultantName" CssClass="bodytextheadbold" runat="server"> </asp:Label>
                                                                                                </td>
                                                                                                <td class="bodytextbold_right" width="14%">
                                                                                                    Rating Date :
                                                                                                </td>
                                                                                                <td class="bodytextright left_border" width="8%">
                                                                                                    <asp:Label ID="lblRatingDate" runat="server"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <span id="spnEdit" runat="server">
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td width="1%">
                                                                                                        <span class="mandatorystar">*</span>
                                                                                                    </td>
                                                                                                    <td class="bodytextheadbold" width="21%">
                                                                                                        Project Number :
                                                                                                    </td>
                                                                                                    <td width="20%">
                                                                                                        <%--G. Vera 06/20/2012 See JIRA VPI-37 Item <asp:DropDownList ID="ddlProjectNumber" CssClass="ddlbox" runat="server" AutoPostBack="True"
                                                                                                            OnSelectedIndexChanged="ddlProjectNumber_SelectedIndexChanged" TabIndex="1">
                                                                                                        </asp:DropDownList>--%>
                                                                                                        <%--G. Vera 06/20/2012 See JIRA VPI-37 Item--%>
                                                                                                        <asp:TextBox ID="ddlProjectNumber" runat="server" CssClass="ddlbox" MaxLength="12"></asp:TextBox>
                                                                                                        <Ajax:AutoCompleteExtender ID="AutoCompleteExtenderProj" runat="server" BehaviorID="AutoCompleteExt1"
                                                                                                                CompletionListElementID="divProjectNumber" EnableCaching="false" FirstRowSelected="True"
                                                                                                                MinimumPrefixLength="1" ServiceMethod="GetProjectListCompletion" ServicePath="~/AutoComplete.asmx"
                                                                                                                TargetControlID="ddlProjectNumber" CompletionSetCount="10" 
                                                                                                            onclientitemselected="OnContactSelected">
                                                                                    
                                                                                                        </Ajax:AutoCompleteExtender>
                                                                                                        <asp:HiddenField ID="projNumberSelected" runat="server" onvaluechanged="ddlProjectNumber_SelectedIndexChanged" />
                                                                                                        <asp:RequiredFieldValidator ID="reqvProjectNumber" runat="server" SetFocusOnError="true"
                                                                                                            Display="None" ValidationGroup="InsertRating" EnableClientScript="true" ControlToValidate="ddlProjectNumber"
                                                                                                            ErrorMessage="Please select project number" ></asp:RequiredFieldValidator>
                                                                                                        <asp:Label ID="lblProjectNumber" runat="server" Visible="false" CssClass="bodytextheadbold"></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="bodytextbold" width="13%">
                                                                                                        Project Name :
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                    </td>
                                                                                                    <td class="bodytextbold ">
                                                                                                        Rater Name :
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:Label ID="lblRaterName" runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="bodytextbold">
                                                                                                        Rater Title :
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:Label ID="lblRaterTitle" runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                    </td>
                                                                                                    <td class="bodytextbold " nowrap="nowrap">
                                                                                                        Final Contract Value :
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                        <asp:TextBox ID="txtFinalSubcontract" runat="server" MaxLength="13" TabIndex="2"
                                                                                                            CssClass="txtboxmedium"></asp:TextBox>
                                                                                                        <Ajax:FilteredTextBoxExtender ID="ftxtFinalSubcontract" runat="server" TargetControlID="txtFinalSubcontract"
                                                                                                            FilterType="Numbers, Custom" ValidChars=",">
                                                                                                        </Ajax:FilteredTextBoxExtender>
                                                                                                    </td>
                                                                                                    <td class="bodytextbold">
                                                                                                    </td>
                                                                                                    <td class="bodytextleft">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <%--  <tr>
                                                                                        <td class="bodytextbold">
                                                                                            Rater Title
                                                                                        </td>
                                                                                        <td class="bodytextleft">
                                                                                            <asp:Label ID="lblRaterTitle1" runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td colspan="2">
                                                                                            <span id="spnModDate" runat="server" style="display: none">
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td class="bodytextleft">
                                                                                                            Last Modified Date
                                                                                                        </td>
                                                                                                        <td class="bodytextleft">
                                                                                                           <asp:Label ID="lblLastModified" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>--%>
                                                                                            </table>
                                                                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td width="2%">
                                                                                                    </td>
                                                                                                    <td class="bodytextbold" width="21%" valign="top">
                                                                                                        Contract scope :
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" valign="top">
                                                                                                        <asp:TextBox ID="txtConsultantScope" onFocus="javascript:textCounter(this,1500,'yes');"
                                                                                                            onKeyDown="javascript:textCounter(this,1500,'yes');" onKeyUp="javascript:textCounter(this,1500,'yes');"
                                                                                                            TabIndex="3" runat="server" TextMode="MultiLine" CssClass="txtboxlarge" Width="520px"
                                                                                                            Height="40px"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="100%">
<%--                                                                            <asp:UpdatePanel ID="updpnl1" runat="server">
                                                                                <ContentTemplate>
--%>                                                                                    <asp:Panel ID="pnlRating" runat="server">
                                                                                        <table width="100%" cellpadding="5" cellspacing="0">
                                                                                            <tr>
                                                                                                <td class="even_pad">
                                                                                                    <table cellpadding="10" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextheadcenter" colspan="5">
                                                                                                                Ratings
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="5" align="center" class="auto-style1">
                                                                                                                <table width="100%">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextheadright" width="50%">
                                                                                                                            <span class="mandatorystar">* </span>Overall Performance
                                                                                                                        </td>
                                                                                                                        <td style="padding: 0 50px 0 15px">
                                                                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                <tr>
                                                                                                                                    <td class="bodytextright" style="padding-right: 5px;" width="1%">
                                                                                                                                        <asp:ImageButton ID="imgCancelOverallPerfomance" runat="server" ImageUrl="~/Images/x.png"
                                                                                                                                            OnClick="imgCancelOverallPerfomance_Click" ToolTip="Clear" />
                                                                                                                                    </td>
                                                                                                                                    <td class="bodytextleft">
                                                                                                                                        <Ajax:Rating ID="rtgOverallPerformance" runat="server" CssClass="ratingStar" CurrentRating="0"
                                                                                                                                            EmptyStarCssClass="Empty" FilledStarCssClass="Filled" MaxRating="5" StarCssClass="ratingItem"
                                                                                                                                            ValidationGroup="InsertRating" TabIndex="4" WaitingStarCssClass="Saved">
                                                                                                                                        </Ajax:Rating>
                                                                                                                                        <asp:CustomValidator ID="cusvOverallPerformance" runat="server" SetFocusOnError="true"
                                                                                                                                            EnableClientScript="true" Display="None" OnServerValidate="cusvOverallPerformance_ServerValidate"></asp:CustomValidator>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleft">Quality Of Work
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextleft" width="1%" style="padding-right: 5px;">
                                                                                                                            <asp:ImageButton ID="imgRtgQualityOfWork" runat="server" ToolTip="Clear" ImageUrl="~/Images/x.png"
                                                                                                                                OnClick="imgRtgQualityOfWork_Click" />
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <Ajax:Rating ID="rtgQualityOfWork" runat="server" CssClass="ratingStar" TabIndex="7"
                                                                                                                                CurrentRating="0" EmptyStarCssClass="Empty" FilledStarCssClass="Filled" MaxRating="5"
                                                                                                                                StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                            </Ajax:Rating>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                            <td width="15px">
                                                                                                            </td>
                                                                                                            <td class="bodytextleft">
                                                                                                                Project Management (Design Phase)
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextleft" width="1%" style="padding-right: 5px;">
                                                                                                                            <asp:ImageButton ID="imgRtgProjectManagement" runat="server" ToolTip="Clear" ImageUrl="~/Images/x.png"
                                                                                                                                OnClick="imgRtgProjectManagement_Click" />
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <Ajax:Rating ID="rtgProjectManagement" runat="server" CssClass="ratingStar" TabIndex="6"
                                                                                                                                CurrentRating="0" EmptyStarCssClass="Empty" FilledStarCssClass="Filled" MaxRating="5"
                                                                                                                                StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                            </Ajax:Rating>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleft">
                                                                                                                Project Management (Construction Phase)
                                                                                                            </td>
                                                                                                            <td width="130px">
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextleft" width="1%" style="padding-right: 5px;">
                                                                                                                            <asp:ImageButton ID="imgRtgFileManagement" runat="server" ToolTip="Clear" ImageUrl="~/Images/x.png"
                                                                                                                                OnClick="imgRtgFileManagement_Click" />
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <Ajax:Rating ID="rtgFileManagement" runat="server" CssClass="ratingStar" TabIndex="8"
                                                                                                                                CurrentRating="0" EmptyStarCssClass="Empty" FilledStarCssClass="Filled" MaxRating="5"
                                                                                                                                StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                            </Ajax:Rating>
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <asp:CheckBox ID="chkNAField" runat="server" Text="N/A" OnCheckedChanged="chkNAField_CheckedChanged"
                                                                                                                                AutoPostBack="true" />
                                                                                                                            
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                            <td width="15px"></td>
                                                                                                            
                                                                                                            <td class="bodytextleft">Schedule Control
                                                                                                            </td>
                                                                                                            <td width="100px">
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextleft" width="1%" style="padding-right: 5px;">
                                                                                                                            <asp:ImageButton ID="imgCancelScehduleControl" runat="server" ToolTip="Clear" ImageUrl="~/Images/x.png"
                                                                                                                                OnClick="imgCancelScehduleControl_Click" />
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <Ajax:Rating ID="rtgSceduleControl" runat="server" CssClass="ratingStar" CurrentRating="0"
                                                                                                                                TabIndex="12" EmptyStarCssClass="Empty" FilledStarCssClass="Filled" MaxRating="5"
                                                                                                                                StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                            </Ajax:Rating>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                           
                                                                                                            <td class="bodytextleft">
                                                                                                                Change Orders/Claims
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextleft" width="1%" style="padding-right: 5px;">
                                                                                                                            <asp:ImageButton ID="imgRtgChange" runat="server" ToolTip="Clear" ImageUrl="~/Images/x.png"
                                                                                                                                OnClick="imgRtgChange_Click" />
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <Ajax:Rating ID="rtgChange" runat="server" CssClass="ratingStar" CurrentRating="0"
                                                                                                                                TabIndex="10" EmptyStarCssClass="Empty" FilledStarCssClass="Filled" MaxRating="5"
                                                                                                                                StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                            </Ajax:Rating>
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <asp:CheckBox ID="chkNAChangeOrders" runat="server" Text="N/A" OnCheckedChanged=chkNAChangeOrders_CheckedChanged AutoPostBack="true"/>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                            <td>

                                                                                                            </td>
                                                                                                            <td class="bodytextleft">Cooperation/Responsiveness
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td class="bodytextleft" width="1%" style="padding-right: 5px;">
                                                                                                                            <asp:ImageButton ID="imgRtgCorporation" runat="server" ImageUrl="~/Images/x.png"
                                                                                                                                OnClick="imgRtgCorporation_Click" />
                                                                                                                        </td>
                                                                                                                        <td class="bodytextleft">
                                                                                                                            <Ajax:Rating ID="rtgCorporation" runat="server" CssClass="ratingStar" CurrentRating="0"
                                                                                                                                TabIndex="14" EmptyStarCssClass="Empty" FilledStarCssClass="Filled" MaxRating="5"
                                                                                                                                StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                                                            </Ajax:Rating>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleft" colspan="3">
                                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Did consultant switch any key team members during the project?
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:RadioButtonList ID="rblpay" runat="server" RepeatDirection="Horizontal" TabIndex="11"
                                                                                                                    CssClass="textboxsmall">
                                                                                                                    <asp:ListItem>Y</asp:ListItem>
                                                                                                                    <asp:ListItem>N</asp:ListItem>
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>
                                                                                                            
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleft" colspan="3">
                                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Did Haskell have to waive insurance levels?
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:RadioButtonList ID="rblLevels" runat="server" RepeatDirection="Horizontal" 
                                                                                                                    TabIndex="13">
                                                                                                                    <asp:ListItem>Y</asp:ListItem>
                                                                                                                    <asp:ListItem>N</asp:ListItem>
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>
                                                                                                            
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="bodytextleft" colspan="3">
                                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Did consultant require special payment terms?
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:RadioButtonList ID="rblPayment" runat="server" RepeatDirection="Horizontal"
                                                                                                                    TabIndex="15">
                                                                                                                    <asp:ListItem>Y</asp:ListItem>
                                                                                                                    <asp:ListItem>N</asp:ListItem>
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold">
                                                                                                    Comments
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtComments" runat="server" Height="125px" TextMode="MultiLine" onFocus="javascript:textCounter(this,1500,'yes');"
                                                                                                        onKeyDown="javascript:textCounter(this,1500,'yes');" onKeyUp="javascript:textCounter(this,1500,'yes');"
                                                                                                                 TabIndex="16" Width="700px" Wrap="true" MaxLength="1500"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="tdheight">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr align="center">
                                                                                                <td>
                                                                                                    <asp:HiddenField ID="hdnrating" runat="server" />
                                                                                                    <asp:HiddenField ID="hdnvendorId" runat="server" />
                                                                                                    <asp:ImageButton ID="imbUpdate" ToolTip="Save" runat="server" ImageUrl="~/images/Update.jpg"
                                                                                                        TabIndex="17" OnClick="imbUpdate_Click" />
                                                                                                    <asp:ImageButton ID="imbSave" ToolTip="Save" runat="server" ImageUrl="~/images/save.jpg"
                                                                                                        OnClick="imbSave_Click" Visible="false" TabIndex="18" />
                                                                                                    <asp:ImageButton ID="imbSubmitAndDone" ToolTip="Submit and Done" runat="server" ImageUrl="~/Images/subndone.jpg"
                                                                                                        TabIndex="19" OnClick="imbSubmitAndDone_Click"/>
                                                                                                    <%--G. Vera 07/25/2012 - changed image--%>
                                                                                                    <asp:ImageButton ID="imbPrint" ToolTip="Print" runat="server" ImageUrl="~/Images/ex2pdf.jpg"
                                                                                                                    TabIndex="20" onclick="imbPrint_Click" />
                                                                                                        
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:Panel>
<%--                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
--%>                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdheight">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                                    PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                                </Ajax:ModalPopupExtender>
                                                <asp:HiddenField ID="lblHidden" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="modalPopupDiv" class="popupdiv" style="display: none">
                                                    <table cellpadding="5" cellspacing="0" border="0" width="350">
                                                        <tr>
                                                            <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                                Message
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight popupdivcl">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popupdivcl">
                                                                <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popupdivcl bodytextcenter">
                                                                <%--<input type="button" value="OK" class="ModalPopupButton" onclick="$find('modalExtnd').hide();" />--%>
                                                               <%--G. Vera 08/29/2012 - Release 1.2--%>
                                                               <asp:ImageButton runat="server" ID="btnOk" ImageUrl="~/Images/ok.jpg" 
                                                                    onclick="btnOk_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight popupdivcl">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
<%--                                </ContentTemplate>
                            </asp:UpdatePanel>
--%>                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
<script language="javascript" type="text/javascript">
    function OnContactSelected(source, eventArgs) {

        var hdnValueID = "<%= projNumberSelected.ClientID %>";

        document.getElementById(hdnValueID).value = eventArgs.get_value();
        __doPostBack(hdnValueID, "");
    }

    
    function DisableEnableControl(controlId, isDisable) {
        debugger;
        document.getElementById(control).disabled = isDisable;
    }
</script>

    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>
</body>
</html>
