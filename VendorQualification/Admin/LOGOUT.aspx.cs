﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_LOGOUT : System.Web.UI.Page
{
    RiskEvaluation riskEvaluation = new RiskEvaluation();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["RISKIDLOGOUT"] != null && Session["LOCKEDBYLOGOUT"] != null)
        {
            Int64 Lockedby = riskEvaluation.LockebByUser(Convert.ToInt64(Session["RISKIDLOGOUT"]));
            if (Lockedby == Convert.ToInt64(Session["LOCKEDBYLOGOUT"]))
            {
                riskEvaluation.UpdateStatus(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                Session["RISKIDLOGOUT"] = null;
            }
        }
    }
}
