﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VQFPage.aspx.cs" Inherits="Admin_VQFPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language ="javascript" type="text/javascript">
          window.onerror = ErrHndl;

          function ErrHndl()
          { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
     <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
            </script>   
     <style type="text/css">

body 
{
	font-family: Arial;
	font-size: 11px;
	color: #333333;
	background: url(../images/body_bg.png) repeat-x;
	background-color: #2672b8;
	  margin-top:0px;
}



</style>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
   <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
           
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                   
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" />
                                    </td>
                                    <td class="contenttd">
                                        <Haskell:AdminMaintananceMenu ID="mnuMain" runat="server" />
                                        
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr><td class="tdheight"></td></tr>
                                      
                                        <tr>
                    <td align="right">
                    <asp:HyperLink ID= "hlCSICODES" CssClass="scopelink" NavigateUrl="~/Admin/AddScopes.aspx" Text="CSI Codes" runat="server"> </asp:HyperLink> | <asp:HyperLink ID= "HyperLink1" CssClass="scopelink" NavigateUrl="~/Admin/AddCertification.aspx" Text=" Company Certification" runat="server"></asp:HyperLink>
                    </td>
                    </tr>
                    </table>
                                    </td>
                                </tr>
                              
                       
                            
                            
                            </table>
                        </td>
                      
                    </tr>
                    
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
          
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
