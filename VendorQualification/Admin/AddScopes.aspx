﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddScopes.aspx.cs" Inherits="Admin_AddScopes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language ="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>

    <link href="../css/Haskell.css" rel="Stylesheet" rev="Stylesheet" type="text/css" />

    <script src="../Script/jquery.js" type="text/javascript"></script>

    <style type="text/css">
        body 
        { 
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
              margin-top:0px;
        }
        .layer1
        {
            position: absolute;
            visibility: hidden;
            width: 400px;
            height: 200px;
            left: 20px;
            top: 300px;
            background-color: #efefef;
            border: solid 1px #3f3f3f;
        }
        .layer2
        {
            position: absolute;
            visibility: hidden;
            width: 300px;
            height: 110px;
            left: 20px;
            top: 300px;
            background-color: #efefef;
            border: solid 1px #3f3f3f;
        }
        .close
        {
            float: right;
        }
    </style>

    

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" />
                                    </td>
                                    <td class="contenttd">
                                        <Haskell:AdminMaintananceMenu ID="mnuMain" runat="server" />
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:HyperLink ID="hlCSICODES" CssClass="scopelink" NavigateUrl="~/Admin/AddScopes.aspx"
                                                        Text="MasterFormat" runat="server"> </asp:HyperLink>
                                                    |
                                                    <asp:HyperLink ID="HyperLink1" CssClass="scopelink" NavigateUrl="~/Admin/AddCertification.aspx"
                                                        Text=" Company Certification" runat="server"></asp:HyperLink>
                                                      |
                                                    <asp:HyperLink ID="hlVQfsnapshot" CssClass="scopelink" NavigateUrl="~/Admin/SnapshotHistory.aspx"
                                                        Text="VQF Snapshot History" runat="server"></asp:HyperLink>
                                                         |
                                                    <asp:HyperLink ID="hlSREsnapshot" CssClass="scopelink" NavigateUrl="~/Admin/SnapshotHistorySRE.aspx"
                                                        Text="SRE Snapshot History" runat="server"></asp:HyperLink>
                                                    |
                                                    <asp:HyperLink ID="hlTermsConditions" CssClass="scopelink" NavigateUrl="~/Admin/TermsConditions.aspx"
                                                        Text="Terms & Conditions" runat="server"></asp:HyperLink> | 
                                                    <%--G. Vera 03/01/2013 - added--%>
                                                    <asp:LinkButton ID="lnkPrintBlankVQF" runat="server" CssClass="scopelink" 
                                                        onclick="lnkPrintBlankVQF_Click" >Print Blank VQF</asp:LinkButton>
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:UpdatePanel ID="updPanel" runat="server">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="tdheight">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdheight">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdheight">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="formhead">
                                                            MasterFormat
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdheight">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdheight">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td width="5%" class="formtdlt">
                                                                        Level 1
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:DropDownList ID="ddlLevel1" CssClass="ddlbox" runat="server" AutoPostBack="true"
                                                                            AppendDataBoundItems="true" OnSelectedIndexChanged="ddlLevel1_SelectedIndexChanged"
                                                                            Width="425">
                                                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:LinkButton ID="lnkAddLevel1" runat="server" Text="Add" CssClass="scopelink"
                                                                            OnClick="lnkAddLevel1_Click"></asp:LinkButton>&nbsp;&nbsp;
                                                                        <div id="layer1" class="layer1" runat="server" style="width: 450px;">
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td id="close" align="center" valign="top" class="searchhdrbarbold">
                                                                                        Add Level 1
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td width="5%" class="bodytextright" height="25px">
                                                                                                    1 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAddLevel1" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    2 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAddLevel2" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    3 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAddLevel3" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    4 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAddLevel4" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    5 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAddLevel5" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="tdheight">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td class="bodytextcenter">
                                                                                                    <asp:ImageButton ID="imgAdd" runat="server" ImageUrl="~/Images/submit.jpg" OnClick="imgAdd_Click"
                                                                                                        ToolTip="Submit" />&nbsp;
                                                                                                    <asp:ImageButton ID="imgAddMore" runat="server" ImageUrl="~/Images/subnaddmore.jpg"
                                                                                                        ToolTip="Submit and Add More" OnClick="imgAddMore_Click" />&nbsp; <a href="javascript:setVisible('layer1'); ">
                                                                                                            <img src="../Images/cancel.jpg" border="0" title="Cancel" /></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <asp:LinkButton ID="lnkModify" runat="server" Text="Modify" OnClick="lnkModify_Click"
                                                                            CssClass="scopelink"></asp:LinkButton>&nbsp;&nbsp;
                                                                        <div id="layer2" class="layer2" runat="server" style="width: 450px">
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td id="Close1" width="390px" align="center" valign="top" class="searchhdrbarbold">
                                                                                        <tr>
                                                                                            <td class="bodytextcenter">
                                                                                                <asp:TextBox ID="txtUpdateLevel1" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                    MaxLength="1000"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextcenter">
                                                                                                <asp:ImageButton ID="imgUpdateLevel1" runat="server" ImageUrl="~/Images/updatel1.jpg"
                                                                                                    ToolTip="Update" OnClick="imgUpdateLevel1_Click" />
                                                                                                <a href="javascript:setVisible('layer2')">
                                                                                                    <img src="../Images/cancel.jpg" border="0" title="Cancel" /></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight">
                                                                                            </td>
                                                                                        </tr>
                                                                            </table>
                                                                        </div>
                                                                        <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_Click"
                                                                            CssClass="scopelink"></asp:LinkButton>
                                                                        <asp:HiddenField ID="hdnLevel" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdheight">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt">
                                                                        Level 2
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:DropDownList ID="ddlLevel2" runat="server" AppendDataBoundItems="true" CssClass="ddlbox"
                                                                            AutoPostBack="True" Width="425" OnSelectedIndexChanged="ddlLevel2_SelectedIndexChanged">
                                                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:LinkButton ID="lnkLevel2Add" runat="server" Text="Add" OnClick="lnkLevel2Add_Click"
                                                                            CssClass="scopelink"></asp:LinkButton>&nbsp;&nbsp;
                                                                        <div id="Level2Layer1" class="layer1" runat="server" style="width: 450px">
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td id="Td1" width="390px" align="center" valign="top" class="searchhdrbarbold">
                                                                                        Add Level 2
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextcenter">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td width="5%" class="bodytextright" height="25px">
                                                                                                    1 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAdd2Level1" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    2 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAdd2Level2" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    3 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAdd2Level3" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    4 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAdd2Level4" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    5 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAdd2Level5" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="tdheight">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="imgAddLevel2" runat="server" ImageUrl="~/Images/submit.jpg"
                                                                                                        OnClick="imgAdd_Click" ToolTip="Submit" />
                                                                                                    &nbsp;<asp:ImageButton ID="imgAddMoreLevel2" runat="server" ImageUrl="~/Images/subnaddmore.jpg"
                                                                                                        OnClick="imgAddMore_Click" ToolTip="Submit and Add More" />
                                                                                                    &nbsp;<a href="javascript:setVisible('Level2Layer1')"><img src="../Images/cancel.jpg"
                                                                                                        border="0" title="Cancel" /></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <asp:LinkButton ID="lnkLevel2Modify" runat="server" Text="Modify" OnClick="lnkLevel2Modify_Click"
                                                                            CssClass="scopelink"></asp:LinkButton>&nbsp;&nbsp;
                                                                        <div id="Level2Layer2" class="layer2" runat="server" style="width: 450px">
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td id="Td2" width="390px" align="center" valign="top" class="searchhdrbarbold">
                                                                                        Update Level 2
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextcenter">
                                                                                        <asp:TextBox ID="txtUpdateLevel2" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                            MaxLength="1000"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextcenter">
                                                                                        <asp:ImageButton ID="imgUpdateLevel2" runat="server" ImageUrl="~/Images/updatel2.jpg"
                                                                                            OnClick="imgUpdateLevel1_Click" ToolTip="Update" />
                                                                                        &nbsp;<a href="javascript:setVisible('Level2Layer2')" style="color: white; text-decoration: none"><img
                                                                                            src="../Images/cancel.jpg" border="0" title="Cancel" /></a>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <asp:LinkButton ID="lnkLevel2Delete" runat="server" Text="Delete" OnClick="lnkDelete_Click"
                                                                            CssClass="scopelink"></asp:LinkButton>&nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdheight">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="formtdlt">
                                                                        Level 3
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:DropDownList ID="ddlLevel3" runat="server" CssClass="ddlbox" Width="425">
                                                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td class="formtdrt">
                                                                        <asp:LinkButton ID="lnkLeve3Add" runat="server" Text="Add" CssClass="scopelink" OnClick="lnkLevel2Add_Click"></asp:LinkButton>&nbsp;&nbsp;
                                                                        <div id="Level3Layer1" class="layer1" runat="server" style="width: 450px">
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td id="Td4" width="390px" align="center" valign="top" class="searchhdrbarbold">
                                                                                        Add Level 3
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextcenter">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td width="5%" class="bodytextright" height="25px">
                                                                                                    1 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAdd3Level1" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    2 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAdd3Level2" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    3 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAdd3Level3" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    4 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAdd3Level4" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextright" height="25px">
                                                                                                    5 &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleft" height="25px">
                                                                                                    <asp:TextBox ID="txtAdd3Level5" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                                        MaxLength="1000"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="tdheight">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="imgLevel3Add" runat="server" ImageUrl="~/Images/submit.jpg"
                                                                                                        OnClick="imgAdd_Click" ToolTip="Submit" />
                                                                                                    &nbsp;<asp:ImageButton ID="imgLevel3AddMore" runat="server" ImageUrl="~/Images/subnaddmore.jpg"
                                                                                                        OnClick="imgAddMore_Click" ToolTip="submit and Add More" />
                                                                                                    &nbsp;<a href="javascript:setVisible('Level3Layer1')"><img src="../Images/cancel.jpg"
                                                                                                        border="0" title="Cancel" /></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <asp:LinkButton ID="lnkLevel3Modify" runat="server" Text="Modify" OnClick="lnkLevel2Modify_Click"
                                                                            CssClass="scopelink"></asp:LinkButton>&nbsp;&nbsp;
                                                                        <div id="Level3Layer2" class="layer2" runat="server" style="width: 450px">
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="450px">
                                                                                <tr>
                                                                                    <td id="Td5" align="center" valign="top" class="searchhdrbarbold">
                                                                                        Update Level 3
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding: 5px">
                                                                                        <asp:TextBox ID="txtUpdateLevel3" runat="server" CssClass="txtboxlarge" Width="400"
                                                                                            MaxLength="1000"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextcenter">
                                                                                        <asp:ImageButton ID="imgUpdateLevel3" runat="server" ImageUrl="~/Images/updatel3.jpg"
                                                                                            OnClick="imgUpdateLevel1_Click" ToolTip="Update" />
                                                                                        <a href="javascript:setVisible('Level3Layer2')">
                                                                                            <img src="../Images/cancel.jpg" border="0" title="Cancel" />
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <asp:LinkButton ID="lnkLevel3Delete" runat="server" Text="Delete" OnClick="lnkDelete_Click"
                                                                            CssClass="scopelink"></asp:LinkButton>&nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdheight">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-bottom: 1px solid #cccccc" class="style1">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TreeView ID="trvScopes" runat="server" AutoGenerateDataBindings="False" ShowLines="True"
                                                                ShowExpandCollapse="false">
                                                                <RootNodeStyle CssClass="list_head" ForeColor="#005785" />
                                                                <ParentNodeStyle CssClass="list_display" />
                                                                <LeafNodeStyle CssClass="list_display1" />
                                                            </asp:TreeView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                                                PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                                            </Ajax:ModalPopupExtender>
                                                            <asp:HiddenField ID="lblHidden" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="modalPopupDiv" class="popupdiv" style="display: none">
                                                                <table cellpadding="5" cellspacing="0" border="0" width="350">
                                                                    <tr>
                                                                        <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                                            Result
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight popupdivcl">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="popupdivcl">
                                                                            <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="popupdivcl bodytextcenter">
                                                                            <input type="button" value="OK" title="Ok" class="ModalPopupButton" onclick="$find('modalExtnd').hide();" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight popupdivcl">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <Ajax:ModalPopupExtender ID="modalConfirm" runat="server" TargetControlID="hdnModal"
                                                                PopupControlID="modalDiv" BackgroundCssClass="popupbg" CancelControlID="imbCancel"
                                                                RepositionMode="RepositionOnWindowResize">
                                                            </Ajax:ModalPopupExtender>
                                                            <asp:HiddenField ID="hdnModal" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="modalDiv" class="popupdiv" style="display: none">
                                                                <table cellpadding="5" cellspacing="0" border="0" width="350">
                                                                    <tr>
                                                                        <td class="searchhdrbarbold" runat="server" id="Td3">
                                                                            Confirm
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight popupdivcl">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="popupdivcl">
                                                                            <asp:Label ID="lblConfirm" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight popupdivcl">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="popupdivcl bodytextcenter">
                                                                            <asp:ImageButton ID="imbOk" runat="server" ImageUrl="~/Images/ok.jpg" OnClick="imgOK_Click"
                                                                                ToolTip="Ok" />
                                                                            &nbsp;
                                                                            <asp:ImageButton ID="imbCancel" runat="server" ImageUrl="~/Images/cancel.jpg" ToolTip="Cancel" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight popupdivcl">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>
</body>
</html>

<script language="javascript" type="text/javascript">

    function setVisible(obj) {
        obj = document.getElementById(obj);
        var single = obj.childNodes;
        var elms = obj.getElementsByTagName("*");
        for (var i = 0, maxI = elms.length; i < maxI; ++i) {
            var elm = elms[i];
            switch (elm.type) {
                case "text":
                    elm.value = "";
            }
        }
        obj.style.visibility = 'hidden';
    }
        
</script>

