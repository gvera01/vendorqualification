﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListEmployee.aspx.cs" Inherits="Admin_ListEmployee" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language ="javascript" type="text/javascript">
          window.onerror = ErrHndl;

          function ErrHndl()
          { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function GetValue(i) {
            var strPageNumber = document.getElementById("<%= hdnPage.ClientID %>");
            strPageNumber.value = i;
        }
        function AssignValue(hdnPageValue) {
            var hdnpages = document.getElementById(hdnPageValue);
            hdnpages.value = "1";
        }
    </script>

</head>
<body>
    <form id="form1" runat="server" defaultbutton="imgSearch">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <Haskell:AdminMaintananceMenu ID="mnuMain" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    List Employees
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextright">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="50%">
                                                        <tr>
                                                            <td class="bodytextright">
                                                                Group
                                                            </td>
                                                            <td class="bodytextleft">
                                                                <asp:DropDownList ID="ddlJob" runat="server" CssClass="ddlbox">
                                                                    <asp:ListItem Text=""> --Select-- </asp:ListItem>
                                                                    <asp:ListItem Text="ProjectManager">Project Manager</asp:ListItem>
                                                                    <asp:ListItem Text="ProjectLeader">Project Leader</asp:ListItem>
                                                                    <asp:ListItem Text="Associate">Associate</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodytextright">
                                                                Access
                                                            </td>
                                                            <td class="bodytextleft">
                                                                <asp:DropDownList ID="ddlRoles" runat="server" CssClass="ddlbox">
                                                                    <asp:ListItem Value="0"> --Select-- </asp:ListItem>
                                                                    <asp:ListItem Value="1">Admin</asp:ListItem>
                                                                    <asp:ListItem Value="2">Employee</asp:ListItem>
                                                                    <asp:ListItem Value="4">Disable</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:HiddenField ID="hdnPageFlag" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodytextright">
                                                                Employee Name or Number
                                                            </td>
                                                            <td class="bodytextleft">
                                                                <asp:TextBox ID="txtEmployeeNumber" runat="server" CssClass="txtboxmedium" MaxLength="10"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodytextright" colspan="1">
                                                                &nbsp;
                                                            </td>
                                                            <td class="bodytextleft">
                                                                <asp:ImageButton ID="imgSearch" runat="server" ImageUrl="~/Images/search.jpg" OnClick="imgSearch_Click"
                                                                    ToolTip="Search" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <asp:Label ID="lblmsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" id="tblContent" runat="server">
                                                        <tr>
                                                            <td colspan="2">
                                                                <table width="100%" cellpadding="3" cellspacing="0">
                                                                    <tr>
                                                                        <td class="bodytextleft">
                                                                            Total Number Of Records:
                                                                            <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td class="bodytextright" colspan="4" align="right">
                                                                            No. of Records per Page
                                                                            <asp:DropDownList ID="ddlNumber" runat="server" CssClass="ddlbox" AutoPostBack="true"
                                                                                OnSelectedIndexChanged="ddlNumber_SelectedIndexChanged">
                                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                                                <asp:ListItem Value="40">40</asp:ListItem>
                                                                                <asp:ListItem Value="50">50</asp:ListItem>
                                                                                <%--Changed By SGS for Task 5--%>
                                                                                <asp:ListItem Value="100">100</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <asp:GridView ID="grdVendor" runat="server" GridLines="None" Width="100%" BorderWidth="0"
                                                                    CellPadding="5" CellSpacing="1" AutoGenerateColumns="False" OnRowDataBound="grdVendor_RowDataBound"
                                                                    AllowSorting="true" OnSorting="grdVendor_Sorting">
                                                                    <RowStyle CssClass="listone" />
                                                                    <AlternatingRowStyle CssClass="listtwo" />
                                                                    <HeaderStyle CssClass="listheading" />
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                            <EditItemTemplate>
                                                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                            </EditItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkAllocate" runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Employee Number" HeaderStyle-CssClass="gridpad" HeaderStyle-ForeColor="White"
                                                                            SortExpression="Employee.EmployeeNumber" ItemStyle-CssClass="gridpad">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("EmployeeNumber") %>'></asp:TextBox>
                                                                            </EditItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblEmployeeNumber" runat="server" Text='<%# Bind("EmployeeNumber") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="EmployeeName" HeaderStyle-ForeColor="White" SortExpression="EmployeeName"
                                                                            HeaderText="Employee Name" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad">
                                                                            <HeaderStyle />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Email" HeaderStyle-ForeColor="White" SortExpression="Email"
                                                                            HeaderText="E-Mail" HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad">
                                                                            <HeaderStyle />
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField HeaderText="Access" HeaderStyle-ForeColor="White" SortExpression="Access"
                                                                            HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Access") %>'></asp:TextBox>
                                                                            </EditItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAccess" runat="server" Text='<%# Bind("Access") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Role" HeaderStyle-ForeColor="White" SortExpression="Employee.EmployeeRole"
                                                                            HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtRole" runat="server" Text='<%# Bind("EmployeeRole") %>'></asp:TextBox>
                                                                            </EditItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblRole" runat="server" Text='<%# Bind("EmployeeRole") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" class="bodytextcenter">
                                                                Page&nbsp;&nbsp;
                                                                <asp:ImageButton ID="imbPrevious" runat="server" ImageUrl="~/Images/pre.jpg" OnClick="imbPrevious_Click" />&nbsp;&nbsp;
                                                                <asp:PlaceHolder ID="plcCrntPage" runat="server"></asp:PlaceHolder>
                                                                of
                                                                <asp:Label ID="lblTotalPage" runat="server"></asp:Label>
                                                                &nbsp;&nbsp;<asp:ImageButton ID="imbNext" runat="server" ImageUrl="~/Images/next1.jpg"
                                                                    OnClick="imbNext_Click" />
                                                                <asp:HiddenField ID="hdnPage" runat="server" />
                                                                <asp:HiddenField ID="hdnCount" runat="server" />
                                                                <asp:HiddenField ID="hdnSort" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight">
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="bodytextleft" colspan="3">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td width="5%">
                                                                            Access
                                                                        </td>
                                                                        <td width="5%">
                                                                            Role
                                                                        </td>
                                                                        <td width="10%">
                                                                            Submit?
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="5%">
                                                                            <asp:DropDownList ID="ddlRoles1" runat="server" CssClass="ddlbox"
                                                                                 AutoPostBack="true"
                                                                                 OnSelectedIndexChanged="ddlRoles1_SelectedIndexChanged">
                                                                                <asp:ListItem Value="1">Admin</asp:ListItem>
                                                                                <asp:ListItem Value="2">Employee</asp:ListItem>
                                                                                <asp:ListItem Value="4">Disable</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td width="5%">
                                                                            <asp:DropDownList ID="ddlEmployeeRoles" runat="server" CssClass="ddlbox" DataTextField="RoleDescription" DataValueField="RoleValue">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="left" width="10%">
                                                                            <asp:ImageButton ID="imgAllocate" runat="server" ImageUrl="~/Images/allocate.jpg"
                                                                                OnClick="imgAllocate_Click" ToolTip="Allocate" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                            PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalPopupDiv" class="popupdiv" style="display: none">
                                            <table cellpadding="5" cellspacing="0" border="0" width="350">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                        Result
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        <asp:ValidationSummary CssClass="summaryerrormsg" DisplayMode="List" runat="server"
                                                            ID="valSummary" />
                                                        <asp:Label ID="lblErrMsg" runat="server" CssClass="errormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextcenter">
                                                        <input type="button" value="OK" class="ModalPopupButton" title="Ok" onclick="$find('modalExtnd').hide();" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>
</body>
</html>
