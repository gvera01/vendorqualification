﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web;

#region Change Comments
/************************************************************************************************
 * G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-15) - CREATED
 * 001 - G. Vera 08/03/2012:  VMS Stabilization Release 1.2 (JIRA:VPI-46)
 * 
 ************************************************************************************************/
#endregion

public partial class Admin_PrintConsultantRating : System.Web.UI.Page
{
    //Instantiate the class
    clsRating objRating = new clsRating();
    Common objCommon = new Common();

    /// <summary>
    /// Page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["RatingId"]))
        {
            //Get the details of the vendor id and display the corresponding details to the corresponding controls
            long ratingID = long.Parse(Request.QueryString["RatingId"]);
            DAL.SRFProjectRating objGetRating = objRating.SelectRatingbyId(ratingID);
            if (objGetRating != null)
            {
                clsRegistration objRegistration = new clsRegistration();
                    DAL.UspGetVendorDetailsByIdResult[] obj1 = objRegistration.GetVendor(Convert.ToInt64(objGetRating.FK_VendorID));
                if (obj1.Count() > 0)
                {
                    lblSubcontractorname.Text = obj1[0].Company;
                }
                lblRaterTitle.Text = objGetRating.EmployeeTitle;
                clsAdminLogin objAdminLogin = new clsAdminLogin();
                lblRaterName.Text = objAdminLogin.GetEmployeeName(Convert.ToString(objGetRating.FK_EmployeeNumber));
                lblProjectNumber.Text = objGetRating.ProjectNumber;
                lblProjectName.Text = objGetRating.ProjectName;
                lblRatingDate.Text = Convert.ToDateTime(objGetRating.CreatedDT).ToShortDateString(); //DateTime.Now.ToShortDateString();
                lblSubcontractValue.Text = objGetRating.FinalSubcontract;

                //001
                string Comments = objGetRating.Comments;
                lblComments.Text = Comments;
                lblComments.Rows = Convert.ToInt32(Math.Round(Convert.ToDouble(((lblComments.Text.Length / 85) + 5))));

                //001
                string Scope = objGetRating.SubcontractorScope;
                lblSubcontractorscope.Text = Scope;
                lblSubcontractorscope.Rows = Convert.ToInt32(Math.Round(Convert.ToDouble(((lblSubcontractorscope.Text.Length / 85) + 5))));

                if (objGetRating.InsuranceLevels == null)
                {
                    lblLevels.Text = "Not Available";
                }
                else if (objGetRating.InsuranceLevels == true)
                {
                    lblLevels.Text = "Yes";
                }
                else
                {
                    lblLevels.Text = "No";
                }
                if (objGetRating.Subs_Vendors == null)
                {
                    lblPay.Text = "Not Available";
                }
                else if (objGetRating.Subs_Vendors == true)
                {
                    lblPay.Text = "Yes";
                }
                else
                {
                    lblPay.Text = "No";
                }
                if (objGetRating.SpecialPayment == null)
                {
                    lblPayment.Text = "Not Available";
                }
                else if (objGetRating.SpecialPayment == true)
                {
                    lblPayment.Text = "Yes";
                }
                else
                {
                    lblPayment.Text = "No";
                }
                imgCorporation.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.Cooperation)) + ".jpg";
                imgFileManagement.ImageUrl = (Convert.ToInt32(objGetRating.FieldManagement) == 6) ? "../Images/na.jpg" : "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.FieldManagement)) + ".jpg";
                imgOrderClaim.ImageUrl = (Convert.ToInt32(objGetRating.ChangeOrdersClaims) == 6) ? "../Images/na.jpg" : "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.ChangeOrdersClaims)) + ".jpg"; 
                imgOverallPerformance.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.OverallPerformance)) + ".jpg";
                imgProjectManagement.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.ProjectManagement)) + ".jpg";
                imgQuality.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.QualityOfWork)) + ".jpg";
                imgScheduleControl.ImageUrl = "../Images/" + Convert.ToString(Convert.ToInt32(objGetRating.ScheduleControl)) + ".jpg";

                this.hdrating.Value = objGetRating.FK_VendorID.ToString();

                if (String.IsNullOrEmpty(Request.QueryString["IsView"]))
                {
                    imbPrint.Visible = false;
                    imgback.Visible = false;
                }

            }
        }
    }

    /// <summary>
    /// Go back to teh previous page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgback_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(this.hdrating.Value))
        {
            Response.Redirect("DisplayRating.aspx?VendorId=" + objCommon.encode( this.hdrating.Value));
        }
    }


    /// <summary>
    /// print this form in PDF
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrint_Click(object sender, ImageClickEventArgs e)
    {
        objCommon = new Common();

        string queryString = "RatingId=" + Request.QueryString["RatingId"];

        string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port +
                     "/Admin/PrintConsultantRating.aspx?" + queryString;
        string baseURI = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;

        objCommon.Print2PDF(url, baseURI, true, "Rating");

        //for testing purposes:  Response.Redirect("PrintConsultantRating.aspx?" + queryString);
    }
}