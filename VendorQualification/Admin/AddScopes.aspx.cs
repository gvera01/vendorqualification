﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 03/05/2013: VMS Enhancements Hot Fix Release 1.3.1
 *
 ****************************************************************************************************************/
#endregion

public partial class Admin_AddScopes : System.Web.UI.Page
{
    #region Declaration
    //Initialize the object for classes
    clsTradeInfomation objTradeInformation = new clsTradeInfomation();
    clsScopes objScopes = new clsScopes();

    #endregion

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        //Check whether session is null and if so redirect to the login page
        if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
        {
            //001
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");


            //Check whether session is null and if so redirect to the login page
            if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
            {
                //001
                Response.Redirect("~/Admin/Login.aspx?timeout=1");
            }
            else
            {
                if (Convert.ToString(Session["Access"]) == "2")
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                {
                    AdminMenu.DisplayMenu("Admin");
                }
                //Bind Level1 in first dropdown
                BindDropDownLevel1();
                //To show the selected menu
                mnuMain.DisplayButton(6);
            }
        }
    }

    #endregion

    #region Dropdownlist Events

    /// <summary>
    /// Bind Level2 when the level1 is selected and bind tree to display the sub levels in level1
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlLevel1_SelectedIndexChanged(object sender, EventArgs e)
    {
        HideControls();
        if (ddlLevel1.SelectedIndex > 0)
        {
            // Call the method to bind second drop down based on the first item selected
            BindDropDownLevel2();
            // Call method to bind tree based on the selected item in first drop down
            BindTree();
        }
    }

    /// <summary>
    /// Display the item in dropdown 3 based on the item selected in second dropdownlist 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlLevel2_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Call method to hide controls
        HideControls();
        //If not "select" is not selected bind dropdownlist3
        if (ddlLevel2.SelectedIndex > 0)
        {
            // Call method to bind dropdownlist3
            BindDropDownLevel3();
        }
    }

    #endregion

    #region Button Events

    /// <summary>
    /// Event raise when the button is clicked to add level1, level2, level3
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgAdd_Click(object sender, EventArgs e)
    {
        string Result = string.Empty;
        ImageButton imgAddLevel1 = (ImageButton)sender;
        switch (imgAddLevel1.ID)
        {
            case "imgAdd":
                if (!string.IsNullOrEmpty(txtAddLevel1.Text.Trim()))
                {
                    //Call method to insert level 1 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAddLevel1.Text.Trim(), "-1", GetRoot(txtAddLevel1), "", 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAddLevel2.Text.Trim()))
                {
                    //Call method to insert level 1 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAddLevel2.Text.Trim(), "-1", GetRoot(txtAddLevel2), "", 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAddLevel3.Text.Trim()))
                {
                    //Call method to insert level 1 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAddLevel3.Text.Trim(), "-1", GetRoot(txtAddLevel3), "", 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAddLevel4.Text.Trim()))
                {
                    //Call method to insert level 1 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAddLevel4.Text.Trim(), "-1", GetRoot(txtAddLevel4), "", 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAddLevel5.Text.Trim()))
                {
                    //Call method to insert level 1 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAddLevel5.Text.Trim(), "-1", GetRoot(txtAddLevel5), "", 1, 0);
                }
                //Call method to bind level 1
                BindDropDownLevel1();
                // Call method to bind tree
                BindTree();
                break;
            case "imgAddLevel2":
                if (!string.IsNullOrEmpty(txtAdd2Level1.Text.Trim()))
                {
                    //Call method to insert level 2 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAdd2Level1.Text.Trim(), ddlLevel1.SelectedItem.Value, GetRoot(txtAdd2Level1), ddlLevel1.SelectedItem.Value, 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAdd2Level2.Text.Trim()))
                {
                    //Call method to insert level 2 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAdd2Level2.Text.Trim(), ddlLevel1.SelectedItem.Value, GetRoot(txtAdd2Level2), ddlLevel1.SelectedItem.Value, 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAdd2Level3.Text.Trim()))
                {
                    //Call method to insert level 2 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAdd2Level3.Text.Trim(), ddlLevel1.SelectedItem.Value, GetRoot(txtAdd2Level3), ddlLevel1.SelectedItem.Value, 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAdd2Level4.Text.Trim()))
                {
                    //Call method to insert level 2 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAdd2Level4.Text.Trim(), ddlLevel1.SelectedItem.Value, GetRoot(txtAdd2Level4), ddlLevel1.SelectedItem.Value, 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAdd2Level5.Text.Trim()))
                {
                    //Call method to insert level 2 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAdd2Level5.Text.Trim(), ddlLevel1.SelectedItem.Value, GetRoot(txtAdd2Level5), ddlLevel1.SelectedItem.Value, 1, 0);
                }
                // Call method to bind Level 2
                BindDropDownLevel2();
                // Call metod to bind tree
                BindTree();
                break;
            case "imgLevel3Add":
                if (!string.IsNullOrEmpty(txtAdd3Level1.Text.Trim()))
                {
                    //Call method to insert level 3 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAdd3Level1.Text.Trim(), ddlLevel2.SelectedItem.Value, GetRoot(txtAdd3Level1), ddlLevel1.SelectedItem.Value, 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAdd3Level2.Text.Trim()))
                {
                    //Call method to insert level 3 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAdd3Level2.Text.Trim(), ddlLevel2.SelectedItem.Value, GetRoot(txtAdd3Level2), ddlLevel1.SelectedItem.Value, 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAdd3Level3.Text.Trim()))
                {
                    //Call method to insert level 3 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAdd3Level3.Text.Trim(), ddlLevel2.SelectedItem.Value, GetRoot(txtAdd3Level3), ddlLevel1.SelectedItem.Value, 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAdd3Level4.Text.Trim()))
                {
                    //Call method to insert level 3 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAdd3Level4.Text.Trim(), ddlLevel2.SelectedItem.Value, GetRoot(txtAdd3Level4), ddlLevel1.SelectedItem.Value, 1, 0);
                }
                if (!string.IsNullOrEmpty(txtAdd3Level5.Text.Trim()))
                {
                    //Call method to insert level 3 and return the result for it.
                    Result += "<li>" + objScopes.InsertScopes(txtAdd3Level5.Text.Trim(), ddlLevel2.SelectedItem.Value, GetRoot(txtAdd3Level5), ddlLevel1.SelectedItem.Value, 1, 0);
                }
                // Call method to bind level 3
                BindDropDownLevel3();
                // Call method to bind tree
                BindTree();
                break;
        }
        // Call method to clear text
        ClearText();
        // Call method to hide controls
        HideControls();
        // Display the returned result from the query and show the modal pop up.
        if (!string.IsNullOrEmpty(Result))
        {
            modalExtnd.Show();
            lblMsg.Text = Result;
        }
    }

    /// <summary>
    /// Add Level1, Level2, Level3 then clear the text and display the div again
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgAddMore_Click(object sender, EventArgs e)
    {
        string Result = string.Empty;
        ImageButton imgSaveAddMore = (ImageButton)sender;
        //Add More Level 1
        if (imgSaveAddMore.ID == "imgAddMore")
        {
            if (!string.IsNullOrEmpty(txtAddLevel1.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAddLevel1.Text.Trim(), "-1", GetRoot(txtAddLevel1), "", 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAddLevel2.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAddLevel2.Text.Trim(), "-1", GetRoot(txtAddLevel2), "", 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAddLevel3.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAddLevel3.Text.Trim(), "-1", GetRoot(txtAddLevel3), "", 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAddLevel4.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAddLevel4.Text.Trim(), "-1", GetRoot(txtAddLevel4), "", 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAddLevel5.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAddLevel5.Text.Trim(), "-1", GetRoot(txtAddLevel5), "", 1, 0);
            }
            //Call method to bind dropdownlist level1
            BindDropDownLevel1();
            //Call method to bind tree.
            BindTree();
        }
        // Add more level 2
        else if (imgSaveAddMore.ID == "imgAddMoreLevel2")
        {
            if (!string.IsNullOrEmpty(txtAdd2Level1.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAdd2Level1.Text.Trim(), ddlLevel1.SelectedItem.Value, GetRoot(txtAdd2Level1), ddlLevel1.SelectedItem.Value, 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAdd2Level2.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAdd2Level2.Text.Trim(), ddlLevel1.SelectedItem.Value, GetRoot(txtAdd2Level2), ddlLevel1.SelectedItem.Value, 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAdd2Level3.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAdd2Level3.Text.Trim(), ddlLevel1.SelectedItem.Value, GetRoot(txtAdd2Level3), ddlLevel1.SelectedItem.Value, 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAdd2Level4.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAdd2Level4.Text.Trim(), ddlLevel1.SelectedItem.Value, GetRoot(txtAdd2Level4), ddlLevel1.SelectedItem.Value, 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAdd2Level5.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAdd2Level5.Text.Trim(), ddlLevel1.SelectedItem.Value, GetRoot(txtAdd2Level5), ddlLevel1.SelectedItem.Value, 1, 0);
            }
            //Call method to bind level 2
            BindDropDownLevel2();
            //Call method to bind tree
            BindTree();
        }
        //Add more level3
        else if (imgSaveAddMore.ID == "imgLevel3AddMore")
        {
            if (!string.IsNullOrEmpty(txtAdd3Level1.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAdd3Level1.Text.Trim(), ddlLevel2.SelectedItem.Value, GetRoot(txtAdd3Level1), ddlLevel1.SelectedItem.Value, 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAdd3Level2.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAdd3Level2.Text.Trim(), ddlLevel2.SelectedItem.Value, GetRoot(txtAdd3Level2), ddlLevel1.SelectedItem.Value, 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAdd3Level3.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAdd3Level3.Text.Trim(), ddlLevel2.SelectedItem.Value, GetRoot(txtAdd3Level3), ddlLevel1.SelectedItem.Value, 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAdd3Level4.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAdd3Level4.Text.Trim(), ddlLevel2.SelectedItem.Value, GetRoot(txtAdd3Level4), ddlLevel1.SelectedItem.Value, 1, 0);
            }
            if (!string.IsNullOrEmpty(txtAdd3Level5.Text.Trim()))
            {
                Result += "<li>" + objScopes.InsertScopes(txtAdd3Level5.Text.Trim(), ddlLevel2.SelectedItem.Value, GetRoot(txtAdd3Level5), ddlLevel1.SelectedItem.Value, 1, 0);
            }
            //Call method to bind level 3
            BindDropDownLevel3();
            //Call method to bind tree
            BindTree();
        }
        //Call method to clear all the textbox
        ClearText();
        // Bind the result and Display the result in modalpop up
        if (!string.IsNullOrEmpty(Result))
        {
            modalExtnd.Show();
            lblMsg.Text = Result;
        }
    }

    /// <summary>
    /// Event raises when clicking on the modify link button of level 1
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkModify_Click(object sender, EventArgs e)
    {
        // Call method to hide all the div
        HideControls();
        //Check whether the option in dropdownlist is selected other than "select" text
        if (ddlLevel1.SelectedIndex == 0)
        {
            //If so throw a validation message
            modalExtnd.Show();
            lblMsg.Text = "Select a level1 option to edit";
        }
        else
        {
            //Display the div to update the selected text of level1 and display that text in the textbox in div
            layer2.Style["visibility"] = "visible";
            txtUpdateLevel1.Text = ddlLevel1.SelectedItem.Text.Trim();
            layer2.Style["left"] = "28%";
            layer2.Style["top"] = "255px";
        }
    }

    /// <summary>
    /// Update level1, level2, level3
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgUpdateLevel1_Click(object sender, EventArgs e)
    {
        string Result = string.Empty;
        ImageButton imgUpdate = (ImageButton)sender;
        switch (imgUpdate.ID)
        {
            case "imgUpdateLevel1":
                //Call method to update level1 and assign the returned result to the varaible result
                Result += "<li>" + objScopes.InsertScopes(txtUpdateLevel1.Text.Trim(), "0", GetRoot(txtUpdateLevel1), ddlLevel1.SelectedItem.Value, 2, Convert.ToInt64(ddlLevel1.SelectedItem.Value));
                //Call method to bind level1 with updated Text
                BindDropDownLevel1();
                //Call method to bind tree with updated text
                BindTree();
                break;
            case "imgUpdateLevel2":
                //Call method to update level2 and assign the returned result to the varaible result
                Result += "<li>" + objScopes.InsertScopes(txtUpdateLevel2.Text.Trim(), "0", GetRoot(txtUpdateLevel2), ddlLevel1.SelectedItem.Value, 2, Convert.ToInt64(ddlLevel2.SelectedItem.Value));
                //Call method to bind level2 with updated Text
                BindDropDownLevel2();
                //Call method to bind tree with updated text
                BindTree();
                break;
            case "imgUpdateLevel3":
                //Call method to update level3 and assign the returned result to the varaible result
                Result += "<li>" + objScopes.InsertScopes(txtUpdateLevel3.Text.Trim(), "0", GetRoot(txtUpdateLevel3), ddlLevel1.SelectedItem.Value, 2, Convert.ToInt64(ddlLevel3.SelectedItem.Value));
                //Call method to bind level3 with updated Text
                BindDropDownLevel3();
                //Call method to bind tree with updated text
                BindTree();
                break;
        }
        //Call method to clear text
        ClearText();
        // Call method to hide controls
        HideControls();
        // Check if the result is empty, if not assign the result to the label and display modal pop up
        if (!string.IsNullOrEmpty(Result))
        {
            modalExtnd.Show();
            lblMsg.Text = Result;
        }
    }

    /// <summary>
    /// Event raises delete button is clicked to delete level1, level2, level3 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        LinkButton imgDelete = (LinkButton)sender;
        switch (imgDelete.ID)
        {
            case "lnkDelete":
                //Check if the text "select" is not selected from the list 1
                if (ddlLevel1.SelectedIndex.Equals(0))
                {
                    //If so show the message to select the option in modalpopup control
                    modalExtnd.Show();
                    lblMsg.Text = "<li>" + "Select a level1 option to delete";
                }
                else
                {
                    //Get confirmation from the user whether they are sure to delete the record
                    lblConfirm.Text = "<li>" + "Are you sure you want to delete the record?";
                    modalConfirm.Show();
                    //Maintain value to delete level1
                    hdnLevel.Value = "Level1";
                }
                break;
            case "lnkLevel2Delete":
                //Check if the text "select" is not selected from the list 2
                if (ddlLevel2.SelectedIndex.Equals(0))
                {
                    //If so show the message to select the option in modalpopup control
                    modalExtnd.Show();
                    lblMsg.Text = "<li>" + "Select a level2 option to delete";
                }
                else
                {
                    //Get confirmation from the user whether they are sure to delete the record
                    lblConfirm.Text = "<li>" + "Are you sure you want to delete the record?";
                    modalConfirm.Show();
                    //Maintain value to delete 2
                    hdnLevel.Value = "Level2";
                }
                break;
            case "lnkLevel3Delete":
                //Check if the text "select" is not selected from the list 3
                if (ddlLevel3.SelectedIndex.Equals(0))
                {
                    //If so show the message to select the option in modalpopup control
                    modalExtnd.Show();
                    lblMsg.Text = "<li>" + "Select a level3 option to delete";
                }
                else
                {
                    //Get confirmation from the user whether they are sure to delete the record
                    lblConfirm.Text = "<li>" + "Are you sure you want to delete the record?";
                    modalConfirm.Show();
                    //Maintain value to delete level3
                    hdnLevel.Value = "Level3";
                }
                break;
        }
        //Hide div.            
        HideControls();
    }

    /// <summary>
    /// Event raises when the user click ok button to delete the record 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgOK_Click(object sender, EventArgs e)
    {
        string Result = string.Empty;
        switch (hdnLevel.Value)
        {
            case "Level1": //Get the stored value in the hidden field, if it is level 1
                //Call the method to delete the record and bind the returned result to the varaible "result"
                Result += "<li>" + objScopes.InsertScopes("0", "0", "0", ddlLevel1.SelectedItem.Value, 3, Convert.ToInt64(ddlLevel1.SelectedItem.Value));
                //Call method to bind level 
                BindDropDownLevel1();
                BindDropDownLevel2();
                BindDropDownLevel3();
                //Call method to display the tree with the updated value
                BindTree();
                break;
            case "Level2": //Get the stored value in the hidden field, if it is level 2
                //Call the method to delete the record and bind the returned result to the varaible "result"
                Result += "<li>" + objScopes.InsertScopes("0", "0", "0", ddlLevel1.SelectedItem.Value, 3, Convert.ToInt64(ddlLevel2.SelectedItem.Value));
                //Call method to bind level 2
                BindDropDownLevel2();
                //Call method to display the tree with the updated value
                BindTree();
                break;
            case "Level3": //Get the stored value in the hidden field, if it is level 3
                //Call the method to delete the record and bind the returned result to the varaible "result"
                Result += "<li>" + objScopes.InsertScopes("0", "0", "0", ddlLevel1.SelectedItem.Value, 3, Convert.ToInt64(ddlLevel3.SelectedItem.Value));
                //Call method to bind level 3
                BindDropDownLevel3();
                //Call method to display the tree with the updated value
                BindTree();
                break;
        }
        //Check if it doesn't meet any of the criteria the result will be null else display the result in modalpop up
        if (!string.IsNullOrEmpty(Result))
        {
            modalExtnd.Show();
            lblMsg.Text = Result;
        }
    }

    /// <summary>
    /// Event raises when clicking on Add button under level2 and level3
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkLevel2Add_Click(object sender, EventArgs e)
    {
        //Hide Div
        HideControls();
        LinkButton lnkAdd = (LinkButton)sender;
        //Display div to add level2
        if (lnkAdd.ID.Equals("lnkLevel2Add"))
        {
            //Check whether level1 have anyother selected text other than "Select", If not raise a validation message
            if (ddlLevel1.SelectedIndex == 0)
            {
                modalExtnd.Show();
                lblMsg.Text = "<li>" + "Select a level1 option to add level2";
            }
            else
            {
                //Display level2 div to add in a specific position
                Level2Layer1.Style["visibility"] = "visible";
                Level2Layer1.Style["left"] = "28%";
                Level2Layer1.Style["top"] = "308px";
            }
        }
        //Display div to add level3
        else
        {
            //Check whether level2 have anyother selected text other than "Select", If not raise a validation message
            if (ddlLevel2.SelectedIndex.Equals(0))
            {
                modalExtnd.Show();
                lblMsg.Text = "<li>" + "Select a level2 option to add level3";
            }
            else
            {
                //Display level3 div to add in a specific position
                Level3Layer1.Style["visibility"] = "visible";
                Level3Layer1.Style["left"] = "28%";
                Level3Layer1.Style["top"] = "360px";
            }
        }
    }

    /// <summary>
    /// Event raises to display the div to modify level2 and level3 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkLevel2Modify_Click(object sender, EventArgs e)
    {
        //Hide all divs
        HideControls();
        LinkButton lnkAdd = (LinkButton)sender;
        //Display div to modify level2
        if (lnkAdd.ID.Equals("lnkLevel2Modify"))
        {
            //Check whether level2 have anyother selected text other than "Select", If not raise a validation message
            if (ddlLevel2.SelectedIndex == 0)
            {
                modalExtnd.Show();
                lblMsg.Text = "<li>" + "Select a level2 option to edit";
            }
            else
            {
                //Display level2 div to modify in a specific position
                Level2Layer2.Style["visibility"] = "visible";
                txtUpdateLevel2.Text = ddlLevel2.SelectedItem.Text.Trim();
                Level2Layer2.Style["left"] = "28%";
                Level2Layer2.Style["top"] = "308px";
            }
        }
        //Display div to modify level3
        else
        {
            //Check whether level3 have anyother selected text other than "Select", If not raise a validation message
            if (ddlLevel3.SelectedIndex.Equals(0))
            {
                modalExtnd.Show();
                lblMsg.Text = "<li>" + "Select a level3 option to edit";
            }
            else
            {
                //Display level3 div to modify in a specific position
                Level3Layer2.Style["visibility"] = "visible";
                txtUpdateLevel3.Text = ddlLevel3.SelectedItem.Text.Trim();
                Level3Layer2.Style["left"] = "28%";
                Level3Layer2.Style["top"] = "360px";
            }
        }
    }

    /// <summary>
    ///  Event raises to get confirmation to delete level1
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkLevel2Delete_Click(object sender, EventArgs e)
    {
        //Hide all divs
        HideControls();
        //Check whether level1 have anyother selected text other than "Select", If not raise a validation message
        if (ddlLevel1.SelectedIndex.Equals(0))
        {
            modalExtnd.Show();
            lblMsg.Text = "<li>" + "Select a level2 option to delete";
        }
        else
        {
            //Get confirmation from the user to delete the level1
            lblConfirm.Text = "<li>" + "Are you sure you want to delete the record?";
            modalConfirm.Show();
        }
    }

    //Link to add level1
    protected void lnkAddLevel1_Click(object sender, EventArgs e)
    {
        //Call method to hide all div
        HideControls();
        // Display Div to add level 1 in specific position
        layer1.Style["visibility"] = "visible";
        layer1.Style["left"] = "28%";
        layer1.Style["top"] = "255px";
    }

    #endregion

    #region Other Methods

    /// <summary>
    /// Bind Level1 in dropdown list
    /// </summary>
    private void BindDropDownLevel1()
    {
        ddlLevel1.Items.Clear();
        ddlLevel1.DataSource = objTradeInformation.GetTradeScope(-1);
        ddlLevel1.DataTextField = "Name";
        ddlLevel1.DataValueField = "Pk_ScopeId";
        ddlLevel1.DataBind();

        ListItem lstLevel1 = new ListItem();
        lstLevel1.Text = "-- Select --";
        lstLevel1.Value = "0";
        ddlLevel1.Items.Insert(0, lstLevel1);
    }

    /// <summary>
    /// Bind Level2 in DropDown  list
    /// </summary>
    private void BindDropDownLevel2()
    {
        ddlLevel2.Items.Clear();
        ddlLevel2.DataSource = objTradeInformation.GetTradeScope(Convert.ToInt32(ddlLevel1.SelectedItem.Value));
        ddlLevel2.DataTextField = "Name";
        ddlLevel2.DataValueField = "Pk_ScopeId";
        ddlLevel2.DataBind();

        ListItem lstLevel2 = new ListItem();
        lstLevel2.Text = "-- Select --";
        lstLevel2.Value = "0";
        ddlLevel2.Items.Insert(0, lstLevel2);
    }

    /// <summary>
    /// Bind Level3 in dropdown list
    /// </summary>
    private void BindDropDownLevel3()
    {
        ddlLevel3.Items.Clear();
        ddlLevel3.DataSource = objTradeInformation.GetTradeScope(Convert.ToInt32(ddlLevel2.SelectedItem.Value));
        ddlLevel3.DataTextField = "Name";
        ddlLevel3.DataValueField = "Pk_ScopeId";
        ddlLevel3.DataBind();

        ListItem lstLevel3 = new ListItem();
        lstLevel3.Text = "-- Select --";
        lstLevel3.Value = "0";
        ddlLevel3.Items.Insert(0, lstLevel3);
    }

    /// <summary>
    /// Method to bind tree based on the item selected
    /// </summary>
    private void BindTree()
    {
        if (ddlLevel1.SelectedIndex > 0)
        {
            trvScopes.Nodes.Clear();
            TreeNode tnLevel1 = new TreeNode();
            tnLevel1.Text = ddlLevel1.SelectedItem.Text.Trim();
            TreeNode tnLevel2, tnLevel3;

            //Fetch the item one by one from the list
            foreach (ListItem LstLevel2 in ddlLevel2.Items)
            {
                //to avoid the text "select" from the list
                if (LstLevel2.Value != "0")
                {
                    tnLevel2 = new TreeNode();
                    tnLevel2.Text = LstLevel2.Text.Trim();
                    //Call the method to get the sub levels
                    DAL.UspGetTradeScopesResult[] objResult = objTradeInformation.GetTradeScope(Convert.ToInt32(LstLevel2.Value));
                    //Display sub levels in the tree
                    foreach (DAL.UspGetTradeScopesResult objLevel3 in objResult)
                    {
                        tnLevel3 = new TreeNode();
                        tnLevel3.Text = objLevel3.Name;
                        tnLevel2.ChildNodes.Add(tnLevel3);
                    }
                    tnLevel2.ExpandAll();
                    // Add Childnodes
                    tnLevel1.ChildNodes.Add(tnLevel2);
                    tnLevel1.ExpandAll();
                }
            }
            trvScopes.Nodes.Add(tnLevel1);
            //Expand all display all the leaves
            trvScopes.ExpandAll();
        }
        else
        {
            trvScopes.Nodes.Clear();
        }
    }

    /// <summary>
    /// Get the root value from the textbox
    /// </summary>
    /// <param name="txtLevel"></param>
    /// <returns></returns>
    private string GetRoot(TextBox txtLevel)
    {
        string[] Codes = txtLevel.Text.Trim().Split(' ');
        string Root = string.Empty;
        if (Codes.Length > 0)
        {
            for (int i = 0; i < Codes.Length; i++)
            {
                //Call method to check whether it is number 
                if (isNumeric(Codes[i], System.Globalization.NumberStyles.Integer))
                {
                    // Concat the numbers with spaces
                    Root += Codes[i] + ' ';
                }
            }
        }
        return Root;
    }

    /// <summary>
    /// Hide all Div
    /// </summary>
    private void HideControls()
    {
        layer1.Style["visibility"] = "hidden";
        layer2.Style["visibility"] = "hidden";
        Level2Layer1.Style["visibility"] = "hidden";
        Level2Layer2.Style["visibility"] = "hidden";
        Level3Layer2.Style["visibility"] = "hidden";
        Level3Layer1.Style["visibility"] = "hidden";
    }

    /// <summary>
    /// Method to clear all the text
    /// </summary>
    private void ClearText()
    {
        txtAdd3Level1.Text = string.Empty;
        txtAdd3Level2.Text = string.Empty;
        txtAdd3Level3.Text = string.Empty;
        txtAdd3Level4.Text = string.Empty;
        txtAdd3Level5.Text = string.Empty;
        txtAddLevel1.Text = string.Empty;
        txtAddLevel2.Text = string.Empty;
        txtAddLevel3.Text = string.Empty;
        txtAddLevel4.Text = string.Empty;
        txtAddLevel5.Text = string.Empty;
        txtUpdateLevel1.Text = string.Empty;
        txtUpdateLevel2.Text = string.Empty;
        txtUpdateLevel3.Text = string.Empty;
        txtAdd2Level1.Text = string.Empty;
        txtAdd2Level2.Text = string.Empty;
        txtAdd2Level3.Text = string.Empty;
        txtAdd2Level4.Text = string.Empty;
        txtAdd2Level5.Text = string.Empty;
    }

    /// <summary>
    /// Method to check is numeric
    /// </summary>
    /// <param name="val"></param>
    /// <param name="NumberStyle"></param>
    /// <returns></returns>
    public bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
    {
        Double result;
        return Double.TryParse(val, NumberStyle,
        System.Globalization.CultureInfo.CurrentCulture, out result);
    }

    #endregion

    /// <summary>
    /// 002 - added
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkPrintBlankVQF_Click(object sender, EventArgs e)
    {
        Common objCommon = new Common();
        string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/VQF/PrintVQFForm.aspx?vendorid=0&print=1";
        string baseURI = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;

        //this.Response.Redirect("~/VQF/PrintVQFForm.aspx?vendorid=0&print=1");
        objCommon.Print2PDF(url, baseURI);
    }
}