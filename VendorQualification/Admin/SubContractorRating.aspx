﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SubContractorRating.aspx.cs"
    Inherits="SubContractorRating" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>

    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>

    <link href="../css/Haskell.css" rel="Stylesheet" />
    <link href="../images/StyleSheet.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>

    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function GetValue(i) {
            var str = document.getElementById("<%= hdnPage.ClientID %>");
            str.value = i;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" Visible="true" />
                                    </td>
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <%-- G. Vera 08/04/2014: Changed to "Vendor"--%>
                                                <td class="formhead">
                                                    Vendor Rating
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="tdButtons" runat="server" style="display: none">
                                                    <asp:ImageButton ID="imgAdd" runat="server" ImageUrl="~/Images/addnew.jpg" OnClick="imgAdd_Click"
                                                        ToolTip="Add New Rating" />
                                                    &nbsp;
                                                    <asp:ImageButton ID="imgExists" runat="server" ImageUrl="~/Images/viewexist.jpg"
                                                        OnClick="imgExists_Click" ToolTip="View Existing" />
                                                    <asp:HiddenField ID="hdnClick" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="Vendortd" runat="server" style="display: none">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                        <tr>
                                                            <td class="formtdlt" width="41%" valign="top">
                                                                Vendor Name from Vendor Database
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtVendor" runat="server" CssClass="ajaxtxtbox" MaxLength="60"></asp:TextBox>
                                                                <div id="listPlacement" style="height: 100px; overflow: scroll;">
                                                                </div>
                                                                <Ajax:AutoCompleteExtender ID="autoComplete1" runat="server" BehaviorID="AutoCompleteEx"
                                                                    CompletionListElementID="listPlacement" EnableCaching="true" FirstRowSelected="True"
                                                                    MinimumPrefixLength="1" OnClientItemSelected="" ServiceMethod="GetCompletionList"
                                                                    ServicePath="~/AutoComplete.asmx" TargetControlID="txtVendor" CompletionSetCount="10">
                                                                    <Animations>
                                                            <OnShow>
                                                              <Sequence>
                                                                <%-- Make the completion list transparent and then show it --%>
                                                                <OpacityAction Opacity="0" />
                                                                <HideAction Visible="true" />
                                                            
                                                                 <%--Cache the original size of the completion list the first time
                                                                  the animation is played and then set it to zero --%>
                                                                  <ScriptAction Script="
                                                                // Cache the size and setup the initial size
                                                                var behavior = $find('AutoCompleteEx');
                                                                if (!behavior._height) {
                                                                    var target = behavior.get_completionList();
                                                                    behavior._height = target.offsetHeight - 2;
                                                                    target.style.height = '0px';
                                                                  }" />
                                                            
                                                            <%-- Expand from 0px to the appropriate size while fading in --%>
                                                                <Parallel Duration=".4">
                                                                <FadeIn />
                                                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                                                                </Parallel>
                                                                </Sequence>
                                                                 </OnShow>
                                                                  <OnHide>
                                                          <%-- Collapse down to 0px and fade out --%>
                                                                 <Parallel Duration=".4">
                                                                 <FadeOut />
                                                                 <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                                                                 </Parallel>
                                                                 </OnHide>
                                                                    </Animations>
                                                                </Ajax:AutoCompleteExtender>
                                                                <asp:HiddenField ID="hdnMatch" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtdlt">
                                                                Vendor Number from E1 / JDE
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:TextBox ID="txtVendorNumber" runat="Server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td class="formtdrt">
                                                                <asp:ImageButton ID="imbSearch" runat="server" ImageUrl="~/Images/search.jpg" OnClick="imbSearch_Click"
                                                                    ToolTip="Search" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <asp:Label ID="lblmsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="ContentTd" runat="server" style="display: none" width="100%">
                                                    <asp:UpdatePanel ID="updPanel" runat="server">
                                                        <ContentTemplate>
                                                            <table width="100%" cellpadding="3" cellspacing="0" id="tblContent" runat="server">
                                                                <tr>
                                                                    <td>
                                                                        <asp:HiddenField ID="hdnCount" runat="server" />
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td class="bodytextleft" valign="middle">
                                                                                    Total Number Of Records:
                                                                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                                                </td>
                                                                                <td class="bodytextright" valign="middle">
                                                                                    Number Of Records Per Page:
                                                                                </td>
                                                                                <td width="5%">
                                                                                    <asp:DropDownList ID="ddlNumber" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlNumber_SelectedIndexChanged"
                                                                                        CssClass="ddlbox">
                                                                                        <asp:ListItem>10</asp:ListItem>
                                                                                        <asp:ListItem>20</asp:ListItem>
                                                                                        <asp:ListItem>30</asp:ListItem>
                                                                                        <asp:ListItem>40</asp:ListItem>
                                                                                        <asp:ListItem>50</asp:ListItem>
                                                                                        <%-- Included by SGS as Per Issue No 5 --%>
                                                                                        <asp:ListItem>100</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="5%">
                                                                        <asp:GridView ID="grdVendor" runat="server" CellPadding="5" GridLines="none" Width="100%"
                                                                            AllowSorting="true" CellSpacing="1" AutoGenerateColumns="False" OnRowDataBound="grdVendor_RowDataBound"
                                                                            OnSorting="grdVendor_Sorting">
                                                                            <RowStyle CssClass="listone" />
                                                                            <AlternatingRowStyle CssClass="listtwo" />
                                                                            <HeaderStyle CssClass="listheading" />
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White">
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="rdbVendor" runat="server" AutoPostBack="True" OnCheckedChanged="rdbSelect_CheckedChanged" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Vendor Name" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                                    ItemStyle-CssClass="gridpad" SortExpression="VendorName">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblVendorName" runat="server" CssClass="bodytextbold" Text='<%# Bind("Company") %>'></asp:Label><br />
                                                                                        <asp:HiddenField ID="hdnVendorId" runat="server" Value='<%# Bind("PK_VendorId") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Vendor Number" HeaderStyle-ForeColor="White" HeaderStyle-CssClass="gridpad"
                                                                                    ItemStyle-CssClass="gridpad" SortExpression="VendorNumber">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblVendorNumber" runat="server" Text='<%# Bind("VendorNumber") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Average Overall Rating" HeaderStyle-ForeColor="White"
                                                                                    HeaderStyle-CssClass="gridpad" ItemStyle-CssClass="gridpad" SortExpression="SubcontractorRating">
                                                                                    <ItemTemplate>
                                                                                        <Ajax:Rating ID="rtgStar" runat="server" CssClass="ratingStar" CurrentRating='<%# Convert.ToInt32(Math.Round(Convert.ToDouble(Eval("SubContractorRating")))) %>'
                                                                                            EmptyStarCssClass="Empty" FilledStarCssClass="Filled" MaxRating="5" ReadOnly="true"
                                                                                            StarCssClass="ratingItem" WaitingStarCssClass="Saved">
                                                                                        </Ajax:Rating>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdheight">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bodytextcenter">
                                                                        Page&nbsp;&nbsp;
                                                                        <asp:ImageButton ID="imbPrevious" runat="server" ImageUrl="~/Images/pre.jpg" OnClick="imbPrevious_Click" />&nbsp;&nbsp;
                                                                        <asp:PlaceHolder ID="plcCrntPage" runat="server"></asp:PlaceHolder>
                                                                        of
                                                                        <asp:Label ID="lblTotalPage" runat="server"></asp:Label>
                                                                        &nbsp;&nbsp;<asp:ImageButton ID="imbNext" runat="server" ImageUrl="~/Images/next1.jpg"
                                                                            OnClick="imbNext_Click" />
                                                                        <asp:HiddenField ID="hdnPage" runat="server" />
                                                                        <asp:HiddenField ID="hdnCurrentPage" runat="server" />
                                                                        <asp:HiddenField ID="hdnSort" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdheight">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="bodytextcenter">
                                                                        <asp:ImageButton ID="imbSearchs" runat="server" ImageUrl="~/Images/search.jpg" OnClick="imbSearchs_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                            PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalPopupDiv" class="popupdiv" style="display: none">
                                            <table cellpadding="5" cellspacing="0" border="0" width="350">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                        Message
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        <asp:Label ID="lblmsg1" runat="server" Text="<Li> Please select at least one vendor</Li>"
                                                            CssClass="summaryerrormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextcenter">
                                                        <input type="button" value="OK" class="ModalPopupButton" title="Ok" onclick="$find('modalExtnd').hide();" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd1" runat="server" TargetControlID="lblHidden1"
                                            PopupControlID="modalPopupDiv1" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalPopupDiv1" class="popupspc" style="display: none">
                                            <table cellpadding="5" cellspacing="0" border="0" width="400" align="center">
                                                <tr>
                                                    <td class="searchhdrbarbold" colspan="2">
                                                        Message
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight" colspan="2">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="errormsg left_border" colspan="2">
                                                        <asp:Label ID="msg" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ModalPopupTd" colspan="2">
                                                        <asp:ImageButton ID="ImageButton1" ToolTip="Yes" runat="server" ImageUrl="~/Images/yes.jpg"
                                                            TabIndex="4" />
                                                        &nbsp;
                                                        <asp:ImageButton ID="ImbNo" ToolTip="No" runat="server" ImageUrl="~/Images/no.jpg"
                                                            TabIndex="5" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalConfirm" runat="server" TargetControlID="hdnModal"
                                            PopupControlID="modalDiv" BackgroundCssClass="popupbg" CancelControlID="imbCancel"
                                            RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="hdnModal" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalDiv" class="popupdiv" style="display: none">
                                            <table cellpadding="5" cellspacing="0" border="0" width="350">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="Td1">
                                                        Confirm
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl">
                                                        <asp:Label ID="lblConfirm" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="popupdivcl bodytextcenter">
                                                        <asp:ImageButton ID="imbOk" runat="server" ImageUrl="~/Images/ok.jpg" />
                                                        &nbsp;
                                                        <asp:ImageButton ID="imbCancel" runat="server" ImageUrl="~/Images/cancel.jpg" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight popupdivcl">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
