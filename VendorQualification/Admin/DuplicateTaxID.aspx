﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DuplicateTaxID.aspx.cs" Inherits="Admin_DuplicateTaxID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <script src="../Script/jquery.js" type="text/javascript"></script>
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <script language="javascript" type="text/javascript">
        window.history.go(1);
        function ChangeUpper(txt) {
            document.getElementById(txt).value = document.getElementById(txt).value.toUpperCase();
        } 
    </script>
    <style type="text/css">
        body
        {
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbsubmit" defaultfocus="txtCompanyName">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftmenutd">
                                        <Haskell:AdminMenu ID="AdminMenu" runat="server" />
                                    </td>
                                    <td class="contenttd">
                                        <Haskell:AdminMaintananceMenu ID="mnuMain" runat="server" />
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextbold_right">
                                                    <asp:LinkButton ID="inkprofilereset" CssClass="scopelink" runat="server" Text="Profile Reset"
                                                        PostBackUrl="~/Admin/ResetPassword.aspx"></asp:LinkButton>&nbsp;|&nbsp;
                                                    <asp:LinkButton ID="lnkAllowDuplicateTaxID" CssClass="scopelink" runat="server" Text="Allow Duplicate TaxID"
                                                        PostBackUrl="~/Admin/DuplicateTaxID.aspx"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;&nbsp;
                                                </td>
                                                <tr>
                                                    <td align="center">
                                                        <asp:UpdatePanel ID="upnlPassword" runat="server">
                                                            <ContentTemplate>
                                                                <table width="100%" class="searchtableclass" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="searchhdrbarbold" colspan="2">
                                                                            DuplicateTax ID
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight" colspan="2">
                                                                        </td>
                                                                    </tr>
                                                                    <%-- <tr>
                                                                    <td class="formtdlt" width="35%" valign="top">
                                                                        &nbsp;</td>
                                                                    <td class="formtdrt">
                                                                                                                    <asp:DropDownList ID="ddlName" runat="server" CssClass="ddlbox" AutoPostBack="True"
                                                                            OnSelectedIndexChanged="ddlName_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                        <asp:CustomValidator ID="cusVendorName" Display="None" runat="server" OnServerValidate="cusVendorName_ServerValidate"></asp:CustomValidator></td>
                                                                </tr>--%>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            <span class="mandatorystar">*</span>Company Name
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:TextBox CssClass="txtboxupper" ID="txtCompanyName" runat="server" TabIndex="1"
                                                                                MaxLength="60"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="reqvCompanyName" ControlToValidate="txtCompanyName"
                                                                                Display="None" ValidationGroup="NewCompany" runat="server" EnableClientScript="false"
                                                                                ErrorMessage="Please enter company name">
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt" valign="middle" width="40%">
                                                                            <span class="mandatorystar">*</span>Country
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:DropDownList ID="ddlCountry" runat="server" Width="150px" CssClass="ddlbox"
                                                                                OnChange="javascript:ShowHide(this.value,'trUSA','trOther')">
                                                                                <asp:ListItem Text="USA" Value="USA"></asp:ListItem>
                                                                                <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="trUSA" runat="server">
                                                                        <td class="formtdlt">
                                                                            <span class="mandatorystar">*</span>U.S. Tax ID
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:TextBox CssClass="txtboxtosmall" ID="txtTaxId" runat="server" TabIndex="2" MaxLength="2"></asp:TextBox>&nbsp;-
                                                                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTaxId"
                                                                                FilterType="Numbers">
                                                                            </Ajax:FilteredTextBoxExtender>
                                                                            <asp:TextBox CssClass="txtboxmedium" ID="txtTaxId1" runat="server" TabIndex="3" MaxLength="7"></asp:TextBox>
                                                                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtTaxId1"
                                                                                FilterType="Numbers">
                                                                            </Ajax:FilteredTextBoxExtender>
                                                                            <asp:CustomValidator ID="Custax" runat="server" Display="None" ValidationGroup="NewCompany"
                                                                                EnableClientScript="false" OnServerValidate="Custax_ServerValidate"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr id="trOther" runat="server">
                                                                        <td class="formtdlt" valign="middle">
                                                                            <span class="mandatorystar">*</span>Business ID
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:TextBox ID="txtBusinessTax" runat="server" MaxLength="50" CssClass="txtbox"></asp:TextBox>
                                                                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtBusinessTax"
                                                                                FilterType="Numbers,UppercaseLetters, LowercaseLetters">
                                                                            </Ajax:FilteredTextBoxExtender>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            <span class="mandatorystar">*</span>First Name
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:TextBox CssClass="txtbox" ID="txtFirstName" runat="server" TabIndex="4" MaxLength="50"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="reqvFirstName" ControlToValidate="txtFirstName" Display="None"
                                                                                EnableClientScript="false" ValidationGroup="NewCompany" runat="server" ErrorMessage="Please enter first name">
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            <span class="mandatorystar">*</span>Last Name
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:TextBox CssClass="txtbox" ID="txtLastName" runat="server" TabIndex="5" MaxLength="50"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="reqvLastName" ControlToValidate="txtLastName" Display="None"
                                                                                ValidationGroup="NewCompany" runat="server" ErrorMessage="Please enter last name"
                                                                                EnableClientScript="false">
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            <span class="mandatorystar">*</span>E-mail
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:TextBox CssClass="txtbox" ID="txtEmailNew" runat="server" TabIndex="6" MaxLength="100"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="reqEmailNew" ControlToValidate="txtEmailNew" Display="None"
                                                                                ValidationGroup="NewCompany" runat="server" ErrorMessage="Please enter e-mail"
                                                                                EnableClientScript="false">
                                                                            </asp:RequiredFieldValidator>
                                                                            <asp:RegularExpressionValidator ID="regvEmail" ControlToValidate="txtEmailNew" Display="None"
                                                                                ValidationGroup="NewCompany" runat="server" ErrorMessage="Please enter valid e-mail"
                                                                                EnableClientScript="false" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtdlt">
                                                                            <span class="mandatorystar">*</span>Password
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:TextBox CssClass="txtbox" ID="txtPasswordNew" runat="server" TextMode="Password"
                                                                                TabIndex="7" MaxLength="20"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="reqPasswordNew" ControlToValidate="txtPasswordNew"
                                                                                Display="None" ValidationGroup="NewCompany" runat="server" ErrorMessage="Please enter password"
                                                                                EnableClientScript="false">
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td class="formtdrt">
                                                                            <asp:ImageButton ID="imbsubmit" runat="server" ImageUrl="~/images/submit.jpg" ValidationGroup="NewCompany"
                                                                                OnClick="imbsubmit_Click" ToolTip="Submit" CausesValidation="false" TabIndex="8" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                                                                PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                                                            </Ajax:ModalPopupExtender>
                                                                            <asp:HiddenField ID="lblHidden" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div id="modalPopupDiv" class="popupdiv" style="display: none">
                                                                                <table cellpadding="5" cellspacing="0" border="0" width="350">
                                                                                    <tr>
                                                                                        <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                                                            Message
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdheight popupdivcl">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="popupdivcl">
                                                                                            <asp:Label ID="lblErrMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdheight popupdivcl">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextcenter popupdivcl">
                                                                                            <input type="button" value="OK" title="Ok" class="ModalPopupButton" onclick="$find('modalExtnd').hide();" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdheight popupdivcl">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                    </td>
                                                </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>
    <!--002-Sooraj-->
    <script type="text/javascript" language="javascript">
        function ShowHide(val, usaPnl, otherPnl) {

            var trOther = document.getElementById(otherPnl);
            var trUSA = document.getElementById(usaPnl);

            if (val == 'Other') {
                trUSA.style.display = "none";
                trOther.style.display = "";
            }
            else {
                trOther.style.display = "none";
                trUSA.style.display = "";
            }
        }
    </script>
</body>
</html>
