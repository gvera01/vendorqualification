﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 06/20/2012: VMS Stabilization Release 1.0 Item 4.9.1 (JIRA:VPI-37)
 *
 ****************************************************************************************************************/
#endregion
public partial class Admin_print : System.Web.UI.Page
{
    clsRating objRating = new clsRating();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
            Panel pnlSubContractor = (Panel)Session["SubContractorRating"];
            Panel pnlRating = (Panel)Session["RatingPanel"];
            HtmlContainerControl spnSubcontractorDetails = (HtmlContainerControl)Session["SubContractorPanel"];
          
           
            foreach (Control ctrl in spnSubcontractorDetails.Controls)
            {
                if (ctrl.ID == "lblSubcontractorName")
                {
                    lblSubcontractorname.Text = ((Label)ctrl).Text;
                }
                if (ctrl.ID == "lblRatingDate")
                {
                    lblRatingDate.Text = ((Label)ctrl).Text;
                }

                if (ctrl.ID == "ddlProjectNumber")
                {
                    //001 - Changed from dorp down to textbox
                    TextBox ddlProject = (TextBox)ctrl;
                    lblProjectNumber.Text = (!String.IsNullOrEmpty(ddlProject.Text)) ? ddlProject.Text : string.Empty;
                }

                if (ctrl.ID == "lblProjectNumber")
                {
                    lblProjectNumber.Text = ((Label)ctrl).Text;
                }

                if (ctrl.ID == "lblProjectName")
                {
                    lblProjectName.Text = ((Label)ctrl).Text;
                }
                if (ctrl.ID == "lblRaterName")
                {
                    lblRaterName.Text = ((Label)ctrl).Text;
                }
                if (ctrl.ID == "lblRaterTitle")
                {
                    lblRaterTitle.Text = ((Label)ctrl).Text;
                }
                if (ctrl.ID == "txtFinalSubcontract")
                {
                    lblFinalSubcontractor.Text = ((TextBox)ctrl).Text;
                }
                if (ctrl.ID == "txtSubContractorScope")
                {
                    txtSubContractorScope.Text = ((TextBox)ctrl).Text;
                }
            }

            foreach (Control ctrl in pnlSubContractor.Controls)
            {
                if (ctrl.ID == "lblSubcontractorName")
                {
                    lblSubcontractorname.Text = ((Label)ctrl).Text;
                    
                }
                if (ctrl.ID == "lblRatingDate")
                {
                    lblRatingDate.Text = ((Label)ctrl).Text;
                }

                if (ctrl.ID == "lblProjectNumber")
                {
                    lblProjectNumber.Text = ((Label)ctrl).Text;
                }


                if (ctrl.ID == "lblProjectName")
                {
                    lblProjectName.Text = ((Label)ctrl).Text;
                }
                if (ctrl.ID == "lblRaterName")
                {
                    lblRaterName.Text = ((Label)ctrl).Text;
                }
                if (ctrl.ID == "lblRaterTitle")
                {
                    lblRaterTitle.Text = ((Label)ctrl).Text;
                }
                if (ctrl.ID == "txtSubContractorScope")
                {
                    txtSubContractorScope.Text = ((TextBox)ctrl).Text;
                }
            }

            foreach (Control ctrl in pnlRating.Controls)
            {
                if (ctrl.ID == "rtgOverallPerformance")
                {
                    int Overall = ((AjaxControlToolkit.Rating)ctrl).CurrentRating;
                    imgOverall.ImageUrl = @"../Images/" + Overall + ".jpg";
                }
                if (ctrl.ID == "rtgSafety")
                {
                    int Overall = ((AjaxControlToolkit.Rating)ctrl).CurrentRating;
                    imgSafety.ImageUrl = @"../Images/" + Overall + ".jpg";
                }
                if (ctrl.ID == "rtgProjectManagement")
                {
                    int Overall = ((AjaxControlToolkit.Rating)ctrl).CurrentRating;
                    imgProject.ImageUrl = @"../Images/" + Overall + ".jpg";
                }
                if (ctrl.ID == "rtgQualityOfWork")
                {
                    int Overall = ((AjaxControlToolkit.Rating)ctrl).CurrentRating;
                    imgQuality.ImageUrl = @"../Images/" + Overall + ".jpg";
                }
                if (ctrl.ID == "rtgFileManagement")
                {
                    int Overall = ((AjaxControlToolkit.Rating)ctrl).CurrentRating;
                    imgField.ImageUrl = @"../Images/" + Overall + ".jpg";
                }
                if (ctrl.ID == "rtgFinancialCapability")
                {
                    int Overall = ((AjaxControlToolkit.Rating)ctrl).CurrentRating;
                    imgFinancial.ImageUrl = @"../Images/" + Overall + ".jpg";
                }
                if (ctrl.ID == "rtgChange")
                {
                    int Overall = ((AjaxControlToolkit.Rating)ctrl).CurrentRating;
                    imgOrders.ImageUrl = @"../Images/" + Overall + ".jpg";
                }
                if (ctrl.ID == "rtgCorporation")
                {
                    int Overall = ((AjaxControlToolkit.Rating)ctrl).CurrentRating;
                    imgCorporation.ImageUrl = @"../Images/" + Overall + ".jpg";
                }
                if (ctrl.ID == "rtgSceduleControl")
                {
                    int Overall = ((AjaxControlToolkit.Rating)ctrl).CurrentRating;
                    imgSchedule.ImageUrl = @"../Images/" + Overall + ".jpg";
                }
                if (ctrl.ID == "rblpay")
                {
                    rblpay.SelectedIndex = ((RadioButtonList)ctrl).SelectedIndex;
                }
                if (ctrl.ID == "rblLevels")
                {
                    rblLevels.SelectedIndex = ((RadioButtonList)ctrl).SelectedIndex;
                }
                if (ctrl.ID == "rblPayment")
                {
                    rblPayment.SelectedIndex = ((RadioButtonList)ctrl).SelectedIndex;
                }



                if (ctrl.ID == "txtComments")
                {
                    txtComments.Text = ((TextBox)ctrl).Text;
                }
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script language='javascript'>");
            sb.Append(@"window.print();");

            sb.Append(@"</script>");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "JSCR", sb.ToString(), false);

        }
    }
}
