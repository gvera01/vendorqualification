﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 
 *
 ****************************************************************************************************************/
#endregion

public partial class Common_TermsConditions : System.Web.UI.Page
{
    clsRegistration oRegistration = new clsRegistration();
    static Int32 messageId;

    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");
            
            //Load terms and contitions
            DataSet ds = new DataSet();

            ds = oRegistration.GetTermsAndConditionsAgreement("TermsAndConditions");

            messageId = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0]);
            txtTermsConditionsTitle.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
            txtTermsConditionsMessage.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString();
        }
    }
    protected void imbSubmit_Click(object sender, ImageClickEventArgs e)
    {
        if (txtTermsConditionsTitle.Text.ToString() != String.Empty && txtTermsConditionsMessage.Text.ToString() != string.Empty)
        {
            //update accepted
            bool bUpdated = oRegistration.ChangeHasAcceptedTermsAgreement(txtTermsConditionsTitle.Text.ToString(), txtTermsConditionsMessage.Text.ToString(), chkTermsConditionsForceRenew.Checked);

            if (bUpdated)
            {
                lblMessage.Text = "Terms and Conditions have been updated.";
            }
            else 
            {
                //throw an exception
                throw new System.ArgumentException("Vendor Management System - Error", "An exception occurred while an administrator was modifying Terms & Conditions.");
            }
        }
    }

    protected void imbClose_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Admin/Login.aspx");
    }
}
