﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net.Mail;
using VMSDAL;
#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 07/25/2012: VMS Stabilization Release 1.2 (JIRA:VPI-19)
 * 003 Sooraj Sudhakaran.T 10/04/2013: VMS Enhancements Phase 3 (International Tax ID/ Business ID)
 * 004 G. Vera 11/19/2014: Bug fixes relate to vendor numbers and JDE flag
 * 005 G. Vera 10/10/2017:  Removed the vendor status filter from the stored procedure that returns the data for
 *                          this vendor match functionallity.  Therefore, need to adjust how the vendor status
 *                          gets updated
 ****************************************************************************************************************/
#endregion

public partial class Admin_VendorMatch : System.Web.UI.Page
{
    //Instantiate the class
    clsVendorMatch objVendorMatch = new clsVendorMatch();
    RiskEvaluation riskEvaluation = new RiskEvaluation();
    Common objCommon = new Common();
    /// <summary>
    /// Call this method before the page loads
    /// </summary>
    /// <param name="e"></param>

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        //Create controls dynamically
        string str = Request.Form["__EVENTTARGET"];
        if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page"]))) && str.ToUpperInvariant() != "DDLNUMBER")
        {

            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {

                    LinkButton lnk = new LinkButton();
                    //  Literal ltr = new Literal();
                    lnk.Click += new EventHandler(lnk_Click);
                    //  if ((i == Convert.ToInt32(Session["Page"])) || (i == (Convert.ToInt32(hdnPage.Value) + 5)))
                    //{ ltr.Text = string.Empty; }
                    //else{
                    //    ltr.Text = ",";
                    //}

                    lnk.Text = Convert.ToString(i);
                    if (i == Convert.ToInt32(hdnPage.Value))
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else { lnk.CssClass = "lnkcolor"; }
                    plcCrntPage.Controls.Add(lnk);
                    lnk.Text = Convert.ToString(i);

                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text + ")");
                    // plcCrntPage.Controls.Add(ltr);
                }
            }
        }

    }

    /// <summary>
    /// page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (!Page.IsPostBack)
        {

            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            //Check for the session variable if null redirect to the login page
            if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
            }
            else
            {
                if (Session["RISKIDLOGOUT"] != null && Session["LOCKEDBYLOGOUT"] != null)
                {
                    Int64 Lockedby = riskEvaluation.LockebByUser(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                    if (Lockedby == Convert.ToInt64(Session["LOCKEDBYLOGOUT"]))
                    {
                        riskEvaluation.UpdateStatus(Convert.ToInt64(Session["RISKIDLOGOUT"]));
                        Session["RISKIDLOGOUT"] = null;
                    }
                }
                mnuMain.DisplayButton(2);
                if (Convert.ToString(Session["Access"]) == "2")
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                {
                    AdminMenu.DisplayMenu("Admin");
                }
                hdnPage.Value = "1";
                Session["DataSet"] = null;
                Session["DataSetCount"] = null;
                ddlNumber.SelectedIndex = 0;
                hdnCount.Value = Convert.ToString(objVendorMatch.GetVendorMatchCount());
                lblTotal.Text = Convert.ToString(hdnCount.Value);
                int page = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToDouble(hdnCount.Value) / Convert.ToDouble(ddlNumber.SelectedItem.Value))));

                Session["Page"] = Convert.ToString(page);
                lblTotalPage.Text = Convert.ToString(Session["Page"]);
                //lblCurrentPage.Text= Convert.ToString(hdnPage.Value);

                if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
                {
                    imbNext.Visible = false;
                }
                else
                {
                    imbNext.Visible = true;
                }
                if ((Convert.ToString(hdnPage.Value) == "1") || (Convert.ToString(hdnPage.Value) == "0"))
                {
                    imbPrevious.Visible = false;
                }
                else
                {
                    imbPrevious.Visible = true;
                }
                if (Convert.ToInt32(hdnPage.Value) == Convert.ToInt32(Session["Page"]))
                {
                    imbNext.Visible = false;
                }
                lblTotalPage.Text = Convert.ToString(Session["Page"]);
                //Call method to bind gridview
                Bind();
            }

        }
        //Retain the dynamically created controls across postback
        string strPageRequest;
        strPageRequest = Request.Form["__EVENTTARGET"];
        if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page"]))) && (string.IsNullOrEmpty(strPageRequest) || (strPageRequest.ToLower() != "ddlnumber")))
        {
            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {
                    LinkButton lnk = new LinkButton();
                    Literal ltr = new Literal();
                    lnk.Click += new EventHandler(lnk_Click);

                    ltr.Text = ",";

                    lnk.Text = Convert.ToString(i);
                    if (i == Convert.ToInt32(hdnPage.Value))
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else { lnk.CssClass = "lnkcolor"; }
                    plcCrntPage.Controls.Add(lnk);
                    lnk.Text = Convert.ToString(i);

                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ")");
                    //plcCrntPage.Controls.Add(ltr);  
                }
            }
        }
    }

    /// <summary>
    /// Redirect to the page to link number
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    void lnk_Click(object sender, EventArgs e)
    {
        LinkButton Currentlnk = new LinkButton();
        Currentlnk = (LinkButton)sender;
        hdnPage.Value = Currentlnk.Text.Trim();

        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == Currentlnk.Text.Trim())
                {
                    lnk1.CssClass = "Selectlnkcolor";

                }
                else { lnk1.CssClass = "lnkcolor"; }
            }

        }
        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if (hdnPage.Value == "1")
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }

        //call the method to bind the gridview
        Bind();

    }

    /// <summary>
    /// Display the number of records per page as per the selected value in te gridview
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    {

        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToDouble(hdnCount.Value) / Convert.ToDouble(ddlNumber.SelectedItem.Value))));
        Session["Page"] = Convert.ToString(page);
        hdnPage.Value = "1";

        //Display previous and next button
        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if ((Convert.ToString(hdnPage.Value) == "1") || (Convert.ToString(hdnPage.Value) == "0"))
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        if (Convert.ToInt32(hdnPage.Value) == Convert.ToInt32(Session["Page"]))
        {
            imbNext.Visible = false;
        }

        //Create controls dynamically
        plcCrntPage.Controls.Clear();
        for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
        {
            if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
            {
                LinkButton lnk = new LinkButton();
                Literal ltr = new Literal();
                lnk.Click += new EventHandler(lnk_Click);

                ltr.Text = ",";

                lnk.Text = Convert.ToString(i);
                if (i == Convert.ToInt32(hdnPage.Value))
                {
                    lnk.CssClass = "Selectlnkcolor";
                }
                else { lnk.CssClass = "lnkcolor"; }
                plcCrntPage.Controls.Add(lnk);
                lnk.Text = Convert.ToString(i);

                lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ")");
                //plcCrntPage.Controls.Add(ltr);
            }
        }
        lblTotalPage.Text = Convert.ToString(Session["Page"]);
        // Call the method to bind the gridview
        Bind();

        //Apply styles for the selected control
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";

                }
                else { lnk1.CssClass = "lnkcolor"; }
            }

        }
    }

    //Display the record in the next page
    protected void imbNext_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) + 1);

        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";

                }
                else { lnk1.CssClass = "lnkcolor"; }
            }

        }
        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if ((Convert.ToString(hdnPage.Value) == "1") || (Convert.ToString(hdnPage.Value) == "0"))
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        if (Convert.ToInt32(hdnPage.Value) == Convert.ToInt32(Session["Page"]))
        {
            imbNext.Visible = false;
        }
        lblTotalPage.Text = Convert.ToString(Session["Page"]);

        //Bind the record to the gridview
        Bind();
    }

    //Bind teh record in the gridview
    private void Bind()
    {
        //Call method to binf gridview
        grdVendor.DataSource = objVendorMatch.GetVendorMatch(Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.SelectedItem.Text.Trim()));
        grdVendor.DataBind();
        hdnCount.Value = Convert.ToString(objVendorMatch.GetVendorMatchCount());
        lblTotal.Text = hdnCount.Value;

        //002 - Added this
        if (grdVendor.Rows.Count > 1) imbSubmitandnext.Visible = true;
        else imbSubmitandnext.Visible = false;

        if (grdVendor.Rows.Count > 0)
        { tblVendor.Style["Display"] = ""; }
        else
        {
            tblVendor.Style["Display"] = "none";
            lblmsgInfo.Text = "No records found";
        }
    }

    //Display record in the previous page
    protected void imbPrevious_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) - 1);
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";

                }
                else { lnk1.CssClass = "lnkcolor"; }
            }

        }

        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if ((Convert.ToString(hdnPage.Value) == "1") || (Convert.ToString(hdnPage.Value) == "0"))
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        if (Convert.ToInt32(hdnPage.Value) == Convert.ToInt32(Session["Page"]))
        {
            imbNext.Visible = false;
        }
        lblTotalPage.Text = Convert.ToString(Session["Page"]);

        //Call method to bind gridview
        Bind();
    }

    //Check only one checkbox in the gridview list
    protected void rboVendor_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow oldrow in grdVendor.Rows)
        {
            ((RadioButton)oldrow.FindControl("rboVendor")).Checked = false;
        }

        //Set the new selected row
        RadioButton rb = (RadioButton)sender;
        GridViewRow row = (GridViewRow)rb.NamingContainer;
        ((RadioButton)row.FindControl("rboVendor")).Checked = true;
    }

    // //Check only one checkbox in the gridview list
    // protected void rboVendorDetails_CheckedChanged(object sender, EventArgs e)
    // {
    //     foreach (GridViewRow oldrow in grdVendorDetails.Rows)
    //     {
    //         ((RadioButton)oldrow.FindControl("rboVendorDetails")).Checked = false;
    //     }

    //     //Set the new selected row
    //     RadioButton rb = (RadioButton)sender;
    //     GridViewRow row = (GridViewRow)rb.NamingContainer;
    //     ((RadioButton)row.FindControl("rboVendorDetails")).Checked = true;
    //}

    /// <summary>
    /// Display taxid in the format
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl = new Label();
            lbl = (Label)e.Row.FindControl("lblTaxId");
            CheckBox chk = new CheckBox();
            chk = (CheckBox)e.Row.FindControl("chkIsBasedInUS");

            string str = lbl.ToolTip.Trim();
            if (str.Length > 0)
            {
                //lbl.Text= str.Substring(0, 2);
                //lbl.Text= lbl.Text.Trim() + "-" + str.Substring(2);
                //003-Sooraj
                if (chk.Checked)
                    lbl.Text = str.ConvertToTaxId(chk.Checked).Substring(0, str.Length > 20 ? 20 : str.Length+1); //003
                else
                    lbl.Text = str.ConvertToTaxId(chk.Checked).Substring(0, str.Length > 20 ? 20 : str.Length); //003

                lbl.ToolTip = str.ConvertToTaxId(chk.Checked); //003

            }
        }
    }

    /// <summary>
    /// Display taxid in the format
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendorDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl = new Label();
            lbl = (Label)e.Row.FindControl("lblTaxId");
            CheckBox chk = new CheckBox();
            chk = (CheckBox)e.Row.FindControl("chkIsBasedInUS"); //003
            string str = lbl.ToolTip.Trim();
            if (str.Length > 0)
            {
             
                if (chk.Checked)
                    lbl.Text = str.ConvertToTaxId(chk.Checked).Substring(0, str.Length > 20 ? 20 : str.Length + 1); //003
                else
                    lbl.Text = str.ConvertToTaxId(chk.Checked).Substring(0, str.Length > 20 ? 20 : str.Length); //003
                //003-Sooraj
                lbl.ToolTip = str.ConvertToTaxId(chk.Checked);  //003
            }
        }
    }

    /// <summary>
    /// Display the vendor details based on the selected taxid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSearch_Click(object sender, ImageClickEventArgs e)
    {
        int flag = 0;

        RadioButton rdo = new RadioButton();
        foreach (GridViewRow grd in grdVendor.Rows)
        {
            flag += 1;
            rdo = (RadioButton)grd.FindControl("rboVendor");
            if (rdo.Checked)
            {
                flag = -1;
                Label lbl = new Label();
                lbl = (Label)grd.FindControl("lblTaxId");
                CheckBox chk = new CheckBox();
                chk = (CheckBox)grd.FindControl("chkIsBasedInUS"); //003

                ViewState["TaxId"] = lbl.ToolTip.ConvertToTaxId(chk.Checked);                  //002 

                HiddenField hdn = new HiddenField();
                hdn = (HiddenField)grd.FindControl("hdnVendorId");
                ViewState["VendorName"] = ((Label)grd.FindControl("lblCompany")).Text;
                Session["RowValue"] = grd.RowIndex;
                Session["VendorAdminId"] = hdn.Value;
                Session["VendorStatus"] = ((HiddenField)grd.FindControl("hdnVendorStatus")).Value;  //005
                string[] str = lbl.Text.Trim().Split('-');
                string taxid = GetTaxID(str); //003
                grdVendorDetails.DataSource = objVendorMatch.DisplayVendorNumber(taxid, (ViewState["VendorName"] as String));   //GV
                grdVendorDetails.DataBind();
                if (grdVendorDetails.Rows.Count > 0)
                {
                    SpnVen1.Style["display"] = "";
                    SpnVen2.Style["display"] = "None";
                }
                else
                {
                    SpnVen2.Style["display"] = "";
                    SpnVen1.Style["display"] = "None";
                }
                break;
            }
        }
        if (flag == grdVendor.Rows.Count)
        {
            lblmsg.Text = "Please select atleast one option from the list";
            modalExtnd.Show();
        }
    }
    //003
    /// <summary>
    /// Return tax Id
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    private string GetTaxID(string[] str)
    {
        string taxtID = string.Empty;
        if (str.Length > 1)
            taxtID = str[0] + str[1];
        else
            taxtID = str[0];
        return taxtID;
    }


    /// <summary>
    /// Update the vendor number and display the next vendor record 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSubmitandnext_Click(object sender, ImageClickEventArgs e)
    {
        int flag = 0;
        string taxid = string.Empty; ;
        CheckBox rdo = new CheckBox();
        string VendorNumber = string.Empty;
        
        foreach (GridViewRow grd in grdVendorDetails.Rows)
        {
            flag += 1;

            rdo = (CheckBox)grd.FindControl("chkVendorDetails");
            if (rdo.Checked)
            {
                flag = -1;
                Label lblTaxId = new Label();
                lblTaxId = (Label)grd.FindControl("lblTaxId");
                CheckBox chk = new CheckBox();
                chk = (CheckBox)grd.FindControl("chkIsBasedInUS"); //003
                Label lblVendorName = new Label();
                lblVendorName = (Label)grd.FindControl("lblVendorName");
                Label lblVendorNumber = new Label();
                lblVendorNumber = (Label)grd.FindControl("lblVendorNumber");

                string[] str = lblTaxId.ToolTip.Trim().Split('-');
                //003
                taxid = GetTaxID(str);
                VendorNumber = string.IsNullOrEmpty(VendorNumber) ? lblVendorNumber.Text : VendorNumber + ", " + lblVendorNumber.Text;
                //004 - enabled the call to UpdateVendorNUmber and changed function to take the vendor ID instead of company and tax id
                //      since the company name in VMS might not match JDE vendor name.
                objVendorMatch.UpdateVendorNumber(Convert.ToInt64(Session["VendorAdminId"]), lblVendorNumber.Text, true);
                objVendorMatch.InsertVendorNumber(Convert.ToInt64(Session["VendorAdminId"]), lblVendorNumber.Text, clsVendorMatch.ActiveFlag.Active);

            }
        }
        if (VendorNumber.Contains(',') && VendorNumber.EndsWith(","))
        {
            VendorNumber = VendorNumber.Remove(VendorNumber.LastIndexOf(','));
        }

        //002 - There was a bug here
        //if (flag == grdVendor.Rows.Count)
        if (flag == grdVendorDetails.Rows.Count)
        {
            #region MyRegion
            //ViewState["SaveAndNext"] = 1;
            //imbCancel.Visible = true; 
            #endregion
            lblmsg.Text = "Please select at least one option from the list";
            modalExtnd.Show();
        }
        else
        {

            clsRegistration objRegistration = new clsRegistration();
            //005 - START
            var status = Convert.ToInt64(Session["VendorStatus"]);

            if (status == 1)
            {
                objRegistration.UpdateVendorBitStatus(Convert.ToInt64(Session["VendorAdminId"]), 2);
            }

            //005 - END
            
            lblmsg.Text = "Vendor Number(s) " + VendorNumber + " has been assigned to " + Convert.ToString(ViewState["VendorName"]);
            modalExtnd.Show();
            Bind();
            SpnVen1.Style["display"] = "none";
            if ((grdVendor.Rows.Count > 0) && (Convert.ToInt32(Session["RowValue"]) < (grdVendor.Rows.Count - 1)))
            {
                RadioButton rdb = new RadioButton();
                rdb = (RadioButton)grdVendor.Rows[Convert.ToInt32(Session["RowValue"])].FindControl("rboVendor");
                rdb.Checked = true;
                imbSearch_Click(null, null);
            }
            else if (grdVendor.Rows.Count == 1)
            {
                RadioButton rdb = new RadioButton();
                rdb = (RadioButton)grdVendor.Rows[0].FindControl("rboVendor");
                rdb.Checked = true;
                imbSearch_Click(null, null);
            }
            else
            {
                imbSubmitandnext.Visible = false;
            }
        }

    }

    /// <summary>
    /// Update the vendor number
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbVendordetailsSubmit_Click(object sender, ImageClickEventArgs e)
    {

        #region ARCHIVED
        //int flag = 0;
        //string taxid = string.Empty;
        //string VendorNumber = string.Empty;
        //CheckBox rdo = new CheckBox();
        //foreach (GridViewRow grd in grdVendorDetails.Rows)
        //{
        //    flag += 1;
        //    rdo = (CheckBox)grd.FindControl("chkVendorDetails");
        //    if (rdo.Checked)
        //    {
        //        flag = -1;
        //        Label lblTaxId = new Label();
        //        lblTaxId = (Label)grd.FindControl("lblTaxId");
        //        Label lblVendorName = new Label();
        //        lblVendorName = (Label)grd.FindControl("lblVendorName");
        //        Label lblVendorNumber = new Label();
        //        lblVendorNumber = (Label)grd.FindControl("lblVendorNumber");

        //        string[] str = lblTaxId.ToolTip.Trim().Split('-');
        //        taxid = str[0] + str[1];
        //        VendorNumber = string.IsNullOrEmpty(VendorNumber) ? lblVendorNumber.Text : VendorNumber + ", " + lblVendorNumber.Text;
        //        //objVendorMatch.UpdateVendorNumber("", taxid, VendorNumber, true);
        //        objVendorMatch.InsertVendorNumber(Convert.ToInt64(Session["VendorAdminId"]),lblVendorNumber.Text);
        //     }
        //}
        //if (flag == grdVendorDetails.Rows.Count)
        //{
        //    //002 - Modified
        //    ViewState["SaveAndNext"] = 0;
        //    imbCancel.Visible = true;
        //    lblmsg.Text = "No option from the list was selected.  Ignore vendor match?";
        //    modalExtnd.Show();
        //}
        //else{  clsRegistration objRegistration = new clsRegistration();
        //    // Modified by N Schoenberger on 11/30/2011 to allow admin vendor match NOT to update modify/ expiry dates
        //    //objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 2);
        //    objRegistration.UpdateVendorBitStatus(Convert.ToInt64(Session["VendorAdminId"]), 2);
        //    // End Modification ***********************************************************************

        //    lblmsg.Text = "Vendor Number(s) " + VendorNumber + " has been assigned to " + Convert.ToString(ViewState["VendorName"]);
        //    modalExtnd.Show();
        //    Bind();

        //    SpnVen1.Style["display"] = "none";} 
        #endregion

        int flag = 0;
        string taxid = string.Empty;
        string VendorNumber = string.Empty;
        CheckBox rdo = new CheckBox();
        foreach (GridViewRow grd in grdVendorDetails.Rows)
        {
            flag += 1;
            rdo = (CheckBox)grd.FindControl("chkVendorDetails");
            if (rdo.Checked)
            {
                flag = -1;
                Label lblTaxId = new Label();
                lblTaxId = (Label)grd.FindControl("lblTaxId");
                Label lblVendorName = new Label();
                lblVendorName = (Label)grd.FindControl("lblVendorName");
                Label lblVendorNumber = new Label();
                lblVendorNumber = (Label)grd.FindControl("lblVendorNumber");

                string[] str = lblTaxId.ToolTip.Trim().Split('-');
                taxid = GetTaxID(str); //003-Sooraj
                VendorNumber = string.IsNullOrEmpty(VendorNumber) ? lblVendorNumber.Text : VendorNumber + ", " + lblVendorNumber.Text;
                //004 - enabled the call to UpdateVendorNUmber and changed function to take the vendor ID instead of company and tax id
                //      since the company name in VMS might not match JDE vendor name.
                objVendorMatch.UpdateVendorNumber(Convert.ToInt64(Session["VendorAdminId"]), lblVendorNumber.Text, true);
                objVendorMatch.InsertVendorNumber(Convert.ToInt64(Session["VendorAdminId"]), lblVendorNumber.Text, clsVendorMatch.ActiveFlag.Active);
            }
        }
        if (flag == grdVendorDetails.Rows.Count)
        {
            lblmsg.Text = "Please select at least one option from the list";
            modalExtnd.Show();
        }
        else
        {
            clsRegistration objRegistration = new clsRegistration();
            // Modified by N Schoenberger on 11/30/2011 to allow admin vendor match NOT to update modify/ expiry dates
            //objRegistration.UpdateVendorBit(Convert.ToInt64(Session["VendorAdminId"]), 2);
            //005 - START
            var status = Convert.ToInt64(Session["VendorStatus"]);

            if (status == 1)
            {
                objRegistration.UpdateVendorBitStatus(Convert.ToInt64(Session["VendorAdminId"]), 2);
            }

            //005 - END
            // End Modification ***********************************************************************

            lblmsg.Text = "Vendor Number(s) " + VendorNumber + " has been assigned to " + Convert.ToString(ViewState["VendorName"]);
            modalExtnd.Show();
            Bind();

            SpnVen1.Style["display"] = "none";
        }
    }

    /// <summary>
    /// Reject the selected Vendor
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void imbReject_Click(object sender, ImageClickEventArgs e)
    {
        int flag = 0;
        RadioButton rdo = new RadioButton();
        foreach (GridViewRow grd in grdVendor.Rows)
        {
            flag += 1;
            rdo = (RadioButton)grd.FindControl("rboVendor");
            if (rdo.Checked)
            {
                Label lbl = new Label();
                lbl = (Label)grd.FindControl("lblTaxId");
                HiddenField hdn = new HiddenField();
                hdn = (HiddenField)grd.FindControl("hdnVendorId");
                Session["VendorAdminId"] = hdn.Value;
                Session["VendorStatus"] = ((HiddenField)grd.FindControl("hdnVendorStatus")).Value;  //005
                string[] str = lbl.ToolTip.Trim().Split('-');
                string taxid = GetTaxID(str); //003-Sooraj
                Label lblCompany = new Label();
                lblCompany = (Label)grd.FindControl("lblCompany");
                //002 - this one is not working.  Is throwing exception objVendorMatch.UpdateVendorNumber(lblCompany.Text.Trim(), taxid, "0",false);
                objVendorMatch.EditVendorNumber(lblCompany.Text.Trim(), taxid, "0", false, clsVendorMatch.ActiveFlag.Ignored);

                clsRegistration objRegistration = new clsRegistration();
                string adminemail = ConfigurationManager.AppSettings["adminemail"].ToString();
                objRegistration.UpdateVendorBitStatus(Convert.ToInt64(Session["VendorAdminId"]), 4);
                DAL.UspGetVendorDetailsByIdResult[] objresult = objRegistration.GetVendor(Convert.ToInt64(hdn.Value));
                StringBuilder Body = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
                Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hi" + " " + Convert.ToString(objresult[0].FirstName) + " " + Convert.ToString(objresult[0].LastName) + ", <br></font></td></tr>");
                Body.Append("<tr><td style='Padding-left:25'><font  face='verdana' size='2'>Your VQF form is rejected by the Haskell Team </font></td></tr>");
                Body.Append("<tr><td>&nbsp;</td></tr>");
                Body.Append("<tr><td><font  face='verdana' size='2'>Respectfully,</font><br/><br/></td></tr>");
                Body.Append("<tr><td><font  face='verdana' size='2'>Haskell</font><br></td></tr></table>");
                clsSafety objSafety = new clsSafety();
                objSafety.SendMail(adminemail, Convert.ToString(objresult[0].Email), "VQF Rejected By Haskell", Body);
                Bind();
                if ((grdVendor.Rows.Count > 0) && (Convert.ToInt32(Session["RowValue"]) < (grdVendor.Rows.Count - 1)))
                {
                    modalExtnd2.Show();
                }
                SpnVen1.Style["display"] = "none";
                SpnVen2.Style["display"] = "none";
            }

        }




    }

    /// <summary>
    /// Send email to the vendor
    /// </summary>
    /// <param name="fromaddress"></param>
    /// <param name="toaddress"></param>
    /// <param name="subject"></param>
    /// <param name="Content"></param>
    public void SendMail(string fromaddress, string toaddress, string subject, StringBuilder Content)
    {
        MailMessage mail = new MailMessage();
        mail.From = new MailAddress(fromaddress);
        mail.To.Add(new MailAddress(toaddress));
        mail.Priority = MailPriority.Normal;
        mail.Subject = subject;
        mail.IsBodyHtml = true;


        mail.Body = Content.ToString();
        SmtpClient client = new SmtpClient();
        client.Host = ConfigurationManager.AppSettings["MailServer"];
        client.Send(mail);

    }

    /// <summary>
    /// Accept the vendor 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAccept_Click(object sender, ImageClickEventArgs e)
    {
        clsRegistration objRegistration = new clsRegistration();

        //005 - START
        var status = Convert.ToInt64(Session["VendorStatus"]);

        if (status == 1)
        {
            objRegistration.UpdateVendorBitStatus(Convert.ToInt64(Session["VendorAdminId"]), 2);
        }

        //005 - END
        DataSet ManagerMail = objCommon.GetVQFManagerAutoMail(Convert.ToInt64(Session["VendorAdminId"]));
        if (ManagerMail.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ManagerMail.Tables[0].Rows.Count; i++)
            {
                StringBuilder Body = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
                Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hi, <br></font></td></tr>");
                Body.Append("<tr><td style='Padding-left:25'><font  face='verdana' size='2'>This is to inform you that" + " " + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + " " + " has successfully completed VQF.</font></td></tr>");
                Body.Append("<tr><td>&nbsp;</td></tr>");
                //Body.Append("<tr><td><font  face='verdana' size='2'>Thanks,</font></td></tr>");
                //Body.Append("<tr><td><font  face='verdana' size='2'> " + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + "</font><br></td></tr></table>");
                objCommon.SendMail(Convert.ToString(ManagerMail.Tables[0].Rows[i]["vendorEmail"]), Convert.ToString(ManagerMail.Tables[0].Rows[i]["ManagerEmail"]), "" + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + " - VQF Status - Complete ", Body);
                objCommon.UpdateVQFManagerAutoMail(Convert.ToInt64(ManagerMail.Tables[0].Rows[i]["NotificationID"]));
            }
        }
        if ((grdVendor.Rows.Count > 0) && (Convert.ToInt32(Session["RowValue"]) < (grdVendor.Rows.Count - 1)))
        {
            modalExtnd2.Show();

        }
        Bind();


        SpnVen2.Style["display"] = "none";
        Bind();
    }

    /// <summary>
    /// Confirmation to display the next record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbYes_Click(object sender, ImageClickEventArgs e)
    {
        SpnVen2.Style["display"] = "none";
        RadioButton rdb = new RadioButton();
        rdb = (RadioButton)grdVendor.Rows[Convert.ToInt32(Session["RowValue"])].FindControl("rboVendor");
        rdb.Checked = false;
        if (grdVendor.Rows.Count - 1 == Convert.ToInt32(Session["RowValue"]))
        {
            rdb = (RadioButton)grdVendor.Rows[grdVendor.Rows.Count - 1].FindControl("rboVendor");
            rdb.Checked = true;
        }
        else if (Convert.ToInt32(Session["RowValue"]) < (grdVendor.Rows.Count - 1))
        {
            rdb = (RadioButton)grdVendor.Rows[Convert.ToInt32(Session["RowValue"])].FindControl("rboVendor");
            rdb.Checked = true;
        }
        else if (grdVendor.Rows.Count == 1)
        {
            rdb = (RadioButton)grdVendor.Rows[0].FindControl("rboVendor");
            rdb.Checked = true;
        }


    }

    /// <summary>
    /// Redirect to the vqf view page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        LinkButton lnk = new LinkButton();
        lnk = (LinkButton)grdVendor.Rows[e.NewSelectedIndex].FindControl("lnkVQF");
        HiddenField hdn = new HiddenField();
        hdn = (HiddenField)grdVendor.Rows[e.NewSelectedIndex].FindControl("hdnVendorId");
        Session["VendorAdminId"] = hdn.Value;
        Session["VendorStatus"] = ((HiddenField)grdVendor.Rows[e.NewSelectedIndex].FindControl("hdnVendorStatus")).Value;  //005
        if (Convert.ToString(Session["Access"]) == "1")
        {
            Response.Redirect("../VQFView/CompanyView.aspx?user=admin&match=match");
        }
        else
        {
            Response.Redirect("../VQFView/CompanyView.aspx?user=employee");
        }
    }

    /// <summary>
    /// 002 - Added
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbOk_Click(object sender, ImageClickEventArgs e)
    {
        modalExtnd.Hide();
    }
    /// <summary>
    /// 002 - Added *** NOT IN USE CURRENTLY ***
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void imbCancel_Click(object sender, ImageClickEventArgs e)
    //{
    //    imbCancel.Visible = false;
    //    modalExtnd.Hide();
    //}

    /// <summary>
    /// 002 - Implemented
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgIgnore_Click(object sender, ImageClickEventArgs e)
    {
        #region CODE
        int flag = 0;
        string taxid = string.Empty;
        string VendorNumbers = string.Empty;
        CheckBox rdo = new CheckBox();

        foreach (GridViewRow grd in grdVendorDetails.Rows)
        {
            flag += 1;
            rdo = (CheckBox)grd.FindControl("chkVendorDetails");
            if (rdo.Checked)
            {
                flag = -1;
                Label lblTaxId = new Label();
                lblTaxId = (Label)grd.FindControl("lblTaxId");
                string company = ViewState["VendorName"] as String;
                Label lblVendorNumber = new Label();
                lblVendorNumber = (Label)grd.FindControl("lblVendorNumber");

                string[] str = lblTaxId.ToolTip.Trim().Split('-');
                taxid = GetTaxID(str); //003-Sooraj
                VendorNumbers = string.IsNullOrEmpty(VendorNumbers) ? lblVendorNumber.Text : VendorNumbers + ", " + lblVendorNumber.Text;
                objVendorMatch.EditVendorNumber(company.Trim(), taxid, lblVendorNumber.Text, true, clsVendorMatch.ActiveFlag.Ignored);
            }
        }

        if (flag == grdVendorDetails.Rows.Count)
        {
            lblmsg.Text = "Please select at least one option from the list";
            modalExtnd.Show();
        }
        else
        {
            clsRegistration objRegistration = new clsRegistration();
            //005 - START
            var status = Convert.ToInt64(Session["VendorStatus"]);

            if (status == 1)
            {
                objRegistration.UpdateVendorBitStatus(Convert.ToInt64(Session["VendorAdminId"]), 2);
            }
            //005 - END            

            lblmsg.Text = "Vendor Number(s) " + VendorNumbers + " have been ignored";
            modalExtnd.Show();
            Bind();

            SpnVen1.Style["display"] = "none";
        }

        #endregion

    }
}
