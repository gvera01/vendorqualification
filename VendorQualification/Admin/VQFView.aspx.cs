﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using VMSDAL;
#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 10/23/2012: VMS Stabilization Release 1.3 (see JIRA issues for details)
 * 003 Sooraj Sudhakaran.T 10/04/2013: VMS Modification - Added code to hide section that do not apply to design consultant 
 * 004 Sooraj Sudhakaran.T 10/04/2013: VMS Modification - Added code to handle International Business Id
 * 005 Sooraj Sudhakaran.T 10/08/2013: VMS Modification - Added question for International Currency
 * 006 Sooraj Sudhakaran.T 10/10/2013: VMS Modification - Added code to handle International Country ,State or province
 * 007 Sooraj Sudhakaran.T 10/10/2013: VMS Modification - Added code to handle Continent for trade region
 * 008 Sooraj Sudhakaran.T 10/25/2013: VMS Modification - Added new question for Non US vendors 
 * 009 Sooraj Sudhakaran.T 10/31/2013: VMS Modification - Added Non US vendors Language Speaks Question
 ****************************************************************************************************************/
#endregion


public partial class Admin_VQFView : System.Web.UI.Page
{

    #region Declaration

    Common objCommon = new Common();
    clsCompany objCompany = new clsCompany();
    clsLegal objLegal = new clsLegal();
    clsTradeInfomation objTradeInformation = new clsTradeInfomation();
    clsReferences objReferences = new clsReferences();
    clsSafety objSafety = new clsSafety();
    clsInsurance objInsurance = new clsInsurance();
    long VendorID;
    DataSet Attch = new DataSet();

    TableRow tr;
    TableCell tc;
    TableCell tc1;
    long tradeId;
    int countRegions;
    int count;
    bool IsBasedInUS; //007
    public DataSet dsstate;
    long prjrefid = 0;
    bool Result;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");
            
            //check for vendor id. if null redirect to home page.           
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["user"])))
            {
                //CompanyTopMenu.Visible = true;
                Session["match"] = Request.QueryString["match"];
            }
            else
            {
                Session["match"] = "";
                //CompanyTopMenu.Visible = false;
            }

            if (Convert.ToInt64(Session["VendorAdminId"]) > 0)
            {
                VendorID = Convert.ToInt64(Session["VendorAdminId"]);
            }
            else
            {
                VendorID = 0;
            }

            // get company details
            if (!string.IsNullOrEmpty(Convert.ToString(VendorID)))
            {
                clsRegistration objregistration = new clsRegistration();
                DAL.UspGetVendorDetailsByIdResult[] objResult = objregistration.GetVendor(Convert.ToInt64(VendorID));
                if (objResult.Count() > 0)
                {

                    IsBasedInUS = objResult[0].IsBasedInUS;
                    ShowHideReferenceSupplierQuestion(IsBasedInUS); //009

                    lblVendorName.Text = objResult[0].Company;
                    lblLastUpdateDate.Text = string.IsNullOrEmpty(Convert.ToString(objResult[0].LastModifiedDT)) ? "" : Convert.ToDateTime(objResult[0].LastModifiedDT).ToShortDateString();
                }
            }
            //   Vendor Qualification Form - Company
            // get company details
            //002
            VMSDAL.VQFCompany objGetCompany = objCompany.GetSingleVQFCompanyData(VendorID);
            if (objGetCompany != null)
            {
                lblDate.Text = Convert.ToDateTime(objGetCompany.FormDt).ToShortDateString();
                lblLegalBusinessName.Text = objGetCompany.LegalBusinessName;
                lblOtherBusinessName.Text = objGetCompany.OtherBusinessName_DBA;
                //004-Sooraj
                //if (objGetCompany.FederalTaxID.Length >= 9)
                //{
                //    lblFederalTax1.Text = Convert.ToString(objGetCompany.FederalTaxID).Substring(0, 2) + " - ";
                //    lblFederalTax2.Text = Convert.ToString(objGetCompany.FederalTaxID).Substring(2, 7);
                //}
                if (objGetCompany.FederalTaxID != null)
                    lblFederalTax1.Text = Convert.ToString(objGetCompany.FederalTaxID).ConvertToTaxId(objGetCompany.IsBasedInUS);
                lblRegisteredAs.Text = objGetCompany.IsBasedInUS ? "USA" : "Non-USA";
                lblDunsNumber.Text = objGetCompany.DUNSNumber;
                lblAddress.Text = objGetCompany.PhysicalAddress;
                lblCity.Text = objGetCompany.PhysicalCity;
                if (objGetCompany.FK_PhysicalState != 0)
                {
                    lblState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(objGetCompany.FK_PhysicalState)).Tables[0].Rows[0].ItemArray[1]);
                }
                if (objGetCompany.FK_PhysicalCountry.HasValue) //006-Sooraj
                {
                    lblCountry.Text = objCommon.GetCountryCode(objGetCompany.FK_PhysicalCountry.Value);
                }
                if (objGetCompany.FK_MailingCountry.HasValue) //006-Sooraj
                {
                    lblMCountry.Text = objCommon.GetCountryCode(objGetCompany.FK_MailingCountry.Value);
                }
                if (objGetCompany.FK_PaymentCountry.HasValue) //006-Sooraj
                {
                    lblPCountry.Text = objCommon.GetCountryCode(objGetCompany.FK_PaymentCountry.Value);
                }

                lblZip.Text = objGetCompany.PhysicalZipCode;
                if (lblState.Text.Equals("Other"))
                {
                    lblState.Text = objGetCompany.OtherPhysicalState;
                }

                lblMAddress.Text = objGetCompany.MailingAddress;
                lblMCity.Text = objGetCompany.MailingCity;
                if (objGetCompany.FK_MailingState != 0)
                {
                    lblMState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(objGetCompany.FK_MailingState)).Tables[0].Rows[0].ItemArray[1]);
                }
                lblMZip.Text = objGetCompany.MailingZipCode;
                if (lblMState.Text.Equals("Other"))
                {
                    lblMState.Text = objGetCompany.OtherMailingState;
                }

                lblPAddress.Text = objGetCompany.PaymentRemittanceAddress;
                lblPCity.Text = objGetCompany.PaymentCity;
                if (objGetCompany.FK_PaymentState != 0)
                {
                    lblPState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(objGetCompany.FK_PaymentState)).Tables[0].Rows[0].ItemArray[1]);
                }
                lblPZip.Text = objGetCompany.PaymentZipcode;
                lblMainPhno1.Text = objGetCompany.MainPhoneNumber;
                lblMainFaxno1.Text = objGetCompany.MainFaxNumber;
                lblCWebAddress.Text = objGetCompany.CompanyWebsiteAddress;
                lblYearinBusiness.Text = objGetCompany.YearsinBusiness;
                if (objGetCompany.NoOfEmployees != -1)
                {
                    lblNoEmp.Text = Convert.ToString(objGetCompany.NoOfEmployees);
                }
                if (objCommon.GetStateCode(Convert.ToInt16(objGetCompany.FK_StateSalesTax)).Tables[0].Rows.Count > 0)
                {
                    ddlState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(objGetCompany.FK_StateSalesTax)).Tables[0].Rows[0].ItemArray[1]);
                }
                if (ddlState.Text.Equals("Other"))
                {
                    ddlState.Text = objGetCompany.OtherStateSalesTax;
                }
                //The above line was commented and below line was included by SGS for Task No: 7
                spnStateTax.Style["display"] = "";
                lblStateTaxNo1.Text = objGetCompany.StateTaxNumber;
               
                rdoBusinessType.Text = objGetCompany.BusinessType;
                if (rdoBusinessType.Text.Equals("Other"))
                {
                    OtherBusiness.Style["Display"] = "";
                    lblOther1.Text = objGetCompany.OtherType;
                }

                string CompanyCertification = null;
                DAL.VQFCompanyCertification[] objCertification = objCompany.GetCompanyCertification(objGetCompany.PK_CompanyID);
                for (int i = 0; i < objCertification.Length; i++)
                {
                    CompanyCertification += string.IsNullOrEmpty(CompanyCertification) ? objCertification[i].Certification : " , " + objCertification[i].Certification;
                }
                rdoCompanyCertiY.Text = CompanyCertification;
                CompanyCertification = string.Empty;
                DAL.VQFCompanyCertification[] objFederalSB = objCompany.GetCompanyFederalSB(objGetCompany.PK_CompanyID);
                for (int i = 0; i < objFederalSB.Length; i++)
                {
                    CompanyCertification += string.IsNullOrEmpty(CompanyCertification) ? objFederalSB[i].Certification : " , " + objFederalSB[i].Certification;
                }
                
                lblISO.Text = "ISO " + objCompany.GetIndustryCertCodes(objGetCompany.PK_CompanyID, "ISO");   //002
                if (lblISO.Text.Length == 4) lblISO.Text += "  NONE";                                    //002
                lblQS.Text = " QS " + objCompany.GetIndustryCertCodes(objGetCompany.PK_CompanyID, "QS");     //002
                if (lblQS.Text.Length == 4) lblQS.Text += "  NONE";                                      //002

                lblFederalSB.Text = CompanyCertification;
                rdoCertifyingAgency.Text = objGetCompany.CertifyingAgency;
                bool? UnionShop = objGetCompany.UnionShop;

                if (UnionShop == true)
                {
                    lblUnionShop.Text = "Yes";
                    tdTxtUnionAffiliation.Style["display"] = "";
                    tdUnionAffiliation.Style["display"] = "";
                }
                else if (UnionShop == false)
                {
                    lblUnionShop.Text = "No";
                    tdTxtUnionAffiliation.Style["display"] = "None";
                    tdUnionAffiliation.Style["display"] = "None";
                }
                else
                {
                    lblUnionShop.Text = "";
                    tdTxtUnionAffiliation.Style["display"] = "None";
                    tdUnionAffiliation.Style["display"] = "None";
                }

                lblUnionAffiliation.Text = objGetCompany.UnionAffiliation;
                lblPrincipal.Text = objGetCompany.PrincipalContactName;
                lblPTitle.Text = objGetCompany.PrincipalContactTitle;
                lblPEmail.Text = objGetCompany.PrincipalContactEmail;
                lblPPhone1.Text = objGetCompany.PrincipalContactPhone;
                lblPFax.Text = objGetCompany.PrincipalContactFax;

                lblProjName.Text = objGetCompany.ProjectManagementContactName;
                lblProjTitle.Text = objGetCompany.ProjectManagementTitle;
                lblProjemail.Text = objGetCompany.ProjectManagementEmail;
                lblProjPhone1.Text = objGetCompany.ProjectManagementPhone;
                lblProjFax1.Text = objGetCompany.ProjectManagementFax;   

                lblArName.Text = objGetCompany.AccountReceivableContactName;
                lblARTitle.Text = objGetCompany.AccountReceivableTitle;
                lblAREmail.Text = objGetCompany.AccountReceivableEmail;
                lblARPhone1.Text = objGetCompany.AccountReceivablePhone;
                lblARFax1.Text = objGetCompany.AccountPayableFax;   

                lblApName.Text = objGetCompany.AccountPayableContactName;
                lblApTitle.Text = objGetCompany.AccountPayableTitle;
                lblApEmail.Text = objGetCompany.AccountPayableEmail;
                lblApPhone1.Text = objGetCompany.AccountPayablePhone;
                lblApFax1.Text = objGetCompany.AccountPayableFax;

                //002 - added below
                lblOnsiteSupportName.Text = objGetCompany.OnsiteSupportContactName;
                lblOnsiteSupportTitle.Text = objGetCompany.OnsiteSupportContactTitle;
                lblOnsiteSupportEmail.Text = objGetCompany.OnsiteSupportContactEmail;
                lblOnsiteSupportPhone.Text = objGetCompany.OnsiteSupportContactPhone;
                lblOnsiteSupportFax.Text = objGetCompany.OnsiteSupportContactFax;


                rptKeyContact.DataSource = objCompany.GetcompanyKeyContactsData(objGetCompany.PK_CompanyID);
                rptKeyContact.DataBind();


                if (lblPState.Text.Equals("Other"))
                {
                    lblPState.Text = objGetCompany.OtherPaymentState;
                }
                lbldesignBuild.Text = ReturnYesNo(objGetCompany.Design_Build);
                if (objGetCompany.Design_Build == true)
                {
                    if (objGetCompany.Design_BuildCapabilities != "")
                    {
                        rdoCapabilitiesY.Text = " - " + objGetCompany.Design_BuildCapabilities;
                    }
                }
                lblPContact.Text = ReturnYesNo(objGetCompany.AnotherCompany);
                if (lblPContact.Text == "Yes")
                {
                    lblPContactY.Text = objGetCompany.ExplainAnotherCompany;
                    trbussiness.Style["Display"] = string.IsNullOrEmpty(lblPContactY.Text) ? "none" : "";
                }
                else
                {
                    trbussiness.Style["Display"] = "none";
                }
                lblProjectIfAvailable.Text = objGetCompany.Project;
                lblTypeOfCompany.Text = objGetCompany.TypeOfCompany;

                switch (lblTypeOfCompany.Text)
                {
                    case "Subcontractor":
                        salestax.Style["Display"] = "";
                        trCertifyingAgency.Style["Display"] = "";
                        trdesigncapabCtl.Style["Display"] = "";
                        tblCompany.Style["Display"] = "";
                        tblLabour.Style["Display"] = "";
                        break;
                    case "Supplier":
                        salestax.Style["Display"] = "";
                        trCertifyingAgency.Style["Display"] = "";
                        trdesigncapabCtl.Style["Display"] = "none";
                        tblCompany.Style["Display"] = "";
                        lbldesignBuild.Visible = false;
                        rdoCapabilitiesY.Visible = false;
                        tblLabour.Style["Display"] = "";
                        break;
                    case "Design Consultant":
                        trCertifyingAgency.Style["Display"] = "none";
                        salestax.Style["Display"] = "none";
                        trdesigncapabCtl.Style["Display"] = "";
                        lbldesignBuild.Visible = true;
                        rdoCapabilitiesY.Visible = true;
                       // tblCompany.Style["Display"] = "none";
                        tblLabour.Style["Display"] = "none";
                        //003-Sooraj - Design Consultant Changes 
                        trSafety.Attributes["style"] = "display: none";
                        tblSupplierCredit.Attributes["style"] = "display: none";
                        tblBondingAndSurety.Attributes["style"] = "display: none";
                        break;
                    default:
                        trCertifyingAgency.Style["Display"] = "";
                        salestax.Style["Display"] = "";
                        trdesigncapabCtl.Style["Display"] = "";
                        lbldesignBuild.Visible = true;
                        rdoCapabilitiesY.Visible = true;
                        tblCompany.Style["Display"] = "";
                        tblLabour.Style["Display"] = "";
                        break;
                }
                dlAdditionalLocations.DataSource = objCompany.GetcompanyBusinessLocation(objGetCompany.PK_CompanyID);
                dlAdditionalLocations.DataBind();
                tdAddLocations.Style["display"] = dlAdditionalLocations.Items.Count > 0 ? "" : "none";
                rptCompanyOfficers.DataSource = objCompany.GetcompanyOfficersData(objGetCompany.PK_CompanyID);
                rptCompanyOfficers.DataBind();
            }
        }

        if (rptCompanyOfficers.Items.Count == 0)
        {
            tdOfficerData.Style["display"] = "";
            rptCompanyOfficers.Visible = false;
        }
        else
        {
            tdOfficerData.Style["display"] = "none";
            rptCompanyOfficers.Visible = true;
        }
        // DisplayHeader();
        trdesigncapabCtlText.Style["display"] = trdesigncapabCtl.Style["display"];
        
        // Vendor Qualification Form - Legal & Financial
        // get legal details
        DAL.VQFLegal objLegalData = objLegal.GetSingleVQLData(VendorID);
        if (objLegalData != null)
        {
            bool? ListOne = objLegalData.Legal1;
            lblListOne.Text = ReturnYesNo(ListOne);
            if (ListOne == true)
            {
                trWorkAwdOne.Style["display"] = "";
                txtWorkAwd.Text = objLegalData.ExplainLegal1;
            }

            bool? ListTwo = objLegalData.Legal2;
            lblListTwo.Text = ReturnYesNo(ListTwo);
            if (ListTwo == true)
            {
                trStock.Style["display"] = "";
                txtStake.Text = objLegalData.ExplainLegal2;
            }

            bool? ListThree = objLegalData.Legal3;
            lblListThree.Text = ReturnYesNo(ListThree);
            if (ListThree == true)
            {
                trBankruptcy.Style["display"] = "";
                txtBankruptcy.Text = objLegalData.ExplainLegal3;
            }

            bool? ListFour = objLegalData.Legal4;
            lblListFour.Text = ReturnYesNo(ListFour);
            if (ListFour == true)
            {
                trConviction.Style["display"] = "";
                txtRisk.Text = objLegalData.ExplainLegal4;
            }

            // Get financial details
            string Currentyear;
            if (DateTime.Now >= Convert.ToDateTime("05/01/" + Convert.ToString(DateTime.Now.Year)))
            {
                Currentyear = Convert.ToString(DateTime.Now.Year - 1);
            }
            else
            {
                Currentyear = Convert.ToString(DateTime.Now.Year - 2);
            }

            DataSet financialDataSet = objLegal.GetLegalFinancialData(Convert.ToInt64(objLegalData.PK_LegalID), Currentyear);
            if (IsFinancialDataExists(financialDataSet, Currentyear))
            {
                rptFinancial.DataSource = financialDataSet;
            }
            else
            {
                financialDataSet = objLegal.GetLegalFinancialData(Convert.ToInt64(objLegalData.PK_LegalID), (Convert.ToInt32(Currentyear) - 1).ToString());
                DataRow[] dataRow = financialDataSet.Tables[0].Select("Year <> '" + Currentyear.ToString() + "'");
                DataTable dataTable = financialDataSet.Tables[0].Clone();
                for (int count = 0; count < dataRow.Length; count++)
                {
                    dataTable.ImportRow(dataRow[count]);
                }
                DataView dataView = new DataView(dataTable, "", "Year DESC", DataViewRowState.CurrentRows);

                rptFinancial.DataSource = dataView.ToTable();
            }
            rptFinancial.DataBind();


            lblProjectRevenue.Text = objLegalData.CurrentYearProjectedRevenue;
            lblTotalBackLog.Text = objLegalData.CurrentTotalBacklog;

            //005-Sooraj
            lblBaseCurrency.Text = objCommon.GetCurrencyCode(objLegalData.FK_CurrencyID);
            lblDenominate.Text = objLegalData.IsDenominateInDollar.ConvertToText();


            //Get banking details
            rptBanking.DataSource = objLegal.GetLegalBankingData(Convert.ToInt64(objLegalData.PK_LegalID));
            rptBanking.DataBind();


        }
        if (rptFinancial.Items.Count == 0)
        {
            tdFinancialData.Style["display"] = "";
            if (DateTime.Now >= Convert.ToDateTime("05/01/" + Convert.ToString(DateTime.Now.Year)))
            {
                lblFinancialYear1.Text = Convert.ToString(DateTime.Now.Year - 1);
                lblFinancialYear2.Text = Convert.ToString(DateTime.Now.Year - 2);
                lblFinancialYear3.Text = Convert.ToString(DateTime.Now.Year - 3);
            }
            else
            {
                lblFinancialYear1.Text = Convert.ToString(DateTime.Now.Year - 2);
                lblFinancialYear2.Text = Convert.ToString(DateTime.Now.Year - 3);
                lblFinancialYear3.Text = Convert.ToString(DateTime.Now.Year - 4);
            }
            tblFinancial.Style["display"] = "none";
        }
        else
        {
            tdFinancialData.Style["display"] = "none";
            tblFinancial.Style["display"] = "";
        }
        tdBankingData.Style["display"] = rptBanking.Items.Count == 0 ? "" : "none";
        
        // Vendor Qualification Form - Trade Information
        int flag = 0;
        tblTradeInfo.Width = Unit.Percentage(100);
        int resCount = objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorID)).Length;

        if (resCount > 0)
        {
            // Display Trade
            tradeId = objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorID))[0].PK_TradeID;
            // Display Scopes
            count = objTradeInformation.GetSelectedTradeScopesHeading(tradeId).Length;
            if (count > 0)
            {
                tblTradeScopeHeader.Style["display"] = "";
                trScopeCompleteDetails.Style["display"] = "";
                trScopeEmptyDetails.Style["display"] = "none";

                int count1 = count - (Convert.ToInt32(Math.Round(Convert.ToDouble(count / 2))));
                if (count1 >= 0 && count1 <= count)
                {
                    if (count1 == 0)
                    {
                        count1 = 1;
                    }
                    for (int i = 0; i < count1; i++)
                    {
                        flag += 1;
                        Table tbl = new Table();
                        TableRow tr = new TableRow();
                        TableCell tc = new TableCell();
                        Label lbl = new Label();
                        TreeView tv = new TreeView();
                        lbl.Text = objTradeInformation.GetSelectedTradeScopesHeading(tradeId)[i].Name;
                        lbl.CssClass = "tree_head";

                        tv.ShowExpandCollapse = false;
                        tv.Attributes.Add("onClick", "return false;");
                        foreach (DAL.USPGetSelectedTradescopesResult obj1 in objTradeInformation.GetSelectedTradeScopes(objTradeInformation.GetSelectedTradeScopesHeading(tradeId)[i].Pk_ScopeId, Convert.ToInt64(VendorID)))
                        {
                            TreeNode tn = new TreeNode();

                            tn.Text = obj1.Name;

                            foreach (DAL.USPGetSelectedTradescopesResult obj2 in objTradeInformation.GetSelectedTradeScopes(obj1.Pk_ScopeId, Convert.ToInt64(VendorID)))
                            {
                                TreeNode tn1 = new TreeNode();
                                tn1.Text = obj2.Name;
                                tn1.Selected = false;
                                tn.ChildNodes.Add(tn1);
                            }
                            tv.Nodes.Add(tn);
                        }

                        tv.CssClass = "Treestyle";
                        tv.RootNodeStyle.CssClass = "list_display";
                        tv.LeafNodeStyle.CssClass = "list_display";
                        tc.Controls.Add(lbl);
                        tc.Controls.Add(tv);
                        tr.Cells.Add(tc);
                        tr.Cells.Add(tc);
                        tbl.Rows.Add(tr);
                        tdCell1.Controls.Add(tbl);
                    }

                    for (int i = count1; i < count; i++)
                    {
                        flag += 1;
                        Table tbl = new Table();
                        TableRow tr = new TableRow();
                        TableCell tc = new TableCell();
                        Label lbl = new Label();
                        TreeView tv = new TreeView();

                        lbl.Text = objTradeInformation.GetSelectedTradeScopesHeading(tradeId)[i].Name;
                        lbl.CssClass = "tree_head";

                        tv.ShowExpandCollapse = false;

                        tv.Attributes.Add("onClick", "return false;");
                        foreach (DAL.USPGetSelectedTradescopesResult obj1 in objTradeInformation.GetSelectedTradeScopes(objTradeInformation.GetSelectedTradeScopesHeading(tradeId)[i].Pk_ScopeId, Convert.ToInt64(VendorID)))
                        {
                            TreeNode tn = new TreeNode();
                            tn.Text = obj1.Name;

                            foreach (DAL.USPGetSelectedTradescopesResult obj2 in objTradeInformation.GetSelectedTradeScopes(obj1.Pk_ScopeId, Convert.ToInt64(VendorID)))
                            {
                                TreeNode tn1 = new TreeNode();
                                tn1.Text = obj2.Name;
                                tn1.Selected = false;
                                tn.ChildNodes.Add(tn1);
                            }
                            tv.Nodes.Add(tn);
                        }

                        tv.CssClass = "Treestyle";
                        tv.RootNodeStyle.CssClass = "list_display";
                        tv.LeafNodeStyle.CssClass = "list_display";
                        tc.Controls.Add(lbl);

                        tc.Controls.Add(tv);
                        tr.Cells.Add(tc);
                        tr.Cells.Add(tc);
                        tbl.Rows.Add(tr);

                        tdCell2.Controls.Add(tbl);
                    }
                }
                else
                {
                    TradeScopeHeader.Visible = false;
                }
            }
            else
            {
                tblTradeScopeHeader.Style["display"] = "";
                trScopeCompleteDetails.Style["display"] = "none";
                trScopeEmptyDetails.Style["display"] = "";
            }
            //006- Display Continents
            dlContinents.DataSource = objTradeInformation.GetVQFTradeRegionContinentsView(tradeId);
            dlContinents.DataBind();

            // Display trade regions
            dlRegions.DataSource = objTradeInformation.GetSelectedTradeRegions(-1, tradeId);
            dlRegions.DataBind();

            // Display license Numbers
            rptLicenseNumber.DataSource = objTradeInformation.GetSelectedTradeLicenseNumbers(tradeId);
            rptLicenseNumber.DataBind();

            if (rptLicenseNumber.Items.Count > 0)
            {
                trEmptyLicense.Style["display"] = "none";
                trLicenseComplete.Style["display"] = "";

            }
            else
            {
                trLicenseComplete.Style["display"] = "none";
                trEmptyLicense.Style["display"] = "";
            }

            lblOwnForces.Text = ReturnYesNo(objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorID))[0].OwnForces);
            trExplain.Style["display"] = lblOwnForces.Text.Equals("Yes") ? "none" : "";

            if (Convert.ToString(objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorID))[0].OwnForces) == "False")
            {
                lblExplain.Text = objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorID))[0].Explain;
            }
            if ((dlRegions.Items.Count == 0) && (rptLicenseNumber.Items.Count <= 0) && (string.IsNullOrEmpty(Convert.ToString(objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorID))[0].OwnForces))))
            {
                tblTradeRegionHeader.Style["display"] = "";
            }

            if ((rptLicenseNumber.Items.Count <= 0) && (dlRegions.Items.Count <= 0) && (lblOwnForces.Text == "") && (lblExplain.Text == ""))
            {
                TradeRegionHeader.Visible = true;
            }
            // Display selected Projects
            dlProjects.DataSource = objTradeInformation.GetSelectedTradeProjects(tradeId);
            dlProjects.DataBind();
            if (dlProjects.Items.Count <= 0)
            {
                ProjectTypesHeader.Visible = true;
                ProjectTypesContent.Visible = true;
                trCompleteProjects.Style["display"] = "none";
                trEmptyProjects.Style["display"] = "";
            }
            else
            {
                trEmptyProjects.Style["display"] = "none";
                trCompleteProjects.Style["display"] = "";
            }
        }

        // Vendor Qualification Form - Trade Information
        if (count > 0)
        {
            tblTradeScopeHeader.Style["display"] = "";
            trScopeCompleteDetails.Style["display"] = "";
            trScopeEmptyDetails.Style["display"] = "none";
        }
        else
        {
            tblTradeScopeHeader.Style["display"] = "";
            trScopeCompleteDetails.Style["display"] = "none";
            trScopeEmptyDetails.Style["display"] = "";
        }

        if (rptLicenseNumber.Items.Count > 0)
        {
            trEmptyLicense.Style["display"] = "none";
            trLicenseComplete.Style["display"] = "";
        }
        else
        {
            trLicenseComplete.Style["display"] = "none";
            trEmptyLicense.Style["display"] = "";
        }
        dlRegions.Visible = dlRegions.Items.Count > 0;
        dlContinents.Visible = dlContinents.Items.Count > 0;
        if (dlRegions.Items.Count > 0 || dlContinents.Items.Count > 0)
        {
            tblTradeRegionHeader.Style["display"] = "";
            TradeRegionHeader.Visible = true;
            trCompleteRegions.Style["display"] = "";
            trEmptyRegions.Style["display"] = "none";
        }
        else
        {
            tblTradeRegionHeader.Style["display"] = "";
            TradeRegionHeader.Visible = true;
            trCompleteRegions.Style["display"] = "none";
            trEmptyRegions.Style["display"] = "";
        }

        if (dlProjects.Items.Count <= 0)
        {
            ProjectTypesHeader.Visible = true;
            ProjectTypesContent.Visible = true;
            trCompleteProjects.Style["display"] = "none";
            trEmptyProjects.Style["display"] = "";
        }
        else
        {
            trEmptyProjects.Style["display"] = "none";
            trCompleteProjects.Style["display"] = "";
        }

        //********** Trade Information********************/
        // Vendor Qualification Form - References

        int Refcount = objReferences.GetRefCount(Convert.ToInt32(VendorID));
        if (Refcount > 0)
        {
            DAL.VQFReference Viewdata = objReferences.GetSingleVQFReferenceData(Convert.ToInt32(VendorID));
            LblcompletePrjstatus.Text = ReturnYesNo(Viewdata.CompletedProjects);
            
            if (objReferences.GetProjectReferences(Convert.ToInt64(Viewdata.PK_ReferencesID)).Count() > 0)
            {
                rptProjectReferences.DataSource = objReferences.GetProjectReferences(Convert.ToInt64(Viewdata.PK_ReferencesID));
                rptProjectReferences.DataBind();
            }
            else
            {
                rptProjectReferences.DataSource = null;
                rptProjectReferences.DataBind();
            }
            tblProjectReference.Style["display"] = rptProjectReferences.Items.Count > 0 ? "none" : "";
            
            rptAddProjectref.DataSource = objReferences.GetReferencesProject(Convert.ToInt64(Viewdata.PK_ReferencesID));
            rptAddProjectref.DataBind();
            tblProjectName.Style["display"] = rptAddProjectref.Items.Count > 0 ? "none" : "";
            if (rptAddProjectref.Items.Count == 0)
            {
                rptAddProjectref.DataSource = null;
                rptAddProjectref.DataBind();
            }
            rptsupl.DataSource = objReferences.GetReferencesSupplier(Convert.ToInt64(Viewdata.PK_ReferencesID));
            rptsupl.DataBind();
        }

        if (rptsupl.Items.Count > 0)
        {
            SuplDetails.Style["display"] = "";
            tdSupplierData.Style["display"] = "none";
        }
        else
        {
            SuplDetails.Style["display"] = "none";
            tdSupplierData.Style["display"] = "";
        }

        foreach (RepeaterItem rit in rptProjectReferences.Items)
        {
            HiddenField hid = new HiddenField();
            hid = (HiddenField)rit.FindControl("hdnStatus");
            if (hid.Value == "display")
            {
                TextBox txt = new TextBox();
                txt = (TextBox)rit.FindControl("txtNotes");
                txt.Style["display"] = "";
            }
        }
        //Vendor Qualification Form - Safety

        #region EMR Rates & OSHA

        int currentYear = 0;
        int oshaCurrentYear = 0;
        // G. Vera Phase 3 Modifications Item 1.3
        //DateTime today = DateTime.Now.AddMonths(1);
        DateTime today = DateTime.Now;
        if (today >= Convert.ToDateTime("01/01/" + Convert.ToString(DateTime.Now.Year)))
        {
            lblYear1.Text = Convert.ToString(DateTime.Now.Year);
            lblYear2.Text = Convert.ToString(DateTime.Now.Year - 1);
            lblYear3.Text = Convert.ToString(DateTime.Now.Year - 2);
            currentYear = DateTime.Now.Year;

            // G. Vera Phase 3 Modifications Item 1.3
            lblOshaYear1.Text = Convert.ToString(DateTime.Now.Year -1);
            lblOshaYear2.Text = Convert.ToString(DateTime.Now.Year - 2);
            lblOshaYear3.Text = Convert.ToString(DateTime.Now.Year - 3);
            oshaCurrentYear = DateTime.Now.Year - 1; // G. Vera Phase 3 Modifications Item 1.3
        }
        else
        {
            lblYear1.Text = Convert.ToString(DateTime.Now.Year - 1);
            lblYear2.Text = Convert.ToString(DateTime.Now.Year - 2);
            lblYear3.Text = Convert.ToString(DateTime.Now.Year - 3);
            currentYear = DateTime.Now.Year - 1;
            
            // G. Vera Phase 3 Modifications Item 1.3
            lblOshaYear1.Text = Convert.ToString(DateTime.Now.Year - 2);
            lblOshaYear2.Text = Convert.ToString(DateTime.Now.Year - 3);
            lblOshaYear3.Text = Convert.ToString(DateTime.Now.Year - 4);
            oshaCurrentYear = DateTime.Now.Year - 2; // G. Vera Phase 3 Modifications Item 1.3
        }

        int Count = objSafety.GetSafetyCount(VendorID);
        if (!IsBasedInUS)
        {
            trNonUSSafetyQuestion.Style["Display"] = "";
            spnContactNonUS.Style["Display"] = "";
        }
        else
        {
            trNonUSSafetyQuestion.Style["Display"] = "none";
            spnContactNonUS.Style["Display"] = "none";
        }
        if (Count > 0)
        {
            DAL.VQFSafety objSafetyData = objSafety.GetSingleVQLSafetyData(VendorID);

            lblSIC.Text = objSafetyData.SICNAICSCode;
            chkSICNA.Checked = objSafetyData.SICNA == true ? true : false;
            lblSICExplain.Text = objSafetyData.Explanation;
            bool? CompanySafety = objSafetyData.CompanySafety;
            lblSafetyDirector.Text = ReturnYesNo(CompanySafety);
            if (CompanySafety == true)
            {
                spnContact.Style["display"] = "";
                lblContactName.Text = objSafetyData.CompanySafetyContactName;
                lblMainPhno2.Text = objSafetyData.CompanySafetyContactNumber.ToString();
            }

            //007 Starts 

            if (!IsBasedInUS)
            {
                trDenominateInDollar.Style["Display"] = "";
                trNonUSSafetyQuestion.Style["Display"] = "";
                bool? SafetyRepresentation = objSafetyData.SafetyRepresentation;
                lblRepresentation.Text = ReturnYesNo(SafetyRepresentation);
                if (SafetyRepresentation == true)
                {
                    spnContactNonUS.Style["Display"] = "";
                    lblRepresentationContactName.Text = objSafetyData.SafetyRepresentationContactName;
                    lblRepresentationContactNumber.Text = objSafetyData.SafetyRepresentationContactNumber.ToString();
                }
            }
           
            //007 Ends 


            lblSafetyProgram.Text = ReturnYesNo(objSafetyData.SafetyProgram);
            lblSafetytraining.Text = ReturnYesNo(objSafetyData.SafetyRequirements);

            VMSDAL.VQFSafetyEMRRate objRates = objSafety.SelectSafetyEMRRates(Convert.ToInt64(objSafetyData.PK_SafetyID), lblYear1.Text);
            currentYear = CheckEMRRates(objRates, currentYear);
            lblYear1.Text = currentYear.ToString();
            objRates = objSafety.SelectSafetyEMRRates(Convert.ToInt64(objSafetyData.PK_SafetyID), lblYear1.Text);

            if (objRates != null)
            {
                lblRate1.Text = objRates.EMRRate;
                chkNA1.Checked = Convert.ToBoolean(objRates.NotAvailable);
                lblExplain1.Text = objRates.Explanation;
            }
            //
            lblYear2.Text = (currentYear - 1).ToString();
            objRates = objSafety.SelectSafetyEMRRates(Convert.ToInt64(objSafetyData.PK_SafetyID), lblYear2.Text);
            if (objRates != null)
            {
                lblRate2.Text = objRates.EMRRate;
                chkNA2.Checked = Convert.ToBoolean(objRates.NotAvailable);
                lblExplain2.Text = objRates.Explanation;
            }
            //
            lblYear3.Text = (currentYear - 2).ToString();
            objRates = objSafety.SelectSafetyEMRRates(Convert.ToInt64(objSafetyData.PK_SafetyID), lblYear3.Text);
            if (objRates != null)
            {
                lblRate3.Text = objRates.EMRRate;
                chkNA3.Checked = Convert.ToBoolean(objRates.NotAvailable);
                lblExplain3.Text = objRates.Explanation;
            }

            VMSDAL.VQFSafetyOSHA objOsha = objSafety.SelectSafetyOsha(Convert.ToInt64(objSafetyData.PK_SafetyID), lblOshaYear1.Text);
            oshaCurrentYear = CheckOSHA(objOsha, oshaCurrentYear);
            //lblOshaYear1.Text = currentYear.ToString();
            // G. Vera
            lblOshaYear1.Text = oshaCurrentYear.ToString();

            objOsha = objSafety.SelectSafetyOsha(Convert.ToInt64(objSafetyData.PK_SafetyID), lblOshaYear1.Text);
            if (objOsha != null)
            {
                chkOSHANA1.Checked = Convert.ToBoolean(objOsha.NotAvailable);
                lblExplainOsha1.Text = objOsha.Explanation;
                lblOshafatalities1.Text = objOsha.TotalFatalities;
                lblOshaCase1.Text = objOsha.TotalNoDays;
                lblRecordableCases1.Text = objOsha.TotalNoOtherRecord;
                lblTotalHours1.Text = objOsha.TotalHoursWorked;
                lblJobTranfer1.Text = objOsha.TotalNoJobTransfer;
            }
            lblOshaYear2.Text = (oshaCurrentYear - 1).ToString();
            objOsha = objSafety.SelectSafetyOsha(Convert.ToInt64(objSafetyData.PK_SafetyID), lblOshaYear2.Text);
            if (objOsha != null)
            {
                chkOSHANA2.Checked = Convert.ToBoolean(objOsha.NotAvailable);
                lblExplainOSHA2.Text = objOsha.Explanation;
                lblOshafatalities2.Text = objOsha.TotalFatalities;
                lblOshaCase2.Text = objOsha.TotalNoDays;
                lblRecordableCases2.Text = objOsha.TotalNoOtherRecord;
                lblJobTranfer2.Text = objOsha.TotalNoJobTransfer;
                lblTotalHours2.Text = objOsha.TotalHoursWorked;
            }
            lblOshaYear3.Text = (oshaCurrentYear - 2).ToString();
            objOsha = objSafety.SelectSafetyOsha(Convert.ToInt64(objSafetyData.PK_SafetyID), lblOshaYear3.Text);
            if (objOsha != null)
            {
                chkNAOSHA3.Checked = Convert.ToBoolean(objOsha.NotAvailable);
                lblExplainOSHA3.Text = objOsha.Explanation;
                lblOshafatalities3.Text = objOsha.TotalFatalities;
                lblOshaCase3.Text = objOsha.TotalNoDays;
                lblRecordableCases3.Text = objOsha.TotalNoOtherRecord;
                lblJobTranfer3.Text = objOsha.TotalNoJobTransfer;
                lblTotalHours3.Text = objOsha.TotalHoursWorked;
            }
        }

        #endregion

        if (objInsurance.GetInsuranceCount(VendorID) > 0)
        {
            // Get insurance details
            //002 - changed DAL name
            VMSDAL.VQFInsurance objInsuranceData = objInsurance.GetSingleVQLInsuranceDate(VendorID);
            lblAgentPhone.Text = objInsuranceData.AgentTelephoneNumber;
            lblBonding.Text = objInsuranceData.Bonding_SuretyCompanyName;
            lblBrokerorCompanyName.Text = objInsuranceData.Broker_CompanyName;
            lblCompanyContactName.Text = objInsuranceData.CompanyContactName;
            lblContactPhonenumber.Text = objInsuranceData.ContactTelephoneNumber;
            lblCurrentBondingcapacity.Text = objInsuranceData.CurrentBondingCapacity;
            lblDiseaseEmployee.Text = objInsuranceData.DiseaseEachEmployee;
            lblDiseasePolicy.Text = objInsuranceData.DiseasePolicyLimit;
            lblEachAccident.Text = objInsuranceData.EachAccident;
            lblEachOccurances.Text = objInsuranceData.GeneralEachOccurrence;
            lblEmployees.Text = objInsuranceData.EmployersLiability;
            lblGenralAggregate.Text = objInsuranceData.GeneralAggregate;
            lblInsuranceAgent.Text = objInsuranceData.InsuranceAgent;
            lblInsuranceCarrier.Text = objInsuranceData.InsuranceCarrier;
            lblEachProfessionalOccur.Text = objInsuranceData.ProfessionalEachOccurrence;
            lblPollutionLiability.Text = objInsuranceData.PollutionPolicyAmount != default(decimal?) ? ((decimal)objInsuranceData.PollutionPolicyAmount).ToString("C", CultureInfo.CurrentUICulture) : string.Empty;

            bool? Insured = objInsuranceData.AdditionalInsured;

            //002 - added marine / cargo data
            string typeOfCompany = new clsCompany().GetSingleVQFCompanyData(VendorID).TypeOfCompany;
            if (typeOfCompany.Trim().ToUpper().Equals(VMSDAL.TypeOfCompany.EquipAndOnsiteSupport.ToUpper()) ||
                typeOfCompany.Trim().ToUpper().Equals(VMSDAL.TypeOfCompany.Supplier.ToUpper()))
            {
                lblMarineCargoIns.Text = (objInsuranceData.IsMarineCargoInsurance == true) ? "Yes" : "No";
                lblMarineCargoInlandTransit.Text = (objInsuranceData.IsInlandTransit == true) ? "Yes" : "No";
                lblMarineCargoStorage.Text = (objInsuranceData.IsStorage == true) ? "Yes" : "No";
                lblMarineCargoInstallEquip.Text = (objInsuranceData.IsInstallationOfEquip == true) ? "Yes" : "No";
                lblMarineCargoConvLimit.Text = objInsuranceData.ConveyanceLimitMarineInsurance;
            }
            else
            {
                lblMarineCargoIns.Text = "N/A";
                lblMarineCargoInlandTransit.Text = "N/A";
                lblMarineCargoStorage.Text = "N/A";
                lblMarineCargoInstallEquip.Text = "N/A";
                lblMarineCargoConvLimit.Text = "N/A";
            }

            if (Insured == true)
            {
                lblInsured.Text = "  Yes";
                trnotes.Style["display"] = "";
            }
            else if (Insured == false)
            {
                lblInsured.Text = "  No";
                trnotes.Style["display"] = "None";
            }
            else
            {
                lblInsured.Text = "";
                trnotes.Style["display"] = "None";
            }
            lblIsPolicy.Text = ReturnYesNo(objInsuranceData.IsPolicy);
            lblIsGeneral.Text = ReturnYesNo(objInsuranceData.IsGeneral);
            lblUmbrellaExcess.Text = ReturnYesNo(objInsuranceData.Umbrella_Excess);
            trUmbrella.Style["Display"] = lblUmbrellaExcess.Text.Equals("Yes") ? "" : "none";
            trExcess.Style["Display"] = lblUmbrellaExcess.Text.Equals("Yes") ? "" : "none";
            lblWCPolicy.Text = ReturnYesNo(objInsuranceData.WCPolicy);
            lblUmbrella.Text = objInsuranceData.Umbrella;
            lblExcess.Text = objInsuranceData.Excess;
            lblOPerations.Text = objInsuranceData.Product_CompletedOperations;
            lblTotalBonding.Text = objInsuranceData.TotalBondingCapacity;
        }
        LoadAttchDocumentData();
    }

    private Boolean IsFinancialDataExists(DataSet dataSet, string currentYear)
    {
        DataRow[] dataRow = dataSet.Tables[0].Select("FK_LegalID <> 0 AND Year = '" + currentYear + "'");
        if (dataRow.Length > 0)
        {
            for (int count = 0; count < dataRow.Length; count++)
            {
                if (!dataRow[count].IsNull("NotAvailable"))
                {
                    if (Convert.ToBoolean(dataRow[count]["NotAvailable"]))
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(dataRow[count]["Explanation"])))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(dataRow[count]["ContractValue"])) && string.IsNullOrEmpty(Convert.ToString(dataRow[count]["AnnualRevenue"])))
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private int CheckEMRRates(object rates, int currentYear)
    {
        int year = currentYear;
        DAL.VQFSafetyEMRRate objRates = (DAL.VQFSafetyEMRRate)rates;
        if (objRates == null)
        {
            year = currentYear - 1;
        }
        else
        {
            if (Convert.ToBoolean(objRates.NotAvailable))
            {
                if (string.IsNullOrEmpty(objRates.Explanation))
                {
                    year = currentYear - 1;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(objRates.EMRRate))
                {
                    year = currentYear - 1;
                }
            }
        }
        return year;
    }

    private int CheckOSHA(object osha, int oshaCurrentYear)
    {
        int year = oshaCurrentYear;
        DAL.VQFSafetyOSHA objOsha = (DAL.VQFSafetyOSHA)osha;
        if (objOsha == null)
        {
            year = oshaCurrentYear - 1;
        }
        else
        {
            if (Convert.ToBoolean(objOsha.NotAvailable))
            {
                if (string.IsNullOrEmpty(Convert.ToString(objOsha.Explanation)))
                {
                    year = oshaCurrentYear - 1;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(Convert.ToString(objOsha.TotalFatalities)) 
                                && string.IsNullOrEmpty(Convert.ToString(objOsha.TotalNoDays)) 
                                && string.IsNullOrEmpty(Convert.ToString(objOsha.TotalNoOtherRecord)) 
                                && string.IsNullOrEmpty(Convert.ToString(objOsha.TotalHoursWorked)) 
                                && string.IsNullOrEmpty(Convert.ToString(objOsha.TotalNoJobTransfer)))
                {
                    year = oshaCurrentYear - 1;
                }
            }
        }
        return year;
    }

    //   Vendor Qualification Form - Company
    protected void dlAdditionalLocations_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lbl = new Label();
            lbl = (Label)e.Item.FindControl("lblState");
            Label lblOtherState = new Label();
            lblOtherState = (Label)e.Item.FindControl("lblOtherState");
            if (lbl.Text.Equals("Other"))
            {
                lblOtherState.Visible = true;
                lbl.Visible = false;
            }
            else
            {
                lblOtherState.Visible = false;
                lbl.Visible = true;
            }
        }
    }

    //rptCompanyOfficers_ItemDataBound
    protected void rptCompanyOfficers_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lbl = new Label();
            lbl = (Label)e.Item.FindControl("lblPercentageOwnership");
            if (lbl.Text.Trim() != "")
            {
                lbl.Text = lbl.Text + "%";
            }
            else
            {
                lbl.Text = "";
            }
        }
    }

    protected void rptKeyContact_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lbl = new Label();
            if (e.Item.ItemIndex == 0)
            {
                lbl = (Label)e.Item.FindControl("lblKeyContact");
                lbl.Visible = true;
            }
            else
            {
                lbl = (Label)e.Item.FindControl("lblKeyContact");
                lbl.Visible = false;
            }
        }
    }

    // Vendor Qualification Form - Legal & Financial
    // to display phone numbers in phone format and others for state
    protected void rptBanking_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblState = new Label();
            Label lblOtherState = new Label();
            lblState = (Label)e.Item.FindControl("lblState");
            if (lblState.Text != "0")
            {
                lblState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(lblState.Text)).Tables[0].Rows[0].ItemArray[1]);
            }
            else
            {
                lblState.Text = "";
            }
            lblState.Visible = !lblState.Text.Equals("Other");
            
            Label lblCredit = new Label();
            lblCredit = (Label)e.Item.FindControl("lblCredit");
            lblCredit.Text = ReturnYesNo(lblCredit.Text);
        }
    }

    protected void rptFinancial_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox hdnNa = new CheckBox();
            HtmlTableRow tr = new HtmlTableRow();
            tr = (HtmlTableRow)e.Item.FindControl("trExplain");
            hdnNa = (CheckBox)e.Item.FindControl("chkFinancialNA");
            tr.Style["Display"] = hdnNa.Checked ? "" : "none";
        }
    }

    //  Vendor Qualification Form - Trade Information
    protected void rptLicenseNumber_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lbl = new Label();
            lbl = (Label)e.Item.FindControl("lblExpirationDate");
            if (lbl.Text.Trim() == "1/1/1753")
            {
                lbl.Text = string.Empty;
            }
            Label lblState = new Label();
            lblState = (Label)e.Item.FindControl("lblState");
            Label lblOtherState = new Label();
            lblOtherState = (Label)e.Item.FindControl("lblOtherState");
            if (lblState.Text.Equals("Other"))
            {
                lblState.Visible = false;
                lblOtherState.Visible = true;
            }
            else
            {
                lblState.Visible = true;
                lblOtherState.Visible = false;
            }
        }
    }

    // Vendor Qualification Form - References
    public string ShowState(Byte stateid)
    {
        string state = string.Empty;
        dsstate = objCommon.GetStateCode(stateid);
        if (dsstate.Tables[0].Rows.Count > 0)
        {
            state = dsstate.Tables[0].Rows[0]["StateCode"].ToString();
        }
        return state;
    }

    protected void rptSupl_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ///009
            HtmlControl trowSupplierSpeaksEnglish = (HtmlControl)e.Item.FindControl("trowSupplierSpeaksEnglish");
            ShowHideQuestionInRepeater(trowSupplierSpeaksEnglish);

            Label LblState = new Label();
            LblState = (Label)e.Item.FindControl("LblState");
            Label lblCount = new Label();
            lblCount = (Label)e.Item.FindControl("lblCount1");
            lblCount.Text = Convert.ToString(e.Item.ItemIndex + 1);
            if (LblState.Text != "0")
            {
                LblState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(LblState.Text)).Tables[0].Rows[0].ItemArray[1]);
            }
            else
            {
                LblState.Text = "";
            }
            if (LblState.Text.Equals("Other"))
            {
                Label LblOtherState = new Label();
                LblOtherState = (Label)e.Item.FindControl("LblOtherState");
                LblState.Text = LblOtherState.Text;
                LblOtherState.Text = "";
                HtmlTableRow trOther = new HtmlTableRow();
                trOther = (HtmlTableRow)e.Item.FindControl("trOther");
                //LblState.Text =Convert.ToString((HtmlTableRow)e.Item.FindControl("trOther"));
                trOther.Style["display"] = "none";
            }
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (Request.QueryString.Count > 0)
            {
                if (Request.QueryString["user"] == "admin")
                {
                    HtmlTableCell tdlblsupl = new HtmlTableCell();
                    tdlblsupl = (HtmlTableCell)e.Item.FindControl("tdlblsupl");
                    HtmlTableCell tdtxtsupl = new HtmlTableCell();
                    tdtxtsupl = (HtmlTableCell)e.Item.FindControl("tdtxtsupl");
                }
            }
        }
    }

    protected void rptSupl_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {

    }
   
    protected void rptProjectReferences_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ///009
            HtmlControl trowSpeaksEnglish = (HtmlControl)e.Item.FindControl("trowSpeaksEnglish");
            ShowHideQuestionInRepeater(trowSpeaksEnglish);

            CheckBox chk = new CheckBox();
            chk = (CheckBox)e.Item.FindControl("chkReferencesNA");
            if (chk.Checked)
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trExplain");
                tr.Style["Display"] = "";
            }
            else
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trExplain");
                tr.Style["Display"] = "None";
            }

            if (Convert.ToString(Session["Access"]).Equals("1"))
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trAdminView");
                tr.Style["Display"] = "";

                TextBox txt = new TextBox();
                txt = (TextBox)e.Item.FindControl("txtNotes");
                HiddenField hdn = new HiddenField();
                hdn = (HiddenField)e.Item.FindControl("hdnStatus");
                if (string.IsNullOrEmpty(txt.Text))
                {
                    txt.Style["display"] = "none";
                }
                else
                {
                    hdn.Value = "display";
                    txt.Style["display"] = "";
                }
            }
            else
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trAdminView");
                tr.Style["Display"] = "none";
            }
            Label lblCount = new Label();
            lblCount = (Label)e.Item.FindControl("lblCount");
            lblCount.Text = Convert.ToString(e.Item.ItemIndex + 1);
            if (Convert.ToInt32(lblCount.Text) <= 3)
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trExplainHead");
                tr.Style["Display"] = "";
            }
            else
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trExplainHead");
                tr.Style["Display"] = "None";
            }
            Label lbl = new Label();
            lbl = (Label)e.Item.FindControl("lblState");
            Label lbl1 = new Label();
            lbl1 = (Label)e.Item.FindControl("lblOtherState");
            Common objCommon = new Common();
            if (lbl.Text == "0")
            {
                lbl.Text = string.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(lbl.Text))
                {
                    if (objCommon.GetStateCode(Convert.ToInt16(lbl.Text.Trim())).Tables[0].Rows.Count > 0)
                    {
                        lbl.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(lbl.Text.Trim())).Tables[0].Rows[0][1]);
                    }
                }
            }
            if (lbl.Text.Equals("Other"))
            {
                lbl.Visible = false;
                lbl1.Visible = true;
            }
            else
            {
                lbl.Visible = true;
                lbl1.Visible = false;
            }
        }
    }


    /// <summary>
    /// 009- Show hide question based on Vendor
    /// </summary>
    /// <param name="IsBasedInUS"></param>
    private void ShowHideReferenceSupplierQuestion(bool IsBasedInUS)
    {
        if (IsBasedInUS)
        {
            ShowHideView("none");
        }
        else
        {
            ShowHideView("");
        }
    }


    /// <summary>
    /// 009- Show hide question based on Vendor
    /// </summary>
    /// <param name="IsBasedInUS"></param>
    private void ShowHideQuestionInRepeater(HtmlControl tableRow)
    {
        if (IsBasedInUS)
            tableRow.Style["display"] = "none";
        else
            tableRow.Style["display"] = "";
    }

    /// <summary>
    /// 009
    /// </summary>
    /// <param name="visibleProperty"></param>
    private void ShowHideView(string visibleProperty)
    {
        trowSpeaksEnglishOne.Style["display"] = visibleProperty;
        trowSpeaksEnglishTwo.Style["display"] = visibleProperty;
        trowSpeaksEnglishThree.Style["display"] = visibleProperty;

        trowSupplierSpeaksEnglishOne.Style["display"] = visibleProperty;
        trowSupplierSpeaksEnglishTwo.Style["display"] = visibleProperty;
        trowSupplierSpeaksEnglishThree.Style["display"] = visibleProperty;

    }


    //To unprint the Empty sections 
    private void DisplayHeader()
    {
        if ((lblBrokerorCompanyName.Text == "") && (lblInsuranceCarrier.Text == "") && (lblInsuranceAgent.Text == "") && (lblAgentPhone.Text == ""))
        {
            ContactInformationHeader.Visible = false;
        }
    }

    private void LoadAttchDocumentData()
    {
        Attch = objCommon.GetAttchDocumentData(VendorID,true,false);
        if (Attch.Tables[0].Rows.Count > 0)
        {
            GridAttchView.DataSource = Attch;
            GridAttchView.DataBind();
        }
        else
        {
            tblAttached.Style["Display"] = "none";
        }
    }

    protected void GridAttchView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Lnk")
        {
            Response.Redirect("../Common/GenerateFile.aspx?region=" + e.CommandArgument);
        }
    }

    protected void dlRegions_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblOtherState = (Label)e.Item.FindControl("lstRegions");
            string OtherState = lblOtherState.Text;
            lblOtherState.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + OtherState;
            DAL.UspGetSelectedRegionsResult[] objRegions = null;
            Label lbl = (Label)e.Item.FindControl("lblName");
            switch (lbl.Text)
            {
                case "CA":
                    objRegions = objTradeInformation.GetSelectedTradeRegions(5, tradeId);
                    if (objRegions.Length > 0)
                    {
                        string Regions = string.Empty;
                        for (int i = 0; i < objRegions.Length; i++)
                        {
                            Regions += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Li>" + objRegions[i].Statecode;
                        }
                        Label lblRegions = (Label)e.Item.FindControl("lstRegions");
                        lblRegions.Text = Regions;
                    }
                    break;
                case "FL":
                    objRegions = objTradeInformation.GetSelectedTradeRegions(5, tradeId);
                    if (objRegions.Length > 0)
                    {
                        string Regions = string.Empty;
                        for (int i = 0; i < objRegions.Length; i++)
                        {
                            Regions += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Li>" + objRegions[i].Statecode;
                        }
                        Label lblRegions = (Label)e.Item.FindControl("lstRegions");
                        lblRegions.Text = Regions;
                    }
                    break;
                case "TX":
                    objRegions = objTradeInformation.GetSelectedTradeRegions(43, tradeId);
                    if (objRegions.Length > 0)
                    {
                        string Regions = string.Empty;
                        for (int i = 0; i < objRegions.Length; i++)
                        {
                            Regions += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Li>" + objRegions[i].Statecode;
                        }
                        Label lblRegions = (Label)e.Item.FindControl("lstRegions");
                        lblRegions.Text = Regions;
                    }
                    break;

            }
        }
    }

    private string ReturnYesNo(object value)
    {
        switch (Convert.ToString(value))
        {
            case "True":
            case "true":
                return "Yes";
            case "False":
            case "false":
                return "No";
            default:
                return "";
        }
    }

}