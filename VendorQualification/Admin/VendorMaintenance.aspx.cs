﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using VMSDAL;

#region Change Comments
/*****************************************************************************************************************
 * 001 - G. Vera 06/15/2012 Phase 3 Item 5.6 (JIRA:VPI-23)
 * 002 - G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 003 - G. Vera 07/27/2012: VMS Stabilization Release 1.2 (JIRA:VPI-18)
 * 004 - G. Vera 07/01/2013: Hot Fix on VQF warning notes.  Refer to HDT # 1167
 * 005 - Sooraj Sudhakaran T. 10/04/2013 - Fixes on Tax Id to accept international Business ID/Tax ID \
 * 006 - G. Vera 11/19/2014: Commenting what Sooraj added to convert tax id based in US
 *****************************************************************************************************************/
#endregion

public partial class VendorMaintenance : System.Web.UI.Page
{
    #region Instantiate the class and declare varaibles
    //Instantiate the class
    clsVendorMatch objVendorMatch = new clsVendorMatch();
    clsRegistration objRegistration = new clsRegistration();
    Common objCommon = new Common();
    DataSet SRAttch = new DataSet();                                                            //001
    public bool isBind { get; set; }    //004


    public string SortExp;
    #endregion
    /// <summary>
    /// on page initilization
    /// </summary>
    /// <param name="e"></param>
    /// 
    #region Page Events
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        string str = Request.Form["__EVENTTARGET"];

        //Create controls dynamically
        plcCrntPage.Controls.Clear();
        if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page"]))) && str.ToUpperInvariant() != "DDLNUMBER" && hdnFlag.Value == "0")
        {

            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {

                    LinkButton lnk = new LinkButton();
                    lnk.Click += new EventHandler(lnk_Click);
                    plcCrntPage.Controls.Add(lnk);
                    lnk.Text = Convert.ToString(i);
                    lnk.ID = "lnk" + Convert.ToString(i);

                    lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ")");
                    if (i == Convert.ToInt32(hdnPage.Value))
                    {
                        lnk.CssClass = "Selectlnkcolor";
                    }
                    else { lnk.CssClass = "lnkcolor"; }
                }
            }
        }

    }

    /// <summary>
    /// Page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 

    protected void Page_Load(object sender, EventArgs e)
    {
        //002
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (!Page.IsPostBack)
        {
            //002
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");


            // G. Vera 4/19/2012 - Edit Vendor Number Phase 3 Enhancement
            //  Need to add this client event to ListBox
            ddlVendorNoData.Attributes.Add("onclick", "selectedVendorNos();");


            //Session["Page"] = null;
            hdnFlag.Value = "0";
            hdnPage.Value = null;
            //005 - Sooraj
            txtTaxId2.Attributes.Add("onkeyup", "javascript:SetFocusOnTax(" + txtTaxId2.ClientID + "," + txtTaxId2.ClientID + ")");

            ddlStatus.DataSource = objRegistration.RetrieveVendorStatus();
            ddlStatus.DataTextField = "StatusDesc";
            ddlStatus.DataValueField = "StatusId";
            ddlStatus.DataBind();

            imbSearch.Attributes.Add("onClick", "AssignValue()");
            if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");  //002
            }
            else
            {
                mnuMain.DisplayButton(1);
                if (Convert.ToString(Session["Access"]) == "2")
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                {
                    AdminMenu.DisplayMenu("Admin");
                }
                tdContent.Style["Display"] = "None";
            }
            if (Request.QueryString.Count > 0)
            {

                ddlNumber.SelectedValue = Request.QueryString["PageRecords"];
                hdnPage.Value = Request.QueryString["CurrentPage"];

                // ****************************************************************************************
                // Added by N Schoenberger on 12/5/2011 *****
                if (!string.IsNullOrEmpty(Convert.ToString(Session["VendorSearchDetails"])))
                {
                    string[] BindingValue = Convert.ToString(Session["VendorSearchDetails"]).Split('~');
                    if (BindingValue.Length >= 6)
                    {
                        txtVendorName.Text = BindingValue[0];
                        txtVendorNumber.Text = BindingValue[1];
                       // txtTaxId1.Text = BindingValue[2];
                        //005 - Sooraj
                        txtTaxId2.Text  =BindingValue[3];
                        txtTrackingNumbrt.Text = BindingValue[4];
                        ddlStatus.SelectedValue = BindingValue[5];
                        ddlNumber.SelectedValue = BindingValue[6];
                        hdnSort.Value = BindingValue[7];

                        //Session["VendorSearchDetails"] = null;
                    }
                }
                // ****************************************************************************************

                hdnCount.Value = Convert.ToString(objVendorMatch.GetVendorMaintenanceCount(objCommon.ReplaceStrRevDS(txtVendorName.Text.Trim()), txtVendorNumber.Text.Trim(),  txtTaxId2.Text.Trim(), txtTrackingNumbrt.Text.Trim(), Convert.ToInt16(ddlStatus.SelectedItem.Value)));  //005-sooraj
                int page = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Value))));
                lblTotal.Text = Convert.ToString(hdnCount.Value);
                Session["Page"] = Convert.ToString(page);
                hdnPage.Value = "1";


                //if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
                //{
                //    imbNext.Visible = false;
                //}
                //else
                //{
                //    imbNext.Visible = true;
                //}
                //if ((Convert.ToString(hdnPage.Value) == "1") || (Convert.ToString(hdnPage.Value) == "0"))
                //{
                //    imbPrevious.Visible = false;
                //}
                //else
                //{
                //    imbPrevious.Visible = true;
                //}
                //if (Convert.ToInt32(hdnPage.Value) == Convert.ToInt32(Session["Page"]))
                //{
                //    imbNext.Visible = false;
                //}

                Bind();

                //if ((Convert.ToInt16(ddlStatus.SelectedItem.Value) == 0) || (Convert.ToInt16(ddlStatus.SelectedItem.Value) == 5))
                //{
                //    grdVendor.Columns[2].Visible = true;
                //    grdVendor.Columns[3].Visible = false;
                //}
                //else if ((Convert.ToInt16(ddlStatus.SelectedItem.Value) == 2) || (Convert.ToInt16(ddlStatus.SelectedItem.Value) == 1))
                //{
                //    grdVendor.Columns[2].Visible = false;
                //    grdVendor.Columns[3].Visible = true;
                //}
                //else if (Convert.ToInt16(ddlStatus.SelectedItem.Value) == -2)
                //{
                //    grdVendor.Columns[2].Visible = true;
                //    grdVendor.Columns[3].Visible = true;
                //}
                //else if ((Convert.ToInt16(ddlStatus.SelectedItem.Value) == 6) || ((Convert.ToInt16(ddlStatus.SelectedItem.Value) == 3)))
                //{
                //    grdVendor.Columns[2].Visible = false;
                //    grdVendor.Columns[3].Visible = true;
                //}
                //else
                //{
                //    grdVendor.Columns[2].Visible = false;
                //    grdVendor.Columns[3].Visible = false;
                //}
                //plcCrntPage.Controls.Clear();
                //lblTotalPage.Text = Convert.ToString(Session["Page"]);

                //for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
                //{
                //    if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                //    {
                //        LinkButton lnkCurrent = new LinkButton();
                //        Literal ltr = new Literal();
                //        lnkCurrent.Click += new EventHandler(lnk_Click);

                //        ltr.Text = ",";

                //        lnkCurrent.Text = Convert.ToString(i);

                //        plcCrntPage.Controls.Add(lnkCurrent);
                //        lnkCurrent.Text = Convert.ToString(i);

                //        lnkCurrent.Attributes.Add("onClick", "GetValue(" + lnkCurrent.Text.Trim() + ")");
                //        if (i == Convert.ToInt32(hdnPage.Value))
                //        {
                //            lnkCurrent.CssClass = "Selectlnkcolor";
                //        }
                //        else { lnkCurrent.CssClass = "lnkcolor"; }

                //    }
                //}
                if (ddlStatus.SelectedIndex == 0)
                {
                    imgActive.Visible = true;
                    imgDeactive.Visible = true;
                    imgCom.Visible = true;
                    imgrev.Visible = true;
                }
                else if (ddlStatus.SelectedItem.Text.Trim() == "Complete")
                {
                    imgActive.Visible = false;
                    imgDeactive.Visible = false;
                    imgCom.Visible = false;
                    imgrev.Visible = true;
                }
                else if ((ddlStatus.SelectedItem.Text.Trim() == "Reject"))
                {
                    imgActive.Visible = false;
                    imgDeactive.Visible = true;
                    imgCom.Visible = false;
                    imgrev.Visible = false;

                }
                else if (ddlStatus.SelectedIndex > 0 && (ddlStatus.SelectedItem.Text.Trim() == "InComplete"))
                {
                    imgActive.Visible = true;
                    imgDeactive.Visible = false;
                    imgCom.Visible = false;
                    imgrev.Visible = false;
                }
                else if ((ddlStatus.SelectedIndex > 0) && (ddlStatus.SelectedItem.Text.Trim() == "Expired"))
                {
                    imgActive.Visible = true;
                    imgDeactive.Visible = false;
                    imgCom.Visible = false;
                    imgrev.Visible = false;
                }
                else
                {
                    imgActive.Visible = false;
                    imgDeactive.Visible = false;
                    imgCom.Visible = true;
                    imgrev.Visible = false;
                }


            }
        }

        //Retain the dynamically created controls across postback
        string strPageRequest;
        strPageRequest = Request.Form["__EVENTTARGET"];
        if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page"]))) && (string.IsNullOrEmpty(strPageRequest) || (strPageRequest.ToLower() != "ddlnumber")) && hdnFlag.Value == "0")
        {
            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {
                    LinkButton lnkCurrent = new LinkButton();
                    Literal ltr = new Literal();
                    lnkCurrent.Click += new EventHandler(lnk_Click);

                    ltr.Text = ",";

                    lnkCurrent.Text = Convert.ToString(i);

                    plcCrntPage.Controls.Add(lnkCurrent);
                    lnkCurrent.Text = Convert.ToString(i);
                    lnkCurrent.ID = "lnk" + Convert.ToString(i);

                    lnkCurrent.Attributes.Add("onClick", "GetValue(" + lnkCurrent.Text.Trim() + ")");
                    if (i == Convert.ToInt32(hdnPage.Value))
                    {
                        lnkCurrent.CssClass = "Selectlnkcolor";
                    }
                    else { lnkCurrent.CssClass = "lnkcolor"; }

                }
            }

            //004 need to re-bind after attachment box is closed only.
            if (string.IsNullOrEmpty(strPageRequest))
            {
                string ctrlStr = String.Empty;
                Control c = null;
                Control control = null;
                foreach (string ctl in this.Request.Form)
                {
                    // handle ImageButton controls ... 
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = this.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = this.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                    c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        if (control.ClientID == "uclAttachments_imbAttachClose")
                        {
                            this.isBind = true;
                        }
                        break;
                    }
                }
            }
            if (this.isBind)
            {
                Bind();
                this.isBind = false;
            }

        }
        hdnFlag.Value = "0";


    }

    /// <summary>
    /// Event raised for the dynamically created controls
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void lnk_Click(object sender, EventArgs e)
    {
        LinkButton Currentlnk = new LinkButton();
        Currentlnk = (LinkButton)sender;
        hdnPage.Value = Currentlnk.Text.Trim();

        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text.Trim() == Currentlnk.Text.Trim())
                {
                    lnk1.CssClass = "Selectlnkcolor";

                }
                else { lnk1.CssClass = "lnkcolor"; }
            }

        }
        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if (hdnPage.Value == "1")
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        Bind();


    }


    /// <summary>
    /// Display the records in the next page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbNext_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) + 1);
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text.Trim() == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";

                }
                else { lnk1.CssClass = "lnkcolor"; }
            }

        }
        lblTotalPage.Text = Convert.ToString(Session["Page"]);

        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if ((Convert.ToString(hdnPage.Value) == "1") || (Convert.ToString(hdnPage.Value) == "0"))
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        if (Convert.ToInt32(hdnPage.Value) == Convert.ToInt32(Session["Page"]))
        {
            imbNext.Visible = false;
        }

        Bind();
    }


    /// <summary>
    /// Display the records per page as per the selected value
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPage.Value = "1";
        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Value))));
        Session["Page"] = Convert.ToString(page);
        lblTotalPage.Text = Convert.ToString(Session["Page"]);
        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if ((Convert.ToString(hdnPage.Value) == "1") || (Convert.ToString(hdnPage.Value) == "0"))
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        if (Convert.ToInt32(hdnPage.Value) == Convert.ToInt32(Session["Page"]))
        {
            imbNext.Visible = false;
        }
        Bind();
        plcCrntPage.Controls.Clear();
        for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
        {
            if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
            {
                LinkButton lnk = new LinkButton();
                lnk.Click += new EventHandler(lnk_Click);
                if (i == Convert.ToInt32(hdnPage.Value))
                {
                    lnk.CssClass = "Selectlnkcolor";
                }
                else { lnk.CssClass = "lnkcolor"; }
                plcCrntPage.Controls.Add(lnk);
                lnk.Text = Convert.ToString(i);
                lnk.ID = "lnk" + Convert.ToString(i);
                lnk.Attributes.Add("onClick", "GetValue(" + lnk.Text.Trim() + ")");
            }
        }
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text.Trim() == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";

                }
                else { lnk1.CssClass = "lnkcolor"; }
            }

        }
    }

    /// <summary>
    /// Acativate the selected record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgActivate_Click(object sender, ImageClickEventArgs e)
    {
        int flag = 0;
        lblInformation.Text = string.Empty;
        ArrayList arCompany = new ArrayList();
        ArrayList arStatus = new ArrayList();
        DataSet ExpiredStatus = new DataSet();
        foreach (GridViewRow grd in grdVendor.Rows)
        {
            flag++;
            CheckBox chk = new CheckBox();
            chk = (CheckBox)grd.FindControl("chkVendor");
            if (chk.Checked)
            {
                flag--;
                LinkButton lblCompany = new LinkButton();
                lblCompany = (LinkButton)grd.FindControl("lblCompany");

                Label lbl = new Label();
                lbl = (Label)grd.FindControl("lblstatus");


                if (lbl.Text.Trim().Trim() == "Pending")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }
                if (lbl.Text.Trim().Trim() == "InProgress")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }
                if (lbl.Text.Trim().Trim() == "Complete")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }
                if (lbl.Text.Trim().Trim() == "Revision")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }
                if (lbl.Text.Trim().Trim() == "Expired")
                {
                    //arCompany.Add(lblCompany.Text.Trim());
                    //arStatus.Add(lbl.Text.Trim());

                    HiddenField hdn = new HiddenField();
                    hdn = (HiddenField)grd.FindControl("hdnVendorId");
                    ExpiredStatus = objCommon.GetVendorDetails(Convert.ToInt64(hdn.Value));
                    if (Convert.ToByte(ExpiredStatus.Tables[0].Rows[0]["ExpiryStatus"]) == 0)
                    {
                        objRegistration.ExpiredActivateVendor(Convert.ToInt64(hdn.Value), 6, Convert.ToByte(ExpiredStatus.Tables[0].Rows[0]["VendorStatus"]));
                    }
                    objRegistration.ActivateVendor(Convert.ToInt64(hdn.Value), Convert.ToByte(ExpiredStatus.Tables[0].Rows[0]["ExpiryStatus"]), 0);
                    lblInformation.Text = "<li>status of vendor can be changed to activate status</li>";
                    ModalInfo.Show();
                }
                else if ((lbl.Text.Trim().Trim() == "InComplete") || (lbl.Text.Trim().Trim() == "Reject"))
                {
                    HiddenField hdn = new HiddenField();
                    hdn = (HiddenField)grd.FindControl("hdnVendorId");
                    objRegistration.ActivateVendor(Convert.ToInt64(hdn.Value), 0, 0);
                    lblInformation.Text = "<li>status of vendor can be changed to activate status</li>";
                    ModalInfo.Show();

                }


            }


        }
        int i = 0;
        foreach (string company in arCompany)
        {
            lblInformation.Text += "<LI>" + arStatus[i] + " status of " + arCompany[i] + " cannot be changed to activate status </LI>";

            i += 1;

        }
        if (i > 0)
        {
            ModalInfo.Show();
        }
        if (flag == grdVendor.Rows.Count)
        {
            lblInformation.Text = "<li>Select at least one option to change the status</li>";
            ModalInfo.Show();
        }
        Bind();


    }

    /// <summary>
    /// Deactivate the selected record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgAccept_Click(object sender, ImageClickEventArgs e)
    {
        int flag = 0;
        ArrayList arCompany = new ArrayList();
        ArrayList arStatus = new ArrayList();
        lblInformation.Text = string.Empty;
        foreach (GridViewRow grd in grdVendor.Rows)
        {
            flag++;
            CheckBox chk = new CheckBox();
            chk = (CheckBox)grd.FindControl("chkVendor");
            if (chk.Checked)
            {
                flag--;
                LinkButton lblCompany = new LinkButton();
                lblCompany = (LinkButton)grd.FindControl("lblCompany");

                Label lbl = new Label();
                lbl = (Label)grd.FindControl("lblstatus");

                //SGS changes On 08/25/2011
                //if (lbl.Text.Trim().Trim() == "Pending")
                //{
                //    arCompany.Add(lblCompany.Text.Trim());
                //    arStatus.Add(lbl.Text.Trim());
                //}
                //if (lbl.Text.Trim().Trim() == "InProgress")
                //{
                //    arCompany.Add(lblCompany.Text.Trim());
                //    arStatus.Add(lbl.Text.Trim());
                //}
                //if (lbl.Text.Trim().Trim() == "Complete")
                //{
                //    arCompany.Add(lblCompany.Text.Trim());
                //    arStatus.Add(lbl.Text.Trim());
                //}
                //if (lbl.Text.Trim().Trim() == "Revision")
                //{
                //    arCompany.Add(lblCompany.Text.Trim());
                //    arStatus.Add(lbl.Text.Trim());
                //}
                //if (lbl.Text.Trim().Trim() == "Expired")
                //{
                //    arCompany.Add(lblCompany.Text.Trim());
                //    arStatus.Add(lbl.Text.Trim());

                //}
                //else if ((lbl.Text.Trim().Trim() == "InComplete") ||
                //


                //SGS changes On 08/25/2011
                if (lbl.Text.Trim().Trim() == "Reject")
                {
                    HiddenField hdn = new HiddenField();
                    hdn = (HiddenField)grd.FindControl("hdnVendorId");
                    objRegistration.ActivateVendor(Convert.ToInt64(hdn.Value), 2, 0);
                    ManagerMail(Convert.ToInt64(hdn.Value));
                    lblInformation.Text = "<li>Select at least one option to change the status</li>";
                    ModalInfo.Show();
                    Bind();
                }
                else
                {
                    lblInformation.Text += "<LI>" + lbl.Text + " status of " + lblCompany.Text + " cannot be changed to accept status </LI>";
                    ModalInfo.Show();
                }

                /////
            }

        }
        //int i = 0;
        //foreach (string company in arCompany)
        //{
        //    lblInformation.Text += "<LI>" + arStatus[i] + " status of " + arCompany[i] + " cannot be changed to accept status </LI>";

        //    i += 1;

        //}
        //if (i > 0)
        //{
        //    ModalInfo.Show();
        //}
        //if (flag == grdVendor.Rows.Count)
        //{
        //    lblInformation.Text = "<li>Select at least one option to change the status</li>";
        //    ModalInfo.Show();
        //}
        //Bind();
    }


    #endregion
    /// <summary>
    /// Bind the data in the gridview
    /// </summary>
    private void Bind()
    {
        SortExp = hdnSort.Value;
        if (SortExp == null)
        {
            SortExp = "";
        }
        //005 - Sooraj  modified for International taxid 
        grdVendor.DataSource = objVendorMatch.GetVendorMaintenance(objCommon.ReplaceStrRevDS(txtVendorName.Text.Trim()), txtVendorNumber.Text.Trim(), txtTaxId2.Text.Trim(), txtTrackingNumbrt.Text.ToString(), Convert.ToInt16(ddlStatus.SelectedItem.Value), Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.SelectedItem.Text.Trim()), SortExp);
        grdVendor.DataBind();
        lblTotal.Text = hdnCount.Value;
        if (grdVendor.Rows.Count <= 0)
        {
            tdContent.Style["Display"] = "None";
            lbldisplaymsgInfo.Text = "Sorry no records found";
        }
        else
        {
            tdContent.Style["Display"] = "";
            lbldisplaymsgInfo.Text = string.Empty;
        }
    }
    /// <summary>
    /// Sorting the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;
        if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortDirection"])))
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
        else if (Convert.ToString(ViewState["SortDirection"]) == Convert.ToString(SortDirection.Ascending))
        {

            GridViewSortDirection = SortDirection.Descending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Descending);
            SortGridView(sortExpression, "DESC");

        }

        else
        {

            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
    }

    /// <summary>
    /// Get sorting direction
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
            {
                ViewState["sortDirection"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["sortDirection"];
        }

        set
        {
            ViewState["sortDirection"] = value;
        }
    }

    /// <summary>
    /// Display limited Character in a cell
    /// </summary>
    /// <param name="desc"></param>
    /// <returns></returns>
    public string GetVendorName(string desc)
    {
        if (desc.Length > 20)
        {
            return desc.Substring(0, 20) + "...";// +desc.Substring(20);
        }
        else
        {
            return desc;
        }
    }
    /// <summary>
    /// Method to sort gridview
    /// </summary>
    /// <param name="sortExpression"></param>
    /// <param name="direction"></param>
    private void SortGridView(string sortExpression, string direction)
    {
        SortExp = sortExpression + " " + direction;
        hdnSort.Value = SortExp;
        Bind();

    }
    /// <summary>
    /// Display the records in the previous page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrevious_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) - 1);
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                if (lnk1.Text.Trim() == hdnPage.Value)
                {
                    lnk1.CssClass = "Selectlnkcolor";

                }
                else { lnk1.CssClass = "lnkcolor"; }
            }

        }
        lblTotalPage.Text = Convert.ToString(Session["Page"]);

        if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
        {
            imbNext.Visible = false;
        }
        else
        {
            imbNext.Visible = true;
        }
        if ((Convert.ToString(hdnPage.Value) == "1") || (Convert.ToString(hdnPage.Value) == "0"))
        {
            imbPrevious.Visible = false;
        }
        else
        {
            imbPrevious.Visible = true;
        }
        if (Convert.ToInt32(hdnPage.Value) == Convert.ToInt32(Session["Page"]))
        {
            imbNext.Visible = false;
        }

        Bind();
    }

    //005 - Sooraj
    /// <summary>
    /// Validate the TaxId
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    //protected void custValidator_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    if ((string.IsNullOrEmpty(txtTaxId2.Text.Trim())))
    //    {
    //        custValidator.ErrorMessage = "Please enter valid tax ID";
    //        args.IsValid = false;
    //    }
    //    else
    //    {
    //        args.IsValid = true;
    //    }

    //}

    /// <summary>
    /// Display the record based on the filtered category
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void imbSearch_Click(object sender, ImageClickEventArgs e)
    {
        // 004
        Session["VendorSearchDetails"] = null;
        Page.Validate();
        if (Page.IsValid)
        {
            //005 - Sooraj
            hdnCount.Value = Convert.ToString(objVendorMatch.GetVendorMaintenanceCount(objCommon.ReplaceStrRevDS(txtVendorName.Text.Trim()), txtVendorNumber.Text.Trim(),  txtTaxId2.Text.Trim(), txtTrackingNumbrt.Text.Trim(), Convert.ToInt16(ddlStatus.SelectedItem.Value)));
            int page = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Value))));
            lblTotal.Text = Convert.ToString(hdnCount.Value);
            Session["Page"] = Convert.ToString(page);
            hdnPage.Value = "1";


            if (Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value))
            {
                imbNext.Visible = false;
            }
            else
            {
                imbNext.Visible = true;
            }
            if ((Convert.ToString(hdnPage.Value) == "1") || (Convert.ToString(hdnPage.Value) == "0"))
            {
                imbPrevious.Visible = false;
            }
            else
            {
                imbPrevious.Visible = true;
            }
            if (Convert.ToInt32(hdnPage.Value) == Convert.ToInt32(Session["Page"]))
            {
                imbNext.Visible = false;
            }

            Bind();
            //001 - Changed column visibility from 2 to 4 and from 3 to 5 since extra columns were added.
            if ((Convert.ToInt16(ddlStatus.SelectedItem.Value) == 0) || (Convert.ToInt16(ddlStatus.SelectedItem.Value) == 5))
            {
                grdVendor.Columns[4].Visible = true;
                grdVendor.Columns[5].Visible = false;
            }
            else if ((Convert.ToInt16(ddlStatus.SelectedItem.Value) == 2) || (Convert.ToInt16(ddlStatus.SelectedItem.Value) == 1))
            {
                grdVendor.Columns[4].Visible = false;
                grdVendor.Columns[5].Visible = true;
            }
            else if (Convert.ToInt16(ddlStatus.SelectedItem.Value) == -2)
            {
                grdVendor.Columns[4].Visible = true;
                grdVendor.Columns[5].Visible = true;
            }
            else if ((Convert.ToInt16(ddlStatus.SelectedItem.Value) == 6) || ((Convert.ToInt16(ddlStatus.SelectedItem.Value) == 3)))
            {
                grdVendor.Columns[4].Visible = false;
                grdVendor.Columns[5].Visible = true;
            }
            else
            {
                grdVendor.Columns[4].Visible = false;
                grdVendor.Columns[5].Visible = false;
            }
            plcCrntPage.Controls.Clear();
            lblTotalPage.Text = Convert.ToString(Session["Page"]);

            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {
                    LinkButton lnkCurrent = new LinkButton();
                    Literal ltr = new Literal();
                    lnkCurrent.Click += new EventHandler(lnk_Click);

                    ltr.Text = ",";

                    lnkCurrent.Text = Convert.ToString(i);

                    plcCrntPage.Controls.Add(lnkCurrent);
                    lnkCurrent.Text = Convert.ToString(i);
                    lnkCurrent.ID = "lnk" + Convert.ToString(i);
                    lnkCurrent.Attributes.Add("onClick", "GetValue(" + lnkCurrent.Text.Trim() + ")");
                    if (i == Convert.ToInt32(hdnPage.Value))
                    {
                        lnkCurrent.CssClass = "Selectlnkcolor";
                    }
                    else { lnkCurrent.CssClass = "lnkcolor"; }

                }
            }
            if (ddlStatus.SelectedIndex == 0)
            {
                imgActive.Visible = true;
                imgDeactive.Visible = true;
                imgCom.Visible = true;
                imgrev.Visible = true;
            }
            else if (ddlStatus.SelectedItem.Text.Trim() == "Complete")
            {
                imgActive.Visible = false;
                imgDeactive.Visible = false;
                imgCom.Visible = false;
                imgrev.Visible = true;
            }
            else if ((ddlStatus.SelectedItem.Text.Trim() == "Reject"))
            {
                imgActive.Visible = false;
                imgDeactive.Visible = true;
                imgCom.Visible = false;
                imgrev.Visible = false;

            }
            else if (ddlStatus.SelectedIndex > 0 && (ddlStatus.SelectedItem.Text.Trim() == "InComplete"))
            {
                imgActive.Visible = true;
                imgDeactive.Visible = false;
                imgCom.Visible = false;
                imgrev.Visible = false;
            }
            else if ((ddlStatus.SelectedIndex > 0) && (ddlStatus.SelectedItem.Text.Trim() == "Expired"))
            {
                imgActive.Visible = true;
                imgDeactive.Visible = false;
                imgCom.Visible = false;
                imgrev.Visible = false;
            }
            else
            {
                imgActive.Visible = false;
                imgDeactive.Visible = false;
                imgCom.Visible = true;
                imgrev.Visible = false;
            }
        }
        else
        {
            lblInformation.Text = Errormsg();
            ModalInfo.Show();
        }
    }

    /// <summary>
    /// Validate page and return the validation message
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {

            if (!validator.IsValid)
            {
                errmsg += "<li>" + validator.ErrorMessage + "</li>";
            }
        }
        return errmsg;

    }

    /// <summary>
    /// Display the label text in the expected format
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.Header)
        {
            CheckBox chb = new CheckBox();
            chb = (CheckBox)e.Row.FindControl("chkSelectAll");
            chb.Attributes.Add("onclick", "javascript:SelectAll('" + chb.ClientID + "')");
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            #region Handle warning image and SR link
            //001 - Start
            HiddenField hdnVendorId = new HiddenField();
            hdnVendorId = (HiddenField)e.Row.FindControl("hdnImgVendorId");
            LinkButton lnkSRRort = new LinkButton();
            lnkSRRort = (LinkButton)e.Row.FindControl("lnkSRRort");
            Image imgTag = new Image();
            imgTag = (Image)e.Row.FindControl("imgTag");
            SRAttch = objCommon.GetAttchDocumentData(Convert.ToInt64(hdnVendorId.Value), false, true);
            DataTable dtCommentsNTag = new clsSearchVendor().GetCommentsAndTag(Convert.ToInt64(hdnVendorId.Value));
            string tag;

            // handle the SR attachment(s) link
            if (Convert.ToInt32(SRAttch.Tables[0].Rows.Count) > 0)
            {
                lnkSRRort.Enabled = true;
                lnkSRRort.CssClass = "green";
            }
            else
            {
                lnkSRRort.Enabled = true;
                lnkSRRort.CssClass = "grey";
            }
            // handle the warning image(s)
            if (dtCommentsNTag.Rows.Count > 0)
            {
                tag = (dtCommentsNTag.Rows[0]["Tag"] != typeof(DBNull)) ? dtCommentsNTag.Rows[0]["Tag"].ToString() : null;

                if (string.IsNullOrEmpty(tag))
                {
                    imgTag.Visible = false;
                }
                else if (tag.Equals("0"))
                {
                    imgTag.Visible = true;
                    imgTag.ImageUrl = @"../Images/red.png";
                    HtmlGenericControl panel = (HtmlGenericControl)e.Row.FindControl("gridPopup1");
                    string showPopup = "ShowPopup('" + panel.ClientID + "')";
                    string hidePopup = "HidePopup('" + panel.ClientID + "')";
                    Label lblComments = new Label();
                    lblComments = (Label)e.Row.FindControl("lbtngrdPrint1");
                    lblComments.Text = Convert.ToString(dtCommentsNTag.Rows[0]["AddNotes"]);
                    e.Row.Cells[1].Attributes.Add("onmouseover", "javascript:" + showPopup);
                    e.Row.Cells[1].Attributes.Add("onmouseout", "javascript:" + hidePopup);
                }
                else if (tag.Equals("1"))
                {
                    imgTag.Visible = true;
                    imgTag.ImageUrl = @"../Images/yellow.png";
                    HtmlGenericControl panel = (HtmlGenericControl)e.Row.FindControl("gridPopup1");
                    string showPopup = "ShowPopup('" + panel.ClientID + "')";
                    string hidePopup = "HidePopup('" + panel.ClientID + "')";
                    Label lblComments = new Label();
                    lblComments = (Label)e.Row.FindControl("lbtngrdPrint1");
                    lblComments.Text = Convert.ToString(dtCommentsNTag.Rows[0]["AddNotes"]);
                    e.Row.Cells[1].Attributes.Add("onmouseover", "javascript:" + showPopup);
                    e.Row.Cells[1].Attributes.Add("onmouseout", "javascript:" + hidePopup);
                }
                else if (tag.Equals("2"))
                {
                    imgTag.Visible = false;
                }
                else
                {
                    imgTag.Visible = false;
                }
            }
            else
            {
                imgTag.Visible = false;
            }
            //001 - End
            #endregion

            Label lbl1 = new Label();
            lbl1 = (Label)e.Row.FindControl("lblVendorNumber");
            if (lbl1.Text.Trim() == "0")
            {
                lbl1.Text = string.Empty;
            }
            Label lbl = new Label();
            lbl = (Label)e.Row.FindControl("lblTaxId");
            CheckBox lblIsBasedInUS = new CheckBox();
            lblIsBasedInUS = (CheckBox)e.Row.FindControl("ChkIsBasedInUS");
            string strVendorNo = lbl.ToolTip.Trim();
            if (strVendorNo.Length > 0)
            {
                //005-sooraj Commented 

                //lbl.Text = strVendorNo.Substring(0, 2);
                //lbl.Text = lbl.Text.Trim() + " - " + strVendorNo.Substring(2);
                lbl.Text = strVendorNo.ConvertToTaxId(lblIsBasedInUS.Checked).Trim().Substring(0, strVendorNo.Length > 20 ? 20 : strVendorNo.Length);

                if (lblIsBasedInUS.Checked)
                    lbl.Text = strVendorNo.ConvertToTaxId(lblIsBasedInUS.Checked).Substring(0, strVendorNo.Length > 20 ? 20 : strVendorNo.Length + 1); //003
                else
                    lbl.Text = strVendorNo.ConvertToTaxId(lblIsBasedInUS.Checked).Substring(0, strVendorNo.Length > 20 ? 20 : strVendorNo.Length); //003

                lbl.ToolTip = strVendorNo.ConvertToTaxId(lblIsBasedInUS.Checked);
              
            }

            // G. Vera 04/19/2012 - Phase 3 Enhancement to edit vendor number
            LinkButton lnkEdit = new LinkButton();
            Label lblStat = new Label();
            lnkEdit = (LinkButton)e.Row.FindControl("lnkEditVendorNo");
            lblStat = (Label)e.Row.FindControl("lblstatus");

            string strJDEVendNo = lnkEdit.Text.Trim();
            string stat = lblStat.Text.Trim();

            if (strJDEVendNo.Length <= 0)
            {
                lnkEdit.Text = "Edit";
            }
            // G. Vera 08/18/2014:  Dianne D. requested to allow edit on any status
            //if (stat == "InProgress" || stat == "InComplete")
            //{
            //    lnkEdit.Enabled = false;
            //}

        }
    }

    /// <summary>
    /// Delete the selected record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgOK_Click(object sender, ImageClickEventArgs e)
    {
        int Count = 0;
        int deleteCount = 0;

        foreach (GridViewRow grd in grdVendor.Rows)
        {
            CheckBox chk = (CheckBox)grd.FindControl("chkVendor");
            if (chk.Checked)
            {
                Count += 1;

                HiddenField hdnVendorId = (HiddenField)grd.FindControl("hdnVendorId");
                deleteCount += objRegistration.Deletevendor(Convert.ToInt64(hdnVendorId.Value));
            }
        }

        if (Count == 0)
        {
            lblInformation.Text = "<li> Select at least one option to delete the record </li>";
            ModalInfo.Show();
        }

        else if (Count == deleteCount)
        {
            lblInformation.Text = "<li> Record deleted successfully </li>";
            ModalInfo.Show();
        }
        else if (deleteCount == 0)
        {
            lblInformation.Text = "<li>Error while deleting the record</li>";
            ModalInfo.Show();
        }
        else if (Count <= deleteCount)
        {
            lblInformation.Text = "<li>Record deleted successfully</li>";
            ModalInfo.Show();
        }
        Bind();
    }

    /// <summary>
    /// Confirmation before deleting the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        int Count = 0;


        foreach (GridViewRow grd in grdVendor.Rows)
        {
            CheckBox chk = (CheckBox)grd.FindControl("chkVendor");
            if (chk.Checked)
            {
                Count += 1;
            }
        }
        if (Count == 0)
        {
            lblInformation.Text = "<li> Select at least one option to delete the record </li>";
            ModalInfo.Show();
        }
        else
        {
            lblConfirm.Text = "Are you sure you want to delete the record(s)?";
            modalConfirm.Show();
        }
    }

    /// <summary>
    /// Complete the selected record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgCom_Click(object sender, ImageClickEventArgs e)
    {
        lblInformation.Text = string.Empty;
        ArrayList arCompany = new ArrayList();
        ArrayList arStatus = new ArrayList();
        int flag = 0;


        foreach (GridViewRow grd in grdVendor.Rows)
        {

            flag++;
            CheckBox chk = new CheckBox();
            chk = (CheckBox)grd.FindControl("chkVendor");
            if (chk.Checked)
            {
                flag++;
                LinkButton lblCompany = new LinkButton();
                lblCompany = (LinkButton)grd.FindControl("lblCompany");

                Label lbl = new Label();
                lbl = (Label)grd.FindControl("lblstatus");
                if (lbl.Text.Trim().Trim() == "Expired")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());

                }
                if (lbl.Text.Trim().Trim() == "InProgress")
                {
                    HiddenField hdn = new HiddenField();
                    hdn = (HiddenField)grd.FindControl("hdnVendorId");
                    int Complete = GetcompleteCount(Convert.ToInt64(hdn.Value));
                    if (Complete == 6)
                    {
                        objRegistration.UpdateVendorBit(Convert.ToInt64(hdn.Value), 2);
                        ManagerMail(Convert.ToInt64(hdn.Value));
                    }
                    else
                    {
                        arCompany.Add(lblCompany.Text.Trim());
                        arStatus.Add(lbl.Text.Trim().Trim());
                    }
                }

                if (lbl.Text.Trim().Trim() == "Pending")
                {
                    HiddenField hdn = new HiddenField();
                    hdn = (HiddenField)grd.FindControl("hdnVendorId");
                    int Complete = GetcompleteCount(Convert.ToInt64(hdn.Value));
                    if (Complete == 6)
                    {
                        objRegistration.UpdateVendorBit(Convert.ToInt64(hdn.Value), 2);
                        ManagerMail(Convert.ToInt64(hdn.Value));
                    }
                    else
                    {
                        arCompany.Add(lblCompany.Text.Trim());
                        arStatus.Add(lbl.Text.Trim().Trim());
                    }
                }
                if (lbl.Text.Trim().Trim() == "Complete")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }
                if (lbl.Text.Trim().Trim() == "Reject")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }
                if (lbl.Text.Trim().Trim() == "InComplete")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }

                if (lbl.Text.Trim().Trim() == "Revision")
                {
                    HiddenField hdn = new HiddenField();
                    hdn = (HiddenField)grd.FindControl("hdnVendorId");
                    int Complete = GetcompleteCount(Convert.ToInt64(hdn.Value));
                    if (Complete == 6)
                    {
                        objRegistration.UpdateVendorBit(Convert.ToInt64(hdn.Value), 2);
                        ManagerMail(Convert.ToInt64(hdn.Value));
                    }
                    else
                    {
                        arCompany.Add(lblCompany.Text.Trim());
                        arStatus.Add(lbl.Text.Trim());
                    }

                }
            }
        }
        int i = 0;
        foreach (string company in arCompany)
        {
            if ((Convert.ToString(arStatus[i]) == "Revision") || (Convert.ToString(arStatus[i]) == "InProgress") || (Convert.ToString(arStatus[i]) == "Pending"))
            {

                lblInformation.Text += "<Li>" + arCompany[i] + " has not completed all the information required and so cannot change the status as complete </LI>";
            }
            else
            {
                lblInformation.Text += "<LI>" + arStatus[i] + " status of " + arCompany[i] + " cannot be changed to completed status </LI>";
            }
            i += 1;

        }
        if (i > 0)
        {
            ModalInfo.Show();
        }
        if (flag == grdVendor.Rows.Count)
        {
            lblInformation.Text = "<li>Select at least one option to change the status</li>";
            ModalInfo.Show();
        }

        Bind();
    }

    /// <summary>
    /// Get the count of records in completed status for particular vendor id
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public int GetcompleteCount(long VendorId)
    {
        Common ObjVQFStatus = new Common();

        return Convert.ToInt32(ObjVQFStatus.GetVQFStatusData(Convert.ToInt64(VendorId), 1).Tables[0].Rows.Count);
    }

    /// <summary>
    /// Change status to revision for the seleceted record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgrev_Click(object sender, ImageClickEventArgs e)
    {
        int flag = 0;
        lblInformation.Text = string.Empty;
        ArrayList arCompany = new ArrayList();
        ArrayList arStatus = new ArrayList();

        foreach (GridViewRow grd in grdVendor.Rows)
        {
            flag++;
            CheckBox chk = new CheckBox();
            chk = (CheckBox)grd.FindControl("chkVendor");
            if (chk.Checked)
            {
                flag++;
                LinkButton lblCompany = new LinkButton();
                lblCompany = (LinkButton)grd.FindControl("lblCompany");

                Label lbl = new Label();
                lbl = (Label)grd.FindControl("lblstatus");
                if (lbl.Text.Trim().Trim() == "Expired")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());

                }
                if (lbl.Text.Trim().Trim() == "Pending")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }
                if (lbl.Text.Trim().Trim() == "InProgress")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }
                if (lbl.Text.Trim().Trim() == "Revision")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }
                if (lbl.Text.Trim().Trim() == "Reject")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }
                if (lbl.Text.Trim().Trim() == "InComplete")
                {
                    arCompany.Add(lblCompany.Text.Trim());
                    arStatus.Add(lbl.Text.Trim());
                }

                if (lbl.Text.Trim().Trim() == "Complete")
                {
                    HiddenField hdn = new HiddenField();
                    hdn = (HiddenField)grd.FindControl("hdnVendorId");
                    objRegistration.UpdateVendorBit(Convert.ToInt64(hdn.Value), 3);
                }
            }
        }
        int i = 0;
        foreach (string company in arCompany)
        {
            lblInformation.Text += "<LI>" + arStatus[i] + " status of " + arCompany[i] + " cannot be changed to revision status </LI>";

            i += 1;

        }
        if (i > 0)
        {
            ModalInfo.Show();
        }
        if (flag == grdVendor.Rows.Count)
        {
            lblInformation.Text = "<li>Select at least one option to change the status</li>";
            ModalInfo.Show();
        }
        Bind();
    }

    protected void grdVendor_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        LinkButton lnk = new LinkButton();
        lnk = (LinkButton)grdVendor.Rows[e.NewSelectedIndex].FindControl("lblCompany");
        HiddenField hdn = new HiddenField();
        hdn = (HiddenField)grdVendor.Rows[e.NewSelectedIndex].FindControl("hdnVendorId");
        Session["VendorAdminId"] = hdn.Value;

        // ****************************************************************************************
        // Added by N Schoenberger on 12/5/2011 *****
        if (Convert.ToString(Session["Access"]) == "1")
        {
            //005 - Sooraj
            Session["VendorSearchDetails"] = txtVendorName.Text.ToString() + "~" + txtVendorNumber.Text.ToString() + "~" + txtTaxId2.Text.ToString() + "~" + txtTaxId2.Text.ToString() + "~" + txtTrackingNumbrt.Text.ToString() + "~" + ddlStatus.SelectedValue.ToString() + "~" + ddlNumber.SelectedValue.ToString() + "~" + hdnSort.Value.ToString();
            Response.Redirect("../VQFView/CompanyView.aspx?user=admin&match=match&PageRecords=" + ddlNumber.SelectedItem.Text.Trim() + "&CurrentPage=" + hdnPage.Value + "&Maintain=Maintain");
        }
        else
        {
            //005 - Sooraj
            Session["VendorSearchDetails"] = txtVendorName.Text.ToString() + "~" + txtVendorNumber.Text.ToString() + "~" + txtTaxId2.Text.ToString() + "~" + txtTaxId2.Text.ToString() + "~" + txtTrackingNumbrt.Text.ToString() + "~" + ddlStatus.SelectedValue.ToString() + "~" + ddlNumber.SelectedValue.ToString() + "~" + hdnSort.Value.ToString();
            Response.Redirect("../VQFView/CompanyView.aspx?user=employee");
        }
        // ****************************************************************************************
    }

    private void ManagerMail(long VendorID)
    {
        DataSet ManagerMail = objCommon.GetVQFManagerAutoMail(Convert.ToInt64(VendorID));
        if (ManagerMail.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ManagerMail.Tables[0].Rows.Count; i++)
            {
                StringBuilder Body = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='100%'>");
                Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Hi, <br></font></td></tr>");
                Body.Append("<tr><td style='Padding-left:25'><font  face='verdana' size='2'>This is to inform you that" + " " + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + " " + " has successfully completed VQF.</font></td></tr>");
                Body.Append("<tr><td>&nbsp;</td></tr>");
                //Body.Append("<tr><td><font  face='verdana' size='2'>Thanks,</font></td></tr>");
                //Body.Append("<tr><td><font  face='verdana' size='2'> " + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + "</font><br></td></tr></table>");
                objCommon.SendMail(Convert.ToString(ManagerMail.Tables[0].Rows[i]["vendorEmail"]), Convert.ToString(ManagerMail.Tables[0].Rows[i]["ManagerEmail"]), "" + Convert.ToString(ManagerMail.Tables[0].Rows[i]["Company"]) + " - VQF Status - Complete", Body);
                objCommon.UpdateVQFManagerAutoMail(Convert.ToInt64(ManagerMail.Tables[0].Rows[i]["NotificationID"]));
            }
        }
    }

    //G. Vera 4/19/2012 - Edit Vendor Number Phase 3 Enhancement
    protected void lnkEditVendorNo_Click(object sender, EventArgs e)
    {
        int Count = 0;
        foreach (GridViewRow grd in grdVendor.Rows)
        {
            CheckBox chk = (CheckBox)grd.FindControl("chkVendor");
            if (chk.Checked)
            {
                Count += 1;
            }
        }

        if (Count <= 0)
        {
            lblInformation.Text = "<li> Select at least one vendor to edit </li>";
            ModalInfo.Show();
        }
        else if (Count > 1)
        {
            lblInformation.Text = "<li> Only one vendor can be selected for this function </li>";
            ModalInfo.Show();
        }
        else
        {
            HiddenField hdn = new HiddenField();
            string vendorNumber = String.Empty;
            foreach (GridViewRow row in grdVendor.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chkVendor");
                if (chk.Checked)
                {
                    hdn = (HiddenField)row.FindControl("hdnVendorId");
                    vendorNumber = ((LinkButton)row.FindControl("lnkEditVendorNo")).Text;
                    break;
                }
            }

            // Handle the Vendor number drop down or textbox.
            //DataTable vendor = objVendorMatch.GetVendorMaintenance(objCommon.ReplaceStrRevDS(txtVendorName.Text.Trim()), txtVendorNumber.Text.Trim(), txtTaxId1.Text.Trim() + txtTaxId2.Text.Trim(), txtTrackingNumbrt.Text.ToString(), Convert.ToInt16(ddlStatus.SelectedItem.Value), Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.SelectedItem.Text.Trim()), SortExp);
            DataSet vendor = objCommon.GetVendorDetails(Convert.ToInt64(hdn.Value));
            clsVendorMatch vm = new clsVendorMatch();
            //005- Sooraj
            List<DAL.HaskellJDE> tempList = vm.HaskellJDEByTaxID(vendor.Tables[0].Rows[0]["TaxID"].ConvertToString().TrimEnd('~'));
            if (tempList.Count <= 0) tempList = vm.HaskellJDEByName(vendor.Tables[0].Rows[0]["Company"] as String);

            txtVendorNoData.Visible = true;
            txtVendorNoData.Text = (vendorNumber != "Edit") ? vendorNumber : String.Empty;
            ddlVendorNoData.Visible = true;
            List<DAL.HaskellJDE> dsList = new List<DAL.HaskellJDE>();

            if (tempList.Count <= 0) ddlVendorNoData.Visible = false;

            else
            {
                ddlVendorNoData.Items.Clear();
                ddlVendorNoData.DataTextField = "VendorName";
                ddlVendorNoData.DataValueField = "VendorNumber";

                DAL.HaskellJDE listObj;
                foreach (var item in tempList)
                {
                    //ddlVendorNoData.Items.Add(item.VendorNumber);
                    listObj = new DAL.HaskellJDE();
                    listObj.VendorName = item.VendorNumber + " (" + item.VendorName + ")";
                    listObj.VendorNumber = item.VendorNumber;
                    dsList.Add(listObj);
                }
            }

            ddlVendorNoData.DataSource = dsList;
            ddlVendorNoData.DataBind();
            lblCompanyData.Text = vendor.Tables[0].Rows[0]["Company"] as String;
            string state = objCommon.GetStateCode((Int16)vendor.Tables[0].Rows[0]["State"]).Tables[0].Rows[0]["StateCode"] as String;
            lblLocationData.Text = vendor.Tables[0].Rows[0]["City"] as String + ", " + vendor.Tables[0].Rows[0]["ZipCode"] as String;

            if (vendor.Tables[0].Rows[0]["TaxID"] != null)
            {
              //  Sooraj commented 
              //  lblTaxIdData.Text = vendor.Tables[0].Rows[0]["TaxID"].ToString().Substring(0, 2) + " - " + vendor.Tables[0].Rows[0]["TaxID"].ToString().Substring(2);
                var IsBasedInUS = vendor.Tables[0].Rows[0]["IsBasedInUS"].ConvertToString().ConvertToBool();
                lblTaxIdData.Text = vendor.Tables[0].Rows[0]["TaxID"].ToString().ConvertToTaxId(IsBasedInUS);

            }
            else lblTaxIdData.Text = String.Empty;



            modalEditvendorNo.Show();


        }
    }
    /// <summary>
    /// G. Vera 04/19/2012 - Phase 3 Enhancement for Admins to edit vendor number.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EVN")
        {
            string vendorId = e.CommandArgument.ToString();
            Session["CurrVendorId"] = vendorId;
            string currentVendorNumber = ((LinkButton)e.CommandSource).Text;
            Session["CurrVendorNumbers"] = currentVendorNumber;

            DataSet vendor = objCommon.GetVendorDetails(Convert.ToInt64(vendorId));
            clsVendorMatch vm = new clsVendorMatch();
            // using the vendor name and not the tax ID to pull HaskellJDE records as the currently set up process
            // to check if JDE = Yes or No is using the vendor name as key field.  Refer to "CheckVendorNumberExists" stored proc.
            //005-Sooraj
            bool IsBasedINUS = vendor.Tables[0].Rows[0]["IsBasedInUS"].ConvertToString().ConvertToBool();
            //006 - GV : List<DAL.HaskellJDE> tempList = vm.HaskellJDEByTaxID(vendor.Tables[0].Rows[0]["TaxID"].ConvertToString().ConvertToTaxId(IsBasedINUS));
            List<DAL.HaskellJDE> tempList = vm.HaskellJDEByTaxID(vendor.Tables[0].Rows[0]["TaxID"].ConvertToString());

            if (tempList.Count <= 0) tempList = vm.HaskellJDEByName(vendor.Tables[0].Rows[0]["Company"] as String);

            txtVendorNoData.Visible = true;
            ddlVendorNoData.Visible = true;
            List<DAL.HaskellJDE> dsList = new List<DAL.HaskellJDE>();

            if (tempList.Count <= 0)
            {
                ddlVendorNoData.Visible = false;
                // if tax id is empty it might be an international vendor.
                //005- Sooraj
                if (!String.IsNullOrEmpty(vendor.Tables[0].Rows[0]["TaxID"].ConvertToString().TrimEnd('~'))) lblEditWarning.Visible = true;
                else lblEditWarning.Visible = false;
            }
            else
            {
                lblEditWarning.Visible = false;
                ddlVendorNoData.Items.Clear();
                ddlVendorNoData.DataTextField = "VendorName";
                ddlVendorNoData.DataValueField = "VendorNumber";

                DAL.HaskellJDE listObj;
                foreach (var item in tempList)
                {
                    listObj = new DAL.HaskellJDE();
                    listObj.VendorName = item.VendorNumber.Trim() + " (" + item.VendorName + ")";
                    listObj.VendorNumber = item.VendorNumber.Trim();
                    dsList.Add(listObj);
                }
            }

            ddlVendorNoData.DataSource = dsList;
            ddlVendorNoData.DataBind();

            // fill the display controls
            txtVendorNoData.Text = (currentVendorNumber != "Edit") ? currentVendorNumber : String.Empty;
            lblCompanyData.Text = vendor.Tables[0].Rows[0]["Company"] as String;
            //G. Vera 08/18/2014: Added check since now vendor might not have selected a state yet.
            string state = string.Empty;
            if (!String.IsNullOrEmpty(vendor.Tables[0].Rows[0]["FK_State"] as string))
            {
                state = objCommon.GetStateCode((Int16)vendor.Tables[0].Rows[0]["FK_State"]).Tables[0].Rows[0]["StateCode"] as String;
            }
            else
            {
                state = "NOT AVAILABLE";
            }
            lblLocationData.Text = vendor.Tables[0].Rows[0]["City"] as String + ", " + state + " " + vendor.Tables[0].Rows[0]["ZipCode"] as String;
            if (vendor.Tables[0].Rows[0]["TaxID"] != null)
            {
                //005-sooraj commented
               // lblTaxIdData.Text = vendor.Tables[0].Rows[0]["TaxID"].ToString().Substring(0, 2) + " - " + vendor.Tables[0].Rows[0]["TaxID"].ToString().Substring(2);
                var IsBasedInUS = vendor.Tables[0].Rows[0]["IsBasedInUS"].ConvertToString().ConvertToBool();
             
                lblTaxIdData.Text = vendor.Tables[0].Rows[0]["TaxID"].ToString().ConvertToTaxId(IsBasedInUS);

            }
            else lblTaxIdData.Text = String.Empty;

            Session["CurrVendorNo"] = txtVendorNoData.Text;

            modalEditvendorNo.Show();
        }

        //001
        if (e.CommandName == "SR")
        {

            ((HiddenField)uclAttachments.FindControl("hdnSRAddDoc")).Value = "true";
            ((HiddenField)uclAttachments.FindControl("hdnInAddDoc")).Value = "false";
            ((Label)uclAttachments.FindControl("lblattach")).Text = "";
            ViewState["VendorID"] = e.CommandArgument;

            ((HtmlControl)uclAttachments.FindControl("tdSRattach")).Style["Display"] = "";

            uclAttachments.SRLoadAttchDocumentData(Convert.ToInt32(e.CommandArgument));

            ((HtmlControl)uclAttachments.FindControl("tblVendors")).Style["Display"] = "none";
            ((HtmlControl)uclAttachments.FindControl("tblAdmin")).Style["Display"] = "none";
            if (uclAttachments.SRAttch.Tables[0].Rows.Count == 0)
            {
                ((HtmlControl)uclAttachments.FindControl("trAttachHeader")).Style["Display"] = "";
            }
            if (Convert.ToString(Session["Access"]).Equals("2"))
            {
                ((HtmlControl)uclAttachments.FindControl("trAttachHeader")).Style["Display"] = "none";
                ((HtmlControl)uclAttachments.FindControl("trAttachDocument")).Style["Display"] = "none";

                ((HtmlControl)uclAttachments.FindControl("tdSRattach")).Style["Display"] = "";
                ((Image)uclAttachments.FindControl("imgClose")).Visible = true;

            }

            Session["VendorId"] = Convert.ToInt32(ViewState["VendorID"]);
            // 004
            //005 -Sooraj
            Session["VendorSearchDetails"] = txtVendorName.Text.ToString() + "~" + txtVendorNumber.Text.ToString() + "~" + txtTaxId2.Text.ToString() + "~" + txtTaxId2.Text.ToString() + "~" + txtTrackingNumbrt.Text.ToString() + "~" + ddlStatus.SelectedValue.ToString() + "~" + ddlNumber.SelectedValue.ToString() + "~" + hdnSort.Value.ToString();
            ((AjaxControlToolkit.ModalPopupExtender)uclAttachments.FindControl("ModalAddDoc")).Show();
        }
        else
        {
            ((HiddenField)uclAttachments.FindControl("hdnSRAddDoc")).Value = "false";
        }

    }

    /// <summary>
    ///  G. Vera 4/19/2012 - Edit Vendor Number Phase 3 Enhancement added
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSaveVendorNumber_Click(object sender, ImageClickEventArgs e)
    {

        // we only want to save if vendor number actually changed.
        if (Session["CurrVendorNo"].ToString() != txtVendorNoData.Text.Trim())
        {

            string[] newVendNums = null;
            string[] oldVendNums = null;
            string[] taxIds = lblTaxIdData.Text.Split('-');
              string taxId="";
              if (taxIds.Length > 1)
                  taxId = taxIds[0].Trim() + taxIds[1].Trim();
              else
                  taxId = taxIds[0];
            string vendorId = Session["CurrVendorId"].ToString();
            string company = lblCompanyData.Text.Trim();
            int update1 = 0;
            bool update2 = false;
            bool hasJDE = true;     //006 GV : per Dianne, she does not add numbers that are not in JDE.  This flag will always be true: ddlVendorNoData.Visible;

            clsVendorMatch vMatch = new clsVendorMatch();

            // first split the input
            if (txtVendorNoData.Text.Contains(','))
            {
                newVendNums = txtVendorNoData.Text.Split(',');
                newVendNums = this.TrimItems(newVendNums);
            }
            // we need to know what the old vendor numbers were and handle them respectively
            if (!String.IsNullOrEmpty(Session["CurrVendorNumbers"] as String))
            {
                // split the current stored vendor numbers
                oldVendNums = Session["CurrVendorNumbers"].ToString().Split(',');
                oldVendNums = this.TrimItems(oldVendNums);
            }
            // verify that we have multiple numbers
            if (newVendNums != null)
            {
                if (ValidateVendNo(newVendNums))
                {
                    lblEditWarning.Visible = false;

                    #region Delete Old Vendor Numbers
                    // first lets inactivate/activate vendor number(s)
                    foreach (string item in oldVendNums)
                    {
                        if (!newVendNums.Contains(item.Trim()))
                        {
                            vMatch.DeleteVendorNumber(long.Parse(vendorId), item.Trim());
                        }
                    }
                    // we need to handle where vendor has duplicate numbers.
                    if (oldVendNums.Count() > 1)
                    {

                        for (int i = 0; i < oldVendNums.Length; i++)
                        {
                            string vNum = oldVendNums[i];
                            int count = (from t in oldVendNums.AsEnumerable() where t.Equals(vNum) select t).Count();
                            if (count > 1)
                            {
                                vMatch.DeleteVendorNumber(long.Parse(vendorId), oldVendNums[i]);
                            }

                        }
                    }
                    #endregion

                    foreach (string vendNo in newVendNums)
                    {
                        //003 - changed all this
                        if (!oldVendNums.Contains(vendNo))
                        {
                            vMatch.EditVendorNumber(company, taxId, vendNo.Trim(), hasJDE, clsVendorMatch.ActiveFlag.Active);
                        }

                    }
                }
                else
                {
                    lblEditWarning.InnerHtml = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Incorrect vendor number format";
                    lblEditWarning.Visible = true;
                    modalEditvendorNo.Show();
                }

            }
            else
            {
                if (ValidateVendNo(txtVendorNoData.Text.Trim()))
                {
                    lblEditWarning.Visible = false;

                    #region Delete old vendor numbers
                    // first lets inactivate/activate/Delete vendor number(s)
                    foreach (string item in oldVendNums)
                    {
                        if (!txtVendorNoData.Text.Trim().Contains(item.Trim()))
                        {
                            vMatch.DeleteVendorNumber(long.Parse(vendorId), item.Trim());
                        }
                    }

                    // we need to handle where vendor has duplicate numbers.
                    if (oldVendNums.Count() > 1)
                    {
                        for (int i = 0; i < oldVendNums.Length; i++)
                        {
                            string vNum = oldVendNums[i];
                            int count = (from t in oldVendNums.AsEnumerable() where t.Equals(vNum) select t).Count();
                            if (count > 1)
                            {
                                vMatch.DeleteVendorNumber(long.Parse(vendorId), oldVendNums[i]);
                            }

                        }
                    }

                    #endregion

                    update1 = vMatch.EditVendorNumber(company, taxId, txtVendorNoData.Text.Trim(), hasJDE, clsVendorMatch.ActiveFlag.Active);
                    //001 - not needed update2 = vMatch.InsertVendorNumber(long.Parse(vendorId), txtVendorNoData.Text.Trim());
                }
                else
                {
                    lblEditWarning.InnerHtml = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Incorrect vendor number format";
                    lblEditWarning.Visible = true;
                    modalEditvendorNo.Show();
                }
            }

            Bind();
        }
    }

    /// <summary>
    /// Will remove all the trailing spaces of each array item.
    /// G. Vera 4/19/2012 Edit Vendor Number Phase 3 Enhancements - Added
    /// </summary>
    /// <param name="oldVendNums"></param>
    /// <returns></returns>
    private string[] TrimItems(string[] arrayToFix)
    {
        string[] result = new string[arrayToFix.Count()];

        for (int i = 0; i < arrayToFix.Length; i++)
        {
            result[i] = arrayToFix[i].Trim();
        }

        return result;
    }

    private bool ValidateVendNo(string[] nums)
    {
        try
        {
            foreach (var item in nums)
            {
                int result;
                int.TryParse(item, out result);
                if (result == 0) return false;
            }

            return true;

        }
        catch (Exception ex)
        {
            return false;
        }
    }

    private bool ValidateVendNo(string p)
    {
        try
        {
            if (!String.IsNullOrEmpty(p.Trim()))
            {
                int result;
                int.TryParse(p, out result);
                if (result == 0) return false;
                else return true;
            }
            else return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }



}
