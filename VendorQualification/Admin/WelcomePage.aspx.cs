﻿using System;

public partial class Admin_WelcomePage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //G. Vera 07/03/2012 - Added code below
            if (!String.IsNullOrEmpty(Request.QueryString["timeout"]))
            {
                lblMsg.Text = "<li>" + "Your session has expired!" + "</li>";
                modalExtnd.Show();
            }

            if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
            {
                Response.Redirect("~/Admin/Login.aspx");
            }
            else
            {
                if (Convert.ToString(Session["Access"]) == "2")
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                { AdminMenu.DisplayMenu("Admin"); }
               
            }
        }
    }

    /// <summary>
    /// G. Vera 07/05/2012 - Added
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbOK_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        Response.Redirect("~/Admin/WelcomePage.aspx");
    }
}