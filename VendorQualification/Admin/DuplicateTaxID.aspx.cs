﻿using System;
using System.Configuration;
using System.Web.UI;
using System.Text;
using VMSDAL;
#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 Sooraj Sudhakaran.T 10/04/2013: VMS Modification - Added code to handle International Business Id
 *
 ****************************************************************************************************************/
#endregion

public partial class Admin_DuplicateTaxID : CommonPage
{
    //Instantiate the object for the classes used in this page
    clsRegistration objRegistration = new clsRegistration();
    clsCompany objVendor = new clsCompany();
    clsAdminLogin objAdmin = new clsAdminLogin();
    Common objCommon = new Common();

    /// <summary>
    /// Page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        //Check for the session variables if null redirect to the login page else proceed.
        if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
        {
            //001
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        ShowHideTaxControls();
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }

        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            //Check for the session variables if null redirect to the login page else proceed.
            if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
            {
                //G. Vera 07/3/2012 - Added
                if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
                {
                    Response.Redirect("~/Admin/Login.aspx?timeout=1");
                }
                Response.Redirect("~/Admin/Login.aspx");
            }
            else
            {
                if (Convert.ToString(Session["Access"]) == "2")
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                {
                    AdminMenu.DisplayMenu("Admin");
                }
                mnuMain.DisplayButton(5);
                txtTaxId.Attributes.Add("onkeyup", "javascript:SetFocusOnTax(" + txtTaxId.ClientID + "," + txtTaxId1.ClientID + ")");
            }
        }
    }

    /// <summary>
    /// Display the tax control based on selected Country
    /// </summary>
    /// <Author>Sooraj Sudhakaran.T</Author>
    /// <Date>3-OCT-2013</Date>
    private void ShowHideTaxControls()
    {
        if (ddlCountry.SelectedIndex == 0)//USA
        {
            txtBusinessTax.Text = string.Empty;
            trUSA.Style["display"] = "";
            trOther.Style["display"] = "none";
        }
        else
        {
            txtTaxId.Text = string.Empty;
            txtTaxId1.Text = string.Empty;
            trOther.Style["display"] = "";
            trUSA.Style["display"] = "none";
        }

    }

    /// <summary>
    /// Method to send e-mail
    /// </summary>
    /// <param name="FirstName"></param>
    /// <param name="LastName"></param>
    /// <param name="email"></param>
    private void MailContant()
    {
        StringBuilder Body = new StringBuilder("<table cellpadding='3' cellspacing='0'  width='70%'>");

        Body.Append("<tr><td><font  face='verdana' size='2'>Hi " + txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim() + ", <br></font></td></tr>");
        Body.Append("<tr><td  colspan='2'><font  face='verdana' size='2'>Welcome to Haskell!</font></td></tr>");
        Body.Append("<tr><td style='Padding-left:25px'><font  face='verdana' size='2'>You have successfully registered with us.<br></font></td></tr>");
        Body.Append("<tr><td><font  face='verdana' size='2'>Kindly complete the Vendor Qualification Form within 90 days from today to continue the association with us. <br></font></td></tr>");
        Body.Append("<tr><td style='Padding-left:25px'><font  face='verdana' size='2'>FirstName: " + txtFirstName.Text.Trim() + "<br></font></td></tr>");
        Body.Append("<tr><td style='Padding-left:25px'><font  face='verdana' size='2'>LastName : " + txtLastName.Text.Trim() + "<br></font></td></tr>");
        Body.Append("<tr><td style='Padding-left:25px'><font  face='verdana' size='2'>Email    : " + txtEmailNew.Text.Trim() + "<br></font></td></tr>");
        Body.Append("<tr><td style='Padding-left:25px'><font  face='verdana' size='2'>Password : " + txtPasswordNew.Text.Trim() + "<br><br><br></font></td></tr>");
        Body.Append("<tr><td><font  face='verdana' size='2'>Respectfully,</font><br/><br/></td></tr>");
        Body.Append("<tr><td><font  face='verdana' size='2'>Haskell</font><br></td></tr></table>");
        Common objCommon = new Common();
        //Send e-mail
        objCommon.SendMail(ConfigurationManager.AppSettings["adminemail"].ToString(), txtEmailNew.Text.Trim(), "Successful registration with Haskell", Body);
    }

    /// <summary>
    /// Method to Update the profile of the selected vendor
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbsubmit_Click(object sender, ImageClickEventArgs e)
    {
        Page.Validate();
        if (Page.IsValid)
        {
            //bool bResult = objRegistration.UpdateVendorPassword(txtFirstName.Text, txtLastName.Text, Convert.ToInt32(hdnVendorId.Value), txtEmail.Text, objCommon.encode(txtNewPassword.Text), hdnPassword.Value);
            string count = objRegistration.CheckRedundancy(txtCompanyName.Text.Trim(), " ", txtEmailNew.Text.Trim(),true);
            //Check taxId already exists. If already exists throw a msg stating taxid already exists
             if (count == "Correct")
             {
                 //the above code was commented and below code was included by SGS for Task no : 13
                 //Call method to update vendor details
                 long VendorId = 0;
                 if (ddlCountry.SelectedIndex == 0) //002-Sooraj
                  VendorId = objVendor.InsertVendor(txtCompanyName.Text.Trim().ToUpper(), txtTaxId.Text.Trim() + txtTaxId1.Text.Trim(), txtFirstName.Text.Trim(), txtLastName.Text.Trim(), txtEmailNew.Text.Trim(), objCommon.encode(txtPasswordNew.Text.Trim()),true,true);
                 else
                  VendorId = objVendor.InsertVendor(txtCompanyName.Text.Trim().ToUpper(),txtBusinessTax.Text, txtFirstName.Text.Trim(), txtLastName.Text.Trim(), txtEmailNew.Text.Trim(), objCommon.encode(txtPasswordNew.Text.Trim()),true,false);

                 MailContant();
                 lblErrMsg.Text = "<li>Vendor saved successfully";
                 modalExtnd.Show();
                 txtEmailNew.Text = string.Empty;
                 //hdnemail.Value = txtEmail.Text;
                 txtFirstName.Text = string.Empty;
                 txtLastName.Text = string.Empty;
                 txtTaxId.Text = string.Empty;
                 txtTaxId1.Text = string.Empty;
                 txtBusinessTax.Text = string.Empty; //002-Sooraj
                 ddlCountry.SelectedIndex = 0;
                 txtPasswordNew.Attributes["value"] = "";
                 txtCompanyName.Text = string.Empty;
             }
             else
             {
                 lblErrMsg.Text = count;
                 modalExtnd.Show();
             }
        }
        else
        {
            lblErrMsg.Text = Errormsg();
            modalExtnd.Show();
        }
    }

    /// <summary>
    /// Method to validate the page and return the result
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                errmsg += "<li>" + validator.ErrorMessage + "</li>";
            }
        }
        return errmsg;
    }
        
    //validate federal tax id
    protected void Custax_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if (((txtTaxId.Text == "") && ((txtTaxId1.Text == "")) && ((txtBusinessTax.Text == ""))))
        {
            Custax.ErrorMessage = "Please enter tax ID";
            args.IsValid = false;
        }
        else
        {
            if (ddlCountry.SelectedIndex == 0)
            {
                bool isValidFormat = objCommon.Validatefederaltaxformat(txtTaxId.Text, txtTaxId1.Text);
                if ((isValidFormat == false))
                {
                    Custax.ErrorMessage = "Please enter tax ID in valid format";
                    args.IsValid = false;
                }
            }
        }
    }
}