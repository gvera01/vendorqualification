﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using VMSDAL;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 10/26/2012: VMS Stabilization Release 1.3 (see JIRA issues for more details)
 *
 ****************************************************************************************************************/
#endregion

public partial class VQFView_InsuranceBondingView : System.Web.UI.Page
{
    #region Declaration

    clsInsurance objInsurance = new clsInsurance();
    
    Common objCommon = new Common();
    long VendorId;

    #endregion

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        //Show or hide view based on Type of company - sooraj
        SwitchDesignConsultantView();
               
       
        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            if (Request.QueryString.Count > 0)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Print"]))
                {
                    VQFViewMenu.Visible = false;
                }
            }
            //check for vendor id. if null redirect to home page.
          
                if ((string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])) || Convert.ToString(Session["VendorAdminId"]) == "0"))
                {
                    Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001

                }
                else
                { VendorId = Convert.ToInt64(Session["VendorAdminId"]);
                hdVendorId.Value = Convert.ToInt64(VendorId).ToString();
                }

               
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["match"])))
                    {
                        
                        imgprint.Attributes.Add("onClick", "printPartOfPageFromMatch()");
                    }
                    else
                    {
                        
                        imgprint.Attributes.Add("onClick", "printPartOfPage()");
                    }
                
            // get company details
            if (!string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])))
            {
                clsRegistration objregistration = new clsRegistration();
                DAL.UspGetVendorDetailsByIdResult[] objResult = objregistration.GetVendor(Convert.ToInt64((Session["VendorAdminId"])));
                if (objResult.Count() > 0)
                {
                    lblVendorName.Text = objResult[0].Company;
                }

            }
           
          
            if (objInsurance.GetInsuranceCount(VendorId) > 0)
            {
                // Get insurance details
                VMSDAL.VQFInsurance objInsuranceData = objInsurance.GetSingleVQLInsuranceDate(VendorId);
                lblAgentPhone.Text = objInsuranceData.AgentTelephoneNumber;
                lblBonding.Text = objInsuranceData.Bonding_SuretyCompanyName;
                lblBrokerorCompanyName.Text = objInsuranceData.Broker_CompanyName;
                lblCompanyContactName.Text = objInsuranceData.CompanyContactName;
                lblContactPhonenumber.Text = objInsuranceData.ContactTelephoneNumber;
                lblCurrentBondingcapacity.Text = objInsuranceData.CurrentBondingCapacity;
                lblDiseaseEmployee.Text = objInsuranceData.DiseaseEachEmployee;
                lblDiseasePolicy.Text = objInsuranceData.DiseasePolicyLimit;
                lblEachAccident.Text = objInsuranceData.EachAccident;
                lblEachOccurances.Text = objInsuranceData.GeneralEachOccurrence;
                lblEmployees.Text = objInsuranceData.EmployersLiability;
                lblGenralAggregate.Text = objInsuranceData.GeneralAggregate;
                lblInsuranceAgent.Text = objInsuranceData.InsuranceAgent;
                lblInsuranceCarrier.Text = objInsuranceData.InsuranceCarrier;
                lblEachProfessionalOccur.Text = objInsuranceData.ProfessionalEachOccurrence;
                lblPollutionLiability.Text = objInsuranceData.PollutionPolicyAmount != default(decimal?) ? ((decimal)objInsuranceData.PollutionPolicyAmount).ToString("C", CultureInfo.CurrentUICulture) : string.Empty;
                lblPollutionLiabExplain.Text = objInsuranceData.PollutionPolicyExplain ?? "";
                bool? Insured = objInsuranceData.AdditionalInsured;

                if (Insured == true)
                {
                    lblInsured.Text = " Yes";
                    trnotes.Style["display"] = "";
                }
                else if (Insured == false)
                {
                    lblInsured.Text = " No";
                    trnotes.Style["display"] = "None";
                }
                else
                {
                    lblInsured.Text = "";
                    trnotes.Style["display"] = "None";
                }

                bool? isPolicy = objInsuranceData.IsPolicy;
                if (isPolicy == true)
                {
                    lblOccurrenceBased.Text = "Yes";
                }
                else if (isPolicy == false)
                { lblOccurrenceBased.Text = "No"; }

                bool? isGeneral = objInsuranceData.IsGeneral;
                if (isGeneral == true)
                {
                    lblGeneralAggregateLimit.Text = "Yes";
                }
                else if (isGeneral == false)
                { lblGeneralAggregateLimit.Text = "No"; }
                bool? isUmbrellaExcess = objInsuranceData.Umbrella_Excess;
                if (isUmbrellaExcess == true)
                {
                    lblUmbrellaExcess.Text = "Yes";
                    trUmbrella.Style["Display"] = "";
                    trExcess.Style["Display"] = "";

                }
                else if (isUmbrellaExcess == false)
                { lblUmbrellaExcess.Text = "No";
                trUmbrella.Style["Display"] = "None";
                trExcess.Style["Display"] = "None";
                }

                bool? isWCPolicy = objInsuranceData.WCPolicy;
                if (isWCPolicy == true)
                {
                    lblWCPolicy.Text = "Yes";
                  

                }
                else if (isWCPolicy == false)
                {
                    lblWCPolicy.Text = "No";
                  
                }

                lblUmbrella.Text = objInsuranceData.Umbrella;
                lblExcess.Text = objInsuranceData.Excess;

                lblOPerations.Text = objInsuranceData.Product_CompletedOperations;
              //  lblStateRequirements.Text = objInsuranceData.StateRequirements;
                lblTotalBonding.Text = objInsuranceData.TotalBondingCapacity;
                //DisplayHeader();

                //002 - added marine / cargo data
                lblMarineCargoIns.Text = (objInsuranceData.IsMarineCargoInsurance == true) ? "Yes" : (objInsuranceData.IsMarineCargoInsurance != null) ? "No" : "";
                lblMarineCargoInlandTransit.Text = (objInsuranceData.IsInlandTransit == true) ? "Yes" : (objInsuranceData.IsInlandTransit != null) ? "No" : "";
                lblMarineCargoStorage.Text = (objInsuranceData.IsStorage == true) ? "Yes" : (objInsuranceData.IsStorage != null) ? "No" : "";
                lblMarineCargoInstallEquip.Text = (objInsuranceData.IsInstallationOfEquip == true) ? "Yes" : (objInsuranceData.IsStorage != null) ? "No" : "";
                lblMarineCargoConvLimit.Text = objInsuranceData.ConveyanceLimitMarineInsurance;
                lblConveyanceLimitInlandTrans.Text = objInsuranceData.ConveyanceLimitInlandTransit;

                if (lblMarineCargoIns.Text == "No") trConveyanceLimitMarineCargo.Attributes["style"] = "display: none";
                if (lblMarineCargoInlandTransit.Text == "No") trConveyanceLimitInlandTrans.Attributes["style"] = "display: none";

            }
        }


    }

    #endregion

    /// <summary>
    /// Switch view based on logged in user
    /// </summary>
    /// <param name="IsShow"></param>
    /// <Author>Sooraj Sudhakaran.T</Author>
    ///  <Date>20-SEP-2013</Date>
    private void SwitchDesignConsultantView()
    {
        //Design Consultant 
        bool IsShow = (Session["VendorCompanyType"].ConvertToString() == TypeOfCompany.DesignConsultant) ? true : false;
        tblBondingAndSurety.Style["display"] = IsShow ? "none" : "";
    }



    //To unprint the Empty sections 

    private void DisplayHeader()
    {
        if ((lblBrokerorCompanyName.Text == "") && (lblInsuranceCarrier.Text == "") && (lblInsuranceAgent.Text == "") && (lblAgentPhone.Text == ""))
        {
            ContactInformationHeader.Visible = false;
        }
        if ((lblEachOccurances.Text == "") && (lblOPerations.Text == "") && (lblGenralAggregate.Text == "") && (lblInsured.Text == "") && (lblEachAccident.Text == "") && (lblWCPolicy.Text == "") && (lblDiseasePolicy.Text == "") && (lblEmployees.Text == "") 
            && (lblDiseaseEmployee.Text == "") && (lblEachProfessionalOccur.Text == "") && (lblPollutionLiability.Text == "") && (lblPollutionLiabExplain.Text == ""))
        {
           // AggregateLimitHeader.Visible = false;
        }
        if ((lblBonding.Text == "") && (lblTotalBonding.Text == "") && (lblCompanyContactName.Text == "") && (lblCurrentBondingcapacity.Text == "") && (lblContactPhonenumber.Text == ""))
        {
           // BondingHeader.Visible = false;
        }
    }

    #region Button Events

    protected void imgPDF_Click(object sender, ImageClickEventArgs e)
    {
        //002
        Common objCommon = new Common();
        string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/VQF/PrintVQFForm.aspx?vendorid=" + hdVendorId.Value + "&print=1";
        string baseURI = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;

        objCommon.Print2PDF(url, baseURI);
        //objCommon.GeneratePDF(Convert.ToInt64(hdVendorId.Value), Convert.ToString(Request.QueryString["user"]));
    }

    #endregion


}
