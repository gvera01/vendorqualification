﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 
 *
 ****************************************************************************************************************/
#endregion

public partial class VQFView_Files : System.Web.UI.Page
{
    Common objCommon = new Common();
    long VendorID;
    string DOCUMENT_LOCATION = string.Empty;
    DataSet Attch = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        DOCUMENT_LOCATION = ConfigurationManager.AppSettings["VQFDocument"].ToString();

        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }

        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            if (Request.QueryString.Count > 0)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Print"]))
                {
                    VQFViewMenu.Visible = false;
                }
            }
            //check for vendor id. if null redirect to home page.
            if ((string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])) || Convert.ToString(Session["VendorAdminId"]) == "0"))
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
            }
            else
            {
                VendorID = Convert.ToInt64(Session["VendorAdminId"]);
            }
            // get company details
            if (!string.IsNullOrEmpty(Convert.ToString(VendorID)))
            {
                clsRegistration objregistration = new clsRegistration();
                DAL.UspGetVendorDetailsByIdResult[] objResult = objregistration.GetVendor(Convert.ToInt64(VendorID));
                if (objResult.Count() > 0)
                {
                    lblVendorName.Text = objResult[0].Company;
                }
            }
        }
        LoadAttchDocumentData();
    }

    private void LoadAttchDocumentData()
    {
        Attch = objCommon.GetAttchDocumentData(VendorID, false,false);
        if (Attch.Tables[0].Rows.Count > 0)
        {
            GridAttchView.DataSource = Attch;
            GridAttchView.DataBind();
        }
        else
        {
            GridAttchView.Visible = false;
            lblMessage.Text = "No Files uploaded";
        }
    }

    protected void GridAttchView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
        if (e.CommandName == "Lnk")
        {
            //SGS Changes on 08/11/2011 
            Response.Redirect("../Common/GenerateFile.aspx?region=" + DOCUMENT_LOCATION + e.CommandArgument);
            ///
        }

    }

}