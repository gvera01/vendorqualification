﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Files.aspx.cs" Inherits="VQFView_Files" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>

    <script language="javascript" type="text/javascript">
          window.onerror = ErrHndl;

          function ErrHndl()
          { return true; }
    </script>

    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <link href="../css/PrintStyle.css" rel="stylesheet" type="text/css" />
    <link href="../css/ScreenStyle.css" rel="stylesheet" type="text/css" />

    <script src="../Script/jquery.js" type="text/javascript"></script>

    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache">

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
  <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
        .style1
        {
        }
    </style>
    <style type="text/css">
        body
        {
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="625px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bodytextboldHead left_borderView">
                            <asp:Label ID="lblVendorName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="contenttdview">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <Haskell:VQFViewMenu ID="VQFViewMenu" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0" class="formhead">
                                                        <tr>
                                                            <td width="50%">
                                                                Vendor Qualification Form - Attached Document
                                                            </td>
                                                            <td style="text-align: right" width="50%">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0"
                                                        width="100%" class="searchtableclass">
                                                        <tr>
                                                            <td class="Header">
                                                                Attached Document files
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="5" cellspacing="3" width="100%">
                                                                    <tr>
                                                                        <td class="left_border">
                                                                            <asp:GridView runat="server" ID="GridAttchView" OnRowCommand="GridAttchView_RowCommand"
                                                                                DataKeyNames="Pk_documentID" AllowSorting="True" AutoGenerateColumns="False"
                                                                                CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="0" Width="100%">
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <tr>
                                                                                                <td class="bodytextcenter" width="8px">
                                                                                                    <%# Eval("Row")%>.
                                                                                                </td>
                                                                                                <td class="bodytextleft" width="400px">
                                                                                                    <asp:LinkButton ID="lnkdowload" runat="server" CommandName="Lnk" CssClass="hyplinks"
                                                                                                        CommandArgument='<%#Eval("Filepath")%>'><%#Eval("FileName")%></asp:LinkButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="errormsg" align="center">
                                                                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="450px">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <Haskell:Footer ID="BottomMenu" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
