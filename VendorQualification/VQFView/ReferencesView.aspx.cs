﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;
using System.Diagnostics;
using System.Web;
using VMSDAL;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 Sooraj Sudhakaran.T 09/24/2013: VMS Modification - Added code to handle project reference type for design consultant
 * 003 Sooraj Sudhakaran.T 09/24/2013: VMS Modification - Hide section that do not apply to design consultant
 * 004 Sooraj Sudhakaran.T 10/31/2013: VMS Modification - Added code to handle International Language Question
 ****************************************************************************************************************/
#endregion

public partial class VQF_View_ReferencesView : CommonPage
{
    #region Declaration

    Common objCommon = new Common();
    clsReferences objReferences = new clsReferences();
    public DataSet dsstate;
    bool IsBasedInUS;
    long prjrefid = 0;

    bool Result;
    long VendorId;

    #endregion

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }

        //Show or hide view based on Type of company - //002 sooraj
        SwitchDesignConsultantView();

        if (!Page.IsPostBack)
        {

            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            if ((string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])) || Convert.ToString(Session["VendorAdminId"]) == "0"))
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");      //001
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["match"])))
            {
                imgprint.Attributes.Add("onClick", "printPartOfPageFromMatch()");
            }
            else
            {
                imgprint.Attributes.Add("onClick", "printPartOfPage()");
            }
            if (Convert.ToInt64(Session["VendorAdminId"]) > 0)
            {
                VendorId = Convert.ToInt64(Session["VendorAdminId"]);
                hdVendorId.Value = Convert.ToInt64(VendorId).ToString();
            }
            // get company details
            if (!string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])))
            {
                clsRegistration objregistration = new clsRegistration();
                DAL.UspGetVendorDetailsByIdResult[] objResult = objregistration.GetVendor(Convert.ToInt64((Session["VendorAdminId"])));
                if (objResult.Count() > 0)
                {
                    IsBasedInUS = objResult[0].IsBasedInUS;
                    ShowHideReferenceSupplierQuestion(IsBasedInUS); //004
                    lblVendorName.Text = objResult[0].Company;
                }
            }
            int Refcount = objReferences.GetRefCount(Convert.ToInt32(VendorId));
            if (Refcount > 0)
            {
                DAL.VQFReference Viewdata = objReferences.GetSingleVQFReferenceData(Convert.ToInt32(VendorId));
               
                if (Convert.ToString(Viewdata.CompletedProjects) == "True")
                {
                    LblcompletePrjstatus.Text = "Yes";
                }
                else if (Convert.ToString(Viewdata.CompletedProjects) == "False")
                {
                    LblcompletePrjstatus.Text = "No";
                }
                else
                {
                    LblcompletePrjstatus.Text = "";
                }

                #region LoadNotes
                //imgbtnupdate.Visible = false;
                if (Request.QueryString.Count > 0)
                {
                    if (Request.QueryString["user"] == "admin")
                    {
                        imgbtnupdate.Visible = true;
                        imgExportexcelproject.Visible = true;
                        imgbtnupdate.Visible = true;
                        //if (Viewdata.AddNote1 != "" || Viewdata.AddNote1 != null)
                        //if(!string.IsNullOrEmpty(Viewdata.AddNote1))
                        //{
                        //    spannworkcompletedone.Style["Display"] = "";                                
                        //    //tdadmintxtProject1.Style["Display"] = "";
                        //    //tdadminlnkProject1.Style["display"] = "none";                                                               
                        //    imgExportexcelsupplier.Visible = true;
                        //    imgExportexcelproject.Visible = true;
                        //    txtworkcompletedOne.Text = Viewdata.AddNote1;
                        //}
                        //else
                        //{
                        //    spannworkcompletedone.Style["Display"] = "";
                        //    //tdadminlnkProject1.Style["Display"] = "";                                
                        //    txtworkcompletedOne.Text = "";
                        //}
                        //if(!string.IsNullOrEmpty(Viewdata.AddNote2))
                        //{
                        //    spannworkcompletedTwo.Style["Display"] = "";                                
                        //    //tdadmintxtProject2.Style["Display"] = "";
                        //    //imgbtnupdate.Visible = true;
                        //    imgExportexcelsupplier.Visible = true;
                        //    imgExportexcelproject.Visible = true;
                        //    txtworkcompletedTwo.Text = Viewdata.AddNote2;
                        //}
                        //else
                        //{
                        //    spannworkcompletedTwo.Style["Display"] = "";
                        //    //tdadminlnkProject2.Style["Display"] = "";
                        //    txtworkcompletedTwo.Text = "";
                        //}
                        //if (!string.IsNullOrEmpty(Viewdata.AddNote3))
                        //{
                        //    spannworkcompletedThree.Style["Display"] = "";                                
                        //    //tdadmintxtProject3.Style["Display"] = "";
                        //    //imgbtnupdate.Visible = true;
                        //    imgExportexcelsupplier.Visible = true;
                        //    imgExportexcelproject.Visible = true;
                        //    txtworkcompletedThree.Text = Viewdata.AddNote3;
                        //}
                        //else
                        //{
                        //    spannworkcompletedThree.Style["Display"] = "";
                        //    //tdadminlnkProject3.Style["Display"] = "";
                        //    txtworkcompletedThree.Text = "";
                        //}
                    }
                    else if (Request.QueryString["user"] == "employee")
                    {
                        imgbtnupdate.Visible = false;
                        imgExportexcelproject.Visible = true;
                        //if (!string.IsNullOrEmpty(Viewdata.AddNote1))
                        //{
                        //    spannworkcompletedone.Style["Display"] = "";
                        //    tduserprjnotetitle1.Style["Display"] = "";
                        //    tduserprjnotedesc1.Style["Display"] = "";
                        //    lblusernotedesc1.Text = Viewdata.AddNote1;
                        //}
                        //else
                        //{
                        //    lblusernotedesc1.Text = "";
                        //}
                        //if (!string.IsNullOrEmpty(Viewdata.AddNote2))
                        //{
                        //    spannworkcompletedTwo.Style["Display"] = "";
                        //    tduserprjnotetitle2.Style["Display"] = "";
                        //    tduserprjnotedesc2.Style["Display"] = "";
                        //    lblusernotedesc2.Text = Viewdata.AddNote2;
                        //}
                        //else
                        //{
                        //    lblusernotedesc2.Text = "";
                        //}
                        //if (!string.IsNullOrEmpty(Viewdata.AddNote3))
                        //{
                        //    spannworkcompletedThree.Style["Display"] = "";
                        //    tduserprjnotetitle3.Style["Display"] = "";
                        //    tduserprjnotedesc3.Style["Display"] = "";
                        //    lblusernotedesc3.Text = Viewdata.AddNote3;
                        //}
                        //else
                        //{
                        //    lblusernotedesc3.Text = "";
                        //}
                    }
                    else
                    {
                        imgbtnupdate.Visible = false;
                    }
                }
                #endregion

                if (objReferences.GetProjectReferences(Convert.ToInt64(Viewdata.PK_ReferencesID)).Count() > 0)
                {
                    rptProjectReferences.DataSource = objReferences.GetProjectReferences(Convert.ToInt64(Viewdata.PK_ReferencesID));
                    rptProjectReferences.DataBind();
                }
                else
                {
                    rptProjectReferences.DataSource = null;
                    rptProjectReferences.DataBind();
                }
                tblProjectReference.Style["display"] = rptProjectReferences.Items.Count > 0 ? "none" : "";
                rptAddProjectref.DataSource = objReferences.GetReferencesProject(Convert.ToInt64(Viewdata.PK_ReferencesID));
                rptAddProjectref.DataBind();

                if (rptAddProjectref.Items.Count > 0)
                {
                    tblProjectName.Style["display"] = "none";
                }
                else
                {
                    tblProjectName.Style["display"] = "";
                    rptAddProjectref.DataSource = null;
                    rptAddProjectref.DataBind();
                }
                rptsupl.DataSource = objReferences.GetReferencesSupplier(Convert.ToInt64(Viewdata.PK_ReferencesID));
                rptsupl.DataBind();
                if (rptsupl.Items.Count != 0)
                {
                    imgExportexcelsupplier.Visible = true;
                }
            }
            if (Request.QueryString.Count > 0)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Print"]))
                {
                    VQFViewMenu.Visible = false;
                    imgExportexcelproject.Visible = false;
                    imgExportexcelsupplier.Visible = false;
                }
            }
        }

        if (rptsupl.Items.Count > 0)
        {
            SuplDetails.Style["Display"] = "";
            tdSupplierData.Style["display"] = "none";
        }
        else
        {
            SuplDetails.Style["display"] = "none";
            tdSupplierData.Style["Display"] = "";
        }
        foreach (RepeaterItem rit in rptProjectReferences.Items)
        {
            HiddenField hid = new HiddenField();
            hid = (HiddenField)rit.FindControl("hdnStatus");
            if (hid.Value.Equals("display"))
            {
                TextBox txt = new TextBox();
                txt = (TextBox)rit.FindControl("txtNotes");
                txt.Style["Display"] = "";

                LinkButton lnk = new LinkButton();
                lnk = (LinkButton)rit.FindControl("lnkAddNotes");
                lnk.Style["display"] = "none";
            }
        }
    }

    #endregion

    /// <summary>
    /// 004- Show hide question based on Vendor
    /// </summary>
    /// <param name="IsBasedInUS"></param>
    private void ShowHideReferenceSupplierQuestion(bool IsBasedInUS)
    {
        if (IsBasedInUS)
        {
            ShowHideView("none");
        }
        else
        {
            ShowHideView("");
        }
    }


    /// <summary>
    /// 004- Show hide question based on Vendor
    /// </summary>
    /// <param name="IsBasedInUS"></param>
    private void ShowHideQuestionInRepeater(HtmlControl tableRow)
    {
        if (IsBasedInUS)
            tableRow.Style["display"] = "none";
        else
            tableRow.Style["display"] = "";
    }
    
    /// <summary>
    /// 004
    /// </summary>
    /// <param name="visibleProperty"></param>
    private void ShowHideView(string visibleProperty)
    {
        trowSpeaksEnglishOne.Style["display"] = visibleProperty;
        trowSpeaksEnglishTwo.Style["display"] = visibleProperty;
        trowSpeaksEnglishThree.Style["display"] = visibleProperty;

        trowSupplierSpeaksEnglishOne.Style["display"] = visibleProperty;
        trowSupplierSpeaksEnglishTwo.Style["display"] = visibleProperty;
        trowSupplierSpeaksEnglishThree.Style["display"] = visibleProperty;
      
    }
    //003
    /// <summary>
    /// Switch view based on logged in user
    /// </summary>
    /// <param name="IsShow"></param>
    /// <Author>Sooraj Sudhakaran.T</Author>
    ///  <Date>20-SEP-2013</Date>
    private void SwitchDesignConsultantView()
    {
        //Design Consultant 
        bool IsShow = (Session["VendorCompanyType"].ConvertToString() == TypeOfCompany.DesignConsultant) ? true : false;
        tblSupplierCredit.Style["display"] = IsShow ? "none" : "";

       
    }


    public string showstate(Byte stateid)
    {
        string state = "";
        dsstate = objCommon.GetStateCode(stateid);
        if (dsstate.Tables[0].Rows.Count > 0)
        {
            state = dsstate.Tables[0].Rows[0]["StateCode"].ToString();
        }
        return state;
    }

    protected void rptSupl_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label LblState = new Label();

            LblState = (Label)e.Item.FindControl("LblState");

            ///004
            HtmlControl trowSupplierSpeaksEnglish = (HtmlControl)e.Item.FindControl("trSpeakEnglishSupplier");
            ShowHideQuestionInRepeater(trowSupplierSpeaksEnglish);


            Label lblCount = new Label();
            lblCount = (Label)e.Item.FindControl("lblCount1");
            lblCount.Text = Convert.ToString(e.Item.ItemIndex + 1);

            if (!LblState.Text.Equals("0"))
            {
                LblState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(LblState.Text)).Tables[0].Rows[0].ItemArray[1]);
            }
            else
            {
                LblState.Text = "";
            }
            if (LblState.Text.Equals("Other"))
            {
                Label LblOtherState = new Label();
                LblOtherState = (Label)e.Item.FindControl("LblOtherState");
                LblState.Text = LblOtherState.Text;
                LblOtherState.Text = "";
                HtmlTableRow trOther = new HtmlTableRow();
                trOther = (HtmlTableRow)e.Item.FindControl("trOther");
                //LblState.Text =Convert.ToString((HtmlTableRow)e.Item.FindControl("trOther"));
                trOther.Style["display"] = "none";
            }
        }

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label LblNotes = new Label();
            LblNotes = (Label)e.Item.FindControl("lblsuplnotedesc");
            if (Request.QueryString.Count > 0)
            {
                if (Request.QueryString["user"] == "admin")
                {
                    LinkButton tdlblsupl = new LinkButton();
                    tdlblsupl = (LinkButton)e.Item.FindControl("tdlnksupl");
                    HtmlTableCell tdtxtsupl = new HtmlTableCell();
                    tdtxtsupl = (HtmlTableCell)e.Item.FindControl("tdtxtsupl");
                    tdlblsupl.Style["display"] = "none";
                    tdtxtsupl.Style["Display"] = "";
                    if (LblNotes.Text == "")
                    {
                        tdtxtsupl.Style["display"] = "none";
                        tdlblsupl.Style["Display"] = "";
                    }
                }
                else if (Request.QueryString["user"] == "employee")
                {
                    HtmlTableRow tdsuplnotetitle = new HtmlTableRow();
                    tdsuplnotetitle = (HtmlTableRow)e.Item.FindControl("tdsuplnotetitle");
                    tdsuplnotetitle.Style["Display"] = "";
                }
            }
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Added by N Schoenberger 1/26/2012 for "Terms & Conditions" reference requests
            if (Request.QueryString["user"] == "admin")
            {
                clsRegistration oRegistration = new clsRegistration();
                Int64 vendorId = Convert.ToInt64(Session["VendorAdminId"]);

                //HtmlTableCell tdhlreqsupl = new HtmlTableCell();
                //tdhlreqsupl = (HtmlTableCell)e.Item.FindControl("tdhlreqsupl");

                //tdhlreqsupl.Style["display"] = "";

                HiddenField hdn = new HiddenField();
                hdn = (HiddenField)e.Item.FindControl("hdnSupplierId");
                string supplierId = hdn.Value;

                HyperLink hyp = new HyperLink();
                hyp = (HyperLink)e.Item.FindControl("hlReqSupReq");

                if (oRegistration.HasAcceptedTermsAgreement(vendorId))
                {
                    hyp.NavigateUrl = "~/VQFView/TermsConditionsPrint.aspx?vendorId=" + Convert.ToString(vendorId) + "&&supplierId=" + supplierId;
                }
                else
                {
                    hyp.Visible = false;
                }
            }
            //End added by N Schoenberger 1/26/2012 for "Terms & Conditions" reference requests
        }
    }

    protected void rptSupl_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        LinkButton lnkbtn = new LinkButton();
        lnkbtn = (LinkButton)e.CommandSource;
        if (lnkbtn.Text.Trim() == "Click to add notes")
        {
            LinkButton tdlblsupl = new LinkButton();
            tdlblsupl = (LinkButton)e.Item.FindControl("tdlnksupl");
            HtmlTableCell tdtxtsupl = new HtmlTableCell();
            tdtxtsupl = (HtmlTableCell)e.Item.FindControl("tdtxtsupl");
            tdlblsupl.Style["display"] = "none";
            tdtxtsupl.Style["Display"] = "";
        }

        //Added by N Schoenberger 1/26/2012 for "Terms & Conditions" reference requests
        if (Request.QueryString["user"] == "admin")
        {
            clsRegistration oRegistration = new clsRegistration();
            Int64 vendorId = Convert.ToInt64(Session["VendorAdminId"]);

            //HtmlTableCell tdhlreqsupl = new HtmlTableCell();
            //tdhlreqsupl = (HtmlTableCell)e.Item.FindControl("tdhlreqsupl");

            //tdhlreqsupl.Style["display"] = "";

            HiddenField hdn = new HiddenField();
            hdn = (HiddenField)e.Item.FindControl("hdnSupplierId");
            string supplierId = hdn.Value;

            HyperLink hyp = new HyperLink();
            hyp = (HyperLink)e.Item.FindControl("hlReqSupReq");

            if (oRegistration.HasAcceptedTermsAgreement(vendorId))
            {
                hyp.NavigateUrl = "~/VQFView/TermsConditionsPrint.aspx?vendorId=" + Convert.ToString(vendorId) + "&&supplierId=" + supplierId;
            }
            else
            {
                hyp.Visible = false;
            }
        }
        //End added by N Schoenberger 1/26/2012 for "Terms & Conditions" reference requests

    }
    protected void lnkworkcompletedOne_Click(object sender, EventArgs e)
    {
        var Txt = ((LinkButton)sender).ID;
        if (Txt == "lnkworkcompletedOne")
        {
            ////tdadmintxtProject1.Style["Display"] = "";
            ////tdadminlnkProject1.Style["display"] = "none";
        }
        else if (Txt == "lnkworkcompletedTwo")
        {
            ////tdadmintxtProject2.Style["Display"] = "";
            ////tdadminlnkProject2.Style["display"] = "none";
        }
        else if (Txt == "lnkworkcompletedThree")
        {
            ////tdadmintxtProject3.Style["Display"] = "";
            ////tdadminlnkProject3.Style["display"] = "none";
        }

    }
    protected void lnkworkcompletedTwo_Click(object sender, EventArgs e)
    {
        var Txt = ((LinkButton)sender).ID;

        if (Txt == "lnkworkcompletedTwo")
        {
            ////tdadmintxtProject2.Style["Display"] = "";
            ////tdadminlnkProject2.Style["display"] = "none";
        }
        else if (Txt == "lnkworkcompletedThree")
        {
            //tdadmintxtProject3.Style["Display"] = "";
            //tdadminlnkProject3.Style["display"] = "none";            
        }
        else if (Txt == "lnkworkcompletedOne")
        {
            //tdadmintxtProject1.Style["Display"] = "";
            //tdadminlnkProject1.Style["display"] = "none";
        }
    }
    protected void lnkworkcompletedThree_Click(object sender, EventArgs e)
    {
        var Txt = ((LinkButton)sender).ID;
        if (Txt == "lnkworkcompletedThree")
        {
            //tdadmintxtProject3.Style["Display"] = "";
            //tdadminlnkProject3.Style["display"] = "none";
        }
        else if (Txt == "lnkworkcompletedOne")
        {
            //tdadmintxtProject1.Style["Display"] = "";
            //tdadminlnkProject1.Style["display"] = "none";
        }
        else if (Txt == "lnkworkcompletedTwo")
        {
            //tdadmintxtProject2.Style["Display"] = "";
            //tdadminlnkProject2.Style["display"] = "none";
        }
    }

    #region ExportExcel

    #region "NAR"
    private void NAR(object o)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(o);
        }
        catch
        { }
        finally
        {
            o = null;
        }
    }
    #endregion

    protected void imgExportexcelproject_Click(object sender, ImageClickEventArgs e)
    {
        int Vendor;

        if (Convert.ToInt32(Session["VendorAdminId"]) > 0)
        {
            Vendor = Convert.ToInt32(Session["VendorAdminId"]);
        }
        else
        {
            Vendor = 0;
        }

        DataTable objDt = objReferences.GetRefPrjdetails(Convert.ToInt32(Vendor));

        if (objDt != null)
        {
            string d_FilePath = Server.MapPath("ReferenceExport") + "\\prj.xls";
            if (File.Exists(d_FilePath))
                File.Delete(d_FilePath);
            Microsoft.Office.Interop.Excel.Application xlsapp = null;
            Microsoft.Office.Interop.Excel._Workbook xlsbook = null;
            Microsoft.Office.Interop.Excel._Worksheet xlssheet = null;

            xlsapp = new Microsoft.Office.Interop.Excel.Application();
            xlsapp.Visible = false;

            xlsbook = (Microsoft.Office.Interop.Excel._Workbook)(xlsapp.Workbooks.Add(System.Reflection.Missing.Value));
            xlssheet = (Microsoft.Office.Interop.Excel._Worksheet)xlsbook.ActiveSheet;
            if (Request.QueryString.Count > 0 && Request.QueryString["user"] == "admin")
            {
                for (int j = 0; j < objDt.Columns.Count; j++)
                {
                    xlssheet.Cells[1, j + 1] = objDt.Columns[j].ColumnName;
                }
            }
            else if (Request.QueryString.Count > 0 && Request.QueryString["user"] == "employee")
            {
                for (int j = 0; j < objDt.Columns.Count - 1; j++)
                {
                    xlssheet.Cells[1, j + 1] = objDt.Columns[j].ColumnName;
                }
            }

            for (int i = 0; i < objDt.Rows.Count; i++)
            {
                xlssheet.Cells[i + 2, 1] = objDt.Rows[i][0].ToString().Trim();
                xlssheet.Cells[i + 2, 2] = objDt.Rows[i][1].ToString().Trim();
                xlssheet.Cells[i + 2, 3] = objDt.Rows[i][2].ToString().Trim();
                xlssheet.Cells[i + 2, 4] = objDt.Rows[i][3].ToString().Trim();
                xlssheet.Cells[i + 2, 5] = objDt.Rows[i][4].ToString().Trim();
                xlssheet.Cells[i + 2, 6] = objDt.Rows[i][5].ToString().Trim();
                xlssheet.Cells[i + 2, 7] = objDt.Rows[i][6].ToString().Trim();
                xlssheet.Cells[i + 2, 8] = objDt.Rows[i][7].ToString().Trim();
                xlssheet.Cells[i + 2, 9] = objDt.Rows[i][8].ToString().Trim();
                xlssheet.Cells[i + 2, 10] = objDt.Rows[i][9].ToString().Trim();
                xlssheet.Cells[i + 2, 11] = objDt.Rows[i][10].ToString().Trim();
                xlssheet.Cells[i + 2, 12] = objDt.Rows[i][11].ToString().Trim();
                xlssheet.Cells[i + 2, 13] = objDt.Rows[i][12].ToString().Trim();
                if (Request.QueryString.Count > 0 && Request.QueryString["user"] == "admin")
                {
                    xlssheet.Cells[i + 2, 14] = objDt.Rows[i][13].ToString().Trim();
                }
            }
            xlsbook.SaveAs(d_FilePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal,
                            null, null, null, null, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlShared,
                            null, null, null, null, null);

            xlssheet = null;
            NAR(xlssheet);
            xlsbook.Close(true, d_FilePath, null);
            NAR(xlsbook);
            xlsbook = null;
            xlsapp.Quit();
            NAR(xlsapp);
            GC.Collect();
            foreach (Process clsprocess in Process.GetProcesses())
            {
                string str = clsprocess.ProcessName;
                if (clsprocess.ProcessName.StartsWith("EXCEL"))
                {
                    clsprocess.Kill();
                }
            }
            string PDFPath = Convert.ToString(d_FilePath);
            FileStream liveStream = new FileStream(PDFPath, FileMode.Open, FileAccess.Read);

            byte[] buffer = new byte[(int)liveStream.Length];
            liveStream.Read(buffer, 0, (int)liveStream.Length);
            liveStream.Close();

            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", buffer.Length.ToString());
            Response.AddHeader("Content-Disposition", "attachment; filename=References%20Project%20Report%20" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            Response.BinaryWrite(buffer);
            Response.End();
        }
    }

    protected void imgExportexcelsupplier_Click(object sender, ImageClickEventArgs e)
    {
        int Vendor;

        if (Convert.ToInt32(Session["VendorAdminId"]) > 0)
        {
            Vendor = Convert.ToInt32(Session["VendorAdminId"]);
        }
        else
        {
            Vendor = 0;
        }

        int Refcount = objReferences.GetRefCount(Convert.ToInt32(Vendor));
        if (Refcount > 0)
        {
            DAL.VQFReference xlsreferenceid = objReferences.GetSingleVQFReferenceData(Convert.ToInt32(Vendor));

            DataTable objDt = objReferences.GetRefSupldetails(xlsreferenceid.PK_ReferencesID);

            if (objDt != null)
            {
                string d_FilePath = Server.MapPath("ReferenceExport") + "\\Supl.xls";
                if (File.Exists(d_FilePath))
                    File.Delete(d_FilePath);
                Microsoft.Office.Interop.Excel.Application xlsapp = null;
                Microsoft.Office.Interop.Excel._Workbook xlsbook = null;
                Microsoft.Office.Interop.Excel._Worksheet xlssheet = null;

                xlsapp = new Microsoft.Office.Interop.Excel.Application();
                xlsapp.Visible = false;

                xlsbook = (Microsoft.Office.Interop.Excel._Workbook)(xlsapp.Workbooks.Add(System.Reflection.Missing.Value));
                xlssheet = (Microsoft.Office.Interop.Excel._Worksheet)xlsbook.ActiveSheet;
                if (Request.QueryString.Count > 0 && Request.QueryString["user"] == "admin")
                {
                    for (int j = 0; j < objDt.Columns.Count; j++)
                    {
                        xlssheet.Cells[1, j + 1] = objDt.Columns[j].ColumnName;
                    }
                }
                else if (Request.QueryString.Count > 0 && Request.QueryString["user"] == "employee")
                {
                    for (int j = 0; j < objDt.Columns.Count - 1; j++)
                    {
                        xlssheet.Cells[1, j + 1] = objDt.Columns[j].ColumnName;
                    }
                }
                for (int i = 0; i < objDt.Rows.Count; i++)
                {
                    xlssheet.Cells[i + 2, 1] = objDt.Rows[i][0].ToString().Trim();
                    xlssheet.Cells[i + 2, 2] = objDt.Rows[i][1].ToString().Trim();
                    xlssheet.Cells[i + 2, 3] = objDt.Rows[i][2].ToString().Trim();
                    xlssheet.Cells[i + 2, 4] = objDt.Rows[i][3].ToString().Trim();
                    xlssheet.Cells[i + 2, 5] = objDt.Rows[i][4].ToString().Trim();
                    xlssheet.Cells[i + 2, 6] = objDt.Rows[i][5].ToString().Trim();
                    xlssheet.Cells[i + 2, 7] = objDt.Rows[i][6].ToString().Trim();
                    xlssheet.Cells[i + 2, 8] = objDt.Rows[i][7].ToString().Trim();
                    xlssheet.Cells[i + 2, 9] = objDt.Rows[i][8].ToString().Trim();
                    xlssheet.Cells[i + 2, 10] = objDt.Rows[i][9].ToString().Trim();
                    xlssheet.Cells[i + 2, 11] = objDt.Rows[i][10].ToString().Trim();
                    xlssheet.Cells[i + 2, 12] = objDt.Rows[i][11].ToString().Trim();
                    if (Request.QueryString.Count > 0 && Request.QueryString["user"] == "admin")
                    {
                        xlssheet.Cells[i + 2, 13] = objDt.Rows[i][12].ToString().Trim();
                    }

                }
                xlsbook.SaveAs(d_FilePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal,
                                null, null, null, null, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlShared,
                                null, null, null, null, null);

                xlssheet = null;
                NAR(xlssheet);
                xlsbook.Close(true, d_FilePath, null);
                NAR(xlsbook);
                xlsbook = null;
                xlsapp.Quit();
                NAR(xlsapp);
                GC.Collect();
                foreach (Process clsprocess in Process.GetProcesses())
                {
                    string str = clsprocess.ProcessName;
                    if (clsprocess.ProcessName.StartsWith("EXCEL"))
                    {
                        clsprocess.Kill();
                    }
                }
                string PDFPath = Convert.ToString(d_FilePath);
                FileStream liveStream = new FileStream(PDFPath, FileMode.Open, FileAccess.Read);

                byte[] buffer = new byte[(int)liveStream.Length];
                liveStream.Read(buffer, 0, (int)liveStream.Length);
                liveStream.Close();

                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Length", buffer.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=References%20Suplier%20Report%20" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                Response.BinaryWrite(buffer);
                Response.End();
            }
        }
    }

    #endregion

    #region UpdateNotes

    protected void imgbtnupdate_Click(object sender, ImageClickEventArgs e)
    {
        UpdNotesonVQFReferences();
        UpdNotesOnVQFSuppliers();
        if (Result == true)
        {
            lblheading.Text = "Result";
            lblMsg.Text = "&nbsp;<li>Reference notes saved successfully</li>";
            modalExtnd.Show();
        }
    }
    public void UpdNotesonVQFReferences()
    {
        int Vendor;

        if (Convert.ToInt32(Session["VendorAdminId"]) > 0)
        {
            Vendor = Convert.ToInt32(Session["VendorAdminId"]);
        }
        else
        {
            Vendor = 0;
        }

        int Refcount = objReferences.GetRefCount(Convert.ToInt32(Vendor));
        if (Refcount > 0)
        {
            DAL.VQFReference Viewdata = objReferences.GetSingleVQFReferenceData(Convert.ToInt32(Vendor));
            prjrefid = Viewdata.PK_ReferencesID;
        }
        foreach (RepeaterItem rit in rptProjectReferences.Items)
        {
            TextBox txt = new TextBox();
            txt = (TextBox)rit.FindControl("txtNotes");

            HiddenField hdn = new HiddenField();
            hdn = (HiddenField)rit.FindControl("hdnId");
            if ((txt.Style["display"] == "") && (!string.IsNullOrEmpty(txt.Text)))
            {
                objReferences.UpdateNotesVQFReferences(Convert.ToInt64(hdn.Value), txt.Text);
            }
        }
    }

    public void UpdNotesOnVQFSuppliers()
    {
        foreach (RepeaterItem ri in rptsupl.Items)
        {
            TextBox txt = new TextBox();
            txt = (TextBox)ri.FindControl("txtsuplnotes");
            HiddenField hdnSupplierId = new HiddenField();
            hdnSupplierId = (HiddenField)ri.FindControl("hdnSupplierId");
            Result = objReferences.UpdateNotesVQFSupplier(Convert.ToInt64(hdnSupplierId.Value), Convert.ToString(txt.Text.Trim()));
        }
    }

    #endregion


    protected void rptProjectReferences_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            HtmlControl trowSpeaksEnglish = (HtmlControl)e.Item.FindControl("trSpeakEnglish");
            ShowHideQuestionInRepeater(trowSpeaksEnglish);


            CheckBox chk = new CheckBox();
            chk = (CheckBox)e.Item.FindControl("chkReferencesNA");
            if (chk.Checked)
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trExplain");
                tr.Style["Display"] = "";
            }
            else
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trExplain");
                tr.Style["Display"] = "None";
            }

            if (Convert.ToString(Session["Access"]).Equals("1"))
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trAdminView");
                tr.Style["Display"] = "";

                LinkButton lnk = new LinkButton();
                lnk = (LinkButton)e.Item.FindControl("lnkAddNotes");

                TextBox txt = new TextBox();
                txt = (TextBox)e.Item.FindControl("txtNotes");
                HiddenField hdn = new HiddenField();
                hdn = (HiddenField)e.Item.FindControl("hdnStatus");
                if (string.IsNullOrEmpty(txt.Text))
                {
                    txt.Style["display"] = "none";
                }
                else
                {
                    hdn.Value = "display";
                    lnk.Style["display"] = "none";
                    txt.Style["Display"] = "";
                }
                lnk.Attributes.Add("onClick", "return DisplayNotes('" + lnk.ClientID + "','" + txt.ClientID + "','" + hdn.ClientID + "');");
            }
            else if (Convert.ToString(Session["Access"]).Equals("2"))
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trEmployeeView");
                tr.Style["Display"] = "";
            }
            else
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trAdminView");
                tr.Style["Display"] = "none";
                HtmlTableRow tr1 = new HtmlTableRow();
                tr1 = (HtmlTableRow)e.Item.FindControl("trEmployeeView");
                tr1.Style["Display"] = "none";
            }
            Label lblCount = new Label();
            lblCount = (Label)e.Item.FindControl("lblCount");
            lblCount.Text = Convert.ToString(e.Item.ItemIndex + 1);
            if (Convert.ToInt32(lblCount.Text) <= 3)
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trExplainHead");
                tr.Style["Display"] = "";
            }
            else
            {
                HtmlTableRow tr = new HtmlTableRow();
                tr = (HtmlTableRow)e.Item.FindControl("trExplainHead");
                tr.Style["Display"] = "None";
            }
            Label lbl = new Label();
            lbl = (Label)e.Item.FindControl("lblState");
            Label lbl1 = new Label();
            lbl1 = (Label)e.Item.FindControl("lblOtherState");
            Common objCommon = new Common();
            if (lbl.Text.Equals("0"))
            {
                lbl.Text = string.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(lbl.Text))
                {
                    if (objCommon.GetStateCode(Convert.ToInt16(lbl.Text.Trim())).Tables[0].Rows.Count > 0)
                    {
                        lbl.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(lbl.Text.Trim())).Tables[0].Rows[0][1]);
                    }
                }
            }
            if (lbl.Text.Equals("Other"))
            {
                lbl.Visible = false;
                lbl1.Visible = true;
            }
            else
            {
                lbl.Visible = true;
                lbl1.Visible = false;
            }
        }
    }

    #region Button Events

    protected void imgPDF_Click(object sender, ImageClickEventArgs e)
    {
        //002
        Common objCommon = new Common();
        string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/VQF/PrintVQFForm.aspx?vendorid=" + hdVendorId.Value + "&print=1";
        string baseURI = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;

        objCommon.Print2PDF(url, baseURI);
        //objCommon.GeneratePDF(Convert.ToInt64(hdVendorId.Value), Convert.ToString(Request.QueryString["user"]));
    }

    #endregion

}