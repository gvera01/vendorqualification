﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 *
 ****************************************************************************************************************/
#endregion

public partial class Common_TermsConditions : System.Web.UI.Page
{
    clsRegistration oRegistration = new clsRegistration();

    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
        {
            if (Convert.ToString(Session["Access"]) == "2")
            {
                Response.Redirect("CompanyView.aspx?user=employee");
            }
        }

        if (!Page.IsPostBack)
        {
            //if (string.IsNullOrEmpty(Convert.ToString(Request.QueryString("") || string.IsNullOrEmpty(Convert.ToString(Request.QueryString(""))
            
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");
            string vendorId = string.Empty;
            string supplierId = string.Empty;
            string authorizerAndCompany = string.Empty;

            if (Request.QueryString.Count > 0)
            {
                vendorId = Request.QueryString["vendorId"];
                supplierId = Request.QueryString["supplierId"];

                DataSet ds = new DataSet();

                ds = oRegistration.GetSignedTermsAndConditionsAgreement(Convert.ToInt64(vendorId), Convert.ToInt64(supplierId));

                authorizerAndCompany = ds.Tables[0].Rows[0].ItemArray[6].ToString() + ", " + ds.Tables[0].Rows[0].ItemArray[2].ToString();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblCompanyName.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString();
                    lblAuthorizedBy.Text = ds.Tables[0].Rows[0].ItemArray[6].ToString();
                    lblCompanyEmail.Text = ds.Tables[0].Rows[0].ItemArray[7].ToString();
                    lblAgreement.Text = ds.Tables[0].Rows[0].ItemArray[5].ToString();
                    //lblAuthorizedOn.Text = ds.Tables[0].Rows[0].ItemArray[8].ToString();
                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                    lblSupplierName.Text = ds.Tables[1].Rows[0].ItemArray[0].ToString();
                    lblSupplierContactName.Text = ds.Tables[1].Rows[0].ItemArray[1].ToString();
                    lblSupplierAddress.Text = ds.Tables[1].Rows[0].ItemArray[2].ToString();
                    lblSupplierCityStateZip.Text = ds.Tables[1].Rows[0].ItemArray[3].ToString();
                    lblSupplierPhone.Text = ds.Tables[1].Rows[0].ItemArray[4].ToString();
                    lblQuestionOne.Text = ds.Tables[1].Rows[0].ItemArray[5].ToString();
                }

                lblAuthorizedByWithCompanyName.Font.Italic = true;
                lblAuthorizedByWithCompanyName.Text = authorizerAndCompany;

                lblAuthorizationStatement.Text = "You are hereby authorized by <i>" + authorizerAndCompany + "</i> to provide Haskell with credit information per the terms and conditions stated below.";
                lblAuthorizationStatementGeneric.Text = authorizerAndCompany + " has listed your company as a credit reference.";
            }
        }
    }
}
