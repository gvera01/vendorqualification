﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TermsConditionsPrint.aspx.cs" Inherits="Common_TermsConditions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            margin-top: 0px;
        }
        </style>
    
    <script type="text/javascript">
        function printWindow() {
            window.print();
        }

        function closeWindow() {
            window.open('', '_self', '');
            window.close();
        }
    </script>
    
    <style type="text/css" media="print">
        .hide_print
        {
            display: none;
        }
        div.page
        {
            page-break-before: always;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="imbPrint">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <img id="Logo" alt="" src="../images/haskelllogo.jpg" /></td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                <!--
                                    <td class="leftmenutd">
                                    </td>
                                    -->
                                    <td class="contenttd">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="formhead">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <asp:Label ID="lblAuthorizationStatement" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                                <asp:Label ID="lblAgreement" runat="server" ForeColor="#003366"></asp:Label>
                                                            </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:CheckBox ID="chkTermsConditions" runat="server" 
                                                                    Text="I hereby agree to these terms and conditions." 
                                                        Checked="True" Enabled="False" />
                                                                &nbsp;-
                                                                <asp:Label ID="lblAuthorizedByWithCompanyName" runat="server"></asp:Label>
                                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                        <table align="center" border="0" cellpadding="5" cellspacing="0" width="50%" 
                                                            style="border: 1px solid #000000">
                                                        <tr>
                                                            <td class="tdheight" align="right">
                                                                <i>Company Name:</i>
                                                                </td>
                                                            <td class="tdheight" align="left">
                                                                <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight" align="right">
                                                                <i>Authorized By:</i>
                                                            </td>
                                                            <td class="tdheight"align="left">
                                                                <asp:Label ID="lblAuthorizedBy" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight" align="right">
                                                                Email Address:
                                                            </td>
                                                            <td class="tdheight"align="left">
                                                                <asp:Label ID="lblCompanyEmail" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        </table>
                                                        </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <asp:Label ID="lblAuthorizationStatementGeneric" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="50%" 
                                                        style="border: 1px solid #000000">
                                                        <tr>
                                                            <td class="tdheight" align="right">
                                                                Supplier Name:</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblSupplierName" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight" align="right">
                                                                Contact Name:</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblSupplierContactName" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight" align="right">
                                                                Supplier Address:</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight" align="right">
                                                                City / State / (ZIP/Postal Code):</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblSupplierCityStateZip" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight" align="right">
                                                                Phone Number:</td>
                                                            <td class="tdheight" align="left">
                                                                <asp:Label ID="lblSupplierPhone" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                          <tr>
                                                                                               <td class="tdheight" align="right">
                                                                                                    Speaks English?:
                                                                                                </td>
                                                                                               <td class="tdheight" align="left">
                                                                                                    <asp:Label ID="lblQuestionOne" runat="server" TabIndex="0" ></asp:Label>&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                        </table>
                                                        </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                        <table align="center" border="0" cellpadding="5" cellspacing="0" width="75%" 
                                                            style="height: 5px">
                                                        <tr>
                                                            <td class="tdheight">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="hide_print" align="right">
                                                                <asp:ImageButton ID="imbPrint" CausesValidation="false" runat="server" ImageUrl="~/Images/print.jpg"
                                                                    ToolTip="Print" onclientclick="printWindow()" />
                                                                &nbsp;<asp:ImageButton ID="imbClose" CausesValidation="false" runat="server" ImageUrl="~/Images/close.jpg"
                                                                    ToolTip="Close" onclientclick="closeWindow()" />
                                                                </td>
                                                            <%--<td class="formtdrt">
                                                                        <span class="mandatorystar">*</span>Comments
                                                                    </td>--%>
                                                        </tr>
                                                        </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
