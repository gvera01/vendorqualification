﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CompanyView.aspx.cs" Inherits="VQFView_CompanyView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <script src="../Script/jquery.js" type="text/javascript"></script>
    <script src="../Script/common.js" type="text/javascript"></script>
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
    <style media="print" type="text/css">
        .hide_print
        {
            display: none;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function printPartOfPage() {
            var printWindow = window.open('../Admin/VQFView.aspx?act=print&user=<%=Request.QueryString["user"] %>', '', 'left=50000,top=50000,width=0,height=0');
            printWindow.focus();
            printWindow.print();
        }
        function printPartOfPageFromMatch() {
            var printWindow = window.open('../Admin/VQFView.aspx?act=print&user=<%=Request.QueryString["user"] %>&Match=match', '', 'left=50000,top=50000,width=0,height=0');
            printWindow.focus();
            printWindow.print();
        }
    </script>
</head>
<body onload="ResetScrollPosition();">
    <form id="form1" runat="server" defaultbutton="imgPDF">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <div id="tableHeader">
    </div>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:VQFAdminCompanyMenu ID="CompanyTopMenu" runat="server" Visible="false">
                            </Haskell:VQFAdminCompanyMenu>
                        </td>
                    </tr>
                    <tr>
                        <td class="bodytextboldHead left_borderView">
                            <asp:Label ID="lblVendorName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="contenttdview">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <span class="hide_print">
                                                        <Haskell:VQFViewMenu ID="VQFViewMenu" runat="server" />
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="50%">
                                                                Vendor Qualification Form - Company
                                                            </td>
                                                            <td style="text-align: right" width="50%">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass ">
                                                        <tr>
                                                            <td class="sectionpad">
                                                                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="bodytextbold_right" width="25%">
                                                                            Registration Date: &nbsp;
                                                                        </td>
                                                                        <td class="bodytextleftView" width="25%">
                                                                            <asp:Label CssClass="bodytextleft" ID="lblDate" runat="server" Width="80px"></asp:Label>&nbsp;
                                                                        </td>
                                                                        <td class="bodytextbold_right" width="10%">
                                                                            Last Update: &nbsp;
                                                                        </td>
                                                                        <td class="bodytextleftView" width="40%">
                                                                            <asp:Label CssClass="bodytextleft" ID="lblLastUpdateDate" runat="server" Width="80px"></asp:Label>&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="bodytextbold_right" width="25%">
                                                                            Project / If Applicable: &nbsp;
                                                                        </td>
                                                                        <td class="bodytextleftView" width="75%" colspan="3">
                                                                            <asp:Label ID="lblProjectIfAvailable" runat="server"></asp:Label>&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                        <tr>
                                                            <td class="Header">
                                                                Business Section
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="sectionpad" width="100%">
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td width="100%">
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                                Legal Business Name:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                                <asp:Label CssClass="txtbox" ID="lblLegalBusinessName" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="25%">
                                                                                                                Other Business Name / DBA:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label CssClass="txtbox" ID="lblOtherBusinessName" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <%--G. Vera 12/12/2016: Add New Questions--%>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="25%">Is Company a Subsidiary?
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label CssClass="txtbox" ID="lblIsSubsidiary" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="25%">Parent Company:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label CssClass="txtbox" ID="lblParentCompany" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="25%">Publicly Held?
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label CssClass="txtbox" ID="lblPubliclyHeld" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <%--G. Vera 12/12/2016: Add New Questions END--%>

                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                                Registered As:
                                                                                                                <!--004-Sooraj-->
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top" width="10%">
                                                                                                                <asp:Label ID="lblRegisteredAs" runat="server"></asp:Label>
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" valign="top" width="20%">
                                                                                                                Federal Tax ID / Business ID:
                                                                                                                <!--004-Sooraj-->
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top" width="45%">
                                                                                                                <asp:Label ID="lblFederalTax1" runat="server" Width="150"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" nowrap="nowrap" width="25%">
                                                                                                                Type of Company:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                <asp:Label CssClass="txtbox" ID="lblTypeOfCompany" runat="server"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" nowrap="nowrap" width="25%">
                                                                                                                D U N S Number:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" >
                                                                                                                <asp:Label CssClass="txtboxsmall" ID="lblDunsNumber" runat="server" MaxLength="2"></asp:Label>&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                                Physical Address:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top" width="40%">
                                                                                                                <asp:Label ID="lblAddress" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" valign="top" width="5%">
                                                                                                                City:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" valign="top" width="30%">
                                                                                                                <asp:Label ID="lblCity" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                    Country:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top" width="25%">
                                                                                                    <asp:Label ID="lblCountry" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv" valign="top" width="7%">
                                                                                                    State:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top" width="20%">
                                                                                                    <asp:Label ID="lblState" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv" valign="top" width="12%">
                                                                                                    ZIP/Postal Code:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top" width="11%">
                                                                                                    <asp:Label ID="lblZip" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" valign="top">
                                                                                                    Phone:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    <asp:Label ID="lblMainPhno1" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv" valign="top">
                                                                                                    Fax:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" colspan="3" valign="top">
                                                                                                    <asp:Label ID="lblMainFaxno1" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="25%">
                                                                                                    Mailing Address:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="40%">
                                                                                                    &nbsp;<asp:Label ID="lblMAddress" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv" width="5%">
                                                                                                    City:
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="30%">
                                                                                                    <asp:Label ID="lblMCity" CssClass="txtbox" runat="server"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table align="right" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                        Country:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" valign="top" width="25%">
                                                                                        <asp:Label ID="lblMCountry" CssClass="txtbox" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv" valign="top" width="7%">
                                                                                        State:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" valign="top" width="20%">
                                                                                        <asp:Label ID="lblMState" CssClass="txtbox" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv" valign="top" width="12%">
                                                                                        ZIP/Postal Code:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" valign="top" width="11%">
                                                                                        <asp:Label ID="lblMZip" CssClass="txtbox" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv" width="25%">
                                                                                        Payment Remittance Address:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" width="40%">
                                                                                        <asp:Label ID="lblPAddress" runat="server" CssClass="txtbox"></asp:Label>
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv" width="5%">
                                                                                        City:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" width="30%">
                                                                                        <asp:Label ID="lblPCity" CssClass="txtbox" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table align="right" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                        Country:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" valign="top" width="25%">
                                                                                        <asp:Label ID="lblPCountry" CssClass="txtbox" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv" valign="top" width="7%">
                                                                                        State:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" valign="top" width="20%">
                                                                                        <asp:Label ID="lblPState" CssClass="txtbox" runat="server"></asp:Label>
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv" valign="top" width="12%">
                                                                                        ZIP/Postal Code:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" valign="top" width="11%">
                                                                                        <asp:Label ID="lblPZip" CssClass="txtbox" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td id="tdAddLocations" runat="server" class="bodytextbold_rightv1" style="display: none"
                                                                                        width="25%">
                                                                                        Additional Locations
                                                                                    </td>
                                                                                    <td width="75%">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%">
                                                                            <asp:DataList ID="dlAdditionalLocations" runat="server" OnItemDataBound="dlAdditionalLocations_ItemDataBound"
                                                                                Width="100%">
                                                                                <HeaderTemplate>
                                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                        Address:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top" width="40%">
                                                                                                        <%# Eval("Address")%>&nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextbold_rightv" valign="top" width="5%">
                                                                                                        City:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top" width="30%">
                                                                                                        <%# Eval("City")%>&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv" valign="top" width="25%">
                                                                                                        Country:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top" width="25%">
                                                                                                        <%# Eval("CountryCode")%>&nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextbold_rightv" valign="top" width="7%">
                                                                                                        State:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top" width="20%">
                                                                                                        <asp:Label ID="lblState" runat="server" Text='<%# Eval("StateCode") %>'></asp:Label>
                                                                                                        <asp:Label ID="lblOtherState" runat="server" Text='<%# Eval("OtherState") %>'></asp:Label>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextbold_rightv" valign="top" width="12%">
                                                                                                        ZIP/Postal Code:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top" width="11%">
                                                                                                        <%# Eval("ZipCode")%>&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv" valign="top">
                                                                                                        Phone:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        <%# Eval("Phone")%>&nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextbold_rightv" valign="top">
                                                                                                        Fax:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top" colspan="3">
                                                                                                        <%# Eval("fax")%>&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    
                                                                                </ItemTemplate>
                                                                                <FooterTemplate>
                                                                                    </table>
                                                                                </FooterTemplate>
                                                                            </asp:DataList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="sectionpad">
                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv" width="25%">
                                                                                        Company Website Address:
                                                                                    </td>
                                                                                    <td class="bodytextleftView">
                                                                                        <asp:Label ID="lblCWebAddress" runat="server" CssClass="txtbox"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                <%--G. Vera 06/09/2014: re-labeled--%>
                                                                                    <td class="bodytextbold_rightv">
                                                                                        <asp:Label ID="lblYearsinBusinessLabel" CssClass="txtbox" runat="server" Text="In Business Since:"></asp:Label>
                                                                                    </td>
                                                                                    <td class="bodytextleftView">
                                                                                        <asp:Label ID="lblYearinBusiness" CssClass="txtbox" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv">
                                                                                        &nbsp; No of Employees:
                                                                                    </td>
                                                                                    <td class="bodytextleftView">
                                                                                        <asp:Label ID="lblNoEmp" runat="server" CssClass="txtbox"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <%--G. Vera 05/07/2013 - display always--%>
                                                                                    <td id="trdesigncapabCtl" runat="server" class="bodytextbold_rightv" style="display: "
                                                                                        valign="top">
                                                                                        Do you have Design-Build Capabilities?
                                                                                    </td>
                                                                                    <td id="trdesigncapabCtlText" runat="server" class="bodytextleftView">
                                                                                        <asp:Label ID="lbldesignBuild" runat="server">&nbsp;&nbsp;&nbsp;</asp:Label>
                                                                                        <asp:Label ID="rdoCapabilitiesY" runat="server"></asp:Label>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="sectionpad">
                                                                            <span id="salestax" runat="server" style="display: " width="100%">
                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv1" width="25%">
                                                                                            State Sales Tax&nbsp;
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            State:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="ddlState" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            State Tax Number:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <span id="spnStateTax" runat="server">
                                                                                                <asp:Label ID="lblStateTaxNo1" runat="server" CssClass="txtboxsmall"></asp:Label>&nbsp;
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                        <tr>
                                                            <td class="Header">
                                                                Business Type
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-right: 15px">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td class="bodytextbold_rightv" width="26%">
                                                                            Select Business Type:
                                                                        </td>
                                                                        <td class="bodytextleftView" width="74%" style="padding-right: 15px">
                                                                            <asp:Label ID="rdoBusinessType" runat="server" Text="&nbsp;"> </asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="OtherBusiness" runat="server" style="display: none">
                                                                        <td class="bodytextbold_rightv">
                                                                            Please Specify:
                                                                        </td>
                                                                        <td class="bodytextleftView" style="padding-right: 15px">
                                                                            <asp:Label CssClass="txtbox" ID="lblOther1" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdheight" colspan="2">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <%--G. Vera 03/01/2017 - START--%>
                                                        <tr>
                                                            <td style="padding-right: 15px">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td class="bodytextbold_rightv" width="26%">W9 Upload Status:
                                                                        </td>
                                                                        <td class="bodytextleftView" width="74%" style="padding-right: 15px">
                                                                            <asp:Label ID="lblW9UploadStatus" runat="server" Text="&nbsp;"> </asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <%--G. Vera 03/01/2017 - END--%>
                                                    </table>
                                                    <table id="tblLabour" runat="server" style="display: " align="center" border="0"
                                                        cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                        <tr>
                                                            <td width="100%" class="Header">
                                                                Labor Affiliation
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15px">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-right: 15px">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="bodytextbold_rightv" width="26%">
                                                                            Union Shop?
                                                                        </td>
                                                                        <td class="bodytextleftView" width="74%">
                                                                            <asp:Label ID="lblUnionShop" runat="server" CssClass="txtbox">
                                                                                                      
                                                                            </asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td nowrap="nowrap" width="26%" class="bodytextbold_rightv">
                                                                            <span id="tdUnionAffiliation" runat="server" style="display: none">Union Affiliation:
                                                                            </span>
                                                                        </td>
                                                                        <td width="74%" align="left">
                                                                            <table id="tdTxtUnionAffiliation" runat="server" style="display: none" cellpadding="0"
                                                                                cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td class="bodytextleftView">
                                                                                        <asp:Label ID="lblUnionAffiliation" runat="server" CssClass="txtbox">
                                                                                                      
                                                                                        </asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="tdheight">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <%# Eval("Address")%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table id="tblCompany" runat="server" style="display: " align="center" border="0"
                                                        cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                        <tr id="CompanyCertificationHeader">
                                                            <td class="Header">
                                                                Company Certifications
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15px">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-right: 15px">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="bodytextbold_rightv" width="26%">
                                                                            Company Certifications:
                                                                        </td>
                                                                        <td class="bodytextleftView" width="74%">
                                                                            <asp:Label ID="rdoCompanyCertiY" runat="server" CssClass="txtbox">                                                                                                    
                                                                            </asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="bodytextbold_rightv">
                                                                            Federal SB:
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label ID="lblFederalSB" runat="server" CssClass="txtbox">                                                                                                    
                                                                            </asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <span id="trCertifyingAgency" runat="server" style="display: ">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv" width="26%">
                                                                                            Certifying Agency:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="rdoCertifyingAgency" runat="server">
                                                                                                                       
                                                                                            </asp:Label>&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <%--G. Vera 10/24/2012 - Added--%>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <span id="Span1" runat="server" style="display: ">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv" width="26%">
                                                                                            Industry Certifications:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblISO" runat="server">
                                                                                                                       
                                                                                            </asp:Label>&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv" width="26%">
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblQS" runat="server">
                                                                                                                       
                                                                                            </asp:Label>&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15px">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                        <tr>
                                                            <td class="Header">
                                                                Key Personnel Contacts
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15px">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-right: 15px">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td width="20%">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td class="bodytextleftView bodytextbold">
                                                                            Name
                                                                        </td>
                                                                        <td class="bodytextleftView bodytextbold">
                                                                            Title
                                                                        </td>
                                                                        <td class="bodytextleftView bodytextbold">
                                                                            E-Mail
                                                                        </td>
                                                                        <td class="bodytextleftView bodytextbold" nowrap="nowrap">
                                                                            Phone Number
                                                                        </td>
                                                                        <td class="bodytextleftView bodytextbold" nowrap="nowrap">
                                                                            Fax Number
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="bodytextbold_rightv" align="right" valign="top" width="20%" nowrap="nowrap">
                                                                            Principal Contact:
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label CssClass="" ID="lblPrincipal" class="txtbox" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label CssClass="" ID="lblPTitle" class="txtbox" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label CssClass="" ID="lblPEmail" class="txtbox" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView" nowrap="nowrap">
                                                                            <asp:Label ID="lblPPhone1" class="txtbox" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView" nowrap="nowrap">
                                                                            <asp:Label CssClass="" ID="lblPFax" class="txtbox" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="bodytextbold_rightv" align="right" valign="top" width="20%" nowrap="nowrap">
                                                                            Project Management Contact:
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label class="txtbox" ID="lblProjName" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label class="txtbox" ID="lblProjTitle" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label class="txtbox" ID="lblProjemail" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView" nowrap="nowrap">
                                                                            <asp:Label CssClass="txtboxsmall" ID="lblProjPhone1" runat="server" MaxLength="3"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView" nowrap="nowrap">
                                                                            <asp:Label CssClass="" ID="lblProjFax1" class="txtbox" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="bodytextbold_rightv" align="right" valign="top" width="20%" nowrap="nowrap">
                                                                            Account Receivable Contact:
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label ID="lblArName" class="txtbox" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label ID="lblARTitle" class="txtbox" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label ID="lblAREmail" class="txtbox" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView" nowrap="nowrap">
                                                                            <asp:Label CssClass="txtboxsmall" ID="lblARPhone1" runat="server" MaxLength="3"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView" nowrap="nowrap">
                                                                            <asp:Label CssClass="" ID="lblARFax1" class="txtbox" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="bodytextbold_rightv" align="right" valign="top" width="20%" nowrap="nowrap">
                                                                            Account Payable Contact:
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label CssClass="txtbox" ID="lblApName" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label CssClass="txtbox" ID="lblApTitle" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label CssClass="txtbox" ID="lblApEmail" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView" nowrap="nowrap">
                                                                            <asp:Label CssClass="txtboxsmall" ID="lblApPhone1" runat="server" MaxLength="3"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView" nowrap="nowrap">
                                                                            <asp:Label CssClass="" ID="lblApFax1" class="txtbox" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <%--G. Vera 11/01/2012 - added--%>
                                                                    <tr id="trOnsiteSupportContact" runat="server" style="display: none">
                                                                        <td class="bodytextbold_rightv" align="right" valign="top" width="20%">
                                                                            Equipment Supplier Onsite Support Contact:
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label CssClass="txtbox" ID="lblOnsiteSupportName" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label CssClass="txtbox" ID="lblOnsiteSupportTitle" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView">
                                                                            <asp:Label CssClass="txtbox" ID="lblOnsiteSupportEmail" runat="server" Style="word-wrap: normal;
                                                                                word-break: break-all;"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView" nowrap="nowrap">
                                                                            <asp:Label CssClass="txtboxsmall" ID="lblOnsiteSupportPhone" runat="server" MaxLength="3"></asp:Label>
                                                                        </td>
                                                                        <td class="bodytextleftView" nowrap="nowrap">
                                                                            <asp:Label CssClass="" ID="lblOnsiteSupportFax" class="txtbox" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <%--G. Vera 11/01/2012 - End--%>
                                                                    <!-- Changed by N Schoenberger 10-01-2011 - Formatting alignment problem with additional contacts -->
                                                                    <asp:Repeater ID="rptKeyContact" runat="server" OnItemDataBound="rptKeyContact_ItemDataBound">
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td class="bodytextbold_rightv" align="right" valign="top" width="20%">
                                                                                    <asp:Label ID="lblKeyContact" runat="server" Text="Additional Key Contacts:"></asp:Label>
                                                                                </td>
                                                                                <td class="bodytextleftView">
                                                                                    <asp:Label class="txtbox" ID="lblApName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                                                                </td>
                                                                                <td class="bodytextleftView">
                                                                                    <asp:Label class="txtbox" ID="lblApTitle" runat="server" Text='<%# Eval("Title")%>'></asp:Label>
                                                                                </td>
                                                                                <td class="bodytextleftView">
                                                                                    <asp:Label class="txtbox" ID="lblApEmail" runat="server" Text='<%# Eval("Email")%>'></asp:Label>
                                                                                </td>
                                                                                <td class="bodytextleftView" nowrap="nowrap">
                                                                                    <asp:Label ID="lblPhone" CssClass="txtboxsmall" runat="server" MaxLength="3" Text='<%# Eval("Phone")%>'></asp:Label>
                                                                                </td>
                                                                                <td class="bodytextleftView" nowrap="nowrap">
                                                                                    <asp:Label CssClass="" ID="lblFax" class="txtbox" runat="server" Text='<%# Eval("fax")%>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15px">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdheight">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                        <tr>
                                                            <td class="Header">
                                                                Officers
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-right: 15px">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="bodytextbold left_border">
                                                                            Corporate officers, partners or proprietors of your firm
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="tdOfficerData" runat="server" style="display: none;">
                                                                        <td align="right">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv" align="left" width="24%">
                                                                                        Name:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" width="20%">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv" align="left" width="5%">
                                                                                        Title:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" width="15%">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv" width="19%">
                                                                                        Percentage of Ownership:
                                                                                    </td>
                                                                                    <td class="bodytextleftView" width="4%">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv" align="left">
                                                                                        Name:
                                                                                    </td>
                                                                                    <td class="bodytextleftView">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv">
                                                                                        Title:
                                                                                    </td>
                                                                                    <td class="bodytextleftView">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv" align="left">
                                                                                        Percentage of Ownership:
                                                                                    </td>
                                                                                    <td class="bodytextleftView">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv" align="left">
                                                                                        Name:
                                                                                    </td>
                                                                                    <td class="bodytextleftView">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv" align="left">
                                                                                        Title:
                                                                                    </td>
                                                                                    <td class="bodytextleftView">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv" align="left">
                                                                                        Percentage of Ownership:
                                                                                    </td>
                                                                                    <td class="bodytextleftView">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Repeater ID="rptCompanyOfficers" runat="server" OnItemDataBound="rptCompanyOfficers_ItemDataBound">
                                                                                <HeaderTemplate>
                                                                                    <table width="75%" align="right">
                                                                                        <tr>
                                                                                            <th class="bodytextbold" width="15%">
                                                                                                Name
                                                                                            </th>
                                                                                            <th class="bodytextbold " width="15%">
                                                                                                Title
                                                                                            </th>
                                                                                            <th class="bodytextbold" width="13%">
                                                                                                Percentage Of Ownership
                                                                                            </th>
                                                                                        </tr>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <tr>
                                                                                        <td class="bodytextleftView" valign="top">
                                                                                            <%# Eval("Name")%>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView" valign="top">
                                                                                            <%# Eval("Title")%>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView" valign="top">
                                                                                            <asp:Label ID="lblPercentageOwnership" runat="server" Text=' <%# Eval("PercentageOfOwnerShip")%> '></asp:Label>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate>
                                                                                    </table></FooterTemplate>
                                                                            </asp:Repeater>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td class="bodytextbold_right" width="57%">
                                                                                        Have any of the officers ever done business with Haskell through another company?&nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextleftView" width="43%">
                                                                                        <asp:Label ID="lblPContact" runat="server" CssClass="txtbox">                                                                                                              
                                                                                        </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3">
                                                                                        <span style="display: none" runat="server" id="trbussiness">
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_right" valign="top" colspan="1" width="16%">
                                                                                                        Please Explain:&nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" colspan="2" valign="top">
                                                                                                        <asp:Label CssClass="txtboxlarge" ID="lblPContactY" runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15px">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <span class="hide_print">&nbsp;<img src="../Images/print.jpg" class="CursorStyle"
                                                        id="imgprint" runat="server" title="Print" visible="false" />
                                                        <asp:ImageButton ID="imbNotify" runat="server" ImageUrl="~/Images/pmfinal.jpg" OnClick="imbNotify_Click"
                                                            TabIndex="3" ToolTip="PM Mail Notification" />
                                                        &nbsp;<asp:ImageButton ID="imgPDF" runat="server" ImageUrl="~/Images/ex2pdf.jpg"
                                                            OnClick="imgPDF_Click" /></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                    <asp:HiddenField ID="hdVendorId" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Haskell:Footer ID="BottomMenu" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
