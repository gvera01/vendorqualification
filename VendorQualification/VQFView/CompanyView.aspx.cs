﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web;
using VMSDAL;
using System.Configuration;
using System.Reflection;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 10/23/2012: VMS Stabilization Release 1.3 (see JIRA issues for details)
 * 003 G. Vera 01/15/2013: VMS Modification - Added code to handle visibility on specialized contact type.
 * 004 Sooraj Sudhakaran.T 10/04/2013: VMS Modification - Added code to handle International Business Id
 * 005 Sooraj Sudhakaran.T 10/10/2013: VMS Modification - Added code to handle International Country ,State or province
 * 006 G. Vera 06/09/2014: New In Business since field
 * 007 G. Vera 09/15/2015: Accepting request from new VMS search application
 * 008 G. Vera 12/12/2016: Add new Business Section questions
 * 009 G. Vera 03/01/2017: New W9 upload requirement
 ****************************************************************************************************************/
#endregion


public partial class VQFView_CompanyView : System.Web.UI.Page
{
    #region Declaration

    clsCompany objCompany = new clsCompany();
    Common objCommon = new Common();
    long Vendor = 0;

    #endregion

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            //check for vendor id. if null redirect to home page.
            if (Request.QueryString.Count > 0)
            {
                //007
                if (!string.IsNullOrEmpty(Request.QueryString["from"]))
                {
                    // need to clear this in case they were searching before
                    Session["Vendordetails"] = null;

                    objCommon.AuthenticateUser();
                    string id = Request.QueryString["id"];
                    Session["VendorAdminId"] = id;

                    if (Session["Access"].ToString() == "1")
                    {
                        Response.Redirect("~/VQFView/CompanyView.aspx?user=admin&id=" + id);
                    }
                }

                VQFViewMenu.Visible = !string.IsNullOrEmpty(Request.QueryString["Print"]) ? false : true;
                if (!string.IsNullOrEmpty(Request.QueryString["user"]))
                {
                    CompanyTopMenu.Visible = true;
                    // TopMenu.Visible = false;
                    if (!string.IsNullOrEmpty(Request.QueryString["match"]))
                    {
                        Session["match"] = Request.QueryString["match"];
                        if (Session["match"].ToString()  == "SRE")
                        {
                            Session["VendorAdminId"] = Convert.ToInt64(objCommon.decode(Request.QueryString["vendorview"]));   
                        }
                        imgprint.Attributes.Add("onClick", "printPartOfPageFromMatch()");
                    }
                    else
                    {
                        Session["match"] = "";
                        imgprint.Attributes.Add("onClick", "printPartOfPage()");
                    }
                }
                else
                {
                    CompanyTopMenu.Visible = false;
                }

                
            }

            if ((string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])) || Convert.ToString(Session["VendorAdminId"]) == "0"))
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
            }

            if (Convert.ToInt64(Session["VendorAdminId"]) > 0)
            {
                Vendor = Convert.ToInt64(Session["VendorAdminId"]);
                hdVendorId.Value=Convert.ToInt64(Vendor).ToString() ; 
            }
            // get company details
            if (!string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])))
            {
                clsRegistration objregistration = new clsRegistration();
                DAL.UspGetVendorDetailsByIdResult[] objResult = objregistration.GetVendor(Convert.ToInt64((Session["VendorAdminId"])));
                if (objResult.Count() > 0)
                {
                    lblVendorName.Text = objResult[0].Company;
                    lblLastUpdateDate.Text =string.IsNullOrEmpty(Convert.ToString(objResult[0].LastModifiedDT))?"":Convert.ToDateTime(objResult[0].LastModifiedDT).ToShortDateString();
                }
            }


            //002
            VMSDAL.VQFCompany objGetCompany = objCompany.GetSingleVQFCompanyData(Vendor);
            if (objGetCompany != null)
            {
                //009 - START
                if (objGetCompany.IsW9Apply != null)
                {
                    if ((bool)objGetCompany.IsW9Apply)
                    {
                        var attachmnt = objCompany.GetW9Record(Vendor);
                        if (attachmnt != null)
                        {
                            lblW9UploadStatus.Text = attachmnt.FileName ?? "&nbsp;";
                        }
                    }
                    else
                    {
                        lblW9UploadStatus.Text = "Does not apply to our company";
                    }
                }
                //009 - END
                
                //SGS Changes on 08/11/2011
		//Modified by G. Vera on 02/15/2012 - Changed it from objGetCompany.CreatedDT to objGetCompany.FormDt
                lblDate.Text = Convert.ToDateTime(objGetCompany.FormDt).ToShortDateString();
                
                lblLegalBusinessName.Text = objGetCompany.LegalBusinessName;
                lblOtherBusinessName.Text = objGetCompany.OtherBusinessName_DBA;
                // 004 Sooraj
                if (objGetCompany.FederalTaxID!=null)
                    lblFederalTax1.Text = Convert.ToString(objGetCompany.FederalTaxID).ConvertToTaxId(objGetCompany.IsBasedInUS);
                lblRegisteredAs.Text = objGetCompany.IsBasedInUS ? "USA" : "Non-USA";
                //if (objGetCompany.FederalTaxID.Length >= 9)
                //{
                //    //lblFederalTax1.Text = Convert.ToString(objGetCompany.FederalTaxID).Substring(0, 2) + " - ";
                //    //lblFederalTax2.Text = Convert.ToString(objGetCompany.FederalTaxID).Substring(2, 7);

                //}

                lblDunsNumber.Text = objGetCompany.DUNSNumber;
                lblAddress.Text = objGetCompany.PhysicalAddress;
                lblCity.Text = objGetCompany.PhysicalCity;
                if (objGetCompany.FK_PhysicalState != 0)
                {
                    lblState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(objGetCompany.FK_PhysicalState)).Tables[0].Rows[0].ItemArray[1]);
                }
                if (objGetCompany.FK_PhysicalCountry.HasValue) //005-Sooraj
                {
                    lblCountry.Text = objCommon.GetCountryCode(objGetCompany.FK_PhysicalCountry.Value);
                }
                lblZip.Text = objGetCompany.PhysicalZipCode;
                if (lblState.Text.Equals("Other"))
                {
                    lblState.Text = objGetCompany.OtherPhysicalState;
                }

                lblMAddress.Text = objGetCompany.MailingAddress;
                lblMCity.Text = objGetCompany.MailingCity;
                if (objGetCompany.FK_MailingState != 0)
                {
                    lblMState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(objGetCompany.FK_MailingState)).Tables[0].Rows[0].ItemArray[1]);
                }
                if (objGetCompany.FK_MailingCountry.HasValue) //005-Sooraj
                {
                    lblMCountry.Text = objCommon.GetCountryCode(objGetCompany.FK_MailingCountry.Value);
                }
                lblMZip.Text = objGetCompany.MailingZipCode;

                if (lblMState.Text.Equals("Other"))
                {
                    lblMState.Text = objGetCompany.OtherMailingState;
                }

                lblPAddress.Text = objGetCompany.PaymentRemittanceAddress;
                lblPCity.Text = objGetCompany.PaymentCity;
                if (objGetCompany.FK_PaymentState != 0)
                {
                    lblPState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(objGetCompany.FK_PaymentState)).Tables[0].Rows[0].ItemArray[1]);
                }
                if (objGetCompany.FK_PaymentCountry.HasValue) //005-Sooraj
                {
                    lblPCountry.Text = objCommon.GetCountryCode(objGetCompany.FK_PaymentCountry.Value);
                }
                lblPZip.Text = objGetCompany.PaymentZipcode;
                lblMainPhno1.Text = objGetCompany.MainPhoneNumber;
                lblMainFaxno1.Text = objGetCompany.MainFaxNumber;
                lblCWebAddress.Text = objGetCompany.CompanyWebsiteAddress;
                lblYearinBusiness.Text = (string.IsNullOrEmpty(objGetCompany.InBusinessSince)) ? objGetCompany.YearsinBusiness : objGetCompany.InBusinessSince ;     //006
                lblYearsinBusinessLabel.Text = (!string.IsNullOrEmpty(objGetCompany.InBusinessSince)) ? "In Business Since:" : "Years in Business";     //006

                if (objGetCompany.NoOfEmployees != -1)
                {
                    lblNoEmp.Text = Convert.ToString(objGetCompany.NoOfEmployees);
                }
                if (objCommon.GetStateCode(Convert.ToInt16(objGetCompany.FK_StateSalesTax)).Tables[0].Rows.Count > 0)
                {
                    ddlState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(objGetCompany.FK_StateSalesTax)).Tables[0].Rows[0].ItemArray[1]);
                }
                if (ddlState.Text.Equals("Other"))
                {
                    ddlState.Text = objGetCompany.OtherStateSalesTax;
                }
                //The above line was commented and below line was included by SGS for Task No: 7
                    spnStateTax.Style["Display"] = "";
                    lblStateTaxNo1.Text = objGetCompany.StateTaxNumber;
               
                rdoBusinessType.Text = objGetCompany.BusinessType;
                if (rdoBusinessType.Text == null || rdoBusinessType.Text == "") rdoBusinessType.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";   //GV
                if (rdoBusinessType.Text.Equals("Other"))
                {
                    OtherBusiness.Style["Display"] = "";
                    lblOther1.Text = objGetCompany.OtherType;
                }
                bool? UnionShop = objGetCompany.UnionShop;
                if (UnionShop == true)
                {
                    lblUnionShop.Text = "Yes";
                    tdTxtUnionAffiliation.Style["display"] = "";
                    tdUnionAffiliation.Style["display"] = "";
                }
                else if (UnionShop == false)
                {
                    lblUnionShop.Text = "No";
                    tdTxtUnionAffiliation.Style["display"] = "None";
                    tdUnionAffiliation.Style["display"] = "None";
                }
                else
                {
                    lblUnionShop.Text = "";
                    tdTxtUnionAffiliation.Style["display"] = "None";
                    tdUnionAffiliation.Style["display"] = "None";
                }
                lblUnionAffiliation.Text = objGetCompany.UnionAffiliation;
                string CompanyCertification = null;
                DAL.VQFCompanyCertification[] objCertification = objCompany.GetCompanyCertification(objGetCompany.PK_CompanyID);
                for (int i = 0; i < objCertification.Length; i++)
                {
                    CompanyCertification += string.IsNullOrEmpty(CompanyCertification) ? objCertification[i].Certification : " , " + objCertification[i].Certification;
                }
                rdoCompanyCertiY.Text = CompanyCertification;
                CompanyCertification = string.Empty;
                DAL.VQFCompanyCertification[] objFederalSB = objCompany.GetCompanyFederalSB(objGetCompany.PK_CompanyID);
                for (int i = 0; i < objFederalSB.Length; i++)
                {
                    CompanyCertification += string.IsNullOrEmpty(CompanyCertification) ? objFederalSB[i].Certification : " , " + objFederalSB[i].Certification;
                }

                lblISO.Text = "ISO " + objCompany.GetIndustryCertCodes(objGetCompany.PK_CompanyID, "ISO");   //002
                if (lblISO.Text.Length == 4) lblISO.Text = "";                                    //002
                lblQS.Text = " QS " + objCompany.GetIndustryCertCodes(objGetCompany.PK_CompanyID, "QS");     //002
                if (lblQS.Text.Length == 4) lblQS.Text = "";                                      //002

                lblFederalSB.Text = CompanyCertification;
                rdoCertifyingAgency.Text = objGetCompany.CertifyingAgency;
                lblPrincipal.Text = objGetCompany.PrincipalContactName;
                lblPTitle.Text = objGetCompany.PrincipalContactTitle;
                lblPEmail.Text = objGetCompany.PrincipalContactEmail;
                lblPPhone1.Text = objGetCompany.PrincipalContactPhone;
                lblPFax.Text = objGetCompany.PrincipalContactFax;

                lblProjName.Text = objGetCompany.ProjectManagementContactName;
                lblProjTitle.Text = objGetCompany.ProjectManagementTitle;
                lblProjemail.Text = objGetCompany.ProjectManagementEmail;
                lblProjPhone1.Text = objGetCompany.ProjectManagementPhone;
                lblProjFax1.Text = objGetCompany.ProjectManagementFax;

                lblArName.Text = objGetCompany.AccountReceivableContactName;
                lblARTitle.Text = objGetCompany.AccountReceivableTitle;
                lblAREmail.Text = objGetCompany.AccountReceivableEmail;
                lblARPhone1.Text = objGetCompany.AccountReceivablePhone;
                lblARFax1.Text = objGetCompany.AccountReceivableFax;

                lblApName.Text = objGetCompany.AccountPayableContactName;
                lblApTitle.Text = objGetCompany.AccountPayableTitle;
                lblApEmail.Text = objGetCompany.AccountPayableEmail;
                lblApPhone1.Text = objGetCompany.AccountPayablePhone;
                lblApFax1.Text = objGetCompany.AccountPayableFax;

                //002 - added below
                lblOnsiteSupportName.Text = objGetCompany.OnsiteSupportContactName;
                lblOnsiteSupportTitle.Text = objGetCompany.OnsiteSupportContactTitle;
                lblOnsiteSupportEmail.Text = objGetCompany.OnsiteSupportContactEmail;
                lblOnsiteSupportPhone.Text = objGetCompany.OnsiteSupportContactPhone;
                lblOnsiteSupportFax.Text = objGetCompany.OnsiteSupportContactFax;
                 

                rptKeyContact.DataSource = objCompany.GetcompanyKeyContactsData(objGetCompany.PK_CompanyID);
                rptKeyContact.DataBind();

                if (lblPState.Text.Equals("Other"))
                {
                    lblPState.Text = objGetCompany.OtherPaymentState;
                }
                if (objGetCompany.Design_Build == true)
                {
                    lbldesignBuild.Text = "Yes";
                    if (objGetCompany.Design_BuildCapabilities != "")
                    {
                        rdoCapabilitiesY.Text = " - " + objGetCompany.Design_BuildCapabilities;
                    }
                }
                else if (objGetCompany.Design_Build == false)
                {
                    lbldesignBuild.Text = "No";
                }
                else
                {
                    lbldesignBuild.Text = "&nbsp;&nbsp;&nbsp;&nbsp;";
                }

                bool? contact = objGetCompany.AnotherCompany;
                if (contact == true)
                {
                    lblPContact.Text = "Yes";
                }
                else if (contact == false)
                {
                    lblPContact.Text = "No";
                }
                else
                {
                    lblPContact.Text = "";
                }

                if (lblPContact.Text == "Yes")
                {
                    lblPContactY.Text = objGetCompany.ExplainAnotherCompany;
                    trbussiness.Style["display"] = string.IsNullOrEmpty(lblPContactY.Text) ? "none" : "";
                }
                else
                {
                    trbussiness.Style["display"] = "none";
                }
                lblProjectIfAvailable.Text = objGetCompany.Project;
                lblTypeOfCompany.Text = objCompany.DecodeCompanytype(objGetCompany.TypeOfCompany);     //002 - modified
                //004-Sooraj - Store the Company Type in Session 
                Session["VendorCompanyType"] = objGetCompany.TypeOfCompany;
                //002 - modified
                switch (objGetCompany.TypeOfCompany)
                {
                    case "Subcontractor":
                        salestax.Style["Display"] = "";
                        trCertifyingAgency.Style["Display"] = "";
                        trdesigncapabCtl.Style["Display"] = "";
                        tblCompany.Style["Display"] = "";
                        tblLabour.Style["Display"] = "";
                        trOnsiteSupportContact.Attributes["style"] = "display: none";   //003
                        break;
                    case "Supplier":
                        salestax.Style["Display"] = "";
                        trCertifyingAgency.Style["Display"] = "";
                        trdesigncapabCtl.Style["Display"] = "none";
                        tblCompany.Style["Display"] = "";
                        lbldesignBuild.Visible = false;
                        rdoCapabilitiesY.Visible = false;
                        tblLabour.Style["Display"] = "";
                        trOnsiteSupportContact.Attributes["style"] = "display: none";   //003
                        break;
                    case "Design Consultant":
                        trCertifyingAgency.Style["Display"] = "";       //G. Vera 05/16/2014: Dianne D has requested this to be reverted
                        salestax.Style["Display"] = "none";
                        trdesigncapabCtl.Style["Display"] = "";
                        lbldesignBuild.Visible = true;
                        rdoCapabilitiesY.Visible = true;
                        tblCompany.Style["Display"] = "";
                        tblLabour.Style["Display"] = "none";
                        trOnsiteSupportContact.Attributes["style"] = "display: none";   //003

                        break;
                    //003 - added new type of company
                    case "Equip/Onsite Support":
                        salestax.Style["Display"] = "";
                        trCertifyingAgency.Style["Display"] = "";
                        trdesigncapabCtl.Style["Display"] = "";
                        tblCompany.Style["Display"] = "";
                        tblLabour.Style["Display"] = "";
                        trOnsiteSupportContact.Attributes["style"] = "display: ";   //003
                        break;
                    default:
                        //002 - changed this to be the opposite
                        trCertifyingAgency.Style["Display"] = "";
                        salestax.Style["Display"] = "";
                        trdesigncapabCtl.Style["Display"] = "";
                        lbldesignBuild.Visible = true;
                        rdoCapabilitiesY.Visible = true;
                        tblCompany.Style["Display"] = "";
                        tblLabour.Style["display"] = "";
                        trOnsiteSupportContact.Attributes["style"] = "display: none";   //003
                        break;
                }
                dlAdditionalLocations.DataSource = objCompany.GetcompanyBusinessLocation(objGetCompany.PK_CompanyID);
                dlAdditionalLocations.DataBind();
                tdAddLocations.Style["display"] = dlAdditionalLocations.Items.Count > 0 ? "" : "none";
                rptCompanyOfficers.DataSource = objCompany.GetcompanyOfficersData(objGetCompany.PK_CompanyID);
                rptCompanyOfficers.DataBind();

                //008- START
                lblIsSubsidiary.Text = (objGetCompany.IsSubsidiary == null) ? "" : (((bool)objGetCompany.IsSubsidiary) ? "Yes" : "No");
                lblPubliclyHeld.Text = (objGetCompany.IsPubliclyHeld == null) ? "" : (((bool)objGetCompany.IsPubliclyHeld) ? "Yes" : "No");
                lblParentCompany.Text = objGetCompany.ParentCompanyName;
                //008 - END
            }
            else
            {
                LinkButton lnk = (LinkButton)CompanyTopMenu.FindControl("lnkAddNotes");
                lnk.Visible = false;
            }
        }

        if (rptCompanyOfficers.Items.Count == 0)
        {
            tdOfficerData.Style["display"] = "";
            rptCompanyOfficers.Visible = false;
        }
        else
        {
            tdOfficerData.Style["display"] = "none";
            rptCompanyOfficers.Visible = true;
        }
        trdesigncapabCtlText.Style["display"] = trdesigncapabCtl.Style["display"];

        //Register client script
        //ResetScrollPosition();
    }

    #endregion

    protected void dlAdditionalLocations_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lbl = new Label();
            lbl = (Label)e.Item.FindControl("lblState");
            Label lblOtherState = new Label();
            lblOtherState = (Label)e.Item.FindControl("lblOtherState");
            if (lbl.Text.Equals("Other"))
            {
                lblOtherState.Visible = true;
                lbl.Visible = false;
            }
            else
            {
                lblOtherState.Visible = false;
                lbl.Visible = true;
            }
        }
    }

    protected void rptCompanyOfficers_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lbl = new Label();
            lbl = (Label)e.Item.FindControl("lblPercentageOwnership");
            if (!string.IsNullOrEmpty(lbl.Text.Trim()))
            {
                lbl.Text = lbl.Text + "%";
            }
            else
            {
                lbl.Text = "";
            }
        }
    }

    protected void rptKeyContact_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.Item.ItemIndex == 0)
            {
                Label lbl = new Label();
                lbl = (Label)e.Item.FindControl("lblKeyContact");
                lbl.Visible = true;
            }
            else
            {
                Label lbl = new Label();
                lbl = (Label)e.Item.FindControl("lblKeyContact");
                lbl.Visible = false;
            }
        }
    }


    #region Button Events

    protected void imbNotify_Click(object sender, ImageClickEventArgs e)
    {
        RiskEvaluation riskEvaluation = new RiskEvaluation();
        DataSet VendorDetails = new DataSet();
        String statusDesc = String.Empty;

        VendorDetails = objCommon.GetVendorDetails(Convert.ToInt64(this.hdVendorId.Value));

        int caseStatus = Convert.ToInt32(VendorDetails.Tables[0].Rows[0]["VendorStatus"].ToString());

        if (caseStatus != 2)
        {
            switch (caseStatus)
            {
                case 0:
                    statusDesc = "InProgress";
                    break;
                case 1:
                    statusDesc = "Pending";
                    break;
                case 2:
                    statusDesc = "Complete";
                    break;
                case 3:
                    statusDesc = "Revision";
                    break;
                case 4:
                    statusDesc = "Reject";
                    break;
                case 5:
                    statusDesc = "InComplete";
                    break;
                case 6:
                    statusDesc = "Expired";
                    break;
            }

            riskEvaluation.AddMailNotification(Convert.ToInt64(this.hdVendorId.Value), VendorDetails.Tables[0].Rows[0]["Company"].ToString(), VendorDetails.Tables[0].Rows[0]["Email"].ToString(), statusDesc, Session["EmployeeName"].ToString(), Session["EmailUser"].ToString(), 0);
            //lblMessage.Text = "PM Notification has been configured.";
        }
        else
        {
            //lblMessage.Text = "The VQF is already in complete status.";
        }
    }

    protected void imgPDF_Click(object sender, ImageClickEventArgs e)
    {
        //002
        Common objCommon = new Common();
        string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/VQF/PrintVQFForm.aspx?vendorid=" + hdVendorId.Value + "&print=1";
        string baseURI = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;
        //string baseURI = ConfigurationManager.AppSettings["SiteUrl"];
        //string url = baseURI + "/VQF/PrintVQFForm.aspx?vendorid=" + hdVendorId.Value + "&print=1";

        objCommon.Print2PDF(url, baseURI);
        //for testing: this.Response.Redirect(url);
        
    }

    #endregion

    #region " Methods "
    //This is now in the common
    //protected void ResetScrollPosition()
    //{
    //    if (!ClientScript.IsClientScriptBlockRegistered(this.GetType(), "ResetScrollPosition"))
    //    {
    //        //Create the ResetScrollPosition() function    
    //        ClientScript.RegisterClientScriptBlock(this.GetType(), "ResetScrollPosition", "function ResetScrollPosition() {     setTimeout('window.scrollTo(0,0)', 0); }", true);
             
    //        //Add the call to the ResetScrollPosition() function    
    //        ClientScript.RegisterStartupScript(this.GetType(), "ResetScrollPosition", "ResetScrollPosition();", true);
    //    }
    //}
    #endregion

}
