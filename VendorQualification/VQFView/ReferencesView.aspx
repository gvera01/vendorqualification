﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReferencesView.aspx.cs" Inherits="VQF_View_ReferencesView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>References View</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache">
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <script src="../Script/jquery.js" type="text/javascript"></script>
    <script src="../Script/common.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function DisplayNotes(lnk, txt, hdn) {
            var lnk1 = document.getElementById(lnk);
            lnk1.style.display = "none";
            var txt1 = document.getElementById(txt);
            txt1.style.display = "";
            var hdn1 = document.getElementById(hdn);
            hdn1.value = "display";
            return false;
        }
    </script>
    <script language="javascript" type="text/javascript">
        function printPartOfPage() {
            var printWindow = window.open('../Admin/VQFView.aspx?act=print&user=<%=Request.QueryString["user"] %>', '', 'left=50000,top=50000,width=0,height=0');
            printWindow.focus();
            printWindow.print();
        }
        function printPartOfPageFromMatch() {
            var printWindow = window.open('../Admin/VQFView.aspx?act=print&user=<%=Request.QueryString["user"] %>&Match=match', '', 'left=50000,top=50000,width=0,height=0');
            printWindow.focus();
            printWindow.print();
        }
    </script>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
        .style1
        {
        }
    </style>
    <style media="print">
        .hide_print
        {
            display: none;
        }
    </style>
</head>
<body onload="ResetScrollPosition();">
    <form id="form1" runat="server" defaultbutton="imgPDF">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="625px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bodytextboldHead left_borderView">
                            <asp:Label ID="lblVendorName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="contenttdview">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <span class="hide_print">
                                                <tr>
                                                    <td>
                                                        <Haskell:VQFViewMenu ID="VQFViewMenu" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                            </span>
                                            <tr>
                                                <td>
                                                    <div id="RefDiv" runat="server">
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="50%">
                                                                Vendor Qualification Form - References
                                                            </td>
                                                            <td width="50%">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight" align="right">
                                                    <asp:LinkButton ID="lnkGenRefRequest" runat="server" CssClass="left_border hide_print"
                                                        Visible="False">Generate Reference Request</asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="upnlReferences" runat="server">
                                                        <ContentTemplate>
                                                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                <tr>
                                                                    <td class="searchhdrbarbold" colspan="2">
                                                                        Project References
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" width="100%" style="padding: 5px;">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" id="tblProjectReference" runat="server">
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td class="keyheader" colspan="2">
                                                                                                Reference 1
                                                                                                <asp:Label ID="Label2" runat="server"></asp:Label>
                                                                                                &nbsp;&nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight" colspan="2">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                N/A:
                                                                                            </td>
                                                                                            <td class="bodytextleftViewv">
                                                                                                <asp:CheckBox ID="chkReferencesNA1" runat="server" Enabled="false" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Please Explain For N/A:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Project Name:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                City:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                Country:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                State:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                Completion Date:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Approximate Contract Value $:
                                                                                            </td>
                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                <!--002-Sooraj-->
                                                                                                <asp:Label ID="lblProjectReferenceTypeOne" runat="server">General Contractor</asp:Label>:
                                                                                            </td>
                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                Please give a brief description of work completed:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Contact Name:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Contact Phone Number:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr id="trowSpeaksEnglishOne" runat="server" style="display: none">
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Speaks English?
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="border-bottom: 1px solid #cccccc" class="style1">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td class="keyheader" colspan="2">
                                                                                                Reference 2
                                                                                                <asp:Label ID="Label1" runat="server"></asp:Label>
                                                                                                &nbsp;&nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight" colspan="2">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                N/A:
                                                                                            </td>
                                                                                            <td class="bodytextleftViewv">
                                                                                                <asp:CheckBox ID="chkReferencesNA2" runat="server" Enabled="false" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Please Explain For N/A:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Project Name:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                City:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                Country:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                State:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                Completion Date:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Approximate Contract Value $:
                                                                                            </td>
                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                <!--002-Sooraj-->
                                                                                                <asp:Label ID="lblProjectReferenceTypeTwo" runat="server">General Contractor</asp:Label>:
                                                                                            </td>
                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                Please give a brief description of work completed:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Contact Name:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Contact Phone Number:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr id="trowSpeaksEnglishTwo" runat="server" style="display: none">
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Speaks English?
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="border-bottom: 1px solid #cccccc" class="style1">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td class="keyheader" colspan="2">
                                                                                                Reference 3
                                                                                                <asp:Label ID="lblCount" runat="server"></asp:Label>
                                                                                                &nbsp;&nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="tdheight" colspan="2">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                N/A:
                                                                                            </td>
                                                                                            <td class="bodytextleftViewv">
                                                                                                <asp:CheckBox ID="chkReferencesNA3" runat="server" Enabled="false" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Please Explain For N/A:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Project Name:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                City:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                Country:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                State:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                Completion Date:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Approximate Contract Value $:
                                                                                            </td>
                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                <!--002-Sooraj-->
                                                                                                <asp:Label ID="lblProjectReferenceTypeThree" runat="server">General Contractor</asp:Label>:
                                                                                            </td>
                                                                                            <td class="bodytextleftView" valign="top">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv">
                                                                                                Please give a brief description of work completed:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Contact Name:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Contact Phone Number:
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr id="trowSpeaksEnglishThree" runat="server" style="display: none">
                                                                                            <td class="bodytextbold_rightv" width="35%">
                                                                                                Speaks English?
                                                                                            </td>
                                                                                            <td class="bodytextleftView">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" width="100%" style="padding-left: 5px">
                                                                        <asp:Repeater ID="rptProjectReferences" runat="server" OnItemDataBound="rptProjectReferences_ItemDataBound">
                                                                            <HeaderTemplate>
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td style="padding: 5px;">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td class="keyheader" colspan="2">
                                                                                                    Reference
                                                                                                    <asp:Label ID="lblCount" runat="server"></asp:Label>
                                                                                                    &nbsp;&nbsp;&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="tdheight" colspan="2">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trExplainHead" runat="server">
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    N/A: &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftViewv">
                                                                                                    <asp:CheckBox ID="chkReferencesNA" runat="server" Enabled="false" Checked='<%#Convert.ToBoolean(Eval("NotAvailable"))%>' />
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trExplain" runat="server">
                                                                                                <td class="bodytextbold_rightv" valign="top" width="35%">
                                                                                                    Please Explain For N/A: &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="lblExplanation" runat="server" TabIndex="0" Text='<%# Eval("Explanation") %>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                    Project Name: &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="txtProjectNameOne" runat="server" TabIndex="0" Text='<%# Eval("ProjectName") %>'></asp:Label>
                                                                                                    <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("PK_ProjectReferencesID") %>'>
                                                                                                    </asp:HiddenField>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    City: &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="txtCity" runat="server" CssClass="txtbox" TabIndex="0" Text='<%# Eval("City") %>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Country: &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="lblCountry" runat="server" CssClass="txtbox" TabIndex="0" Text='<%# Eval("CountryName") %>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    State: &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="lblState" CssClass="txtbox" runat="server" Text='<%# Eval("FK_State") %>'
                                                                                                        TabIndex="0">
                                                                                                    </asp:Label>
                                                                                                    <asp:Label ID="lblOtherState" CssClass="txtbox" runat="server" Text='<%# Eval("OtherState") %>'
                                                                                                        TabIndex="0" Visible="false">
                                                                                                    </asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Completion Date: &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="lblCompletionDateOne" CssClass="txtbox" runat="server" TabIndex="0"
                                                                                                        Text='<%# Eval("CompletionDT") %>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                    Approximate Contract Value $: &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    <asp:Label ID="lblApproximateContractAmtOne" runat="server" Text='<%# Eval("ApproximateContract") %>'
                                                                                                        TabIndex="0"></asp:Label>&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    <!--002-Sooraj-->
                                                                                                    <asp:Label ID="lblProjectReferenceType" runat="server" Text='<%# Eval("ProjectReferenceType") %>'
                                                                                                        TabIndex="0"></asp:Label>
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView" valign="top">
                                                                                                    <asp:Label ID="lblContractorNameOne" runat="server" Text='<%# Eval("GeneralContractor") %>'
                                                                                                        TabIndex="0"></asp:Label>
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv">
                                                                                                    Please give a brief description of work completed: &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="lblWorkCompletedOne" CssClass="txtbox" runat="server" Text='<%# Eval("WorkCompleted") %>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                    Contact Name: &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="lblCtrContactNameOne" runat="server" TabIndex="0" Text='<%# Eval("ContactName") %>'></asp:Label>&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                    Contact Phone Number: &nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="lblMainPhnoOne" runat="server" TabIndex="0" Text='<%# Eval("ContactPhoneNumber") %>'></asp:Label>&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trSpeakEnglish" runat="server" style="display: none">
                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                    Speaks English?
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                    <asp:Label ID="lblQuestionOne" runat="server" TabIndex="0" Text='<%# Eval("IsSpeaksEnglishText") %>'></asp:Label>&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trAdminView" runat="server" style="display: none">
                                                                                    <td>
                                                                                        <table cellpadding="5" cellspacing="3" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td width="35%" class="left_border hide_print">
                                                                                                    <asp:LinkButton ID="lnkAddNotes" runat="server" Text="Click to Add Notes"></asp:LinkButton>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtNotes" runat="server" CssClass="txtboxmultilarge" TextMode="MultiLine"
                                                                                                        Height="50px" Width="609px" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                        onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"
                                                                                                        Text='<%# Eval("AddNote") %>'></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnStatus" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <span class="hide_print">
                                                                                    <tr id="trEmployeeView" runat="server" style="display: none">
                                                                                        <td>
                                                                                            <table cellpadding="15" cellspacing="10" width="100%" border="0">
                                                                                                <tr>
                                                                                                    <td class="special" width="34%" valign="top">
                                                                                                        <asp:Label ID="lblHead" runat="server" Text="Haskell Admin Notes:"></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" width="65%">
                                                                                                        &nbsp;
                                                                                                        <asp:Label ID="lblAddnot" runat="server" Text='<%# Eval("AddNote") %>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </span>
                                                                                <tr>
                                                                                    <td class="tdheight">
                                                                                    </td>
                                                                                </tr>
                                                                                <%-- <tr>
                                                                                                    <td colspan="2" style="border-bottom: 1px solid #cccccc" class="style1">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>--%>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table></FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <span class="hide_print">
                                                                                        <asp:ImageButton ID="imgExportexcelproject" runat="server" Visible="false" ImageUrl="~/Images/extoexcel.jpg"
                                                                                            OnClick="imgExportexcelproject_Click" /></span>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="1%">
                                                                    </td>
                                                                    <td class="bodytextleft">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td class="bodytextbold_rightv" width="34%">
                                                                                    Have you ever completed any projects<br />
                                                                                    with Haskell?
                                                                                </td>
                                                                                <td class="bodytextleftView">
                                                                                    <asp:Label ID="LblcompletePrjstatus" runat="server"></asp:Label>&nbsp;
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td class="bodytextbold" align="right">
                                                                        <table width="66%" align="right" id="tblProjectName" runat="server">
                                                                            <tr>
                                                                                <td width="30%">
                                                                                    Project Name
                                                                                </td>
                                                                                <td width="1%">
                                                                                </td>
                                                                                <td width="28%">
                                                                                    Project Manager
                                                                                </td>
                                                                                <td width="1%">
                                                                                </td>
                                                                                <td width="7%" nowrap="nowrap">
                                                                                    Year Completed
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytextleftView" valign="top">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                                <td class="bodytextleftView" valign="top">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                                <td class="bodytextleftView" valign="top">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytextleftView" valign="top">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                                <td class="bodytextleftView" valign="top">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                                <td class="bodytextleftView" valign="top">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytextleftView" valign="top">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                                <td class="bodytextleftView" valign="top">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                                <td class="bodytextleftView" valign="top">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="1%">
                                                                    </td>
                                                                    <td class="bodytextleft" align="right">
                                                                        <asp:Repeater ID="rptAddProjectref" runat="server">
                                                                            <HeaderTemplate>
                                                                                <table width="66%" align="right">
                                                                                    <tr>
                                                                                        <td width="30%" class="bodytextbold">
                                                                                            Project Name
                                                                                        </td>
                                                                                        <td width="1%" class="bodytextbold">
                                                                                        </td>
                                                                                        <td width="30%" class="bodytextbold">
                                                                                            Project Manager
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                        <td width="4%" class="bodytextbold" nowrap="nowrap">
                                                                                            Year Completed
                                                                                        </td>
                                                                                    </tr>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td class="bodytextleftView" valign="top">
                                                                                        <%# Eval("ProjectName") %>
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td class="bodytextleftView" valign="top">
                                                                                        <%#Eval("ProjectManager") %>
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td class="bodytextleftView" valign="top">
                                                                                        <%#Eval("Year") %>
                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table></FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdheight">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass"
                                                                id="tblSupplierCredit" runat="server">
                                                                <tr>
                                                                    <td class="searchhdrbarbold">
                                                                        Supplier/Credit
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdheight">
                                                                    </td>
                                                                </tr>
                                                                <%-- <tr>
                                                                                    <td class="subHeadtext">
                                                                                        &nbsp;&nbsp;&nbsp;Please provide us at least 3  Supplier/Credit references
                                                                                    </td>
                                                                                </tr>--%>
                                                               <tr id="tdSupplierData" runat="server" style="display: none;">
                                                                                    <td class="sectionpad">
                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td class="keyheader" colspan="2" width="100%">
                                                                                                    Supplier/Credit 1
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Company Name:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Company Address:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="36%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="5%">
                                                                                                                City:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="25%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Country:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="7%">
                                                                                                                State:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="15%">
                                                                                                                ZIP/Postal Code:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="14%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Contact Name:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Phone Number:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trowSupplierSpeaksEnglishOne" runat="server" style="display: none">
                                                                                                <td class="bodytextbold_rightv" width="34%">
                                                                                                    Speaks English?
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="keyheader" colspan="2" width="100%">
                                                                                                    Supplier/Credit 1
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Company Name:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Company Address:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="36%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="5%">
                                                                                                                City:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="25%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Country:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="7%">
                                                                                                                State:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="15%">
                                                                                                                ZIP/Postal Code:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="14%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Contact Name:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Phone Number:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trowSupplierSpeaksEnglishTwo" runat="server" style="display: none">
                                                                                                <td class="bodytextbold_rightv" width="34%">
                                                                                                    Speaks English?
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="keyheader" colspan="2" width="100%">
                                                                                                    Supplier/Credit 1
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Company Name:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Company Address:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="36%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="5%">
                                                                                                                City:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="25%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Country:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="7%">
                                                                                                                State:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="15%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="bodytextbold_rightv" width="15%">
                                                                                                                ZIP/Postal Code:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView" width="14%">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Contact Name:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" align="left">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td class="bodytextbold_rightv" width="34%">
                                                                                                                Phone Number:
                                                                                                            </td>
                                                                                                            <td class="bodytextleftView">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="trowSupplierSpeaksEnglishThree" runat="server" style="display: none">
                                                                                                <td class="bodytextbold_rightv" width="34%">
                                                                                                    Speaks English?
                                                                                                </td>
                                                                                                <td class="bodytextleftView">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                <tr>
                                                                    <td id="SuplDetails" runat="server" style="display: none; padding: 5px 10px 0px 10px">
                                                                        <asp:Repeater ID="rptsupl" runat="server" OnItemDataBound="rptSupl_ItemDataBound"
                                                                            OnItemCommand="rptSupl_ItemCommand">
                                                                            <HeaderTemplate>
                                                                                <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td class="keyheader" colspan="4">
                                                                                        Supplier/Credit
                                                                                        <asp:Label ID="lblCount1" runat="server"></asp:Label>&nbsp
                                                                                        <asp:HyperLink ID="hlReqSupReq" runat="server" Target="_blank" Text="(Reference Request)"
                                                                                            CssClass=""></asp:HyperLink>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdheight" colspan="4">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv" width="35%">
                                                                                        Company Name: &nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextleftView" colspan="3">
                                                                                        <%# Eval("CompanyName")%>
                                                                                        <asp:HiddenField ID="hdnSupplierId" runat="server" Value='<%# Eval("PK_SupplierId") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv">
                                                                                        Contact Name: &nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextleftView" colspan="3">
                                                                                        <%# Eval("ContactName")%>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv" width="35%">
                                                                                        Company Address: &nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextleftView" width="35%">
                                                                                        <%# Eval("CompanyAddress")%>&nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextbold_rightv" width="5%">
                                                                                        City:&nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextleftView" width="25%">
                                                                                        <%# Eval("City")%>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4" width="100%">
                                                                                        <table width="100%" cellpadding="0">
                                                                                            <tr>
                                                                                                <td class="bodytextbold_rightv" width="35%">
                                                                                                    Country:&nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="20%">
                                                                                                    <%# Eval("CountryName")%>&nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextbold_rightv" width="5%">
                                                                                                    State:&nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="12%">
                                                                                                    <asp:Label ID="LblState" runat="server" Text='<%# Eval("Fk_State") %>'></asp:Label>&nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextbold" width="12%">
                                                                                                    ZIP/Postal Code:&nbsp;
                                                                                                </td>
                                                                                                <td class="bodytextleftView" width="16%">
                                                                                                    <%# Eval("ZipCode")%>&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trOther" runat="server" style="display: none">
                                                                                    <td class="bodytextbold_rightv" width="65%" colspan="3">
                                                                                        Please Specify State
                                                                                    </td>
                                                                                    <td class="bodytextleft" colspan="2">
                                                                                        <asp:Label ID="LblOtherState" runat="server" Text='<%# Eval("OtherState") %>'></asp:Label>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytextbold_rightv">
                                                                                        Phone Number: &nbsp;
                                                                                    </td>
                                                                                    <td class="bodytextleftView" colspan="3">
                                                                                        <%# Eval("Phone")%>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trSpeakEnglishSupplier" runat="server" style="display: none">
                                                                                    <td class="bodytextbold_rightv">
                                                                                        Speaks English?
                                                                                    </td>
                                                                                    <td class="bodytextleftView" colspan="3">
                                                                                        <asp:Label ID="lblQuestionOne" runat="server" TabIndex="0" Text='<%# Eval("IsSpeaksEnglishText") %>'></asp:Label>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <%--  /  <tr>
                                                                                                    <td valign="top" colspan="2">
                                                                                                        <table width="100%">
                                                                                                            <tr id="tdlnkl1" runat="server" style="display: none">
                                                                                                                <td class="bodytextbold_rightv">
                                                                                                                    Notes: &nbsp;
                                                                                                                </td>
                                                                                                                <td class="bodytextleftView ">
                                                                                                                    <asp:Label ID="Label55" runat="server">Notes</asp:Label>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>////--%>
                                                                                <span class="hide_print">
                                                                                    <tr>
                                                                                        <td colspan="4" valign="top">
                                                                                            <%--<span id="spannworkcompletedsupl" runat="server">--%>
                                                                                            <table cellpadding="3" cellspacing="3" width="100%" border="0">
                                                                                                <%--G. Vera 07/18/2014: The curly brackets were misplaced and changed the width of the notes text box to 100%
                                                                                                so that it is set to the parents element width--%>
                                                                                                <% if (Request.QueryString["user"] == "admin")
                                                                                                   { %>
                                                                                                        <tr>
                                                                                                            <td id="tdlblsupl" runat="server" width="33%" class="left_border hide_print">
                                                                                                                <asp:LinkButton ID="tdlnksupl" runat="server" Text="Click to add notes" CommandName="cmdnamelnksupl"></asp:LinkButton>
                                                                                                            </td>
                                                                                                            <td id="tdtxtsupl" runat="server" width="64%" style="display: none; text-align: right;">
                                                                                                                <asp:TextBox TextMode="MultiLine" Width="100%" ID="txtsuplnotes" Text='<%# Eval("AddNote") %>'
                                                                                                                    runat="server" Height="50px" onFocus="javascript:textCounter(this,1000,'yes');"
                                                                                                                    onKeyDown="javascript:textCounter(this,1000,'yes');" onKeyUp="javascript:textCounter(this,1000,'yes');"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                <% }%>
                                                                                             </table>
                                                                                                <!-- TEMP REMOVED *** hlReqSup: Supplier Reference Request ***  Width="609px" -->
                                                                                         </td>
                                                                                    </tr>
                                                                                    
                                                                                    <tr>
                                                                                        <td colspan="4" valign="top" align="right">
                                                                                            <table cellpadding="15" cellspacing="10" width="100%" border="0">
                                                                                                <tr id="tdsuplnotetitle" runat="server" style="display: none">
                                                                                                    <td width="33%" class="special" valign="top">
                                                                                                        <asp:Label ID="lblusernotetitle3" runat="server" Text="Haskell Admin Notes:"></asp:Label>
                                                                                                    </td>
                                                                                                    <td id="tdsuplnotedesc" runat="server" class="bodytextleftView" width="67%" align="right">
                                                                                                        &nbsp;
                                                                                                        <asp:Label ID="lblsuplnotedesc" Text='<%# Eval("AddNote") %>' runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--</table>--%>
                                                                                    <%--</span>--%>
                                                                                    </td> </tr> </span>
                                                                                <tr>
                                                                                    <td class="tdheight" colspan="4">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4" style="border-top: 1px solid #cccccc">
                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table></FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" style="padding-right: 10px">
                                                                        <span class="hide_print">
                                                                            <asp:ImageButton ID="imgExportexcelsupplier" runat="server" Visible="false" ImageUrl="~/Images/extoexcel.jpg"
                                                                                OnClick="imgExportexcelsupplier_Click" /></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdheight">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="imgExportexcelproject" />
                                                            <asp:PostBackTrigger ControlID="imgExportexcelsupplier" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdheight">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bodytextcenter">
                                        <span class="hide_print">
                                            <asp:ImageButton ID="imgbtnupdate" runat="server" ToolTip="Update" ImageUrl="~/Images/update.jpg"
                                                Visible="false" OnClick="imgbtnupdate_Click" />
                                            <img src="../Images/print.jpg" id="imgprint" class="CursorStyle" title="Print" runat="server"
                                                onclick="JavaScript:printPartOfPage();" visible="false" />
                                            <asp:ImageButton ID="imgPDF" runat="server" ImageUrl="~/Images/ex2pdf.jpg" OnClick="imgPDF_Click" /><asp:HiddenField
                                                ID="hdVendorId" runat="server" />
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="Footer" runat="server" />
                            <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" CancelControlID="imbOk">
                            </Ajax:ModalPopupExtender>
                            <asp:HiddenField ID="lblHidden" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="modalPopupDiv" class="popupdivsmall" style="display: none">
                                <table cellpadding="0" cellspacing="0" border="0" width="425">
                                    <tr>
                                        <td class="searchhdrbarbold" runat="server" id="msgTd" colspan="3">
                                            <asp:Label ID="lblheading" runat="server" CssClass="searchhdrbarbold"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl">
                                            &nbsp;
                                            <asp:Label ID="lblMsg" runat="server" CssClass="summaryerrormsg"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="popupdivcl" align="center">
                                            <input type="button" value="OK" id="imbOk" title="Ok" class="ModalPopupButton" onclick="$find('modalExtnd').hide();" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="popupdivcl">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>
</body>
</html>
