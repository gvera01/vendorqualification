﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using System.Collections.Generic;
using VMSDAL;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 10/17/2012: VMS Stabilization Release 1.3 (refer to its JIRA issues for details)
 * 003 G. Vera 02/28/2013: VMS Enhancement Release 1.3.1 (BIM Questions)
 * 004 Sooraj Sudhakaran.T 10/11/2013: VMS Modification - Added code to handle continent selected for trade regions 
 * 005 - G. Vera 05/29/2014: New BIM questions for Design Consultant
 ****************************************************************************************************************/
#endregion

public partial class VQFView_TradeView : CommonPage
{
    #region Declaration

    Common objCommon = new Common();
    clsTradeInfomation objTradeInformation = new clsTradeInfomation();
    //TableRow tr;
    //TableCell tc;
    //TableCell tc1;
    static bool IsDesignConsultant = false;     //005
    long tradeId;
    long VendorId;
    int count;

    #endregion

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            if (Request.QueryString.Count > 0)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Print"]))
                {
                    VQFViewMenu.Visible = false;
                }
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["match"])))
            {
                imgprint.Attributes.Add("onClick", "printPartOfPageFromMatch()");
            }
            else
            {
                imgprint.Attributes.Add("onClick", "printPartOfPage()");
            }
            if ((string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])) || Convert.ToString(Session["VendorAdminId"]) == "0"))
            {
                Response.Redirect("~/Admin/Login.aspx");
            }
            else
            {
                VendorId = Convert.ToInt32(Session["VendorAdminId"]);
                hdVendorId.Value = Convert.ToInt64(VendorId).ToString();
            }
            // get company details
            if (!string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])))
            {
                long VendorID = Convert.ToInt64((Session["VendorAdminId"]));    //005
                clsRegistration objregistration = new clsRegistration();
                DAL.UspGetVendorDetailsByIdResult[] objResult = objregistration.GetVendor(VendorID);        //005
                
                if (objResult.Count() > 0)
                {
                    lblVendorName.Text = objResult[0].Company;
                }

                //005
                IsDesignConsultant = (new clsCompany().GetTypeOfCompany(VendorID) == TypeOfCompany.DesignConsultant);
            }
            int flag = 0;
            tblTradeInfo.Width = Unit.Percentage(100);
            int resCount = objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorId)).Length;
            if (resCount > 0)
            {
                // Display Trade
                tradeId = objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorId))[0].PK_TradeID;
                // Display Scopes
                count = objTradeInformation.GetSelectedTradeScopesHeading(tradeId).Length;
                if (count > 0)
                {
                    tblTradeScopeHeader.Style["Display"] = "";
                    trScopeCompleteDetails.Style["Display"] = "";
                    trScopeEmptyDetails.Style["display"] = "none";
                    int count1 = count - (Convert.ToInt32(Math.Round(Convert.ToDouble(count / 2))));
                    if (count1 >= 0 && count1 <= count)
                    {
                        if (count1 == 0)
                        {
                            count1 = 1;
                        }
                        for (int i = 0; i < count1; i++)
                        {
                            flag += 1;
                            Table tbl = new Table();
                            TableRow tr = new TableRow();
                            TableCell tc = new TableCell();
                            Label lbl = new Label();
                            TreeView tv = new TreeView();
                            lbl.Text = objTradeInformation.GetSelectedTradeScopesHeading(tradeId)[i].Name;
                            lbl.CssClass = "tree_head";
                            tv.ShowExpandCollapse = false;
                            tv.Attributes.Add("onClick", "return test();");
                            foreach (DAL.USPGetSelectedTradescopesResult obj1 in objTradeInformation.GetSelectedTradeScopes(objTradeInformation.GetSelectedTradeScopesHeading(tradeId)[i].Pk_ScopeId, Convert.ToInt64(VendorId)))
                            {
                                TreeNode tn = new TreeNode();
                                tn.Text = obj1.Name;
                                foreach (DAL.USPGetSelectedTradescopesResult obj2 in objTradeInformation.GetSelectedTradeScopes(obj1.Pk_ScopeId, Convert.ToInt64(VendorId)))
                                {
                                    TreeNode tn1 = new TreeNode();
                                    tn1.Text = obj2.Name;
                                    tn1.Selected = false;
                                    tn.ChildNodes.Add(tn1);
                                }
                                tv.Nodes.Add(tn);
                            }

                            tv.CssClass = "Treestyle";
                            tv.RootNodeStyle.CssClass = "list_display";
                            tv.LeafNodeStyle.CssClass = "list_display";
                            tc.Controls.Add(lbl);
                            tc.Controls.Add(tv);
                            tr.Cells.Add(tc);
                            tr.Cells.Add(tc);
                            tbl.Rows.Add(tr);
                            tdCell1.Controls.Add(tbl);
                        }
                        for (int i = count1; i < count; i++)
                        {
                            flag += 1;
                            Table tbl = new Table();
                            TableRow tr = new TableRow();
                            TableCell tc = new TableCell();
                            Label lbl = new Label();
                            TreeView tv = new TreeView();

                            lbl.Text = objTradeInformation.GetSelectedTradeScopesHeading(tradeId)[i].Name;
                            lbl.CssClass = "tree_head";
                            tv.ShowExpandCollapse = false;
                            tv.Attributes.Add("onClick", "return test();");
                            foreach (DAL.USPGetSelectedTradescopesResult obj1 in objTradeInformation.GetSelectedTradeScopes(objTradeInformation.GetSelectedTradeScopesHeading(tradeId)[i].Pk_ScopeId, Convert.ToInt64(VendorId)))
                            {
                                TreeNode tn = new TreeNode();
                                tn.Text = obj1.Name;
                                foreach (DAL.USPGetSelectedTradescopesResult obj2 in objTradeInformation.GetSelectedTradeScopes(obj1.Pk_ScopeId, Convert.ToInt64(VendorId)))
                                {
                                    TreeNode tn1 = new TreeNode();
                                    tn1.Text = obj2.Name;
                                    tn1.Selected = false;
                                    tn.ChildNodes.Add(tn1);
                                }
                                tv.Nodes.Add(tn);
                            }
                            tv.CssClass = "Treestyle";
                            tv.RootNodeStyle.CssClass = "list_display";
                            tv.LeafNodeStyle.CssClass = "list_display";
                            tc.Controls.Add(lbl);

                            tc.Controls.Add(tv);
                            tr.Cells.Add(tc);
                            tr.Cells.Add(tc);
                            tbl.Rows.Add(tr);
                            tdCell2.Controls.Add(tbl);
                        }
                    }
                    else
                    {
                        TradeScopeHeader.Visible = false;
                    }
                }
                else
                {
                    tblTradeScopeHeader.Style["Display"] = "";
                    trScopeCompleteDetails.Style["display"] = "none";
                    trScopeEmptyDetails.Style["Display"] = "";
                }

                //006- Display Continents
                dlContinents.DataSource = objTradeInformation.GetVQFTradeRegionContinentsView(tradeId);
                dlContinents.DataBind();

                // Display trade regions
                dlRegions.DataSource = objTradeInformation.GetSelectedTradeRegions(-1, tradeId);
                dlRegions.DataBind();
                // Display license Numbers
                rptLicenseNumber.DataSource = objTradeInformation.GetSelectedTradeLicenseNumbers(tradeId);
                rptLicenseNumber.DataBind();

                //002 - added below
                //List<VendorTradeQuestions> vendorQuestions = objTradeInformation.GetVendorTradeQuestions(tradeId).Where(q => q.YesNoAnswer == "Y").ToList();
                List<VendorTradeQuestions> vendorQuestions = objTradeInformation.GetTradeScopeQuestions(tradeId);
                hrTradeQuestions.Attributes["style"] = (vendorQuestions.Count > 0) ? "display: " : "display: none";
                dlTradeQuestions.DataSource = vendorQuestions;
                dlTradeQuestions.DataBind();

                //003 - added below
                //005 - modified
                List<VendorTradeQuestions> bimQuestions = new List<VendorTradeQuestions>();
                bimQuestions = (!IsDesignConsultant) ? objTradeInformation.GetTradeBIMQuestions(tradeId) : objTradeInformation.GetTradeBIMQuestions(tradeId, VQFSection.TradeBIMDesign);
                List<VendorTradeQuestions> dataSource = new List<VendorTradeQuestions>();
                if (bimQuestions.Count > 0)
                {
                    dataSource = bimQuestions.Where(q => q.YesNoAnswer != "" || q.OtherAnswer != "").ToList();
                }
                trBIMNoData.Attributes["style"] = (dataSource.Count > 0) ? "display: none" : "display: ";
                dlBIMQuestions.DataSource = dataSource;
                dlBIMQuestions.DataBind();

                if (rptLicenseNumber.Items.Count > 0)
                {
                    trEmptyLicense.Style["display"] = "none";
                    trLicenseComplete.Style["Display"] = "";
                }
                else
                {
                    trLicenseComplete.Style["display"] = "none";
                    trEmptyLicense.Style["Display"] = "";
                }
                if (Convert.ToString(objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorId))[0].OwnForces) == "False")
                {
                    lblOwnForces.Text = "No";
                    trExplain.Style["Display"] = "";
                    lblExplain.Text = objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorId))[0].Explain;
                }
                else if (Convert.ToString(objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorId))[0].OwnForces) == "True")
                {
                    lblOwnForces.Text = "Yes";
                    trExplain.Style["display"] = "none";
                }
                else
                {
                    lblOwnForces.Text = "";
                    trExplain.Style["display"] = "none";
                }
                if ((dlRegions.Items.Count == 0) && (rptLicenseNumber.Items.Count <= 0) && (string.IsNullOrEmpty(Convert.ToString(objTradeInformation.GetVQFTrade(Convert.ToInt32(VendorId))[0].OwnForces))))
                {
                    tblTradeRegionHeader.Style["Display"] = "";
                }
                if ((rptLicenseNumber.Items.Count <= 0) && (dlRegions.Items.Count <= 0) && (lblOwnForces.Text == "") && (lblExplain.Text == ""))
                {
                    TradeRegionHeader.Visible = true;
                }
                // Display selected Projects
                dlProjects.DataSource = objTradeInformation.GetSelectedTradeProjects(tradeId, true);
                dlProjects.DataBind();
                if (dlProjects.Items.Count <= 0)
                {
                    ProjectTypesHeader.Visible = true;
                    ProjectTypesContent.Visible = true;
                    trCompleteProjects.Style["display"] = "none";
                    trEmptyProjects.Style["Display"] = "";
                }
                else
                {
                    trEmptyProjects.Style["display"] = "none";
                    trCompleteProjects.Style["Display"] = "";
                }
            }
            else
            {
                trBIMNoData.Attributes["style"] = "display: ";
            }
        }
        if (count > 0)
        {
            tblTradeScopeHeader.Style["Display"] = "";
            trScopeCompleteDetails.Style["Display"] = "";
            trScopeEmptyDetails.Style["display"] = "none";
        }
        else
        {
            tblTradeScopeHeader.Style["Display"] = "";
            trScopeCompleteDetails.Style["display"] = "none";
            trScopeEmptyDetails.Style["Display"] = "";
        }

        if (rptLicenseNumber.Items.Count > 0)
        {
            trEmptyLicense.Style["display"] = "none";
            trLicenseComplete.Style["Display"] = "";
        }
        else
        {
            trLicenseComplete.Style["display"] = "none";
            trEmptyLicense.Style["Display"] = "";
        }
        dlRegions.Visible = dlRegions.Items.Count > 0;
        dlContinents.Visible = dlContinents.Items.Count > 0;
        if (dlRegions.Items.Count > 0 || dlContinents.Items.Count > 0)
        {

            tblTradeRegionHeader.Style["Display"] = "";
            TradeRegionHeader.Visible = true;
            trCompleteRegions.Style["Display"] = "";
            trEmptyRegions.Style["display"] = "none";
        }
        else
        {
            tblTradeRegionHeader.Style["Display"] = "";
            TradeRegionHeader.Visible = true;
            trCompleteRegions.Style["display"] = "none";
            trEmptyRegions.Style["Display"] = "";
           
        }

        if (dlProjects.Items.Count <= 0)
        {
            ProjectTypesHeader.Visible = true;
            ProjectTypesContent.Visible = true;
            trCompleteProjects.Style["display"] = "none";
            trEmptyProjects.Style["Display"] = "";
        }
        else
        {
            trEmptyProjects.Style["display"] = "none";
            trCompleteProjects.Style["Display"] = "";
        }
    }

    #endregion
    
    protected void dlRegions_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblOtherState = (Label)e.Item.FindControl("lstRegions");
            string OtherState = lblOtherState.Text;
            lblOtherState.Text = OtherState;

            Label lbl = (Label)e.Item.FindControl("lblName");

            if (lbl.Text.Equals("CA"))
            {
                DAL.UspGetSelectedRegionsResult[] objRegions = objTradeInformation.GetSelectedTradeRegions(5, tradeId);
                if (objRegions.Length > 0)
                {
                    string Regions = string.Empty;
                    for (int i = 0; i < objRegions.Length; i++)
                    {
                        Regions += "<li>" + objRegions[i].Statecode;
                    }
                    Label lblRegions = (Label)e.Item.FindControl("lstRegions");
                    lblRegions.Text = Regions;
                }
            }
            if (lbl.Text.Equals("FL"))
            {
                DAL.UspGetSelectedRegionsResult[] objRegions = objTradeInformation.GetSelectedTradeRegions(9, tradeId);
                if (objRegions.Length > 0)
                {
                    string Regions = string.Empty;
                    for (int i = 0; i < objRegions.Length; i++)
                    {
                        Regions += "<li>" + objRegions[i].Statecode;
                    }
                    Label lblRegions = (Label)e.Item.FindControl("lstRegions");
                    lblRegions.Text = Regions;
                }
            }
            if (lbl.Text.Equals("TX"))
            {
                DAL.UspGetSelectedRegionsResult[] objRegions = objTradeInformation.GetSelectedTradeRegions(43, tradeId);
                if (objRegions.Length > 0)
                {
                    string Regions = string.Empty;
                    for (int i = 0; i < objRegions.Length; i++)
                    {
                        Regions += "<li>" + objRegions[i].Statecode;
                    }
                    Label lblRegions = (Label)e.Item.FindControl("lstRegions");
                    lblRegions.Text = Regions;
                }
            }
        }
    }

    protected void ddlGroups_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    #region Button Events

    protected void imbsubmit_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("Safety.aspx");
    }

    #endregion

    protected void rptLicenseNumber_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lbl = new Label();
            lbl = (Label)e.Item.FindControl("lblExpirationDate");
            if (lbl.Text.Trim() == "1/1/1753")
            {
                lbl.Text = string.Empty;
            }
            Label lblState = new Label();
            lblState = (Label)e.Item.FindControl("lblState");
            Label lblOtherState = new Label();
            lblOtherState = (Label)e.Item.FindControl("lblOtherState");
            if (lblState.Text.Equals("Other"))
            {
                lblState.Visible = false;
                lblOtherState.Visible = true;
            }
            else
            {
                lblState.Visible = true;
                lblOtherState.Visible = false;
            }
        }
    }

    #region Button Events

    protected void imgPDF_Click(object sender, ImageClickEventArgs e)
    {
        //002
        Common objCommon = new Common();
        string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/VQF/PrintVQFForm.aspx?vendorid=" + hdVendorId.Value + "&print=1";
        string baseURI = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;

        objCommon.Print2PDF(url, baseURI);
        //objCommon.GeneratePDF(Convert.ToInt64(hdVendorId.Value), Convert.ToString(Request.QueryString["user"]));
    }

    #endregion

    /// <summary>
    /// 002 - implemented
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlProjects_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if ((e.Item.DataItem as VMSDAL.UspGetSelectedProjectsResult).ProjectName.Trim().ToUpper() == "OTHER")
        {
            (e.Item.FindControl("lblProjects") as Label).Text += ": " + (e.Item.DataItem as VMSDAL.UspGetSelectedProjectsResult).OtherProjectName;
        }
    }

    /// <summary>
    /// 002 - implemented
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlTradeQuestions_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        VendorTradeQuestions vq = e.Item.DataItem as VendorTradeQuestions;
        Literal lbl = e.Item.FindControl("lblTradeQuestions") as Literal;
        if (vq.YesNoAnswer == "Y") lbl.Text = lbl.Text + ":&nbsp;&nbsp;&nbsp;" + "<u>Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>";
        if (vq.YesNoAnswer == "N") lbl.Text = lbl.Text + ":&nbsp;&nbsp;&nbsp;" + "<u>No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u/>";
        if (vq.YesNoAnswer == "NA") lbl.Text = lbl.Text + ":&nbsp;&nbsp;&nbsp;" + "<u>N/A&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u/>";
        if (string.IsNullOrEmpty(vq.YesNoAnswer)) lbl.Text = lbl.Text + ":&nbsp;&nbsp;&nbsp;" + "<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u/>";
    }

    /// <summary>
    /// 003 - implemented
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlBIMQuestions_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            VendorTradeQuestions vq = e.Item.DataItem as VendorTradeQuestions;
            Label lbl = e.Item.FindControl("lblBIMAnswer") as Label;

            if (!string.IsNullOrEmpty(vq.OtherAnswer)) lbl.Text = vq.OtherAnswer;
            else lbl.Text = (vq.YesNoAnswer == "Y") ? "Yes" : ((vq.YesNoAnswer == "N") ? "No" : "") ;

        }
    }
    protected void dlBIMQuestions_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}