﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TradeView.aspx.cs" Inherits="VQFView_TradeView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache">
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <script src="../Script/jquery.js" type="text/javascript"></script>
    <script src="../Script/common.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function printPartOfPage() {
            var printWindow = window.open('../Admin/VQFView.aspx?act=print&user=<%=Request.QueryString["user"] %>', '', 'left=50000,top=50000,width=0,height=0');
            printWindow.focus();
            printWindow.print();
        }
        function printPartOfPageFromMatch() {
            var printWindow = window.open('../Admin/VQFView.aspx?act=print&user=<%=Request.QueryString["user"] %>&Match=match', '', 'left=50000,top=50000,width=0,height=0');
            printWindow.focus();
            printWindow.print();
        }
    </script>
    <script language="javascript" type="text/javascript">
        function test() {
            return false;
        }
    </script>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
    <style media="print">
        .hide_print
        {
            display: none;
        }
    </style>
</head>
<body onload="ResetScrollPosition();">
    <form id="form1" runat="server" defaultbutton="imgPDF">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="650px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bodytextboldHead left_borderView">
                            <asp:Label ID="lblVendorName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="contenttdview">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <span class="hide_print">
                                                        <Haskell:VQFViewMenu ID="VQFViewMenu" runat="server" />
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formhead">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="50%">
                                                                Vendor Qualification Form - Trade Information
                                                            </td>
                                                            <td width="50%">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="TradeInfoDiv">
                                                        <table id="tblTradeScopeHeader" runat="server" align="center" border="0" cellpadding="5"
                                                            cellspacing="0" width="100%" class="searchtableclass">
                                                            <tr id="TradeScopeHeader" runat="server">
                                                                <td class="searchhdrbarbold" colspan="2">
                                                                    Scopes of Work
                                                                </td>
                                                            </tr>
                                                            <tr id="trScopeEmptyDetails" runat="server" style="display: none">
                                                                <td>
                                                                    <%-- <table width="100%">
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="50%">
                                                                   01 – GENERAL REQUIREMENTS
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                    25 – INTEGRATED AUTOMATION
                                                                 </td >
                                                                 </tr>
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="50%" >
                                                                    02 – EXISTING CONDITIONS
                                                                 </td>
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                   26 – ELECTRICAL
                                                                 </td >
                                                                 </tr>
                                                                 <tr>
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                 03 – CONCRETE</td>
                                                                 <td class="bodytextbold left_border" width="50%">
                                                                 27 – COMMUNICATIONS
                                                                 </td >
                                                                 </tr> 
                                                                <tr>
                                                                 <td class="bodytextbold left_border" width="50%">
                                                                04 – MASONRY
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                 28 – ELECTRONIC SAFETY AND SECURITY
                                                                 </td >
                                                                 </tr>                                                                 
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="50%" >
                                                                 05 – METALS</td>
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                 31 – EARTHWORK
                                                                 </td >
                                                                 </tr>
                                                                 <tr>
                                                                  <td class="bodytextbold left_border" width="50%" >
                                                                 06 – WOOD, PLASTICS AND COMPOSITES</td>
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                 32 – EXTERIOR IMPROVEMENTS
                                                                 </td>
                                                                 </tr>                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="50%">
                                                                 07 – THERMAL AND MOISTURE PROTECTION
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="50%">
                                                                 33 – UTILITIES
                                                                 </td ></tr>
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="50%" >
                                                                 08 – OPENINGS</td>
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                 34 – TRANSPORTATION
                                                                 </td >
                                                                 </tr>
                                                                 <tr>
                                                                  <td class="bodytextbold left_border" width="50%" >
                                                                  09 –  FINISHES</td>
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                 35 – WATERWAY AND MARINE CONSTRUCTION
                                                                 </td >
                                                                 </tr>
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="50%">
                                                                   10 – SPECIALITIES
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                40 – PROCESS INTEGRATION
                                                                 </td >
                                                                 </tr>
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="50%" >
                                                                 11 – EQUIPMENT
                                                                 </td>
                                                                  <td class="bodytextbold left_border" width="50%">
                                                               41 –  MATERIAL PROCESSING AND HANDLING EQUIPMENT
                                                                 </td >
                                                                 </tr>
                                                                 <tr>
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                 12 – FURNISHINGS</td>
                                                                 <td class="bodytextbold left_border" width="50%">
                                                                  42 – PROCESS HEATING, COOLING AND DRYING EQUIPMENT
                                                                 </td >
                                                                 </tr>                                                                
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="50%">
                                                                 13 – SPECIAL CONSTRUCTION
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                43 – PROCESS GAS AND LIQUID HANDLING, PURIFICATION
                                                                 </td >
                                                                 </tr>                                                                 
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="50%" >
                                                                 14 – CONVEYING EQUIPMENT</td>
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                 44 –  POLLUTION CONTROL EQUIPMENT
                                                                 </td >
                                                                 </tr>
                                                                 <tr>
                                                                  <td class="bodytextbold left_border" width="50%" >
                                                                21 – FIRE SUPPRESSION</td>
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                 45 – INDUSTRY-SPECIFIC MANUFACTURING EQUIPMENT
                                                                 </td >
                                                                 </tr>                                                                  
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="50%">
                                                                 22 – PLUMBING
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="50%">
                                                                48 – ELECTRICAL POWER GENERATION
                                                                 </td ></tr>
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="50%" >
                                                                 23 – HEATING, VENTILATING AND AIR-CONDITIONING(HVAC)</td>
                                                                  <td class="bodytextbold left_border" width="50%">
                                                                 
                                                                 </td >
                                                                 </tr>                                                                                                                                                                                      
                                                                 
                                                                 
                                                                                                                                                                                                             
                                                                                                                               
                                                                </table>  --%>
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td class="bodytextbold left_border" colspan="2">
                                                                                No data selected
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="trTradeQuestions" runat="server">
                                                                <td colspan="2" style="text-align: left; vertical-align: top; font-size: 12px; font-weight: bold;
                                                                    font-family: Tahoma;">
                                                                    <asp:DataList ID="dlTradeQuestions" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                                                                        CellPadding="10" OnItemDataBound="dlTradeQuestions_ItemDataBound">
                                                                        <ItemTemplate>
                                                                            <strong>
                                                                                <asp:Literal ID="lblTradeQuestions" runat="server" Text='<%# Bind("Question") %>' /></strong>
                                                                        </ItemTemplate>
                                                                    </asp:DataList>
                                                                    <hr id="hrTradeQuestions" runat="server" style="display: " />
                                                                </td>
                                                            </tr>
                                                            <tr id="trScopeCompleteDetails" runat="server" style="display: none">
                                                                <td colspan="2">
                                                                    <asp:Table ID="tblTradeInfo" runat="server" Width="100%">
                                                                        <asp:TableRow ID="trRow1" runat="server">
                                                                            <asp:TableCell ID="tdCell1" runat="server" VerticalAlign="Top" Width="50%"></asp:TableCell>
                                                                            <asp:TableCell ID="tdCell2" runat="server" VerticalAlign="Top" Width="50%"></asp:TableCell>
                                                                        </asp:TableRow>
                                                                        <%--G. Vera 10/23/2012 - Added--%>
                                                                        <%--<asp:TableRow ID="trRow2" runat="server">
                                                                            <asp:TableCell ID="tdCell3" runat="server" VerticalAlign="Top" Width="100%"></asp:TableCell>
                                                                        </asp:TableRow>--%>
                                                                    </asp:Table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table id="tblTradeRegionHeader" runat="server" align="center" border="0" cellpadding="5"
                                                            cellspacing="0" width="100%" class="searchtableclass">
                                                            <tr id="TradeRegionHeader" runat="server">
                                                                <td class="searchhdrbarbold" colspan="2">
                                                                    Regions
                                                                </td>
                                                            </tr>
                                                            <tr id="trCompleteRegions" runat="server" style="display: none">
                                                                <td colspan="2" style="padding-left: 15px">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td class="bodytextleft">
                                                                                <asp:DataList ID="dlRegions" runat="server" RepeatColumns="4" ItemStyle-CssClass="pad_left"
                                                                                    AlternatingItemStyle-CssClass="pad_left" RepeatDirection="Horizontal" Width="100%"
                                                                                    RepeatLayout="Table" ItemStyle-Wrap="true" ItemStyle-Width="150px" ItemStyle-VerticalAlign="Top"
                                                                                    AlternatingItemStyle-VerticalAlign="Top" OnItemDataBound="dlRegions_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="Label1" runat="server" CssClass="tree_head1" Text="United States of America"></asp:Label>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblName" runat="server" CssClass="tree_head1" Text='<%# Eval("StateCode") %>'></asp:Label>
                                                                                        <div style="padding-left: 15px;">
                                                                                            <asp:Label ID="lstRegions" runat="server" Text='<%# Eval("OtherSate") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:DataList>
                                                                                <asp:DataList ID="dlContinents" runat="server" RepeatColumns="4" ItemStyle-CssClass="pad_left"
                                                                                    AlternatingItemStyle-CssClass="pad_left" RepeatDirection="Horizontal" Width="100%"
                                                                                    RepeatLayout="Table" ItemStyle-Wrap="true" ItemStyle-Width="150px" ItemStyle-VerticalAlign="Top"
                                                                                    AlternatingItemStyle-VerticalAlign="Top">
                                                                                    <%--    <HeaderTemplate>
                                                                                        <span class="text">Continents</span>
                                                                                    </HeaderTemplate>--%>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblContinent" runat="server" CssClass="tree_head1" Text='<%# Eval("ContinentName") %>'></asp:Label>
                                                                                        <div style="padding-left: 15px;">
                                                                                            <asp:Label ID="lstCountries" runat="server" Text='<%# Eval("CountryName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:DataList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <%-- <asp:CheckBoxList ID="testcheck1" runat="server" RepeatColumns="4" RepeatDirection="Horizontal"
                                                                   CssClass="lstbox" Width="100%" AutoPostBack="true"
                                                                       onselectedindexchanged="testcheck1_SelectedIndexChanged" >
                                                                    --%>
                                                                </td>
                                                            </tr>
                                                            <tr id="trEmptyRegions" runat="server" style="display: none">
                                                                <td colspan="2">
                                                                    <%--          <table width="100%">
                                                                                <tr><td colspan="4">&nbsp;</td></tr>
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="AK" ID="chkAK" Enabled="false" />
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="AL" ID="CheckBox1" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="AR" ID="CheckBox2" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="AZ" ID="CheckBox3" Enabled="false"/>
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="CA" ID="CheckBox4" Enabled="false" />
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="CO" ID="CheckBox5" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="CT" ID="CheckBox6" Enabled="false" />
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="DE" ID="CheckBox7" Enabled="false" />
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="FL" ID="CheckBox8" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="GA" ID="CheckBox9" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="HI" ID="CheckBox10" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="IA" ID="CheckBox11" Enabled="false"/>
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="ID" ID="CheckBox12" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="IN" ID="CheckBox13" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="KS" ID="CheckBox14" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="KY" ID="CheckBox15" Enabled="false"/>
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="LA" ID="CheckBox16" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="MA" ID="CheckBox17" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="MD" ID="CheckBox18" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="ME" ID="CheckBox19" Enabled="false"/>
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="MN" ID="CheckBox20" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="MO" ID="CheckBox21" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="MS" ID="CheckBox22" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="MT" ID="CheckBox23" Enabled="false"/>
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="NC" ID="CheckBox24" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="ND" ID="CheckBox25" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="NE" ID="CheckBox26" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="NH" ID="CheckBox27" Enabled="false"/>
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="NJ" ID="CheckBox28" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="NM" ID="CheckBox29" Enabled="false" />
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="NV" ID="CheckBox30" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="NY" ID="CheckBox31" Enabled="false"/>
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="OH" ID="CheckBox32" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="OK" ID="CheckBox33" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="OR" ID="CheckBox34" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="PA" ID="CheckBox35" Enabled="false"/>
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="RI" ID="CheckBox36" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="SC" ID="CheckBox37" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="SD" ID="CheckBox38" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="TN" ID="CheckBox39" Enabled="false"/>
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="TX" ID="CheckBox40" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="UT" ID="CheckBox41" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="VA" ID="CheckBox42" Enabled="false" />
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="VT" ID="CheckBox43" Enabled="false"/>
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="WA" ID="CheckBox44" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="WI" ID="CheckBox45" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="WV" ID="CheckBox46" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                 <asp:CheckBox runat="server" Text="WY" ID="CheckBox47" Enabled="false"/>
                                                                 </td >
                                                                 </tr>
                                                                 
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="25%">
                                                                   <asp:CheckBox runat="server" Text="OTHER" ID="CheckBox48" Enabled="false" />
                                                                 </td >
                                                                 <td colspan="3">&nbsp;</td>                                                                 
                                                                 </tr>
                                                                    
                                                                                </table>--%>
                                                                    <table cellpadding="0" cellspacing="2" width="100%">
                                                                        <tr>
                                                                            <td class="bodytextbold left_border" colspan="2">
                                                                                No data selected
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="border-bottom: 1px solid #cccccc">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" class="bodytextbold">
                                                                    &nbsp;&nbsp;List License numbers in which your company is legally qualified to work
                                                                </td>
                                                            </tr>
                                                            <tr id="trLicenseComplete" runat="server" style="display: none;">
                                                                <td colspan="2">
                                                                    <table width="100%">
                                                                        <tr>
                                                                          
                                                                            <td>
                                                                                <asp:Repeater ID="rptLicenseNumber" runat="server" OnItemDataBound="rptLicenseNumber_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                        <td width="5%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold" width="25%" valign="top">
                                                                                                            Country
                                                                                                        </td>
                                                                                                        <td width="2%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold" width="25%" valign="top">
                                                                                                            State
                                                                                                        </td>
                                                                                                        <td width="2%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold" width="20%" valign="top">
                                                                                                            License Number
                                                                                                        </td>
                                                                                                        <td width="2%">
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td class="bodytextbold" width="15%" valign="top">
                                                                                                            Expiration Date
                                                                                                        </td>
                                                                                                    </tr>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                          <tr>
                                                                                                    <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        <asp:Label ID="Label3" runat="Server" Text='<%# Eval("CountryName") %>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        &nbsp;<asp:Label ID="lblState" runat="Server" Text='<%# Eval("StateCode") %>'></asp:Label>
                                                                                                        <asp:Label ID="lblOtherState" runat="Server" Text='<%# Eval("OtherState") %>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        <%# Eval("LicenseNumber")%>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" valign="top">
                                                                                                        <asp:Label ID="lblExpirationDate" runat="Server" Text='<%# Convert.ToDateTime(Eval("Expirationdate")).ToShortDateString()%>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table></FooterTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="trEmptyLicense" runat="server" style="display: none">
                                                                <td colspan="2">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td width="10%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="30%">
                                                                                Country:
                                                                            </td>
                                                                            <td class="bodytextleftView" width="60%">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="10%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="30%">
                                                                                State:
                                                                            </td>
                                                                            <td class="bodytextleftView" width="60%">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="10%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="30%">
                                                                                License Number:
                                                                            </td>
                                                                            <td class="bodytextleftView" width="60%">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="10%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="30%">
                                                                                Expiry Date:
                                                                            </td>
                                                                            <td class="bodytextleftView" width="60%">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td width="5%">&nbsp;
                                                                            </td>
                                                                            <td class="bodytextbold" width="32%">
                                                                                Will you perform all work with your own forces?
                                                                            </td>
                                                                            <td class="bodytextleftView" width="63%">
                                                                                <asp:Label ID="lblOwnForces" runat="server" CssClass="txtbox"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <table width="100%">
                                                                        <tr id="trExplain" runat="server" style="display: none">
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="25%" valign="top">
                                                                                If No, Please Explain:
                                                                            </td>
                                                                            <td class="bodytextleftView" width="70%">
                                                                                <asp:Label ID="lblExplain" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                            <tr id="ProjectTypesHeader" runat="server">
                                                                <td class="searchhdrbarbold" colspan="4">
                                                                    Types of Projects
                                                                </td>
                                                            </tr>
                                                            <tr id="ProjectTypesContent" runat="server" style="display: none">
                                                                <td class="exampletext">
                                                                    &nbsp;&nbsp;Project types that your company performs work or in which it specializes
                                                                </td>
                                                            </tr>
                                                            <tr id="trEmptyProjects" runat="server" style="display: none">
                                                                <td>
                                                                    <%--       <table width="100%">
                                                                 <tr><td colspan="3">&nbsp;</td></tr>
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="30%">
                                                                   <asp:CheckBox runat="server" Text="Airports" ID="CheckBox49" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="30%">
                                                                 <asp:CheckBox runat="server" Text="Aviation projects" ID="CheckBox50" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="40%">
                                                                 <asp:CheckBox runat="server" Text="bridges" ID="CheckBox51" Enabled="false"/>
                                                                 </td >                                                                 
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="30%">
                                                                   <asp:CheckBox runat="server" Text="Condominiums" ID="CheckBox53" Enabled="false" />
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="30%">
                                                                 <asp:CheckBox runat="server" Text="Distribution Facilities" ID="CheckBox54" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="40%">
                                                                 <asp:CheckBox runat="server" Text="Education Facilities" ID="CheckBox55" Enabled="false" />
                                                                 </td >
                                                                
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="30%">
                                                                   <asp:CheckBox runat="server" Text="Food Facilities" ID="CheckBox57" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="30%">
                                                                 <asp:CheckBox runat="server" Text="Government" ID="CheckBox58" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="40%">
                                                                 <asp:CheckBox runat="server" Text="Heavy Civil" ID="CheckBox59" Enabled="false"/>
                                                                 </td >
                                                               
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="30%">
                                                                   <asp:CheckBox runat="server" Text="Heavy Civil Infrastructure" ID="CheckBox61" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="30%">
                                                                 <asp:CheckBox runat="server" Text="Heavy Industrial" ID="CheckBox62" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="40%">
                                                                 <asp:CheckBox runat="server" Text="High-Rise Buildings" ID="CheckBox63" Enabled="false"/>
                                                                 </td >                                                                
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="30%">
                                                                   <asp:CheckBox runat="server" Text="Highway Projects" ID="CheckBox65" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="30%">
                                                                 <asp:CheckBox runat="server" Text="Highways" ID="CheckBox66" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="40%">
                                                                 <asp:CheckBox runat="server" Text="Hospitals" ID="CheckBox67" Enabled="false"/>
                                                                 </td >
                                                                
                                                                 </tr>
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="30%">
                                                                   <asp:CheckBox runat="server" Text="Life Care Facilities" ID="CheckBox69" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="30%">
                                                                 <asp:CheckBox runat="server" Text="Light Commercial" ID="CheckBox70" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="40%">
                                                                 <asp:CheckBox runat="server" Text="Light Industrial" ID="CheckBox71" Enabled="false"/>
                                                                 </td >
                                                                 
                                                                 </tr>
                                                                 
                                                                 
                                                                 
                                                                  <tr>
                                                                 <td class="bodytextbold left_border" width="30%">
                                                                   <asp:CheckBox runat="server" Text="Low-rise Buildings" ID="CheckBox77" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="30%">
                                                                 <asp:CheckBox runat="server" Text="Manufacturing Facilities" ID="CheckBox78" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="40%">
                                                                 <asp:CheckBox runat="server" Text="Marine" ID="CheckBox79" Enabled="false"/>
                                                                 </td >                                                                 
                                                                 </tr>
                                                                 
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="30%">
                                                                   <asp:CheckBox runat="server" Text="Medical Office Buildings" ID="CheckBox81" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="30%">
                                                                 <asp:CheckBox runat="server" Text="Office Towers" ID="CheckBox82" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="40%">
                                                                 <asp:CheckBox runat="server" Text="Office/Retail Buildings" ID="CheckBox83" Enabled="false"/>
                                                                 </td >                                                               
                                                                 </tr>
                                                                 
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="30%">
                                                                   <asp:CheckBox runat="server" Text="Parking Structures" ID="CheckBox85" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="30%">
                                                                 <asp:CheckBox runat="server" Text="Process Equipment" ID="CheckBox86" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="40%">
                                                                 <asp:CheckBox runat="server" Text="Prisons" ID="CheckBox87" Enabled="false"/>
                                                                 </td >                                                                
                                                                 </tr>
                                                                 
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="30%">
                                                                   <asp:CheckBox runat="server" Text="Recreational Facilities" ID="CheckBox89" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="30%">
                                                                 <asp:CheckBox runat="server" Text="Residentials" ID="CheckBox90" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="40%">
                                                                 <asp:CheckBox runat="server" Text="Schools" ID="CheckBox91" Enabled="false"/>
                                                                 </td >
                                                                
                                                                 </tr>
                                                                 
                                                                 <tr>
                                                                 <td class="bodytextbold left_border" width="30%">
                                                                   <asp:CheckBox runat="server" Text="Senior Living" ID="CheckBox93" Enabled="false"/>
                                                                 </td >
                                                                  <td class="bodytextbold left_border" width="30%">
                                                                 <asp:CheckBox runat="server" Text="Shopping Malls" ID="CheckBox94" Enabled="false"/>
                                                                 </td >
                                                                 <td class="bodytextbold left_border" width="40%">
                                                                 <asp:CheckBox runat="server" Text="Water And Wastewater Treatment Projects" ID="CheckBox95" Enabled="false"/>
                                                                 </td >                                                                
                                                                 </tr>                                                               
                                                                  <tr><td colspan="3">&nbsp;</td></tr>
                                                                    
                                                                                </table>--%>
                                                                    <table cellpadding="5" cellspacing="2" width="100%">
                                                                        <tr>
                                                                            <td class="bodytextbold left_border" colspan="2">
                                                                                No data selected
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr id="trCompleteProjects" runat="server" style="display: none;">
                                                                <td class="left_border">
                                                                    <asp:DataList ID="dlProjects" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                                                                        RepeatColumns="3" RepeatDirection="Horizontal" RepeatLayout="Table" OnItemDataBound="dlProjects_ItemDataBound">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblProjects" CssClass="bodytextleft" runat="server" Text='<%# Eval("ProjectName") %>'></asp:Label>
                                                                            <asp:HiddenField ID="hdnOtherProjName" runat="server" Value='<%#Eval("OtherProjectName") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:DataList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <%--G. Vera 02/28/2013 - added BIM section--%>
                                                        <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                            <tr id="Tr1" runat="server">
                                                                <td class="searchhdrbarbold" colspan="4">
                                                                    Building Information Modeling Qualifications (BIM)
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr2" runat="server" style="display: none">
                                                                <td class="exampletext">
                                                                    &nbsp;&nbsp;Any wording here????
                                                                </td>
                                                            </tr>
                                                            <tr id="trBIMNoData" runat="server" style="display: none">
                                                                <td>
                                                                    <table cellpadding="5" cellspacing="2" width="100%">
                                                                        <tr>
                                                                            <td class="bodytextbold left_border" colspan="2">
                                                                                No BIM Questions answered
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="left_border">
                                                                    <asp:DataList ID="dlBIMQuestions" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                                                                        RepeatColumns="1" RepeatDirection="Horizontal" RepeatLayout="Table" 
                                                                        OnItemDataBound="dlBIMQuestions_ItemDataBound" 
                                                                        OnSelectedIndexChanged="dlBIMQuestions_SelectedIndexChanged">
                                                                        <ItemTemplate>
                                                                            <div style="margin: 10PX">
                                                                                <div>
                                                                                    <asp:Label ID="lblBIMQuestion" CssClass="bodytextbold" runat="server" Text='<%# Eval("Question") %>'></asp:Label></div>
                                                                                <%--<div><br /></div>--%>
                                                                                <div>
                                                                                    <asp:Label ID="lblBIMAnswer" runat="server" CssClass="bodytextleftView" Width="100%" /></div>
                                                                                <div>
                                                                                    <br />
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:DataList>
                                                                </td>
                                                            </tr>
                                                            <%--                                                            <tr id="tr4" runat="server" style="display: none;">
                                                                <td class="left_border">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                Is your company capable of producing 3D fabrication model shop drawings (BIM)?
                                                                            </td>
                                                                            <td class="bodytextleftView" width=30%">
                                                                                <asp:Label ID="lblBIMQuestion1" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                What software package(s), including version(s), do you utilize for 3D modeling/detailing?
                                                                            </td>
                                                                            <td class="bodytextleftView" width="30%">
                                                                                <asp:Label ID="lblBIMQuestion1A" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                Is the 3D modeling performed by your own staff?
                                                                            </td>
                                                                            <td class="bodytextleftView" width="30%">
                                                                                <asp:Label ID="lblBIMQuestion1B" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                How many BIM draftsmen/detailers/coordinators do you have on staff?
                                                                            </td>
                                                                            <td class="bodytextleftView" width="30%">
                                                                                <asp:Label ID="lblBIMQuestion1C" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                What company do you utilize to produce 3D fabrication model shop drawings?
                                                                            </td>
                                                                            <td class="bodytextleftView" width="30%">
                                                                                <asp:Label ID="lblBIMQuestion1D" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                Explain the highest level of 3D detail you provided on your past projects. Please
                                                                                be specific and list for example, smallest pipe size modeled, and if any hangers,
                                                                                insulation, or "no fly-zone" clearances were included in the model.
                                                                            </td>
                                                                            <td class="bodytextleftView" width="30%">
                                                                                <asp:Label ID="lblBIMQuestion2" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                Has your company participated in 3D model coordination with other trades?
                                                                            </td>
                                                                            <td class="bodytextleftView" width="30%">
                                                                                <asp:Label ID="lblBIMQuestion3" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                Explain the extent of your involvement in model based trade coordination and include
                                                                                any clash detection software utilized during the coordination efforts, for example,
                                                                                Navisworks &reg;, Solibri &reg;, Vico &reg;, BIMsight &reg;, BIM 360 Glue &reg;,
                                                                                etc.
                                                                            </td>
                                                                            <td class="bodytextleftView" width="30%">
                                                                                <asp:Label ID="lblBIMQuestion3A" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                Does your company utilize 3D fabrication models for automated pre-fabrication?
                                                                            </td>
                                                                            <td class="bodytextleftView" width="30%">
                                                                                <asp:Label ID="lblBIMQuestion4" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                Explain the equipment and process, for example, direct download from model to plasma
                                                                                cutter.
                                                                            </td>
                                                                            <td class="bodytextleftView" width="30%">
                                                                                <asp:Label ID="lblBIMQuestion4A" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                Does your company utilize coordinates extracted from 3D fabrication models for field
                                                                                layouts?
                                                                            </td>
                                                                            <td class="bodytextleftView" width="30%">
                                                                                <asp:Label ID="lblBIMQuestion5" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="5%">
                                                                            </td>
                                                                            <td class="bodytextbold_rightv" width="50%">
                                                                                Explain the extend of what you layout utilizing model based coordinates.
                                                                            </td>
                                                                            <td class="bodytextleftView" width="30%">
                                                                                <asp:Label ID="lblBIMQuestion5A" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            --%>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <span class="hide_print">
                                                        <img src="../Images/print.jpg" id="imgprint" runat="server" title="Print" class="CursorStyle"
                                                            onclick="JavaScript:printPartOfPage();" visible="false" />
                                                        <asp:ImageButton ID="imgPDF" runat="server" ImageUrl="~/Images/ex2pdf.jpg" OnClick="imgPDF_Click" />
                                                        <asp:HiddenField ID="hdVendorId" runat="server" />
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
