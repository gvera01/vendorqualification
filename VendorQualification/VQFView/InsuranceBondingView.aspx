﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InsuranceBondingView.aspx.cs"
    Inherits="VQFView_InsuranceBondingView" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>
    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <script src="../Script/jquery.js" type="text/javascript"></script>
    <script src="../Script/common.js" type="text/javascript"></script>
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>
    <script language="javascript" type="text/javascript">
        function printPartOfPage() {
            var printWindow = window.open('../Admin/VQFView.aspx?act=print&user=<%=Request.QueryString["user"] %>', '', 'left=50000,top=50000,width=0,height=0');
            printWindow.focus();
            printWindow.print();
        }
        function printPartOfPageFromMatch() {
            var printWindow = window.open('../Admin/VQFView.aspx?act=print&user=<%=Request.QueryString["user"] %>&Match=match', '', 'left=50000,top=50000,width=0,height=0');
            printWindow.focus();
            printWindow.print();
        }
    </script>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
    </style>
    <style type="text/css" media="print">
        .hide_print
        {
            display: none;
        }
    </style>
</head>
<body onload="ResetScrollPosition();">
    <form id="form1" runat="server" defaultbutton="imgPDF">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="900px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bodytextboldHead left_borderView">
                            <asp:Label ID="lblVendorName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="contenttdview">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <span class="hide_print">
                                                        <Haskell:VQFViewMenu ID="VQFViewMenu" runat="server" />
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="InsyDiv" runat="server">
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="0" cellspacing="0" class="formhead">
                                                                        <tr>
                                                                            <td width="50%">
                                                                                Vendor Qualification Form - Insurance & Bonding
                                                                            </td>
                                                                            <td style="text-align: right" width="50%">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <%-- <asp:UpdatePanel ID="upnlBonding" runat="server">
                                                                        <ContentTemplate>--%>
                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                        <tr id="ContactInformationHeader" runat="server">
                                                                            <td class="searchhdrbarbold" colspan="2">
                                                                                Contact Information
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="sectionpad">
                                                                                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" width="40%" valign="top">
                                                                                            Broker / Company Name: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblBrokerorCompanyName" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Insurance Carrier: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblInsuranceCarrier" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Insurance Agent: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblInsuranceAgent" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Agent Phone Number: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblAgentPhone" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                        <tr>
                                                                            <td class="searchhdrbarbold" colspan="2">
                                                                                Aggregate Limits / Coverage
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="sectionpad">
                                                                                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td colspan="2" class="hdrbold">
                                                                                            General Liability
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" width="40%" valign="top">
                                                                                            Each Occurrence $: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblEachOccurances" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Product/Completed Operations Aggregate $: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblOPerations" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            General Aggregate $: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblGenralAggregate" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Is Policy "Occurrence Based"? &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblOccurrenceBased" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Is General Aggregate Limit "Per Project"? &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblGeneralAggregateLimit" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="hdrbold" colspan="2">
                                                                                            Additional Insured Endorsements
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right">
                                                                                            Will you provide additional insured endorsements
                                                                                                for ongoing and completed operations:
                                                                                        </td>
                                                                                        <td>
                                                                                            <span class="bodytextleftView">
                                                                                                <asp:Label ID="lblInsured" runat="server" Width="100%">&nbsp;</asp:Label></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trnotes" runat="server" style="display: none">
                                                                                        <td class="formtdrt" colspan="2">
                                                                                            Contractor shall be included as an additional insured under the CGL policy for both
                                                                                            ongoing and completed operations. ISO additional insured endorsements CG 20 10 (for
                                                                                            ongoing operations) and CG 20 37 (for completed operations) or substitute endorsements
                                                                                            providing equivalent coverage, will be attached to Subcontractor's CGL. The umbrella
                                                                                            insurance, if any, shall provide following form of additional insured coverage.
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="hdrbold" colspan="2">
                                                                                            Umbrella/Excess Liability
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Do You Have Umbrella/Excess Liability? &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblUmbrellaExcess" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trUmbrella" runat="server" style="display: none">
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Umbrella Each Occurrence/Aggregate: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblUmbrella" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trExcess" runat="server" style="display: none">
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Excess Each Occurrence/Aggregate: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblExcess" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="hdrbold" colspan="2">
                                                                                            Auto
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Each Accident $: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblEachAccident" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="hdrbold" colspan="2">
                                                                                            Workers Compensation And Employers' Liability
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Does WC Policy Meet Statutory Limits?
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblWCPolicy" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Employers Liability-Each Accident $: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblEmployees" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Employers liability Disease-Each Employee $: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblDiseaseEmployee" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Employers liability Disease-Policy Limit $: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblDiseasePolicy" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="hdrbold" colspan="2">
                                                                                            Professional Liability
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Each Occurrence $: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblEachProfessionalOccur" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--G. Vera 06/11/2021 - Added below--%>
                                                                                    <tr>
                                                                                        <td class="hdrbold" colspan="2">
                                                                                            Pollution Liability
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Each Occurrence/Aggregate $: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblPollutionLiability" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--G. Vera 07/19/2021 - Added below--%>
                                                                                    <tr>
                                                                                        <td class="hdrbold" colspan="2">
                                                                                            Are you willing to obtain Pollution Liability?
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Please explain: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblPollutionLiabExplain" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>

                                                                                    <%--G. Vera 10/26/2012 - Added below--%>
                                                                                    <tr>
                                                                                        <td class="hdrbold" colspan="2">
                                                                                            Marine Cargo/Inland Transit Insurance
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Marine Cargo Insurance? &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblMarineCargoIns" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trConveyanceLimitMarineCargo" runat="server" style="display: ">
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Per Conveyance Limit USD: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblMarineCargoConvLimit" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Inland Transit? &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblMarineCargoInlandTransit" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trConveyanceLimitInlandTrans" runat="server" style="display: ">
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Per Conveyance Limit USD: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblConveyanceLimitInlandTrans" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Storage? &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblMarineCargoStorage" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Installation of Equipment? &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblMarineCargoInstallEquip" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass"
                                                                        id="tblBondingAndSurety" runat="server">
                                                                        <tr>
                                                                            <td class="searchhdrbarbold" colspan="2">
                                                                                Bonding and Surety
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="sectionpad">
                                                                                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top" width="40%">
                                                                                            Bonding/Surety Company Name: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblBonding" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Company Contact Name: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblCompanyContactName" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Contact Telephone Number: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblContactPhonenumber" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Total Bonding Capacity $: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblTotalBonding" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_right" valign="top">
                                                                                            Available Bonding Capacity $: &nbsp;
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblCurrentBondingcapacity" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                        <td width="1%">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <%--</ContentTemplate>
                                                                    </asp:UpdatePanel>--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdheight">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <span class="hide_print">
                                                        <img src="../Images/print.jpg" id="imgprint" class="CursorStyle" runat="server" title="Print"
                                                            visible="false" onclick="JavaScript:printPartOfPage();" />
                                                        <asp:ImageButton ID="imgPDF" runat="server" ImageUrl="~/Images/ex2pdf.jpg" OnClick="imgPDF_Click" />
                                                        <asp:HiddenField ID="hdVendorId" runat="server" />
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
