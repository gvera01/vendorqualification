﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SafetyView.aspx.cs" Inherits="VQFView_SafetyView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>

    <script language="javascript" type="text/javascript">
        window.onerror = ErrHndl;

        function ErrHndl()
        { return true; }
    </script>

    <link href="../css/Haskell.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache">

    <script language="javascript" type="text/javascript">
        window.history.go(1);        
    </script>

    <script src="../Script/jquery.js" type="text/javascript"></script>
    <script src="../Script/common.js" type="text/javascript"></script>

    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11px;
            color: #333333;
            background: url(../images/body_bg.png) repeat-x;
            background-color: #2672b8;
            margin-top: 0px;
        }
        .style2
        {
            text-decoration: none;
            font-family: Tahoma;
            color: White;
            font-weight: bold;
            font-size: 11px;
            text-align: left;
            height: 20px;
            padding-left: 8px;
            padding-right: 3px;
            padding-top: 3px;
            padding-bottom: 3px;
            background-color: #005785;
        }
        /*G. VEra 06/12/2014: Added*/
        .auto-style1
        {
            font-family: Tahoma;
            font-size: 11px;
            color: black;
            text-align: right;
            font-weight: bold;
            width: 331px;
            padding-left: 0px;
            padding-right: 10px;
            padding-top: 5px;
            padding-bottom: 5px;
        }
    </style>
    <style media="print">
        .hide_print
        {
            display: none;
        }
    </style>
</head>
<body onload="ResetScrollPosition();">
    <form id="form1" runat="server" defaultbutton="imgPDF">
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="685px" border="0" align="center">
        <tr>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <Haskell:TopMenu ID="TopMenu" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bodytextboldHead left_borderView">
                            <asp:Label ID="lblVendorName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="contenttdview">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <span class="hide_print">
                                                        <Haskell:VQFViewMenu ID="VQFViewMenu" runat="server" />
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="SafetyDiv" runat="server">
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td class="formhead">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td width="50%">
                                                                                Vendor Qualification Form - Safety
                                                                            </td>
                                                                            <td width="50%">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                        <tr>
                                                                            <%--G. Vera 01/30/2013 - Changed header--%>
                                                                            <td class="style2" colspan="7">
                                                                                Experience Modification Rate (EMR)
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="tblEmrRate" runat="server" style="display: ;">
                                                                            <td style="padding-right: 10px; padding-left: 10px">
                                                                                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="bodytextbold" width="5%">
                                                                                            Year
                                                                                        </td>
                                                                                        <td class="bodytextbold" width="20%">
                                                                                            Rate
                                                                                        </td>
                                                                                        <%--G. Vera 06/20/2014: added--%>
                                                                                        <td class="bodytextcenter_bold" width="3%">
                                                                                            Doc
                                                                                        </td>
                                                                                        <td class="bodytextbold_rightv" width="5%">
                                                                                            N/A
                                                                                        </td>
                                                                                        <td class="bodytextbold" width="67%">
                                                                                            Please Explain
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="5">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblYear1" runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblRate1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                        <%--G. Vera 06/20/2014: added--%>
                                                                                        <td class="bodytextcenter_bold">
                                                                                            <asp:ImageButton ID="imbAttachEMR1" OnClick="imbAttach_Click" runat="server"
                                                                                                ImageUrl="~/Images/paperclip_gray.png" Enabled="false"/>
                                                                                        </td>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            <asp:CheckBox ID="chkNA1" runat="server" Enabled="false" />
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblExplain1" runat="server" CssClass="txtbox MultiLabel"></asp:Label>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblYear2" runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblRate2" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                        <%--G. Vera 06/20/2014: added--%>
                                                                                        <td class="bodytextcenter_bold">
                                                                                            <asp:ImageButton ID="imbAttachEMR2" OnClick="imbAttach_Click" runat="server"
                                                                                                ImageUrl="~/Images/paperclip_gray.png" Enabled="false" />
                                                                                        </td>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            <asp:CheckBox ID="chkNA2" runat="server" Enabled="false" />
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblExplain2" runat="server" CssClass="txtbox MultiLabel"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblYear3" runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblRate3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                        <%--G. Vera 06/20/2014: added--%>
                                                                                        <td class="bodytextcenter_bold">
                                                                                            <asp:ImageButton ID="imbAttachEMR3" OnClick="imbAttach_Click" runat="server"
                                                                                                ImageUrl="~/Images/paperclip_gray.png" Enabled="false" />
                                                                                        </td>
                                                                                        <td class="bodytextbold_rightv">

                                                                                            <asp:CheckBox ID="chkNA3" runat="server" Enabled="false" />
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblExplain3" runat="server" CssClass="txtbox MultiLabel"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--G. Vera 1/30/2013 - Added--%>
                                                                                    <tr id="trEMRYear4" runat="server" style="display: none">
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblYear4" runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblRate4" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                        <%--G. Vera 06/20/2014: added--%>
                                                                                        <td class="bodytextcenter_bold">
                                                                                            <asp:ImageButton ID="imbAttachEMR4" OnClick="imbAttach_Click" runat="server"
                                                                                                ImageUrl="~/Images/paperclip_gray.png" Enabled="false" />
                                                                                        </td>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            <asp:CheckBox ID="chkNA4" runat="server" Enabled="false" />
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblExplain4" runat="server" CssClass="txtbox MultiLabel"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdheight" colspan="5">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" class="searchtableclass">
                                                                        <tr>
                                                                            <td class="searchhdrbarbold">
                                                                                Safety History / OSHA 300 & 300 A
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                                            <td class="Safetytextbold">We understand that OSHA may not apply to your company. However, Haskell would like to obtain the information below.</td>
                                                                                            </tr>
                                                                        <tr>
                                                                            <td style="padding-left: 5px; padding-right: 10px; padding-bottom: 10px">
                                                                                <table cellpadding="5" cellspacing="0" width="100%" border="0">
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv" width="36%">
                                                                                            Year:
                                                                                        </td>
                                                                                        <td class="bodytextleftView" width="64%">
                                                                                            <asp:Label ID="lblOshaYear1" runat="server"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            N/A:
                                                                                        </td>
                                                                                        <td class="bodytextleftViewv">
                                                                                            <asp:CheckBox ID="chkOSHANA1" runat="server" Enabled="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Please Explain For N/A:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblExplainOsha1" runat="server" CssClass="txtbox MultiLabel"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of fatalities:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblOshafatalities1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of Cases with Days Away from Work:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblOshaCase1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of Cases with Job Transfer or Restriction:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblJobTranfer1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of Other Recordable Cases:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblRecordableCases1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Hours Worked By All Employees:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblTotalHours1" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--G. Vera 06/20/2014: added--%>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">OSHA 300 & 300 A log:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:ImageButton ID="imbAttachOSHA1" OnClick="imbAttach_Click" runat="server"
                                                                                                ImageUrl="~/Images/paperclip_gray.png" Enabled="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="auto-style1">I do not have supporting OSHA 300 & 300 A documentation
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:CheckBox ID="chkNoAttachment1" runat="server" CssClass="chkbox" Enabled="false">
                                                                                            </asp:CheckBox>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="" colspan="4" style="border-bottom: 1px solid #cccccc">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Year:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblOshaYear2" runat="server"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            N/A:
                                                                                        </td>
                                                                                        <td class="bodytextleftViewv">
                                                                                            <asp:CheckBox ID="chkOSHANA2" runat="server" Enabled="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Please Explain For N/A:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblExplainOSHA2" runat="server" CssClass="txtbox MultiLabel"> </asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of fatalities:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblOshafatalities2" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of Cases with Days Away from Work:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblOshaCase2" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of Cases with Job Transfer or Restriction:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblJobTranfer2" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of Other Recordable Cases:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblRecordableCases2" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Hours Worked By All Employees:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblTotalHours2" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--G. Vera 06/20/2014: added--%>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">OSHA 300 & 300 A log:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:ImageButton ID="imbAttachOSHA2" OnClick="imbAttach_Click" runat="server"
                                                                                                ImageUrl="~/Images/paperclip_gray.png" Enabled="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="auto-style1">I do not have supporting OSHA 300 & 300 A documentation
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:CheckBox ID="chkNoAttachment2" runat="server" CssClass="chkbox" Enabled="false"></asp:CheckBox>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="" colspan="4" style="border-bottom: 1px solid #cccccc">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Year:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblOshaYear3" runat="server"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            N/A:
                                                                                        </td>
                                                                                        <td class="bodytextleftViewv">
                                                                                            <asp:CheckBox ID="chkNAOSHA3" runat="server" Enabled="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Please Explain For N/A:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblExplainOSHA3" runat="server" CssClass="txtbox MultiLabel"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of fatalities:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblOshafatalities3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of Cases with Days Away from Work:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblOshaCase3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of Cases with Job Transfer or Restriction:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblJobTranfer3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Number of Other Recordable Cases:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblRecordableCases3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Total Hours Worked By All Employees:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblTotalHours3" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--G. Vera 06/20/2014: added--%>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">OSHA 300 & 300 A log:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:ImageButton ID="imbAttachOSHA3" OnClick="imbAttach_Click" runat="server"
                                                                                                ImageUrl="~/Images/paperclip_gray.png" Enabled="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="auto-style1">I do not have supporting OSHA 300 & 300 A documentation
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:CheckBox ID="chkNoAttachment3" runat="server" CssClass="chkbox" Enabled="false"></asp:CheckBox>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="" colspan="4" style="border-bottom: 1px solid #cccccc">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>

                                                                                    <%--G. Vera 06/11/2014: OSHA Year 4 if first is NA--%>
                                                                                    <tr id="trOSHAYear4" runat="server" style="display: none">
                                                                                        <td colspan="4">
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td class="auto-style1">Year:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:Label ID="lblOshaYear4" runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="auto-style1">N/A:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftViewv">
                                                                                                        <asp:CheckBox ID="chkNAOSHA4" runat="server" Enabled="false" />
                                                                                                    </td>
                                                                                                    
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="auto-style1">Please Explain For N/A:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:Label ID="lblExplainOSHA4" runat="server" CssClass="txtbox MultiLabel"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="auto-style1">Total Number of fatalities:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:Label ID="lblOshafatalities4" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="auto-style1">Total Number of Cases with Days Away from Work:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:Label ID="lblOshaCase4" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="auto-style1">Total Number of Cases with Job Transfer or Restriction:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:Label ID="lblJobTranfer4" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="auto-style1">Total Number of Other Recordable Cases:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:Label ID="lblRecordableCases4" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="auto-style1">Total Hours Worked By All Employees:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:Label ID="lblTotalHours4" runat="server" CssClass="txtbox"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                </tr>
                                                                                                <%--G. Vera 06/20/2014: added--%>
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv">OSHA Document:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:ImageButton ID="imbAttachOSHA4" OnClick="imbAttach_Click" runat="server"
                                                                                                            ImageUrl="~/Images/paperclip_gray.png" Enabled="false" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="auto-style1">I do not have supporting OSHA 300 & 300 A documentation
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:CheckBox ID="chkNoAttachment4" runat="server" CssClass="chkbox" Enabled="false"></asp:CheckBox>
                                                                                                    </td>

                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="" colspan="4" style="border-bottom: 1px solid #cccccc">&nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--End of Change--%>

                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            SIC or NAICS Code (From OSHA 300 Form):
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblSIC" runat="server"></asp:Label>&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            N/A:
                                                                                        </td>
                                                                                        <td class="bodytextleftViewv">
                                                                                            <asp:CheckBox ID="chkSICNA" runat="server" Enabled="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Please Explain For N/A:
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblSICExplain" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdheight">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="searchtableclass">
                                                                        <tr>
                                                                            <td class="searchhdrbarbold" colspan="2">
                                                                                Questionnaire
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                &nbsp
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="padding-left: 5px; padding-right: 10px">
                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv" width="36%">
                                                                                            Does your company have a written Safety Program?
                                                                                        </td>
                                                                                        <td class="bodytextleftView" width="64%">
                                                                                            <asp:Label ID="lblSafetyProgram" runat="server" CssClass="txtbox"> </asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            &nbsp
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Are all employees trained in safety requirements?
                                                                                        </td>
                                                                                        <td class="bodytextleftView">
                                                                                            <asp:Label ID="lblSafetytraining" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trProvideDoc" runat="server" style="display: none">
                                                                                        <td colspan="2">
                                                                                            <span>
                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" width="36%">If yes, can you provide documentation?
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="64%">
                                                                                                            <asp:Label ID="lblProvideDoc" runat="server" CssClass="txtbox">&nbsp;</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            &nbsp
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">
                                                                                            Do you have a Company Safety Director or other Safety Professionals on Staff?
                                                                                        </td>
                                                                                        <td class="bodytextleftView" valign="bottom">
                                                                                            <asp:Label ID="lblSafetyDirector" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="spnContact" runat="server" style="display: none">
                                                                                        <td colspan="2">
                                                                                            <span>
                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                    
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" width="36%">
                                                                                                            Please Specify Contact Name:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="64%">
                                                                                                            <asp:Label ID="lblContactName" runat="server" CssClass="txtbox">&nbsp;</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" width="36%">
                                                                                                            Contact Number:
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="64%">
                                                                                                            <asp:Label ID="lblMainPhno1" runat="server" CssClass="txtboxsmall">&nbsp;</asp:Label>
                                                                                                            <%--      <asp:Label ID="lblMainPhno2" runat="server" CssClass="txtboxsmall" MaxLength="3"></asp:Label>
                                                                                           
                                                                                            
                                                                                            <asp:Label ID="lblMainPhno3" runat="server" CssClass="txtboxsmall" MaxLength="4"></asp:Label>--%>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <%--G. Vera 12/20/2016: START--%>
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" width="36%">Please provide the level of certification and training
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="64%">
                                                                                                            <asp:Label ID="lblLevelOfTraining" runat="server" CssClass="txtbox MultiLabel-711PX" >&nbsp;</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <%--G. Vera 12/20/2016: END--%>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    
                                                                                       <%--Sooraj 10/25/2013 - New Question added--%>
                                                                                      <tr id="trNonUSSafetyQuestion" runat="server" style="display: none">
                                                                                   
                                                                                    <td class="bodytextbold_rightv" >
                                                                                      Are you able to provide onsite qualified safety representation? 
                                                                                    </td>
                                                                                    <td class="bodytextleftView" valign="bottom">
                                                                                        <asp:Label ID="lblRepresentation"  runat="server" RepeatDirection="Horizontal"  >                                                                                        
                                                                                        </asp:Label>
                                                                                     
                                                                                    </td>
                                                                                </tr>
                                                                                   <tr>
                                                                                        <td colspan="2">
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                  <tr>
                                                                                    <td colspan="2" id="spnContactNonUS" runat="server" style="display: none">
                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                   
                                                                                                    <td class="bodytextbold_rightv" width="36%">
                                                                                                        If Yes, Please Specify Contact Name:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" width="64%">
                                                                                                        <asp:Label ID="lblRepresentationContactName" runat="server" RepeatDirection="Horizontal">&nbsp;</asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                   
                                                                                                    <td class="bodytextbold_rightv">
                                                                                                        Contact Phone Number:
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView">
                                                                                                        <asp:Label ID="lblRepresentationContactNumber" runat="server" RepeatDirection="Horizontal">&nbsp;</asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                       
                                                                                    </td>
                                                                                </tr>
                                                                                    <%--G. Vera 12/20/2016: START--%>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">Has your firm been cited by an Occupational Safety & Health or Environmental Enforcement Agency for the following years?
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv" width="36%">Any citations for year <%=this.CitationYear1 %>?
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" width="32%">
                                                                                                        <asp:Label ID="lblCitation1" runat="server" />
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="32%">&nbsp;&nbsp;
                                                                                                        <asp:ImageButton ID="imbAttachCITATION1" OnClick="imbAttach_Click" runat="server"
                                                                                                            ImageUrl="~/Images/paperclip_gray.png" Enabled="false" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv" width="36%">Any citations for year <%=this.CitationYear2 %>?
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" width="32%">
                                                                                                        <asp:Label ID="lblCitation2" runat="server" />
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="32%">&nbsp;&nbsp;
                                                                                                        <asp:ImageButton ID="imbAttachCITATION2" OnClick="imbAttach_Click" runat="server"
                                                                                                            ImageUrl="~/Images/paperclip_gray.png" Enabled="false" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="bodytextbold_rightv" width="36%">Any citations for year <%=this.CitationYear3 %>?
                                                                                                    </td>
                                                                                                    <td class="bodytextleftView" width="32%">
                                                                                                        <asp:Label ID="lblCitation3" runat="server" />
                                                                                                    </td>
                                                                                                    <td class="bodytextleft" width="32%">&nbsp;&nbsp;
                                                                                                        <asp:ImageButton ID="imbAttachCITATION3" OnClick="imbAttach_Click" runat="server"
                                                                                                            ImageUrl="~/Images/paperclip_gray.png" Enabled="false" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--<tr>
                                                                                        <td class="bodytextbold_rightv">Do you have a new employee training program?
                                                                                        </td>
                                                                                        <td class="bodytextleftView" valign="bottom">
                                                                                            <asp:Label ID="lblEmployeeProgram" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>--%>
                                                                                    
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">Do you have a written drug and alcohol program?
                                                                                        </td>
                                                                                        <td class="bodytextleftView" valign="bottom">
                                                                                            <asp:Label ID="lblDrugAlcoholProgram" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">Do you hold periodic safety meetings for your employees?
                                                                                        </td>
                                                                                        <td class="bodytextleftView" valign="bottom">
                                                                                            <asp:Label ID="lblSafetyMeetings" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSafetyMeetings" runat="server" style="display: none">
                                                                                        <td colspan="2">
                                                                                            <span>
                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" width="36%">If Yes, how often?
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="64%">
                                                                                                            <asp:Label ID="lblSafetyMeetingsPeriods" runat="server" CssClass="txtbox">&nbsp;</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">Do you conduct field safety inspection of work in progress?
                                                                                        </td>
                                                                                        <td class="bodytextleftView" valign="bottom">
                                                                                            <asp:Label ID="lblInspectionWIP" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trInspectionWIP" runat="server" style="display: none">
                                                                                        <td colspan="2">
                                                                                            <span>
                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" width="36%">If Yes, who conducts the inspection?
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="64%">
                                                                                                            <asp:Label ID="lblInspectionWIPWho" runat="server" CssClass="txtbox">&nbsp;</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" width="36%">How often?
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="64%">
                                                                                                            <asp:Label ID="lblInspectionWIPPeriods" runat="server" CssClass="txtbox">&nbsp;</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="bodytextbold_rightv">Do you conduct job hazard analysis or task hazard analysis?
                                                                                        </td>
                                                                                        <td class="bodytextleftView" valign="bottom">
                                                                                            <asp:Label ID="lblHazardAnalysis" runat="server" CssClass="txtbox"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trHazardAnalysis" runat="server" style="display: none">
                                                                                        <td colspan="2">
                                                                                            <span>
                                                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td class="bodytextbold_rightv" width="36%">If yes, How often?
                                                                                                        </td>
                                                                                                        <td class="bodytextleftView" width="64%">
                                                                                                            <asp:Label ID="lblHazardAnalysisPreiods" runat="server" CssClass="txtbox">&nbsp;</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    

                                                                                    <%--G. Vera 12/20/2016: END--%>

                                                                                    <%--G. Vera 12/14/2016: Add note at bottom of the questionnaire section--%>
                                                                                    <tr>
                                                                                        <td colspan="8" class="bodytextboldBlue">Note:  Subcontractors are responsible for the safety management of tiered subcontractors under their control.
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                &nbsp
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdheight">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bodytextcenter">
                                                    <span class="hide_print">
                                                        <img src="../Images/print.jpg" id="imgprint" runat="server" title="Print" class="CursorStyle"
                                                            onclick="JavaScript:printPartOfPage();" visible="false" />
                                                        <asp:ImageButton ID="imgPDF" runat="server" ImageUrl="~/Images/ex2pdf.jpg" OnClick="imgPDF_Click" /><asp:HiddenField
                                                            ID="hdVendorId" runat="server" />
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <Ajax:ModalPopupExtender ID="modalExtnd" runat="server" TargetControlID="lblHidden"
                                            PopupControlID="modalPopupDiv" BackgroundCssClass="popupbg" RepositionMode="RepositionOnWindowResize">
                                        </Ajax:ModalPopupExtender>
                                        <asp:HiddenField ID="lblHidden" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="modalPopupDiv" class="popupdivcl" style="display: none">
                                            <table cellpadding="5" cellspacing="0" border="0" width="550">
                                                <tr>
                                                    <td class="searchhdrbarbold" runat="server" id="msgTd">
                                                        Result
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:ValidationSummary CssClass="summaryerrormsg" DisplayMode="List" runat="server"
                                                            ID="valsSafety" />
                                                        <asp:Label ID="lblMsg" runat="server" CssClass="errormsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ModalPopupTd">
                                                        <input type="button" title="OK" value="OK" class="ModalPopupButton" onclick="$find('modalExtnd').hide();" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdheight">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnSafetyId" runat="server" />
    </form>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-41184898-1', 'haskell.com');
        ga('send', 'pageview');

    </script>
</body>
</html>

<script language="javascript" type="text/javascript">
    function printPartOfPage() {
        var printWindow = window.open('../Admin/VQFView.aspx?act=print&user=<%=Request.QueryString["user"] %>', '', 'left=50000,top=50000,width=0,height=0');
        printWindow.focus();
        printWindow.print();
    }
    function printPartOfPageFromMatch() {
        var printWindow = window.open('../Admin/VQFView.aspx?act=print&user=<%=Request.QueryString["user"] %>&Match=match', '', 'left=50000,top=50000,width=0,height=0');
        printWindow.focus();
        printWindow.print();
    }
</script>

