﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web;
using VMSDAL;
using System.Web.UI.WebControls;
using System.Configuration;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 10/23/2012: VMS Stabilization Release 1.3 (see JIRA issues for details)
 * 003 G. Vera 01/29/2013: Hot Fix 1.3.1 (JIRA:VPI-90)
 * 004 Sooraj Sudhakaran.T 10/25/2013: VMS Modification - Added new question for Non US vendors 
 * 005 G. Vera 06/11/2014: New OSHA Year four if checked NA on first year AND 06/26/2014: Bug fix on EMR Rates year
 * 006 G. Vera 06/20/2014: Safety document attachments availability
 * 007 G. Vera 12/20/2016: Additional questions
 * 008 G. Vera 03/16/2017: Change May 1st cut off to March 1st
 ****************************************************************************************************************/
#endregion

public partial class VQFView_SafetyView : System.Web.UI.Page
{
    #region Declaration

    Common objCommon = new Common();
    clsSafety objSafety = new clsSafety();
    long VendorId;
    public string CitationYear1 {
        get { return ViewState["CitationYear1"] != null ? (ViewState["CitationYear1"] as string) : null; }
        set { ViewState["CitationYear1"] = value; }
    }
    public string CitationYear2 {
        get { return ViewState["CitationYear2"] != null ? (ViewState["CitationYear2"] as string) : null; }
        set { ViewState["CitationYear2"] = value; }
    }
    public string CitationYear3 {
        get { return ViewState["CitationYear3"] != null ? (ViewState["CitationYear3"] as string) : null; }
        set { ViewState["CitationYear3"] = value; }
    }

    #endregion

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        bool showOSHA4 = false;     //005
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (!IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            if (!string.IsNullOrEmpty(Convert.ToString(Session["match"])))
            {
                imgprint.Attributes.Add("onClick", "printPartOfPageFromMatch()");
            }
            else
            {
                imgprint.Attributes.Add("onClick", "printPartOfPage()");
            }
            Common objCommon = new Common();

            // included by sgs for 20 & 26
            // SOC - 29/12/2010
            int currentYear = 0;
            int oshaCurrentYear = 0;
            var IsNonUsVendor = false;
            var IsOnsiteSupport = false;    //G. Vera 05/16/2014: added

            //Included the above line for task no : 20
            // G. Vera Phase 3 Modifications Item 1.3
            //DateTime today = DateTime.Now.AddMonths(1);
            DateTime today = DateTime.Now;

            if (today >= Convert.ToDateTime("01/01/" + Convert.ToString(DateTime.Now.Year)))    //008
            {
                //currentYear = DateTime.Now.Year;
                //lblYear1.Text = Convert.ToString(DateTime.Now.Year);
                //lblYear2.Text = Convert.ToString(DateTime.Now.Year - 1);
                //lblYear3.Text = Convert.ToString(DateTime.Now.Year - 2);

                //oshaCurrentYear = DateTime.Now.Year;
                //lblOshaYear1.Text = Convert.ToString(DateTime.Now.Year);
                //lblOshaYear2.Text = Convert.ToString(DateTime.Now.Year - 1);
                //lblOshaYear3.Text = Convert.ToString(DateTime.Now.Year - 2);

                // G. Vera Phase 3 Modifications Item 1.3
                currentYear = DateTime.Now.Year;
                lblYear1.Text = Convert.ToString(DateTime.Now.Year);
                lblYear2.Text = Convert.ToString(DateTime.Now.Year - 1);
                lblYear3.Text = Convert.ToString(DateTime.Now.Year - 2);
                lblYear4.Text = Convert.ToString(DateTime.Now.Year - 3);    //003

                oshaCurrentYear = DateTime.Now.Year - 1; // G. Vera Phase 3 Modifications Item 1.3
                lblOshaYear1.Text = Convert.ToString(DateTime.Now.Year -1);
                lblOshaYear2.Text = Convert.ToString(DateTime.Now.Year - 2);
                lblOshaYear3.Text = Convert.ToString(DateTime.Now.Year - 3);
                lblOshaYear4.Text = Convert.ToString(DateTime.Now.Year - 4);    //005
            }
            else
            {
                currentYear = DateTime.Now.Year - 1;
                lblYear1.Text = Convert.ToString(DateTime.Now.Year - 1);
                lblYear2.Text = Convert.ToString(DateTime.Now.Year - 2);
                lblYear3.Text = Convert.ToString(DateTime.Now.Year - 3);
                lblYear4.Text = Convert.ToString(DateTime.Now.Year - 4);    //003

                oshaCurrentYear = DateTime.Now.Year - 2;    // G. Vera Phase 3 Modifications Item 1.3
                // G. Vera Phase 3 Modifications Item 1.3
                lblOshaYear1.Text = Convert.ToString(DateTime.Now.Year - 2);
                lblOshaYear2.Text = Convert.ToString(DateTime.Now.Year - 3);
                lblOshaYear3.Text = Convert.ToString(DateTime.Now.Year - 4);
                lblOshaYear4.Text = Convert.ToString(DateTime.Now.Year - 5);    //005
            }
            // EOC
            if (Request.QueryString.Count > 0)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Print"]))
                {
                    VQFViewMenu.Visible = false;
                }
            }

            if ((string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])) || Convert.ToString(Session["VendorAdminId"]) == "0"))
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
            }
            else
            {
                VendorId = Convert.ToInt64(Session["VendorAdminId"]);
                hdVendorId.Value = Convert.ToInt64(VendorId).ToString();
            }
            // Get company details
            if (!string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])))
            {
                clsRegistration objregistration = new clsRegistration();
                clsCompany objCompany = new clsCompany();

                DAL.UspGetVendorDetailsByIdResult[] objResult = objregistration.GetVendor(Convert.ToInt64((Session["VendorAdminId"])));
                if (objResult.Count() > 0)
                {
                    lblVendorName.Text = objResult[0].Company;
                    IsNonUsVendor = !(objResult[0].IsBasedInUS);  //Non US //005
                    var company = objCompany.GetSingleVQFCompanyData(VendorId);
                    if (company != null)
                        IsOnsiteSupport = (company.TypeOfCompany == TypeOfCompany.EquipAndOnsiteSupport);
                }
            }
            int Count = objSafety.GetSafetyCount(VendorId);
            //G. Vera 05/16/2014: Changed
            if (IsOnsiteSupport)
            
            {
                trNonUSSafetyQuestion.Style["Display"] = "";
                spnContactNonUS.Style["Display"] = "";
            }
            else
            {
                trNonUSSafetyQuestion.Style["Display"] = "none";
                spnContactNonUS.Style["Display"] = "none";
            }
            if (Count > 0)
            {
                DAL.VQFSafety objSafetyData = objSafety.GetSingleVQLSafetyData(VendorId);
                lblSIC.Text = objSafetyData.SICNAICSCode;
                chkSICNA.Checked = objSafetyData.SICNA == true ? true : false;
                lblSICExplain.Text = objSafetyData.Explanation;
                bool? CompanySafety = objSafetyData.CompanySafety;
                lblSafetyDirector.Text = ReturnYesNo(CompanySafety);
                hdnSafetyId.Value = objSafetyData.PK_SafetyID.ToString();       //006

                if (CompanySafety == true)
                {
                    spnContact.Style["Display"] = "";
                    lblContactName.Text = objSafetyData.CompanySafetyContactName;
                    lblMainPhno1.Text = objSafetyData.CompanySafetyContactNumber.ToString();
                }

                //004 Starts 

                if (IsOnsiteSupport)
                {
                    trNonUSSafetyQuestion.Style["Display"] = "";
                    bool? SafetyRepresentation = objSafetyData.SafetyRepresentation;
                    lblRepresentation.Text = ReturnYesNo(SafetyRepresentation);
                    if (SafetyRepresentation == true)
                    {
                        spnContactNonUS.Style["Display"] = "";
                        lblRepresentationContactName.Text = objSafetyData.SafetyRepresentationContactName;
                        lblRepresentationContactNumber.Text = objSafetyData.SafetyRepresentationContactNumber.ToString();
                    }
                }
                //004 Ends 

                //007 - START
                VMSDAL.VQFSafety safety = objSafety.GetSingleVQFSafetyData(VendorId);

                lblLevelOfTraining.Text = safety.ProfessionalLevelOfTraining;
                lblProvideDoc.Text = (safety.IsProvideEmployeeTrainingDocumentation != null) ? ((bool)safety.IsProvideEmployeeTrainingDocumentation).ConvertToYesNo() : "";
                if (safety.SafetyRequirements == true) trProvideDoc.Attributes["style"] = "display: ";

                lblDrugAlcoholProgram.Text = (safety.IsWrittenDrugAlcoholProgram != null) ? ((bool)safety.IsWrittenDrugAlcoholProgram).ConvertToYesNo() : "";
                lblSafetyMeetings.Text = (safety.IsPeriodicSafetyMeetings != null) ? ((bool)safety.IsPeriodicSafetyMeetings).ConvertToYesNo() : "";
                lblSafetyMeetingsPeriods.Text = (safety.SafetyMeetingsPeriods != null) ? safety.SafetyMeetingsPeriods.Replace("|", ",").Replace("?", "") : "";
                if (lblSafetyMeetings.Text == "Yes") trSafetyMeetings.Attributes["style"] = "display: ";

                lblInspectionWIP.Text = (safety.IsSafetyInspectionOfWIP != null) ? ((bool)safety.IsSafetyInspectionOfWIP).ConvertToYesNo() : "";
                lblInspectionWIPWho.Text = (safety.WhoConductsInspectionOfWIP != null) ? safety.WhoConductsInspectionOfWIP : "";
                lblInspectionWIPPeriods.Text = (safety.InspectionOfWIPPeriods != null) ? safety.InspectionOfWIPPeriods.Replace("|", ",").Replace("?", "") : "";
                if (lblInspectionWIP.Text == "Yes") trInspectionWIP.Attributes["style"] = "display: ";

                lblHazardAnalysis.Text = (safety.IsConductHazardAnalysis != null) ? ((bool)safety.IsConductHazardAnalysis).ConvertToYesNo() : "";
                lblHazardAnalysisPreiods.Text = (safety.ConductHazardAnalysisPeriods != null) ? safety.ConductHazardAnalysisPeriods.Replace("|", ",").Replace("?", "") : "";
                if (lblHazardAnalysis.Text == "Yes") trHazardAnalysis.Attributes["style"] = "display: ";

                LoadCitations(safety);

                lblSafetyProgram.Text = ReturnYesNo(objSafetyData.SafetyProgram);
                lblSafetytraining.Text = ReturnYesNo(objSafetyData.SafetyRequirements);

                // SOC - 12/29/2010
                VMSDAL.VQFSafetyEMRRate objRates = objSafety.SelectSafetyEMRRates(Convert.ToInt64(objSafetyData.PK_SafetyID), lblYear1.Text);
                currentYear = CheckEMRRates(objRates, currentYear);
                lblYear1.Text = currentYear.ToString();
                objRates = objSafety.SelectSafetyEMRRates(Convert.ToInt64(objSafetyData.PK_SafetyID), lblYear1.Text);

                if (objRates != null)
                {
                    lblRate1.Text = objRates.EMRRate;
                    chkNA1.Checked = Convert.ToBoolean(objRates.NotAvailable);
                    lblExplain1.Text = objRates.Explanation;
                    //006
                    HandleAttachmentsButton((!chkNA1.Checked && !String.IsNullOrEmpty(objRates.FilePath)), imbAttachEMR1);

                }
                lblYear2.Text = (currentYear - 1).ToString();
                objRates = objSafety.SelectSafetyEMRRates(Convert.ToInt64(objSafetyData.PK_SafetyID), lblYear2.Text);
                if (objRates != null)
                {
                    lblRate2.Text = objRates.EMRRate;
                    chkNA2.Checked = Convert.ToBoolean(objRates.NotAvailable);
                    lblExplain2.Text = objRates.Explanation;
                    //006
                    HandleAttachmentsButton((!chkNA2.Checked && !String.IsNullOrEmpty(objRates.FilePath)), imbAttachEMR2);
                }
                lblYear3.Text = (currentYear - 2).ToString();
                objRates = objSafety.SelectSafetyEMRRates(Convert.ToInt64(objSafetyData.PK_SafetyID), lblYear3.Text);
                if (objRates != null)
                {
                    lblRate3.Text = objRates.EMRRate;
                    chkNA3.Checked = Convert.ToBoolean(objRates.NotAvailable);
                    lblExplain3.Text = objRates.Explanation;
                    //006
                    HandleAttachmentsButton((!chkNA3.Checked && !String.IsNullOrEmpty(objRates.FilePath)), imbAttachEMR3);
                }
                //003
                //005 - need to change year 4 as well
                lblYear4.Text = (currentYear - 3).ToString();   //005
                objRates = objSafety.SelectSafetyEMRRates(Convert.ToInt64(objSafetyData.PK_SafetyID), lblYear4.Text);
                if (objRates != null)
                {
                    lblRate4.Text = objRates.EMRRate;
                    chkNA4.Checked = Convert.ToBoolean(objRates.NotAvailable);
                    lblExplain4.Text = objRates.Explanation;
                    //006
                    HandleAttachmentsButton((!chkNA4.Checked && !String.IsNullOrEmpty(objRates.FilePath)), imbAttachEMR4);
                }

                VMSDAL.VQFSafetyOSHA objOsha = objSafety.SelectSafetyOsha(Convert.ToInt64(objSafetyData.PK_SafetyID), lblOshaYear1.Text);
                oshaCurrentYear = CheckOSHA(objOsha, oshaCurrentYear);
                lblOshaYear1.Text = oshaCurrentYear.ToString();
                objOsha = objSafety.SelectSafetyOsha(Convert.ToInt64(objSafetyData.PK_SafetyID), lblOshaYear1.Text);

                if (objOsha != null)
                {
                    chkOSHANA1.Checked = Convert.ToBoolean(objOsha.NotAvailable);
                    lblExplainOsha1.Text = objOsha.Explanation;
                    lblOshafatalities1.Text = objOsha.TotalFatalities;
                    lblOshaCase1.Text = objOsha.TotalNoDays;
                    lblRecordableCases1.Text = objOsha.TotalNoOtherRecord;
                    lblJobTranfer1.Text = objOsha.TotalNoJobTransfer;
                    lblTotalHours1.Text = objOsha.TotalHoursWorked;
                    //006
                    HandleAttachmentsButton((!chkOSHANA1.Checked && !String.IsNullOrEmpty(objOsha.FilePath)), imbAttachOSHA1);
                    chkNoAttachment1.Checked = !(bool)objOsha.HasOSHALog;
                }
                else
                {
                    showOSHA4 = true;   //005 need to show if 1st year is null
                }

                lblOshaYear2.Text = (oshaCurrentYear - 1).ToString();
                objOsha = objSafety.SelectSafetyOsha(Convert.ToInt64(objSafetyData.PK_SafetyID), lblOshaYear2.Text);
                if (objOsha != null)
                {
                    chkOSHANA2.Checked = Convert.ToBoolean(objOsha.NotAvailable);
                    lblExplainOSHA2.Text = objOsha.Explanation;
                    lblOshafatalities2.Text = objOsha.TotalFatalities;
                    lblOshaCase2.Text = objOsha.TotalNoDays;
                    lblRecordableCases2.Text = objOsha.TotalNoOtherRecord;
                    lblJobTranfer2.Text = objOsha.TotalNoJobTransfer;
                    lblTotalHours2.Text = objOsha.TotalHoursWorked;
                    //006
                    HandleAttachmentsButton((!chkOSHANA2.Checked && !String.IsNullOrEmpty(objOsha.FilePath)), imbAttachOSHA2);
                    chkNoAttachment2.Checked = !(bool)objOsha.HasOSHALog;
                }
                lblOshaYear3.Text = (oshaCurrentYear - 2).ToString();
                objOsha = objSafety.SelectSafetyOsha(Convert.ToInt64(objSafetyData.PK_SafetyID), lblOshaYear3.Text);
                if (objOsha != null)
                {
                    chkNAOSHA3.Checked = Convert.ToBoolean(objOsha.NotAvailable);
                    lblExplainOSHA3.Text = objOsha.Explanation;
                    lblOshafatalities3.Text = objOsha.TotalFatalities;
                    lblOshaCase3.Text = objOsha.TotalNoDays;
                    lblRecordableCases3.Text = objOsha.TotalNoOtherRecord;
                    lblJobTranfer3.Text = objOsha.TotalNoJobTransfer;
                    lblTotalHours3.Text = objOsha.TotalHoursWorked;
                    //006
                    HandleAttachmentsButton((!chkNAOSHA3.Checked && !String.IsNullOrEmpty(objOsha.FilePath)), imbAttachOSHA3);
                    chkNoAttachment3.Checked = !(bool)objOsha.HasOSHALog;
                }
                // EOC

                //005
                lblOshaYear4.Text = (oshaCurrentYear - 3).ToString();
                objOsha = objSafety.SelectSafetyOsha(Convert.ToInt64(objSafetyData.PK_SafetyID), lblOshaYear4.Text);
                if (objOsha != null)
                {
                    chkNAOSHA4.Checked = Convert.ToBoolean(objOsha.NotAvailable);
                    lblExplainOSHA4.Text = objOsha.Explanation;
                    lblOshafatalities4.Text = objOsha.TotalFatalities;
                    lblOshaCase4.Text = objOsha.TotalNoDays;
                    lblRecordableCases4.Text = objOsha.TotalNoOtherRecord;
                    lblJobTranfer4.Text = objOsha.TotalNoJobTransfer;
                    lblTotalHours4.Text = objOsha.TotalHoursWorked;
                    //006
                    HandleAttachmentsButton((!chkNAOSHA4.Checked && !String.IsNullOrEmpty(objOsha.FilePath)), imbAttachOSHA4);
                    chkNoAttachment4.Checked = !(bool)objOsha.HasOSHALog;
                }

                //EOC


            }
        }

        //003, 005 - added the OR
        if (chkNA1.Checked || string.IsNullOrEmpty(lblRate1.Text))
        {
            trEMRYear4.Attributes["style"] = @"display: """;
        }
        else
        {
            trEMRYear4.Attributes["style"] = @"display: none";
        }

        //005
        if (chkOSHANA1.Checked || showOSHA4)
        {
            trOSHAYear4.Attributes["style"] = @"display: """;
        }
        else
        {
            trOSHAYear4.Attributes["style"] = @"display: none";
        }
    }

    private void LoadCitations(VQFSafety safety)
    {
        //Citations
        VMSDAL.VQFSafetyCitation citation1 = objSafety.SelectSafetyCitation(safety.PK_SafetyID, DateTime.Now.Year.ToString());
        VMSDAL.VQFSafetyCitation citation2 = objSafety.SelectSafetyCitation(safety.PK_SafetyID, (DateTime.Now.Year - 1).ToString());
        VMSDAL.VQFSafetyCitation citation3 = objSafety.SelectSafetyCitation(safety.PK_SafetyID, (DateTime.Now.Year - 2).ToString());

        CitationYear1 = DateTime.Now.Year.ToString();
        CitationYear2 = (DateTime.Now.Year - 1).ToString();
        CitationYear3 = (DateTime.Now.Year - 2).ToString();

        //current year has not been answered
        if (citation1.IsCitation == null)
        {
            citation1 = objSafety.SelectSafetyCitation(safety.PK_SafetyID, (DateTime.Now.Year - 1).ToString());
            citation2 = objSafety.SelectSafetyCitation(safety.PK_SafetyID, (DateTime.Now.Year - 2).ToString());
            citation3 = objSafety.SelectSafetyCitation(safety.PK_SafetyID, (DateTime.Now.Year - 3).ToString());
            CitationYear1 = (DateTime.Now.Year - 1).ToString();
            CitationYear2 = (DateTime.Now.Year - 2).ToString();
            CitationYear3 = (DateTime.Now.Year - 3).ToString();

        }

        lblCitation1.Text = (citation1.IsCitation != null) ? ((bool)citation1.IsCitation).ConvertToYesNo() : "";
        lblCitation2.Text = (citation2.IsCitation != null) ? ((bool)citation2.IsCitation).ConvertToYesNo() : "";
        lblCitation3.Text = (citation3.IsCitation != null) ? ((bool)citation3.IsCitation).ConvertToYesNo() : "";
        imbAttachCITATION1.Enabled = (!string.IsNullOrEmpty(citation1.CitationFilePath));
        imbAttachCITATION2.Enabled = (!string.IsNullOrEmpty(citation2.CitationFilePath));
        imbAttachCITATION3.Enabled = (!string.IsNullOrEmpty(citation3.CitationFilePath));

        imbAttachCITATION1.ImageUrl = (!string.IsNullOrEmpty(citation1.CitationFilePath)) ? "~/Images/paperclip.png" : "~/Images/paperclip_gray.png";
        imbAttachCITATION2.ImageUrl = (!string.IsNullOrEmpty(citation2.CitationFilePath)) ? "~/Images/paperclip.png" : "~/Images/paperclip_gray.png";
        imbAttachCITATION3.ImageUrl = (!string.IsNullOrEmpty(citation3.CitationFilePath)) ? "~/Images/paperclip.png" : "~/Images/paperclip_gray.png";
        //007 - END
    }


    /// <summary>
    /// 006 - added
    /// </summary>
    /// <param name="isActive"></param>
    /// <param name="imageButton"></param>
    private void HandleAttachmentsButton(bool isActive, ImageButton imageButton)
    {
        if (isActive)
        {
            imageButton.Enabled = true;
            imageButton.ImageUrl = "~/Images/paperclip.png";
        }
        else
        {
            imageButton.Enabled = false;
            imageButton.ImageUrl = "~/Images/paperclip_gray.png";
        }
    }

    #endregion

    #region Button Events

    protected void imgPDF_Click(object sender, ImageClickEventArgs e)
    {
        //002
        //Common objCommon = new Common();
        string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/VQF/PrintVQFForm.aspx?vendorid=" + hdVendorId.Value + "&print=1";
        string baseURI = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;

        //For testing purposes
        //var siteUrl = ConfigurationManager.AppSettings["SiteUrl"] + "/VQF/PrintVQFForm.aspx?vendorid=" + hdVendorId.Value + "&print=1";
        //string url = string.Format("{0}/VQF/PrintVQFForm.aspx?vendorid={1}&print=1", siteUrl, hdVendorId.Value);
        //string baseURI = string.Format("{0}", siteUrl);
        //this.Response.Redirect(siteUrl);

        objCommon.Print2PDF(url, baseURI);
        //objCommon.GeneratePDF(Convert.ToInt64(hdVendorId.Value), Convert.ToString(Request.QueryString["user"]));
    }

    #endregion

    private int CheckEMRRates(object rates, int currentYear)
    {
        int year = currentYear;
        VMSDAL.VQFSafetyEMRRate objRates = (VMSDAL.VQFSafetyEMRRate)rates;
        if (objRates == null)
        {
            year = currentYear - 1;
        }
        else
        {
            if (Convert.ToBoolean(objRates.NotAvailable))
            {
                if (string.IsNullOrEmpty(objRates.Explanation))
                {
                    year = currentYear - 1;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(objRates.EMRRate))
                {
                    year = currentYear - 1;
                }
            }
        }
        return year;
    }

    private int CheckOSHA(object osha, int oshaCurrentYear)
    {
        int year = oshaCurrentYear;
        VMSDAL.VQFSafetyOSHA objOsha = (VMSDAL.VQFSafetyOSHA)osha;
        if (objOsha == null)
        {
            year = oshaCurrentYear - 1;
        }
        else
        {
            if (Convert.ToBoolean(objOsha.NotAvailable))
            {
                if (string.IsNullOrEmpty(Convert.ToString(objOsha.Explanation)))
                {
                    year = oshaCurrentYear - 1;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(Convert.ToString(objOsha.TotalFatalities))
                                && string.IsNullOrEmpty(Convert.ToString(objOsha.TotalNoDays))
                                && string.IsNullOrEmpty(Convert.ToString(objOsha.TotalNoOtherRecord))
                                && string.IsNullOrEmpty(Convert.ToString(objOsha.TotalHoursWorked))
                                && string.IsNullOrEmpty(Convert.ToString(objOsha.TotalNoJobTransfer)))
                {
                    year = oshaCurrentYear - 1;
                }
            }
        }
        return year;
    }


    /// <summary>
    /// 006 will load the attachment user control
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAttach_Click(object sender, EventArgs e)
    {
        string id = (sender as ImageButton).ID.Replace("imbAttach", "");
        long safetyId = Convert.ToInt64(hdnSafetyId.Value);
        string filePath = ConfigurationManager.AppSettings["SafetyDocuments"] + "//";
        string fileName = string.Empty;

        switch (id)
        { 
            case "EMR1":
                var emr = objSafety.SelectSafetyEMRRates(safetyId, lblYear1.Text);
                fileName = emr.FilePath;
                break;
            case "EMR2":
                var emr2 = objSafety.SelectSafetyEMRRates(safetyId, lblYear2.Text);
                fileName = emr2.FilePath;
                break;
            case "EMR3":
                var emr3 = objSafety.SelectSafetyEMRRates(safetyId, lblYear3.Text);
                fileName = emr3.FilePath;
                break;
            case "EMR4":
                var emr4 = objSafety.SelectSafetyEMRRates(safetyId, lblYear4.Text);
                fileName = emr4.FilePath;
                break;
            case "OSHA1":
                var osha = objSafety.SelectSafetyOsha(safetyId, lblOshaYear1.Text);
                fileName = osha.FilePath;
                break;
            case "OSHA2":
                var osha2 = objSafety.SelectSafetyOsha(safetyId, lblOshaYear2.Text);
                fileName = osha2.FilePath;
                break;
            case "OSHA3":
                var osha3 = objSafety.SelectSafetyOsha(safetyId, lblOshaYear3.Text);
                fileName = osha3.FilePath;
                break;
            case "OSHA4":
                var osha4 = objSafety.SelectSafetyOsha(safetyId, lblOshaYear4.Text);
                fileName = osha4.FilePath;
                break;
            //007 - START
            case "CITATION1":
                var cit1 = objSafety.SelectSafetyCitation(safetyId, CitationYear1);
                fileName = cit1.CitationFilePath;
                break;
            case "CITATION2":
                var cit2 = objSafety.SelectSafetyCitation(safetyId, CitationYear2);
                fileName = cit2.CitationFilePath;
                break;
            case "CITATION3":
                var cit3 = objSafety.SelectSafetyCitation(safetyId, CitationYear3);
                fileName = cit3.CitationFilePath;
                break;
            //007 - END
            default:
                break;
        }

        if(!string.IsNullOrEmpty(fileName))
            Response.Redirect("../Common/GenerateFile.aspx?region=" + filePath + fileName);


    }

    private string ReturnYesNo(object value)
    {
        switch (Convert.ToString(value))
        {
            case "True":
            case "true":
                return "Yes";
            case "False":
            case "false":
                return "No";
            default:
                return "";
        }
    }
}
