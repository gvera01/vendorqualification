﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Errorpage.aspx.cs" Inherits="Errorpage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Haskell</title>
    <link href="css/Haskell.css" rel="stylesheet" type="text/css" />
    <style type="text/css">

body 
{
	font-family: Arial;
	font-size: 11px;
	color: #333333;
	background: url(images/body_bg.png) repeat-x;
	background-color: #2672b8;
	margin-top:0px;
}



</style>
</head>
<body>
    <form id="form1" runat="server" >
    <asp:ScriptManager ID="scriptMgr" runat="server">
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr>
            <td>
                &nbsp;
            </td>
            <td class="container">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" style="padding: 5px">
                          <img src="images/haskelllogo.jpg" alt="Haskell" /> 
                        </td>
                        <td width="50%" class="bodytextbold_right" >Version: &nbsp;<asp:Literal Text='<%$ appSettings:Version %>' runat="server" ID="ltrlVersion"></asp:Literal>&nbsp;
                          
                        </td>
                    </tr>
                </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td >                                                                          
                                    </td>
                                    <td class="contenttd">
                                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    
                                                    <tr>
                                                        <td class="tdheight">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                <table cellpadding="0" cellspacing="0" border="0">
           <tr>
              
               <td align="center"   class=" bodytextcenter errorcolor"> 
                   Server Problem !!!
               </td></tr>
           <tr>
              
               <td>&nbsp;</td></tr>
           <tr>
               
               <td class="errorcolorleft">
                   The server encountered an internal problem and was unable to complete your request.</td></tr>
           <tr>
              
                <td>&nbsp;</td></tr>
      <%--  <tr>
            
            <td class="errorcolorleft" >
                Please send an e-mail to <a href="mailto:ponnuraj@themajesticpeople.com">ponnuraj@themajesticpeople.com </a>and describe
                which URL(s) you were trying to access or what activity you were trying to complete
                on the site...</td>
        </tr>--%>
        <tr>
           
              <td>&nbsp;</td></tr>
        <tr>
            
            <td  class="errorcolorleft" >
                We'll try to correct the problem as soon as possible and we apologize for the inconvenience!
            </td>
        </tr>
           
           </table>
                                             
                                           
                                    </td></tr></table></td>  
                                </tr>                                                    
                             
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Haskell:Footer ID="BottomMenu" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
</body>
</html>