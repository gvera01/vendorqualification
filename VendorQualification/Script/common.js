﻿// *** Added By N Schoenberger on 11/15/2011 to default scrolling to top of page ***
function ResetScrollPosition() {
    setTimeout('window.scrollTo(0,0)', 0); 
}
// *** End Added By N Schoenberger on 11/15/2011 to default scrolling to top of page ***

//G. Vera 06/28/2012 - Added for future.
//window.onbeforeunload = windowOnBeforeUnload;
//function windowOnBeforeUnload() {
//    var hasSaved = document.getElementById("hasSaved").value;
//    if (hasSaved == 'true')
//        return;   // Let the page unload
//    if (window.event)
//        window.event.returnValue = 'Your changes will be lost if not saved.'; // IE
//    else
//        return 'Your changes will be lost if not saved'; // FX
//}

////G. Vera 06/28/2012 - Added
//function HasSaved(result) {
//    document.getElementById("hasSaved").value = result;
//}


function Comma(nStr, prefix, ctrl) {
    var number = nStr;
    
    number = '' + number;
    if (number.length > 3) {
        var mod = number.length % 3;
        var output = (mod > 0 ? (number.substring(0, mod)) : '');
        for (i = 0; i < Math.floor(number.length / 3); i++) {
            if ((mod == 0) && (i == 0))
                output += number.substring(mod + 3 * i, mod + 3 * i + 3);
            else
                output += ',' + number.substring(mod + 3 * i, mod + 3 * i + 3);
        }
        var txt = document.getElementById(ctrl);
        
        txt.value = output;
        return (output);
    }
    else return number;
}

function numberFormat(nStr, prefix, ctrl) {
    var txt = document.getElementById(ctrl);
    if (txt.value != "0") {

        var prefix = prefix || '';
        nStr += '';
        nStr = nStr.replace(/^[0]+/g, "");

        y = nStr.split(',');
        var y1 = '';
        for (var i = 0; i < y.length; i++) {
            y1 += y[i];

        }
        if (y.length > 0) {
            nStr = y1;
        }
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1))
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        var txt = document.getElementById(ctrl);

        txt.value = prefix + x1 + x2;
    }
}