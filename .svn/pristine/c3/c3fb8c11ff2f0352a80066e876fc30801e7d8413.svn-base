﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/*********************************************************************************************************
 * Change Comments
 * ================
 * 001 G. Vera 10/01/2012: Modification (JIRA:VPI-69 & VPI-71)
 * 002 G. Vera 09/21/2012: Modification (JIRA:VPI-69, VPI-71, & VPI-70)
 * 
 *********************************************************************************************************/

public partial class SRE_PrintSRE : System.Web.UI.Page
{

    #region Instantiate the class and declare varaibles

    DataSet SREDetails;
    private Int64 riskId = 0;
    RiskEvaluation riskEvaluation = new RiskEvaluation();
    public DataSet Lookup = new DataSet();

    #endregion

    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        //002
        LoadRiskManagmentChecklist();

        long RISKID = 0;
        int statusId = -1;
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["RiskID"])))
        {
           RISKID = Convert.ToInt64(Request.QueryString["RiskID"]);
           statusId = Convert.ToInt32(Request.QueryString["statusidprint"]);
        }
        else if (!string.IsNullOrEmpty(Convert.ToString(Session["riskId"])))
        {
            RISKID = Convert.ToInt64(Session["riskId"]);
            statusId = Convert.ToInt32(Session["statusidprint"]);
        }
        else
        {
            statusId = -1;
            RISKID = -1;
        }

        //G. Vera 09/05/2012 - Hot Fix on Lookup percentage value found after release of 1.2 version.
        // has been incorrect since delivery oif this product
        SREDetails = riskEvaluation.GetSelectedRiskDetail(RISKID);
        statusId = (SREDetails.Tables[0].Rows.Count > 0 ) ? Convert.ToInt32(SREDetails.Tables[0].Rows[0]["StatusId"]) : 0;  //001 - bug
        lblOriginator.Text = "Originated By: ";  //001 - added

        if (statusId == 2 || statusId == 4 || statusId == 5)
            Lookup = riskEvaluation.GetSRELookUpData(RISKID);
        else
        {
            Lookup = riskEvaluation.GetSRELookUpData(-1);
        }
        BusinessYear.InnerText = "Has the Subcontractor been in business less than " + Lookup.Tables[0].Rows[0]["BusinessYear"].ToString() + " years?";
        LargestMaxPercentage.InnerText = "Does Subcontractor's bid exceed their largest maximum contract value by " + Lookup.Tables[0].Rows[0]["LargestMaxPercentage"].ToString() + "%?";
        LargestMaxPastYear.InnerText = "Largest Max Contract (over past " + Lookup.Tables[0].Rows[0]["LargestMaxPastYear"].ToString() + " yrs):";
        AannualRevenuePastYear.InnerText = "Is Subcontractor's current total backlog > the " + Lookup.Tables[0].Rows[0]["AannualRevenuePastYear"].ToString() + " year average annual revenue?";
        AannualRevenuePastYear1.InnerText = Lookup.Tables[0].Rows[0]["AannualRevenuePastYear"].ToString() + " Year Average Annual Revenue:";
        BidAannualRevenuePercentage.InnerText = "Is Subcontractor's bid > " + Lookup.Tables[0].Rows[0]["BidAannualRevenuePercentage"].ToString() + "% of " + Lookup.Tables[0].Rows[0]["BidAannualRevenuePastYear"].ToString() + " year average annual revenue?";
        LowestbidPercentage.InnerText = "Is the Subcontractor's bid " + Lookup.Tables[0].Rows[0]["LowestbidPercentage"].ToString() + "% or more lower than next bidder?";
        EMRPastYear.InnerText = "Is EMR history in any of past " + Lookup.Tables[0].Rows[0]["EMRPastYear"].ToString() + " years greater than " + Lookup.Tables[0].Rows[0]["EMRValue"].ToString() + "?";

        if (SREDetails.Tables[0].Rows.Count > 0)    //001 - bug
        {
            LoadRiskDetails(SREDetails);
            if (SREDetails.Tables[0].Rows.Count > 0)
            {
                var vendor = riskEvaluation.GetVendorDetail(Convert.ToInt64(SREDetails.Tables[0].Rows[0]["FK_VendorID"].ToString()));
                txtVendorName.Text = vendor.Company;
               
                txtVendorContactName.Text = vendor.FirstName + " " + vendor.LastName;
                if (SREDetails.Tables[0].Rows[0]["SREModifiedDate"].ToString() == "")
                    txtDate.Text = DateTime.Now.ToString();
                else
                    txtDate.Text = SREDetails.Tables[0].Rows[0]["SREModifiedDate"].ToString();
                txtVendorContactName.Text = SREDetails.Tables[0].Rows[0]["contactName"].ToString();

                if (txtVendorContactName.Text == "")
                {
                    txtVendorContactName.Text = vendor.FirstName + " " + vendor.LastName;
                }

                //txtDate.Text = SREDetails.Tables[0].Rows[0]["SREModifiedDate"].ToString();
                txtProjectNumber.Text = SREDetails.Tables[0].Rows[0]["ProjectNumber"].ToString().TrimStart();
                txtVendorQualificationDate.Text = SREDetails.Tables[0].Rows[0]["VendorQualificationDate"].ToString();
                txtProjectName.Text = riskEvaluation.GetProjectName(SREDetails.Tables[0].Rows[0]["ProjectNumber"].ToString());
                txtAppSubContractorValue.Text = SREDetails.Tables[0].Rows[0]["AppSubcontractorValue"].ToString();

                txtMaxContract3Year.Text = SREDetails.Tables[0].Rows[0]["LargestMaxContract"].ToString();
                txtCurrentTotalBacklog.Text = SREDetails.Tables[0].Rows[0]["CurrentBacklog"].ToString();

                txtAverageAnnualRevenue.Text = SREDetails.Tables[0].Rows[0]["AverageAnnualRevenue"].ToString();
                txtAverageAnnualRevenue.Text = string.Format("{0:#,0}", Convert.ToDouble((txtAverageAnnualRevenue.Text).ToString()));
                txtAvailBondingCapacity.Text = SREDetails.Tables[0].Rows[0]["AvailableBondingCapacity"].ToString();
            }
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //sb.Append(@"<script language='javascript'>");
            //sb.Append(@"window.print();");

            //sb.Append(@"</script>");

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "JSCR", sb.ToString(), false);

        }
    }
    #endregion

    #region Methods

    /// <summary>
    /// Bind the SRE Details Method
    /// </summary>
    /// <param name="SREDetails"></param>
    private void LoadRiskDetails(DataSet SREDetails)
    {
        if (SREDetails.Tables[0].Rows.Count > 0)
        {

            DataRow dataRow = SREDetails.Tables[0].Rows[0];
            txtBidpackageNumber.Text = dataRow["BidPackageNumber"].ToString();
            txtBidpackageDesc.Text = dataRow["BidPackageDescription"].ToString();
            txtAppSubContractorValue.Text = dataRow["AppSubcontractorValue"].ToString();

            CheckYesNoRadio(dataRow["Business1"], rdoBusiness1Yes, rdoBusiness1No);
            CheckYesNoRadio(dataRow["Business2"], rdoBusiness2Yes, rdoBusiness2No);
            CheckYesNoRadio(dataRow["Business3"], rdoBusiness3Yes, rdoBusiness3No);
            CheckYesNoRadio(dataRow["Business4"], rdoBusiness4Yes, rdoBusiness4No);
            CheckRiskIndicator(dataRow["BusinessRiskIndicator"], chkBusinessRiskIndicator);

            txtNextLowestBid.Text = dataRow["NextLowestBid"].ToString();
            txtSubcontractorCurrentBid.Text = dataRow["SubcontractorCurrentBid"].ToString();

            CheckYesNoRadio(dataRow["Financial1"], rdoFinancial1Yes, rdoFinancial1No);
            CheckYesNoRadio(dataRow["Financial2"], rdoFinancial2Yes, rdoFinancial2No);
            CheckYesNoRadio(dataRow["Financial3"], rdoFinancial3Yes, rdoFinancial3No);
            CheckYesNoRadio(dataRow["Financial4"], rdoFinancial4Yes, rdoFinancial4No);
            CheckYesNoRadio(dataRow["Financial5"], rdoFinancial5Yes, rdoFinancial5No);
            CheckRiskIndicator(dataRow["FinancialRiskIndicator"], chkFinancialRiskIndicator);

            // ***** Modified/ Added by N Schoenberger 11/16/2011 *****
            //txtPMNotes.Text = dataRow["PMVerificationNotes"].ToString();
            string PMVerificationNotes = dataRow["PMVerificationNotes"].ToString();
            PMVerificationNotes = PMVerificationNotes.Replace("\r\n", "<BR/>");
            PMVerificationNotes = PMVerificationNotes.Replace(" ", " &nbsp;");
            lblPMVerificationNotes.InnerHtml = PMVerificationNotes;
            //***** End modification *****

            txtVerificationDate.Text = dataRow["PMVerificationDate"].ToString();
            CheckYesNoRadio(dataRow["License"], rdoLicense1Yes, rdoLicense1No);

            CheckYesNoRadio(dataRow["Insurance1"], rdoInsurance1Yes, rdoInsurance1No);
            CheckYesNoRadio(dataRow["Insurance2"], rdoInsurance2Yes, rdoInsurance2No);
            CheckYesNoRadio(dataRow["Insurance3"], rdoInsurance3Yes, rdoInsurance3No);
            CheckYesNoRadio(dataRow["Insurance4"], rdoInsurance4Yes, rdoInsurance4No);
            CheckRiskIndicator(dataRow["InsuranceRiskIndicator"], chkInsuranceRiskIndicator);

            CheckYesNoRadio(dataRow["Safety1"], rdoSafety1Yes, rdoSafety1No);
            CheckYesNoRadio(dataRow["Safety2"], rdoSafety2Yes, rdoSafety2No);
            CheckRiskIndicator(dataRow["SafetyRiskIndicator"], chkSafetyRiskIndicator);

            txtProjectReference1.Text = dataRow["ProjectReference1"].ToString();
            txtReferenceDate1.Text = dataRow["ProjectReference1DateCalled"].ToString();
            txtProjectReference2.Text = dataRow["ProjectReference2"].ToString();
            txtReferenceDate2.Text = dataRow["ProjectReference2DateCalled"].ToString();
            txtProjectReference3.Text = dataRow["ProjectReference3"].ToString();
            txtReferenceDate3.Text = dataRow["ProjectReference3DateCalled"].ToString();
            txtSupplierReference1.Text = dataRow["SupplierReference1"].ToString();
            txtSupplierDate1.Text = dataRow["SupplierReference1DateCalled"].ToString();
            txtSupplierReference2.Text = dataRow["SupplierReference2"].ToString();
            txtSupplierReference3.Text = dataRow["SupplierReference3"].ToString();
            txtSupplierDate2.Text = dataRow["SupplierReference2DateCalled"].ToString();
            txtSupplierDate3.Text = dataRow["SupplierReference3DateCalled"].ToString();
            CheckYesNoRadio(dataRow["Reference1"], rdoReference1Yes, rdoReference1No);
            CheckYesNoRadio(dataRow["Reference2"], rdoReference2Yes, rdoReference2No);
            CheckYesNoRadio(dataRow["Reference3"], rdoReference3Yes, rdoReference3No);
            //Added by N Schoenberger for "Not Applicable" functionality 10/20/2011
            CheckNotApplicable(dataRow["Reference3NA"], rdoReference3NA);

            CheckRiskIndicator(dataRow["ReferenceRiskIndicator"], chkReferenceRiskIndicator);
            txtProjectManagerName.Text = dataRow["ProjectManagerName"].ToString();


            //  DataRow EMRRate = SREDetails.Tables[2].Rows[0];

            if (SREDetails.Tables[2].Rows[0]["EMRRate"].ToString() == "")
            {
                txtRate1.Text = "N/A";
            }
            else 
            {
                txtRate1.Text = SREDetails.Tables[2].Rows[0]["EMRRate"].ToString();
            }
            if (SREDetails.Tables[2].Rows[1]["EMRRate"].ToString() == "")
            {
                txtRate2.Text = "N/A";
            }
            else
            {
                txtRate2.Text = SREDetails.Tables[2].Rows[1]["EMRRate"].ToString();
            }
            if (SREDetails.Tables[2].Rows[2]["EMRRate"].ToString() == "")
            {
                txtRate3.Text = "N/A";
            }
            else
            {
                txtRate3.Text = SREDetails.Tables[2].Rows[2]["EMRRate"].ToString();
            }

            // Modified by N Schoenberger 11/7/2011 [PM Risk/ DOC Risk Dates]
            //txtRiskDate1.Text = dataRow["SREDate"].ToString();
            txtRiskDate1.Text = string.Format("{0:M/d/yyyy}", dataRow["SubmittedDate"]);
            // ****************************************************************

            string BComments = dataRow["BusinessComments"].ToString();
            BComments = BComments.Replace("\r\n", "<BR/>");
            BComments = BComments.Replace(" ", " &nbsp;");
            lblBComments.InnerHtml = BComments;
            string FComments = dataRow["FinanceComments"].ToString();
            FComments = FComments.Replace("\r\n", "<BR/>");
            FComments = FComments.Replace(" ", " &nbsp;");
            lblFComments.InnerHtml = FComments;
            //SGS Changes on 08/11/2011
            string LComments = dataRow["LicenseComments"].ToString();
            LComments = LComments.Replace("\r\n", "<BR/>");
            LComments = LComments.Replace(" ", " &nbsp;");
            lblLComments.InnerHtml = LComments;
            ///
            string SComments = dataRow["SaftyComments"].ToString();
            SComments = SComments.Replace("\r\n", "<BR/>");
            SComments = SComments.Replace(" ", " &nbsp;");
            lblSComments.InnerHtml = SComments;
            //SGS Changes on 08/11/2011
            string IComments = dataRow["InsuranceComments"].ToString();
            IComments = IComments.Replace("\r\n", "<BR/>");
            IComments = IComments.Replace(" ", " &nbsp;");
            lblIComments.InnerHtml = IComments;
            //
            string RComments = dataRow["ReferenceComments"].ToString();
            RComments = RComments.Replace("\r\n", "<BR/>");
            RComments = RComments.Replace(" ", " &nbsp;");
            lblRComments.InnerHtml = RComments;
            //SGS Changes on 08/11/2011
            string RiskComments = dataRow["Comments"].ToString();
            RiskComments = RiskComments.Replace("\r\n", "<BR/>");
            RiskComments = RiskComments.Replace(" ", " &nbsp;");
            lblRiskComments.InnerHtml = RiskComments;
            txtRiskMgmntComments.InnerHtml =  dataRow["RiskManagementComments"].ToString();   //002
            //
            //001
            long createdBY = Convert.ToInt64(dataRow["CreatedBy"].ToString());
            lblOriginator.Text = "Originated By: " + new clsAdminLogin().GetEmployeeName(createdBY);



            if (!dataRow.IsNull("RiskFlag"))
            {
                switch (Convert.ToInt32(dataRow["RiskFlag"]))
                {
                    case 1:
                        chkAverageRisk.Checked = true;
                        txtProjectManagerName.Text = dataRow["ProjectManagerName"].ToString();
                        txtRiskDate1.Text = dataRow["SREDate"].ToString();
                        //txtProjectManagerName.Enabled = true;
                        //txtRiskDate1.Enabled = true;
                        break;
                    case 2:
                        chkHighRisk.Checked = true;
                        txtGroupName1.Text = dataRow["GroupDOCName"].ToString();
                        txtRiskDate2.Text = dataRow["SREDate"].ToString();
                        //txtGroupName1.Enabled = true;
                        //txtRiskDate2.Enabled = true;
                        break;
                    case 3:
                        chkDisapproved.Checked = true;
                        txtGroupName2.Text = dataRow["GroupDOCName"].ToString();
                        txtRiskDate3.Text = dataRow["SREDate"].ToString();
                        //txtGroupName1.Enabled = true;
                        //txtRiskDate3.Enabled = true;
                        break;
                }
            }
        }
    }

    /// <summary>
    /// Select the Yes/No Radiobuttons Method
    /// </summary>
    /// <param name="value"></param>
    /// <param name="rdoYes"></param>
    /// <param name="rdoNo"></param>
    private void CheckYesNoRadio(object value, RadioButton rdoYes, RadioButton rdoNo)
    {
        if (!value.Equals(DBNull.Value))
        {
            if (Convert.ToBoolean(value))
            {
                rdoYes.Checked = true;
            }
            else
            {
                rdoNo.Checked = true;
            }
        }
    }

    private void CheckNotApplicable(object value, RadioButton rdoNotApplicable)
    {
        if (!value.Equals(DBNull.Value))
        {
            if (Convert.ToBoolean(value))
                rdoNotApplicable.Checked = true;
            else
                rdoNotApplicable.Checked = false;
        }
    }
    /// <summary>
    /// Check the Risk indicator Checkbox Method
    /// </summary>
    /// <param name="value"></param>
    /// <param name="chkRiskIndicator"></param>
    private void CheckRiskIndicator(object value, CheckBox chkRiskIndicator)
    {
        if (!value.Equals(DBNull.Value))
        {
            chkRiskIndicator.Checked = Convert.ToBoolean(value);
        }
    }

    /// <summary>
    /// 002 - Implemented
    /// </summary>
    protected void LoadRiskManagmentChecklist()
    {

        RiskManagementChecklist mngmtChkList = new RiskManagementChecklist();
        RiskEnrollment objEnrollment = new RiskEnrollment();

        int i = 0;
        foreach (string item in mngmtChkList.AverageRiskSection)
        {
            chkListAverageRisk.Items.Add(item);
            chkListAverageRisk.Items[i].Attributes.Add("onclick", "Javascript:Uncheck(this)");
            i++;
        }

        i = 0;
        foreach (string item in mngmtChkList.HighRiskSection)
        {
            chkListHighRisk.Items.Add(item);
            chkListHighRisk.Items[i].Attributes.Add("onclick", "Javascript:Uncheck(this)");
            i++;
        }

        i = 0;
        foreach (string item in mngmtChkList.HighRiskDetermination)
        {
            chkListHighRiskDet.Items.Add(item);
            chkListHighRiskDet.Items[i].Attributes.Add("onclick", "Javascript:Uncheck(this)");
            //lblListHighRiskDet.InnerText = item;
            //chkListHighRiskDet.Attributes.Add("onclick", "Javascript:Uncheck(this)");
            //lblListHighRiskDet2.InnerText = item;
            //chkListHighRiskDet2.Attributes.Add("onclick", "Javascript:Uncheck(this)");
            //lblListHighRiskDet3.InnerText = item;
            //chkListHighRiskDet3.Attributes.Add("onclick", "Javascript:Uncheck(this)");
            //lblListHighRiskDet4.InnerText = item;
            //chkListHighRiskDet4.Attributes.Add("onclick", "Javascript:Uncheck(this)");
            i++;
        }

        i = 0;
        foreach (string item in objEnrollment.CheckList)
        {
            chklstEnrollment.Items.Add(item);
            chklstEnrollment.Items[i].Attributes.Add("onclick", "Javascript:Uncheck(this)");
            i++;
        }

    }

    #endregion
}
