﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web;
using VMSDAL;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 002 G. Vera 10/26/2012: VMS Stabilization Release 1.3 (see JIRA issues for more details)
 * 003 Sooraj Sudhakaran.T 10/08/2013: VMS Modification - Added question for International Currency
 * 004 G. Vera 06/11/2014: Year 4 in case year one is NA
 ****************************************************************************************************************/
#endregion

public partial class VQFView_LegalFinancialView : System.Web.UI.Page
{
    #region Declaration

    clsLegal objLegal = new clsLegal();
    Common objCommon = new Common();
    long Vendor = 0;
    static bool IsBasedInUs = true; //G. vera added as Sooraj left out

    #endregion

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }
        if (!Page.IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            if (Request.QueryString.Count > 0)
            {
                VQFViewMenu.Visible = !string.IsNullOrEmpty(Request.QueryString["Print"]) ? false : true;
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["match"])))
            {
                imgprint.Attributes.Add("onClick", "printPartOfPageFromMatch()");
            }
            else
            {
                imgprint.Attributes.Add("onClick", "printPartOfPage()");
            }
        }

        if ((string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])) || Convert.ToString(Session["VendorAdminId"]) == "0"))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");  //001
        }

        if (Convert.ToInt64(Session["VendorAdminId"]) > 0)
        {
            Vendor = Convert.ToInt64(Session["VendorAdminId"]);
            hdVendorId.Value = Convert.ToInt64(Vendor).ToString();
        }
        // get company details
        if (!string.IsNullOrEmpty(Convert.ToString(Session["VendorAdminId"])))
        {
            clsRegistration objregistration = new clsRegistration();
            DAL.UspGetVendorDetailsByIdResult[] objResult = objregistration.GetVendor(Convert.ToInt64((Session["VendorAdminId"])));
            if (objResult.Count() > 0)
            {
                lblVendorName.Text = objResult[0].Company;
                IsBasedInUs = objResult[0].IsBasedInUS; //G. vera added as Sooraj left out
            }

        }
        // get legal details
        DAL.VQFLegal objLegalData = objLegal.GetSingleVQLData(Convert.ToInt64(Session["VendorAdminId"]));
        if (objLegalData != null)
        {
            bool? ListOne = objLegalData.Legal1;
            if (ListOne == true)
            {
                lblListOne.Text = "Yes";
                trWorkAwdOne.Style["Display"] = "";
                txtWorkAwd.Text = objLegalData.ExplainLegal1;
            }
            else if (ListOne == false)
            {
                lblListOne.Text = "No";
            }
            else
            {
                lblListOne.Text = "";
            }

            bool? ListTwo = objLegalData.Legal2;
            if (ListTwo == true)
            {
                lblListTwo.Text = "Yes";
                trStock.Style["Display"] = "";
                txtStake.Text = objLegalData.ExplainLegal2;
            }
            else if (ListTwo == false)
            {
                lblListTwo.Text = "No";
            }
            else
            {
                lblListTwo.Text = "";
            }

            bool? ListThree = objLegalData.Legal3;
            if (ListThree == true)
            {
                lblListThree.Text = "Yes";
                trBankruptcy.Style["Display"] = "";
                txtBankruptcy.Text = objLegalData.ExplainLegal3;
            }
            else if (ListThree == false)
            {
                lblListThree.Text = "No";
            }
            else
            {
                lblListThree.Text = "";
            }

            bool? ListFour = objLegalData.Legal4;
            if (ListFour == true)
            {
                lblListFour.Text = "Yes";
                trConviction.Style["Display"] = "";
                txtRisk.Text = objLegalData.ExplainLegal4;
            }
            else if (ListFour == false)
            {
                lblListFour.Text = "No";
            }
            else
            {
                lblListFour.Text = "";
            }

            // Get financial details
            //included by sgs for 20 & 26
            string Currentyear;
            if (DateTime.Now >= Convert.ToDateTime("01/01/" + Convert.ToString(DateTime.Now.Year)))     //004 - changed new year toi start in January
            {
                Currentyear = Convert.ToString(DateTime.Now.Year - 1);
            }
            else
            {
                Currentyear = Convert.ToString(DateTime.Now.Year - 2);
            }
            // SOC - 12/29/2010
            DataSet financialDataSet = objLegal.GetLegalFinancialData(Convert.ToInt64(objLegalData.PK_LegalID), Currentyear);
            if (IsFinancialDataExists(financialDataSet, Currentyear))
            {
                rptFinancial.DataSource = financialDataSet.Tables[0];
            }
            else
            {
                financialDataSet = objLegal.GetLegalFinancialData(Convert.ToInt64(objLegalData.PK_LegalID), (Convert.ToInt32(Currentyear) - 1).ToString());
                DataRow[] dataRow = financialDataSet.Tables[0].Select("Year <> '" + Currentyear.ToString() + "'");
                DataTable dataTable = financialDataSet.Tables[0].Clone();
                for (int count = 0; count < dataRow.Length; count++)
                {
                    dataTable.ImportRow(dataRow[count]);
                }
                DataView dataView = new DataView(dataTable, "", "Year DESC", DataViewRowState.CurrentRows);
                rptFinancial.DataSource = dataView.ToTable();
            }
            rptFinancial.DataBind();

            lblProjectRevenue.Text = objLegalData.CurrentYearProjectedRevenue;
            lblTotalBackLog.Text = objLegalData.CurrentTotalBacklog;

            //003-Sooraj
            lblBaseCurrency.Text =objCommon.GetCurrencyCode(objLegalData.FK_CurrencyID);
            lblDenominate.Text =objLegalData.IsDenominateInDollar.ConvertToText();
            trDenominateinDollar.Style["Display"] = IsBasedInUs ? "none" : "";  //G. vera added as Sooraj left out

            //Get banking details
            rptBanking.DataSource = objLegal.GetLegalBankingData(Convert.ToInt64(objLegalData.PK_LegalID));
            rptBanking.DataBind();
        }
        if (rptFinancial.Items.Count == 0)
        {
            tdFinancialData.Style["Display"] = "";

            if (DateTime.Now >= Convert.ToDateTime("05/01/" + Convert.ToString(DateTime.Now.Year)))
            {
                lblFinancialYear1.Text = Convert.ToString(DateTime.Now.Year - 1);
                lblFinancialYear2.Text = Convert.ToString(DateTime.Now.Year - 2);
                lblFinancialYear3.Text = Convert.ToString(DateTime.Now.Year - 3);
            }
            else
            {
                lblFinancialYear1.Text = Convert.ToString(DateTime.Now.Year - 2);
                lblFinancialYear2.Text = Convert.ToString(DateTime.Now.Year - 3);
                lblFinancialYear3.Text = Convert.ToString(DateTime.Now.Year - 4);
            }
            tblFinancial.Style["display"] = "none";
        }
        else
        {
            tdFinancialData.Style["display"] = "none";
            tblFinancial.Style["Display"] = "";
        }
        tdBankingData.Style["Display"] = rptBanking.Items.Count == 0 ? "" : "none";
    }

    #endregion


    protected void rptBanking_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblState = new Label();
            Label lblOtherState = new Label();
            lblState = (Label)e.Item.FindControl("lblState");
            if (lblState.Text != "0")
            {
                lblState.Text = Convert.ToString(objCommon.GetStateCode(Convert.ToInt16(lblState.Text)).Tables[0].Rows[0].ItemArray[1]);
            }
            else
            {
                lblState.Text = "";
            }
            lblState.Visible = lblState.Text.Equals("Other") ? false : true;
            
            Label lblCredit = new Label();
            lblCredit = (Label)e.Item.FindControl("lblCredit");
            if (!string.IsNullOrEmpty(lblCredit.Text.Trim()))
            {
                lblCredit.Text = lblCredit.Text.Equals("True") ? "Yes" : "No";
            }
            else
            {
                lblCredit.Text = "";
            }
        }
    }

    protected void rptFinancial_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox hdnNa = new CheckBox();
            hdnNa = (CheckBox)e.Item.FindControl("chkFinancialNA");
            HtmlTableRow tr = new HtmlTableRow();
            tr = (HtmlTableRow)e.Item.FindControl("trExplain");
            tr.Style["Display"] = hdnNa.Checked ? "" : "none";
        }
    }

    #region Button Events

    protected void imgPDF_Click(object sender, ImageClickEventArgs e)
    {
        //002
        Common objCommon = new Common();
        string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/VQF/PrintVQFForm.aspx?vendorid=" + hdVendorId.Value + "&print=1";
        string baseURI = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;
        objCommon.Print2PDF(url, baseURI);
        //objCommon.GeneratePDF(Convert.ToInt64(hdVendorId.Value), Convert.ToString(Request.QueryString["user"]));
    }

    #endregion

    private Boolean IsFinancialDataExists(DataSet dataSet, string currentYear)
    {
        DataRow[] dataRow = dataSet.Tables[0].Select("FK_LegalID <> 0 AND Year = '" + currentYear + "'");
        if (dataRow.Length > 0)
        {
            for (int count = 0; count < dataRow.Length; count++)
            {
                if (!dataRow[count].IsNull("NotAvailable"))
                {
                    if (Convert.ToBoolean(dataRow[count]["NotAvailable"]))
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(dataRow[count]["Explanation"])))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(dataRow[count]["ContractValue"])) && string.IsNullOrEmpty(Convert.ToString(dataRow[count]["AnnualRevenue"])))
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

}