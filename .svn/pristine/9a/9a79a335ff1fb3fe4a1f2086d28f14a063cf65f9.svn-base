﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text; //Added by N Schoenberger

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 06/20/2012: VMS Stabilization Release 1.0 Item 4.9.1 (JIRA:VPI-37)
 * 002 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 
 *
 ****************************************************************************************************************/
#endregion

public partial class Admin_InsertRating : System.Web.UI.Page
{

    //initilize object for the class
    clsRating objRating = new clsRating();
    Common objCommon = new Common();

    /// <summary>
    /// Page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //Check for the session variable, if it is null redirect to login page
        if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
        {
            //002
            if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
            {
                Response.Redirect("~/Admin/Login.aspx?timeout=1");
            }
            Response.Redirect("~/Admin/Login.aspx");
        }

        if (!Page.IsPostBack)
        {
            //002
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            //Check for the session variable, if it is null redirect to login page
            if (string.IsNullOrEmpty(Convert.ToString(Session["EmployeeId"])) || Convert.ToString(Session["EmployeeId"]) == "0")
            {
                //002
                if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
                {
                    Response.Redirect("~/Admin/Login.aspx?timeout=1");
                }
                Response.Redirect("~/Admin/Login.aspx");
            }
            else
            {
                //Call method avaod clicking the rater control
                RatingClick();
                txtFinalSubcontract.Attributes.Add("onBlur", "javascript:numberFormat(this.value,'','" + txtFinalSubcontract.ClientID + "')");
                txtFinalSubcontract.Attributes.Add("onKeyUp", "javascript:numberFormat(this.value,'','" + txtFinalSubcontract.ClientID + "')");

                if (Request.UrlReferrer != null)
                {
                    //Get the Page url to know from which the place is been transmitted
                    Session["Previous"] = Request.UrlReferrer.ToString().Trim();
                }
                //If logged in person is an employee display the employee menu
                if (Convert.ToString(Session["Access"]).Equals("2"))
                {
                    AdminMenu.DisplayMenu("Employee");
                }
                else
                {
                    AdminMenu.DisplayMenu("Admin");
                }
                //Bind Project
                //001 - BindProject();
                hdnvendorId.Value = Convert.ToInt64(Session["VendorAdminId"]).ToString() ;
                //Instantiate the class get the details about the vebdor and display the company name in the contractor label
                clsRegistration objRegistration = new clsRegistration();
                DAL.UspGetVendorDetailsByIdResult[] objVendor = objRegistration.GetVendor(Convert.ToInt64(hdnvendorId.Value));
                if (objVendor.Count() > 0)
                {
                    lblSubcontractorName.Text = objVendor[0].Company;
                }
                if (Request.QueryString.Count > 0)
                {
                    imbSave.Visible = false;
                    imbSubmitAndDone.Visible = false;
                    lblProjectNumber.Visible = true;
                    hdnRatingId.Value = objCommon.decode(Request.QueryString["RatingId"]);
                    ddlProjectNumber_SelectedIndexChanged(null, null);
                }
                else
                {
                    hdnRatingId.Value = string.Empty;
                    lblProjectNumber.Visible = false;
                    imbUpdate.Visible = false;
                    lblRatingDate.Text = DateTime.Now.ToShortDateString();
                    lblRaterName.Text = Convert.ToString(Session["EmployeeName"]);
                    lblRaterTitle.Text = Convert.ToString(Session["EmployeeTitle"]);
                }
            }
        }
    }

    /// <summary>
    /// Bind project dropdownlist
    /// </summary>
    /// 001 - Commented out
    //private void BindProject()
    //{
    //    ddlProjectNumber.DataSource = objRating.GetProject();
    //    ddlProjectNumber.DataTextField = "ProjectNumber";
    //    ddlProjectNumber.DataBind();
        
    //    ListItem lstSelect = new ListItem();
    //    lstSelect.Text = "--Select--";
    //    ddlProjectNumber.Items.Insert(0, lstSelect);
    //}

    /// <summary>
    /// Method to avoid clicking on the rating control
    /// </summary>
    protected void imgCancelScehduleControl_Click(object sender, ImageClickEventArgs e)
    {
        rtgSceduleControl.CurrentRating = 0;
    }

    private void RatingClick()
    {
        rtgOverallPerformance.Attributes.Add("Onclick", "return false;");
        rtgSafety.Attributes.Add("Onclick", "return false;");
        rtgQualityOfWork.Attributes.Add("Onclick", "return false;");
        rtgProjectManagement.Attributes.Add("Onclick", "return false;");
        rtgFinancialCapability.Attributes.Add("Onclick", "return false;");
        rtgFileManagement.Attributes.Add("Onclick", "return false;");
        rtgChange.Attributes.Add("Onclick", "return false;");
        rtgSceduleControl.Attributes.Add("Onclick", "return false;"); //Added by N Schoenberger on 8/12/2011
        rtgCorporation.Attributes.Add("Onclick", "return false;");
    }

   /// <summary>
   /// Method to save the records
   /// </summary>
   /// <param name="sender"></param>
   /// <param name="e"></param>
    protected void imbSave_Click(object sender, ImageClickEventArgs e)
    {
        //Validate the page
        Page.Validate();
        //If page is valid
        if (Page.IsValid)
        {
            //If project number selectedindex is > 0
            //001 - if (ddlProjectNumber.Text != "--Select--")
            if(!String.IsNullOrEmpty(ddlProjectNumber.Text))
            {
                bool? pay = null, payment = null, levels = null;

                if (rblpay.SelectedIndex < 0)
                {
                    pay = null;
                }
                else
                {
                    pay = rblpay.Items[0].Selected;
                }

                if (rblPayment.SelectedIndex < 0)
                {
                    payment = null;
                }
                else
                {
                    pay = rblpay.Items[0].Selected;
                }

                if (rblLevels.SelectedIndex < 0)
                {
                    levels = null;
                }
                else
                {
                    levels = rblpay.Items[0].Selected;
                }

                //Call method to insert the records
                int Count = objRating.InsertRating(Convert.ToByte(rtgChange.CurrentRating), txtComments.Text.Trim(), Convert.ToByte(rtgCorporation.CurrentRating), lblRaterTitle.Text.Trim(), Convert.ToByte(rtgFileManagement.CurrentRating), Convert.ToByte(rtgFinancialCapability.CurrentRating), Convert.ToString(Session["EmployeeNumber"]), Convert.ToInt64(hdnvendorId.Value), levels, Convert.ToDouble(rtgOverallPerformance.CurrentRating), Convert.ToByte(rtgProjectManagement.CurrentRating), lblProjectName.Text.Trim(), ddlProjectNumber.Text.Trim(), Convert.ToByte(rtgQualityOfWork.CurrentRating), 0, Convert.ToByte(rtgSafety.CurrentRating), payment, txtSubContractorScope.Text.Trim(), pay, 1,Convert.ToString(Session["Access"]),hdnRatingId.Value,txtFinalSubcontract.Text,Convert.ToByte(rtgSceduleControl.CurrentRating));

                if (Count != 0)
                {
                    lblMsg.Text = "<li>Rating already exists for this project";
                    modalExtnd.Show();
                }
                else
                {
                    lblMsg.Text = "<li>Subcontractor rating saved successfully";
                    modalExtnd.Show();
                }
                //  ddlProjectNumber.SelectedIndex = 0;
                // Clear();
            }
            else
            {
                lblMsg.Text = "<li>Please select project number";
                modalExtnd.Show();
            }
        }
        else
        {
            lblMsg.Text = Errormsg();
            modalExtnd.Show();
        }
    }

    /// <summary>
    /// Save and display the record in the list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSubmitAndDone_Click(object sender, ImageClickEventArgs e)
    {
        //Call method to check whether page is valid
        Page.Validate("InsertRating");
        //Validate the page
        if (Page.IsValid)
        {
            //Check Projectnumber selectedIndex is greater than zero
            bool? ispay = null, ispayment = null, islevels = null;
            ispay = rblpay.SelectedIndex < 0 ? new Nullable<bool>() : rblpay.Items[0].Selected;
            ispayment = rblPayment.SelectedIndex < 0 ? new Nullable<bool>() : rblPayment.Items[0].Selected;
            islevels = rblLevels.SelectedIndex < 0 ? new Nullable<bool>() : rblLevels.Items[0].Selected;
            //Call method to insert the record
            int Count = objRating.InsertRating(Convert.ToByte(rtgChange.CurrentRating), txtComments.Text.Trim(), Convert.ToByte(rtgCorporation.CurrentRating), lblRaterTitle.Text.Trim(), Convert.ToByte(rtgFileManagement.CurrentRating), Convert.ToByte(rtgFinancialCapability.CurrentRating), Convert.ToString(Session["EmployeeNumber"]), Convert.ToInt64(hdnvendorId.Value), islevels, Convert.ToByte(rtgOverallPerformance.CurrentRating), Convert.ToByte(rtgProjectManagement.CurrentRating), lblProjectName.Text.Trim(), ddlProjectNumber.Text.Trim(), Convert.ToByte(rtgQualityOfWork.CurrentRating), 1, Convert.ToByte(rtgSafety.CurrentRating), ispayment, txtSubContractorScope.Text.Trim(), ispay, 1, Convert.ToString(Session["Access"]), hdnRatingId.Value, txtFinalSubcontract.Text.Trim(), Convert.ToByte(rtgSceduleControl.CurrentRating));

            // Added by N Schoenberger on 8/17/2011 ***************
            Common objCommon = new Common();
            StringBuilder Body = new StringBuilder();

            Body.Append(lblRaterName.Text + " " + "has entered a new rating for project " + lblProjectName.Text.ToString().Trim() + " (" + ddlProjectNumber.Text.ToString().Trim() + ") on vendor " + lblSubcontractorName.Text.ToString() + "."); //Added by N. Schoenberger on 8/17/2011

            //call method to send e-mail
            objCommon.SendMail("noreply@haskell.com", ConfigurationManager.AppSettings["adminemail"].ToString(), "New Subcontractor Rating" + " (" + ddlProjectNumber.Text.ToString().Trim() + ")", Body);
            // *****************************************************

            if (Count != 0)
            {
                lblMsg.Text = "<li>Rating already exists for this project";
                modalExtnd.Show();
            }
            else
            {
                if (Convert.ToString(Session["Previous"]).Contains("?"))
                {
                    Response.Redirect(Convert.ToString(Session["Previous"]) + "&Page=Insert&display=Insert");
                }
                else 
                {
                    Response.Redirect(Convert.ToString(Session["Previous"]) + "?Page=Insert"); 
                }
            }
        }
        else
        {
            lblMsg.Text = Errormsg();
            modalExtnd.Show();
        }
    }

   /// <summary>
   /// Display the rating of the selected project
   /// </summary>
   /// <param name="sender"></param>
   /// <param name="e"></param>
    protected void ddlProjectNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlProjectNumber.Text.Trim() != "") lblProjectName.Text = objRating.GetProjectDescription(ddlProjectNumber.Text.Trim());    //001

        //001 - Commented
        //if (ddlProjectNumber.SelectedIndex > 0)
        //{
        //    lblProjectName.Text = objRating.GetProjectDescription(ddlProjectNumber.Text.Trim());
        //}
        //else
        //{
        //    lblProjectName.Text = string.Empty;
        //}
        //If querystring is greater than zero, the record is used for updation
        if (Request.QueryString.Count > 0)
        {
            //Get the details about the rating for the selected project number
            DAL.SRFProjectRating objGetRating = objRating.SelectRatingbyId(Convert.ToInt64(objCommon.decode(Request.QueryString["RatingID"])));
            if (objGetRating != null)
            {
                ddlProjectNumber.Visible = false;
                lblProjectName.Text = objGetRating.ProjectName;
                lblProjectNumber.Text = objGetRating.ProjectNumber;

                clsAdminLogin objAdminLogin = new clsAdminLogin();
                lblRaterName.Text = objAdminLogin.GetEmployeeName(objGetRating.FK_EmployeeNumber);

                clsRegistration objRegistration = new clsRegistration();
                DAL.UspGetVendorDetailsByIdResult[] objVendorDetails = objRegistration.GetVendor(Convert.ToInt64(objGetRating.FK_VendorID));
                lblSubcontractorName.Text = objVendorDetails[0].Company;
                lblRaterTitle.Text = objGetRating.EmployeeTitle;
                lblRatingDate.Text = Convert.ToDateTime(objGetRating.CreatedDT).ToShortDateString();
                txtComments.Text = objGetRating.Comments;
                txtSubContractorScope.Text = objGetRating.SubcontractorScope;
                txtFinalSubcontract.Text = objGetRating.FinalSubcontract;

                if (objGetRating.InsuranceLevels == null)
                {
                    rblLevels.SelectedIndex = -1;
                }
                else if (objGetRating.InsuranceLevels == true)
                {
                    rblLevels.SelectedIndex = 0;
                }
                else
                {
                    rblLevels.SelectedIndex = 1;
                }
                if (objGetRating.Subs_Vendors == true)
                {
                    rblpay.SelectedIndex = 0;
                }
                else if (objGetRating.Subs_Vendors == null)
                {
                    rblpay.SelectedIndex = -1;
                }
                else
                {
                    rblpay.SelectedIndex = 1;
                }

                if (objGetRating.SpecialPayment == true)
                {
                    rblPayment.SelectedIndex = 0;
                }
                else if (objGetRating.SpecialPayment == null)
                {
                    rblPayment.SelectedIndex = -1;
                }
                else
                {
                    rblPayment.SelectedIndex = 1;
                }
                rtgChange.CurrentRating = Convert.ToInt32(objGetRating.ChangeOrdersClaims);
                rtgCorporation.CurrentRating = Convert.ToInt32(objGetRating.Cooperation);
                rtgFileManagement.CurrentRating = Convert.ToInt32(objGetRating.FieldManagement);
                rtgFinancialCapability.CurrentRating = Convert.ToInt32(objGetRating.FinancialCapability);
                rtgOverallPerformance.CurrentRating = Convert.ToInt32(objGetRating.OverallPerformance);
                rtgProjectManagement.CurrentRating = Convert.ToInt32(objGetRating.ProjectManagement);
                rtgQualityOfWork.CurrentRating = Convert.ToInt32(objGetRating.QualityOfWork);
                rtgSafety.CurrentRating = Convert.ToInt32(objGetRating.Safety);
                rtgSceduleControl.CurrentRating = Convert.ToInt32(objGetRating.ScheduleControl);
                //Call method to enable the controls
                EnableControls();
            }
            else
            {
                //Clear text and enable the controls
                Clear();
                EnableControls();
            }
        }
    }

    /// <summary>
    /// Disable Controls
    /// </summary>
    private void DisableControls()
    {
        rtgChange.ReadOnly = true;
        rtgCorporation.ReadOnly = true;
        rtgFileManagement.ReadOnly = true;
        rtgFinancialCapability.ReadOnly = true;
        rtgOverallPerformance.ReadOnly = true;
        rtgProjectManagement.ReadOnly = true;
        rtgQualityOfWork.ReadOnly = true;
        rtgSafety.ReadOnly = true;
        rblLevels.Enabled = false;
        rblpay.Enabled = false;
        rblPayment.Enabled = false;
    }

    /// <summary>
    /// Enable Controls
    /// </summary>
    private void EnableControls()
    {
        rtgChange.ReadOnly = false;
        rtgCorporation.ReadOnly = false;
        rtgFileManagement.ReadOnly = false;
        rtgFinancialCapability.ReadOnly = false;
        rtgOverallPerformance.ReadOnly = false;
        rtgProjectManagement.ReadOnly = false;
        rtgQualityOfWork.ReadOnly = false;
        rtgSafety.ReadOnly = false;
        rblLevels.Enabled = true;
        rblpay.Enabled = true;
        rblPayment.Enabled = true;
    }

    /// <summary>
    /// Clear the text
    /// </summary>
    private void Clear()
    {
        txtComments.Text = string.Empty;
        txtSubContractorScope.Text = string.Empty;
        rblLevels.SelectedIndex = -1;
        rblpay.SelectedIndex = -1;
        rblPayment.SelectedIndex = -1;
        rtgChange.CurrentRating = 0;
        rtgCorporation.CurrentRating = 0;
        rtgFileManagement.CurrentRating = 0;
        rtgFinancialCapability.CurrentRating = 0;
        rtgOverallPerformance.CurrentRating = 0;
        rtgProjectManagement.CurrentRating = 0;
        rtgQualityOfWork.CurrentRating = 0;
        rtgSafety.CurrentRating = 0;
    }

    /// <summary>
    /// Validate the page and return the validation message
    /// </summary>
    /// <returns></returns>
    private string Errormsg()
    {
        string errmsg = String.Empty;
        foreach (IValidator validator in Page.Validators)
        {
            if (!validator.IsValid)
            {
                errmsg += "<li>" + validator.ErrorMessage + "</li>";
            }
        }
        return errmsg;
    }

    /// <summary>
    /// Return to the back page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgback_Click(object sender, ImageClickEventArgs e)
    {
        string strPreviousPage = Convert.ToString(Session["Previous"]).Replace("FromPage", "From").Trim();
        if (Convert.ToString(Session["Previous"]).Contains("?"))
        {
            if (strPreviousPage.Contains("DisplayRating"))
            {
               // Response.Redirect("DisplayRating.aspx?VendorID=" + objCommon.encode(Convert.ToString(Session["VendorAdminId"])) + "&From=" + Request.QueryString["FromPage"] + "&Page=Insert");
                Response.Redirect("DisplayRating.aspx?VendorID=" + Request.QueryString["VendorID"] + "&From=" + Request.QueryString["FromPage"] + "&Page=Insert");
            }
            else 
            { 
                Response.Redirect("SubContractorRating.aspx?From=" + Request.QueryString["FromPage"] + "&Page=Insert"); 
            }
        }
        else 
        {
            Response.Redirect(strPreviousPage + "?Page=Insert"); 
        }
    }

    /// <summary>
    /// Update the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbUpdate_Click(object sender, ImageClickEventArgs e)
    {
        if (rtgOverallPerformance.CurrentRating != 0)
        {
            bool? ispay = null, ispayment = null, islevels = null;

            if (Request.QueryString.Count > 0)
            {
                if (rblLevels.Items[0].Selected)
                {
                    islevels = true;
                }
                else if (rblLevels.Items[1].Selected)
                {
                    islevels = false;
                }
                if (rblpay.Items[0].Selected)
                {
                    ispay = true;
                }
                else if (rblpay.Items[1].Selected)
                {
                    ispay = false;
                }

                if (rblPayment.Items[0].Selected)
                {
                    ispayment = true;
                }
                else if (rblPayment.Items[1].Selected)
                {
                    ispayment = false;
                }

                //Call method to get details
                DAL.SRFProjectRating objDet = objRating.SelectRatingbyId(Convert.ToInt64(objCommon.decode(Request.QueryString["RatingID"])));

                if (objDet != null)
                {
                    //Call method to update
                    int Count = objRating.InsertRating(Convert.ToByte(rtgChange.CurrentRating), txtComments.Text.Trim(), Convert.ToByte(rtgCorporation.CurrentRating), Convert.ToString(objDet.EmployeeTitle), Convert.ToByte(rtgFileManagement.CurrentRating), Convert.ToByte(rtgFinancialCapability.CurrentRating), Convert.ToString(objDet.FK_EmployeeNumber), Convert.ToInt64(objDet.FK_VendorID), islevels, Convert.ToDouble(rtgOverallPerformance.CurrentRating), Convert.ToByte(rtgProjectManagement.CurrentRating), Convert.ToString(objDet.ProjectName), Convert.ToString(objDet.ProjectNumber), Convert.ToByte(rtgQualityOfWork.CurrentRating), Convert.ToByte(objDet.RatingStatus), Convert.ToByte(rtgSafety.CurrentRating), ispayment, txtSubContractorScope.Text.Trim(), ispay, 0, Convert.ToString(Session["Access"]), hdnRatingId.Value, txtFinalSubcontract.Text, Convert.ToByte(rtgSceduleControl.CurrentRating));
                    if (Count != 2)
                    {
                        lblMsg.Text = "Error while updating the record";
                        modalExtnd.Show();
                    }
                    else
                    {
                        lblMsg.Text = "<li>Subcontractor rating updated successfully";
                        modalExtnd.Show();
                    }
                }
            }
        }
        else
        {
            lblMsg.Text = "<li>Please select overall performance rating";
            modalExtnd.Show();
        }
    }

    /// <summary>
    /// Print the form
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrint_Click(object sender, ImageClickEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(@"<script language='javascript'>");
        sb.Append(@"window.open('print.aspx','PrintMe','height=300px,width=300px,scrollbars=1');");

        sb.Append(@"</script>");
        Session["SubContractorRating"] = pnlSubcontractorRating;
        Session["RatingPanel"] = pnlRating;
        Session["SubContractorPanel"] = spnEdit;
        ScriptManager.RegisterStartupScript(imbPrint, this.GetType(), "JSCR", sb.ToString(), false);
    }

    /// <summary>
    /// Clear Rating Star
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgClearRating_Click(object sender, ImageClickEventArgs e)
    {
        rtgChange.CurrentRating = 0;
        rtgCorporation.CurrentRating = 0;
        rtgFileManagement.CurrentRating = 0;
        rtgFinancialCapability.CurrentRating = 0;
        rtgOverallPerformance.CurrentRating = 0;
        rtgProjectManagement.CurrentRating = 0;
        rtgQualityOfWork.CurrentRating = 0;
        rtgSafety.CurrentRating = 0;
    }

    protected void cusvOverallPerformance_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (rtgOverallPerformance.CurrentRating == 0)
        {
            cusvOverallPerformance.ErrorMessage = "Please select overall performance";
            args.IsValid = false;
        }
    }

    protected void imgCancelOverallPerfomance_Click(object sender, ImageClickEventArgs e)
    {
        rtgOverallPerformance.CurrentRating = 0;
    }

    protected void imgRtgSafety_Click(object sender, ImageClickEventArgs e)
    {
        rtgSafety.CurrentRating = 0;
    }

    protected void imgRtgProjectManagement_Click(object sender, ImageClickEventArgs e)
    {
        rtgProjectManagement.CurrentRating = 0;
    }

    protected void imgRtgQualityOfWork_Click(object sender, ImageClickEventArgs e)
    {
        rtgQualityOfWork.CurrentRating = 0;
    }

    protected void imgRtgFileManagement_Click(object sender, ImageClickEventArgs e)
    {
        rtgFileManagement.CurrentRating = 0;
    }

    protected void imgRtgFinancialCapability_Click(object sender, ImageClickEventArgs e)
    {
        rtgFinancialCapability.CurrentRating = 0;
    }

    protected void imgRtgChange_Click(object sender, ImageClickEventArgs e)
    {
        rtgChange.CurrentRating = 0;
    }

    protected void imgRtgCorporation_Click(object sender, ImageClickEventArgs e)
    {
        rtgCorporation.CurrentRating = 0;
    }
    
}