﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using Winnovative.WnvHtmlConvert;
using System.IO;
using System.Collections;
using System.Drawing;

#region Change Comments
/*******************************************************************************************************
 * 001 G. Vera 07/06/2012 - VMS Stabilization Release 1.1 (JIRA:VPI-15)
 * 
 * 
 *******************************************************************************************************/
#endregion

/// <summary>
/// Summary description for Common
/// </summary>
public class Common : CommonPage
{
    string SREPath = string.Empty;
    string VQFPath = string.Empty;
    public string CompareChanges = string.Empty;
    public string CompareDelete = string.Empty;
    public string CompareChangeConsolidated = string.Empty;
    public string CompareChangeOutlook = string.Empty;

    public Common()
    {
        //
        // TODO: Add constructor logic here
        //
        SREPath = ConfigurationManager.AppSettings["SRESnapshot"].ToString();
        VQFPath = ConfigurationManager.AppSettings["VQFSnapshot"].ToString();

    }

    //Get Display State
    public void DisplayddlData(DropDownList ddl)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            ddl.DataSource = datacontext.UspGetState();
            ddl.DataTextField = "StateCode";
            ddl.DataValueField = "PK_StateID";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("--Select--", "0"));
        }
    }

    /// <summary>
    /// To Return State
    /// </summary>
    /// <returns>States</returns>
    public DAL.UspGetRegionsResult[] getRegion(int regionId)
    {
        using (DAL.HaskellDataContext datacontext = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            DAL.UspGetRegionsResult[] retval = datacontext.UspGetRegions(regionId).ToArray();
            return retval;
        }
    }

    /// <summary>
    /// To Return VendorDetails
    /// </summary>
    /// <returns>VendorDetails</returns>
    public DataSet GetVendorDetails(long vendorId)
    {
        return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "GetVendorDetails",
                                        new SqlParameter("@VendorID", vendorId));
    }

    /// <summary>
    /// Phone format
    /// </summary>
    /// <param name="pho1"></param>
    /// <param name="pho2"></param>
    /// <param name="pho3"></param>
    /// <returns>phone format</returns>
    public String Phoneformat(String pho1, String pho2, String pho3)
    {
        String ph = "";
        if ((pho1.Trim().Length != 0) && (pho2.Trim().Length != 0) && (pho3.Trim().Length != 0))
        {
            ph = "(" + pho1.Trim() + ")  " + pho2.Trim() + " - " + pho3.Trim();
        }
        return ph;
    }

    /// <summary>
    /// To replace the string which has apostrophe symbol.
    /// </summary>
    /// <param name="vstr"></param>
    /// <returns></returns>
    public String ReplaceStrRevDS(String vstr)
    {
        String result;
        result = vstr.Replace("'", "''");
        return result;
    }

    //validate  federal tax Format
    public bool Validatefederaltaxformat(String ft1, String ft2)
    {
        Regex objrev1 = new Regex(@"\d{2}");
        Regex objrev2 = new Regex(@"\d{7}");
        bool b1 = true;

        if (objrev1.IsMatch(ft1.Trim()) & objrev2.IsMatch(ft2.Trim()))
        {
            b1 = true;
        }
        else
        {
            b1 = false;
        }
        return b1;
    }


    //validate State Tax Format
    public bool ValidateStateTaxformat(String ph1, String ph2, String ph3)
    {
        Regex objrev1 = new Regex(@"[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789]");
        bool b1 = true;
        if (objrev1.IsMatch(ph1.Trim()) & objrev1.IsMatch(ph2.Trim()) & objrev1.IsMatch(ph3.Trim()))
        {
            b1 = true;
        }
        else
        {
            b1 = false;
        }
        return b1;
    }

    //validate Phone Format
    public bool ValidatePhoneformat(String ph1, String ph2, String ph3)
    {
        Regex objrev1 = new Regex(@"\d{3}");
        Regex objrev2 = new Regex(@"\d{4}");
        bool b1 = true;
        if (objrev1.IsMatch(ph1.Trim()) & objrev1.IsMatch(ph2.Trim()) & objrev2.IsMatch(ph3.Trim()))
        {
            b1 = true;
        }
        else
        {
            b1 = false;
        }
        return b1;
    }

    //To Split the Phone Number 
    public void loadphone(TextBox txtph1, TextBox txtph2, TextBox txtph3, String strphone)
    {
        if (!String.IsNullOrEmpty(strphone))
        {
            if (strphone.Length >= 10)
            {
                txtph1.Text = strphone.Substring(1, 3);
                txtph2.Text = strphone.Substring(7, 3);
                txtph3.Text = strphone.Substring(13, 4);
            }
        }
    }

    /// <summary>
    /// method to Get Attch Document Data
    /// </summary>
    public DataSet GetAttchDocumentData(long VendorID, bool IsAdmin,bool isSRadmin)
    {
        return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetAttachDocView",
                                        new SqlParameter("@VendorID", VendorID),
                                        new SqlParameter("@IsAdmin", IsAdmin), new SqlParameter("@isSRAdmin", isSRadmin));
    }

    /// <summary>
    /// get State Code
    /// </summary>
    /// <param name="StateId">State Id</param>
    /// <returns>state Code</returns>
    public DataSet GetStateCode(Byte StateId)
    {
        return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "GetStateCode",
                                        new SqlParameter("@StateId", StateId));
    }

    /// <summary>
    /// method to Delete Attch Document
    /// </summary>
    public Int32 DeleteAttchDoc(long DocumentID)
    {
        return SqlHelper.ExecuteNonQuery(con, CommandType.StoredProcedure, "UspDeleteAttachDoc",
                                        new SqlParameter("@DocumentID", DocumentID));
    }

    /// <summary>
    /// method to Select Attch Document Name
    /// </summary>
    public DataSet SelectAttchDocName(long DocumentID)
    {
        return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspSelectAttatchDocName",
                                       new SqlParameter("@DocumentID", DocumentID));
    }

    /// <summary>
    /// method to Get VQFStatus Data
    /// </summary>
    public DataSet GetVQFStatusData(long VendorID, Int16 Status)
    {
        return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspGetVQFStatus",
                                        new SqlParameter("@VendorID", VendorID),
                                        new SqlParameter("@Status", Status));
    }

    public object Phoneformat(string p, string p_2, string p_3, string p_4)
    {
        throw new NotImplementedException();
    }

    //To Check the Phonelength
    public string CheckPhone(string phone1, string phone2, string phone3)
    {
        string Phone = string.Empty;
        int Length;
        if ((phone1 != "") && (phone2 != "") && (phone3 != ""))
        {
            Phone = phone1 + phone2 + phone3;
            Length = Phone.Length;
            if (Length == 10)
            {
                Phone = Phoneformat(phone1, phone2, phone3);
            }
            else
            {
                Phone = "";
            }
        }
        else
        {
            Phone = "";
        }
        return Phone;
    }

    /// <summary>
    /// Send email
    /// </summary>
    /// <param name="clientName"></param>
    /// <param name="userName"></param>
    /// <param name="password"></param>
    /// <param name="mailTo"></param>
    /// <param name="Content"></param>
    public void SendMail(string fromaddress, string toaddress, string subject, StringBuilder Content)
    {
        MailMessage mail = new MailMessage();
        mail.From = new MailAddress(fromaddress);
        mail.To.Add(new MailAddress(toaddress));
        mail.Priority = MailPriority.Normal;
        mail.Subject = subject;
        mail.IsBodyHtml = true;

        mail.Body = Content.ToString();
        SmtpClient client = new SmtpClient();
        client.Host = ConfigurationManager.AppSettings["MailServer"];

        try
        {
            client.Send(mail);
        }
        catch (Exception e)
        {
            client.Host = "smtp.haskell.com";
            mail.To.Add(new MailAddress("vendorhelpdesk@haskell.com"));
            mail.Body = mail.Body + e.Message;
            client.Send(mail);
        }
    }

    /// <summary>
    /// Send email to multiple user
    /// </summary>
    /// <param name="clientName"></param>
    /// <param name="userName"></param>
    /// <param name="password"></param>
    /// <param name="mailTo"></param>
    /// <param name="Content"></param>
    public void SendMailToAdmin(string fromaddress, string toaddress, string subject, StringBuilder Content)
    {
        MailMessage mail = new MailMessage();
        mail.From = new MailAddress(fromaddress);
        //for (int i = 0; i < toaddress.Length; i++)
        //{
        //    mail.To.Add(new MailAddress(toaddress[i]));
        //}
        mail.To.Add(new MailAddress(toaddress));
        mail.Priority = MailPriority.Normal;
        mail.Subject = subject;
        mail.IsBodyHtml = true;

        mail.Body = Content.ToString();
        SmtpClient client = new SmtpClient();
        client.Host = ConfigurationManager.AppSettings["MailServer"];
        try
        {
            client.Send(mail);
        }
        catch (Exception e)
        {
            client.Host = "smtp.haskell.com";
            mail.To.Add(new MailAddress("vendorhelpdesk@haskell.com"));
            mail.Body = mail.Body + e.Message;
            client.Send(mail);
        }
    }

    //Encode the password
    public String encode(String strenc)
    {
        String enc = "";
        String pwd = strenc;
        for (int j = pwd.Length; j > 0; j--)
        {
            char c = Convert.ToChar(pwd.Substring(0, 1));
            int b = (int)c;
            b = b + 126;
            char d = (char)b;
            enc = enc + d;
            int n = pwd.Length - 1;
            pwd = pwd.Substring(1, n);
        }
        return enc.ToString();
    }

    //Decode the password
    public String decode(String strdecode)
    {
        String denc = "";
        String encpwd = strdecode;
        for (int j = encpwd.Length; j > 0; j--)
        {
            char c = Convert.ToChar(encpwd.Substring(0, 1));
            int b = (int)c;
            b = b - 126;
            char d = (char)b;
            denc = denc + d;
            int n = encpwd.Length - 1;
            encpwd = encpwd.Substring(1, n);
        }
        return denc;
    }


    public void GeneratePDF(Int64 vendorid, String username)
    {
        PdfConverter objPdfConverter = new PdfConverter();
        objPdfConverter.LicenseKey = ConfigurationManager.AppSettings["PDFLicense"].ToString();

        objPdfConverter.PageWidth = 0;
        objPdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;

        objPdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Best;
        objPdfConverter.PdfHeaderOptions.DrawHeaderLine = true;

        objPdfConverter.PdfDocumentOptions.AutoSizePdfPage = true;
        objPdfConverter.PdfDocumentOptions.ShowFooter = true;
        objPdfConverter.PdfDocumentOptions.EmbedFonts = false;

        objPdfConverter.PdfDocumentOptions.TopMargin = 10;
        objPdfConverter.PdfDocumentOptions.LeftMargin = 10;
        objPdfConverter.PdfDocumentOptions.RightMargin = 10;
        objPdfConverter.PdfDocumentOptions.BottomMargin = 10;

        // header
        objPdfConverter.PdfHeaderOptions.HeaderText = string.Empty;
        objPdfConverter.PdfHeaderOptions.HeaderHeight = 50;
        objPdfConverter.PdfHeaderOptions.HeaderTextAlign = HorizontalTextAlign.Left;
        objPdfConverter.PdfHeaderOptions.HeaderTextFontSize = 9;
        objPdfConverter.PdfDocumentOptions.ShowHeader = true;

        objPdfConverter.PdfHeaderOptions.HeaderSubtitleText = string.Empty;
        objPdfConverter.PdfDocumentInfo.AuthorName = "HTML to PDF Converter";
        objPdfConverter.AuthenticationOptions.Username =  ConfigurationSettings.AppSettings["ADAdminUser"].ToString();
        objPdfConverter.AuthenticationOptions.Password = ConfigurationSettings.AppSettings["ADAdminPassword"].ToString();

        string MyURL = string.Empty;
        byte[] downloadBytes = null;
        
        MyURL = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/Admin/VQFPdf.aspx?user=" + username + "&VendorID=" + vendorid;
        //MyURL = "http://" + ConfigurationManager.AppSettings["PdfURL"].ToString() + ":" + HttpContext.Current.Request.Url.Port + "/Admin/VQFPdf.aspx?user=" + username + "&VendorID=" + vendorid;

        string downloadName = "VQF";
        downloadName += ".pdf";

        downloadBytes = objPdfConverter.GetPdfBytesFromUrl(MyURL);

        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        response.Clear();
        response.AddHeader("Content-Type", "binary/octet-stream");
        response.AddHeader("Content-Disposition", "attachment; filename=" + downloadName + "; size=" + downloadBytes.Length.ToString());
        response.Flush();
        response.BinaryWrite(downloadBytes);
        response.Flush();
        response.End();
    }

    public void GenerateSREPDF()
    {
        PdfConverter objPdfConverter = new PdfConverter();
        objPdfConverter.LicenseKey = ConfigurationManager.AppSettings["PDFLicense"].ToString();

        objPdfConverter.PageWidth = 0;
        objPdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;

        objPdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Best;
        objPdfConverter.PdfHeaderOptions.DrawHeaderLine = true;

        objPdfConverter.PdfDocumentOptions.AutoSizePdfPage = true;
        objPdfConverter.PdfDocumentOptions.ShowFooter = true;
        objPdfConverter.PdfDocumentOptions.EmbedFonts = false;

        objPdfConverter.PdfDocumentOptions.TopMargin = 10;
        objPdfConverter.PdfDocumentOptions.LeftMargin = 10;
        objPdfConverter.PdfDocumentOptions.RightMargin = 10;
        objPdfConverter.PdfDocumentOptions.BottomMargin = 10;

        // header
        objPdfConverter.PdfHeaderOptions.HeaderText = string.Empty;
        objPdfConverter.PdfHeaderOptions.HeaderHeight = 50;
        objPdfConverter.PdfHeaderOptions.HeaderTextAlign = HorizontalTextAlign.Left;
        objPdfConverter.PdfHeaderOptions.HeaderTextFontSize = 9;
        objPdfConverter.PdfDocumentOptions.ShowHeader = true;

        objPdfConverter.PdfHeaderOptions.HeaderSubtitleText = string.Empty;
        objPdfConverter.PdfDocumentInfo.AuthorName = "HTML to PDF Converter";
        objPdfConverter.AuthenticationOptions.Username = ConfigurationSettings.AppSettings["ADAdminUser"].ToString();
        objPdfConverter.AuthenticationOptions.Password = ConfigurationSettings.AppSettings["ADAdminPassword"].ToString();

        string MyURL = string.Empty;
        byte[] downloadBytes = null;

        MyURL = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/SRE/PrintSRE.aspx?RiskId=" + Session["riskId"].ToString() + "&statusidprint=" + Session["statusidprint"].ToString();
        //MyURL = "http://" + ConfigurationManager.AppSettings["PdfURL"].ToString() + ":" + HttpContext.Current.Request.Url.Port + "/SRE/PrintSRE.aspx?RiskId=" + Session["riskId"].ToString() + "&statusidprint=0";
        //MyURL = "http://" + ConfigurationManager.AppSettings["PdfURL"].ToString() + ":" + HttpContext.Current.Request.Url.Port + "/VendorQualification/SRE/PrintSRE.aspx";
        string downloadName = "VQF";
        downloadName += ".pdf";

        downloadBytes = objPdfConverter.GetPdfBytesFromUrl(MyURL);

        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        response.Clear();
        response.AddHeader("Content-Type", "binary/octet-stream");
        response.AddHeader("Content-Disposition", "attachment; filename=" + downloadName + "; size=" + downloadBytes.Length.ToString());
        response.Flush();
        response.BinaryWrite(downloadBytes);
        response.Flush();
        response.End();
    }
	
    public Boolean SaveVQFSnapshot(Int64 vendorid, String username, string fileName)
    {
        try
        {
            PdfConverter objPdfConverter = new PdfConverter();
            objPdfConverter.LicenseKey = ConfigurationManager.AppSettings["PDFLicense"].ToString();

            objPdfConverter.PageWidth = 0;
            objPdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;

            objPdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Best;
            objPdfConverter.PdfHeaderOptions.DrawHeaderLine = true;

            objPdfConverter.PdfDocumentOptions.AutoSizePdfPage = true;
            objPdfConverter.PdfDocumentOptions.ShowFooter = true;
            objPdfConverter.PdfDocumentOptions.EmbedFonts = false;

            objPdfConverter.PdfDocumentOptions.TopMargin = 10;
            objPdfConverter.PdfDocumentOptions.LeftMargin = 10;
            objPdfConverter.PdfDocumentOptions.RightMargin = 10;
            objPdfConverter.PdfDocumentOptions.BottomMargin = 10;

            // header
            objPdfConverter.PdfHeaderOptions.HeaderText = string.Empty;
            objPdfConverter.PdfHeaderOptions.HeaderHeight = 50;
            objPdfConverter.PdfHeaderOptions.HeaderTextAlign = HorizontalTextAlign.Left;
            objPdfConverter.PdfHeaderOptions.HeaderTextFontSize = 9;
            objPdfConverter.PdfDocumentOptions.ShowHeader = true;

            objPdfConverter.PdfHeaderOptions.HeaderSubtitleText = string.Empty;
            objPdfConverter.PdfDocumentInfo.AuthorName = "HTML to PDF Converter";
	    objPdfConverter.AuthenticationOptions.Username =  ConfigurationSettings.AppSettings["ADAdminUser"].ToString();
	    objPdfConverter.AuthenticationOptions.Password = ConfigurationSettings.AppSettings["ADAdminPassword"].ToString();


            string MyURL = string.Empty;
            byte[] downloadBytes = null;
            MyURL = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/Admin/VQFPdf.aspx?user=" + username + "&VendorID=" + vendorid;
	    //MyURL = "http://" + ConfigurationManager.AppSettings["PdfURL"].ToString() + ":" + HttpContext.Current.Request.Url.Port + "/Admin/VQFPdf.aspx?user=" + username + "&VendorID=" + vendorid;
   
            string downloadName = "VQF";
            downloadName += ".pdf";

            downloadBytes = objPdfConverter.GetPdfBytesFromUrl(MyURL);

            if (System.IO.Directory.Exists(VQFPath))
            {
                System.IO.Directory.CreateDirectory(VQFPath);
            }
            string filePath = string.Concat(VQFPath, fileName, ".pdf");
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            System.IO.File.WriteAllBytes(filePath, downloadBytes);
            return true;
        }
        catch
        {
            return false;
        }
    }


    public int GetDocumentCount(long VendorID)
    {
        using (DAL.HaskellDataContext objHaskell = new DAL.HaskellDataContext(System.Configuration.ConfigurationManager.ConnectionStrings[1].ConnectionString))
        {
            return Convert.ToInt32(objHaskell.VQFAttachDocuments.Count(p => p.FK_VendorID == VendorID && p.isSRAdminUploaded ==false));
        }
    }

    public static string GetDocumentContentType(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".htm":
            case ".html":
            case ".log":
                return "text/HTML";
            case ".txt":
                return "text/plain";
            case ".docx":
            case ".doc":
                return "application/ms-word";
            case ".tiff":
            case ".tif":
                return "image/tiff";
            case ".asf":
                return "video/x-ms-asf";
            case ".avi":
                return "video/avi";
            case ".zip":
                return "application/zip";
            case ".xls":
            case ".csv":
                return "application/vnd.ms-excel";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            case ".wav":
                return "audio/wav";
            case ".mp3":
                return "audio/mpeg3";
            case ".mpg":
            case "mpeg":
                return "video/mpeg";
            case ".rtf":
                return "application/rtf";
            case ".asp":
                return "text/asp";
            case ".pdf":
                return "application/pdf";
            case ".fdf":
                return "application/vnd.fdf";
            case ".ppt":
                return "application/mspowerpoint";
            case ".dwg":
                return "image/vnd.dwg";
            case ".msg":
                return "application/msoutlook";
            case ".xml":
            case ".sdxl":
                return "application/xml";
            case ".xdp":
                return "application/vnd.adobe.xdp+xml";
            default:
                return "application/octet-stream";
        }
    }

    public Boolean SaveSRESnapshot(Int64 riskId, string fileName, Int32 statusid)
    {
        try
        {
            PdfConverter objPdfConverter = new PdfConverter();
            objPdfConverter.LicenseKey = ConfigurationManager.AppSettings["PDFLicense"].ToString();

            objPdfConverter.PageWidth = 0;
            objPdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;

            objPdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Best;
            objPdfConverter.PdfHeaderOptions.DrawHeaderLine = true;

            objPdfConverter.PdfDocumentOptions.AutoSizePdfPage = true;
            objPdfConverter.PdfDocumentOptions.ShowFooter = true;
            objPdfConverter.PdfDocumentOptions.EmbedFonts = false;

            objPdfConverter.PdfDocumentOptions.TopMargin = 10;
            objPdfConverter.PdfDocumentOptions.LeftMargin = 10;
            objPdfConverter.PdfDocumentOptions.RightMargin = 10;
            objPdfConverter.PdfDocumentOptions.BottomMargin = 10;

            // header
            objPdfConverter.PdfHeaderOptions.HeaderText = string.Empty;
            objPdfConverter.PdfHeaderOptions.HeaderHeight = 50;
            objPdfConverter.PdfHeaderOptions.HeaderTextAlign = HorizontalTextAlign.Left;
            objPdfConverter.PdfHeaderOptions.HeaderTextFontSize = 9;
            objPdfConverter.PdfDocumentOptions.ShowHeader = true;

            objPdfConverter.PdfHeaderOptions.HeaderSubtitleText = string.Empty;
            objPdfConverter.PdfDocumentInfo.AuthorName = "HTML to PDF Converter";
	    objPdfConverter.AuthenticationOptions.Username =  ConfigurationSettings.AppSettings["ADAdminUser"].ToString();
	    objPdfConverter.AuthenticationOptions.Password = ConfigurationSettings.AppSettings["ADAdminPassword"].ToString();


            string MyURL = string.Empty;
            byte[] downloadBytes = null;
            MyURL ="http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/SRE/PrintSRE.aspx?RiskID=" + riskId + "&statusidprint=" + statusid; ;
	    //MyURL ="http://" + ConfigurationManager.AppSettings["PdfURL"].ToString() + ":" + HttpContext.Current.Request.Url.Port + "/SRE/PrintSRE.aspx?RiskID=" + riskId + "&statusidprint=" + statusid; ;
	    string downloadName = "SRESnapShot";
            downloadName += ".pdf";

            downloadBytes = objPdfConverter.GetPdfBytesFromUrl(MyURL);
           
            string filePath = string.Concat(SREPath, fileName, ".pdf");
            
            if (System.IO.Directory.Exists(SREPath))
            {
                System.IO.Directory.CreateDirectory(SREPath);
            }
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
           
            System.IO.File.WriteAllBytes(filePath, downloadBytes);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public DataSet GetViewHistory(string Name, DateTime FromDate, DateTime ToDate, string sortExpression)
    {
        return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspViewVQFHistory",
            new SqlParameter("@Vendor", Name),
            new SqlParameter("@FromDate", FromDate), new SqlParameter("@ToDate", ToDate),
            new SqlParameter("@SortExp", sortExpression));
    }


    public DataSet GetViewSREHistory(string Name, DateTime FromDate, DateTime ToDate, string sortExpression)
    {
        return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspViewSREHistory",
            new SqlParameter("@Vendor", Name),
            new SqlParameter("@FromDate", FromDate), new SqlParameter("@ToDate", ToDate),
            new SqlParameter("@SortExp", sortExpression));
    }



    /// <summary>
    /// GetVQFManagerAutoMail
    /// </summary>
    /// <param name="VendorId"></param>
    /// <returns></returns>
    public DataSet GetVQFManagerAutoMail(long VendorId)
    {
        return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspVQFManagerAutoMailNotification",
            new SqlParameter("@VendorId", VendorId));
    }

    /// <summary>
    /// UpdateVQFManagerAutoMail
    /// </summary>
    /// <param name="NotificationID"></param>
    /// <returns></returns>
    public int UpdateVQFManagerAutoMail(long NotificationID)
    {
        return SqlHelper.ExecuteNonQuery(con, CommandType.StoredProcedure, "UspUpdateVQFManagerAutoMailStatus",
            new SqlParameter("@NotificationID", NotificationID));
    }



   //SGS Add new field on 09/12/2011
    /// <summary>
    /// To Check New Field
    /// </summary>
    /// <returns>CheckNewField</returns>
    public DataSet CheckNewField(long vendorId)
    {
        return SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "UspCheckNewField",
                                        new SqlParameter("@VendorID", vendorId));
    }

    /// <summary>
    /// 001 - Added
    /// </summary>
    /// <param name="url"></param>
    public void Print2PDF(string url)
    {
        PdfConverter objPdfConverter = new PdfConverter();
        objPdfConverter.LicenseKey = ConfigurationManager.AppSettings["PDFLicense"].ToString();

        objPdfConverter.PageWidth = 0;
        objPdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;

        objPdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Best;
        objPdfConverter.PdfHeaderOptions.DrawHeaderLine = true;

        objPdfConverter.PdfDocumentOptions.AutoSizePdfPage = true;
        objPdfConverter.PdfDocumentOptions.ShowFooter = true;
        objPdfConverter.PdfDocumentOptions.EmbedFonts = false;

        objPdfConverter.PdfDocumentOptions.TopMargin = 10;
        objPdfConverter.PdfDocumentOptions.LeftMargin = 10;
        objPdfConverter.PdfDocumentOptions.RightMargin = 10;
        objPdfConverter.PdfDocumentOptions.BottomMargin = 10;

        // header
        objPdfConverter.PdfHeaderOptions.HeaderText = string.Empty;
        objPdfConverter.PdfHeaderOptions.HeaderHeight = 50;
        objPdfConverter.PdfHeaderOptions.HeaderTextAlign = HorizontalTextAlign.Left;
        objPdfConverter.PdfHeaderOptions.HeaderTextFontSize = 9;
        objPdfConverter.PdfDocumentOptions.ShowHeader = true;

        objPdfConverter.PdfHeaderOptions.HeaderSubtitleText = string.Empty;
        objPdfConverter.PdfDocumentInfo.AuthorName = "HTML to PDF Converter";
        objPdfConverter.AuthenticationOptions.Username = ConfigurationSettings.AppSettings["ADAdminUser"].ToString();
        objPdfConverter.AuthenticationOptions.Password = ConfigurationSettings.AppSettings["ADAdminPassword"].ToString();

        string MyURL = string.Empty;
        byte[] downloadBytes = null;

        string downloadName = "VQF";
        downloadName += ".pdf";

        downloadBytes = objPdfConverter.GetPdfBytesFromUrl(url);

        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        response.Clear();
        response.AddHeader("Content-Type", "binary/octet-stream");
        response.AddHeader("Content-Disposition", "attachment; filename=" + downloadName + "; size=" + downloadBytes.Length.ToString());
        response.Flush();
        response.BinaryWrite(downloadBytes);
        response.Flush();
        response.End();
    }


    public string FixedWrapping(string comments, int lineLength)
    {
        string test = String.Empty;
        string result = String.Empty;
        ArrayList tempList = new ArrayList();

        int start = 0;
        if (!String.IsNullOrEmpty(comments))
        {
            if (comments.Length >= lineLength)
            {
                try
                {
                    while (start < comments.Length)
                    {
                        string temp = comments.Substring(start);
                        if (temp.EndsWith("."))
                        {
                            if (temp.Length > lineLength)
                            {
                                test = temp.Substring(start, temp.LastIndexOf(" "));
                            }
                            else
                            {
                                test = comments.Substring(start);
                            }
                        }
                        else
                        {
                            if (lineLength > comments.Length - start) test = comments.Substring(start);
                            else test = comments.Substring(start, lineLength);
                        }
                        
                        if (test.EndsWith(" ")) test = test.Substring(0, test.LastIndexOf(" "));                            
                        tempList.Add(test);
                        start = start + test.Length;
                    }

                    foreach (string item in tempList)
                    {
                        result += item.Trim() + "<BR />";
                    }
                }
                catch (Exception e)
                {
                    
                    throw;
                }
            }
            else result = comments;
        }

        return result;
    }
}