﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMSDAL;

public partial class UserControl_uclSafetyAttachmentBox : System.Web.UI.UserControl
{

    static clsSafety bllSafety = new clsSafety();
    DataSet Attch1 = new DataSet();
    public string SortExp;
    public string SearchSortExp;
    string DOCUMENT_LOCATION = string.Empty;
    string sMimeType;
    private static SafetySection safetySection;
    private string modalBoxId = "modalAttachments";
    private static string MAX_FILESIZE = ConfigurationManager.AppSettings["MaxFileSizeMBs"].ToString();
    private string targetControlId = "hdnTarget";
    public delegate void EventHandler(object sender, EventArgs e);
    public event EventHandler CloseEvent;
    private static bool changeSafetyStatus;
    private static string attachmentChanged = string.Empty;

    public string AttachmentChanged
    {
        get { return attachmentChanged; }
        set { attachmentChanged = value; }
    }

    public bool ChangeSafetyStatus
    {
        get { return changeSafetyStatus; }
        set { changeSafetyStatus = value; }
    }

    public string TargetControlId
    {
        get { return targetControlId; }
        set { targetControlId = value; }
    }

    public string ModalBoxId
    {
        get { return modalBoxId; }
        set { modalBoxId = value; }
    }

    public SafetySection SafetySection
    {
        get { return safetySection; }
        set { safetySection = value; }
    }
    private VMSDAL.VQFSafetyEMRRate emr;
    public VMSDAL.VQFSafetyEMRRate EMR
    {
        get { return emr; }
        set { emr = value; }
    }
    private VMSDAL.VQFSafetyOSHA osha;
    public VMSDAL.VQFSafetyOSHA OSHA
    {
        get { return osha; }
        set { osha = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DOCUMENT_LOCATION = ConfigurationManager.AppSettings["SafetyDocuments"].ToString() + "/";
        this.modalAttachments.TargetControlID = TargetControlId;

        if (!this.IsPostBack)
        {
            
        }
    }



    /// <summary>
    /// Opens this control in a modal box
    /// </summary>
    public void Open()
    {
        this.modalAttachments.Show();       
    }

    /// <summary>
    /// Load attached document
    /// </summary>
    public void InitiateData()
    {
        switch (SafetySection)
        {
            case SafetySection.EMR:
                if (EMR != null)
                {
                    hdnFilePath.Value = (!string.IsNullOrEmpty(EMR.FilePath)) ? DOCUMENT_LOCATION + EMR.FilePath : "";
                    hdnSafetyID.Value = EMR.FK_SafetyID.ToString();
                    hdnYear.Value = EMR.EMRYear.ToString();
                }
                break;
            case SafetySection.OSHA:
                if (OSHA != null)
                {
                    hdnFilePath.Value = (!string.IsNullOrEmpty(OSHA.FilePath)) ? DOCUMENT_LOCATION + OSHA.FilePath : "";
                    hdnSafetyID.Value = OSHA.FK_SafetyID.ToString();
                    hdnYear.Value = OSHA.OSHAYear.ToString();
                }
                break;
            case SafetySection.QUESTIONS:
                break;
            default:
                break;
        } 

        if (!string.IsNullOrEmpty(hdnFilePath.Value))
        {
            string fileName = hdnFilePath.Value.Substring(hdnFilePath.Value.LastIndexOf('/') + 1);
            lnkFileName.Text = fileName;
            trAttachedDoc.Style["Display"] = "";
            lblempty.Visible = false;
        }
        else
        {
            lblempty.Visible = true;
            trAttachedDoc.Style["Display"] = "none";
            lblempty.Text = "<br />No documents attached";
        }

        

    }

    protected void Delete_Click(object sender, ImageClickEventArgs e)
    {
        if (File.Exists(hdnFilePath.Value))
        {
            if (SafetySection == SafetySection.EMR)
            {
                EMR = this.GetEMR();
                if (EMR != null)
                {
                    EMR.FilePath = "";
                    bllSafety.UpdateSafetyEMR(EMR);
                    if (ChangeSafetyStatus) bllSafety.UpdateSafetyStatus(EMR.FK_SafetyID, 0);
                    File.Delete(hdnFilePath.Value);
                    lblattach.Text = "&nbsp;<li>File deleted successfully</li>";
                    AttachmentChanged += "%09" + EMR.EMRYear + " EMR attachment was changed by administrator%0A";
                }
            }
            if (SafetySection == SafetySection.OSHA)
            {
                OSHA = this.GetOSHA();
                if (OSHA != null)
                {
                    OSHA.FilePath = "";
                    bllSafety.UpdateSafetyOsha(OSHA);
                    if(ChangeSafetyStatus) bllSafety.UpdateSafetyStatus(OSHA.FK_SafetyID, 0);
                    File.Delete(hdnFilePath.Value);
                    lblattach.Text = "&nbsp;<li>File deleted successfully</li>";
                    AttachmentChanged += "%09" + OSHA.OSHAYear + " OSHA attachment was changed by administrator%0A";
                }
            }

            InitiateData();
            modalAttachments.Show();
        }

    }

    private VQFSafetyEMRRate GetEMR()
    {
        return bllSafety.SelectSafetyEMRRates(Convert.ToInt64(hdnSafetyID.Value), hdnYear.Value);
    }

    protected void lnkFileName_Click(object sender, EventArgs e)
    {
        string FileName = hdnFilePath.Value;
        Response.Redirect("../Common/GenerateFile.aspx?region=" + FileName);
        modalAttachments.Show();
    }

    private void DeleteFile(string filePath)
    {
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
        }
    }


    protected void imbUploadDoc_Click(object sender, ImageClickEventArgs e)
    {
        lblattach.Text = "";
        try
        {
            if (Page.IsValid)
            {
                lblattach.Text = string.Empty;

                if (!(fupFile.HasFile))
                {
                    lblattach.Text = "&nbsp;<li>Please upload at least one document </li>";
                    modalAttachments.Show();
                }
                else
                {
                    if (!System.IO.Directory.Exists(DOCUMENT_LOCATION))
                    {
                        System.IO.Directory.CreateDirectory(DOCUMENT_LOCATION);
                    }
                    if ((fupFile.HasFile) && (lblattach.Text == ""))
                    {
                        sMimeType = fupFile.PostedFile.ContentType;
                        if (sMimeType != "")
                        {
                            int max = Convert.ToInt32(MAX_FILESIZE);
                            if (fupFile.PostedFile.ContentLength - (max * 1024 * 1024) <= 0)
                            {
                                string filePath = string.Empty;
                                String ext = System.IO.Path.GetExtension(fupFile.FileName);
                                string fileName = string.Empty;

                                try
                                {
                                    switch (SafetySection)
                                    {
                                        case SafetySection.EMR:
                                            this.EMR = GetEMR();
                                            fileName = EMR.FK_SafetyID + "-" + SafetySection.EMR + "-" + EMR.EMRYear + ext;
                                            filePath = DOCUMENT_LOCATION + fileName;
                                            if (!string.IsNullOrEmpty(EMR.FilePath)) this.DeleteFile(DOCUMENT_LOCATION + EMR.FilePath);
                                            fupFile.PostedFile.SaveAs(filePath);
                                            this.EMR.FilePath = fileName;
                                            bllSafety.UpdateSafetyEMR(this.EMR);
                                            AttachmentChanged += "%09" + EMR.EMRYear + " EMR attachment was changed by administrator%0A";
                                            break;
                                        case SafetySection.OSHA:
                                            this.OSHA = this.GetOSHA();
                                            fileName = OSHA.FK_SafetyID + "-" + SafetySection.OSHA + "-" + OSHA.OSHAYear + ext;
                                            filePath = DOCUMENT_LOCATION + fileName;
                                            if (!string.IsNullOrEmpty(OSHA.FilePath)) this.DeleteFile(DOCUMENT_LOCATION + OSHA.FilePath);
                                            fupFile.PostedFile.SaveAs(filePath);
                                            OSHA.FilePath = fileName;
                                            bllSafety.UpdateSafetyOsha(OSHA);
                                            AttachmentChanged += "%09" + OSHA.OSHAYear + " OSHA attachment was changed by administrator%0A";
                                            break;
                                        case SafetySection.QUESTIONS:
                                            break;
                                        default:
                                            break;
                                    }

                                    lblattach.Text = "&nbsp;<li>Document uploaded successfully</li>";
                                }
                                catch (Exception ex)
                                {
                                    HaskellWebMail mail = new HaskellWebMail();
                                    mail.SendMail(ex.Message + "<br/><br/>" + ex.StackTrace, true, "VMS Error Saving Safety Attachment", "VendorHelpdesk@haskell.com", ("VendorHelpdesk@haskell.com;").Split(';'));
                                    lblattach.Text = "&nbsp;<li>Error occurred while saving file</li>";
                                    modalAttachments.Show();
                                }

                            }
                            else
                            {
                                lblattach.Text += "&nbsp;<li>File should be less than " + MAX_FILESIZE + "MB of size</li>";
                            }
                        }
                        else
                        {
                            lblattach.Text += "&nbsp;<li>Invalid file type</li>";
                        }
                    }
                    else
                    {

                        lblattach.Text = "&nbsp;<li>Please select a file to upload</li>";
                        modalAttachments.Show();
                    }

                    fupFile.Dispose();
                    InitiateData();
                    modalAttachments.Show();

                }
            }
        }
        catch (System.UnauthorizedAccessException ex)
        {
            lblattach.Text = "<li>Access denied to the document attached</li>";
            modalAttachments.Show();
        }
        finally
        {
        }
    }

    private VQFSafetyOSHA GetOSHA()
    {
        return bllSafety.SelectSafetyOsha(Convert.ToInt64(hdnSafetyID.Value), hdnYear.Value);
    }
    protected void imbAttachClose1_Click(object sender, ImageClickEventArgs e)
    {
        lblattach.Text = string.Empty;
        this.hdnFilePath.Value = string.Empty;
        this.hdnSafetyID.Value = string.Empty;
        this.hdnTitle.Value = string.Empty;
        this.hdnYear.Value = string.Empty;
        CloseEvent(this, null);
        //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "close", "<script language=javascript>window.opener.location.reload(true);self.close();</script>");
        //this.modalAttachments.Hide();
    }
}