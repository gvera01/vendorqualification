﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#region Change Comments
/****************************************************************************************************************
 * 001 G. Vera 07/05/2012: VMS Stabilization Release 1.1 (JIRA:VPI-21)
 * 
 *
 ****************************************************************************************************************/
#endregion

public partial class Admin_ListEmployee : CommonPage
{
    //Initialize the object for class
    clsUserMaintenance objUserMaintenance = new clsUserMaintenance();

    //Declare a flag variable
    public int flag = 0;
    public string SortExp;

    /// <summary>
    /// Create controls dyamically while page is been initialized
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        string strPageRequest = Request.Form["__EVENTTARGET"];
        if (hdnPageFlag.Value != "1")
        {
            //Call method to create controls dynamically
            CreatePagingControls(true, strPageRequest);
        }
        base.OnInit(e);
    }

    /// <summary>
    /// Page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        //001
        if (!String.IsNullOrEmpty(this.Request.QueryString["timeout"]))
        {
            Response.Redirect("~/Admin/Login.aspx?timeout=1");
        }

        if (!IsPostBack)
        {
            //001
            Response.AppendHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 1) +
                                      ";URL=" + this.AppRelativeVirtualPath.ToString().Substring(this.AppRelativeVirtualPath.ToString().LastIndexOf('/') + 1) +
                                      "?timeout=1");

            mnuMain.DisplayButton(4);
            hdnPage.Value = "1";
            ddlNumber.SelectedIndex = 0;
            ddlRoles1.SelectedIndex = 0;
            string job;
            Session["Page"] = null;
            if (Convert.ToString(Session["Access"]) == "2")
            {
                AdminMenu.DisplayMenu("Employee");
            }
            else
            {
                AdminMenu.DisplayMenu("Admin");
            }
            Byte Access = 0;
            job = ddlJob.SelectedIndex <= 0 ? "" : ddlJob.SelectedItem.Text.Trim();
            Access = ddlRoles.SelectedIndex <= 0 ? (byte)0 : Convert.ToByte(ddlRoles.SelectedItem.Value);
            
            imgSearch.Attributes.Add("onClick", "AssignValue('" + hdnPageFlag.ClientID + "');");
            hdnCount.Value = Convert.ToString(objUserMaintenance.DisplayUserMaintenanceTotal(job, Access,txtEmployeeNumber.Text));
            int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));
            lblTotal.Text = Convert.ToString(hdnCount.Value);
            lblTotalPage.Text = Convert.ToString(page);
            Session["Page"] = Convert.ToString(page);

            DisplayNextPrevious();
            Bind();
        }
        else
        {
            string job;
            Byte Access;
            job = ddlJob.SelectedIndex <= 0 ? "" : ddlJob.SelectedItem.Text.Trim();
            Access = ddlRoles.SelectedIndex <= 0 ? (byte)0 : Convert.ToByte(ddlRoles.SelectedItem.Value);
            hdnCount.Value = Convert.ToString(objUserMaintenance.DisplayUserMaintenanceTotal(job, Access,txtEmployeeNumber.Text));
            int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));
            lblTotal.Text = Convert.ToString(hdnCount.Value);
            lblTotalPage.Text = Convert.ToString(page);
            Session["Page"] = Convert.ToString(page);
            DisplayNextPrevious();
        }

        string strPageRequest;
        strPageRequest = Request.Form["__EVENTTARGET"];
        if (hdnPageFlag.Value != "1")
        {
            //Call method to display the controls created dynamically across postback
            CreatePagingControls(true, strPageRequest);
        }
        hdnPageFlag.Value = "0";
    }


    /// <summary>
    /// Method to create controls dynamically for paging
    /// </summary>
    /// <param name="FromLoad"></param>
    /// <param name="strPageRequest"></param>
    private void CreatePagingControls(bool FromLoad, string strPageRequest)
    {
        if (FromLoad)
        {
            //Paging
            //1. Current page is assigned in hdnpage
            //2. Total number of Pages is in session["page"]
            //3. Check Whether Current page and total number of pages are not null 
            //Since the number-of-pages changes as per the Records-per-page count in dropdown we dynamically create controls there
            //controls created in dropdown change event will be replaced here and so first time teh event does not occur
            //to avoid that problem, get the id of the event raised by the control is not the dropdown of Records-per-page
        
            if ((!string.IsNullOrEmpty(hdnPage.Value)) && (!string.IsNullOrEmpty(Convert.ToString(Session["Page"]))) && (string.IsNullOrEmpty(strPageRequest) || (strPageRequest.ToLower() != "ddlnumber")))
            {
                //Loop to create controls. condition to display only 5 number in a page
                for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
                {
                    // check i greater than zero and less than the total number of pages
                    if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                    {
                        //instantiate link button
                        LinkButton lnkPage = new LinkButton();
                        //Call javascript method on client client to assign value to get the current page selected
                        lnkPage.Attributes.Add("onClick", "GetValue(" + i + ")");
                        //Dynamically raise event for the controls created
                        lnkPage.Click += new EventHandler(lnkPage_Click);
                        //Add controls to the placeholder
                        plcCrntPage.Controls.Add(lnkPage);
                        //Assign text to the created controls
                        lnkPage.Text = Convert.ToString(i);
                    }
                }
            }
        }
        else
        {
            //Clear the controls in placeholder
            plcCrntPage.Controls.Clear();
            for (int i = (Convert.ToInt32(hdnPage.Value) - 5); i <= (Convert.ToInt32(hdnPage.Value) + 5); i++)
            {
                if (i > 0 && i <= Convert.ToInt32(Session["Page"]))
                {
                    LinkButton lnkPage = new LinkButton();
                    lnkPage.Attributes.Add("onClick", "GetValue(" + i + ")");
                    lnkPage.Click += new EventHandler(lnkPage_Click);
                    plcCrntPage.Controls.Add(lnkPage);
                    lnkPage.Text = Convert.ToString(i);
                }
            }
        }
        foreach (Control lnkpaging in plcCrntPage.Controls)
        {
            LinkButton lnkCurrentPage = (LinkButton)lnkpaging;
            lnkCurrentPage.CssClass = lnkCurrentPage.Text.Equals(hdnPage.Value) ? "Selectlnkcolor" : "lnkcolor";
        }
    }

    /// <summary>
    /// Method to display next and previous buttons
    /// </summary>
    private void DisplayNextPrevious()
    {
        imbNext.Visible = Convert.ToInt32(Session["Page"]) <= Convert.ToInt32(hdnPage.Value) ? false : true;
        imbPrevious.Visible = hdnPage.Value.Equals("1") ? false : true;
    }

    /// <summary>
    /// Change the css for the selected control
    /// </summary>
    private void StyleForSelectedControl(string CurrentPage)
    {
        foreach (Control ctrl in plcCrntPage.Controls)
        {
            if (ctrl.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
            {
                LinkButton lnk1 = (LinkButton)ctrl;
                lnk1.CssClass = lnk1.Text.Equals(CurrentPage) ? "Selectlnkcolor" : "lnkcolor";
            }
        }
    }

    /// <summary>
    /// Method for the click event to dynamically created controls
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void lnkPage_Click(object sender, EventArgs e)
    {
        LinkButton Currentlnk = new LinkButton();
        Currentlnk = (LinkButton)sender;
        hdnPage.Value = Currentlnk.Text;
        //Call method to apply styles for the selected link
        StyleForSelectedControl(Currentlnk.Text.Trim());
        //Call method to display next and previous button
        DisplayNextPrevious();
        //Call method to bind gridview
        Bind();
    }

    /// <summary>
    /// Bind data to the gridview
    /// </summary>
    private void Bind()
    {
        string job;
        Byte Access = 0;
        //Get the designation of the employee
        if (ddlJob.SelectedIndex <= 0)
        {
            job = "";
        }
        else
        {
            job = ddlJob.SelectedItem.Text.Trim();
        }

        //Get the roles of the employee
        if (ddlRoles.SelectedIndex <= 0)
        {
            Access = 0;
        }
        else
        {
            Access = Convert.ToByte(ddlRoles.SelectedItem.Value);
        }
        SortExp = hdnSort.Value;   
        if (SortExp == null)
        {
            SortExp = "";
        }

        //Based on the selection filter the recrods
        grdVendor.DataSource = objUserMaintenance.DisplayUserMaintenance(job,Access,txtEmployeeNumber.Text, Convert.ToInt32(hdnPage.Value), Convert.ToInt32(ddlNumber.SelectedItem.Text.Trim()), SortExp);
        grdVendor.DataBind();

        //If not record is found display the message
        if (grdVendor.Rows.Count > 0)
        {
            tblContent.Style["display"] = "";
            lblmsg.Text = string.Empty;
        }
        else
        {
            tblContent.Style["display"] = "None";
            lblmsg.Text = "Sorry no records found";
        }
    }

    /// <summary>
    /// Sorting the record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;
        if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortDirection"])))
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
        else if (Convert.ToString(ViewState["SortDirection"]) == Convert.ToString(SortDirection.Ascending))
        {
            GridViewSortDirection = SortDirection.Descending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Descending);
            SortGridView(sortExpression, "DESC");
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            ViewState["SortDirection"] = Convert.ToString(SortDirection.Ascending);
            SortGridView(sortExpression, "ASC");
        }
    }

    /// <summary>
    /// Get sorting direction
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
            {
                ViewState["sortDirection"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["sortDirection"];
        }
        set
        {
            ViewState["sortDirection"] = value;
        }
    }

    /// <summary>
    /// Method to sort gridview
    /// </summary>
    /// <param name="sortExpression"></param>
    /// <param name="direction"></param>
    private void SortGridView(string sortExpression, string direction)
    {
        SortExp = sortExpression + " " + direction;
        hdnSort.Value = SortExp;   
        Bind();
    }

    /// <summary>
    /// Display Next page records
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbNext_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) + 1);
        lblTotalPage.Text = Convert.ToString(Session["Page"]);
        //Call method to display previous and next button
        DisplayNextPrevious();
        //Apply style for the selected link
        StyleForSelectedControl(hdnPage.Value);
        //Call method to bind gridview
        Bind();
    }

    /// <summary>
    /// Display previous page records
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbPrevious_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = Convert.ToString(Convert.ToInt32(hdnPage.Value) - 1);
        lblTotalPage.Text = Convert.ToString(Session["Page"]);
        //Call method to display previous and next button
        DisplayNextPrevious();
        //Apply style for the selected link
        StyleForSelectedControl(hdnPage.Value);
        //Call method to bind gridview
        Bind();
    }

    /// <summary>
    /// Display records as the per the selected value of records-per-page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnPage.Value = "1";
        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Value))));
        Session["Page"] = Convert.ToString(page);
        lblTotalPage.Text = Convert.ToString(page);
        //Call method to create controls dynamically
        CreatePagingControls(false, "");
        //Call method to display previous and next button
        DisplayNextPrevious();
        //Apply style for the selected link
        StyleForSelectedControl(hdnPage.Value);
        //Call method to bind gridview
        Bind();
    }

    /// <summary>
    /// Display the record based on the criteria selected
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgSearch_Click(object sender, ImageClickEventArgs e)
    {
        hdnPage.Value = "1";
        ddlNumber.SelectedIndex = 0;
        string job;
        Byte Access = 0;
        job = ddlJob.SelectedIndex <= 0 ? "" : ddlJob.SelectedItem.Text.Trim();
        Access = ddlRoles.SelectedIndex <= 0 ? (byte)0 : Convert.ToByte(ddlRoles.SelectedItem.Value);

        hdnCount.Value = Convert.ToString(objUserMaintenance.DisplayUserMaintenanceTotal(job, Access,txtEmployeeNumber.Text));
        int page = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(hdnCount.Value) / Convert.ToDecimal(ddlNumber.SelectedItem.Text.Trim()))));
        lblTotal.Text = Convert.ToString(hdnCount.Value);
        lblTotalPage.Text = Convert.ToString(page);
        Session["Page"] = Convert.ToString(page);
        //Call method to bind gridview
        Bind();
        //Call method to display next and previous buttons
        DisplayNextPrevious();
        plcCrntPage.Controls.Clear();
        //Create Paging controls
        CreatePagingControls(false, "");
        //Apply styles for the selected page
        StyleForSelectedControl("1");
    }

    /// <summary>
    /// Change status and roles for the selected employee
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgAllocate_Click(object sender, ImageClickEventArgs e)
    {
        int flag = 0;
        foreach (GridViewRow grd in grdVendor.Rows)
        {
            flag++;
            Label lbl = new Label();
            lbl = (Label)grd.FindControl("lblEmployeeNumber");
            CheckBox chk = new CheckBox();
            chk = (CheckBox)grd.FindControl("chkAllocate");
            if (chk.Checked)
            {
                flag--;
                //Call method to update the record
                objUserMaintenance.UpdateEmployeeAccess(lbl.Text.Trim(), Convert.ToByte(ddlRoles1.SelectedItem.Value));
            }
        }
        if (flag == grdVendor.Rows.Count)
        {
            modalExtnd.Show();
            lblErrMsg.Text = "<li> Please select at least one option</li>";
        }
        else
        {
            modalExtnd.Show();
            lblErrMsg.Text = "<li>Record updated successfully</li>";
        }
        //Call method to bind gridview
        Bind();
    }

    /// <summary>
    /// Define roles for the access
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbl = new Label();
            lbl = (Label)e.Row.FindControl("lblAccess");
            switch (lbl.Text.Trim())
            { 
                case "1":
                    lbl.Text = "Admin";
                    break;
                case "2":
                    lbl.Text = "Employee";
                    break;
                case "3":
                    lbl.Text = "Enable";
                    break;
                case "4":
                    lbl.Text = "Disable";
                    break;
            }
        }
    }
}