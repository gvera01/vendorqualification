# Vendor Management System


### About
The VMS, formerly called Vendor Qualification System, was designed as an externally-accessible web form for subcontractors. Each vendor completes a profile. From that profile, Haskell employees can complete a summary risk assessment (SRE) before the subcontrator is allowed to bid on a scope of work.

Ideally, new SREs must be performed for each prject a subcontractor is bidding on or at any individual PM's discretion. 

### Tech
Hosted web applcation. ASP.NET, C#, SQL backend. HTML4/Javascript front end. 

### History
This system was a custom, made-to-order solution by [SGS Technologies](http://www.sgstechnologies.net/) written before 2011. 

### Primary Users
Dianne Dutkiewicz

### Maintenance
Primary: @gvera

Secondary: @jcbrough