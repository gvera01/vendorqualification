﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VMSDAL
{
    public class ISOandQSCerts
    {
        public long? Pk_CompanyCertificationID {get;set;}
        public long? FK_CompanyID { get; set; }
        public string Certification { get; set; }
        public bool? IsFederalSB { get; set; }
        public bool? Other { get; set; }
        public bool? IsIndustryCert { get; set; }
        public long? FK_CompanyCertificationID { get; set; }

    }
}
