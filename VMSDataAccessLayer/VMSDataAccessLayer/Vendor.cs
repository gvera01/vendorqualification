//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VMSDAL
{
    using System;
    using System.Collections.Generic;
    
    [Serializable]
    public partial class Vendor
    {
        public Vendor()
        {
            this.TermsConditionsAccepteds = new HashSet<TermsConditionsAccepted>();
            this.VQFAttachDocuments = new HashSet<VQFAttachDocument>();
            this.VQFCompanies = new HashSet<VQFCompany>();
            this.VQFInsurances = new HashSet<VQFInsurance>();
            this.VQFLegals = new HashSet<VQFLegal>();
            this.VQFReferences = new HashSet<VQFReference>();
            this.VQFSafeties = new HashSet<VQFSafety>();
            this.VQFTrades = new HashSet<VQFTrade>();
            this.SRFProjectRatings = new HashSet<SRFProjectRating>();
        }
    
        public long PK_VendorID { get; set; }
        public string Company { get; set; }
        public string VendorNumber { get; set; }
        public string TrackingNumber { get; set; }
        public string TaxID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public Nullable<short> FK_State { get; set; }
        public string OtherState { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public byte VendorStatus { get; set; }
        public bool PasswordStatus { get; set; }
        public bool JDE { get; set; }
        public Nullable<decimal> SubContractorRating { get; set; }
        public System.DateTime CreatedDT { get; set; }
        public System.DateTime ExpiryDT { get; set; }
        public Nullable<System.DateTime> LastModifiedDT { get; set; }
        public Nullable<System.DateTime> RegisteredDate { get; set; }
        public Nullable<System.DateTime> SubmittedDate { get; set; }
        public Nullable<byte> ExpiryStatus { get; set; }
        public bool HasAcceptedTermsConditions { get; set; }
        public Nullable<System.DateTime> HasAcceptedTermsConditionsDate { get; set; }
        public Nullable<bool> NewRequirementAlert { get; set; }
        public bool IsBasedInUS { get; set; }
    
        public virtual ICollection<TermsConditionsAccepted> TermsConditionsAccepteds { get; set; }
        public virtual ICollection<VQFAttachDocument> VQFAttachDocuments { get; set; }
        public virtual ICollection<VQFCompany> VQFCompanies { get; set; }
        public virtual ICollection<VQFInsurance> VQFInsurances { get; set; }
        public virtual ICollection<VQFLegal> VQFLegals { get; set; }
        public virtual ICollection<VQFReference> VQFReferences { get; set; }
        public virtual ICollection<VQFSafety> VQFSafeties { get; set; }
        public virtual ICollection<VQFTrade> VQFTrades { get; set; }
        public virtual ICollection<SRFProjectRating> SRFProjectRatings { get; set; }
    }
}
