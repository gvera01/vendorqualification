﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Collections;
using System.Web.UI;
using System.Linq;

#region Comments
//*********************************************************************************************************************
//001  G. vera - Added method
//002  G. Vera 12/20/2016 -  Add new method
//*********************************************************************************************************************

#endregion

namespace VMSDAL
{
    /*
     Sooraj - 
     
     Extension class to handle general data type conversions      
     
     */
    public static class Extension
    {
       
        //001
        public static string DecodeVQFStatus(this byte value)
        {
            switch (value)
            {
                case 0:
                    return "In Progress";
                case 1:
                    return "Pending";
                case 2:
                    return "Complete";
                case 3:
                    return "Revision";
                case 4:
                    return "Reject";
                case 5:
                    return "Incomplete";
                case 6:
                    return "Expired";
                default:
                    break;
            }

            return string.Empty;
        }

        //001
        public static int ConvertYesNoToSelectedIndex(this string value)
        {
            if (value == "Y") return 0;
            if (value == "N") return 1;
            return -1;
        }
        public static short ConvertToShortInt(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return 0;
            return short.Parse(value);
        }

        public static string ConvertToString(this short value)
        {
            return string.Format("{0}", value);
        }


        public static string ConvertToString(this int value)
        {
            return string.Format("{0}", value);
        }

        public static string ConvertToString(this object value)
        {
            return string.Format("{0}", value);
        }

        //002
        public static string ConvertToYesNo(this bool value)
        {
            return (value) ? "Yes" : "No";
        }

        public static bool ConvertToBool(this string value)
        {
            if (value != null && value.Length > 2)
            {
                if (value == "Yes") return true;
                if (value == "True") return true;
                return false;
            }
            return (value == "1"); // Return True for 1 else False 
        }

        //Returns all controls in all levels:
        public static IEnumerable<Control> AllControls(this Control theStartControl)
        {
            var controlsInThisLevel = theStartControl.Controls.Cast<Control>();
            return controlsInThisLevel.SelectMany(AllControls).Concat(controlsInThisLevel);
        }

        //Returns all controls of a certain type in all levels:
        public static IEnumerable<Control> AllControls<TheControlType>(this Control theStartControl) where TheControlType : Control
        {
            var controlsInThisLevel = theStartControl.Controls.Cast<Control>();
            return controlsInThisLevel.SelectMany(AllControls<TheControlType>).Concat(controlsInThisLevel.OfType<TheControlType>());
        }

        //(Another way) Returns all controls of a certain type in all levels, integrity derivation:
        public static IEnumerable<Control> AllControlsOfType<TheControlType>(this Control theStartControl) where TheControlType : Control
        {
            return theStartControl.AllControls().OfType<TheControlType>();
        }

        //(Another way) Returns all controls with a certain ID in all levels
        public static Control ControlWithID(this Control theStartControl, string ID)
        {
            return theStartControl.AllControls().SingleOrDefault(c => c.ID == ID);
        }

        public static string ConvertToString(this bool value)
        {
            return (value == true) ? "1" : "0"; // Return 1 for True else 0 
        }

        public static string ConvertToText(this bool value)
        {
            return (value == true) ? "Yes" : "No"; // Return 1 for True else 0 
        }

        public static string ConvertToBusinessId(this string value)
        {

            return string.Format("{0}", value.Trim()); //For BusinessId append ~ at end 
        }

        public static string ConvertToTaxId(this string value,bool IsBasedInUS=true)
        {
            string returnValue = string.Empty;
            
            if(IsBasedInUS)
            {
                if (value.Length == 9)
                {
                    returnValue = value.Substring(0, 2) + "-" + value.Substring(2, 7);
                }
                else if (value.Length == 10)  //Vendor table has Tax ID with length 10 , so used separate logic here 
                {
                    returnValue = value.Substring(0, 2) + "-" + value.Substring(2);
                }

                else if (value == "Tax ID")
                {
                    returnValue = "Tax ID";
                }
                else
                {
                    returnValue = "";
                }
            }
            else
            {
                returnValue = value.TrimEnd('~');
            }

            return returnValue;
        }

        /// <summary>
        /// 001
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ConvertToInteger(this object value)
        {
            int ret = 0;
            try
            {
                ret = Convert.ToInt32(value);
                return ret;
            }
            catch (Exception)
            {

                return 0;
            }
        }
    }
}
