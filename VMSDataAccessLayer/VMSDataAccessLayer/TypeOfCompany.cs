﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VMSDAL
{
    public class TypeOfCompany
    {
        public static string Supplier = "Supplier";
        public static string DesignConsultant = "Design Consultant";
        public static string EquipAndOnsiteSupport = "Equip/Onsite Support";
        public static string Subcontractor = "Subcontractor";
    }
}
