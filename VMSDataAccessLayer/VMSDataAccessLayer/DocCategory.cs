﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VMSDAL
{
    public enum DocCategory
    {
        VQFAdminUpload = 1,
        VQFVendorUpload = 2,
        VQFSRUpload = 3,
        W9 = 4,
        FinancialStatement = 5
    }
}
