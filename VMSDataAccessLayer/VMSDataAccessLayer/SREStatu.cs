//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VMSDAL
{
    using System;
    using System.Collections.Generic;
    
    [Serializable]
    public partial class SREStatu
    {
        public int PK_StatusID { get; set; }
        public int StatusID { get; set; }
        public string StatusDescription { get; set; }
    }
}
