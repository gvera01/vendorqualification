//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VMSDAL
{
    using System;
    
    public partial class UspGetVQFInsuranceResult
    {
        public long PK_InsuranceID { get; set; }
        public long FK_VendorID { get; set; }
        public string Broker_CompanyName { get; set; }
        public string InsuranceAgent { get; set; }
        public string AgentTelephoneNumber { get; set; }
        public string InsuranceCarrier { get; set; }
        public string GeneralEachOccurrence { get; set; }
        public string Product_CompletedOperations { get; set; }
        public string GeneralAggregate { get; set; }
        public Nullable<bool> AdditionalInsured { get; set; }
        public string EachAccident { get; set; }
        public string StateRequirements { get; set; }
        public string EmployeesLiability { get; set; }
        public string DiseaseEachEmployee { get; set; }
        public string DiseasePolicyLimit { get; set; }
        public string ProfessionalEachOccurrence { get; set; }
        public string Bonding_SuretyCompanyName { get; set; }
        public string CompanyContactName { get; set; }
        public string ContactTelephoneNumber { get; set; }
        public string TotalBondingCapacity { get; set; }
        public string CurrentBondingCapacity { get; set; }
        public Nullable<byte> InsuranceStatus { get; set; }
        public System.DateTime CreatedDT { get; set; }
        public Nullable<System.DateTime> LastModifiedDT { get; set; }
        public Nullable<bool> IsPolicy { get; set; }
        public Nullable<bool> IsGeneral { get; set; }
        public string Umbrella { get; set; }
        public string Excess { get; set; }
        public Nullable<bool> WCPolicy { get; set; }
        public Nullable<bool> Umbrella_Excess { get; set; }
        public string EmployersLiability { get; set; }
    }
}
