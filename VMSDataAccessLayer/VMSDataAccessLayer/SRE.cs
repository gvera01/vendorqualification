//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VMSDAL
{
    using System;
    using System.Collections.Generic;
    
    [Serializable]
    public partial class SRE
    {
        public long FK_RiskID { get; set; }
        public Nullable<System.DateTime> VendorQualificationDate { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string ContactName { get; set; }
        public string AppSubcontractorValue { get; set; }
        public string BidPackageDescription { get; set; }
        public string YearsInBusiness { get; set; }
        public Nullable<bool> VQFLegal1 { get; set; }
        public Nullable<bool> VQFLegal2 { get; set; }
        public Nullable<bool> VQFLegal3 { get; set; }
        public Nullable<bool> Business1 { get; set; }
        public Nullable<bool> Business2 { get; set; }
        public Nullable<bool> Business3 { get; set; }
        public Nullable<bool> Business4 { get; set; }
        public Nullable<bool> BusinessRiskIndicator { get; set; }
        public string SubcontractorCurrentBid { get; set; }
        public string LargestMaxContract { get; set; }
        public string CurrentBacklog { get; set; }
        public string AverageAnnualRevenue { get; set; }
        public string NextLowestBid { get; set; }
        public Nullable<bool> Financial1 { get; set; }
        public Nullable<bool> Financial2 { get; set; }
        public Nullable<bool> Financial3 { get; set; }
        public Nullable<bool> Financial4 { get; set; }
        public Nullable<bool> Financial5 { get; set; }
        public Nullable<bool> FinancialRiskIndicator { get; set; }
        public Nullable<System.DateTime> PMVerificationDate { get; set; }
        public string PMVerificationNotes { get; set; }
        public Nullable<bool> License { get; set; }
        public Nullable<bool> SafetyProgram { get; set; }
        public Nullable<bool> SafetyRequirements { get; set; }
        public Nullable<bool> CompanySafety { get; set; }
        public Nullable<bool> Safety1 { get; set; }
        public Nullable<bool> Safety2 { get; set; }
        public Nullable<bool> SafetyRiskIndicator { get; set; }
        public string AvailableBondingCapacity { get; set; }
        public Nullable<bool> Insurance1 { get; set; }
        public Nullable<bool> Insurance2 { get; set; }
        public Nullable<bool> Insurance3 { get; set; }
        public Nullable<bool> Insurance4 { get; set; }
        public Nullable<bool> InsuranceRiskIndicator { get; set; }
        public string ProjectReference1 { get; set; }
        public Nullable<System.DateTime> ProjectReference1DateCalled { get; set; }
        public string ProjectReference2 { get; set; }
        public Nullable<System.DateTime> ProjectReference2DateCalled { get; set; }
        public string ProjectReference3 { get; set; }
        public Nullable<System.DateTime> ProjectReference3DateCalled { get; set; }
        public string SupplierReference1 { get; set; }
        public Nullable<System.DateTime> SupplierReference1DateCalled { get; set; }
        public string SupplierReference2 { get; set; }
        public Nullable<System.DateTime> SupplierReference2DateCalled { get; set; }
        public string SupplierReference3 { get; set; }
        public Nullable<System.DateTime> SupplierReference3DateCalled { get; set; }
        public Nullable<bool> Reference1 { get; set; }
        public Nullable<bool> Reference2 { get; set; }
        public Nullable<bool> Reference3 { get; set; }
        public Nullable<bool> ReferenceRiskIndicator { get; set; }
        public Nullable<byte> RiskFlag { get; set; }
        public string ProjectManagerName { get; set; }
        public string GroupDOCName { get; set; }
        public Nullable<System.DateTime> SREDate { get; set; }
        public Nullable<System.DateTime> SubmittedDate { get; set; }
        public Nullable<System.DateTime> CompletedDate { get; set; }
        public Nullable<System.DateTime> AwardedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ContractNo { get; set; }
        public string Reason { get; set; }
        public Nullable<System.DateTime> AvgDate { get; set; }
        public string BusinessComments { get; set; }
        public string FinanceComments { get; set; }
        public string LicenseComments { get; set; }
        public string SaftyComments { get; set; }
        public string InsuranceComments { get; set; }
        public string ReferenceComments { get; set; }
        public string Comments { get; set; }
        public Nullable<bool> Reference3NA { get; set; }
        public string RiskManagementComments { get; set; }
        public Nullable<bool> VQFLegalAnyLitigation { get; set; }
        public Nullable<bool> Business5 { get; set; }
        public Nullable<bool> SafetyCitation { get; set; }
        public Nullable<bool> Safety3 { get; set; }
        public Nullable<bool> SafetyDrugPolicy { get; set; }
        public Nullable<bool> SafetyWIPInspections { get; set; }
        public Nullable<bool> SafetyPeriodicMeetings { get; set; }
    }
}
