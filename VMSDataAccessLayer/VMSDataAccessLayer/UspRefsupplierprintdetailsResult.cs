//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VMSDAL
{
    using System;
    
    public partial class UspRefsupplierprintdetailsResult
    {
        public Nullable<long> Sno { get; set; }
        public long SupplierId { get; set; }
        public long ReferencesID { get; set; }
        public string Companyname { get; set; }
        public string Companyaddress { get; set; }
        public string City { get; set; }
        public string Statecode { get; set; }
        public string Otherstate { get; set; }
        public string Zip { get; set; }
        public string Contactname { get; set; }
        public string Phone { get; set; }
        public string AddNote { get; set; }
    }
}
