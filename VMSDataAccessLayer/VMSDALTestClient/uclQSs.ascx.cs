﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMSClassLibrary;
using System.Collections;
using System.Text.RegularExpressions;

namespace VMSDALTestClient
{
    public partial class uclQSs : System.Web.UI.UserControl
    {
        #region Properties

        private List<IndustryCertifications> _certificationCodes;
        /// <summary>
        /// List of certification codes added by user.
        /// </summary>
        public List<IndustryCertifications> CertificationCodes
        {
            get { return _certificationCodes; }
            set { _certificationCodes = value; }
        }

        private List<IndustryCertifications> _deletedCodes;
        /// <summary>
        /// List of codes that have been removed by user.
        /// </summary>
        public List<IndustryCertifications> DeletedCodes
        {
            get { return _deletedCodes; }
            set { _deletedCodes = value; }
        }


        private string _checkBoxText;
        /// <summary>
        /// Check box display text.
        /// </summary>
        public string CheckBoxText
        {
            get { return _checkBoxText; }
            set { _checkBoxText = value; }
        }

        private string _toolTipText;
        /// <summary>
        /// Tool tip text for the certification code text box.
        /// </summary>
        public string ToolTipText
        {
            get { return _toolTipText; }
            set { _toolTipText = value; }
        }

        private string _imgButtonURL;
        /// <summary>
        /// Server path of image.
        /// </summary>
        public string ImgButtonURL
        {
            get { return _imgButtonURL; }
            set { _imgButtonURL = value; }
        }
        /// <summary>
        /// Message to present on a dialog box to user.
        /// </summary>
        private string _errorMessage;
        private bool _cheked;

        public bool Cheked
        {
            get { return _cheked; }
            set { _cheked = value; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        } 
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tdQSText.Visible = false;
                Session["QSCodesAdded"] = null;
                Session["QSCodesRemoved"] = null;
                //CertificationCodes = null;

                // set element properties
                chkQS.Text = this._checkBoxText;
                chkQS.Checked = this._cheked;
                imbAddQS.ImageUrl = this._imgButtonURL;
                txtQS.ToolTip = this._toolTipText;

                if (chkQS.Checked) chkQS_CheckedChanged(chkQS, null);
                if (this.CertificationCodes != null)
                {
                    Session["QSCodesAdded"] = this.CertificationCodes;
                    LoadList();
                }
            }
        }

        private void LoadList()
        {
            grdQSList.DataSource = this.CertificationCodes;
            grdQSList.DataBind();
        }

        protected void imbAddQS_Click(object sender, ImageClickEventArgs e)
        {
            List<IndustryCertifications> temp = (List<IndustryCertifications>)Session["QSCodesAdded"];
            //List<IndustryCertifications> temp = CertificationCodes;
            CertificationCodes = (temp == null) ? new List<IndustryCertifications>() : temp;

            if (!string.IsNullOrEmpty(txtQS.Text.Trim()) && CertificationCodes.Count < 10)
            {
                if (txtQS.Text.Contains(','))
                {
                    string[] codes = txtQS.Text.Split(',');
                    for (int i = 0; i < codes.Length; i++)
                    {
                        var r = from ic in CertificationCodes where ic.CertCodeName == codes[i].Trim() select ic;
                        if (r.Count() == 0)
                        {
                            IndustryCertifications obj = new IndustryCertifications();

                            if(!string.IsNullOrEmpty(codes[i].Trim()))
                            {
                                obj.CertCodeName = codes[i].Trim();
                                obj.CertFullName = this.CheckBoxText.Trim() + " " + codes[i].Trim();
                                CertificationCodes.Add(obj);
                            };

                            // update the deleted codes list
                            DeletedCodes = Session["QSCodesRemoved"] as List<IndustryCertifications>;
                            if (DeletedCodes != null)
                            {
                                var q = (from dc in DeletedCodes where dc.CertCodeName == obj.CertCodeName select dc);
                                if (q.Count() > 0) DeletedCodes.Remove(obj);
                                Session["QSCodesRemoved"] = DeletedCodes;
                            }
                        }
                    }

                    Session["QSCodesAdded"] = CertificationCodes;
                    grdQSList.DataSource = CertificationCodes;
                    grdQSList.DataBind();
                    txtQS.Text = string.Empty;
                }
                else
                {
                    string pattern = "[(;.*?-_/\')]";
                    MatchCollection matches = Regex.Matches(txtQS.Text, pattern);

                    if (matches.Count <= 0)
                    {
                        // eliminate duplicates
                        var r = from ic in CertificationCodes where ic.CertCodeName == txtQS.Text.Trim() select ic;

                        IndustryCertifications obj = new IndustryCertifications();

                        if (r.Count() == 0)
                        {
                            obj.CertCodeName = txtQS.Text.Trim();
                            obj.CertFullName = this.CheckBoxText.Trim() + " " + txtQS.Text.Trim();
                            CertificationCodes.Add(obj);

                            // update the deleted codes list
                            DeletedCodes = Session["QSCodesRemoved"] as List<IndustryCertifications>;
                            if (DeletedCodes != null)
                            {
                                var q = (from dc in DeletedCodes where dc.CertCodeName == obj.CertCodeName select dc);
                                if (q.Count() > 0) DeletedCodes.Remove(obj);
                                Session["QSCodesRemoved"] = DeletedCodes;
                            }

                        }
                        
                        Session["QSCodesAdded"] = CertificationCodes;
                        grdQSList.DataSource = CertificationCodes;
                        grdQSList.DataBind();
                        txtQS.Text = string.Empty;
                        txtQS.Focus();
                    }
                    else
                    {
                        this.ErrorMessage = "Incorrect input format";
                    }
                } 
            }

            if (CertificationCodes.Count > 10)
            {
                //TODO:  Add a modal box to display user message.
                this.ErrorMessage = "Maximum codes allowed is 10";
                txtQS.Text = string.Empty;
            }
        }

        protected void grdQSList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                (e.Row.FindControl("imbRemoveQS") as ImageButton).Visible = true;
            }
        }

        protected void chkQS_CheckedChanged(object sender, EventArgs e)
        {
            var isChecked = (sender as CheckBox).Checked;
            if(isChecked)
            {
                tdQSText.Visible = true;
                StatusUpdatePanel.Visible = true;
            }
            else
            {
                tdQSText.Visible = false;
                txtQS.Text = string.Empty;
                StatusUpdatePanel.Visible = false;
            }

        }

        protected void grdQSList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // LINQ query logic
            string certCodeName = e.CommandArgument.ToString().Trim();
            var q = (from cc in Session["QSCodesAdded"] as List<IndustryCertifications> where cc.CertCodeName == certCodeName select cc).ToList();
            var item = (q.Count > 0) ? q.Single() : null;

            if (item != null)
            {
                // remove from list of added codes
                CertificationCodes = Session["QSCodesAdded"] as List<IndustryCertifications>;
                CertificationCodes.Remove(item);
                Session["QSCodesAdded"] = CertificationCodes;

                // add to list of removed codes
                DeletedCodes = Session["QSCodesRemoved"] as List<IndustryCertifications>;
                if (DeletedCodes == null ) DeletedCodes = new List<IndustryCertifications>();
                DeletedCodes.Add(item);
                Session["QSCodesRemoved"] = DeletedCodes;

                // rebind grid
                this.grdQSList.DataSource = CertificationCodes;
                this.grdQSList.DataBind();
            }
        }
    }
}