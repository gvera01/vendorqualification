﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestForm.aspx.cs" Inherits="VMSDALTestClient.TestForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css" >
        .expanImg
        {
            margin-left:10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table cellpadding="0" cellspacing="3" border="0" width="50%">
        <tr  >
            <td valign="top" bgcolor="#dfdfdf" style="margin: 3px" colspan="3">
              <asp:Image  ID="imgExpand" runat="server" ImageUrl="~/images/expand_blue.jpg" onclick="Javascript:OpenHide();" CssClass="expanImg" 
               onmouseover="this.style.cursor='pointer';" />

                <asp:CheckBox ID="chkContinent" runat="server" AutoPostBack="false"
                    oncheckedchanged="chkContinent_CheckedChanged" 
                    onclick="Javascript:CheckAll('chkContinent', 'grdCountries1');CheckAll('chkContinent', 'grdCountries2');CheckAll('chkContinent', 'grdCountries3');" /> 

            </td>
            
        </tr>

        <tr id="tdCountries" runat="server" style="display: none">
            <td valign="top" bgcolor="#efefef" width="30%" runat="server"> 
                <asp:GridView ID="grdCountries1" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_CountryId"
                    GridLines="None" ShowHeader="False">
                    <Columns>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCountryId" runat="server" Text='<%# Bind("PK_CountryId") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnCountryId" runat="server" Value='<%# Bind("PK_CountryId") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemStyle VerticalAlign="Top" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkCheck" runat="server"  AutoPostBack="false" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("CountryName") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server"  Text='<%# Bind("CountryName") %>'></asp:Label>
                                <asp:CheckBoxList ID="chklRegions"  runat="server" CellPadding="0"
                                    CellSpacing="0">
                                </asp:CheckBoxList>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
              </td>
                                                                                                  
            <td valign="top" bgcolor="#efefef" width="30%">
                <asp:GridView ID="grdCountries2" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_CountryId"
                    GridLines="None" ShowHeader="False">
                    <Columns>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCountryId" runat="server" Text='<%# Bind("PK_CountryId") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnCountryId" runat="server" Value='<%# Bind("PK_CountryId") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemStyle VerticalAlign="Top" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkCheck" runat="server" AutoPostBack="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("CountryName") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# Bind("CountryName") %>'></asp:Label>
                                <asp:CheckBoxList ID="chklRegions" runat="server" CellPadding="0"
                                    CellSpacing="0">
                                </asp:CheckBoxList>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>

            <td valign="top" bgcolor="#efefef" width="30%">
                            <asp:GridView ID="grdCountries3" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_CountryId"
                                GridLines="None" ShowHeader="False" >
                                <Columns>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCountryId" runat="server" Text='<%# Bind("PK_CountryId") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnCountryId" runat="server" Value='<%# Bind("PK_CountryId") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemStyle VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkCheck" runat="server" AutoPostBack="false" />
                                                                                                                                     
                                                                                                                      
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("CountryName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server"  Text='<%# Bind("CountryName") %>'></asp:Label>
                                                <asp:CheckBoxList ID="chklRegions" runat="server" CellPadding="0"
                                    CellSpacing="0">  </asp:CheckBoxList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
            </td>
        </tr>

    </table>
    </div>
    <script type="text/javascript" language="javascript" >
        function OpenHide() {

            var displayState = document.getElementById("<%=tdCountries.ClientID %>").style.display;
            if (displayState == "") {
                document.getElementById("<%=tdCountries.ClientID %>").style.display = "none";
            }
            if (displayState == "none") {
                document.getElementById("<%=tdCountries.ClientID %>").style.display = "";
            }
            CheckAll("chkContinent", "grdCountries1");
            CheckAll("chkContinent", "grdCountries2");
            CheckAll("chkContinent", "grdCountries3");
        }

        function CheckAll(cbxCheckAll, gridViewId) {
            var grdView = document.getElementById(gridViewId);
            var checkval = document.getElementById(cbxCheckAll).checked;
            var rCount = grdView.rows.length;
            var rowIdx = 0;
            for (rowIdx; rowIdx <= rCount - 1; rowIdx++) {
                var rowElement = grdView.rows[rowIdx];
                var chkBox = rowElement.cells[1].childNodes[1];
                if (chkBox.attributes == null) chkBox = rowElement.cells[1].childNodes[0]; ;
                chkBox.checked = checkval;

            }
        }
    </script>
    </form>
</body>
</html>
