﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestForm2.aspx.cs" Inherits="VMSDALTestClient.TestForm2" %>
<%@ Register Src="uclRegions.ascx" TagName="Regions" TagPrefix="IntRegions" %>
<%@ Register Src="uclISOs.ascx" TagName="ISOCodes" TagPrefix="ucl" %>
<%@ Register Src="uclQSs.ascx" TagName="QSCodes" TagPrefix="ucl2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <link href="css/Haskell.css" rel="stylesheet" type="text/css" />
    
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <IntRegions:Regions ID="uclIntRegions" runat="server" />
    </div>
    <div>
        <asp:Label ID="lblIndustryCerts" runat="server" Text="Industry Certifications:" CssClass="hdrbold"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblMsg" runat="server" CssClass="errormsg"></asp:Label>
        <br />
    </div>
    <div >
        <ucl:ISOCodes ID="uclISOCodes" runat="server" ToolTipText="You can also enter comma separated ISO codes (ex: 9001:2008, 14000)"
            CheckBoxText="  ISO" ImgButtonURL="~/images/add-button.png" />
    </div>
    <div>
        <ucl2:QSCodes ID="uclQSCodes" runat="server" ToolTipText="You can also enter comma separated QS codes (ex: 9001:2008, 14000)"
            CheckBoxText="  QS" ImgButtonURL="~/images/add-button.png" />
    </div>
    <div>
        <asp:Button ID="btnTest" runat="server" Text="Run Test" 
            onclick="btnTest_Click" />
    </div>
    <div>
        <asp:GridView ID="grdResults" runat="server" />
    </div>
    
    </form>
</body>
</html>
