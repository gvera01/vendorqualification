﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMSClassLibrary;

namespace VMSDALTestClient
{
    public partial class TestForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillCerts();
                uclIntRegions.TradeId = 36;
                //LoadIndustryCerts();
            }
        }

        private void LoadIndustryCerts()
        {
            // load controls
            uclISOs uclISO = (uclISOs)Page.LoadControl("~/uclISOs.ascx");
            uclQSs uclQS = (uclQSs)Page.LoadControl("~/uclQSs.ascx");
            
            uclISO.CheckBoxText = "  ISO";
            uclISO.ImgButtonURL = "~/images/add-button.png";
            uclISO.ToolTipText = "You can also enter comma separated ISO codes (ex: 9001:2008, 14000)";
            


            uclQS.CheckBoxText = "  QS";
            uclQS.ImgButtonURL = "~/images/add-button.png";
            uclQS.ToolTipText = "You can also enter comma separated QS codes (ex: 9001:2008, 14000)";

            // get data from database
            //TODO:  Find all the industry certs for specific vendor
            uclQS.CertificationCodes = new List<IndustryCertifications>();
            uclQS.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78525", CertFullName = "QS 7825" });
            uclQS.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78526", CertFullName = "QS 78526" });
            uclQS.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78527", CertFullName = "QS 78527" });
            uclQS.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78528", CertFullName = "QS 78528" });
            uclQS.Cheked = true;


            //rptIndustryCertControls.Controls.Add(uclISO);
            //rptIndustryCertControls.Controls.Add(uclQS);

        }

        private void FillCerts()
        {
            uclQSCodes.CertificationCodes = new List<IndustryCertifications>();
            uclQSCodes.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78525", CertFullName = "QS 7825" });
            uclQSCodes.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78526", CertFullName = "QS 78526" });
            uclQSCodes.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78527", CertFullName = "QS 78527" });
            uclQSCodes.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78528", CertFullName = "QS 78528" });

            uclQSCodes.Cheked = true;

            uclISOCodes.CertificationCodes = new List<IndustryCertifications>();
            uclISOCodes.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78525", CertFullName = "ISO 7825" });
            uclISOCodes.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78526", CertFullName = "ISO 78526" });
            uclISOCodes.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78527", CertFullName = "ISO 78527" });
            uclISOCodes.CertificationCodes.Add(new IndustryCertifications() { CertCodeName = "78528", CertFullName = "ISO 78528" });

            uclISOCodes.Cheked = true;


        }

        protected void btnTest_Click(object sender, EventArgs e)
        {
            List<VMSDAL.Country> source = this.uclIntRegions.GetSelectedCountries();
            grdResults.DataSource = source;
            grdResults.DataBind();
        }
    }
}