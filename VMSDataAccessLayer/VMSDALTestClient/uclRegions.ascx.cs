﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMSClassLibrary;
using System.Web.UI.HtmlControls;

namespace VMSDALTestClient
{
    public partial class uclRegions : System.Web.UI.UserControl
    {
        public long TradeId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BindCountries();
            }
        }

        #region Public Methods
        public void BindCountries()
        {
            List<InternationalRegions> regions = new blInternationalRegions().GetInternationalRegionsDataSource();
            dlIntRegions.DataSource = regions;
            dlIntRegions.DataBind();
        }

        public void BindCountries(long tradeId)
        {
            List<InternationalRegions> regions = new blInternationalRegions().GetInternationalRegionsDataSource();
            dlIntRegions.DataSource = regions;
            dlIntRegions.DataBind();
        }

        /// <summary>
        /// Will return a generic list of integers which represent all the selected Country primary keys (PK_CountryID)
        /// </summary>
        /// <returns></returns>
        public List<VMSDAL.Country> GetSelectedCountries()
        {
            List<VMSDAL.Country> countries = new List<VMSDAL.Country>();

            foreach (DataListItem item in dlIntRegions.Items)
            {
                DataList dlCtry = item.FindControl("dlCountries") as DataList;
                string continentName = (item.FindControl("chkContinent") as CheckBox).Text;

                foreach (DataListItem c in dlCtry.Items)
                {
                    CheckBox cbx = c.FindControl("chkCountry") as CheckBox;
                    HiddenField hdnPK = c.FindControl("hdnCountryID") as HiddenField;
                    Label lbl = c.FindControl("lblCountryName") as Label;

                    if (cbx.Checked)
                    {
                        VMSDAL.Country obj = new VMSDAL.Country();
                        obj.PK_CountryId = Convert.ToInt32(hdnPK.Value);
                        obj.CountryName = lbl.Text;
                        obj.Continent = new VMSDAL.Continent() { ContinentName = continentName.Remove(0, continentName.IndexOf('/') + 1).Trim() };
                        countries.Add(obj);
                    }
                }
            }

            return countries;
        } 
        #endregion

        #region Control Events
        protected void dlIntRegions_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            CheckBox chk = e.Item.FindControl("chkContinent") as CheckBox;
            HtmlTableRow trHeaderElement = e.Item.FindControl("trHeader") as HtmlTableRow;
            DataList countries = e.Item.FindControl("dlCountries") as DataList;
            string dlClientId = countries.ClientID;
            string chkID = chk.ClientID;
            string tdClientId = e.Item.FindControl("tdCountries").ClientID;

            chk.Text = "    All / " + chk.Text;
            chk.Attributes.Add("onclick", "Javascript:OpenHide('" + tdClientId + "');Javascript:CheckAll('" + chkID + "','" + dlClientId + "');");
            trHeaderElement.Attributes.Add("onclick", "Javascript:OpenHide('" + tdClientId + "')");

            countries.DataSource = (e.Item.DataItem as InternationalRegions).CountriesALL;
            countries.DataBind();
        }

        protected void dlCountries_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (TradeId != 0)
            {
                CheckBox chk = e.Item.FindControl("chkCountry") as CheckBox;
                HiddenField countryId = e.Item.FindControl("hdnCountryID") as HiddenField;

                var query = new blInternationalRegions().GetVQFInternationalRegions(TradeId, Convert.ToInt32(countryId.Value));
                if (query != null) chk.Checked = true;
            }
        } 
        #endregion
    }
}