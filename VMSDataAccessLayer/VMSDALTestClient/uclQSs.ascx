﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uclQSs.ascx.cs" Inherits="VMSDALTestClient.uclQSs" %>
<%@ Register Assembly="Infragistics4.WebUI.WebDataInput.v10.2, Version=10.2.20102.1011, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.WebDataInput" TagPrefix="infra" %>

<style type="text/css">
    .style1
    {
        width: 88px;
    }
</style>

<asp:UpdatePanel ID="upnlMain" runat="server" UpdateMode="Always" ViewStateMode="Enabled">
    <ContentTemplate>
        <div style="float: none; margin: 10px">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr align="left">
                    <td style="width: 10%">
                        <div style="float: left">
                            <asp:CheckBox ID="chkQS" runat="server" Checked="false" CssClass="chkall" OnCheckedChanged="chkQS_CheckedChanged"
                                AutoPostBack="true" BorderStyle="None" />
                        </div>
                        <div id="tdQSText" runat="server" style="float: left; padding-top: 10px">
                            &nbsp;&nbsp;&nbsp;
                            <asp:TextBox ID="txtQS" runat="server" Height="21px" CssClass="txtbox" Width="200px"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:ImageButton ID="imbAddQS" runat="server" Height="16px" ToolTip="Add" ImageAlign="Middle"
                                OnClick="imbAddQS_Click" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="StatusUpdatePanel" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:GridView ID="grdQSList" runat="server" ShowHeader="false" Width="290px" AutoGenerateColumns="false"
                                    GridLines="None" OnRowDataBound="grdQSList_RowDataBound" OnRowCommand="grdQSList_RowCommand">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblQSCode" runat="server" Width="45px" Text='<%# Bind("CertCodeName") %>'
                                                    CssClass="txtbox"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbRemoveQS" runat="server" ImageUrl="~/images/remove-button.png"
                                                    Visible="false" Style="height: 16px" CommandArgument='<%# Bind("CertCodeName") %>'
                                                    CommandName="DC" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

