﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMSClassLibrary;

namespace VMSDALTestClient
{
    public partial class TestForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BindCountries();
            }
        }

        private void BindCountries(string continent = "")
        {
            //List<InternationalRegions> regions = new InternationalRegions().GetInternationalRegions();

            //var temp = regions.Single(r => r.ContinentName == "South America");//(InternationalRegions)(from r in regions where r.ContinentName == "South America" select r);
            //int perColumn = (Convert.ToInt32(Math.Round(Convert.ToDouble(temp.Countries.Count / 3)))) + 1;
            //var col1 = temp.Countries.GetRange(0, perColumn);
            //var col2 = temp.Countries.GetRange(perColumn, perColumn);
            //perColumn = temp.Countries.Count - (col1.Count() + col2.Count());
            //var col3 = temp.Countries.GetRange((col1.Count() + col2.Count()), perColumn);

            //grdCountries1.DataSource = col1;
            //grdCountries2.DataSource = col2;
            //grdCountries3.DataSource = col3;
            //grdCountries1.DataBind();
            //grdCountries2.DataBind();
            //grdCountries3.DataBind();

            //chkContinent.Text = "All - " + temp.ContinentName;

        }

        protected void chkContinent_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow gr in grdCountries1.Rows)
            {
                CheckBox chk1 = (CheckBox)gr.FindControl("chkCheck");
                chk1.Checked = (sender as CheckBox).Checked;
            }
            foreach (GridViewRow gr in grdCountries2.Rows)
            {
                CheckBox chk1 = (CheckBox)gr.FindControl("chkCheck");
                chk1.Checked = (sender as CheckBox).Checked;
            }
            foreach (GridViewRow gr in grdCountries3.Rows)
            {
                CheckBox chk1 = (CheckBox)gr.FindControl("chkCheck");
                chk1.Checked = (sender as CheckBox).Checked;
            }

            if ((sender as CheckBox).Checked) tdCountries.Visible = true;
            
        }
        
    }
}