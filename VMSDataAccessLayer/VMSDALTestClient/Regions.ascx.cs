﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMSClassLibrary;

namespace VMSDALTestClient
{
    public partial class Regions : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindCountries();
        }

        public void BindCountries()
        {
            List<InternationalRegions> regions = new blInternationalRegions().GetInternationalRegionsDataSource();
            grdMain.DataSource = regions;
            grdMain.DataBind();
        }

        protected void grdMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //onclick="Javascript:OpenHide(tr);"
            //onclick="Javascript:CheckAll('chkContinent', 'grdCountries1');CheckAll('chkContinent', 'grdCountries2');CheckAll('chkContinent', 'grdCountries3');"
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Image imExpand = e.Row.FindControl("imgExpand") as Image;
                CheckBox chk = e.Row.FindControl("chkContinent") as CheckBox;
                GridView col1 = e.Row.FindControl("grdCountries1") as GridView;
                GridView col2 = e.Row.FindControl("grdCountries2") as GridView;
                GridView col3 = e.Row.FindControl("grdCountries3") as GridView;
                string grdId1 = col1.ClientID;
                string grdId2 = col2.ClientID;
                string grdId3 = col3.ClientID;
                string chkID = chk.ClientID;
                string trId = e.Row.FindControl("tdCountries").ClientID;

                chk.Attributes.Add("onclick", "Javascript:CheckAll('" + chkID + "','" + grdId1 + "');" + "Javascript:CheckAll('" + chkID + "','" + grdId2 + "');" + "Javascript:CheckAll('" + chkID + "','" + grdId3 + "');");
                imExpand.Attributes.Add("onclick", "Javascript:OpenHide('" + trId + "')");

                col1.DataSource = (e.Row.DataItem as InternationalRegions).Countries1;
                col2.DataSource = (e.Row.DataItem as InternationalRegions).Countries2;
                col3.DataSource = (e.Row.DataItem as InternationalRegions).Countries3;
                col1.DataBind();
                col2.DataBind();
                col3.DataBind();
            }


        }

    }
}