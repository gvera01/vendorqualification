﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uclRegions.ascx.cs" Inherits="VMSDALTestClient.uclRegions" %>

<%--Content--%>
<div>
    <asp:DataList ID="dlIntRegions" runat="server" Width="100%" 
        onitemdatabound="dlIntRegions_ItemDataBound">
                <ItemTemplate>
                    <table cellpadding="0" cellspacing="3" border="0" width="50%" style="font-family: Tahoma;font-size:9pt">
                        <tr onmouseover="this.style.cursor='pointer';" id="trHeader" runat="server">
                            <td valign="middle" bgcolor="#dfdfdf" style="margin: 3px; height: 30PX; width: 25%" >
                                <asp:CheckBox ID="chkContinent" runat="server" AutoPostBack="false" Text='<%# Bind("ContinentName") %>' Font-Bold="true"/>
                            </td>
                        </tr>
                        <tr id="tdCountries" runat="server" style="display: none" >
                            <td id="Td1" valign="top" bgcolor="#efefef" width="25%" runat="server" style="margin-left: 60px">
                                <asp:DataList ID="dlCountries" runat="server" Width="100%" RepeatColumns="3" RepeatDirection="Vertical"
                                    CellPadding="5" OnItemDataBound="dlCountries_ItemDataBound">
                                    <ItemTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" runat="server">
                                            <tr>
                                                <td colspan="2">
                                                    <asp:HiddenField ID="hdnCountryID" Value='<%# Bind("PK_CountryId") %>' runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <asp:CheckBox ID="chkCountry" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("CountryName") %>' />
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
    </asp:DataList>
</div>

<%--Client Scripts--%>
<script type="text/javascript" language="javascript">
    function OpenHide(control) {

        var displayState = document.getElementById(control).style.display;
        if (displayState == "") {
            document.getElementById(control).style.display = "none";
        }
        if (displayState == "none") {
            document.getElementById(control).style.display = "";
        }
    }

    function CheckAll(cbxCheckAll, listViewId) {
        var listView = document.getElementById(listViewId);
        var checkval = document.getElementById(cbxCheckAll).checked;
        var rCount = listView.rows.length;
        var rowIdx = 0;
        //column 1 of data list
        for (rowIdx; rowIdx <= rCount - 1; rowIdx++) {
            var rowElement = listView.rows[rowIdx];
            if (rowElement.cells[0].childNodes.length > 0) {
                var chkBox = rowElement.cells[0].childNodes[1].getElementsByTagName("input")[1];
                if (chkBox != null) {
                    if (chkBox.attributes == null) chkBox = rowElement.cells[0].childNodes[2].getElementsByTagName("input")[1];
                    chkBox.checked = checkval;
                }
            }
        }
        //column 2 of data list
        rowIdx = 0;
        for (rowIdx; rowIdx <= rCount - 1; rowIdx++) {
            var rowElement = listView.rows[rowIdx];
            if (rowElement.cells[1].childNodes.length > 0) {
                var chkBox = rowElement.cells[1].childNodes[1].getElementsByTagName("input")[1];
                if (chkBox != null) {
                    if (chkBox.attributes == null) chkBox = rowElement.cells[2].childNodes[2].getElementsByTagName("input")[1];
                    chkBox.checked = checkval;
                }
            }

        }
        //column 3 of data list
        rowIdx = 0;
        for (rowIdx; rowIdx <= rCount - 1; rowIdx++) {
            var rowElement = listView.rows[rowIdx];
            if (rowElement.cells[2].childNodes.length > 0) {
                var chkBox = rowElement.cells[2].childNodes[1].getElementsByTagName("input")[1];
                if (chkBox != null) {
                    if (chkBox.attributes == null) chkBox = rowElement.cells[4].childNodes[2].getElementsByTagName("input")[1];
                    chkBox.checked = checkval;
                }
            }

        }
    }
</script>
