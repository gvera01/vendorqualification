﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VMSClassLibrary;
using System.Collections;
using System.Text.RegularExpressions;

namespace VMSDALTestClient
{
    public partial class uclISOs : System.Web.UI.UserControl
    {
        #region Properties

        private List<IndustryCertifications> _certificationCodes;
        /// <summary>
        /// List of certification codes added by user.
        /// </summary>
        public List<IndustryCertifications> CertificationCodes
        {
            get { return _certificationCodes; }
            set { _certificationCodes = value; }
        }

        private List<IndustryCertifications> _deletedCodes;
        /// <summary>
        /// List of codes that have been removed by user.
        /// </summary>
        public List<IndustryCertifications> DeletedCodes
        {
            get { return _deletedCodes; }
            set { _deletedCodes = value; }
        }


        private string _checkBoxText;
        /// <summary>
        /// Check box display text.
        /// </summary>
        public string CheckBoxText
        {
            get { return _checkBoxText; }
            set { _checkBoxText = value; }
        }

        private string _toolTipText;
        /// <summary>
        /// Tool tip text for the certification code text box.
        /// </summary>
        public string ToolTipText
        {
            get { return _toolTipText; }
            set { _toolTipText = value; }
        }

        private string _imgButtonURL;
        /// <summary>
        /// Server path of image.
        /// </summary>
        public string ImgButtonURL
        {
            get { return _imgButtonURL; }
            set { _imgButtonURL = value; }
        }
        /// <summary>
        /// Message to present on a dialog box to user.
        /// </summary>
        private string _errorMessage;
        private bool _cheked;

        public bool Cheked
        {
            get { return _cheked; }
            set { _cheked = value; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tdISOText.Visible = false;
                Session["ISOCodesAdded"] = null;
                Session["ISOCodesRemoved"] = null;
                //CertificationCodes = null;

                // set element properties
                chkISO.Text = this._checkBoxText;
                chkISO.Checked = this._cheked;
                imbAddISO.ImageUrl = this._imgButtonURL;
                txtISO.ToolTip = this._toolTipText;

                if (chkISO.Checked) chkISO_CheckedChanged(chkISO, null);
                if (this.CertificationCodes != null)
                {
                    Session["ISOCodesAdded"] = this.CertificationCodes;
                    LoadList();
                }
            }
        }

        private void LoadList()
        {
            grdISOList.DataSource = this.CertificationCodes;
            grdISOList.DataBind();
        }

        protected void imbAddISO_Click(object sender, ImageClickEventArgs e)
        {
            List<IndustryCertifications> temp = (List<IndustryCertifications>)Session["ISOCodesAdded"];
            //List<IndustryCertifications> temp = CertificationCodes;
            CertificationCodes = (temp == null) ? new List<IndustryCertifications>() : temp;

            if (!string.IsNullOrEmpty(txtISO.Text.Trim()) && CertificationCodes.Count < 10)
            {
                if (txtISO.Text.Contains(','))
                {
                    string[] codes = txtISO.Text.Split(',');
                    for (int i = 0; i < codes.Length; i++)
                    {
                        var r = from ic in CertificationCodes where ic.CertCodeName == codes[i].Trim() select ic;
                        if (r.Count() == 0)
                        {
                            IndustryCertifications obj = new IndustryCertifications();

                            if (!string.IsNullOrEmpty(codes[i].Trim()))
                            {
                                obj.CertCodeName = codes[i].Trim();
                                obj.CertFullName = this.CheckBoxText.Trim() + " " + codes[i].Trim();
                                CertificationCodes.Add(obj);
                            };

                            // update the deleted codes list
                            DeletedCodes = Session["ISOCodesRemoved"] as List<IndustryCertifications>;
                            if (DeletedCodes != null)
                            {
                                var q = (from dc in DeletedCodes where dc.CertCodeName == obj.CertCodeName select dc);
                                if (q.Count() > 0) DeletedCodes.Remove(obj);
                                Session["ISOCodesRemoved"] = DeletedCodes;
                            }
                        }
                    }

                    Session["ISOCodesAdded"] = CertificationCodes;
                    grdISOList.DataSource = CertificationCodes;
                    grdISOList.DataBind();
                    txtISO.Text = string.Empty;
                }
                else
                {
                    string pattern = "[(;.*?-_/\')]";
                    MatchCollection matches = Regex.Matches(txtISO.Text, pattern);

                    if (matches.Count <= 0)
                    {
                        // eliminate duplicates
                        var r = from ic in CertificationCodes where ic.CertCodeName == txtISO.Text.Trim() select ic;

                        IndustryCertifications obj = new IndustryCertifications();

                        if (r.Count() == 0)
                        {
                            obj.CertCodeName = txtISO.Text.Trim();
                            obj.CertFullName = this.CheckBoxText.Trim() + " " + txtISO.Text.Trim();
                            CertificationCodes.Add(obj);

                            // update the deleted codes list
                            DeletedCodes = Session["ISOCodesRemoved"] as List<IndustryCertifications>;
                            if (DeletedCodes != null)
                            {
                                var q = (from dc in DeletedCodes where dc.CertCodeName == obj.CertCodeName select dc);
                                if (q.Count() > 0) DeletedCodes.Remove(obj);
                                Session["ISOCodesRemoved"] = DeletedCodes;
                            }

                        }

                        Session["ISOCodesAdded"] = CertificationCodes;
                        grdISOList.DataSource = CertificationCodes;
                        grdISOList.DataBind();
                        txtISO.Text = string.Empty;
                        txtISO.Focus();
                    }
                    else
                    {
                        this.ErrorMessage = "Incorrect input format";
                    }
                }
            }

            if (CertificationCodes.Count > 10)
            {
                //TODO:  Add a modal box to display user message.
                this.ErrorMessage = "Maximum codes allowed is 10";
                txtISO.Text = string.Empty;
            }
        }

        protected void grdISOList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                (e.Row.FindControl("imbRemoveISO") as ImageButton).Visible = true;
            }
        }

        protected void chkISO_CheckedChanged(object sender, EventArgs e)
        {
            var isChecked = (sender as CheckBox).Checked;
            if (isChecked)
            {
                tdISOText.Visible = true;
                StatusUpdatePanel.Visible = true;
            }
            else
            {
                tdISOText.Visible = false;
                txtISO.Text = string.Empty;
                StatusUpdatePanel.Visible = false;
            }

        }

        protected void grdISOList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // LINQ query logic
            string certCodeName = e.CommandArgument.ToString().Trim();
            var q = (from cc in Session["ISOCodesAdded"] as List<IndustryCertifications> where cc.CertCodeName == certCodeName select cc).ToList();
            var item = (q.Count > 0) ? q.Single() : null;

            if (item != null)
            {
                // remove from list of added codes
                CertificationCodes = Session["ISOCodesAdded"] as List<IndustryCertifications>;
                CertificationCodes.Remove(item);
                Session["ISOCodesAdded"] = CertificationCodes;

                // add to list of removed codes
                DeletedCodes = Session["ISOCodesRemoved"] as List<IndustryCertifications>;
                if (DeletedCodes == null) DeletedCodes = new List<IndustryCertifications>();
                DeletedCodes.Add(item);
                Session["ISOCodesRemoved"] = DeletedCodes;

                // rebind grid
                this.grdISOList.DataSource = CertificationCodes;
                this.grdISOList.DataBind();
            }
        }

    }
}