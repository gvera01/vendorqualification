﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Regions.ascx.cs" Inherits="VMSDALTestClient.Regions" %>

    <div>

    <asp:GridView ID="grdMain" runat="server" AutoGenerateColumns="False" 
            GridLines="None" ShowHeader="False" onrowdatabound="grdMain_RowDataBound" 
            Width="1420px">
        <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                            <table cellpadding="0" cellspacing="3" border="0" width="50%">
                                <tr  >
                                    <td valign="middle" bgcolor="#dfdfdf" style="margin: 3px; height: 30PX" colspan="3">
                                      <asp:Image  ID="imgExpand" runat="server" ImageUrl="~/images/expand_blue.jpg" CssClass="expanImg" 
                                       onmouseover="this.style.cursor='pointer';" Style=" margin-left: 10px" />

                                        <asp:CheckBox ID="chkContinent" runat="server" AutoPostBack="false" Text='<%# Bind("ContinentName") %>'
                                                       />

                                    </td>
                                </tr>

                                <tr id="tdCountries" runat="server" style="display: none">
                                    <td id="Td1" valign="top" bgcolor="#efefef" width="30%" runat="server" style="margin-left: 60px"> 
                                        <asp:GridView ID="grdCountries1" runat="server" AutoGenerateColumns="False"
                                            GridLines="None" ShowHeader="False">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtCountryId" runat="server" Text='<%# Bind("PK_CountryId") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnCountryId" runat="server" Value='<%# Bind("PK_CountryId") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <ItemStyle VerticalAlign="Top" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkCheck" runat="server"  AutoPostBack="false" />
                                                    </ItemTemplate>
                                                    <ItemStyle VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("CountryName") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server"  Text='<%# Bind("CountryName") %>'></asp:Label>
                                                        <asp:CheckBoxList ID="chklRegions"  runat="server" CellPadding="0"
                                                            CellSpacing="0">
                                                        </asp:CheckBoxList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                                                                                  
                                    <td valign="top" bgcolor="#efefef" width="30%">
                                        <asp:GridView ID="grdCountries2" runat="server" AutoGenerateColumns="False"
                                            GridLines="None" ShowHeader="False">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtCountryId" runat="server" Text='<%# Bind("PK_CountryId") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnCountryId" runat="server" Value='<%# Bind("PK_CountryId") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <ItemStyle VerticalAlign="Top" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkCheck" runat="server" AutoPostBack="false" Style="margin-left: 50px"/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("CountryName") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("CountryName") %>'></asp:Label>
                                                        <asp:CheckBoxList ID="chklRegions" runat="server" CellPadding="10"
                                                            CellSpacing="0">
                                                        </asp:CheckBoxList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>

                                    <td valign="top" bgcolor="#efefef" width="30%">
                                                    <asp:GridView ID="grdCountries3" runat="server" AutoGenerateColumns="False"
                                                        GridLines="None" ShowHeader="False" >
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtCountryId" runat="server" Text='<%# Bind("PK_CountryId") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hdnCountryId" runat="server" Value='<%# Bind("PK_CountryId") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="False">
                                                                <ItemStyle VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkCheck" runat="server" AutoPostBack="false" />
                                                                                                                                     
                                                                                                                      
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("CountryName") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblName" runat="server"  Text='<%# Bind("CountryName") %>'></asp:Label>
                                                                        <asp:CheckBoxList ID="chklRegions" runat="server" CellPadding="0"
                                                            CellSpacing="0">  </asp:CheckBoxList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                    </td>
                                </tr>

                            </table>
                    </ItemTemplate>
                </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
    <script type="text/javascript" language="javascript" >
        function OpenHide(control) {

            var displayState = document.getElementById(control).style.display;
            if (displayState == "") 
            {
                document.getElementById(control).style.display = "none";
            }
            if (displayState == "none") 
            {
                document.getElementById(control).style.display = "";
            }
        }

        function CheckAll(cbxCheckAll, gridViewId) {
            var grdView = document.getElementById(gridViewId);
            var checkval = document.getElementById(cbxCheckAll).checked;
            var rCount = grdView.rows.length;
            var rowIdx = 0;
            for (rowIdx; rowIdx <= rCount - 1; rowIdx++) {
                var rowElement = grdView.rows[rowIdx];
                var chkBox = rowElement.cells[1].childNodes[1];
                if (chkBox.attributes == null) chkBox = rowElement.cells[1].childNodes[0];
                chkBox.checked = checkval;

            }
        }
    </script>
